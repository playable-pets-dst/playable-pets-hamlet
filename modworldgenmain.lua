GLOBAL.require("map/terrain")

--TODO literally adding anything is causing it to lockup, figured out why
--[[
-----------------------------------------------------
-- Compability
-----------------------------------------------------
local Layouts = GLOBAL.require("map/layouts").Layouts
local StaticLayout = GLOBAL.require("map/static_layout")
local STRINGS = GLOBAL.STRINGS

--Add custom map tags
--Adding more tags caused a lockup... every tag must be used???
local MapTags = { "pig_test"}
    AddGlobalClassPostConstruct("map/storygen", "Story", function(self)
        for k, v in pairs(MapTags) do
            self.map_tags.Tag[v] = function(tagdata)
                return "TAG", v
            end
        end
    end)

-----------------------------------------------
-- Keys/Locks
-----------------------------------------------
--TODO figure out what this means.
local LOCKS_ARRAY = GLOBAL.LOCKS_ARRAY
local KEYS_ARRAY = GLOBAL.KEYS_ARRAY
local LOCKS_KEYS = GLOBAL.LOCKS_KEYS

local LOCKS = GLOBAL.LOCKS
local KEYS = GLOBAL.KEYS

local NEW_LOCKS_AND_KEYS = {
    "DINOFOREST",
    "DINOVALLEY",
    "DINOJUNGLE",
    "DINOMARSH",
}

for k, v in pairs(NEW_LOCKS_AND_KEYS) do
    table.insert(LOCKS_ARRAY, v)
    table.insert(KEYS_ARRAY, v)
end

LOCKS = {}
for i,v in ipairs(LOCKS_ARRAY) do
    GLOBAL.assert(LOCKS[v] == nil, "Lock "..v.." is defined twice!")
    LOCKS[v] = i
end

KEYS = {}
for i,v in ipairs(KEYS_ARRAY) do
    GLOBAL.assert(KEYS[v] == nil, "Key "..v.." is defined twice!")
    KEYS[v] = i
end

LOCKS_KEYS[LOCKS.DINOFOREST] = {KEYS.DINOFOREST}
LOCKS_KEYS[LOCKS.DINOVALLEY] = {KEYS.DINOVALLEY}
LOCKS_KEYS[LOCKS.DINOJUNGLE] = {KEYS.DINOJUNGLE}
LOCKS_KEYS[LOCKS.DINOMARSH] = {KEYS.DINOMARSH}
    
for lock,keyset in pairs(GLOBAL.LOCKS_KEYS) do
    --GLOBAL.assert(lock ~= nil and lock == LOCKS[LOCKS_ARRAY[lock]]--, "A lock in the lock_keys is misnamed!")
    --local count = 0
    --for i,key in pairs(keyset) do
       -- GLOBAL.assert(key ~= nil and key == KEYS[KEYS_ARRAY[key]], "A key in lock "..LOCKS_ARRAY[lock].." is misnamed!")
       -- count = count + 1
    --end
    --GLOBAL.assert(#keyset == count, "There appears to be an incorrectly named key in locks_keys: "..LOCKS_ARRAY[lock])
--end
------------------------------------------------------
--The Custom Stuff
------------------------------------------------------
--[[
--Pig City
Layouts["citypig_village_small"] = StaticLayout.Get("map/static_layouts/citypig_village_small")
Layouts["citypig_village_street_test"] = StaticLayout.Get("map/static_layouts/citypig_village_street_test")
GLOBAL.require("map/tasks/pigtasks")
AddRoomPreInit("PigTest", function(room)
    if not room.contents.countstaticlayouts then
        room.contents.countstaticlayouts = {}
    end
    --Add the static layouts, need to return a number.
    room.contents.countstaticlayouts["citypig_village_small"] = 1 --whats this value do? It made 3 of them.
    room.contents.countstaticlayouts["citypig_village_street_test"] = 3
end)

AddTaskSetPreInitAny(function(tasksetdata)
    if tasksetdata.location ~= "forest" then
        return
    end
    table.insert(tasksetdata.tasks, "PigCity")
end)
]]