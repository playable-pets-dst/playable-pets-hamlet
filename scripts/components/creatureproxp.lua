local function GetExcludeTags()
	if TheNet:GetPVPEnabled() then
		return  {"notarget", "wall", "laserimmune"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion", "INLIMBO", "notarget", "laserimmune"}
	else	
		return {"player", "companion", "INLIMBO", "notarget", "wall", "laserimmune"}
	end
end

local function DoTest(inst)   
	if inst:HasTag("INTERIOR_LIMBO") then
		return
	end

    local component = inst.components.creatureproxp
    if component and component.enabled and not inst:HasTag("INTERIOR_LIMBO") then      
    
        local x,y,z = inst.Transform:GetWorldPosition()

        local range = nil       

        if component.isclose then
            range = component.far
        else
            range = component.near
        end

        local musthave = { "animal","character","monster","stationarymonster"}

        if component.inventorytrigger then
            musthave = {"isinventoryitem", "monster", "animal", "character" }
        end

        local nothave = GetExcludeTags()
        local ents = TheSim:FindEntities(x,y,z, range, nil, nothave,  musthave )
        local close = nil

        for i=#ents,1,-1 do        
            if ents[i] == inst or ( component.testfn and not component.testfn(ents[i]) ) then
                table.remove(ents,i)           
            end      
        end

        if #ents > 0 and inst then 
            close = true      
            if component.inproxfn then
                for i, ent in ipairs(ents)do
                    component.inproxfn(inst,ent)
                end
            end
        end
        if component.isclose ~= close then
            component.isclose = close
            if component.isclose and component.onnear then
                component.onnear(inst, ents)
            end

            if not component.isclose and component.onfar then
                component.onfar(inst)
            end        
        end        
        if component.piggybackfn then
            component.piggybackfn(inst)
        end
    end
end

local CreatureProxp = Class(function(self, inst)
    self.inst = inst
    self.near = 2
    self.far = 3
    self.period = .333
    self.onnear = nil
    self.onfar = nil
    self.isclose = nil
    self.enabled = true    
    
    self.task = nil
    
    self:Schedule()
end)

function CreatureProxp:GetDebugString()
    return self.isclose and "NEAR" or "FAR"
end

function CreatureProxp:SetOnPlayerNear(fn)
    self.onnear = fn
end


function CreatureProxp:OnSave()
   local data = {
        enabled = self.enabled
    }
end
function CreatureProxp:OnLoad(data)
    if data.enabled then
        self.enabled = data.enabled
    end
end

function CreatureProxp:SetEnabled(enabled)
    self.enabled = enabled
    if enabled == false then
        self.isclose = nil
    end
end

function CreatureProxp:SetOnPlayerFar(fn)
    self.onfar = fn
end

function CreatureProxp:IsPlayerClose()
	return self.isclose
end

function CreatureProxp:SetDist(near, far)
    self.near = near
    self.far = far
end

function CreatureProxp:SetTestfn(testfn)
    self.testfn = testfn    
end

function CreatureProxp:forcetest()
    DoTest(self.inst)
end


function CreatureProxp:Schedule()
    if self.task then
        self.task:Cancel()
        self.task = nil
    end
	if not self.inst:IsAsleep() then
	    self.task = self.inst:DoPeriodicTask(self.period, DoTest, math.random() * self.period)
	end
end

function CreatureProxp:OnEntitySleep()
    if self.task then
        self.task:Cancel()
        self.task = nil
    end
end

function CreatureProxp:OnEntityWake()
    self:Schedule()
end

function CreatureProxp:OnRemoveEntity()
    if self.task then
        self.task:Cancel()
        self.task = nil
    end
end

return CreatureProxp
