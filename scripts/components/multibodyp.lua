local pu = require "prefabs/pugaliskp_util"

STATES = {
	IDLE = 1,
	MOVING = 2,
	DEAD = 3,
}

local Multibodyp = Class(function(self, inst)
    self.inst = inst    
    self.maxbodies = 1
    self.bodies = {}
    self.bodyprefab = "pugaliskp_body" 
    self.state = STATES.MOVING 

    self.inst:ListenForEvent("startmove", function(inst, data)
    	self:OnStartMove()
    end)
    
    self.inst:ListenForEvent("stopmove", function(inst, data)
    	self:OnStopMove()
    end)
end)


function Multibodyp:SpawnBody(angle,percent,pos)

	assert(pos) -- where the body spawns
	assert(angle) -- the direction of the travel
	assert(percent) -- how far along the travel the body should spawn in at

	local newbody = SpawnPrefab(self.bodyprefab)
	newbody.Transform:SetPosition(pos.x,pos.y,pos.z)	
	newbody.components.segmentedp:Start(angle, nil, percent)
	newbody.host = self.inst
	newbody:DoTaskInTime(0, function(inst)
		inst.host = self.inst
	end)

	table.insert(self.bodies, newbody)

	for i,body in ipairs(self.bodies) do			
		if i == #self.bodies - (PUGSEG == 3 and 1 or 2) then
			body.invulnerable = false
		else
			body.invulnerable = true
		end
	end

	if #self.bodies > self.maxbodies then
		self.bodies[1].components.segmentedp:SetToEnd()
	end
	
end

function Multibodyp:RemoveBody(body)
	for i,lbody in ipairs(self.bodies)do
		if lbody == body then
			table.remove(self.bodies,i)
			break
		end
	end
end

function Multibodyp:Setup(num,prefab)
	self.state = STATES.IDLE
	if prefab then
		self.bodyprefab = prefab
	end
	if num then
		self.maxbodies = num
	end
end

function Multibodyp:OnSave()

	local refs = {}
	local data =
	{
		bodies = {},	
	}
	
	for i,body in ipairs(self.bodies)do		
		if i ~= #self.bodies then
			--local x,y,z = body.Transform:GetWorldPosition() 
		
			--local angle = body.angle
			--table.insert(data.bodies,{angle=angle,x=x,y=y,z=z})
		end
	end

	return data, refs
end

function Multibodyp:OnLoad(data)
	if data then
		for i, body in ipairs(data.bodies) do
			--self:SpawnBody(body.angle,1,Vector3(body.x,body.y,body.z))
		end
	end
end

function Multibodyp:IsMoveState()
	return self.state == STATES.MOVING
end

function Multibodyp:OnStartMove()
	if self.state ~= STATES.MOVING and self.state ~= STATES.DEAD then
		self.state = STATES.MOVING
		--print("START MOVE")
		for i,body in ipairs(self.bodies)do
			body.components.segmentedp:StartMove()
		end
		
		if self.tail then
			self.tail.sg:GoToState("tail_exit")
		end
		
	end
end

function Multibodyp:OnStopMove()
	if self.state ~= STATES.IDLE and self.state ~= STATES.DEAD then
		self.state = STATES.IDLE
		print("STOP MOVE")
		for i,body in ipairs(self.bodies)do
			if i==1 and #self.bodies == self.maxbodies then			
				body.components.segmentedp:SetToEnd()
				body:AddTag("switchToTailProp")
			end
			body.components.segmentedp:StopMove()
		end

	end
end

function Multibodyp:Kill()
	self.state = STATES.DEAD
	self.maxbodies = 1
    self.bodies = {}
end

function Multibodyp:KillAll()
	self.state = STATES.DEAD
	for i,body in ipairs(self.bodies)do
		--if i==1 and #self.bodies == self.maxbodies then
		body.components.segmentedp.state = STATES.DEAD
		body.components.segmentedp:onhostdeath()
		--body.components.segmentedp:removeAllSegments()
		body:DoTaskInTime(5, function(inst)
		body:Remove()
		end)
		--end
		--body.components.segmentedp:StopMove()
	end
	if self.tail then
		self.tail:Remove()
	end	
	self.maxbodies = 1
    self.bodies = {}
	self.state = STATES.DEAD
end


return Multibodyp
