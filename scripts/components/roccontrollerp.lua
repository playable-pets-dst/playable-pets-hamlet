local easing = require("easing")

local SCREEN_DIST = 50
local HEAD_ATTACK_DIST = 0.1 -- 1.5
local SCALERATE = 1/(30 *2)  -- 2 seconds to go from 0 to 1

local HEADDIST = 17
local HEADDIST_TARGET = 15
local BODY_DIST_TOLLERANCE = 2

local TAILDIST = 13

local LEGDIST = 6
local LEG_WALKDIST = 4
local LEG_WALKDIST_BIG = 6
local LAND_PROX = 7

local RocControllerp = Class(function(self, inst)
    self.inst = inst    
    self.speed = 10	
    self.stages = 3
    self.startscale = 0.35   

    self.head_vel = 0
    self.head_acc = 3
    self.head_vel_max = 6
	self.body_vel = {x=0,z=0}
    self.body_acc = 0.3
    self.body_dec = 1
    self.body_vel_max = 10 --6

    self.tail_vel =  {x=0,z=0}
    self.tail_acc = 3
    self.tail_dec = 6
    self.tail_vel_max = self.speed

    self.turn_threshold = 20

    self.dungtime = 3

    self.angular_body_acc =  5  

    self.inst.sounddistance = 0  

    self.player_was_invincible = false
end)

function RocControllerp:Setup(speed, scale, stages)
	if speed then
		self.speed = speed
	end
	if scale then	
		self.startscale = scale
	end
	if stages then
		self.stages = stages		
	end

	self.inst:ListenForEvent("liftoff", function() 
			self.busy = true
			--local head = self.inst.head
			--head:PushEvent("taunt")
			
		end, self.inst) 
	
	self:setscale(self.startscale)
	
	self.inst:DoTaskInTime(0,function() 
		if not self.landed or self.liftoff then 
			--self.inst:PushEvent("fly") 
		end 
	end)

	self.inst:DoPeriodicTask(30+(math.random()*30), function() self:CheckScale() end )
	self:CheckScale()
	self.inst:DoPeriodicTask(1, function() self:CheckScale() end )
end

function RocControllerp:Start()
	self.inst:StartUpdatingComponent(self)
end

function RocControllerp:Stop()
	self.inst:StopUpdatingComponent(self)
end

function RocControllerp:CheckScale()
--	print("CHECKING SCALE",self.inst.Transform:GetScale())
	self.inst.Transform:SetScale(1, 1, 1)
	if self.inst.Transform:GetScale() ~= 1 then

		local delta = (1-self.startscale) / self.stages

		self.scaleup = {
			targetscale = math.min(self.inst.Transform:GetScale() + delta, 1)
		}
	end
end

function RocControllerp:setscale(scale)
	self.inst.Transform:SetScale(scale,scale,scale)
	if self.scalefn then
		self.scalefn(self.inst,scale)
	end
	self.inst.sounddistance = Remap(scale, self.startscale, 1, 0, 1)	
end

function RocControllerp:doliftoff()
	if self.inst.bodyparts and #self.inst.bodyparts > 0 then
		for i,part in ipairs(self.inst.bodyparts) do
			part:PushEvent("exit")
		end
		self.inst.bodyparts = nil
		self.inst.head = nil
		self.tail = nil
		self.leg1 = nil
		self.leg2 = nil
		self.liftoff = true
		self.landed = nil
		self.currentleg = nil

		self.inst:PushEvent("takeoff")
	end
end

local function CheckWaterTile(x, y, z)
	if SW_PP_MODE == true then
		if IsOnWater(x, 0, z) then
			return true
		else
			return false
		end
	else
		return TheWorld.Map:IsAboveGroundAtPoint(x, 0, z)
	end
end

function RocControllerp:Spawnbodyparts()

	if not self.inst.bodyparts then
		self.inst.bodyparts = {}
	end

	local angle = self.inst.Transform:GetRotation()*DEGREES
	local pos = Vector3(self.inst.Transform:GetWorldPosition())

	local offset = nil

	self.inst:DoTaskInTime(0.3,function()
		offset = Vector3(LEGDIST * math.cos( angle+(PI/2) ), 0, -LEGDIST * math.sin( angle+(PI/2) ))
		local leg1 = SpawnPrefab("roc_legp")
		leg1.Transform:SetPosition(pos.x + offset.x,0,pos.z + offset.z)
		--print("spawning leg at "..pos.x + offset.x..", 0, "..pos.z + offset.z)
		leg1.Transform:SetRotation(self.inst.Transform:GetRotation())
		leg1.sg:GoToState("enter")
		leg1.body = self.inst
		leg1.legoffsetdir = 3.14/2
		table.insert(self.inst.bodyparts,leg1)
		self.leg1 = leg1
		self.currentleg = self.leg1
	end)	

	self.inst:DoTaskInTime(0.3,function()
		offset = Vector3(LEGDIST * math.cos( angle-(PI/2) ), 0, -LEGDIST * math.sin( angle-(PI/2) ))	
		local leg2 = SpawnPrefab("roc_legp")
		leg2.Transform:SetPosition(pos.x + offset.x,0,pos.z + offset.z)
		--print("spawning leg2 at "..pos.x + offset.x..", 0, "..pos.z + offset.z)
		leg2.Transform:SetRotation(self.inst.Transform:GetRotation())
		leg2.sg:GoToState("enter")			
		leg2.body = self.inst
		leg2.legoffsetdir = -3.14/2
		table.insert(self.inst.bodyparts,leg2)
		self.leg2 = leg2
	end)

	self.inst:DoTaskInTime(0.5,function()								
		offset = Vector3(HEADDIST * math.cos( angle ), 0, -HEADDIST * math.sin( angle ))
		if self.inst.head then
			if TheWorld.Map:IsAboveGroundAtPoint(pos.x + offset.x,0,pos.z + offset.z) and CheckWaterTile(pos.x + offset.x, 0, pos.z + offset.z) then
				self.inst.head.Transform:SetPosition(pos.x + offset.x,0,pos.z + offset.z)
			end
			self.inst.head.Transform:SetRotation(self.inst.Transform:GetRotation())
			--may or may not let the head decide where to go
			self.inst.head.sg:GoToState("enter")
			self.inst.head.body = self.inst
		end	
	end)	

	offset = Vector3(TAILDIST * math.cos( angle -PI ), 0, -TAILDIST * math.sin( angle -PI ))
	local tail = SpawnPrefab("roc_tailp")
	tail.Transform:SetPosition(pos.x + offset.x,0,pos.z + offset.z)
	tail.Transform:SetRotation(self.inst.Transform:GetRotation())
	tail.sg:GoToState("enter")
	self.tail = tail
	table.insert(self.inst.bodyparts,tail)

end

function RocControllerp:EatSomething(food)
	food:Remove()
	print("FOOD EATEN")
end

function RocControllerp:GetTarget()

	if not self.target or not self.target:IsValid() or self.target == GetPlayer() then		
		-- look for items.. 
		local pos = Vector3(self.inst.Transform:GetWorldPosition())
		local ents = TheSim:FindEntities(pos.x,pos.y,pos.z, 20, {"structure"})

		local sorted = {}
		if #ents > 0 then
			for i, ent in ipairs(ents)do
				if ent then
					local x,y,z = ent.Transform:GetWorldPosition()
					local ground = GetWorld()

					local tile = ground.Map:GetTileAtPoint(x, y, z)

					if tile ~= GROUND.FOUNDATION and tile ~= GROUND.COBBLEROAD and tile ~= GROUND.LAWN and tile ~= GROUND.LILYPOND then --  tile ~= GROUND.FIELDS and 
						table.insert(sorted, {ent, ent:GetDistanceSqToInst(self.inst.head)})
					end
				end
			end
			if #sorted > 0 then
				table.sort( sorted, function( a, b ) return a[2] > b[2] end )				
				self.target = sorted[#sorted][1]
			end
		end		
		-- look for structures..
		-- look for player		
	end

	if self.target and self.target:IsValid() then
		return self.target
	end

	self.target = GetPlayer()
	return GetPlayer()
end

function RocControllerp:OnUpdate(dt)

	local function getanglepointtopoint(x1,z1,x2,z2)			    
	    local dz = z1 - z2
	    local dx = x2 - x1
	    local angle = math.atan2(dz, dx) / DEGREES
	    return angle				
	end	

	--[[
	local player = GetPlayer()
	local px,py,pz = player.Transform:GetWorldPosition()
	
	local ground = TheWorld
	local tile = ground.Map:GetTileAtPoint(px, py, pz)
	local onvalidtiles = true
	if tile == GROUND.FOUNDATION or tile == GROUND.COBBLEROAD or tile == GROUND.LAWN or tile == GROUND.LILYPOND or tile == GROUND.DEEPRAINFOREST or tile == GROUND.GASJUNGLE then	 --  tile == GROUND.FIELDS or
		onvalidtiles = false
	end

	local onvaliddungtiles = false
	if tile == GROUND.RAINFOREST or tile == GROUND.PLAINS then	 
		onvaliddungtiles = true
	end
	]]

	if self.scaleup then
		local currentscale = self.inst.Transform:GetScale()
		if currentscale ~= self.scaleup.targetscale then
			local setscale = math.min( currentscale + (SCALERATE*dt), self.scaleup.targetscale )
			self:setscale(setscale)			
		else 
			self.scaleup = nil
		end
	end

	if not self.busy then
		if self.inst.head and self.inst.head.components.health and not self.inst.head.components.health:IsDead() and self.tail and self.leg1 and self.leg2 then

				--local target = self:GetTarget()
				
				-- HEAD
				--Head is the true master, not the body. You can't tell what the head to do

				-- BODY
				local bodistsq = self.inst:GetDistanceSqToInst(self.inst.head) 
				local pos = Vector3(self.inst.Transform:GetWorldPosition())
				
				local BOD_VEL_MAX = self.speed
				local BOD_ACC_MAX = 0.5 --5
				local targetpos = Vector3(self.inst.head.Transform:GetWorldPosition())
				local angle = self.inst.head:GetAngleToPoint(pos)*DEGREES
				local offset = Vector3(1 * math.cos( angle ), 0, -1 * math.sin( angle ))
				offset.x = offset.x*HEADDIST_TARGET
				offset.z = offset.z*HEADDIST_TARGET
				targetpos = targetpos + Vector3(offset.x,0,offset.z)

				local bodistsq = self.inst:GetDistanceSqToPoint(targetpos) 

				if bodistsq > BODY_DIST_TOLLERANCE * BODY_DIST_TOLLERANCE then
					local cpbv = pos + Vector3(self.body_vel.x,0,self.body_vel.z)
					local angle = getanglepointtopoint(cpbv.x,cpbv.z,targetpos.x,targetpos.z)*DEGREES			
					local offset = Vector3(BOD_ACC_MAX * math.cos( angle ), 0, -BOD_ACC_MAX * math.sin( angle ))			
					local cpbvtv = cpbv + Vector3(offset.x,0,offset.z)
					local finalangle = self.inst:GetAngleToPoint(cpbvtv)*DEGREES
					local finalvel = math.min(BOD_VEL_MAX, math.sqrt(self.inst:GetDistanceSqToPoint(cpbvtv))    )
					self.body_vel = Vector3(finalvel * math.cos( finalangle ), 0, -finalvel * math.sin( finalangle ))
				else

					local angle = self.inst:GetAngleToPoint(targetpos)*DEGREES			
					local vel = math.max( math.sqrt((self.body_vel.x * self.body_vel.x) + (self.body_vel.z * self.body_vel.z)) - (BOD_ACC_MAX*dt) , 0)
					self.body_vel = Vector3(vel * math.cos( angle ), 0, -vel * math.sin( angle ))
				end
				self.inst.Transform:SetPosition(pos.x+(self.body_vel.x * dt),0,pos.z+(self.body_vel.z *dt)	)

				--TAIL
				local angle = (self.inst.Transform:GetRotation()*DEGREES) + PI
				local tailtarget =  Vector3(TAILDIST * math.cos( angle ), 0, -TAILDIST * math.sin( angle ))
				tailtarget =Vector3(self.inst.Transform:GetWorldPosition()) + tailtarget
				local taildistsq = self.tail:GetDistanceSqToPoint(tailtarget)
				local pos = Vector3(self.tail.Transform:GetWorldPosition())
				local TAIL_VEL_MAX = self.speed 
				local TAIL_ACC_MAX = 0.3 --5

				if taildistsq > 1 * 1 then
					local cpbv = pos + Vector3(self.tail_vel.x,0,self.tail_vel.z)
					local angle = getanglepointtopoint(cpbv.x,cpbv.z,tailtarget.x,tailtarget.z)*DEGREES			
					local offset = Vector3(TAIL_ACC_MAX * math.cos( angle ), 0, -TAIL_ACC_MAX * math.sin( angle ))			
					local cpbvtv = cpbv + Vector3(offset.x,0,offset.z)
					local finalangle = self.tail:GetAngleToPoint(cpbvtv)*DEGREES
					local finalvel = math.min(TAIL_VEL_MAX, math.sqrt(self.tail:GetDistanceSqToPoint(cpbvtv))    )
					self.tail_vel = Vector3(finalvel * math.cos( finalangle ), 0, -finalvel * math.sin( finalangle ))
				else

					local angle = self.tail:GetAngleToPoint(tailtarget)*DEGREES			
					local vel = math.max( math.sqrt((self.tail_vel.x * self.tail_vel.x) + (self.tail_vel.z * self.tail_vel.z)) - (TAIL_ACC_MAX*dt) , 0)
					self.tail_vel = Vector3(vel * math.cos( angle ), 0, -vel * math.sin( angle ))
				end
				self.tail.Transform:SetPosition(pos.x+(self.tail_vel.x * dt),0,pos.z+(self.tail_vel.z *dt)	)

				-- set rotations
				local headpos =Vector3(self.inst.head.Transform:GetWorldPosition())

						-- body rotation has velocity. 
						local body_angular_vel_max = 36/3
						if not self.body_angle_vel then
							self.body_angle_vel = 0
						end
						
						local targetAngle = self.inst:GetAngleToPoint(headpos)
						local currentAngle = self.inst.Transform:GetRotation()
					
						if math.abs(anglediff( currentAngle, targetAngle)) < 20 then

							if self.body_angle_vel > 0 then
								self.body_angle_vel = math.max(0, self.body_angle_vel - (self.angular_body_acc *dt))
							elseif self.body_angle_vel < 0 then
								self.body_angle_vel = math.min(0, self.body_angle_vel + (self.angular_body_acc *dt))
							end
							
						else
							if targetAngle > currentAngle then
								if targetAngle - currentAngle < 180 then
									self.body_angle_vel = math.min(body_angular_vel_max, self.body_angle_vel + (self.angular_body_acc *dt))
								else
									self.body_angle_vel = math.max(-body_angular_vel_max, self.body_angle_vel - (self.angular_body_acc *dt))
								end
							else
								if currentAngle - targetAngle < 180 then
									self.body_angle_vel = math.max(-body_angular_vel_max, self.body_angle_vel - (self.angular_body_acc *dt))							
								else
									self.body_angle_vel = math.min(body_angular_vel_max, self.body_angle_vel + (self.angular_body_acc *dt))
								end						
							end
						end 

						--print("self.body_angle_vel",self.body_angle_vel)
						currentAngle = currentAngle + (self.body_angle_vel*dt)			
						self.inst.Transform:SetRotation( currentAngle )
				

				--self.inst.head.Transform:SetRotation(self.inst.Transform:GetRotation())	
				self.tail.Transform:SetRotation(self.inst.Transform:GetRotation())	

				-- LEGS
				if not self.leg1.sg:HasStateTag("walking") and not self.leg2.sg:HasStateTag("walking") then

					local legdir = PI/2
					if self.currentleg == 2 then
						legdir = legdir * -1
					end

					local angle = self.inst.Transform:GetRotation()*DEGREES

					local currentlegtargetpos = Vector3(self.inst.Transform:GetWorldPosition()) + Vector3(LEGDIST * math.cos( angle+legdir ), 0, -LEGDIST * math.sin( angle+legdir ))
					local legdistsq = self.currentleg:GetDistanceSqToPoint(currentlegtargetpos)
					local anglediff =  anglediff(self.currentleg.Transform:GetRotation(), self.inst.Transform:GetRotation())
					if legdistsq > LEG_WALKDIST * LEG_WALKDIST or anglediff > self.turn_threshold then

						if legdistsq < LEG_WALKDIST_BIG*LEG_WALKDIST_BIG  or (anglediff > self.turn_threshold and legdistsq <= LEG_WALKDIST_BIG*LEG_WALKDIST_BIG ) then
							self.currentleg:PushEvent("walkfast")
						else
							self.currentleg:PushEvent("walk")
						end
															
						if self.currentleg == self.leg1 then
							self.currentleg = self.leg2
						else
							self.currentleg = self.leg1
						end
					end			
				end
			-- move tail to point in position like head. 
		end
	end
end

function RocControllerp:OnEntitySleep()
	self:Stop()
end

function RocControllerp:OnEntityWake()
	self:Start()
end

function RocControllerp:OnSave()	

end 

function RocControllerp:OnLoad(data)

end 

function RocControllerp:LoadPostPass(ents, data)

end

return RocControllerp