require("stategraphs/commonstates")
--require("prefabs/herald_util")

local function GetExcludeTags(inst)
	if TheNet:GetPVPEnabled() then
		return {"playerghost", "notarget", "shadow", "laserimmune", "laser", "DECOR", "INLIMBO"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion", "playerghost", "laserimmune", "laser", "DECOR", "INLIMBO"}
	else	
		return {"player", "companion", "playerghost", "notarget", "laserimmune", "laser", "DECOR", "INLIMBO"}
	end
end

local function ShakeIfClose(inst)
    ShakeAllCameras(CAMERASHAKE.VERTICAL, .3, .05, .5, inst, 20)
end

--SummonMinions
local function SummonMeteors(inst, target)	
    inst.components.combat:StartAttack()
    if target and target:IsValid() then
        local num = 4
        local offset = 6
        local targetpos = target:GetPosition()
        for i = 1, num do
            --inst:DoTaskInTime(20 - math.max((20/i), 0.01*i), function(inst)
            inst:DoTaskInTime(0.5 + (i ~= 1 and 0.2*i or 0), function(inst)
                local meteor = SpawnPrefab("herald_meteorp")
                if i == 1 then
                    meteor.Transform:SetPosition(targetpos.x, 0, targetpos.z)
                else
                    meteor.Transform:SetPosition(targetpos.x + math.random(-offset, offset), 0, targetpos.z + math.random(-offset, offset))
                end
                meteor.master = inst
            end)
        end
    end
end

local miniontable = {"shadowcreatures"}

--SummonMinions
local function SummonMinions(inst)
	local minions = inst.components.leader:CountFollowers()
	
	if minions < inst.maxminions then
			inst.taunt2 = false
			inst:DoTaskInTime(TheNet:GetServerGameMode() == "lavaarena" and PPHAM_FORGE.HERALD.SPECIALCD or 15, function(inst) inst.taunt2 = true end)
			local miniontype = "shadowcreatures" --miniontable[math.random(1, #miniontable)]
			for i = 1, TheNet:GetServerGameMode() == "lavaarena" and 2 or 3 do
				local theta = math.random() * 2 * PI
				local pt = inst:GetPosition()
				local radius = math.random(3, 6)
				local offset = FindWalkableOffset(pt, theta, radius, 3, true, true)
					if offset ~= nil then
						pt.x = pt.x + offset.x
						pt.z = pt.z + offset.z
					end
				if miniontype == "shadowcreatures" then
					if TheNet:GetServerGameMode() == "lavaarena" then
						local armor = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.BODY) or nil
						inst.components.petleash.maxpets = 6
						inst.components.petleash.petprefab = math.random(1,100) > 60 and "herald_nightmarebeak" or "herald_crawlingnightmare"
						local pet = inst.components.petleash:SpawnPetAt(pt.x, 0, pt.z)
						if armor and armor.prefab == "whisperinggrandarmor" then
							pet:PushEvent("buffpets", {buff = 2})
						end
					else
						local mob = SpawnPrefab(math.random(1,100) > 60 and "herald_nightmarebeak" or "herald_crawlingnightmare")
						mob.Transform:SetPosition(pt.x, 0, pt.z)
						inst.components.leader:AddFollower(mob)
						table.insert(inst.minions, mob)
						--mob:DoTaskInTime(20, function(inst) inst.sg:GoToState("death") end)
					end
				end	
				
				
			end
	end		
	
end

--Laser stuff

local function SetFires(x,y,z, rad)
    for i, v in ipairs(TheSim:FindEntities(x, 0, z, rad, nil, GetExcludeTags())) do 
        if v.components.burnable and MOBFIRE == "Disable" then
            v.components.burnable:Ignite()
        end
    end
end
local function DoDamage(inst, rad)
    local targets = {}
    local x, y, z = inst.Transform:GetWorldPosition()
  
    --SetFires(x,y,z, rad)
    for i, v in ipairs(TheSim:FindEntities(x, 0, z, rad, nil, GetExcludeTags(), { "_combat", "pickable", "campfire", "CHOP_workable", "HAMMER_workable", "MINE_workable", "DIG_workable" })) do  --  { "_combat", "pickable", "campfire", "CHOP_workable", "HAMMER_workable", "MINE_workable", "DIG_workable" }
        if not targets[v] and v:IsValid() and not v:IsInLimbo() and not (v.components.health ~= nil and v.components.health:IsDead()) and not v:HasTag("laserimmune") and v ~= inst and not (v.components.follower and v.components.follower.leader == inst) then            
            local vradius = 0
            if v.Physics then
                vradius = v.Physics:GetRadius()
            end

            local range = rad + vradius
            if v:GetDistanceSqToPoint(Vector3(x, y, z)) < range * range then
                local isworkable = false
                if v.components.workable ~= nil and MOBFIRE == "Disable" then
                    local work_action = v.components.workable:GetWorkAction()
                    --V2C: nil action for campfires
                    isworkable =
                        (   work_action == nil and v:HasTag("campfire")    ) or
                        
                            (   work_action == ACTIONS.CHOP or
                                work_action == ACTIONS.HAMMER or
                                work_action == ACTIONS.MINE or   
                                work_action == ACTIONS.DIG
                            )
                end
                if isworkable then
                    targets[v] = true
                    v:DoTaskInTime(0.6, function() 
                        if v.components.workable then
                            v.components.workable:Destroy(inst) 
                            local vx,vy,vz = v.Transform:GetWorldPosition()
                            v:DoTaskInTime(0.3, function() SetFires(vx,vy,vz,1) end)
                        end
                     end)
                    if v:IsValid() and v:HasTag("stump") then
                       -- v:Remove()
                    end
                elseif v.components.pickable ~= nil
                    and v.components.pickable:CanBePicked()
                    and not v:HasTag("intense") then
                    targets[v] = true
                    local num = v.components.pickable.numtoharvest or 1
                    local product = v.components.pickable.product
                    local x1, y1, z1 = v.Transform:GetWorldPosition()
                    v.components.pickable:Pick(inst) -- only calling this to trigger callbacks on the object
                    if product ~= nil and num > 0 then
                        for i = 1, num do
                            local loot = SpawnPrefab(product)
                            loot.Transform:SetPosition(x1, 0, z1)
                            targets[loot] = true
                        end
                    end

                elseif v.components.health then                    
                    inst.components.combat:DoAttack(v)                
                    if v:IsValid() then
                        if not v.components.health or not v.components.health:IsDead() then
                            if v.components.freezable ~= nil then
                                if v.components.freezable:IsFrozen() then
                                    v.components.freezable:Unfreeze()
                                elseif v.components.freezable.coldness > 0 then
                                    v.components.freezable:AddColdness(-2)
                                end
                            end
                            if v.components.temperature ~= nil then
                                local maxtemp = math.min(v.components.temperature:GetMax(), 10)
                                local curtemp = v.components.temperature:GetCurrent()
                                if maxtemp > curtemp then
                                    v.components.temperature:DoDelta(math.min(10, maxtemp - curtemp))
                                end
                            end
                        end
                    end                   
                end
                if v:IsValid() and v.AnimState then
                    SpawnPrefab("laserhitp"):SetTarget(v)
                end
            end
        end
    end
end

local function UpdateHit(inst)
    if inst:IsValid() then
        local oldflash = inst.flash
        inst.flash = math.max(0, inst.flash - .075)
        if inst.flash > 0 then
            local c = math.min(1, inst.flash)
            if inst.components.colouradder ~= nil then
                inst.components.colouradder:PushColour(inst, c, 0, 0, 0)
            else
                inst.AnimState:SetAddColour(c, 0, 0, 0)
            end
            if inst.flash < .3 and oldflash >= .3 then
                if inst.components.bloomer ~= nil then
                    inst.components.bloomer:PopBloom(inst)
                else
                    inst.AnimState:ClearBloomEffectHandle()
                end
            end
        else
            inst.flashtask:Cancel()
            inst.flashtask = nil
        end
    end
end

local function PowerGlow(inst)
    
    if inst.components.bloomer ~= nil then
        inst.components.bloomer:PushBloom(inst, "shaders/anim.ksh", -1)
    else
        inst.AnimState:SetBloomEffectHandle("shaders/anim.ksh")
    end
    inst.flash = 1.7 -- .8 + math.random() * .4
    inst.flashtask = inst:DoPeriodicTask(0, UpdateHit, nil, inst)
end

local function SpawnLaser(inst)
	inst.SoundEmitter:PlaySound("dontstarve/creatures/deerclops/laser")
    assert(inst.sg.statemem.targetpos)
    local numsteps = 10   
    local x, y, z = inst.Transform:GetWorldPosition()

    --if inst.components.combat.target then
    --    x,y,z = inst.components.combat.target.Transform:GetWorldPosition()
    --end

    local xt = inst.sg.statemem.targetpos.x
    local yt = inst.sg.statemem.targetpos.y
    local zt = inst.sg.statemem.targetpos.z

    local dist =  math.sqrt(inst:GetDistanceSqToPoint(  Vector3(xt, yt, zt)  )) -3--  math.sqrt( ) ) - 2

    local angle = (inst:GetAngleToPoint(xt, yt, zt) +90)* DEGREES

    local step = .75   
    local ground = TheWorld.Map
    local targets, skiptoss = {}, {}
    local i = -1
    local noground = false
    local fx, delay, x1, z1
    while i < numsteps do
        i = i + 1
        dist = dist + step
        delay = math.max(0, i - 1)
        x1 = x + dist * math.sin(angle)
        z1 = z + dist * math.cos(angle)
        local tile = ground:GetTileAtPoint(x1, 0, z1)
        
        if tile == 255 or tile < 2 then
            if i <= 0 then
                return
            end
            noground = true
        end
        fx = SpawnPrefab(i > 0 and "laserp" or "laserp")
        fx.caster = inst
        fx.Transform:SetPosition(x1, 0, z1)
        fx:Trigger(delay * FRAMES, targets, skiptoss)
        if i == 0 then
        --    ShakeAllCameras(CAMERASHAKE.FULL, .7, .02, .6, fx, 30)
        end
        if noground then
            break
        end
    end
--[[
    if i < numsteps then
        dist = (i + .5) * step + offset
        x1 = x + dist * math.sin(angle)
        z1 = z + dist * math.cos(angle)
    end
    ]]

    local function delay_spawn(delay_offset)
        fx = SpawnPrefab("laserp")
        fx.Transform:SetPosition(x1, 0, z1)
        fx:Trigger((delay + delay_offset) * FRAMES, targets, skiptoss)
    end

    delay_spawn(1)
    delay_spawn(2)
end

local function SetLightValue(inst, val)
    if inst.Light ~= nil then
        inst.Light:SetIntensity(.6 * val * val)
        inst.Light:SetRadius(5 * val)
        inst.Light:SetFalloff(3 * val)
    end
end

local function SetLightValueAndOverride(inst, val, override)
    if inst.Light ~= nil then
        inst.Light:SetIntensity(.6 * val * val)
        inst.Light:SetRadius(5 * val)
        inst.Light:SetFalloff(3 * val)
        inst.AnimState:SetLightOverride(override)
    end
end

local function SetLightColour(inst, val)
    if inst.Light ~= nil then
        inst.Light:SetColour(val, 0, 0)
    end
end

local longaction = "action"
local shortaction = "action"
local workaction = "action"
local otheraction = "action"

local actionhandlers = PP_CommonStates.GetCommonActions(longaction, shortaction, workaction, otheraction)

local actionhandler_overrides = 
{ 
	ActionHandler(ACTIONS.ATTACK, function(inst)
		local hands = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS) or nil
		if TheNet:GetServerGameMode() == "lavaarena" and hands and hands:HasTag("magicweapon") then
			return "action_attack"
		else
			return "attack"
		end
	
	end),
	ActionHandler(ACTIONS.CASTAOE, "action_aoe"),
	ActionHandler(ACTIONS.REVIVE_CORPSE, "failaction"), --no reviving
}

if #actionhandler_overrides > 0 then
	for i, v in ipairs(actionhandler_overrides) do
		table.insert(actionhandlers, v)
	end
end

local events=
{
	EventHandler("attacked", function(inst, data) 
		if data.stimuli and data.stimuli == "strong" and not (inst.sg:HasStateTag("transform") or inst.sg:HasStateTag("nointerrupt")) and not inst.components.health:IsDead() then
			inst.sg:GoToState("hit")
		elseif data.stimuli and (data.stimuli == "electric" or data.stimuli == "explosive") and not inst.components.health:IsDead() then
			inst.sg:GoToState("hit")
		elseif not inst.components.health:IsDead() and not inst.sg:HasStateTag("busy") and not inst.sg:HasStateTag("nointerrupt") and TheNet:GetServerGameMode() ~= "lavaarena" and (inst.sg.mem.last_hit_time or 0) + 1 < GetTime() then 
			inst.sg:GoToState("hit")
		end 
	end),
	PP_CommonHandlers.AddCommonHandlers(),
    EventHandler("death", function(inst, data)
        if inst.sleepingbag ~= nil and (inst.sg:HasStateTag("bedroll") or inst.sg:HasStateTag("tent")) then -- wakeup on death to "consume" sleeping bag first
            inst.sleepingbag.components.sleepingbag:DoWakeUp()
            inst.sleepingbag = nil
        end

        if (data ~= nil and data.cause == "file_load") or inst.components.revivablecorpse ~= nil then
            inst.sg:GoToState("corpse", true)
        else
            inst.sg:GoToState("death")
        end
    end),
    EventHandler("doattack", function(inst, data) if not inst.components.health:IsDead() and (inst.sg:HasStateTag("hit") or not inst.sg:HasStateTag("busy")) then inst.sg:GoToState("attack", data.target) end end),
    CommonHandlers.OnSleep(),
    PP_CommonHandlers.OnLocomoteAdvanced(),
    CommonHandlers.OnFreeze(),
	
	 EventHandler("ms_opengift",
        function(inst)
            if not inst.sg:HasStateTag("busy") then
                inst.sg:GoToState("opengift")
            end
        end),
		
	EventHandler("respawnfromghost", function(inst)  
			if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:Enable(true)
            end

            inst.components.health:SetInvincible(false)
            inst:ShowHUD(true)
            inst:SetCameraDistance()

            SerializeUserSession(inst) end),	
}

if CurrentRelease.GreaterOrEqualTo(ReleaseID.IDs.R08_ROT_TURNOFTIDES) then
	table.insert(events, PP_CommonHandlers.OnSink())
end


 local states=
{

    State{
        name = "idle",
        tags = {"idle", "canrotate"},
        
        onenter = function(inst)
            inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation("idle", true)
        end,
    },
	
	State
    {
        name = "appear",
        tags = {"busy"},
        onenter = function(inst)
            inst.AnimState:PlayAnimation("appear")
            inst.SoundEmitter:PlaySound("dontstarve/ghost/ghost_howl")
            inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/ancient_herald/appear")

        end,
        
        events=
        {
            EventHandler("animover", function(inst, data) 
                inst.sg:GoToState("idle") 
            end)
        },
        
    }, 
	
	State{
        name = "hit",
        tags = {"busy"},
        
        onenter = function(inst)
            inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/ancient_herald/hit")
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("hit")
			inst.sg.mem.last_hit_time = GetTime()
        end,
        
        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end ),
        },
    },
    
	 State{
        name = "taunt",
        tags = {"busy","canrotate"},
        
        onenter = function(inst)
            inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation("taunt")
        end,

        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },        


        timeline =
        {
			TimeEvent(25*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/ancient_herald/taunt") end),
        },
    },
	
    State{
        name = "special_atk1",
        tags = {"busy","canrotate"},
        
        onenter = function(inst)
            inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation("taunt")
        end,

        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },        


        timeline =
        {
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/ancient_herald/taunt") end),
        },


    },

    State
    {
        name = "attack", --Summon meteors
        tags = {"busy"},
        onenter = function(inst)
            local buffaction = inst:GetBufferedAction()
			local target = buffaction ~= nil and buffaction.target or nil
            if target ~= nil then
				if target:IsValid() then
                inst:ForceFacePoint(target:GetPosition())
                inst.sg.statemem.attacktarget = target
				end
			end
            inst.components.combat:StartAttack()
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("summon")
            inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/ancient_herald/summon")
            inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/ancient_herald/summon_2d")
        end,
        
        timeline=
        {
            TimeEvent(30*FRAMES, function(inst)
                SummonMeteors(inst, inst.sg.statemem.attacktarget)
            end)
        },

        events=
        {
            EventHandler("animover", function(inst, data) inst.sg:GoToState("idle") end)
        },
    },
	
	State
    {
        name = "special_atk2",
        tags = {"busy"},
        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("summon")
            inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/ancient_herald/summon")
            inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/ancient_herald/summon_2d")
        end,
        
        timeline=
        {
            TimeEvent(30*FRAMES, function(inst)
                SummonMinions(inst)
            end)
        },

        events=
        {
            EventHandler("animover", function(inst, data) inst.sg:GoToState("idle") end)
        },
    },
	
	State
    {
        name = "action_aoe",
        tags = {"busy"},
        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("summon")
            inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/ancient_herald/summon")
            inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/ancient_herald/summon_2d")
        end,
        
        timeline=
        {
            TimeEvent(30*FRAMES, function(inst)
                inst:PerformBufferedAction()
            end)
        },

        events=
        {
            EventHandler("animover", function(inst, data) inst.sg:GoToState("idle") end)
        },
    },
	
	State
    {
        name = "action_attack", --For Forge
        tags = {"attack", "busy"},
        onenter = function(inst, target)
			local buffaction = inst:GetBufferedAction()
            local target = buffaction ~= nil and buffaction.target or nil
            inst.components.combat:SetTarget(target)
			if target ~= nil and target:IsValid() then
                inst:ForceFacePoint(target.Transform:GetWorldPosition())
                inst.sg.statemem.attacktarget = target
            end
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("walk_pst")
			inst.AnimState:SetTime(8 * FRAMES)
            --inst:PerformBufferedAction()
        end,
        
        timeline=
        {
			TimeEvent(2*FRAMES, function(inst)
                inst:PerformBufferedAction()
            end)
        },

        events=
        {
            EventHandler("animover", function(inst, data) inst.sg:GoToState("idle") end)
        },
    },
	
	State{
        name = "special_atk3",
        tags = {"attack", "busy", "autopredict"},
        
        onenter = function(inst, target)
            if inst.components.locomotor then
                inst.components.locomotor:StopMoving()
            end
            inst.components.combat:StartAttack()
            inst.AnimState:PlayAnimation("attack")
			inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/ancient_herald/attack")
            inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/ancient_herald/attack_2d")
        end,
        
        timeline = 
        {
            TimeEvent(20*FRAMES, function(inst)
                local ring = SpawnPrefab("laser_ringp")
                ring.Transform:SetPosition(inst.Transform:GetWorldPosition())
                ring.Transform:SetScale(1.1, 1.1, 1.1)
                DoDamage(inst, 6)
            end)
        }, 
        
        onexit = function(inst)
            inst.taunt3 = false
            if inst._taunt3_task then
                inst._taunt3_task:Cancel()
                inst._taunt3_task = nil
            end
            inst._taunt3_task = inst:DoTaskInTime(5, function(inst) inst.taunt3 = true end)
        end,

        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
    State{
        name = "death",
        tags = {"busy"},

        onenter = function(inst)
			inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/ancient_herald/death")
            inst.AnimState:PlayAnimation("death")
            inst.Physics:Stop()
            RemovePhysicsColliders(inst)     
			inst.components.lootdropper:DropLoot(Vector3(inst.Transform:GetWorldPosition()))
			inst.components.inventory:DropEverything(true)
        end,
		
		timeline=
        {
			
        },

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.noskeleton = true
					PlayablePets.DoDeath(inst)
                end
            end),
        },

    },

	
	State
    {
        name = "run_start",
        tags = { "moving", "running", "canrotate" },

        onenter = function(inst)
			inst.components.locomotor:WalkForward()
            inst.AnimState:PlayAnimation("walk_pre")
			
        end,
		
		timeline = 
		{

		},
		
		onexit = function(inst)

		end,
		
        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("run") end ),
        },
    },
	
	State
    {
        name = "run",
        tags = { "moving", "running", "canrotate" },

        onenter = function(inst)
			--inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/run")
			--inst.components.locomotor:WalkForward()
			inst.AnimState:PlayAnimation("walk_loop")
        end,
		
		timeline = 
		{
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/ancient_herald/breath_in") end),
			TimeEvent(17*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/ancient_herald/breath_out") end),   
		},
		
		onexit = function(inst)
			inst.SoundEmitter:KillSound("robo_walk_LP")
		end,
		
        events=
			{
				EventHandler("animqueueover", function(inst) 
					inst.sg:GoToState("run")  
				end ),
			},

    },
	
	State
    {
        name = "run_stop",
        tags = { "idle" },

        onenter = function(inst) 
            inst.components.locomotor:StopMoving()
			inst.AnimState:PlayAnimation("walk_pst")
			--inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/step")            
        end,

		timeline= 
        {

        },
		
		onexit = function(inst)

		end,
		
        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end ),
        },
    },	
}

PP_CommonStates.AddActionStates(states, 
	{
		TimeEvent(0 * FRAMES, function(inst)
            inst:PerformBufferedAction()
        end),
	}, 
	"walk_pst", nil, nil, "summon", "walk_pst") --(states, timelines, anim, anim2, enteranim, loopanim, exitanim, noanim, events)
PP_CommonStates.AddCorpseStates(states, true, 
	--timelines = 
	{
		corpse =
		{
			TimeEvent(0*FRAMES, function(inst) 
				inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/ancient_herald/death")
			end),
		},
		
		corpse_taunt =
		{
			TimeEvent(0*FRAMES, function(inst) 
				inst.SoundEmitter:PlaySound("dontstarve/ghost/ghost_howl")
				inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/ancient_herald/appear")
			end),
		},
	
	},
	--anims = 
	{
		corpse = "death",
		corpse_taunt = "appear"
	},
	--sounds =
	{

	},
	--fns =
	{

	}
	) ---(states, isflying, timelines, anims, sounds, fns, events, nofx)
PP_CommonStates.AddJumpInStates(states, nil, "walk_pst")
PP_CommonStates.AddOpenGiftStates(states, "idle")
PP_CommonStates.AddSailStates(states, {}, "walk_pst", "idle")
local simpleanim = "walk_pst"
local simpleidle = "idle"
local simplemove = "walk"
if CurrentRelease.GreaterOrEqualTo(ReleaseID.IDs.R08_ROT_TURNOFTIDES) then
CommonStates.AddHopStates(states, false, {pre = simplemove.."_pre", loop = simplemove.."_loop", pst = simplemove.."_pst"}, nil, "death")
end
PP_CommonStates.AddSailorStates(states, nil, 
{
	plank_idle = simpleanim,
	plank_idle_loop = simpleidle,
	plank_idle_pst = simpleanim,
	
	plank_hop_pre = simplemove.."_pre",
	plank_hop = simplemove.."_loop",
	
	steer_pre = simpleanim,
	steer_idle = simpleidle,
	steer_turning = simpleanim,
	stop_steering = simpleanim,
	
	row = "walk_pst",
}
)


    
return StateGraph("heraldp", states, events, "appear", actionhandlers)

