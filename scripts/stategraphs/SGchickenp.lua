require("stategraphs/commonstates")
require("stategraphs/ppstates")

local function GetExcludeTags(inst)
	if TheNet:GetPVPEnabled() then
		return {"INLIMBO", "playerghost", "notarget", "shadow", "groundpoundimmune"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion", "playerghost", "INLIMBO", "groundpoundimmune"}
	else	
		return {"player", "companion", "INLIMBO", "playerghost", "notarget", "shadow", "groundpoundimmune"}
	end
end

local function ShakeIfClose(inst)
    ShakeAllCameras(CAMERASHAKE.VERTICAL, .15, .02, .5, inst, 20)
end

local longaction = "action"
local shortaction = "action"
local workaction = "action"
local otheraction = "eat_pre"

local actionhandlers = PP_CommonStates.GetCommonActions(longaction, shortaction, workaction, otheraction)

local actionhandler_overrides = 
{ 

}

if #actionhandler_overrides > 0 then
	for i, v in ipairs(actionhandler_overrides) do
		table.insert(actionhandlers, v)
	end
end

local events=
{
	EventHandler("attacked", function(inst, data) 
		if data.stimuli and data.stimuli == "strong" and not (inst.sg:HasStateTag("transform") or inst.sg:HasStateTag("nointerrupt")) and not inst.components.health:IsDead() then
			inst.sg:GoToState("hit")
		elseif data.stimuli and (data.stimuli == "electric" or data.stimuli == "explosive") and not inst.components.health:IsDead() then
			inst.sg:GoToState("hit", data.stimuli)
		elseif not inst.components.health:IsDead() and not inst.sg:HasStateTag("busy") and not inst.sg:HasStateTag("nointerrupt") then 
			inst.sg:GoToState("hit") 
		end 
	end),
	PP_CommonHandlers.AddCommonHandlers(),
    EventHandler("doattack", function(inst, data) if not inst.components.health:IsDead() and (inst.sg:HasStateTag("hit") or not inst.sg:HasStateTag("busy")) then inst.sg:GoToState("attack", data.target) end end),
    CommonHandlers.OnSleep(),
    --CommonHandlers.OnLocomote(true,false),
    CommonHandlers.OnFreeze(),
	PP_CommonHandlers.OpenGift(),
	PP_CommonHandlers.OnKnockback(),
	EventHandler("locomote", 
        function(inst) 
            if not inst.sg:HasStateTag("idle") and not inst.sg:HasStateTag("moving") then return end
            
            if not inst.components.locomotor:WantsToMoveForward() then
                if not inst.sg:HasStateTag("idle") then
                    if not inst.sg:HasStateTag("running") then
                        inst.sg:GoToState("idle")
                    end

                    inst.sg:GoToState("idle")
                end
            elseif inst.components.locomotor:WantsToRun() then
                if not inst.sg:HasStateTag("hopping") then
                    inst.sg:GoToState("hop")
                end
            else
                if not inst.sg:HasStateTag("hopping") then
                    inst.sg:GoToState("hop")
                end
            end
        end),
	PP_CommonHandlers.OnDeath(),
		
	EventHandler("respawnfromghost", function(inst)  
			if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:Enable(true)
            end

            inst.components.health:SetInvincible(false)
            inst:ShowHUD(true)
            inst:SetCameraDistance()

            SerializeUserSession(inst) end),	
}

if CurrentRelease.GreaterOrEqualTo(ReleaseID.IDs.R08_ROT_TURNOFTIDES) then
	table.insert(events, PP_CommonHandlers.OnSink())
	table.insert(events, CommonHandlers.OnHop())
end

 local states=
{
    State
    {
        name = "idle",
        tags = {"idle", "canrotate"},

        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("idle")                             
        end,
		
		timeline = 
        {

        },
        
        events =
        {
            EventHandler("animover", function(inst, data) inst.sg:GoToState("idle") end),
        },
    },

	State
    {
        name = "attack",
        tags = {"attack", "busy"},
        
        onenter = function(inst, cb)
			if inst._isflying then
				inst.sg:GoToState("idle")
			else
				inst.Physics:Stop()
				inst.components.combat:StartAttack()
				inst.AnimState:PlayAnimation("peck")
			end
        end,

        timeline =
        {
            TimeEvent(10*FRAMES, function(inst) inst:PerformBufferedAction() end),
        },

        events =
        {
            EventHandler("animover", function(inst, data) inst.sg:GoToState("idle") end),
        },
    },
	
	 State
    {
        name = "hop",
        tags = {"moving", "canrotate", "hopping"},
        
        timeline =
        {
            TimeEvent(5*FRAMES, function(inst) 
				if not inst._isflying then
					inst.Physics:Stop() 
					inst.SoundEmitter:PlaySound("dontstarve/rabbit/hop")
				end
            end ),
        },
        
        onenter = function(inst) 
            inst.AnimState:PlayAnimation("hop")
            inst.components.locomotor:WalkForward()
        end,
        
        onupdate = function(inst)
            if not inst.components.locomotor:WantsToMoveForward() then
                inst.sg:GoToState("idle")
            end
        end,        
        
        events =
        {
            EventHandler("animover",
                function(inst, data)
                    inst.sg:GoToState("idle")
                end),
        }
    },

    State
    {
        name = "eat_pre",

        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("eat_pre", false)
        end,

        timeline =
        {
            TimeEvent(10*FRAMES, function(inst) inst:PerformBufferedAction() end),
        },

        events =
        {
            EventHandler("animover",
                function(inst, data)
                    inst.sg:GoToState("eat")
                end),
        }
    },

    State
    {
        name = "eat",

        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("eat", true)
            inst.sg:SetTimeout(1)
        end,

        ontimeout = function(inst)
            inst.sg:GoToState("eat_pst")
        end,
    },

    State
    {
        name = "eat_pst",

        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PushAnimation("eat_pst")
        end,

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State
    {
        name = "hit",
        tags = {"busy"},
        
        onenter = function(inst)
            inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/piko/scream")
            inst.AnimState:PlayAnimation("hit")
            inst.Physics:Stop()            
        end,

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },        
    },
	
	State
    {
        name = "special_atk1",
        tags = {"idle"},

        onenter = function(inst)
			if inst._isflying then
				inst.sg:GoToState("idle")
			else
				inst.Physics:Stop()
				inst.AnimState:PlayAnimation("honk")
			end
        end,

        events =
        {
            EventHandler("animover", function(inst, data)
                inst.sg:GoToState("idle")
            end),
        }
    },
	
	 State{
        name = "special_atk2",
        tags = {"flight", "busy"}, --sliding happens
        onenter = function(inst)
			if inst.taunt2 == nil or inst.taunt2 == false then
				inst.sg:GoToState("idle")
			end
			inst.taunt2 = false
            inst.components.locomotor:StopMoving()
            inst.sg:SetTimeout(.1+math.random()*.2)
            inst.sg.statemem.vert = math.random() > .5
			
			inst.Physics:ClearCollisionMask()
			inst.Physics:CollidesWith(COLLISION.WORLD)
			if TheNet:GetServerGameMode() ~= "lavaarena" and CurrentRelease.GreaterOrEqualTo(ReleaseID.IDs.R08_ROT_TURNOFTIDES) then
				inst.Physics:ClearCollidesWith(COLLISION.LIMITS)
			end
			
	        inst.DynamicShadow:Enable(false)
			inst.components.health:SetInvincible(true)
            inst.AnimState:PlayAnimation("takeoff_vertical_pre")
			inst.AnimState:PushAnimation("takeoff_vertical_loop", true)
        end,
        
        ontimeout= function(inst)
			 inst.components.locomotor.disable = true
             inst.Physics:SetMotorVel(0,15+math.random()*5,0)
        end,
        
        timeline = 
        {
            TimeEvent(1.5, function(inst) 
				inst:Hide()
				inst._isflying = true
				inst.components.locomotor.disable = false
				if TheNet:GetServerGameMode() == "lavaarena" then
					inst:DoTaskInTime(5, function(inst) if inst._isflying == true and not inst.sg:HasStateTag("landing") then inst.sg:GoToState("land") end end)
				end	
				inst.sg:GoToState("glide") 
			end)
        }
        
    },	
	
	State{
        name = "land",
        tags = {"busy", "flight", "landing"},
        onenter= function(inst)
			if inst.amphibious or not CheckWaterTile(inst:GetPosition()) then
				inst.components.locomotor:StopMoving()
				inst:Show()
				local pt = Point(inst.Transform:GetWorldPosition())
				pt.y = 25
				inst.Physics:Stop()
				inst.components.locomotor:StopMoving()
				inst.Physics:Teleport(pt.x,pt.y,pt.z)
				inst.components.locomotor:StopMoving()
				inst.AnimState:PlayAnimation("glide", true)
				inst.sg:SetTimeout(inst.AnimState:GetCurrentAnimationLength())
			
				inst:RemoveTag("notarget")
				inst.components.locomotor:StopMoving()
				inst.components.locomotor.disable = true
				inst.Physics:SetMotorVel(0,-20+math.random()*10,0)
				inst.SoundEmitter:PlaySound("dontstarve/birds/flyin", "flyin")
			else
				----print("DEBUG: failed, going to glide state")
				inst.sg:GoToState("glide")
			end
        end,
		
		onexit = function(inst)
			if inst.amphibious or not CheckWaterTile(inst:GetPosition()) then --this should assume that the state failed, thus not setting the mobs to landform internally
				inst._isflying = false
				inst:DoTaskInTime(10, function(inst) inst.taunt2 = true end)
				inst.components.health:SetInvincible(false)
				inst.components.locomotor.disable = false
			end
		end,	
		
		 timeline = 
        {
            TimeEvent(5, function(inst) --added as a failsafe. Teleports to the ground if takes too long.
				local pt = Point(inst.Transform:GetWorldPosition())
				pt.y = 0
                inst.components.locomotor:StopMoving()
                inst.Physics:Teleport(pt.x,pt.y,pt.z)
                inst.AnimState:PlayAnimation("land")
	            inst.DynamicShadow:Enable(true)
				inst.components.health:SetInvincible(false)
                inst.sg:GoToState("idle", true)
			end),
        },
        
        onupdate= function(inst)
            local pt = Point(inst.Transform:GetWorldPosition())
            inst.Physics:SetMotorVel(0,-20+math.random()*10,0)
            if pt.y <= 1 then
                pt.y = 0
                inst.components.locomotor:StopMoving()
                inst.Physics:Teleport(pt.x,pt.y,pt.z)
                inst.AnimState:PlayAnimation("land")
				if not inst.amphibious then
					ChangeToCharacterPhysics(inst)
				end
	            inst.DynamicShadow:Enable(true)
				inst.components.health:SetInvincible(false)
                inst.sg:GoToState("idle", true)
            end
        end,
		

        ontimeout = function(inst)
             inst.SoundEmitter:PlaySound("dontstarve/birds/flyin", "flyin")
             --inst.sg:GoToState("idle")
        end,
    },

	State{
        name = "glide",
        tags = {"flight2", "canrotate"},
        onenter= function(inst)
			
			local pt = Point(inst.Transform:GetWorldPosition())
			if pt.y > .1 then
                pt.y = 0
                inst.Physics:Stop()
                inst.Physics:Teleport(pt.x,pt.y,pt.z)
				
			end	
			inst._isflying = true
			--inst.Physics:SetMotorVel(8,0,0)
            inst.AnimState:PlayAnimation("idle", true)
        end,
        
        onupdate= function(inst)
        end,
		

        ontimeout = function(inst)
            inst.SoundEmitter:PlaySound("dontstarve/birds/flyin", "flyin")
        end,
    },
	
    State{
        name = "death",
        tags = {"busy"},

        onenter = function(inst)
			inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/piko/scream")
            inst.AnimState:PlayAnimation("death")
            inst.Physics:Stop()
            RemovePhysicsColliders(inst)     
			inst.components.lootdropper:DropLoot(Vector3(inst.Transform:GetWorldPosition()))
			inst.components.inventory:DropEverything(true)
        end,
		
		timeline=
        {
			--TimeEvent(1*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/pog/death") end),
        },

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
					PlayablePets.DoDeath(inst)
                end
            end),
        },

    },
	
	State {
        name = "sleep",
        tags = { "sleeping", "busy" }, --add tag "busy" if you hate sliding

        onenter = function(inst)
			if inst._isflying then
				inst.sg:GoToState("idle")
			else
				if inst.components.locomotor ~= nil then
					inst.components.locomotor:StopMoving()
				end
				inst.AnimState:PlayAnimation("sleep_pre")
			end
        end,

        timeline = 
		{
			--TimeEvent(1*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/pog/yawn") end)
		},

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("sleeping") end ),
            EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "sleeping",
        tags = { "sleeping", "busy" },

        --onenter = onentersleeping,
		
		onenter = function(inst)
				inst.components.locomotor:StopMoving()
				PlayablePets.SleepHeal(inst)
				inst.AnimState:PlayAnimation("sleep_loop")
			end,
			
		onexit = function(inst)
		--inst.components.sanity.dapperness = -0.5
		end,

		timeline=
        {
			--TimeEvent(37*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/catcoon/sleep",nil,.25) end)
        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("sleeping") end ),
			EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "wake",
        tags = { "busy", "waking" },

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pst")
            if inst.components.sleeper ~= nil and inst.components.sleeper:IsAsleep() then
                inst.components.sleeper:WakeUp()
            end
            --if fns ~= nil and fns.onwake ~= nil then
                --fns.onwake(inst)
            --end
        end,

        --timeline = timelines ~= nil and timelines.waketimeline or nil,

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	
}

CommonStates.AddFrozenStates(states)

PP_CommonStates.AddKnockbackState(states, nil, "hit") --(states, timeline, anim, sounds, fns)
PP_CommonStates.AddActionStates(states, 
	{
		TimeEvent(0 * FRAMES, function(inst)
            inst:PerformBufferedAction()
        end),
	}, 
	"peck", nil, "eat_pre", "eat", "eat_pst") --(states, timelines, anim, anim2, enteranim, loopanim, exitanim, noanim, events)
PP_CommonStates.AddCorpseStates(states, true, 
	--timelines = 
	{
		corpse =
		{
			--TimeEvent(1*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/pog/death") end),
		},
		
		corpse_taunt =
		{

		},
	
	},
	--anims = 
	{
		corpse = "death",
		corpse_taunt = "idle"
	},
	--sounds =
	{
		corpse = "dontstarve_DLC003/creatures/piko/scream"
	},
	--fns =
	{

	}
	) ---(states, isflying, timelines, anims, sounds, fns, events, nofx)
PP_CommonStates.AddJumpInStates(states, nil, "eat_pst")
PP_CommonStates.AddOpenGiftStates(states, "idle")
PP_CommonStates.AddSailStates(states, {}, "run_pst", "idle")
local simpleanim = "land"
local simpleidle = "idle"
local simplemove = "hop"
if CurrentRelease.GreaterOrEqualTo(ReleaseID.IDs.R08_ROT_TURNOFTIDES) then
CommonStates.AddHopStates(states, false, {pre = simplemove, loop = simplemove, pst = simplemove}, nil, "death")
end
PP_CommonStates.AddSailorStates(states, nil, 
{
	plank_idle = simpleanim,
	plank_idle_loop = simpleidle,
	plank_idle_pst = simpleanim,
	
	plank_hop_pre = simplemove.."_pre",
	plank_hop = simplemove,
	
	steer_pre = simpleanim,
	steer_idle = simpleidle,
	steer_turning = simpleanim,
	stop_steering = simpleanim,
	
	row = "land",
}
)

PP_CommonStates.AddAoeStates(states, nil, 
{
	generic = "idle",
	
	leap_pre = simplemove,
	leap_loop = simplemove,
	leap_pst = simplemove,
	
	lunge_pre = simpleanim,
	lunge_loop = simpleanim,
	lunge_pst = simpleanim,
	
	superjump_pre = simpleanim,
	superjump_loop = simpleanim,
	superjump_pst = simpleanim,
	
	castspelltime = 10,
})
    
return StateGraph("chickenp", states, events, "idle", actionhandlers)

