require("stategraphs/commonstates")

local function GetExcludeTags(inst)
	if TheNet:GetPVPEnabled() then
		return {"INLIMBO", "playerghost", "notarget", "shadow", "ant", "wall", "groundpoundimmune", "antqueen"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion", "playerghost", "INLIMBO", "notarget", "groundpoundimmune", "ant", "antqueen"}
	else	
		return {"player", "companion", "INLIMBO", "playerghost", "notarget", "shadow", "ant", "wall", "groundpoundimmune", "antqueen"}
	end
end

---Attack----------

local PHASE1 = 0.75
local PHASE2 = 0.5
local PHASE3 = 0.3
local SPAWN_CAP = 5

local function GetSummonCount(inst)
	local health = inst.components.health:GetPercent() or nil
	local numfollowers = inst.components.leader:CountFollowers("ant") or 0
	if health and health >= PHASE1 then
		inst.current_summon_count = 2
	elseif health and health < PHASE1 and health >= PHASE2 then
		inst.current_summon_count = 2
	elseif health and health < PHASE2 and health > PHASE3 then
		inst.current_summon_count = 2
	else
		--We're sick and tired of our enemy's shit, so just spawn up to the cap.
		inst.current_summon_count = 2
	end
end

---Attack 2---------
local MUSIC_TICK = 0.2

local function DoMusicAttack(inst)
	--TODO: Once you checked all the "stun" states on the onattacked handlers, just set this to explosive.
	inst.components.combat:DoAreaAttack(inst, 30, nil, nil, inst.components.revivablecorpse and "explosive" or nil, GetExcludeTags())
end

local function ShakeIfClose(inst)
    ShakeAllCameras(CAMERASHAKE.VERTICAL, .6, .06, .7, inst, 30)
end

local function ShakeIfCloseWalk(inst)
    ShakeAllCameras(CAMERASHAKE.VERTICAL, .5, .045, .6, inst, 30)
end
----------------------------
local longaction = "action"
local shortaction = "action"
local workaction = "action"
local otheraction = "action"

local actionhandlers = PP_CommonStates.GetCommonActions(longaction, shortaction, workaction, otheraction)

local actionhandler_overrides = 
{ 
	ActionHandler(ACTIONS.REVIVE_CORPSE, "failaction"), --no reviving
}

if #actionhandler_overrides > 0 then
	for i, v in ipairs(actionhandler_overrides) do
		table.insert(actionhandlers, v)
	end
end

--for action_loop state
local function StopActionMeter(inst, flash)
    if inst.HUD ~= nil then
        inst.HUD:HideRingMeter(flash)
    end
    if inst.sg.mem.actionmetertask ~= nil then
        inst.sg.mem.actionmetertask:Cancel()
        inst.sg.mem.actionmetertask = nil
        inst.player_classified.actionmeter:set(flash and 1 or 0)
    end
end

local function UpdateActionMeter(inst, starttime)
    inst.player_classified.actionmeter:set_local(math.min(255, math.floor((GetTime() - starttime) * 10 + 2.5)))
end

local function StartActionMeter(inst, duration)
    if inst.HUD ~= nil then
        inst.HUD:ShowRingMeter(inst:GetPosition(), duration)
    end
    inst.player_classified.actionmetertime:set(math.min(255, math.floor(duration * 10 + .5)))
    inst.player_classified.actionmeter:set(2)
    if inst.sg.mem.actionmetertask == nil then
        inst.sg.mem.actionmetertask = inst:DoPeriodicTask(.1, UpdateActionMeter, nil, GetTime())
    end
end

local events=
{
	EventHandler("attacked", function(inst, data) 
		if data.stimuli and data.stimuli == "strong" and not (inst.sg:HasStateTag("transform") or inst.sg:HasStateTag("nointerrupt")) and not inst.components.health:IsDead() then
			inst.sg:GoToState("hit")
		elseif data.stimuli and (data.stimuli == "electric" or data.stimuli == "explosive") and not inst.components.health:IsDead() then
			inst.sg:GoToState("hit", data.stimuli)
		elseif not inst.components.health:IsDead() and not inst.sg:HasStateTag("busy") and not inst.sg:HasStateTag("nointerrupt") and (inst.sg.mem.last_hit_time or 0) + 2 < GetTime() then 
			inst.sg:GoToState("hit") 
		end 
	end),
	PP_CommonHandlers.AddCommonHandlers(),
    EventHandler("death", function(inst, data)
        if inst.sleepingbag ~= nil and (inst.sg:HasStateTag("bedroll") or inst.sg:HasStateTag("tent")) then -- wakeup on death to "consume" sleeping bag first
            inst.sleepingbag.components.sleepingbag:DoWakeUp()
            inst.sleepingbag = nil
        end

        if (data ~= nil and data.cause == "file_load") or inst.components.revivablecorpse ~= nil then
            inst.sg:GoToState("corpse", true)
        else
            inst.sg:GoToState("death")
        end
    end),
    EventHandler("doattack", function(inst, data) if not inst.components.health:IsDead() and (inst.sg:HasStateTag("hit") or not inst.sg:HasStateTag("busy")) then inst.sg:GoToState("attack", data.target) end end),
    CommonHandlers.OnSleep(),
    --CommonHandlers.OnLocomote(true,false),
	EventHandler("locomote", function(inst, data)
        if inst.sg:HasStateTag("busy") then
            return
        end
        local is_moving = inst.sg:HasStateTag("moving")
        local should_move = inst.components.locomotor:WantsToMoveForward()
		local is_looking = inst.sg:HasStateTag("looking")

        if inst.sg:HasStateTag("bedroll") or inst.sg:HasStateTag("tent") or inst.sg:HasStateTag("waking") then -- wakeup on locomote
            if inst.sleepingbag ~= nil and inst.sg:HasStateTag("sleeping") then
                inst.sleepingbag.components.sleepingbag:DoWakeUp()
                inst.sleepingbag = nil
				inst:Show()
				--inst.AnimState:PushAnimation("pig_pickup")
				SetSleeperAwakeState(inst)
				--inst.AnimState:PushAnimation("pig_pickup")
				--inst.sg:GoToState("idle", true)
				
            end 
		elseif is_moving and not should_move then
            inst.sg:GoToState("idle")
        elseif not is_moving and should_move and not is_looking then
            inst.sg:GoToState("run_jump")
        elseif data.force_idle_state and not (is_moving or should_move or inst.sg:HasStateTag("idle") or is_looking) then
            inst.sg:GoToState("idle")
        end
    end),
    CommonHandlers.OnFreeze(),
	
	 EventHandler("ms_opengift",
        function(inst)
            if not inst.sg:HasStateTag("busy") then
                inst.sg:GoToState("opengift")
            end
        end),
		
	EventHandler("respawnfromghost", function(inst)  
			if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:Enable(true)
            end

            inst.components.health:SetInvincible(false)
            inst:ShowHUD(true)
            inst:SetCameraDistance()

            SerializeUserSession(inst) end),	
}

if CurrentRelease.GreaterOrEqualTo(ReleaseID.IDs.R08_ROT_TURNOFTIDES) then
	table.insert(events, PP_CommonHandlers.OnSink())
	table.insert(events, CommonHandlers.OnHop())
end

 local states=
{

	State{
        name = "attack",
        tags = {"attack", "busy", "nointerrupt"},

        onenter = function (inst, loop)
			if inst.components.leader:CountFollowers("ant") <= SPAWN_CAP or loop then
				if not loop then
					GetSummonCount(inst) --how many should we spawn today?
				end
				inst.components.combat:StartAttack()
				inst.AnimState:PlayAnimation("atk1", false)
			else
				inst.sg:GoToState("taunt")
			end
        end,

        events =
        {
            EventHandler("animover", function(inst)
				
                inst.current_summon_count = inst.current_summon_count - 1
                if inst.current_summon_count > 0 then
                    inst.sg:GoToState("attack", true)
                else
                    inst.sg:GoToState("idle")
                end
            end),
        },

        timeline = 
        {   
            TimeEvent(1*FRAMES, function(inst)inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/antqueen/atk_1_rumble") end),
            TimeEvent(18*FRAMES, function(inst)inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/antqueen/atk_1_pre") end),
            TimeEvent(20*FRAMES, function(inst)inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/antqueen/atk_1") end),
            TimeEvent(20*FRAMES, function(inst) inst.SpawnWarrior(inst) end),
        },
    },

     State{
        name= "idle",
        tags = {"idle", "canrotate"},
        
        onenter = function(inst)
            inst.Physics:Stop()
            -- inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/crickant/hunger")
            inst.AnimState:PlayAnimation("idle")
        end,
        
        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end ),
        },
    },
	
	State{
        name= "taunt",
        tags = {"busy"},
        
        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("taunt")
        end,
        
        timeline=
        {
            TimeEvent(18*FRAMES, function(inst)inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/antqueen/taunt")end),
        },

        events =
        {
            EventHandler("animover", function(inst) 
                inst.sg:GoToState("idle") 
            end ),
        },
    },
	
	State{
        name = "special_atk1",
        tags = {"busy", "nointerrupt"},

        onenter = function (inst)
			inst.Physics:Stop()
            inst.taunt = false
			inst.AnimState:PlayAnimation("atk2", false)
			inst.AnimState:Show("fx_bits")
        end,

        timeline=
        {
            TimeEvent(1*FRAMES, function(inst)inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/antqueen/atk_2_fly") end),
            TimeEvent(7*FRAMES, function(inst)inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/antqueen/atk_2_VO") end),
            TimeEvent(21*FRAMES, function(inst)inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/antqueen/land") end),
			TimeEvent(21*FRAMES, ShakeIfClose),
            TimeEvent(24*FRAMES, function(inst) 
				inst.components.combat:DoAreaAttack(inst, 7, nil, nil, nil, GetExcludeTags())
				inst.components.groundpounder:GroundPound() 
			end),
        },
		
		onexit = function(inst)
			inst.AnimState:Hide("fx_bits")
			inst:DoTaskInTime(inst.components.revivablecorpse and PPHAM_FORGE.ANTQUEEN.SPECIAL_CD or 8, function(inst) inst.taunt = true end)
		end,

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end ),
        },
    },
	
	State{
        name= "special_atk2",
        tags = {"busy"},
        
        onenter = function(inst)
            inst.Physics:Stop()
			if inst.altattack and inst.altattack == true then
				inst.sg:GoToState("music_attack")
			else
				inst.AnimState:PlayAnimation("taunt")
			end
        end,
        
        timeline=
        {
            TimeEvent(18*FRAMES, function(inst)inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/antqueen/taunt")end),
        },

        events =
        {
            EventHandler("animover", function(inst) 
                inst.sg:GoToState("idle") 
            end ),
        },
    },
	
	
	State{
        name = "music_attack",
        tags = {"busy"},

        onenter = function (inst)
			inst.altattack = false
            inst.AnimState:PlayAnimation("atk3_pre", false)
            inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/antqueen/atk_3_breath_in")
            
            
            for i=1,4 do
                inst.AnimState:PushAnimation("atk3_loop", false)
            end
            
            inst.AnimState:PushAnimation("atk3_pst", false)
        end,

        timeline = 
        {
            TimeEvent(5*FRAMES, function (inst)  inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/antqueen/atk_3_breath_in") end),
            TimeEvent(22*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/antqueen/insane_LP","insane") end),
            TimeEvent(27*FRAMES, function(inst)
				inst.components.combat:SetDefaultDamage(inst.components.revivablecorpse and 5 or 1)
				inst.musictask = inst:DoPeriodicTask(MUSIC_TICK, DoMusicAttack) 
			end),
           -- TimeEvent(27*FRAMES, function(inst) TheMixer:PushMix("mute") end),
            -- --print("mix_on") 
            --TimeEvent(23*FRAMES*4, function(inst) if inst.musictask then inst.musictask:Cancel() inst.musictask = nil end inst.SoundEmitter:KillSound("insane") end),
            -- --print("mix_off") 
        },

        events =
        {
            EventHandler("animqueueover", function(inst) if inst.musictask then inst.musictask:Cancel() inst.musictask = nil end inst.SoundEmitter:KillSound("insane")  inst.sg:GoToState("idle") end ),
        },

        onexit = function(inst)
			if inst.musictask then
				inst.musictask:Cancel()
				inst.musictask = nil
			end
			inst.components.combat:SetDefaultDamage(inst.components.revivablecorpse and PPHAM_FORGE.ANTQUEEN.DAMAGE or 100*2)
            inst.SoundEmitter:KillSound("insane")
			inst:DoTaskInTime(inst.components.revivablecorpse and PPHAM_FORGE.ANTQUEEN.MUSIC_CD or 20, function(inst) inst.altattack = true end)
            -- TheMixer:PopMix("mute")
            ----print("mix_off")           
        end,
    },

	
	State{
        name = "opengift",
        tags = { "busy", "pausepredict" },

        onenter = function(inst)
            inst.components.locomotor:Stop()
            inst.components.locomotor:Clear()
            inst:ClearBufferedAction()

            --if IsNearDanger(inst) then
                --inst.sg.statemem.isdanger = true
                --inst.sg:GoToState("idle")
                --if inst.components.talker ~= nil then
                   -- inst.components.talker:Say(GetString(inst, "ANNOUNCE_NODANGERGIFT"))
                --end
                --return
           -- end

            inst.SoundEmitter:PlaySound("dontstarve/common/player_receives_gift")
            inst.AnimState:PlayAnimation("idle")
            inst.AnimState:PushAnimation("idle", true)
            -- NOTE: the previously used ripping paper anim is called "giift_loop"

            if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:RemotePausePrediction()
                inst.components.playercontroller:EnableMapControls(false)
                inst.components.playercontroller:Enable(false)
            end
            inst.components.inventory:Hide()
            inst:PushEvent("ms_closepopups")
            inst:ShowActions(false)
            inst:ShowPopUp(POPUPS.GIFTITEM, true)

            if inst.components.giftreceiver ~= nil then
                inst.components.giftreceiver:OnStartOpenGift()
            end
        end,

        timeline =
        {
            -- Timing of the gift box opening animation on giftitempopup.lua
            TimeEvent(155 * FRAMES, function(inst)
               -- inst.AnimState:PlayAnimation("gift_open_pre")
                inst.AnimState:PushAnimation("atk_loop", true)
            end),
        },

        events =
        {
            EventHandler("firedamage", function(inst)
                inst.AnimState:PlayAnimation("taunt")
                inst.sg:GoToState("idle", true)
                if inst.components.talker ~= nil then
                    inst.components.talker:Say(GetString(inst, "ANNOUNCE_NODANGERGIFT"))
                end
            end),
            EventHandler("ms_doneopengift", function(inst, data)
				inst.sg:GoToState("idle", true)
                
            end),
        },

        onexit = function(inst)
            if inst.sg.statemem.isdanger then
                return
            elseif not inst.sg.statemem.isopeningwardrobe then
                if inst.components.playercontroller ~= nil then
                    inst.components.playercontroller:EnableMapControls(true)
                    inst.components.playercontroller:Enable(true)
                end
                inst.components.inventory:Show()
                inst:ShowActions(true)
            end
            inst:ShowPopUp(POPUPS.GIFTITEM, false)
        end,
    },
	
	State{
		name = "hit",
        tags = {"busy", "hit"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("hit")
			inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/antqueen/hit")
        end,

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State
    {
        name = "action_loop",
        tags = { "doing", "nodangle" },

        onenter = function(inst, timeout)
            inst.sg:SetTimeout(timeout or 1)
            inst.components.locomotor:Stop()
            inst.AnimState:PlayAnimation("idle", true)
            if inst.bufferedaction ~= nil then
                inst.sg.statemem.action = inst.bufferedaction
                if inst.bufferedaction.action.actionmeter then
                    inst.sg.statemem.actionmeter = true
                    StartActionMeter(inst, timeout or 1)
                end
                if inst.bufferedaction.target ~= nil and inst.bufferedaction.target:IsValid() then
                    inst.bufferedaction.target:PushEvent("startlongaction")
                end
            end
        end,

        timeline =
        {
            TimeEvent(4 * FRAMES, function(inst)
                inst.sg:RemoveStateTag("busy")
            end),
        },

        ontimeout = function(inst)
            inst.AnimState:PlayAnimation("idle")
            if inst.sg.statemem.actionmeter then
                inst.sg.statemem.actionmeter = nil
                StopActionMeter(inst, true)
            end
            inst:PerformBufferedAction()
        end,

        events =
        {
            EventHandler("animqueueover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("idle")
                end
            end),
        },

        onexit = function(inst)
            if inst.sg.statemem.actionmeter then
                StopActionMeter(inst, false)
            end
            if inst.bufferedaction == inst.sg.statemem.action then
                inst:ClearBufferedAction()
            end
        end,
    },
	
    State{
        name = "death",
        tags = {"busy"},

        onenter = function(inst)
            inst.AnimState:PlayAnimation("death")
            inst.Physics:Stop()
            RemovePhysicsColliders(inst)     
			inst.components.lootdropper:DropLoot(Vector3(inst.Transform:GetWorldPosition()))
			inst.components.inventory:DropEverything(true)
        end,
		
		timeline=
        {
			TimeEvent(6*FRAMES, function (inst)  inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/antqueen/death") end),
            TimeEvent(6*FRAMES, function (inst)  inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/antqueen/explode",nil,.2) end),
            TimeEvent(10*FRAMES, function (inst)  inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/antqueen/explode",nil,.3) end),
            TimeEvent(14*FRAMES, function (inst)  inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/antqueen/explode",nil,.4) end),
            TimeEvent(18*FRAMES, function (inst)  inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/antqueen/explode",nil,.5) end),
            TimeEvent(22*FRAMES, function (inst)  inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/antqueen/explode",nil,.6) end),
            TimeEvent(26*FRAMES, function (inst)  inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/antqueen/explode",nil,.7) end),
            TimeEvent(28*FRAMES, function (inst)  inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/antqueen/explode") end),
            TimeEvent(43*FRAMES, function (inst)  inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/antqueen/land") end),
        },

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
					PlayablePets.DoDeath(inst)
                end
            end),
        },

    },
	
	State {
        name = "sleep",
        tags = { "sleeping", "busy" }, --add tag "busy" if you hate sliding

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pre")
            --if fns ~= nil and fns.onsleep ~= nil then
                --fns.onsleep(inst)
            --end
        end,

        timeline = 
        {
            TimeEvent(35*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/antqueen/breath_out") end),
        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("sleeping") end ),
            EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "sleeping",
        tags = { "sleeping", "busy" },

        --onenter = onentersleeping,
		
		onenter = function(inst)
				inst.components.locomotor:StopMoving()
				PlayablePets.SleepHeal(inst)
				inst.AnimState:PlayAnimation("sleep_loop")
			end,
			
		onexit = function(inst)
		--inst.components.sanity.dapperness = -0.5
		end,
        --timeline = timelines ~= nil and timelines.sleeptimeline or nil,
		timeline=
        {
			TimeEvent(6*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/antqueen/breath_in") end ),
            TimeEvent(36*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/antqueen/breath_out") end ),
        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("sleeping") end ),
			EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "wake",
        tags = { "busy", "waking" },

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pst")
            if inst.components.sleeper ~= nil and inst.components.sleeper:IsAsleep() then
                inst.components.sleeper:WakeUp()
            end
            --if fns ~= nil and fns.onwake ~= nil then
                --fns.onwake(inst)
            --end
        end,

        --timeline = timelines ~= nil and timelines.waketimeline or nil,

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State
    {
        name = "run_start",
        tags = { "moving", "running", "canrotate" },

        onenter = function(inst)
			inst.components.locomotor:RunForward()
            inst.AnimState:PlayAnimation("run_pre")
			
        end,
		
		timeline = 
		{
			--TimeEvent(0*FRAMES, function(inst) inst.Physics:Stop() end ),
		},
		
        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("run") end ),
        },
    },
	
	State{
        name = "run_jump",
        tags = {"moving", "running", "canrotate"},

        onenter = function (inst)
			inst.AnimState:PlayAnimation("atk2")
			inst.components.locomotor:RunForward()
			--inst.sg:SetTimeout(1.5)
			inst.AnimState:Hide("fx_bits")
        end,

        timeline=
        {
            TimeEvent(1*FRAMES, function(inst)inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/antqueen/atk_2_fly") end),
            TimeEvent(7*FRAMES, function(inst)inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/antqueen/atk_2_VO") end),
            TimeEvent(21*FRAMES, function(inst)inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/antqueen/land") end),
			TimeEvent(24*FRAMES, ShakeIfCloseWalk),
            TimeEvent(24*FRAMES, function(inst) 
				--inst.components.combat:DoAreaAttack(inst, 7, nil, nil, nil, GetExcludeTags())
				--inst.components.locomotor:Stop()
				inst.Physics:Stop()
				--inst.components.groundpounder:GroundPound() 
			end),
        },
		
		ontimeout = function(inst)
			inst.sg:GoToState("idle")
		end,
		
		onexit = function(inst)
			--inst.AnimState:Hide("fx_bits")
			--inst:DoTaskInTime(4, function(inst) inst.taunt2 = true end)
		end,

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end ),
        },
    },
	
	State
    {
        name = "run",
        tags = { "moving", "running", "canrotate" },

        onenter = function(inst)
			--inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/run")
			inst.components.locomotor:RunForward()
			inst.AnimState:PlayAnimation("run_loop")
        end,
		
		timeline = {

			TimeEvent(2*FRAMES, PlayFootstep ),
        -- TimeEvent(12*FRAMES, PlayFootstep ),
			TimeEvent(2*FRAMES, function(inst) 
            if inst.should_play_idle then
                    inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/thunderbird/run")
                    inst.should_play_idle = false
                else
                    inst.should_play_idle = true
                end
            end),
			
			--TimeEvent(23*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/spiderqueen/walk_spiderqueen") inst.SoundEmitter:PlaySound("dontstarve/creatures/rocklobster/footstep") end),
		},
		
        events=
			{
				EventHandler("animqueueover", function(inst) 
					inst.sg:GoToState("run")  
				end ),
			},

    },
	
	State
    {
        name = "run_stop",
        tags = { "idle" },

        onenter = function(inst) 
            inst.components.locomotor:StopMoving()
			inst.AnimState:PlayAnimation("walk_pst")
			--inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/step")            
        end,

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end ),
        },
    },
	
	--=========================
	-----wormhole states----
	--=========================
	
	State{
		name = "jumpin_pre",
        tags = {"busy"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
			inst:PerformBufferedAction()
            inst.AnimState:PlayAnimation("idle")
			----print("DEBUG: JUMPIN PRE ran!")
        end,

		timeline=
        {
			
        },

        events=
        {
			EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    if inst.bufferedaction ~= nil then
                        inst:PerformBufferedAction()
						----print("DEBUG: Buffered Action did not return nil! "..inst.bufferedaction)
                    else
						----print("DEBUG: Buffered Action did return nil!")
                        inst.sg:GoToState("idle")
                    end
                end
            end),
        },
    },
	
	State{
		name = "jumpin",
        tags = {"doing", "busy"},

        onenter = function(inst, data)
            inst.Physics:Stop()
			----print("DEBUG: Jumpin State ran")
			inst:PerformBufferedAction()
            inst.AnimState:PlayAnimation("idle")
			
			inst.components.locomotor:Stop()

            inst.sg.statemem.target = data.teleporter
            inst.sg.statemem.heavy = inst.components.inventory:IsHeavyLifting()

            if data.teleporter ~= nil and data.teleporter.components.teleporter ~= nil then
				----print("DEBUG: DATA IS NOT NIL!")
                data.teleporter.components.teleporter:RegisterTeleportee(inst)
            end
			
            local pos = data ~= nil and data.teleporter and data.teleporter:GetPosition() or nil
			
			inst.sg.statemem.teleportarrivestate = "idle"
        end,

		timeline=
        {
			--TimeEvent(13*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/hound/bark") end),
			--TimeEvent(24*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/hound/bark") end),
        },

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    if inst.sg.statemem.target ~= nil and
                        inst.sg.statemem.target:IsValid() and
                        inst.sg.statemem.target.components.teleporter ~= nil then
                        --Unregister first before actually teleporting
                        inst.sg.statemem.target.components.teleporter:UnregisterTeleportee(inst)
                        if inst.sg.statemem.target.components.teleporter:Activate(inst) then
                            inst.sg.statemem.isteleporting = true
                            inst.components.health:SetInvincible(true)
                            if inst.components.playercontroller ~= nil then
                                inst.components.playercontroller:Enable(false)
                            end
                            inst:Hide()
                            inst.DynamicShadow:Enable(false)
                            return
                        end
                    end
                    inst.sg:GoToState("idle")
                end
            end),
        },

        onexit = function(inst)
            if inst.sg.statemem.isphysicstoggle then
                ToggleOnPhysics(inst)
            end

            if inst.sg.statemem.isteleporting then
                inst.components.health:SetInvincible(false)
                if inst.components.playercontroller ~= nil then
                    inst.components.playercontroller:Enable(true)
                end
                inst:Show()
                inst.DynamicShadow:Enable(true)
            elseif inst.sg.statemem.target ~= nil
                and inst.sg.statemem.target:IsValid()
                and inst.sg.statemem.target.components.teleporter ~= nil then
                inst.sg.statemem.target.components.teleporter:UnregisterTeleportee(inst)
            end
        end,
    },
	
	State{
		name = "jumpout", --unused but is functional
        tags = {"busy"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
			inst:PerformBufferedAction()
            inst.AnimState:PlayAnimation("idle")
			----print("DEBUG: JUMPOUT ran!")
        end,

		timeline=
        {
			
        },

        events=
        {
			EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                        inst.sg:GoToState("idle")
                end
            end),
        },
    },
	--==============================
	-------------------Forge States------------------------	
	State
    {
        name = "corpse",
        tags = { "busy", "noattack", "nopredict", "nomorph", "nodangle" },

        onenter = function(inst, fromload)
            if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:Enable(false)
            end

            inst:PushEvent("playerdied", { loading = fromload, skeleton = false })

            inst:ShowActions(false)
            inst.components.health:SetInvincible(true)

            if inst.components.locomotor then
				inst.components.locomotor:StopMoving()
			end
			inst.AnimState:PlayAnimation("death")
        end,
		
		timeline=
		{
			TimeEvent(6*FRAMES, function (inst)  inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/antqueen/death") end),
            TimeEvent(6*FRAMES, function (inst)  inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/antqueen/explode",nil,.2) end),
            TimeEvent(10*FRAMES, function (inst)  inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/antqueen/explode",nil,.3) end),
            TimeEvent(14*FRAMES, function (inst)  inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/antqueen/explode",nil,.4) end),
            TimeEvent(18*FRAMES, function (inst)  inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/antqueen/explode",nil,.5) end),
            TimeEvent(22*FRAMES, function (inst)  inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/antqueen/explode",nil,.6) end),
            TimeEvent(26*FRAMES, function (inst)  inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/antqueen/explode",nil,.7) end),
            TimeEvent(28*FRAMES, function (inst)  inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/antqueen/explode") end),
            TimeEvent(43*FRAMES, function (inst)  inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/antqueen/land") end),
		},

        onexit = function(inst)
            if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:Enable(true)
            end
            inst:ShowActions(true)
            inst.components.health:SetInvincible(false)
        end,
    },
	
	State{
        name = "corpse_rebirth",
        tags = { "busy", "noattack", "nopredict", "nomorph" },

        onenter = function(inst)
            if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:RemotePausePrediction()
                inst.components.playercontroller:Enable(false)
            end

			inst.sg:SetTimeout(107 * FRAMES)
			
			inst.Physics:CollidesWith(COLLISION.WORLD)
            inst.Physics:CollidesWith(COLLISION.OBSTACLES)
            inst.Physics:CollidesWith(COLLISION.SMALLOBSTACLES)
            inst.Physics:CollidesWith(COLLISION.CHARACTERS)
            inst.Physics:CollidesWith(COLLISION.GIANTS)

            inst.components.health:SetInvincible(true)
            inst:ShowActions(false)
            inst:SetCameraDistance(14)
        end,

        timeline =
        {
            TimeEvent(53 * FRAMES, function(inst)
                inst.components.bloomer:PushBloom("corpse_rebirth", "shaders/anim.ksh", -2)
                inst.sg.statemem.fadeintime = (86 - 53) * FRAMES
                inst.sg.statemem.fadetime = 0
            end),
            TimeEvent(86 * FRAMES, function(inst)
                inst.sg.statemem.physicsrestored = true
                inst.Physics:ClearCollisionMask()
                inst.Physics:CollidesWith(COLLISION.WORLD)
                inst.Physics:CollidesWith(COLLISION.OBSTACLES)
                inst.Physics:CollidesWith(COLLISION.SMALLOBSTACLES)
                inst.Physics:CollidesWith(COLLISION.CHARACTERS)
                inst.Physics:CollidesWith(COLLISION.GIANTS)

                
                if inst.sg.statemem.fade ~= nil then
                    inst.sg.statemem.fadeouttime = 20 * FRAMES
                    inst.sg.statemem.fadetotal = inst.sg.statemem.fade
                end
                inst.sg.statemem.fadeintime = nil
            end),
			TimeEvent((96) * FRAMES, function(inst)
				local pos = inst:GetPosition()
				local fire = SpawnPrefab("lavaarena_portal_player_fx")
				fire.Transform:SetPosition(pos.x, 0, pos.z)
				if inst:HasTag("largecreature") or inst:HasTag("epic") then --totally not for PP I swear! --Fid: Hmmmm are you sure?
					fire.Transform:SetScale(3, 3, 3)
				end
            end),
            TimeEvent((86 + 20) * FRAMES, function(inst)
				inst:Hide()
                inst.components.bloomer:PopBloom("corpse_rebirth")
            end),
        },
		
		
        onupdate = function(inst, dt)
            if inst.sg.statemem.fadeouttime ~= nil then
                inst.sg.statemem.fade = math.max(0, inst.sg.statemem.fade - inst.sg.statemem.fadetotal * dt / inst.sg.statemem.fadeouttime)
                if inst.sg.statemem.fade > 0 then
                    inst.components.colouradder:PushColour("corpse_rebirth", inst.sg.statemem.fade, inst.sg.statemem.fade, inst.sg.statemem.fade, 0)
                else
                    inst.components.colouradder:PopColour("corpse_rebirth")
                    inst.sg.statemem.fadeouttime = nil
                end
            elseif inst.sg.statemem.fadeintime ~= nil then
                local k = 1 - inst.sg.statemem.fadetime / inst.sg.statemem.fadeintime
                inst.sg.statemem.fade = .8 * (1 - k * k)
                inst.components.colouradder:PushColour("corpse_rebirth", inst.sg.statemem.fade, inst.sg.statemem.fade, inst.sg.statemem.fade, 0)
                inst.sg.statemem.fadetime = inst.sg.statemem.fadetime + dt
            end
        end,
		
		ontimeout = function(inst)
			inst.components.bloomer:PopBloom("corpse_rebirth")
            inst.sg:GoToState("corpse_taunt")
        end,
		
        onexit = function(inst)
            inst:ShowActions(true)
            inst:SetCameraDistance()
			inst:Show()
            if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:Enable(true)
            end
            inst.components.health:SetInvincible(false)

            inst.Physics:ClearCollisionMask()
            inst.Physics:CollidesWith(COLLISION.WORLD)
            inst.Physics:CollidesWith(COLLISION.OBSTACLES)
            inst.Physics:CollidesWith(COLLISION.SMALLOBSTACLES)
            inst.Physics:CollidesWith(COLLISION.CHARACTERS)
            inst.Physics:CollidesWith(COLLISION.GIANTS)

            SerializeUserSession(inst)
        end,
    },
	
	State{
        name = "corpse_taunt",
        tags = { "busy", "noattack", "nopredict", "nomorph" },

        onenter = function(inst)
            if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:RemotePausePrediction()
                inst.components.playercontroller:Enable(false)
            end
			
			inst.Physics:CollidesWith(COLLISION.WORLD)
            inst.Physics:CollidesWith(COLLISION.OBSTACLES)
            inst.Physics:CollidesWith(COLLISION.SMALLOBSTACLES)
            inst.Physics:CollidesWith(COLLISION.CHARACTERS)
            inst.Physics:CollidesWith(COLLISION.GIANTS)

			inst.AnimState:PlayAnimation("taunt")
			
            inst.components.health:SetInvincible(true)
            inst:ShowActions(false)
            inst:SetCameraDistance(14)
        end,

		timeline=
		{
		
		},

        events =
        {
            EventHandler("animqueueover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("idle")
                end
            end),
        },

        onexit = function(inst)
            inst:ShowActions(true)
            inst:SetCameraDistance()
            if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:Enable(true)
            end
            inst.components.health:SetInvincible(false)

            inst.Physics:ClearCollisionMask()
            inst.Physics:CollidesWith(COLLISION.WORLD)
            inst.Physics:CollidesWith(COLLISION.OBSTACLES)
            inst.Physics:CollidesWith(COLLISION.SMALLOBSTACLES)
            inst.Physics:CollidesWith(COLLISION.CHARACTERS)
            inst.Physics:CollidesWith(COLLISION.GIANTS)

            SerializeUserSession(inst)
        end,
    },
	
	State
    {
        name = "revivecorpse",

        onenter = function(inst)
            --inst.components.talker:Say(GetString(inst, "ANNOUNCE_REVIVING_CORPSE"))
            local buffaction = inst:GetBufferedAction()
            local target = buffaction ~= nil and buffaction.target or nil
            inst.sg:GoToState("action_loop",
                TUNING.REVIVE_CORPSE_ACTION_TIME *
                (inst.components.corpsereviver ~= nil and inst.components.corpsereviver:GetReviverSpeedMult() or 1) *
                (target ~= nil and target.components.revivablecorpse ~= nil and target.components.revivablecorpse:GetReviveSpeedMult() or 1)
            )
        end,
    },
	
}

CommonStates.AddFrozenStates(states)
PP_CommonStates.AddActionStates(states, 
	{
		TimeEvent(0 * FRAMES, function(inst)
            inst:PerformBufferedAction()
        end),
		TimeEvent(18*FRAMES, function(inst)inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/antqueen/taunt")end),
	}, 
	"taunt", nil, nil, "taunt", "idle") --(states, timelines, anim, anim2, enteranim, loopanim, exitanim, noanim, events)
PP_CommonStates.AddSailStates(states, {}, "atk2", "idle")
local simpleanim = "idle"
local simpleidle = "idle"
local simplemove = "run"
if CurrentRelease.GreaterOrEqualTo(ReleaseID.IDs.R08_ROT_TURNOFTIDES) then
CommonStates.AddHopStates(states, false, {pre = "atk2", loop = "atk2", pst = "atk2"}, nil, "death")
end
PP_CommonStates.AddSailorStates(states, nil, 
{
	plank_idle = simpleanim,
	plank_idle_loop = simpleidle,
	plank_idle_pst = simpleanim,
	
	plank_hop_pre = simpleanim,
	plank_hop = "atk2",
	
	steer_pre = simpleanim,
	steer_idle = simpleidle,
	steer_turning = "taunt",
	stop_steering = simpleanim,
	
	row = "taunt",
}
)


    
return StateGraph("antqueenp", states, events, "idle", actionhandlers)

