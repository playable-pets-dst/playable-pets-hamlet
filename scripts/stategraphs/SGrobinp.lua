require("stategraphs/commonstates")
require("stategraphs/ppstates")

local function GetExcludeTags(inst)
	if TheNet:GetPVPEnabled() then
		return {"INLIMBO", "playerghost", "notarget", "shadow", "groundpoundimmune"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion", "playerghost", "INLIMBO", "groundpoundimmune"}
	else	
		return {"player", "companion", "INLIMBO", "playerghost", "notarget", "shadow", "groundpoundimmune"}
	end
end

local function ShakeIfClose(inst)
    ShakeAllCameras(CAMERASHAKE.VERTICAL, .15, .02, .5, inst, 20)
end

local longaction = "action"
local shortaction = "action"
local workaction = "action"
local otheraction = "open"

local actionhandlers = PP_CommonStates.GetCommonActions(longaction, shortaction, workaction, otheraction)

local actionhandler_overrides = 
{ 
	ActionHandler(ACTIONS.ATTACK, function(inst)
		if TheNet:GetServerGameMode() == "lavaarena" then
			return "attack"
		else
			return "attack_no"
		end		
	end),
}

if #actionhandler_overrides > 0 then
	for i, v in ipairs(actionhandler_overrides) do
		table.insert(actionhandlers, v)
	end
end

local events=
{
	EventHandler("attacked", function(inst, data) 
		if data.stimuli and data.stimuli == "strong" and not (inst.sg:HasStateTag("transform") or inst.sg:HasStateTag("nointerrupt")) and not inst.components.health:IsDead() then
			inst.sg:GoToState("hit")
		elseif data.stimuli and (data.stimuli == "electric" or data.stimuli == "explosive") and not inst.components.health:IsDead() then
			inst.sg:GoToState("hit", data.stimuli)
		elseif not inst.components.health:IsDead() and not inst.sg:HasStateTag("busy") and not inst.sg:HasStateTag("nointerrupt") then 
			inst.sg:GoToState("hit") 
		end 
	end),
    EventHandler("doattack", function(inst, data) if not inst.components.health:IsDead() and (inst.sg:HasStateTag("hit") or not inst.sg:HasStateTag("busy")) then inst.sg:GoToState("attack", data.target) end end),
    CommonHandlers.OnSleep(),
	PP_CommonHandlers.AddCommonHandlers(),
    --CommonHandlers.OnLocomote(true,false),
    CommonHandlers.OnFreeze(),
	EventHandler("emote", function(inst, data)
        if not (inst.sg:HasStateTag("busy") or
                inst.sg:HasStateTag("nopredict") or
                inst.sg:HasStateTag("sleeping"))
            and not inst.components.inventory:IsHeavyLifting()
            and (data.mounted or not inst.components.rider:IsRiding())
            and (data.beaver or not inst:HasTag("beaver"))
            and (not data.requires_validation or TheInventory:CheckClientOwnership(inst.userid, data.item_type)) then
            inst.sg:GoToState("emote", data)
        end
    end),
	---
	PP_CommonHandlers.OpenGift(),
	PP_CommonHandlers.OnKnockback(),
	PP_CommonHandlers.OnLocomoteAdvanced(), --if mob uses toggeable walk or homes
	PP_CommonHandlers.OnDeath(),
		
	EventHandler("respawnfromghost", function(inst)  
			if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:Enable(true)
            end

            inst.components.health:SetInvincible(false)
            inst:ShowHUD(true)
            inst:SetCameraDistance()

            SerializeUserSession(inst) end),	
}

if CurrentRelease.GreaterOrEqualTo(ReleaseID.IDs.R08_ROT_TURNOFTIDES) then
	table.insert(events, PP_CommonHandlers.OnSink())
	--table.insert(events, CommonHandlers.OnHop())
end

local function OnWaterSound(inst)
    if (inst.onwater or inst:HasTag("swimming")) then
        inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/ro_bin/flap")
    end
end

local states=
{
    State{
        name = "idle",
        tags = {"idle", "canrotate"},
        
        onenter = function(inst, pushanim)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("idle_loop")
        end,
        
        events=
        {
            EventHandler("animover", function(inst)
                inst.sg:GoToState("idle") 
            end),
        },

        timeline=
        {   
            TimeEvent(1*FRAMES, function(inst) OnWaterSound(inst) end),
            TimeEvent(8*FRAMES, function(inst) OnWaterSound(inst) end),
            TimeEvent(15*FRAMES, function(inst) OnWaterSound(inst) end),
            TimeEvent(22*FRAMES, function(inst) OnWaterSound(inst) end),
            TimeEvent(29*FRAMES, function(inst) OnWaterSound(inst) end),
        },
    },

    State{
        name = "special_atk1",
        tags = {"busy"},
        
        onenter = function(inst, pushanim)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("emote_idle")
        end,
        
        events=
        {
            EventHandler("animover", function(inst)
                inst.sg:GoToState("idle") 
            end),
        },

        timeline=
        {   
            TimeEvent(5*FRAMES, function(inst)inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/ro_bin/emote_idle") end),
            TimeEvent(7*FRAMES, function(inst)inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/ro_bin/flap") end),
            TimeEvent(8*FRAMES, function(inst)inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/ro_bin/flap",nil,.5) end),
            TimeEvent(14*FRAMES, function(inst)inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/ro_bin/flap") end),
            TimeEvent(16*FRAMES, function(inst)inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/ro_bin/flap",nil,.5) end),
            TimeEvent(18*FRAMES, function(inst)inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/ro_bin/emote_idle") end), 
            TimeEvent(21*FRAMES, function(inst)inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/ro_bin/flap") end),
            TimeEvent(22*FRAMES, function(inst)inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/ro_bin/flap",nil,.5) end),
        },
   },
   
   State{
        name = "castaoe",
        tags = {"busy"},
        
        onenter = function(inst, pushanim)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("emote_idle")
        end,
        
        events=
        {
            EventHandler("animover", function(inst)
                inst.sg:GoToState("idle") 
            end),
        },

        timeline=
        {   
            TimeEvent(5*FRAMES, function(inst)inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/ro_bin/emote_idle") end),
            TimeEvent(7*FRAMES, function(inst)inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/ro_bin/flap") end),
            TimeEvent(8*FRAMES, function(inst)inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/ro_bin/flap",nil,.5) end),
            TimeEvent(14*FRAMES, function(inst)inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/ro_bin/flap") end),
            TimeEvent(16*FRAMES, function(inst)inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/ro_bin/flap",nil,.5) end),
            TimeEvent(18*FRAMES, function(inst)inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/ro_bin/emote_idle") end), 
            TimeEvent(21*FRAMES, function(inst)inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/ro_bin/flap") inst:PerformBufferedAction() end),
            TimeEvent(22*FRAMES, function(inst)inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/ro_bin/flap",nil,.5) end),
        },
   },
   
   State{
        name = "open",
        tags = {"busy", "attack"},
        
        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("open")
            inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/ro_bin/open")
        end,

        events=
        {   
            EventHandler("animover", function(inst) inst.sg:GoToState("close") end ),
        },

        -- timeline=
        -- {
        --     TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/chester/open") end),
        -- },        
    },

    State{
        name = "attack",
        tags = {"busy", "attack"},
        
        onenter = function(inst)
            inst.AnimState:PlayAnimation("closed")
            inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/ro_bin/close")
        end,
		
		timeline=
        {
			TimeEvent(2*FRAMES, function(inst)
                inst:PerformBufferedAction()
            end)
        },


        events=
        {   
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end ),
        },


    },

    State{
        name = "close",
        tags = {"busy", "attack"},
        
        onenter = function(inst)
            inst.AnimState:PlayAnimation("closed")
            inst:PerformBufferedAction()
            inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/ro_bin/close")
        end,

        events=
        {   
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end ),
        },       
    },
	
    State{
        name = "spawn",
        tags = {"canrotate", "busy"},
        
        onenter = function(inst)
            inst.AnimState:PlayAnimation("idle")
        end,
       
        events=
        {
            EventHandler("animover", function(inst) 
                inst.sg:GoToState("idle")
            end),
        },
    },    

    State{
        name = "land",
        tags = {"canrotate", "busy"},
        
        onenter = function(inst)
            local should_move = inst.components.locomotor:WantsToMoveForward()
            local should_run = inst.components.locomotor:WantsToRun()
            if should_move then
                inst.components.locomotor:WalkForward()
            elseif should_run then
                inst.components.locomotor:RunForward()
            end
            inst.AnimState:SetBank("ro_bin_water")
            inst.AnimState:PlayAnimation("land")

        end,
       
        events=
        {    
            EventHandler("animover", function(inst) 
                inst.AnimState:SetBank("ro_bin")
                inst.sg:GoToState("idle")
            end),
        },
        
    },

    State{
        name = "takeoff",
        tags = {"canrotate", "busy"},
        
        onenter = function(inst)
            local should_move = inst.components.locomotor:WantsToMoveForward()
            local should_run = inst.components.locomotor:WantsToRun()
            if should_move then
                inst.components.locomotor:WalkForward()
            elseif should_run then
                inst.components.locomotor:RunForward()
            end

            inst.AnimState:SetBank("ro_bin_water")
            inst.AnimState:PlayAnimation("takeoff")
        end,
       
        events=
        {
            EventHandler("animover", function(inst) 
                inst.sg:GoToState("idle")
            end),
        },
    },
	
	State{
        name = "attack_no",
        tags = {"attack", "busy"},
        
        onenter = function(inst, cb)
			inst.Physics:Stop()
			inst.AnimState:PlayAnimation("walk_pst")
        end,
        
        timeline=
        {

        },
        
        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State{
        name = "hit",
        tags = {"busy"},
        
        onenter = function(inst)            
            inst.AnimState:PlayAnimation("hit")
            inst.Physics:Stop()            
        end,
        
        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end ),
        },        
    },   
	
    State{
        name = "death",
        tags = {"busy"},

        onenter = function(inst)
            inst.AnimState:PlayAnimation("death")
			inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/ro_bin/death")
            inst.Physics:Stop()
            RemovePhysicsColliders(inst)     
			inst.components.lootdropper:DropLoot(Vector3(inst.Transform:GetWorldPosition()))
			inst.components.inventory:DropEverything(true)
        end,
		
		timeline=
        {

        },

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
					PlayablePets.DoDeath(inst)
                end
            end),
        },

    },
	
	State {
        name = "sleep",
        tags = { "sleeping", "busy" }, --add tag "busy" if you hate sliding

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pre")
        end,

        timeline = 
		{
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/ro_bin/hit") end)
		},

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("sleeping") end ),
            EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "sleeping",
        tags = { "sleeping", "busy" },

        --onenter = onentersleeping,
		
		onenter = function(inst)
				inst.components.locomotor:StopMoving()
				PlayablePets.SleepHeal(inst)
				inst.AnimState:PlayAnimation("sleep_loop")
			end,
			
		onexit = function(inst)
		--inst.components.sanity.dapperness = -0.5
		end,

		timeline=
        {

        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("sleeping") end ),
			EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "wake",
        tags = { "busy", "waking" },

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pst")
            if inst.components.sleeper ~= nil and inst.components.sleeper:IsAsleep() then
                inst.components.sleeper:WakeUp()
            end
            --if fns ~= nil and fns.onwake ~= nil then
                --fns.onwake(inst)
            --end
        end,

        timeline = 
		{
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/ro_bin/hit") end)
		},

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State
    {
        name = "walk_start",
        tags = { "moving", "running", "canrotate" },

        onenter = function(inst)
			inst.components.locomotor:WalkForward()
            inst.AnimState:PlayAnimation("walk_pre")
			
        end,
		
		timeline = 
		{
			TimeEvent(1*FRAMES, function(inst) 
				if (inst.onwater or inst:HasTag("swimming")) then
					inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/ro_bin/flap")
					inst.components.locomotor:WalkForward() 
				else
					-- inst.SoundEmitter:PlaySound("dontstarve/creatures/chester/boing")
					-- inst.components.locomotor:RunForward() 
				end
			end),
		},
		
        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("walk") end ),
        },
    },
	
	State
    {
        name = "walk",
        tags = { "moving", "running", "canrotate" },

        onenter = function(inst)
			--inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/run")
			inst.components.locomotor:WalkForward()
			inst.AnimState:PlayAnimation((inst.altstep and not inst:HasTag("swimming")) and "walk_loop_alt" or "walk_loop")
			
        end,
		
		timeline=
        {
            TimeEvent(0*FRAMES, function(inst)
            if inst.altstep then               -- and not inst.onwater
                inst.altstep = nil
            else
                --inst.AnimState:PlayAnimation("walk_loop")
                if not (inst:HasTag("swimming")) then
                    inst.altstep = true
				else
					inst.altstep = nil
                end
            end   
        end),
        --TimeEvent(0*FRAMES, function(inst)  end),
        TimeEvent(1*FRAMES, function(inst) 
            if (inst.onwater or inst:HasTag("swimming")) then
                inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/ro_bin/flap")
                inst.components.locomotor:WalkForward() 
            else
                inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/ro_bin/step")
                inst.components.locomotor:RunForward() 
            end
        end),

        TimeEvent(13*FRAMES, function(inst) 
                inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/ro_bin/bounce")
                inst.SoundEmitter:PlaySound("dontstarve_DLC002/common/foley/blubber_suit",nil,.3) end),
        TimeEvent(8*FRAMES, function(inst) 
            if (inst.onwater or inst:HasTag("swimming")) then
                inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/ro_bin/flap")
            end
        end),        
    
        TimeEvent(12*FRAMES, function(inst) 
            if (inst.onwater or inst:HasTag("swimming")) then
            else
                PlayFootstep(inst)
               -- inst.components.locomotor:Stop()
            end
        end),
        },		
		
		onexit = function(inst)

		end,
		
        events=
		{
			EventHandler("animqueueover", function(inst) 
				inst.sg:GoToState("walk")  
			end ),
		},

    },
	
	State
    {
        name = "walk_stop",
        tags = { "idle" },

        onenter = function(inst) 
            inst.components.locomotor:StopMoving()
			inst.AnimState:PlayAnimation("walk_pst")
			--inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/step")            
        end,
		
		timeline = 
		{
			TimeEvent(1*FRAMES, function(inst) 
				if (inst.onwater or inst:HasTag("swimming")) then
					inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/ro_bin/flap")
					inst.components.locomotor:WalkForward() 
				else
					-- inst.SoundEmitter:PlaySound("dontstarve/creatures/chester/boing")
					-- inst.components.locomotor:RunForward() 
				end
			end),
		},

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end ),
        },
    },
}

--CommonStates.AddFrozenStates(states)
CommonStates.AddSimpleState(states, "hit", "hit", {"busy"})

PP_CommonStates.AddKnockbackState(states, nil, "hit") --(states, timeline, anim, sounds, fns)
PP_CommonStates.AddActionStates(states, 
	{
		TimeEvent(0 * FRAMES, function(inst)
            inst:PerformBufferedAction()
        end),
	}, 
	"walk_pst", nil, nil, "emote_idle", "walk_pst") --(states, timelines, anim, anim2, enteranim, loopanim, exitanim, noanim, events)
PP_CommonStates.AddCorpseStates(states, true, 
	--timelines = 
	{
		corpse =
		{
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/ro_bin/death") end),
		},
		
		corpse_taunt =
		{
			TimeEvent(5*FRAMES, function(inst)inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/ro_bin/emote_idle") end),
            TimeEvent(7*FRAMES, function(inst)inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/ro_bin/flap") end),
            TimeEvent(8*FRAMES, function(inst)inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/ro_bin/flap",nil,.5) end),
            TimeEvent(14*FRAMES, function(inst)inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/ro_bin/flap") end),
            TimeEvent(16*FRAMES, function(inst)inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/ro_bin/flap",nil,.5) end),
            TimeEvent(18*FRAMES, function(inst)inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/ro_bin/emote_idle") end), 
            TimeEvent(21*FRAMES, function(inst)inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/ro_bin/flap") end),
            TimeEvent(22*FRAMES, function(inst)inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/ro_bin/flap",nil,.5) end),
		},
	
	},
	--anims = 
	{
		corpse = "death",
		corpse_taunt = "emote_idle"
	},
	--sounds =
	{
		--corpse = "dontstarve_DLC003/creatures/enemy/grabbing_vine/death"
	},
	--fns =
	{

	}
	) ---(states, isflying, timelines, anims, sounds, fns, events, nofx)
PP_CommonStates.AddJumpInStates(states, nil, "walk_pst")
PP_CommonStates.AddOpenGiftStates(states, "emote_idle")
PP_CommonStates.AddSailStates(states, {}, "walk_pst", "idle_loop")
local simpleanim = "walk_pst"
local simpleidle = "idle_loop"
local simplemove = "walk"
if CurrentRelease.GreaterOrEqualTo(ReleaseID.IDs.R08_ROT_TURNOFTIDES) then
CommonStates.AddHopStates(states, false, {pre = simplemove.."_pre", loop = simplemove.."_loop", pst = simplemove.."_pst"}, nil, "death")
end
PP_CommonStates.AddSailorStates(states, nil, 
{
	plank_idle = simpleanim,
	plank_idle_loop = simpleidle,
	plank_idle_pst = simpleanim,
	
	plank_hop_pre = simplemove.."_pre",
	plank_hop = simplemove.."_loop",
	
	steer_pre = simpleanim,
	steer_idle = simpleidle,
	steer_turning = simpleanim,
	stop_steering = simpleanim,
	
	row = "walk_pst",
}
)

PP_CommonStates.AddAoeStates(states, nil, 
{
	generic = "idle_loop",
	
	leap_pre = simplemove.."_pre",
	leap_loop = simplemove.."_loop",
	leap_pst = simplemove.."_pst",
	
	lunge_pre = simpleanim,
	lunge_loop = simpleanim,
	lunge_pst = simpleanim,
	
	superjump_pre = simpleanim,
	superjump_loop = simpleanim,
	superjump_pst = simpleanim,
	
	castspelltime = 10,
})
    
return StateGraph("robinp", states, events, "idle", actionhandlers)

