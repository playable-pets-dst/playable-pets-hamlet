require("stategraphs/commonstates")
require("stategraphs/ppstates")

local MINE_CD = 15
local TELE_CD = 20
local LASER_CD = 10
local CAGE_CD = 40
local BEAMRAD = 7

local function GetExcludeTags(inst)
	if TheNet:GetPVPEnabled() then
		return {"INLIMBO", "playerghost", "notarget", "shadow", "groundpoundimmune"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion", "playerghost", "INLIMBO", "groundpoundimmune"}
	else	
		return {"player", "companion", "INLIMBO", "playerghost", "notarget", "shadow", "groundpoundimmune"}
	end
end

local function ShakeIfClose(inst)
    ShakeAllCameras(CAMERASHAKE.VERTICAL, .15, .02, .5, inst, 20)
end

local function ShakeIfClose_Explode(inst)
    ShakeAllCameras(CAMERASHAKE.VERTICAL, .15, .02, .5, inst, 40)
end

local function DoFootstep(inst)
    inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/head/step")
end

local function spawnburns(inst,rad,startangle,endangle,num)
    startangle = startangle *DEGREES
    endangle = endangle *DEGREES
    local pt = Vector3(inst.Transform:GetWorldPosition()) 
    local down = TheCamera:GetDownVec()             
    local angle = math.atan2(down.z, down.x)

    local angle = angle + startangle
    local angdiff = (endangle-startangle)/num
    for i=1,num do
        local offset = Vector3(rad * math.cos( angle ), 0, rad * math.sin( angle ))
        local newpt = pt + offset  
        local fx = SpawnPrefab("laserp")
        fx:Trigger(0 * FRAMES, {},{})
        fx.Transform:SetPosition(newpt.x,newpt.y,newpt.z)
        local burn =  SpawnPrefab("laserscorchp")
        burn.Transform:SetPosition(newpt.x,newpt.y,newpt.z)
        angle = angle + angdiff           
    end    
end

local function launchprojectile(inst, dir)
    local pt = Vector3(inst.Transform:GetWorldPosition())

    local theta = dir - (PI/6) + (PI/3*math.random())

    local offset = nil

        offset = FindWalkableOffset(pt, theta, 6 + math.random()*6, 12, true) --12

    if offset then
        pt.x = pt.x + offset.x
        pt.y=0
        pt.z = pt.z + offset.z
        inst.LaunchProjectile(inst,pt)
    end
end

local function CalcJumpSpeed(inst, target)
    local x, y, z = target.Transform:GetWorldPosition()
    local distsq = inst:GetDistanceSqToPoint(x, y, z)
    if distsq > 0 then
        inst:ForceFacePoint(x, y, z)
        local dist = math.sqrt(distsq) - (inst:GetPhysicsRadius(0) + target:GetPhysicsRadius(-1))
        if dist > 0 then
            return math.min(8, dist) / (10 * FRAMES)
        end
    end
    return 0
end

local longaction = "action"
local shortaction = "action"
local workaction = "attack"
local otheraction = "action"

local actionhandlers = PP_CommonStates.GetCommonActions(longaction, shortaction, workaction, otheraction)

local actionhandler_overrides = 
{ 
	ActionHandler(ACTIONS.HULK_TELEPORT, "action"),
	ActionHandler(ACTIONS.REVIVE_CORPSE, "failaction"), --no reviving
}

if #actionhandler_overrides > 0 then
	for i, v in ipairs(actionhandler_overrides) do
		table.insert(actionhandlers, v)
	end
end

local events=
{
	EventHandler("attacked", function(inst, data) 
		if data.stimuli and data.stimuli == "strong" and not (inst.sg:HasStateTag("transform") or inst.sg:HasStateTag("nointerrupt")) and not inst.components.health:IsDead() then
			inst.sg:GoToState("hit")
		elseif data.stimuli and (data.stimuli == "electric" or data.stimuli == "explosive") and not inst.sg:HasStateTag("nointerrupt") and not inst.components.health:IsDead() then
			inst.sg:GoToState("hit", data.stimuli)
		elseif not inst.components.health:IsDead() and not inst.sg:HasStateTag("busy") and not inst.sg:HasStateTag("nointerrupt") then 
			inst.sg:GoToState("hit") 
		end 
	end),
	PP_CommonHandlers.AddCommonHandlers(),
    EventHandler("doattack", function(inst, data) if not inst.components.health:IsDead() and (inst.sg:HasStateTag("hit") or not inst.sg:HasStateTag("busy")) then inst.sg:GoToState("attack", data.target) end end),
    CommonHandlers.OnSleep(),
    PP_CommonHandlers.OnLocomoteAdvanced(),
    CommonHandlers.OnFreeze(),
	---
	PP_CommonHandlers.OpenGift(),
	PP_CommonHandlers.OnDeath(),
		
	EventHandler("respawnfromghost", function(inst)  
			if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:Enable(true)
            end

            inst.components.health:SetInvincible(false)
            inst:ShowHUD(true)
            inst:SetCameraDistance()

            SerializeUserSession(inst) end),	
}

if CurrentRelease.GreaterOrEqualTo(ReleaseID.IDs.R08_ROT_TURNOFTIDES) then
	table.insert(events, PP_CommonHandlers.OnSink())
	table.insert(events, CommonHandlers.OnHop())
end

 local states=
{
    State{
        name = "idle",
        tags = {"idle", "canrotate"},

        onenter = function(inst, playanim)        
            inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation("idle")
        end,

        timeline=
        {
    ----------gears loop--------------
            TimeEvent(19*FRAMES, function(inst) 
                inst.SoundEmitter:SetParameter( "gears", "intensity", .2 )             
            end),
            TimeEvent(46*FRAMES, function(inst) 
                inst.SoundEmitter:SetParameter( "gears", "intensity", .5 )             
            end),

        },  

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end ),    
        },
    },
	
	------------------ACTIVATE--------------

    State{
        name = "activate",
        tags = {"busy"},
        
        onenter = function(inst, cb)
            if inst.components.locomotor then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("activate")
            inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/gears_LP","gears")
        end,
        
        timeline=
        {
            ----start---
            TimeEvent(46*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/start") end),
            -----------gears loop--------------------
            TimeEvent(0*FRAMES, function(inst)              
                inst.SoundEmitter:SetParameter( "gears", "intensity", 0.5 )
            end),
            TimeEvent(25*FRAMES, function(inst) 
                inst.SoundEmitter:SetParameter( "gears", "intensity", 0.8 )
            end),
            TimeEvent(50*FRAMES, function(inst)
                inst.SoundEmitter:SetParameter( "gears", "intensity", 0.9 )
            end),
            TimeEvent(75*FRAMES, function(inst) 
                inst.SoundEmitter:SetParameter( "gears", "intensity", 1 )
            end),

            TimeEvent(100*FRAMES, function(inst) 
                inst.SoundEmitter:SetParameter( "gears", "intensity", .7 )             
            end),

            ---------------electric--------------------
            TimeEvent(1*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/electro") end),
            TimeEvent(4*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/electro",nil,.5) end),
            TimeEvent(24*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/electro") end),
            TimeEvent(27*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/electro",nil,.5) end),
            TimeEvent(36*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/electro") end),
            TimeEvent(39*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/electro",nil,.5) end),
            TimeEvent(42*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/electro") end),
            TimeEvent(65*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/electro",nil,.5) end),
            TimeEvent(83*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/electro") end),
            TimeEvent(86*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/electro",nil,.5) end),
            TimeEvent(103*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/electro") end),
            TimeEvent(106*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/electro",nil,.25) end),
            TimeEvent(113*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/electro",nil,.25) end),
        ---------------green lights--------------------
            TimeEvent(6*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/arm/active") end),
            TimeEvent(10*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/arm/active") end),
            TimeEvent(12*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/arm/active",nil,.5) end),
            TimeEvent(20*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/arm/active") end),
            TimeEvent(40*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/head/active") end),
            TimeEvent(44*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/head/active") end),
            TimeEvent(54*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/leg/active") end),
            TimeEvent(56*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/leg/active") end),
            TimeEvent(58*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/leg/active") end),
            TimeEvent(60*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/leg/active") end),
        -------------step---------------
            TimeEvent(37*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/ribs/step") end),
            TimeEvent(101*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/leg/step") end),
        -------------servo---------------            
            TimeEvent(28*FRAMES, function(inst)
                inst.SoundEmitter:PlaySoundWithParams("dontstarve_DLC003/creatures/boss/hulk_metal_robot/ribs/servo", {intensity=math.random()})
            end),
            TimeEvent(46*FRAMES, function(inst)
                inst.SoundEmitter:PlaySoundWithParams("dontstarve_DLC003/creatures/boss/hulk_metal_robot/ribs/servo", {intensity=math.random()})
            end),
            TimeEvent(64*FRAMES, function(inst)
                inst.SoundEmitter:PlaySoundWithParams("dontstarve_DLC003/creatures/boss/hulk_metal_robot/ribs/servo", {intensity=math.random()})
            end),
            TimeEvent(84*FRAMES, function(inst)
                inst.SoundEmitter:PlaySoundWithParams("dontstarve_DLC003/creatures/boss/hulk_metal_robot/ribs/servo", {intensity=math.random()})
            end),
            TimeEvent(128*FRAMES, function(inst)
                inst.SoundEmitter:PlaySoundWithParams("dontstarve_DLC003/creatures/boss/hulk_metal_robot/ribs/servo", {intensity=math.random()})
            end),
    -------------tanut---------------
            TimeEvent(106*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/taunt") end),
            
        },

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State{
        name = "attack",
        tags = {"attack", "busy", "canrotate"},
        
        onenter = function(inst, target)
			local buffaction = inst:GetBufferedAction()
			local target = buffaction ~= nil and buffaction.target or nil
			if target and inst:IsNear(target, 6.5) then
				if inst.components.locomotor then
					inst.components.locomotor:StopMoving()
				end
				inst.components.combat:StartAttack()
				inst.AnimState:PlayAnimation("atk_chomp")
			else
				inst.sg:GoToState("lob", target)
			end
        end,

        timeline=
        {
            TimeEvent(8*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/dig") end),
            TimeEvent(22*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/drag") end),
            TimeEvent(15*FRAMES, function(inst) inst:PerformBufferedAction() end),
        },
        
        events=
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State{
        name = "hit",
        tags = {"busy"},
        
        onenter = function(inst)           
			inst.AnimState:PlayAnimation("hit")
            inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/hit")
            inst.Physics:Stop()            
        end,
        
        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end ),
        },        
    },  
	
    State{
        name = "death",
        tags = {"busy"},

        onenter = function(inst)
            inst.AnimState:PlayAnimation("death_explode")
            inst.Physics:Stop()
            RemovePhysicsColliders(inst)     
        end,
		
		timeline=
        {
            -------------green_explotion---------------            
            TimeEvent(2*FRAMES, function(inst)
                 inst.SoundEmitter:PlaySoundWithParams("dontstarve_DLC003/creatures/boss/hulk_metal_robot/laser", {intensity= .5})
            end),
            TimeEvent(6*FRAMES, function(inst)
                 inst.SoundEmitter:PlaySoundWithParams("dontstarve_DLC003/creatures/boss/hulk_metal_robot/laser", {intensity= .5})
            end),
            TimeEvent(23*FRAMES, function(inst)
                 inst.SoundEmitter:PlaySoundWithParams("dontstarve_DLC003/creatures/boss/hulk_metal_robot/laser", {intensity= .5})
            end),
            TimeEvent(26*FRAMES, function(inst)
                 inst.SoundEmitter:PlaySoundWithParams("dontstarve_DLC003/creatures/boss/hulk_metal_robot/laser", {intensity= .6})
            end),
            TimeEvent(33*FRAMES, function(inst)
                 inst.SoundEmitter:PlaySoundWithParams("dontstarve_DLC003/creatures/boss/hulk_metal_robot/laser", {intensity= .8})
            end),
            TimeEvent(36*FRAMES, function(inst)
                 inst.SoundEmitter:PlaySoundWithParams("dontstarve_DLC003/creatures/boss/hulk_metal_robot/laser", {intensity= 1})
            end),
            ----gears loop_---
            TimeEvent(17*FRAMES, function (inst) inst.SoundEmitter:KillSound("gears") end),
            ----death voice----
            TimeEvent(17*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/death") end),
            TimeEvent(20*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/death_taunt") end),
            ---- explode---
            TimeEvent(61*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/explode_small",nil,.7) end),
            TimeEvent(67*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/explode_small",nil,.8) end),
            TimeEvent(77*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/explode_small",nil,.1) end),
            TimeEvent(79*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/explode_small",nil,.8) end),
            TimeEvent(82*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/explode") end),

            TimeEvent(81*FRAMES, function(inst) 
                    ShakeIfClose_Explode(inst)      
                    local x,y,z = inst.Transform:GetWorldPosition()
                    SpawnPrefab("laserscorchp").Transform:SetPosition(x, 0, z) 
                    SpawnPrefab("laserscorchp").Transform:SetPosition(x+1, 0, z-1)    
                    SpawnPrefab("laserscorchp").Transform:SetPosition(x-1, 0, z+1)
                    SpawnPrefab("laserscorchp").Transform:SetPosition(x+1, 0, z)
                    SpawnPrefab("laserscorchp").Transform:SetPosition(x, 0, z+1)
                    SpawnPrefab("laserscorchp").Transform:SetPosition(x, 0, z-1)
                    SpawnPrefab("laserscorchp").Transform:SetPosition(x-1, 0, z)

                    inst.DoDamage(inst, 6)
                     inst.components.lootdropper:DropLoot(Vector3(inst.Transform:GetWorldPosition()))
					 inst.components.inventory:DropEverything(true)    
                end),
        },
		
        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.noskeleton = true
					PlayablePets.DoDeath(inst)
                end
            end),
        },

    },
	
	------------- TELEPORT ----------------------------

    State{
        name = "portal_jumpin",  
        tags = {"busy", "nointerrupt"},
        
        onenter = function(inst, pos)
			inst.can_tele = false
			if pos then
				inst.tele_pos = pos
			end
            if inst.components.locomotor then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("teleport_out_pre")

        end,

        events =
        {   
            EventHandler("animover", function(inst) inst.sg:GoToState("telportout", inst.tele_pos or nil) end ),        
        },
		
		onexit = function(inst)
			inst:DoTaskInTime(TELE_CD, function(inst) inst.can_tele = true end)
		end,

        timeline=
        {
                        ---teleport---
            TimeEvent(18*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/teleport_out") end),
                    -------------step---------------
            TimeEvent(9*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/leg/step") end),
            TimeEvent(20*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/leg/step") end),
        },
    },

    State{
        name = "telportout",  
        tags = {"busy", "nointerrupt"},
        
        onenter = function(inst)
            if inst.components.locomotor then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("teleport_out")          
        end,            
        
        events =
        {   
            EventHandler("animover", function(inst)                
                inst:Hide()
                inst:DoTaskInTime(0.5,function(inst) inst.sg:GoToState("telportin", inst.tele_pos or nil) end)
            end ),        
        },

        timeline=
        {
                        -----------servo---------------            
            TimeEvent(11*FRAMES, function(inst)
                inst.SoundEmitter:PlaySoundWithParams("dontstarve_DLC003/creatures/boss/hulk_metal_robot/ribs/servo", {intensity=math.random()})
            end),
            ----steps---
            TimeEvent(15*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/leg/step") end),
            TimeEvent(16*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/leg/step") end),
            TimeEvent(18*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/head/step") end),
            TimeEvent(39*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/leg/step") end),
            ----------gears loop--------------
            TimeEvent(19*FRAMES, function(inst) 
                inst.SoundEmitter:SetParameter( "gears", "intensity", .2 )             
            end),
            TimeEvent(5*FRAMES, function(inst)
                inst.DoDamage(inst,4)
            end),
            TimeEvent(10*FRAMES, function(inst)
                inst.Physics:SetActive(false)
				inst.components.health:SetInvincible(true)
                inst.DynamicShadow:Enable(false)
                --inst.Physics:ClearCollisionMask()        
                inst.DoDamage(inst,5)
            end),
            TimeEvent(15*FRAMES, function(inst)
                inst.DoDamage(inst,5)
            end),
            TimeEvent(20*FRAMES, function(inst)
                inst.DoDamage(inst,4)
            end),
        },
    },

    State{
        name = "telportin",  
        tags = {"busy", "nointerrupt"},
        
        onenter = function(inst, pos)
            inst:Show()
			inst.components.health:SetInvincible(false)
			if inst.tele_pos then 
				inst.Transform:SetPosition(inst.tele_pos.x, 0 ,inst.tele_pos.z) 
			end 
            inst.DynamicShadow:Enable(true)
            if inst.components.locomotor then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("teleport_in")
        end,            
		
		onexit = function(inst)
			inst.Physics:SetActive(true)
		end,
        
        events =
        {   
            EventHandler("animover", function(inst) 
                inst.sg:GoToState("idle")
            end ),        
        },

        timeline=
        {
            TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/teleport_in") end),
            -----------step---------------
            TimeEvent(15*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/leg/step") end),
            TimeEvent(19*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/leg/step") end),
            TimeEvent(16*FRAMES, function(inst) TheMixer:PushMix("boom")
            end),
            TimeEvent(17*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/bearger/groundpound")
            end),
            TimeEvent(17*FRAMES, function(inst)   
				--player logging--
				local name = inst:GetDisplayName() or "UNKNOWN USER"
				local pos = inst:GetPosition()
				print("LOGGED: "..name.."teleported in at "..pos.x.. ", 0," ..pos.z.." as Ancient Hulk, kleiid: "..inst.userid)
				------------------
                ShakeIfClose_Explode(inst)        
				inst.components.combat:SetDefaultDamage(300*2)
				inst.components.combat:DoAreaAttack(inst, 6)
				inst.components.combat:SetDefaultDamage(100*2)
                inst.components.groundpounder:GroundPound()
            end),
            TimeEvent(19*FRAMES, function(inst) TheMixer:PopMix("boom")
            end),
        },
    },
	
	 --------------------- BOMBS -------------------------------

    State{
        name = "special_sleep",  
        tags = {"busy"},
        
        onenter = function(inst)
			if inst.can_mine and inst.can_mine == true then
				if inst.components.locomotor then
					inst.components.locomotor:StopMoving()
				end
				inst.AnimState:PlayAnimation("atk_bomb_pre")
			else
				inst.AnimState:PlayAnimation("walk_pst")
				inst.sg:GoToState("idle")
			end
        end,            

        events =
        {   
            EventHandler("animover", function(inst) 
                inst.sg:GoToState("bomb")
            end ),        
        },
        timeline=
        {   
            -----rust----
            TimeEvent(16*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/rust",nil,.5) end),
            TimeEvent(18*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/rust",nil,.5) end),
            TimeEvent(20*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/rust",nil,.5) end),
            TimeEvent(22*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/rust",nil,.5) end),
            -----bomb ting----
            TimeEvent(18*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/ting") end),
            TimeEvent(20*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/ting") end),
            TimeEvent(22*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/ting") end),
            TimeEvent(24*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/ting") end),
            ----electro-----
            TimeEvent(12*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/electro",nil,.5) end),
            TimeEvent(15*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/electro",nil,.5) end),
            TimeEvent(19*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/electro",nil,.5) end),
            TimeEvent(23*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/electro",nil,.5) end),
        },
    },

    State{
        name = "bomb",  
        tags = {"busy"},
        
        onenter = function(inst)
            if inst.components.locomotor then
                inst.components.locomotor:StopMoving()
            end
			inst.can_mine = false
            inst.AnimState:PlayAnimation("atk_bomb_loop")
        end,            
        
        timeline=
        {   ---mine shoot---
            TimeEvent(4*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/mine_shot") end),
            TimeEvent(8*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/mine_shot") end),
            TimeEvent(12*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/mine_shot") end),
            TimeEvent(18*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/mine_shot") end),


            TimeEvent(1*FRAMES, function(inst)
                launchprojectile(inst, 0)
            end),
            TimeEvent(6*FRAMES, function(inst)    
                launchprojectile(inst, PI*0.5)
            end),
            TimeEvent(11*FRAMES, function(inst)    
                launchprojectile(inst, PI)
            end),
            TimeEvent(16*FRAMES, function(inst)    
                launchprojectile(inst, PI*1.5)
            end),
        },
		
		onexit = function(inst)
			inst:DoTaskInTime(MINE_CD, function(inst) inst.can_mine = true end)
		end,

        events =
        {   
            EventHandler("animover", function(inst) 
                inst.sg:GoToState("bomb_pst")
            end ),        
        },
    },

    State{
        name = "bomb_pst",  
        tags = {"busy"},
        
        onenter = function(inst)
            if inst.components.locomotor then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("atk_bomb_pst")
        end,            

        events =
        {   
            EventHandler("animover", function(inst) 
                inst.sg:GoToState("idle")
            end ),        
        },
        
        timeline=
        {
            -----rust----
            TimeEvent(6*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/rust",nil,.5) end),
            TimeEvent(9*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/rust",nil,.5) end),
            TimeEvent(11*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/rust",nil,.5) end),
            TimeEvent(17*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/rust",nil,.5) end),
            -----bomb ting----
            TimeEvent(8*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/ting") end),
            TimeEvent(11*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/ting") end),
            TimeEvent(13*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/ting") end),
            TimeEvent(19*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/ting") end),
             -----------servo---------------            
            TimeEvent(11*FRAMES, function(inst)
                inst.SoundEmitter:PlaySoundWithParams("dontstarve_DLC003/creatures/boss/hulk_metal_robot/ribs/servo", {intensity=math.random()}) end),
        },
    },
	
	---------------------------LOB---------------

    State{
        name = "lob",  
        tags = {"busy","canrotate"},
        
        onenter = function(inst, target)
			if target then
				inst.sg.statemem.attacktarget = target
				inst.AnimState:PlayAnimation("atk_lob")

				inst.lobtarget = nil
				if inst.components.combat.target and inst.components.combat.target:IsValid() then
					inst.lobtarget = Vector3(inst.sg.statemem.attacktarget.Transform:GetWorldPosition())
				else
					local angle = inst.Transform:GetRotation() * DEGREES
					local offset = Vector3(15 * math.cos( angle ), 0, -15 * math.sin( angle ))
					local pt = Vector3(inst.Transform:GetWorldPosition())

					inst.lobtarget = Vector3(pt.x + offset.x,pt.y + offset.y,pt.z + offset.z)
				end        
				if inst.components.locomotor then
					inst.components.locomotor:StopMoving()
				end
			else
				--print("ERROR: No target for hulk to lob at!")
				inst.sg:GoToState("idle")
			end
        end,            

        timeline =
        {
            TimeEvent(30*FRAMES, function(inst)     
                local pt = inst.lobtarget
                if inst.sg.statemem.attacktarget and inst.sg.statemem.attacktarget:IsValid() then   
                    inst.lobtarget = Vector3(inst.sg.statemem.attacktarget.Transform:GetWorldPosition())
                end                  
                inst.ShootProjectile(inst, inst.lobtarget)
            end),

            TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/laser_pre") end),
            TimeEvent(30*FRAMES, function(inst) 
                inst.SoundEmitter:PlaySoundWithParams("dontstarve_DLC003/creatures/boss/hulk_metal_robot/laser", {intensity=math.random()}) 
            end),
        },

        onupdate = function(inst)
            if inst.sg.statemem.attacktarget and inst.sg.statemem.attacktarget:IsValid() then 
                inst:ForceFacePoint(Vector3(inst.sg.statemem.attacktarget.Transform:GetWorldPosition()))
            end
        end,
		
		onexit = function(inst)
			inst.sg.statemem.attacktarget = nil
			inst.lobtarget = nil
		end,

        events =
        {   
            EventHandler("animover", function(inst) 
                inst.sg:GoToState("idle")
            end ),        
        },
    },

----------------------SPIN--------------------

    State{
        name = "special_atk1",  
        tags = {"busy"},
        
        onenter = function(inst)
            inst.Transform:SetNoFaced()
			inst.taunt = false
            if inst.components.locomotor then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("atk_circle")
        end,            
        
        timeline=
        {   
            -------------step---------------
            TimeEvent(10*FRAMES, function(inst)inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/ribs/step",nil,.5) end),
            TimeEvent(68*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/leg/step",nil,.5) end),
            TimeEvent(70*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/leg/step",nil,.5) end),
            TimeEvent(82*FRAMES, function(inst)inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/ribs/step",nil,.5) end),
            TimeEvent(90*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/leg/step",nil,.5) end),

            -----------servo---------------            
            TimeEvent(11*FRAMES, function(inst)
                inst.SoundEmitter:PlaySoundWithParams("dontstarve_DLC003/creatures/boss/hulk_metal_robot/ribs/servo", {intensity=math.random()})
            end),
            TimeEvent(62*FRAMES, function(inst)
                inst.SoundEmitter:PlaySoundWithParams("dontstarve_DLC003/creatures/boss/hulk_metal_robot/ribs/servo", {intensity=math.random()})
            end),
            ----electro-----
            TimeEvent(14*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/electro") 
            end),
            TimeEvent(21*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/electro")
            end),
            TimeEvent(26*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/electro") 
            end),
            ---------spin laser--------
            TimeEvent(30*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/spin") 
            end),
            TimeEvent(30*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/burn_LP","laserburn") 
            end),
            TimeEvent(49*FRAMES, function(inst)
                inst.SoundEmitter:KillSound("laserburn")
            end),
            ---mix---
            TimeEvent(49*FRAMES, function(inst) TheMixer:PushMix("boom")
            end),
             ---------spin laser ground--------
            TimeEvent(37*FRAMES, function(inst)
                inst.DoDamage(inst,BEAMRAD,0,45)
                inst.SoundEmitter:PlaySoundWithParams("dontstarve_DLC003/creatures/boss/hulk_metal_robot/laser", {intensity= 0})
                spawnburns(inst,BEAMRAD,0,45,5)
            end),
            TimeEvent(39*FRAMES, function(inst)
                inst.DoDamage(inst,BEAMRAD,45,90)
                spawnburns(inst,BEAMRAD,45,90,5)
            end),
            TimeEvent(40*FRAMES, function(inst)
                inst.DoDamage(inst,BEAMRAD,90,135)
                inst.SoundEmitter:PlaySoundWithParams("dontstarve_DLC003/creatures/boss/hulk_metal_robot/laser", {intensity= .3})
                spawnburns(inst,BEAMRAD,90,135,5)
            end),
            TimeEvent(41*FRAMES, function(inst)
                inst.DoDamage(inst,BEAMRAD,135,180)

                spawnburns(inst,BEAMRAD,135,180,5)
            end),
            TimeEvent(42*FRAMES, function(inst)
                inst.DoDamage(inst,BEAMRAD,180,225)

                inst.SoundEmitter:PlaySoundWithParams("dontstarve_DLC003/creatures/boss/hulk_metal_robot/laser", {intensity= 0.5})
                spawnburns(inst,BEAMRAD,180,225,5)
            end),            
            TimeEvent(45*FRAMES, function(inst)
                inst.DoDamage(inst,BEAMRAD,225,270)

                spawnburns(inst,BEAMRAD,225,270,5)
            end),
            TimeEvent(47*FRAMES, function(inst)
                inst.DoDamage(inst,BEAMRAD,270,315)
                inst.SoundEmitter:PlaySoundWithParams("dontstarve_DLC003/creatures/boss/hulk_metal_robot/laser", {intensity= 0.7})
                spawnburns(inst,BEAMRAD,270,315,5)
            end),                                    
            TimeEvent(48*FRAMES, function(inst)
                inst.DoDamage(inst,BEAMRAD,315,360)
                spawnburns(inst,BEAMRAD,315,360,5)
            end),
            TimeEvent(50*FRAMES, function(inst)
                inst.DoDamage(inst,BEAMRAD,0,45)

                inst.SoundEmitter:PlaySoundWithParams("dontstarve_DLC003/creatures/boss/hulk_metal_robot/laser", {intensity= 1})
                spawnburns(inst,BEAMRAD,0,45,5)
            end),
            ---mix---
            TimeEvent(51*FRAMES, function(inst) TheMixer:PopMix("boom")
            end),
        },


        onexit = function(inst)
            inst.Transform:SetSixFaced()
			inst.SoundEmitter:KillSound("laserburn")
            inst.spintime = 10            
			inst:DoTaskInTime(LASER_CD, function(inst) inst.taunt = true end)
        end, 

        events =
        {   
            EventHandler("animover", function(inst) 
                inst.sg:GoToState("idle")
            end ),        
        },
    },

	----------------------BARRIER--------------------

    State{
        name = "special_atk2",  
        tags = {"busy", "nointerrupt"},
        
        onenter = function(inst)
            inst.Transform:SetNoFaced()
			inst.taunt2 = false
            if inst.components.locomotor then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("atk_barrier")
        end,            
        
        timeline=
        {        
            --step---
            TimeEvent(12*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/ribs/step") 
            end),
            ---barrier attack---
            TimeEvent(19*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/barrier") 
            end),

            TimeEvent(67*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/bearger/groundpound")
            end),
            
            TimeEvent(67*FRAMES, function(inst) TheMixer:PushMix("boom")
            end),

            TimeEvent(90*FRAMES, function(inst) TheMixer:PopMix("boom")
            end),

            TimeEvent(64*FRAMES, function(inst)                
                inst.components.groundpounder.damageRings = 0
                inst.components.groundpounder.destructionRings = 4
                inst.components.groundpounder.numRings = 4                
                ShakeIfClose(inst)        
                inst.components.groundpounder:GroundPound()
				inst.components.combat:SetDefaultDamage(TheNet:GetServerGameMode() == "lavaarena" and PPHAM_FORGE.ANCIENT_HULK.ALT_DAMAGE or 400)
				inst.components.combat:DoAreaAttack(inst, 7, nil, nil, "electric")
				inst.components.combat:SetDefaultDamage(TheNet:GetServerGameMode() == "lavaarena" and PPHAM_FORGE.ANCIENT_HULK.DAMAGE or 200)
				inst.spawnbarrier(inst, inst:GetPosition())
                local pt = Vector3(inst.Transform:GetWorldPosition())
                local fx = SpawnPrefab("metal_hulk_ring_fxp")
                fx.Transform:SetPosition(pt.x,pt.y,pt.z)
                fx.AnimState:SetOrientation( ANIM_ORIENTATION.OnGround )
                fx.AnimState:SetLayer( LAYER_BACKGROUND )
                fx.AnimState:SetSortOrder( 2 )
            end),
        },

        onexit = function(inst)
            inst.Transform:SetSixFaced()
            inst.components.groundpounder.damageRings = 0
            inst.components.groundpounder.destructionRings = 3
            inst.components.groundpounder.numRings = 3  
			inst:DoTaskInTime(CAGE_CD, function(inst) inst.taunt2 = true end)
        end, 

        events =
        {   
            EventHandler("animover", function(inst) 
                inst.sg:GoToState("idle")
            end ),        
        },
    },
	---------------------------------
	State
    {
        name = "run_start",
        tags = { "moving", "running", "canrotate" },

        onenter = function(inst)
            inst.AnimState:PlayAnimation("walk_pre")			
        end,
		
		timeline = 
		{

		},
		
        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("run") end ),
        },
    },
	
	State
	{
		name = "run",
		tags = { "moving", "running", "canrotate" },

		onenter = function(inst)
			inst.AnimState:PlayAnimation("walk_loop")
			inst.components.locomotor:WalkForward()
		end,

		timeline=
        {
            TimeEvent(12*FRAMES, function(inst)
                DoFootstep(inst)
            end),
            TimeEvent(16*FRAMES, function(inst)
                 DoFootstep(inst)
            end),
            TimeEvent(20*FRAMES, function(inst) 
                inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/arm/step") 
			end),
            TimeEvent(3*FRAMES, function(inst) 
                inst.SoundEmitter:PlaySoundWithParams("dontstarve_DLC003/creatures/boss/hulk_metal_robot/ribs/servo", {intensity = 0.5}) 
			end),
            
        },
		
        onexit = function(inst)
       
        end,

		events=
		{
			EventHandler("animover", function(inst) 
				inst.sg:GoToState("run")  
			end ),
		},
	},
	
	State
    {
        name = "run_stop",
        tags = { "idle" },

        onenter = function(inst) 
            inst.components.locomotor:StopMoving()
			inst.AnimState:PlayAnimation("walk_pst")           
        end,

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end ),
        },
    },
}

--CommonStates.AddFrozenStates(states)

PP_CommonStates.AddKnockbackState(states, nil, "hit") --(states, timeline, anim, sounds, fns)
PP_CommonStates.AddActionStates(states, 
	{
		TimeEvent(0 * FRAMES, function(inst)
            inst:PerformBufferedAction()
        end),
	}, 
	"walk_pst", nil, nil, "idle", "walk_pst") --(states, timelines, anim, anim2, enteranim, loopanim, exitanim, noanim, events)
PP_CommonStates.AddCorpseStates(states, nil, 
	--timelines = 
	{
		corpse =
		{
			-------------green_explotion---------------            
            TimeEvent(2*FRAMES, function(inst)
                 inst.SoundEmitter:PlaySoundWithParams("dontstarve_DLC003/creatures/boss/hulk_metal_robot/laser", {intensity= .2})
            end),
            TimeEvent(6*FRAMES, function(inst)
                 inst.SoundEmitter:PlaySoundWithParams("dontstarve_DLC003/creatures/boss/hulk_metal_robot/laser", {intensity= .3})
            end),
            TimeEvent(23*FRAMES, function(inst)
                 inst.SoundEmitter:PlaySoundWithParams("dontstarve_DLC003/creatures/boss/hulk_metal_robot/laser", {intensity= .4})
            end),
            TimeEvent(26*FRAMES, function(inst)
                 inst.SoundEmitter:PlaySoundWithParams("dontstarve_DLC003/creatures/boss/hulk_metal_robot/laser", {intensity= .6})
            end),
            TimeEvent(33*FRAMES, function(inst)
                 inst.SoundEmitter:PlaySoundWithParams("dontstarve_DLC003/creatures/boss/hulk_metal_robot/laser", {intensity= .8})
            end),
            TimeEvent(36*FRAMES, function(inst)
                 inst.SoundEmitter:PlaySoundWithParams("dontstarve_DLC003/creatures/boss/hulk_metal_robot/laser", {intensity= 1})
            end),
            ----gears loop_---
            TimeEvent(17*FRAMES, function (inst) inst.SoundEmitter:KillSound("gears") end),
            ----death voice----
            TimeEvent(17*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/death") end),
            TimeEvent(20*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/death_taunt") end),
		},
		
		corpse_taunt =
		{
			----start---
            TimeEvent(46*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/start") end),
            -----------gears loop--------------------
            TimeEvent(0*FRAMES, function(inst)              
                inst.SoundEmitter:SetParameter( "gears", "intensity", 0.2 )
            end),
            TimeEvent(25*FRAMES, function(inst) 
                inst.SoundEmitter:SetParameter( "gears", "intensity", 0.3 )
            end),
            TimeEvent(50*FRAMES, function(inst)
                inst.SoundEmitter:SetParameter( "gears", "intensity", 0.4 )
            end),
            TimeEvent(75*FRAMES, function(inst) 
                inst.SoundEmitter:SetParameter( "gears", "intensity", 1 )
            end),

            TimeEvent(100*FRAMES, function(inst) 
                inst.SoundEmitter:SetParameter( "gears", "intensity", .7 )             
            end),

            ---------------electric--------------------
            TimeEvent(1*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/electro") end),
            TimeEvent(4*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/electro",nil,.5) end),
            TimeEvent(24*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/electro") end),
            TimeEvent(27*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/electro",nil,.5) end),
            TimeEvent(36*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/electro") end),
            TimeEvent(39*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/electro",nil,.5) end),
            TimeEvent(42*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/electro") end),
            TimeEvent(65*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/electro",nil,.5) end),
            TimeEvent(83*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/electro") end),
            TimeEvent(86*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/electro",nil,.5) end),
            TimeEvent(103*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/electro") end),
            TimeEvent(106*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/electro",nil,.25) end),
            TimeEvent(113*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/electro",nil,.25) end),
        ---------------green lights--------------------
            TimeEvent(6*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/arm/active") end),
            TimeEvent(10*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/arm/active") end),
            TimeEvent(12*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/arm/active",nil,.5) end),
            TimeEvent(20*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/arm/active") end),
            TimeEvent(40*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/head/active") end),
            TimeEvent(44*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/head/active") end),
            TimeEvent(54*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/leg/active") end),
            TimeEvent(56*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/leg/active") end),
            TimeEvent(58*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/leg/active") end),
            TimeEvent(60*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/leg/active") end),
        -------------step---------------
            TimeEvent(37*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/ribs/step") end),
            TimeEvent(101*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/leg/step") end),
        -------------servo---------------            
            TimeEvent(28*FRAMES, function(inst)
                inst.SoundEmitter:PlaySoundWithParams("dontstarve_DLC003/creatures/boss/hulk_metal_robot/ribs/servo", {intensity=math.random()})
            end),
            TimeEvent(46*FRAMES, function(inst)
                inst.SoundEmitter:PlaySoundWithParams("dontstarve_DLC003/creatures/boss/hulk_metal_robot/ribs/servo", {intensity=math.random()})
            end),
            TimeEvent(64*FRAMES, function(inst)
                inst.SoundEmitter:PlaySoundWithParams("dontstarve_DLC003/creatures/boss/hulk_metal_robot/ribs/servo", {intensity=math.random()})
            end),
            TimeEvent(84*FRAMES, function(inst)
                inst.SoundEmitter:PlaySoundWithParams("dontstarve_DLC003/creatures/boss/hulk_metal_robot/ribs/servo", {intensity=math.random()})
            end),
            TimeEvent(128*FRAMES, function(inst)
                inst.SoundEmitter:PlaySoundWithParams("dontstarve_DLC003/creatures/boss/hulk_metal_robot/ribs/servo", {intensity=math.random()})
            end),
    -------------tanut---------------
            TimeEvent(106*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/taunt") end),
		},
	
	},
	--anims = 
	{
		corpse = "death",
		corpse_taunt = "activate"
	},
	--sounds =
	{
		--corpse = "dontstarve_DLC003/creatures/enemy/grabbing_vine/death"
	},
	--fns =
	{

	}
	) ---(states, isflying, timelines, anims, sounds, fns, events, nofx)
PP_CommonStates.AddJumpInStates(states, nil, "walk_pst")
PP_CommonStates.AddOpenGiftStates(states, "idle")
PP_CommonStates.AddSailStates(states, {}, "walk_pst", "idle")
local simpleanim = "walk_pst"
local simpleidle = "idle"
local simplemove = "walk"
if CurrentRelease.GreaterOrEqualTo(ReleaseID.IDs.R08_ROT_TURNOFTIDES) then
CommonStates.AddHopStates(states, false, {pre = simplemove.."_pre", loop = simplemove.."_loop", pst = simplemove.."_pst"}, nil, "death")
end
PP_CommonStates.AddSailorStates(states, nil, 
{
	plank_idle = simpleanim,
	plank_idle_loop = simpleidle,
	plank_idle_pst = simpleanim,
	
	plank_hop_pre = simplemove.."_pre",
	plank_hop = simplemove.."_loop",
	
	steer_pre = simpleanim,
	steer_idle = simpleidle,
	steer_turning = simpleanim,
	stop_steering = simpleanim,
	
	row = simpleanim,
}
)
    
return StateGraph("ancient_hulkp", states, events, "activate", actionhandlers)

