require("stategraphs/commonstates")
require("stategraphs/ppstates")

local actionhandlers = 
{
    ActionHandler(ACTIONS.GOHOME, "gohome"),
    ActionHandler(ACTIONS.WALKTO, "daily_gift"),
    ActionHandler(ACTIONS.EAT, "eat"),
    ActionHandler(ACTIONS.CHOP, "chop"),
    --ActionHandler(ACTIONS.FIX, "chop"),
    --ActionHandler(ACTIONS.SPECIAL_ACTION, nil),
    ActionHandler(ACTIONS.PICKUP, "pickup"),
    ActionHandler(ACTIONS.EQUIP, "pickup"),
    ActionHandler(ACTIONS.ADDFUEL, "pickup"),
    ActionHandler(ACTIONS.TAKEITEM, "pickup"),
    --ActionHandler(ACTIONS.STOCK, "interact"),    
    ActionHandler(ACTIONS.SMOTHER, "smother"),
    --ActionHandler(ACTIONS.UNPIN, "pickup"), 
	
	
}

local events=
{
    CommonHandlers.OnStep(),
    CommonHandlers.OnLocomote(true,true),
    CommonHandlers.OnSleep(),
    CommonHandlers.OnFreeze(),
    --CommonHandlers.OnAttack(),
    EventHandler("doattack", function(inst)
        if inst:HasTag("_blunderbuss") then
            inst.sg:GoToState("attack_blunderbuss")
        else
            inst.sg:GoToState("attack")
        end
    end),
    CommonHandlers.OnAttacked(true),
    CommonHandlers.OnDeath(),
    EventHandler("transformnormal", function(inst) if inst.components.health:GetPercent() > 0 then inst.sg:GoToState("transformNormal") end end),
    EventHandler("behappy", 
        function(inst, data) 
          inst.sg:GoToState("happy")
        end),
    EventHandler("dance", 
        function(inst, data) 
            if not inst.sg:HasStateTag("busy") then
                inst.sg:GoToState("dance")
            end
        end),    
}

local function getSpeechType(inst, speech)
    local line = speech.DEFAULT

    if inst.talkertype and speech[inst.talkertype] then
        line = speech[inst.talkertype]
    end
    return line
end

local states=
{


     State {
        name = "idle",
        tags = {"idle", "canrotate"},
        onenter = function(inst, pushanim)
            inst.components.locomotor:StopMoving()
            local anim = "idle_loop"
               
            if pushanim then
                if type(pushanim) == "string" then
                    inst.AnimState:PlayAnimation(pushanim)
                end
                inst.AnimState:PushAnimation(anim, true)
            else
                inst.AnimState:PlayAnimation(anim, true)
            end
        end,
        
       events=
        {
            EventHandler("animover", function(inst) 
                inst.sg:GoToState("idle")                 
            end),
        }, 

    },    

     State {
        name = "dance",
        tags = {"canrotate", "busy"},
        onenter = function(inst, pushanim)
            if math.random()<0.3 then
                local speechset = getSpeechType(inst, STRINGS.CITY_PIG_TALK_FIESTA)
                inst.components.talker:Say(speechset[math.random(#speechset)])   
            end
            inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation("idle_happy")
        end,
        
       events=
        {
            EventHandler("animover", function(inst)                 
                inst.sg:GoToState("idle")                                 
            end),
        }, 
    },  

    State{
        name= "alert",
        tags = {"idle"},
        
        onenter = function(inst)
            if inst.alerted then
                inst.sg:GoToState("idle")
            else
                inst.alerted = true                
                inst:DoTaskInTime(120,function(inst) inst.alerted = nil end)

    			inst.Physics:Stop()
                local daytime = TheWorld.state.isday           
                if daytime then 
                    --inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/city_pig/conversational_talk")
                    if (inst:HasTag("emote_nohat") or math.random() < 0.3) and not inst:HasTag("emote_nocurtsy") then
                        inst.AnimState:PlayAnimation("emote_bow")           
                    else
                        inst.AnimState:PlayAnimation("emote_hat")           
                    end 
                end
            end
        end,
        
        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end ),
        },        
    },

    State{
        name= "scared",
        tags = {"idle"},
        
        onenter = function(inst)
            inst.Physics:Stop()
            inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/city_pig/scream")
            inst.AnimState:PlayAnimation("idle_scared")         
        end,
        
        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end ),
        },        
    },
    
    State{
        name = "happy",
        tags = {"busy"},
        
        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("idle_happy")
        end,
        
        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },

    State {
		name = "frozen",
		tags = {"busy"},
		
        onenter = function(inst)
            inst.AnimState:PlayAnimation("frozen")
            inst.Physics:Stop()
            --inst.components.highlight:SetAddColour(Vector3(82/255, 115/255, 124/255))
        end,
    },
    
    State{
        name = "death",
        tags = {"busy"},
        
        onenter = function(inst)
            inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/city_pig/death")
            inst.AnimState:PlayAnimation("death")
            inst.AnimState:Hide("ARM_carry")
	        inst.AnimState:Show("ARM_normal")
            inst.Physics:Stop()
            RemovePhysicsColliders(inst)            
            inst.components.lootdropper:DropLoot(Vector3(inst.Transform:GetWorldPosition()))            
        end,
        
    },
    
    State{
		name = "abandon",
		tags = {"busy"},
		
		onenter = function(inst, leader)
			inst.Physics:Stop()
			inst.AnimState:PlayAnimation("abandon")
            inst:FacePoint(Vector3(leader.Transform:GetWorldPosition()))
		end,
		
        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end ),
        },        
    },
    
    State{
        name = "attack",
        tags = {"attack", "busy"},
        
        onenter = function(inst)
            local equip = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS)
            if equip then
                if equip.prefab == "torch" then 
                    inst.SoundEmitter:PlaySound("dontstarve/wilson/attack_firestaff",nil,.5)
                elseif equip.prefab == "halberdp" then
                    inst.SoundEmitter:PlaySound("dontstarve_DLC003/common/items/weapon/halberd")
                elseif equip.soundoverride and equip.soundoverride.attack then
                    inst.SoundEmitter:PlaySound(equip.soundoverride.attack)
                end
            end
            inst.components.combat:StartAttack()
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("atk")
        end,
        
        timeline=
        {
            TimeEvent(13*FRAMES, function(inst) inst.components.combat:DoAttack() inst.sg:RemoveStateTag("attack") inst.sg:RemoveStateTag("busy") end),
        },
        
        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },

    State{
        name = "attack_blunderbuss",
        tags = {"attack", "busy"},
        
        onenter = function(inst, target)
			local target = inst.components.combat.target
			if target ~= nil then
				if target:IsValid() then
					inst:ForceFacePoint(target:GetPosition())
				end
			end
            if inst._attack_period then
                inst.components.combat:SetAttackPeriod(inst._attack_period * (math.random() + 0.5))
            end
            inst.components.combat:StartAttack()
            inst.Physics:Stop()
            inst.SoundEmitter:PlaySound(inst.sounds.gun_pre)
            inst.AnimState:PlayAnimation("blunderbuss_pre")
            inst.AnimState:PushAnimation("blunderbuss_idle", false)
            inst.AnimState:PushAnimation("blunderbuss_fire", false)
            inst.AnimState:PushAnimation("blunderbuss_pst", false)
        end,

        onupdate = function(inst)
            local target = inst.components.combat.target
			if target ~= nil and inst.sg:HasStateTag("attack") then
				if target:IsValid() then
					inst:ForceFacePoint(target:GetPosition())
				end
			end
        end,
        
        timeline=
        {
            TimeEvent(44*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound(inst.sounds.gun_fire)
                inst.sg:RemoveStateTag("attack")
                if inst.components.combat.target and inst.components.combat.target:IsValid() then
                    inst.FireProjectile(inst, inst.components.combat.target)
                end
            end),
        },
        
        events=
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end),
        },
    },

    State{
        name = "interact",
        tags = {"interact","busy"},
        
        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("interact")
        end,
        
        timeline=
        {
            
            TimeEvent(13*FRAMES, function(inst) inst:PerformBufferedAction() end ),
        },
        
        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },

    State{
        name = "smother",
        tags = {"interact","busy"},
        
        onenter = function(inst, target)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("interact")
        end,
        
        timeline=
        {
            
            TimeEvent(13*FRAMES, function(inst) inst:PerformBufferedAction() 
                if inst._extinguish_target and inst._extinguish_target:IsValid() and inst._extinguish_target.components.burnable then
                    inst._extinguish_target.components.burnable:Extinguish(true, TUNING.SMOTHERER_EXTINGUISH_HEAT_PERCENT)
                end
                inst._extinguish_target = nil
            end ),
        },
        
        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },

    State{
        name = "chop",
        tags = {"chopping"},
        
        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("atk")

        end,
        
        timeline=
        {          
            TimeEvent(13*FRAMES, function(inst)  
                inst.SoundEmitter:PlaySound("dontstarve_DLC003/common/crafted/house_repair")
                inst:PerformBufferedAction() 
            end ),
        },
        
        events=
        {
            EventHandler("animover", function(inst) 
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("idle") 
                end
            end),
        },
    },
    
    State{
        name = "eat",
        tags = {"busy"},
        
        onenter = function(inst)
            inst.Physics:Stop()            
            inst.AnimState:PlayAnimation("eat")
        end,
        
        timeline=
        {
            TimeEvent(10*FRAMES, function(inst) inst:PerformBufferedAction() end),
        },
        
        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end ),
        },        
    },
    State{
        name = "hit",
        tags = {"busy"},
        
        onenter = function(inst)

            inst.SoundEmitter:PlaySound(inst.sounds.scream,nil,.25)
            inst.SoundEmitter:PlaySound(inst.sounds.armorhit)
            inst.SoundEmitter:PlaySound(inst.sounds.alert)
            inst.AnimState:PlayAnimation("hit")
            inst.Physics:Stop()

            inst.components.combat.laststartattacktime = 0            
        end,
        
        timeline= 
        {
            
            TimeEvent(12*FRAMES, function (inst) 
                if inst.sounds.foley then
                    inst.SoundEmitter:PlaySound(inst.sounds.foley)
                end
            end),

            TimeEvent(13*FRAMES, function (inst) if inst:HasTag("guard") then inst.sg:GoToState("idle") end end),

        },

        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end ),
        },        
    },
}

CommonStates.AddWalkStates(states,
{
	walktimeline = {
		TimeEvent(0*FRAMES, function(inst) 
                if inst.sounds.step then
                    inst.SoundEmitter:PlaySound(inst.sounds.step)
                else
                    PlayFootstep(inst) 
                end
                if inst.sounds.foley then
                    inst.SoundEmitter:PlaySound(inst.sounds.foley)
                end
            end ),
		TimeEvent(12*FRAMES, function(inst)
                if inst.sounds.step then
                    inst.SoundEmitter:PlaySound(inst.sounds.step)
                else
                    PlayFootstep(inst) 
                end
                if inst.sounds.foley then
                    inst.SoundEmitter:PlaySound(inst.sounds.foley)
                end
            end),
	},
})
CommonStates.AddRunStates(states,
{
	runtimeline = {
		TimeEvent(0*FRAMES, function(inst)
            if inst.sounds.step then
                inst.SoundEmitter:PlaySound(inst.sounds.step)
            else
                PlayFootstep(inst) 
            end
        end),

        TimeEvent(3*FRAMES, function(inst) 
            if inst.sounds.foley then
                inst.SoundEmitter:PlaySound(inst.sounds.foley)
            end
         end ),
        
        TimeEvent(10*FRAMES, function(inst)
            if inst.sounds.step then
                inst.SoundEmitter:PlaySound(inst.sounds.step)
            else
                PlayFootstep(inst) 
            end
        end),

        TimeEvent(11*FRAMES, function(inst) 
            if inst.sounds.foley then
                inst.SoundEmitter:PlaySound(inst.sounds.foley)
            end
         end ),
	},
})

CommonStates.AddSleepStates(states,
{
	sleeptimeline = 
	{
		TimeEvent(35*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.sleep) end ),
	},
})

CommonStates.AddSimpleState(states,"refuse", "pig_reject", {"busy"})
CommonStates.AddFrozenStates(states)

CommonStates.AddSimpleActionState(states,"pickup", "pig_pickup", 10*FRAMES, {"busy"})
CommonStates.AddSimpleActionState(states, "gohome", "pig_pickup", 4*FRAMES, {"busy"})

return StateGraph("pigsoldierp", states, events, "idle", actionhandlers)