require("stategraphs/commonstates")

local function GetExcludeTags(inst)
	if TheNet:GetPVPEnabled() then
		return {"playerghost", "notarget", "shadow", "laserimmune", "laser", "DECOR", "INLIMBO"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion", "playerghost", "laserimmune", "laser", "DECOR", "INLIMBO"}
	else	
		return {"player", "companion", "playerghost", "notarget", "laserimmune", "laser", "DECOR", "INLIMBO"}
	end
end

local function ShakeIfClose(inst)
    ShakeAllCameras(CAMERASHAKE.VERTICAL, .3, .05, .5, inst, 20)
end

--Laser stuff

local SHAKE_DIST = 40

local function CalcJumpSpeed(inst, target)
    local x, y, z = target.Transform:GetWorldPosition()
    local distsq = inst:GetDistanceSqToPoint(x, y, z)
    if distsq > 0 then
		--print(distsq)
        inst:ForceFacePoint(x, y, z)
        local dist = math.sqrt(distsq) - (inst:GetPhysicsRadius(0) + target:GetPhysicsRadius(1))
        if dist > 0 then
            return math.min(8, dist) / (15 * FRAMES)
        end
    end
    return 0
end

local function SetJumpPhysics(inst)
	ToggleOffCharacterCollisions(inst)
end

local function SetNormalPhysics(inst)
	ToggleOnCharacterCollisions(inst)
end

local function RemoveMoss(inst)
    if inst.ismossy then  
		inst.sleepstarttime = nil
        inst.ismossy = nil
        local x, y, z = inst.Transform:GetWorldPosition()
        ---for i=1,math.random(12,15) do
            --inst:DoTaskInTime(math.random()*0.5,function()                
                --local fx = SpawnPrefab("robot_leaf_fxp")
                --fx.Transform:SetPosition(x + (math.random()*4) -2 ,y,z + (math.random()*4) -2)
            --end)
        --end
    end    
end

local function SetFires(x,y,z, rad)
    for i, v in ipairs(TheSim:FindEntities(x, 0, z, rad, nil, GetExcludeTags())) do 
        if v.components.burnable and MOBFIRE == "Disable" then
            v.components.burnable:Ignite()
        end
    end
end
local function DoDamage(inst, rad)
    local targets = {}
    local x, y, z = inst.Transform:GetWorldPosition()
  
    SetFires(x,y,z, rad)
    for i, v in ipairs(TheSim:FindEntities(x, 0, z, rad, nil, GetExcludeTags())) do  --  { "_combat", "pickable", "campfire", "CHOP_workable", "HAMMER_workable", "MINE_workable", "DIG_workable" }
        if not targets[v] and v:IsValid() and not v:IsInLimbo() and not (v.components.health ~= nil and v.components.health:IsDead()) and not v:HasTag("laserimmune") and v ~= inst then            
            local vradius = 0
            if v.Physics then
                vradius = v.Physics:GetRadius()
            end

            local range = rad + vradius
            if v:GetDistanceSqToPoint(Vector3(x, y, z)) < range * range then
                local isworkable = false
                if v.components.workable ~= nil then
                    local work_action = v.components.workable:GetWorkAction()
                    --V2C: nil action for campfires
                    isworkable =
                        (   work_action == nil and v:HasTag("campfire")    ) or
                        
                            (   work_action == ACTIONS.CHOP or
                                work_action == ACTIONS.HAMMER or
                                work_action == ACTIONS.MINE or   
                                work_action == ACTIONS.DIG
                            )
                end
                if isworkable then
                    targets[v] = true
                    v:DoTaskInTime(0.6, function() 
                        if v.components.workable then
                            v.components.workable:Destroy(inst) 
                            local vx,vy,vz = v.Transform:GetWorldPosition()
                            v:DoTaskInTime(0.3, function() SetFires(vx,vy,vz,1) end)
                        end
                     end)
                    if v:IsValid() and v:HasTag("stump") then
                       -- v:Remove()
                    end
                elseif v.components.pickable ~= nil
                    and v.components.pickable:CanBePicked()
                    and not v:HasTag("intense") then
                    targets[v] = true
                    local num = v.components.pickable.numtoharvest or 1
                    local product = v.components.pickable.product
                    local x1, y1, z1 = v.Transform:GetWorldPosition()
                    v.components.pickable:Pick(inst) -- only calling this to trigger callbacks on the object
                    if product ~= nil and num > 0 then
                        for i = 1, num do
                            local loot = SpawnPrefab(product)
                            loot.Transform:SetPosition(x1, 0, z1)
                            targets[loot] = true
                        end
                    end

                elseif v.components.health then                    
                    inst.components.combat:DoAttack(v)                
                    if v:IsValid() then
                        if not v.components.health or not v.components.health:IsDead() then
                            if v.components.freezable ~= nil then
                                if v.components.freezable:IsFrozen() then
                                    v.components.freezable:Unfreeze()
                                elseif v.components.freezable.coldness > 0 then
                                    v.components.freezable:AddColdness(-2)
                                end
                            end
                            if v.components.temperature ~= nil then
                                local maxtemp = math.min(v.components.temperature:GetMax(), 10)
                                local curtemp = v.components.temperature:GetCurrent()
                                if maxtemp > curtemp then
                                    v.components.temperature:DoDelta(math.min(10, maxtemp - curtemp))
                                end
                            end
                        end
                    end                   
                end
                if v:IsValid() and v.AnimState then
                    SpawnPrefab("laserhitp"):SetTarget(v)
                end
            end
        end
    end
end

local function UpdateHit(inst)
    if inst:IsValid() then
        local oldflash = inst.flash
        inst.flash = math.max(0, inst.flash - .075)
        if inst.flash > 0 then
            local c = math.min(1, inst.flash)
            if inst.components.colouradder ~= nil then
                inst.components.colouradder:PushColour(inst, c, 0, 0, 0)
            else
                inst.AnimState:SetAddColour(c, 0, 0, 0)
            end
            if inst.flash < .3 and oldflash >= .3 then
                if inst.components.bloomer ~= nil then
                    inst.components.bloomer:PopBloom(inst)
                else
                    inst.AnimState:ClearBloomEffectHandle()
                end
            end
        else
            inst.flashtask:Cancel()
            inst.flashtask = nil
        end
    end
end

local function PowerGlow(inst)
    
    if inst.components.bloomer ~= nil then
        inst.components.bloomer:PushBloom(inst, "shaders/anim.ksh", -1)
    else
        inst.AnimState:SetBloomEffectHandle("shaders/anim.ksh")
    end
    inst.flash = 1.7 -- .8 + math.random() * .4
    inst.flashtask = inst:DoPeriodicTask(0, UpdateHit, nil, inst)
end

local function SpawnLaser(inst)
	inst.SoundEmitter:PlaySound("dontstarve/creatures/deerclops/laser")
    assert(inst.sg.statemem.targetpos)
    local numsteps = 10   
    local x, y, z = inst.Transform:GetWorldPosition()

    --if inst.components.combat.target then
    --    x,y,z = inst.components.combat.target.Transform:GetWorldPosition()
    --end

    local xt = inst.sg.statemem.targetpos.x
    local yt = inst.sg.statemem.targetpos.y
    local zt = inst.sg.statemem.targetpos.z

    local dist =  math.sqrt(inst:GetDistanceSqToPoint(  Vector3(xt, yt, zt)  )) -3--  math.sqrt( ) ) - 2

    local angle = (inst:GetAngleToPoint(xt, yt, zt) +90)* DEGREES

    local step = .75   
    local ground = TheWorld.Map
    local targets, skiptoss = {}, {}
    local i = -1
    local noground = false
    local fx, delay, x1, z1
    while i < numsteps do
        i = i + 1
        dist = dist + step
        delay = math.max(0, i - 1)
        x1 = x + dist * math.sin(angle)
        z1 = z + dist * math.cos(angle)
        local tile = ground:GetTileAtPoint(x1, 0, z1)
        
        if tile == 255 or tile < 2 then
            if i <= 0 then
                return
            end
            noground = true
        end
        fx = SpawnPrefab(i > 0 and "laserp" or "laserp")
        fx.caster = inst
        fx.Transform:SetPosition(x1, 0, z1)
        fx:Trigger(delay * FRAMES, targets, skiptoss)
        if i == 0 then
        --    ShakeAllCameras(CAMERASHAKE.FULL, .7, .02, .6, fx, 30)
        end
        if noground then
            break
        end
    end
--[[
    if i < numsteps then
        dist = (i + .5) * step + offset
        x1 = x + dist * math.sin(angle)
        z1 = z + dist * math.cos(angle)
    end
    ]]

    local function delay_spawn(delay_offset)
        fx = SpawnPrefab("laserp")
        fx.Transform:SetPosition(x1, 0, z1)
        fx:Trigger((delay + delay_offset) * FRAMES, targets, skiptoss)
    end

    delay_spawn(1)
    delay_spawn(2)
end

local function SetLightValue(inst, val)
    if inst.Light ~= nil then
        inst.Light:SetIntensity(.6 * val * val)
        inst.Light:SetRadius(5 * val)
        inst.Light:SetFalloff(3 * val)
    end
end

local function SetLightValueAndOverride(inst, val, override)
    if inst.Light ~= nil then
        inst.Light:SetIntensity(.6 * val * val)
        inst.Light:SetRadius(5 * val)
        inst.Light:SetFalloff(3 * val)
        inst.AnimState:SetLightOverride(override)
    end
end

local function SetLightColour(inst, val)
    if inst.Light ~= nil then
        inst.Light:SetColour(val, 0, 0)
    end
end

local longaction = "action"
local shortaction = "action"
local workaction = "action"
local otheraction = "action"

local actionhandlers = PP_CommonStates.GetCommonActions(longaction, shortaction, workaction, otheraction)

local actionhandler_overrides = 
{ 
	ActionHandler(ACTIONS.ATTACK, function(inst)
		return inst.jump_attack and "leap_attack_pre" or "laserbeam"
	end),
	ActionHandler(ACTIONS.REVIVE_CORPSE, function(inst)
		if inst.prefab == "ancient_robot_clawp" then
			return "revivecorpse"
		else
			return "taunt" --can't revive
		end
	end),
}

if #actionhandler_overrides > 0 then
	for i, v in ipairs(actionhandler_overrides) do
		table.insert(actionhandlers, v)
	end
end

--for action_loop state
local function StopActionMeter(inst, flash)
    if inst.HUD ~= nil then
        inst.HUD:HideRingMeter(flash)
    end
    if inst.sg.mem.actionmetertask ~= nil then
        inst.sg.mem.actionmetertask:Cancel()
        inst.sg.mem.actionmetertask = nil
        inst.player_classified.actionmeter:set(flash and 1 or 0)
    end
end

local function UpdateActionMeter(inst, starttime)
    inst.player_classified.actionmeter:set_local(math.min(255, math.floor((GetTime() - starttime) * 10 + 2.5)))
end

local function StartActionMeter(inst, duration)
    if inst.HUD ~= nil then
        inst.HUD:ShowRingMeter(inst:GetPosition(), duration)
    end
    inst.player_classified.actionmetertime:set(math.min(255, math.floor(duration * 10 + .5)))
    inst.player_classified.actionmeter:set(2)
    if inst.sg.mem.actionmetertask == nil then
        inst.sg.mem.actionmetertask = inst:DoPeriodicTask(.1, UpdateActionMeter, nil, GetTime())
    end
end

local events=
{
	EventHandler("attacked", function(inst, data) 
		if data.stimuli and data.stimuli == "strong" and not (inst.sg:HasStateTag("transform") or inst.sg:HasStateTag("nointerrupt")) and not inst.components.health:IsDead() then
			inst.sg:GoToState(inst.sg:HasStateTag("sleeping") and "sleep_hit" or "hit")
		elseif data.stimuli and (data.stimuli == "electric" or data.stimuli == "explosive") and not inst.components.health:IsDead() then
			inst.sg:GoToState(inst.sg:HasStateTag("sleeping") and "sleep_hit" or "hit")
		elseif not inst.components.health:IsDead() and not inst.sg:HasStateTag("busy") and not inst.sg:HasStateTag("nointerrupt") then 
			inst.sg:GoToState(inst.sg:HasStateTag("sleeping") and "sleep_hit" or "hit")
		end 
	end),
    EventHandler("death", function(inst, data)
        if inst.sleepingbag ~= nil and (inst.sg:HasStateTag("bedroll") or inst.sg:HasStateTag("tent")) then -- wakeup on death to "consume" sleeping bag first
            inst.sleepingbag.components.sleepingbag:DoWakeUp()
            inst.sleepingbag = nil
        end

        if (data ~= nil and data.cause == "file_load") or inst.components.revivablecorpse ~= nil then
            inst.sg:GoToState("corpse", true)
        else
            inst.sg:GoToState("death")
        end
    end),
	PP_CommonHandlers.AddCommonHandlers(),
    EventHandler("doattack", function(inst, data) if not inst.components.health:IsDead() and (inst.sg:HasStateTag("hit") or not inst.sg:HasStateTag("busy")) then inst.sg:GoToState("attack", data.target) end end),
    CommonHandlers.OnSleep(),
    --CommonHandlers.OnLocomote(true,false),
	EventHandler("locomote", function(inst, data)
        if inst.sg:HasStateTag("busy") then
            return
        end
        local is_moving = inst.sg:HasStateTag("moving")
        local should_move = inst.components.locomotor:WantsToMoveForward()
		local is_looking = inst.sg:HasStateTag("looking")

        if inst.sg:HasStateTag("bedroll") or inst.sg:HasStateTag("tent") or inst.sg:HasStateTag("waking") then -- wakeup on locomote
            if inst.sleepingbag ~= nil and inst.sg:HasStateTag("sleeping") then
                inst.sleepingbag.components.sleepingbag:DoWakeUp()
                inst.sleepingbag = nil
				inst:Show()
				--inst.AnimState:PushAnimation("pig_pickup")
				SetSleeperAwakeState(inst)
				--inst.AnimState:PushAnimation("pig_pickup")
				--inst.sg:GoToState("idle", true)
				
            end 
			elseif is_moving and not should_move then
            inst.sg:GoToState("run_stop")
        elseif not is_moving and should_move and not is_looking then
            inst.sg:GoToState("run_start")
        elseif data.force_idle_state and not (is_moving or should_move or inst.sg:HasStateTag("idle") or is_looking) then
            inst.sg:GoToState("idle")
        end
    end),
    CommonHandlers.OnFreeze(),
	
	 EventHandler("ms_opengift",
        function(inst)
            if not inst.sg:HasStateTag("busy") then
                inst.sg:GoToState("opengift")
            end
        end),
		
	EventHandler("respawnfromghost", function(inst)  
			if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:Enable(true)
            end

            inst.components.health:SetInvincible(false)
            inst:ShowHUD(true)
            inst:SetCameraDistance()

            SerializeUserSession(inst) end),	
}

if CurrentRelease.GreaterOrEqualTo(ReleaseID.IDs.R08_ROT_TURNOFTIDES) then
	table.insert(events, PP_CommonHandlers.OnSink())
	table.insert(events, CommonHandlers.OnHop())
end


 local states=
{

    State{
        name = "idle",
        tags = {"idle", "canrotate"},
        
        onenter = function(inst)
            inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation("idle", true)
            inst.sg:SetTimeout(2 + 2*math.random())
        end,
        
        ontimeout=function(inst)
            inst.sg:GoToState("taunt")
        end,
    },
	
	State{
        name = "gore",
        tags = {"attack", "busy"},
        
        onenter = function(inst, target)
            if inst.components.locomotor then
                inst.components.locomotor:StopMoving()
            end
            inst.components.combat:StartAttack()
            inst.AnimState:PlayAnimation("atk")
            inst.sg.statemem.target = target
        end,
        
        timeline = 
        {
           TimeEvent(16*FRAMES, function(inst) inst:PerformBufferedAction() end),
           TimeEvent(13*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/hippo/leap_attack") end ),

        },        

        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State{
        name = "hit",
        tags = {"busy", "hit"},
        
        onenter = function(inst, pushanim)
            --inst.wantstodeactivate = nil
			inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/ribs/hit")
			inst.sleepstarttime = nil
			RemoveMoss(inst)
            inst.components.locomotor:StopMoving()          
            inst.AnimState:PlayAnimation("hit")          
        end,   
        
        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },           

    },
	
	State{
        name = "sleep_hit",
        tags = {"busy","dormant"},
        
        onenter = function(inst, pushanim)
            --inst.wantstodeactivate = nil
			inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/ribs/hit")
			inst.sleepstarttime = nil
			RemoveMoss(inst)
            inst.components.locomotor:StopMoving()          
            inst.AnimState:PlayAnimation("dormant_hit")          
        end,   
        
        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("sleeping") end),
        },           

    },
	
	State{
        name = "shock",
        tags = {"busy","activating"},
        
        onenter = function(inst, pushanim)
            --inst.AnimState:SetBloomEffectHandle( "shaders/anim.ksh" )
            RemoveMoss(inst) 
           -- inst:RemoveTag("dormant")      
            inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation("shock")
        end,
        
        timeline=
        {
--------------------------------------------- leg
            TimeEvent(6*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_legp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/electro",nil,0.5)
                end
            end),
            TimeEvent(9*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_legp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/electro",nil,0.5)
                end
            end),
--------------------------------------------- ribs
            TimeEvent(9*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_spiderp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/electro",nil,0.5)
                end
            end),
            TimeEvent(13*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_legp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/electro",nil,0.5)
                end
            end),
--------------------------------------------- arm
            TimeEvent(10*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_clawp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/electro",nil,0.5)
                end
            end),
            TimeEvent(10*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_clawp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/electro",nil,0.5)
                end
            end),
    --------------------------------------------- head
            TimeEvent(2*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_headp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/electro",nil,0.5)
                end
            end),
            TimeEvent(5*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_headp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/electro")
                end
            end),
            TimeEvent(7*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_headp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/electro",nil,0.5)
                end
            end),
            TimeEvent(10*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_headp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/electro")
                end
            end),
        },

        events=
        {
            EventHandler("animover", function(inst)
                inst.sg:GoToState("activate") 
            end),
        }, 
    },
	
	State{
        name = "activate",
        tags = {"busy","activating"},
        
        onenter = function(inst, pushanim)
            RemoveMoss(inst)
            
            inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation("activate")
            inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/gears_LP","gears")
            inst.SoundEmitter:SetParameter("gears", "intensity", .5)
        end,
        
        timeline=
        {
--------------------------------------------- arm/claw
            TimeEvent(3*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_clawp" then
                    inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/arm/active")
                    inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/arm/start")
                end
            end),
            TimeEvent(4*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_clawp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/arm/active")
                end
            end),
            TimeEvent(6*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_clawp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/arm/active") 
                end
            end),
            TimeEvent(8*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_clawp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/arm/active") 
                end
            end),
            TimeEvent(9*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_clawp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/arm/active") 
                end
            end),
            TimeEvent(12*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_clawp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/arm/active") 
                end
            end),

            TimeEvent(16*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_clawp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/arm/active") 
                end
            end),
            TimeEvent(30*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_clawp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/electro")
                end
            end),
            TimeEvent(37*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_clawp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/arm/servo") 
                end
            end),
            TimeEvent(30*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_clawp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/electro")
                end
            end),
            TimeEvent(41*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_clawp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/electro")
                end
            end),
            TimeEvent(50*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_clawp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/electro")
                end
            end),
            TimeEvent(51*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_clawp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/arm/servo") 
                end
            end),
            TimeEvent(53*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_clawp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/electro")
                end
            end),
            TimeEvent(62*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_clawp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/arm/servo") 
                end
            end),
            TimeEvent(70*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_clawp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/arm/servo") 
                end
            end),
--------------------------------------------- head
            TimeEvent(0*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_headp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/head/active")
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/head/start")
                end
            end),

            TimeEvent(1*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_headp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/head/active")
                end
            end),
            TimeEvent(3*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_headp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/head/active")
                end
            end),
            TimeEvent(5*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_headp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/head/active")
                end
            end),
            TimeEvent(6*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_headp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/head/active")
                end
            end),
            TimeEvent(9*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_headp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/head/active")
                end
            end),
            TimeEvent(12*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_headp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/head/active")
                end
            end),
            TimeEvent(14*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_headp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/electro")
                end
            end),
            TimeEvent(16*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_headp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/head/active")
                end
            end),
            TimeEvent(17*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_headp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/electro")
                end
            end),
            TimeEvent(27*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_headp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/electro")
                end
            end),
            TimeEvent(30*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_headp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/electro")
                end
            end),
            TimeEvent(37*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_headp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/electro")
                end
            end),
            TimeEvent(40*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_headp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/electro")
                end
            end),
            TimeEvent(54*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_headp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/electro")
                end
            end),
            TimeEvent(57*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_headp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/electro")
                end
            end),
            
            --------------------------------------------- leg
            TimeEvent(0*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_legp" then
                    inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/leg/active")
                    inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/leg/start")
                end
            end),
            
            TimeEvent(4*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_legp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/leg/active")
                end
            end),
            TimeEvent(5*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_legp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/leg/active")
                end
            end),
            TimeEvent(9*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_legp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/leg/active")
                end
            end),
            TimeEvent(13*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_legp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/leg/active")
                end
            end),
             TimeEvent(14*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_legp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/electro")
                end
            end),
            TimeEvent(17*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_legp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/electro")
                end
            end),
            TimeEvent(27*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_legp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/leg/servo")
                end
            end),
            TimeEvent(44*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_legp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/leg/servo")
                end
            end),
            TimeEvent(58*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_legp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/leg/step",nil,.06)
                end
            end),
            --------------------------------------------- ribs
            TimeEvent(2*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_spiderp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/ribs/active")
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/ribs/start")
                end
            end),
            
            TimeEvent(4*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_spiderp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/ribs/active")
                end
            end),
            TimeEvent(6*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_spiderp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/ribs/active")
                end
            end),
            TimeEvent(8*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_spiderp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/ribs/active")
                end
            end),
            TimeEvent(9*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_spiderp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/electro")
                end
            end),
            TimeEvent(10*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_spiderp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/ribs/active")
                end
            end),
            TimeEvent(12*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_spiderp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/ribs/active")
                end
            end),
            TimeEvent(13*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_spiderp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/electro")
                end
            end),
            TimeEvent(14*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_spiderp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/ribs/active")
                end
            end),
            TimeEvent(16*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_spiderp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/ribs/active")
                end
            end),
            TimeEvent(18*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_spiderp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/electro")
                end
            end),
            TimeEvent(21*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_spiderp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/electro")
                end
            end),
            TimeEvent(30*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_spiderp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/electro")
                end
            end),
            TimeEvent(33*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_spiderp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/electro")
                end
            end),
            TimeEvent(36*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_spiderp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/electro")
                end
            end),
            TimeEvent(39*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_spiderp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/electro")
                end
            end),
 
        },
        
        events=
        {
            EventHandler("animover", function(inst)
                --inst:RestartBrain()
                inst.sg:GoToState("taunt") 
            end),
        },        
    }, 

    State{
        name = "deactivate",
        tags = {"busy","deactivating"},
        
        onenter = function(inst, pushanim)
            --inst.AnimState:SetBloomEffectHandle( "" )
           -- inst.wantstodeactivate = nil
            --inst:StopBrain()
            inst.components.locomotor:StopMoving()
            --inst:AddTag("dormant")              
            inst.AnimState:PlayAnimation("deactivate")
            if inst.prefab == "ancient_robot_clawp" then 
                inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/arm/stop")
                end
            if inst.prefab == "ancient_robot_headp" then 
                inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/head/stop")
                end
            if inst.prefab == "ancient_robot_legp" then 
                inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/leg/stop")
                end
            if inst.prefab == "ancient_robot_spiderp" then 
                inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/ribs/stop")
                end
        end,

        timeline=
        {
--------------------------------------------- arm/claw
            TimeEvent(0*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_clawp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/arm/green")
                end
            end),
            TimeEvent(9*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_clawp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/arm/green")
                end
            end),
            TimeEvent(14*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_clawp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/arm/green")
                end
            end),
            TimeEvent(21*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_clawp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/arm/green")
                end
            end),
            TimeEvent(38*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_clawp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/arm/green")
                end
            end),
            TimeEvent(36*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_clawp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/arm/servo")
                end
            end),
--------------------------------------------- head
            TimeEvent(5*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_headp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/head/green")
                end
            end),
            TimeEvent(13*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_headp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/head/green")
                end
            end),
            TimeEvent(19*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_headp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/head/green")
                end
            end),
            TimeEvent(23*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_headp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/head/green")
                end
            end),
            TimeEvent(38*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_headp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/head/green")
                end
            end),
--------------------------------------------- leg
            TimeEvent(3*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_legp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/leg/green")
                end
            end),
            TimeEvent(12*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_legp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/leg/green")
                end
            end),
            TimeEvent(16*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_legp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/leg/green")
                end
            end),
            TimeEvent(23*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_legp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/leg/green")
                end
            end),
            TimeEvent(31*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_legp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/leg/green")
                end
            end),
            TimeEvent(43*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_legp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/leg/step")
                end
            end),
    --------------------------------------------- ribs
            TimeEvent(0*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_spiderp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/ribs/green")
                end
            end),
            TimeEvent(9*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_spiderp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/ribs/green")
                end
            end),
            TimeEvent(14*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_spiderp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/ribs/green")
                end
            end),
            TimeEvent(21*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_spiderp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/ribs/green")
                end
            end),

        },

        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle_dormant") end),
        },        
    },            
    
	 State{
        name = "taunt",
        tags = {"busy","canrotate"},
        
        onenter = function(inst)
            inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation("taunt")
        end,

        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },        


        timeline =
        {
--------------------------------------------- arm/claw
            TimeEvent(3*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_clawp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/arm/servo")
                end
            end),
            TimeEvent(7*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_clawp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/arm/servo")
                end
            end),
            TimeEvent(15*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_clawp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/arm/taunt")
                end
            end),
            TimeEvent(29*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_clawp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/arm/servo")
                end
            end),
            TimeEvent(33*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_clawp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/arm/servo")
                end
            end),

--------------------------------------------- ribs
            TimeEvent(4*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_spiderp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/ribs/servo")
                end
            end),
            TimeEvent(17*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_spiderp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/ribs/servo")
                end
            end),
            TimeEvent(21*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_spiderp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/ribs/taunt")
                end
            end),
            TimeEvent(45*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_spiderp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/ribs/servo")
                end
            end),

--------------------------------------------- leg
            TimeEvent(5*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_legp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/leg/servo")
                end
            end),
            TimeEvent(15*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_legp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/leg/servo")
                end
            end),
            TimeEvent(42*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_legp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/leg/servo")
                end
            end),
            --------------------------------------------- head
            TimeEvent(0*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_headp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/head/servo")
                end
            end),
            TimeEvent(2*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_headp" then
                    inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/head/taunt")
                end
            end),
            TimeEvent(12*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_headp" then
                    inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/head/step")
                    inst.SoundEmitter:SetParameter("steps", "intensity", .05)

                end
            end),
            TimeEvent(17*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_headp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/head/servo")
                end
            end),
            TimeEvent(19*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_headp" then
                    inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/head/taunt")
                end
            end),
            TimeEvent(24*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_headp" then
                    inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/head/step")
                    inst.SoundEmitter:SetParameter("steps", "intensity", .08)

                end
            end),
            TimeEvent(32*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_headp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/head/servo")
                end
            end),
--------------------------------------------------

            TimeEvent(23 * FRAMES, function(inst)
                if inst:HasTag("lightning_taunt") then
					--ms_lightningstrike here
                end
            end),
        },


    },
	
    State{
        name = "special_atk1",
        tags = {"busy","canrotate"},
        
        onenter = function(inst)
            inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation("taunt")
        end,

        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },        


        timeline =
        {
--------------------------------------------- arm/claw
            TimeEvent(3*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_clawp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/arm/servo")
                end
            end),
            TimeEvent(7*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_clawp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/arm/servo")
                end
            end),
            TimeEvent(15*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_clawp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/arm/taunt")
                end
            end),
            TimeEvent(29*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_clawp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/arm/servo")
                end
            end),
            TimeEvent(33*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_clawp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/arm/servo")
                end
            end),

--------------------------------------------- ribs
            TimeEvent(4*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_spiderp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/ribs/servo")
                end
            end),
            TimeEvent(17*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_spiderp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/ribs/servo")
                end
            end),
            TimeEvent(21*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_spiderp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/ribs/taunt")
                end
            end),
            TimeEvent(45*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_spiderp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/ribs/servo")
                end
            end),

--------------------------------------------- leg
            TimeEvent(5*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_legp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/leg/servo")
                end
            end),
            TimeEvent(15*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_legp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/leg/servo")
                end
            end),
            TimeEvent(42*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_legp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/leg/servo")
                end
            end),
            --------------------------------------------- head
            TimeEvent(0*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_headp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/head/servo")
                end
            end),
            TimeEvent(2*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_headp" then
                    inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/head/taunt")
                end
            end),
            TimeEvent(12*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_headp" then
                    inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/head/step")
                    inst.SoundEmitter:SetParameter("steps", "intensity", .05)

                end
            end),
            TimeEvent(17*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_headp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/head/servo")
                end
            end),
            TimeEvent(19*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_headp" then
                    inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/head/taunt")
                end
            end),
            TimeEvent(24*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_headp" then
                    inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/head/step")
                    inst.SoundEmitter:SetParameter("steps", "intensity", .08)

                end
            end),
            TimeEvent(32*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_headp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/head/servo")
                end
            end),
--------------------------------------------------

            TimeEvent(23 * FRAMES, function(inst)
                if inst.prefab == "ancient_robot_legp" and TheNet:GetServerGameMode() == "lavaarena" then
					local lightning = SpawnPrefab("lightning")
					lightning.Transform:SetPosition(inst:GetPosition():Get())
					inst.components.health:DoDelta(40, false)
					inst.components.combat:DoAreaAttack(inst, 3,nil, nil, "electric", GetExcludeTags())
					inst.taunt = false
					inst:DoTaskInTime(PPHAM_FORGE.ROBOT_LEG.SPECIAL_CD, function(inst) inst.taunt = true end)
					--ms_lightningstrike here
                end
            end),
        },


    },
	
	State{
        name = "laserbeam",
        tags = { "busy","attack" },

        onenter = function(inst, target)
			local buffaction = inst:GetBufferedAction()
			local target = buffaction ~= nil and buffaction.target or nil
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("atk")
			inst.components.combat:StartAttack()
            if not inst.noeightfaced then
                inst.Transform:SetEightFaced()
            end
     --       EnableEightFaced(inst)
            if target ~= nil and target:IsValid() then
                if inst.components.combat:TargetIs(target) then
                    inst.components.combat:StartAttack()
                end
                inst:ForceFacePoint(target.Transform:GetWorldPosition())
                inst.sg.statemem.target = target
                inst.sg.statemem.targetpos = Vector3(target.Transform:GetWorldPosition())
            end
        end,

        onupdate = function(inst)
            if inst.sg.statemem.target ~= nil then
                if inst.sg.statemem.target:IsValid() then
                    local x, y, z = inst.Transform:GetWorldPosition()
                    local x1, y1, z1 = inst.sg.statemem.target.Transform:GetWorldPosition()
                    local dx, dz = x1 - x, z1 - z
                    if dx * dx + dz * dz < 256 and math.abs(anglediff(inst.Transform:GetRotation(), math.atan2(-dz, dx) / DEGREES)) < 45 then
                        inst:ForceFacePoint(x1, y1, z1)
                        return
                    end
                end
                inst.sg.statemem.target = nil
            end
            if inst.sg.statemem.lightval ~= nil then
                inst.sg.statemem.lightval = inst.sg.statemem.lightval * .99
                SetLightValueAndOverride(inst, inst.sg.statemem.lightval, (inst.sg.statemem.lightval - 1) * 3)
            end
        end,

        timeline =
        {
            --------------------------------------------- arm/claw
            TimeEvent(3*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_clawp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/arm/servo")
                end
            end),
            -- TimeEvent(4*FRAMES, function(inst) 
            --     if inst.prefab == "ancient_robot_clawp" then
            --        inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/ribs/servo")
            --     end
            -- end),

            TimeEvent(7*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_clawp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/laser_pre")
                end
            end),

            TimeEvent(19*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_clawp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/arm/servo")
                end
            end),

            TimeEvent(30*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_clawp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/laser","laserfilter")
                   inst.SoundEmitter:SetParameter("laserfilter", "intensity", .12)
                end
            end),
            TimeEvent(32*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_clawp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/laser","laserfilter")
                   inst.SoundEmitter:SetParameter("laserfilter", "intensity", .24)
                end
            end),
            TimeEvent(34*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_clawp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/laser","laserfilter")
                   inst.SoundEmitter:SetParameter("laserfilter", "intensity", .48)
                end
            end),
            TimeEvent(36*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_clawp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/laser","laserfilter")
                   inst.SoundEmitter:SetParameter("laserfilter", "intensity", .60)
                end
            end),
            TimeEvent(38*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_clawp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/laser","laserfilter")
                   inst.SoundEmitter:SetParameter("laserfilter", "intensity", .72)
                end
            end),
            TimeEvent(40*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_clawp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/laser","laserfilter")
                   inst.SoundEmitter:SetParameter("laserfilter", "intensity", .84)
                end
            end),
            TimeEvent(42*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_clawp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/laser","laserfilter")
                   inst.SoundEmitter:SetParameter("laserfilter", "intensity", .96)
                end
            end),
            TimeEvent(44*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_clawp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/laser","laserfilter")
                   inst.SoundEmitter:SetParameter("laserfilter", "intensity", 1)
                end
            end),
            TimeEvent(47*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_clawp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/arm/step")
                end
            end),
            --------------------------------------------- ribs
            TimeEvent(4*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_spiderp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/ribs/servo")
                end
            end),
            TimeEvent(4*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_spiderp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/ribs/servo")
                end
            end),

            TimeEvent(2*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_spiderp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/laser_pre")
                end
            end),

            TimeEvent(19*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_spiderp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/ribs/servo")
                end
            end),

            TimeEvent(22*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_spiderp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/laser","laserfilter")
                   inst.SoundEmitter:SetParameter("laserfilter", "intensity", .12)
                end
            end),
            TimeEvent(24*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_spiderp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/laser","laserfilter")
                   inst.SoundEmitter:SetParameter("laserfilter", "intensity", .24)
                end
            end),
            TimeEvent(26*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_spiderp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/laser","laserfilter")
                   inst.SoundEmitter:SetParameter("laserfilter", "intensity", .48)
                end
            end),
            TimeEvent(28*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_spiderp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/laser","laserfilter")
                   inst.SoundEmitter:SetParameter("laserfilter", "intensity", .60)
                end
            end),
            TimeEvent(30*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_spiderp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/laser","laserfilter")
                   inst.SoundEmitter:SetParameter("laserfilter", "intensity", .72)
                end
            end),
            TimeEvent(32*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_spiderp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/laser","laserfilter")
                   inst.SoundEmitter:SetParameter("laserfilter", "intensity", .84)
                end
            end),
            TimeEvent(34*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_spiderp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/laser","laserfilter")
                   inst.SoundEmitter:SetParameter("laserfilter", "intensity", .96)
                end
            end),
            TimeEvent(36*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_spiderp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/laser","laserfilter")
                   inst.SoundEmitter:SetParameter("laserfilter", "intensity", 1)
                end
            end),

            TimeEvent(6 * FRAMES, function(inst)
               -- TheCamera:Shake("VERTICAL",  .2,  .02, .5)
                SetLightValue(inst, .97)
            end),

            TimeEvent(8 * FRAMES, function(inst) inst.Light:Enable(true) 
                                                 SetLightValueAndOverride(inst, 0.05, .2) end),
            TimeEvent(9 * FRAMES, function(inst) SetLightValueAndOverride(inst, 0.1, .15) end),
            TimeEvent(10 * FRAMES, function(inst) SetLightValueAndOverride(inst, 0.15, .05) end),
            TimeEvent(11 * FRAMES, function(inst) SetLightValueAndOverride(inst, 0.20, 0) end),
            TimeEvent(12 * FRAMES, function(inst) SetLightValueAndOverride(inst, 0.25, .35) end),
            TimeEvent(13 * FRAMES, function(inst) SetLightValueAndOverride(inst, 0.30, .3) end),
            TimeEvent(14 * FRAMES, function(inst) SetLightValueAndOverride(inst, 0.35, .05) end),
            TimeEvent(15 * FRAMES, function(inst) SetLightValueAndOverride(inst, 0.40, 0) end),
            TimeEvent(16 * FRAMES, function(inst) SetLightValueAndOverride(inst, 0.45, .3) end),
            TimeEvent(17 * FRAMES, function(inst) SetLightValueAndOverride(inst, 0.50, .15) end),
            TimeEvent(18 * FRAMES, function(inst) SetLightValueAndOverride(inst, 0.55, .05) end),
            TimeEvent(19 * FRAMES, function(inst) SetLightValueAndOverride(inst, 0.60, 0) end),
            TimeEvent(20 * FRAMES, function(inst) SetLightValueAndOverride(inst, 0.65, .35) end),
            TimeEvent(21 * FRAMES, function(inst) SetLightValueAndOverride(inst, 0.70, .3) end),
            TimeEvent(22 * FRAMES, function(inst) SetLightValueAndOverride(inst, 0.75, .05) end),
            TimeEvent(23 * FRAMES, function(inst) SetLightValueAndOverride(inst, 0.80, 0) end),
            TimeEvent(24 * FRAMES, function(inst) SetLightValueAndOverride(inst, 0.85, .3) end),
            TimeEvent(25 * FRAMES, function(inst) SetLightValueAndOverride(inst, 0.90, .15) end),
            TimeEvent(26 * FRAMES, function(inst) SetLightValueAndOverride(inst, 0.95, .05) end),
            TimeEvent(27 * FRAMES, function(inst) SetLightValueAndOverride(inst, 1, 0) end),
            TimeEvent(28 * FRAMES, function(inst) SetLightValueAndOverride(inst, 1.01, .35) end),

            TimeEvent(29 * FRAMES, function(inst)
                SetLightValueAndOverride(inst, .9, 0)                
            end),
            TimeEvent(30 * FRAMES, function(inst)
                SpawnLaser(inst)
                inst.sg.statemem.target = nil
                SetLightValueAndOverride(inst, 1.08, .7)
            end),
            TimeEvent(31 * FRAMES, function(inst) SetLightValueAndOverride(inst, 1.12, 1) end),
            TimeEvent(32 * FRAMES, function(inst) SetLightValueAndOverride(inst, 1.1, .9) end),
            TimeEvent(33 * FRAMES, function(inst) SetLightValueAndOverride(inst, 1.06, .4) end),
            TimeEvent(34 * FRAMES, function(inst) SetLightValueAndOverride(inst, 1.1, .6) end),
            TimeEvent(35 * FRAMES, function(inst) inst.sg.statemem.lightval = 1.1 end),
            TimeEvent(36 * FRAMES, function(inst) 
                inst.sg.statemem.lightval = 1.035
                SetLightColour(inst, .9)
            end),

            TimeEvent(37 * FRAMES, function(inst)
                inst.sg.statemem.lightval = nil
                SetLightValueAndOverride(inst, .9, 0)
                SetLightColour(inst, .9)
            end),
            TimeEvent(38 * FRAMES, function(inst)
                inst.sg:RemoveStateTag("busy")
                SetLightValue(inst, 1)
                SetLightColour(inst, 1)
                inst.Light:Enable(false)
            end),
        },

        events =
        {
            EventHandler("animover", function(inst)
                inst.sg.statemem.keepfacing = true
                inst.sg:GoToState("idle")
            end),
        },

        onexit = function(inst)
            if inst.issixedfaced then 
                inst.Transform:SetSixFaced()
            else
                inst.Transform:SetFourFaced()
            end
            SetLightValueAndOverride(inst, 1, 0)
            SetLightColour(inst, 1)
            if not inst.sg.statemem.keepfacing then
      --          DisableEightFaced(inst)
            end
        end,
    },
	
	State{
            
        name = "leap_attack_pre",
        tags = {"attack", "canrotate", "busy","leapattack"},
        
        onenter = function(inst, target)
            inst.components.locomotor:Stop()                    
            inst.AnimState:PlayAnimation("atk_pre")
        end,

    timeline=
        {
--------------------------------------------- leg
            TimeEvent(0*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_legp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/leg/servo")
                end
            end),
--------------------------------------------- head
            TimeEvent(7*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_headp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/head/servo")
                end
            end),
            TimeEvent(9*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_headp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/head/servo")
                end
            end),
            TimeEvent(11*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_headp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/head/servo")
                end
            end),
        },
            
        events=
        {
            EventHandler("animover", function(inst) 
				local buffaction = inst:GetBufferedAction()
				local target = buffaction ~= nil and buffaction.target or nil
				if target then
					inst:ForceFacePoint(target:GetPosition():Get())
				end
				inst.sg:GoToState("leap_attack", target) 
			end),
        },
    },

    State{

        name = "leap_attack",
        tags = {"attack", "canrotate", "busy", "leapattack"},
        
        onenter = function(inst, data)
			local buffaction = inst:GetBufferedAction()
			local target = buffaction ~= nil and buffaction.target or nil
			if target then
				if target:IsValid() then
					inst:FacePoint(target:GetPosition())
					inst.sg.statemem.attacktarget = target
					inst.sg.statemem.startingpos = inst:GetPosition()
					inst.sg.statemem.targetpos = target:GetPosition()
				end
			end
            inst.components.locomotor:Stop()	
			SetJumpPhysics(inst)
            inst.components.locomotor:EnableGroundSpeedMultiplier(false)
            inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/bearger/swhoosh")

            inst.components.combat:StartAttack()
            inst.AnimState:PlayAnimation("atk_loop")            
			
			if inst.sg.statemem.attacktarget and inst.sg.statemem.attacktarget:IsValid()  then
				inst.sg.statemem.speed = CalcJumpSpeed(inst, inst.sg.statemem.attacktarget)		
				inst.sg.statemem.jump = true
			end
        end,

		onupdate = function(inst)
			if inst.sg.statemem.jump then
				inst.Physics:SetMotorVel(inst.sg.statemem.speed and inst.sg.statemem.speed or 5, 0, 0)
			end
		end,

        timeline=
        {
--------------------------------------------- leg
            TimeEvent(18*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_legp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/leg/servo")
                end
            end),
--------------------------------------------- arm/claw
--------------------------------------------- ribs
--------------------------------------------- leg
--------------------------------------------- head
            TimeEvent(0*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_headp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/head/servo")
                end
            end),
             TimeEvent(3*FRAMES, function(inst) 
                 if inst.prefab == "ancient_robot_headp" then
                    inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/head/attack")
                 end
             end),
             TimeEvent(25*FRAMES, function(inst) PowerGlow(inst) end), 
        },

        onexit = function(inst)
            SetNormalPhysics(inst)
            --inst.Physics:ClearMotorVelOverride()
            inst.components.locomotor:Stop()
            inst.components.locomotor:EnableGroundSpeedMultiplier(true)
            inst.sg.statemem.startpos = nil
            inst.sg.statemem.targetpos = nil
			inst.components.locomotor:Stop()
			inst.sg.statemem.jump = nil
        end,
        
        
        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("leap_attack_pst") end),
        },
    },


    State{

        name = "leap_attack_pst",
        tags = {"busy"},
        
        onenter = function(inst, target)            
            --inst.components.groundpounder:GroundPound()

            ShakeIfClose(inst)

            local ring = SpawnPrefab("laser_ringp")
            ring.Transform:SetPosition(inst.Transform:GetWorldPosition())

            inst.components.locomotor:Stop()
            inst.AnimState:PlayAnimation("atk_pst")
			inst.components.combat:StartAttack()
        end,

        timeline =
        {
            TimeEvent(5*FRAMES, function(inst)DoDamage(inst, 2.5) end), 
            TimeEvent(10*FRAMES, function(inst)DoDamage(inst, 3.5) end), 
            TimeEvent(15*FRAMES, function(inst)DoDamage(inst, 4.5) end),
--------------------------------------------- leg
            TimeEvent(2*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_legp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/smash")
                end
            end),
            TimeEvent(12*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_legp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/leg/servo")
                end
            end),
            TimeEvent(18*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_legp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/leg/servo")
                end
            end),
            TimeEvent(28*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_legp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/leg/step",nil,.06)
                end
            end),
            TimeEvent(30*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_legp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/leg/servo")
                end
            end),
--------------------------------------------- head
            TimeEvent(0*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_headp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/smash")
                end
            end),
            TimeEvent(13*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_headp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/head/servo")
                end
            end),
            TimeEvent(17*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_headp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/head/step")
                end
            end),
            TimeEvent(31*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_headp" then
                    inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/head/step")
                    inst.SoundEmitter:SetParameter("steps", "intensity", .08)

                end
            end),
        },

        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State{
        name = "attack",
        tags = {"attack", "busy"},
        
        onenter = function(inst, target)
            if inst.components.locomotor then
                inst.components.locomotor:StopMoving()
            end
            inst.components.combat:StartAttack()
            inst.AnimState:PlayAnimation("atk")
            inst.sg.statemem.target = target
        end,
        
        timeline = 
        {
            TimeEvent(4*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/hippo/attack") end),
			TimeEvent(17*FRAMES, function(inst)
                inst:PerformBufferedAction()
            end),

        },        

        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },

    State{
            
        name = "leap_attack_pre2",
        tags = {"attack", "canrotate", "busy","leapattack"},
        
        onenter = function(inst, target)
			local buffaction = inst:GetBufferedAction()
			local target = buffaction ~= nil and buffaction.target or nil
            inst.components.locomotor:Stop()                    
            inst.AnimState:PlayAnimation("atk_pre")
            inst.sg.statemem.startpos = Vector3(inst.Transform:GetWorldPosition())
            inst.sg.statemem.targetpos = Vector3(target.Transform:GetWorldPosition())
        end,
            
		 timeline=
        {
--------------------------------------------- leg
            TimeEvent(0*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_legp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/leg/servo")
                end
            end),
--------------------------------------------- head
            TimeEvent(7*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_headp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/head/servo")
                end
            end),
            TimeEvent(9*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_headp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/head/servo")
                end
            end),
            TimeEvent(11*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_headp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/head/servo")
                end
            end),
        },	
			
        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("leap_attack",{startpos =inst.sg.statemem.startpos, targetpos =inst.sg.statemem.targetpos}) end),
        },
    },


    State{

        name = "leap_attack2",
        tags = {"attack", "canrotate", "busy", "leapattack"},
        
        onenter = function(inst, data)
            inst.sg.statemem.startpos = data.startpos
            inst.sg.statemem.targetpos = data.targetpos
            inst.components.locomotor:Stop()
            inst.components.locomotor:EnableGroundSpeedMultiplier(false)
            inst.components.combat:StartAttack()
            inst.AnimState:PlayAnimation("atk_loop")     
			inst:DoTaskInTime(0, function(inst) inst.Physics:SetMotorVelOverride(25,0,0) end)
			--inst.Physics:SetActive(false)
        end,

        onupdate = function(inst)
            
        end,

        onexit = function(inst)
            inst.Physics:SetActive(true)
            --inst.Physics:ClearMotorVelOverride()
            inst.components.locomotor:Stop()
            inst.components.locomotor:EnableGroundSpeedMultiplier(true)
            inst.sg.statemem.startpos = nil
            inst.sg.statemem.targetpos = nil
        end,
        
       timeline = 
        {
            TimeEvent(4*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/hippo/leap_attack") end ),
            ---TimeEvent(20*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/hippo/huff_out") end ),
        },
        
        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("leap_attack_pst") end),
        },
    },


    State{

        name = "leap_attack_pst2",
        tags = {"busy"},
        
        onenter = function(inst, target)
            if SW_PP_MODE == true then
				inst.SoundEmitter:PlaySound("ia/creatures/seacreature_movement/splash_large") 
				SpawnWaves(inst, 12, 360, 4)
				if not inst:GetIsOnWater(Vector3(inst.Transform:GetWorldPosition()) ) then
					inst.components.combat:DoAreaAttack(inst, 4, nil, nil, "explosive", GetExcludeTags())
					inst.components.groundpounder:GroundPound()
					ShakeIfClose(inst)
					inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/bearger/groundpound",nil,.5)
				else
					inst.components.combat:DoAreaAttack(inst, 4, nil, nil, "explosive", GetExcludeTags())
				end 
			else
				inst.components.combat:DoAreaAttack(inst, 4, nil, nil, "explosive", GetExcludeTags())
				ShakeIfClose(inst)
				inst.components.groundpounder:GroundPound()
				inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/bearger/groundpound",nil,.5)
            end

            

            inst.components.locomotor:Stop()
            inst.AnimState:PlayAnimation("jump_atk_pst")
        end,

        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("taunt") end),
        },
    },
	
	State{
        name = "opengift",
        tags = { "busy", "pausepredict" },

        onenter = function(inst)
            inst.components.locomotor:Stop()
            inst.components.locomotor:Clear()
            inst:ClearBufferedAction()

            --if IsNearDanger(inst) then
                --inst.sg.statemem.isdanger = true
                --inst.sg:GoToState("idle")
                --if inst.components.talker ~= nil then
                   -- inst.components.talker:Say(GetString(inst, "ANNOUNCE_NODANGERGIFT"))
                --end
                --return
           -- end

            inst.SoundEmitter:PlaySound("dontstarve/common/player_receives_gift")
            inst.AnimState:PlayAnimation("taunt", true)
            -- NOTE: the previously used ripping paper anim is called "giift_loop"

            if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:RemotePausePrediction()
                inst.components.playercontroller:EnableMapControls(false)
                inst.components.playercontroller:Enable(false)
            end
            inst.components.inventory:Hide()
            inst:PushEvent("ms_closepopups")
            inst:ShowActions(false)
            inst:ShowPopUp(POPUPS.GIFTITEM, true)

            if inst.components.giftreceiver ~= nil then
                inst.components.giftreceiver:OnStartOpenGift()
            end
        end,

        timeline =
        {
            -- Timing of the gift box opening animation on giftitempopup.lua
            TimeEvent(155 * FRAMES, function(inst)
               -- inst.AnimState:PlayAnimation("gift_open_pre")
                inst.AnimState:PushAnimation("taunt", true)
            end),
        },

        events =
        {
            EventHandler("firedamage", function(inst)
                inst.AnimState:PlayAnimation("taunt")
                inst.sg:GoToState("idle", true)
                if inst.components.talker ~= nil then
                    inst.components.talker:Say(GetString(inst, "ANNOUNCE_NODANGERGIFT"))
                end
            end),
            EventHandler("ms_doneopengift", function(inst, data)
				inst.sg:GoToState("idle", true)
                
            end),
        },

        onexit = function(inst)
            if inst.sg.statemem.isdanger then
                return
            elseif not inst.sg.statemem.isopeningwardrobe then
                if inst.components.playercontroller ~= nil then
                    inst.components.playercontroller:EnableMapControls(true)
                    inst.components.playercontroller:Enable(true)
                end
                inst.components.inventory:Show()
                inst:ShowActions(true)
            end
            inst:ShowPopUp(POPUPS.GIFTITEM, false)
        end,
    },
	
	State{
		name = "action",
        tags = {"busy"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
			inst:PerformBufferedAction()
            inst.AnimState:PlayAnimation("walk_pst")
        end,

		timeline=
        {

        },

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State
    {
        name = "action_loop",
        tags = { "doing", "nodangle", "nointerrupt" },

        onenter = function(inst, timeout)
			if TheNet:GetServerGameMode() == "lavaarena" and inst.prefab ~= "ancient_robot_clawp" then
				inst.sg:GoToState("idle")
			end
            inst.sg:SetTimeout(timeout or 1)
            inst.components.locomotor:Stop()
            inst.AnimState:PlayAnimation("taunt", true)
            if inst.bufferedaction ~= nil then
                inst.sg.statemem.action = inst.bufferedaction
                if inst.bufferedaction.action.actionmeter then
                    inst.sg.statemem.actionmeter = true
                    StartActionMeter(inst, timeout or 1)
                end
                if inst.bufferedaction.target ~= nil and inst.bufferedaction.target:IsValid() then
                    inst.bufferedaction.target:PushEvent("startlongaction")
                end
            end
        end,

        timeline =
        {
            TimeEvent(4 * FRAMES, function(inst)
                inst.sg:RemoveStateTag("busy")
            end),
        },

        ontimeout = function(inst)
            inst.AnimState:PlayAnimation("walk_pst")
            if inst.sg.statemem.actionmeter then
                inst.sg.statemem.actionmeter = nil
                StopActionMeter(inst, true)
            end
            inst:PerformBufferedAction()
        end,

        events =
        {
            EventHandler("animqueueover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("idle")
                end
            end),
        },

        onexit = function(inst)
            if inst.sg.statemem.actionmeter then
                StopActionMeter(inst, false)
            end
            if inst.bufferedaction == inst.sg.statemem.action then
                inst:ClearBufferedAction()
            end
        end,
    },
	
    State{
        name = "death",
        tags = {"busy"},

        onenter = function(inst)
			--inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/hippo/death")
            inst.AnimState:PlayAnimation("death")
            inst.Physics:Stop()
            RemovePhysicsColliders(inst)     
			inst.components.lootdropper:DropLoot(Vector3(inst.Transform:GetWorldPosition()))
			inst.components.inventory:DropEverything(true)
        end,
		
		timeline=
        {
			TimeEvent(0*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_clawp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/arm/green")
                end
            end),
            TimeEvent(9*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_clawp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/arm/green")
                end
            end),
            TimeEvent(14*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_clawp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/arm/green")
                end
            end),
            TimeEvent(21*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_clawp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/arm/green")
                end
            end),
            TimeEvent(38*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_clawp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/arm/green")
                end
            end),
            TimeEvent(36*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_clawp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/arm/servo")
                end
            end),
--------------------------------------------- head
            TimeEvent(5*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_headp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/head/green")
                end
            end),
            TimeEvent(13*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_headp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/head/green")
                end
            end),
            TimeEvent(19*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_headp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/head/green")
                end
            end),
            TimeEvent(23*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_headp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/head/green")
                end
            end),
            TimeEvent(38*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_headp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/head/green")
                end
            end),
--------------------------------------------- leg
            TimeEvent(3*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_legp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/leg/green")
                end
            end),
            TimeEvent(12*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_legp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/leg/green")
                end
            end),
            TimeEvent(16*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_legp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/leg/green")
                end
            end),
            TimeEvent(23*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_legp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/leg/green")
                end
            end),
            TimeEvent(31*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_legp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/leg/green")
                end
            end),
            TimeEvent(43*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_legp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/leg/step")
                end
            end),
    --------------------------------------------- ribs
            TimeEvent(0*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_spiderp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/ribs/green")
                end
            end),
            TimeEvent(9*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_spiderp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/ribs/green")
                end
            end),
            TimeEvent(14*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_spiderp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/ribs/green")
                end
            end),
            TimeEvent(21*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_spiderp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/ribs/green")
                end
            end),
        },

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
					PlayablePets.DoDeath(inst)
                end
            end),
        },

    },
	
	State {
        name = "sleep",
        tags = { "sleeping", "busy" }, --add tag "busy" if you hate sliding

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
			inst.SoundEmitter:KillSound("gears")
            inst.AnimState:PlayAnimation("deactivate")
            if inst.prefab == "ancient_robot_clawp" then 
                inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/arm/stop")
                end
            if inst.prefab == "ancient_robot_headp" then 
                inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/head/stop")
                end
            if inst.prefab == "ancient_robot_legp" then 
                inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/leg/stop")
                end
            if inst.prefab == "ancient_robot_spiderp" then 
                inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/ribs/stop")
            end
			
			inst.sleepstarttime = GetTime()
            inst:RemoveTag("monster")
        end,

        timeline=
        {
--------------------------------------------- arm/claw
            TimeEvent(0*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_clawp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/arm/green")
                end
            end),
            TimeEvent(9*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_clawp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/arm/green")
                end
            end),
            TimeEvent(14*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_clawp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/arm/green")
                end
            end),
            TimeEvent(21*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_clawp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/arm/green")
                end
            end),
            TimeEvent(38*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_clawp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/arm/green")
                end
            end),
            TimeEvent(36*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_clawp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/arm/servo")
                end
            end),
--------------------------------------------- head
            TimeEvent(5*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_headp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/head/green")
                end
            end),
            TimeEvent(13*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_headp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/head/green")
                end
            end),
            TimeEvent(19*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_headp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/head/green")
                end
            end),
            TimeEvent(23*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_headp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/head/green")
                end
            end),
            TimeEvent(38*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_headp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/head/green")
                end
            end),
--------------------------------------------- leg
            TimeEvent(3*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_legp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/leg/green")
                end
            end),
            TimeEvent(12*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_legp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/leg/green")
                end
            end),
            TimeEvent(16*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_legp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/leg/green")
                end
            end),
            TimeEvent(23*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_legp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/leg/green")
                end
            end),
            TimeEvent(31*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_legp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/leg/green")
                end
            end),
            TimeEvent(43*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_legp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/leg/step")
                end
            end),
    --------------------------------------------- ribs
            TimeEvent(0*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_spiderp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/ribs/green")
                end
            end),
            TimeEvent(9*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_spiderp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/ribs/green")
                end
            end),
            TimeEvent(14*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_spiderp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/ribs/green")
                end
            end),
            TimeEvent(21*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_spiderp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/ribs/green")
                end
            end),
        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("sleeping") end ),
            EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "sleeping",
        tags = { "sleeping", "busy" },

        --onenter = onentersleeping,
		
		onenter = function(inst)
				inst.components.locomotor:StopMoving()
				--if inst.components.sanity.current >= 50 then
				PlayablePets.SleepHeal(inst)
				
				--end
				--inst.components.sanity:DoDelta(1, false)
				--inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/bearger/sleep")
				if inst.sleepstarttime and (GetTime() - inst.sleepstarttime) > 120 then
					if not inst.ismossy then
						inst.ismossy = true
						inst.AnimState:PlayAnimation("mossy_full")
					end	
				else
					inst.AnimState:PlayAnimation("full")
				end	
				
				inst.sg:SetTimeout(2)
			end,
			
		ontimeout = function(inst)
			inst.sg:GoToState("sleeping")
		--inst.components.sanity.dapperness = -0.5
		end,
        --timeline = timelines ~= nil and timelines.sleeptimeline or nil,
		timeline=
        {

        },

        events =
        {
            --EventHandler("animover", function(inst) inst.sg:GoToState("sleeping") end ),
			EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "wake",
        tags = { "busy", "waking" },

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end

            if inst.components.sleeper ~= nil and inst.components.sleeper:IsAsleep() then
                inst.components.sleeper:WakeUp()
            end
            
			inst.sleepstarttime = nil
			RemoveMoss(inst)
            
            inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation("activate")
            inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/gears_LP","gears")
            inst.SoundEmitter:SetParameter("gears", "intensity", .5)
            inst:AddTag("monster")
        end,

        timeline=
        {
--------------------------------------------- arm/claw
            TimeEvent(3*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_clawp" then
                    inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/arm/active")
                    inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/arm/start")
                end
            end),
            TimeEvent(4*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_clawp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/arm/active")
                end
            end),
            TimeEvent(6*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_clawp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/arm/active") 
                end
            end),
            TimeEvent(8*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_clawp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/arm/active") 
                end
            end),
            TimeEvent(9*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_clawp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/arm/active") 
                end
            end),
            TimeEvent(12*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_clawp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/arm/active") 
                end
            end),

            TimeEvent(16*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_clawp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/arm/active") 
                end
            end),
            TimeEvent(30*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_clawp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/electro")
                end
            end),
            TimeEvent(37*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_clawp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/arm/servo") 
                end
            end),
            TimeEvent(30*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_clawp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/electro")
                end
            end),
            TimeEvent(41*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_clawp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/electro")
                end
            end),
            TimeEvent(50*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_clawp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/electro")
                end
            end),
            TimeEvent(51*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_clawp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/arm/servo") 
                end
            end),
            TimeEvent(53*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_clawp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/electro")
                end
            end),
            TimeEvent(62*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_clawp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/arm/servo") 
                end
            end),
            TimeEvent(70*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_clawp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/arm/servo") 
                end
            end),
--------------------------------------------- head
            TimeEvent(0*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_headp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/head/active")
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/head/start")
                end
            end),

            TimeEvent(1*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_headp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/head/active")
                end
            end),
            TimeEvent(3*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_headp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/head/active")
                end
            end),
            TimeEvent(5*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_headp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/head/active")
                end
            end),
            TimeEvent(6*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_headp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/head/active")
                end
            end),
            TimeEvent(9*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_headp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/head/active")
                end
            end),
            TimeEvent(12*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_headp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/head/active")
                end
            end),
            TimeEvent(14*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_headp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/electro")
                end
            end),
            TimeEvent(16*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_headp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/head/active")
                end
            end),
            TimeEvent(17*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_headp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/electro")
                end
            end),
            TimeEvent(27*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_headp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/electro")
                end
            end),
            TimeEvent(30*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_headp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/electro")
                end
            end),
            TimeEvent(37*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_headp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/electro")
                end
            end),
            TimeEvent(40*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_headp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/electro")
                end
            end),
            TimeEvent(54*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_headp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/electro")
                end
            end),
            TimeEvent(57*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_headp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/electro")
                end
            end),
            
            --------------------------------------------- leg
            TimeEvent(0*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_legp" then
                    inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/leg/active")
                    inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/leg/start")
                end
            end),
            
            TimeEvent(4*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_legp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/leg/active")
                end
            end),
            TimeEvent(5*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_legp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/leg/active")
                end
            end),
            TimeEvent(9*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_legp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/leg/active")
                end
            end),
            TimeEvent(13*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_legp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/leg/active")
                end
            end),
             TimeEvent(14*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_legp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/electro")
                end
            end),
            TimeEvent(17*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_legp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/electro")
                end
            end),
            TimeEvent(27*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_legp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/leg/servo")
                end
            end),
            TimeEvent(44*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_legp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/leg/servo")
                end
            end),
            TimeEvent(58*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_legp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/leg/step",nil,.06)
                end
            end),
            --------------------------------------------- ribs
            TimeEvent(2*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_spiderp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/ribs/active")
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/ribs/start")
                end
            end),
            
            TimeEvent(4*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_spiderp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/ribs/active")
                end
            end),
            TimeEvent(6*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_spiderp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/ribs/active")
                end
            end),
            TimeEvent(8*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_spiderp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/ribs/active")
                end
            end),
            TimeEvent(9*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_spiderp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/electro")
                end
            end),
            TimeEvent(10*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_spiderp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/ribs/active")
                end
            end),
            TimeEvent(12*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_spiderp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/ribs/active")
                end
            end),
            TimeEvent(13*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_spiderp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/electro")
                end
            end),
            TimeEvent(14*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_spiderp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/ribs/active")
                end
            end),
            TimeEvent(16*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_spiderp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/ribs/active")
                end
            end),
            TimeEvent(18*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_spiderp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/electro")
                end
            end),
            TimeEvent(21*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_spiderp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/electro")
                end
            end),
            TimeEvent(30*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_spiderp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/electro")
                end
            end),
            TimeEvent(33*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_spiderp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/electro")
                end
            end),
            TimeEvent(36*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_spiderp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/electro")
                end
            end),
            TimeEvent(39*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_spiderp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/electro")
                end
            end),
 
        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State
    {
        name = "run_start",
        tags = { "moving", "running", "canrotate" },

        onenter = function(inst)
			inst.components.locomotor:WalkForward()
            inst.AnimState:PlayAnimation("walk_pre")
			
        end,
		
		timeline = 
		{
			TimeEvent(0*FRAMES, function(inst)                                          
                    if inst.prefab == "ancient_robot_headp" then
                       inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/head/servo")
                        inst.SoundEmitter:SetParameter("servo", "intensity", math.random())
                    end
                end),
--------------------------------------------- arm/claw
--------------------------------------------- ribs
                TimeEvent(1*FRAMES, function(inst)                                          
                    if inst.prefab == "ancient_robot_spiderp" then
                        inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/ribs/step")
                        inst.SoundEmitter:SetParameter("steps", "intensity", math.random())
                    end
                end),

            TimeEvent(0*FRAMES, function(inst) inst.Physics:Stop() end ),
		},
		
		onexit = function(inst)
			inst.SoundEmitter:KillSound("robo_walk_LP")
		end,
		
        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("run") end ),
        },
    },
	
	State
    {
        name = "run",
        tags = { "moving", "running", "canrotate" },

        onenter = function(inst)
			--inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/run")
			--inst.components.locomotor:WalkForward()
			inst.AnimState:PlayAnimation("walk_loop")
        end,
		
		timeline = {

			TimeEvent(0*FRAMES, function(inst) 
                    --inst.Physics:Stop() 
                    ----print("CHECKL 1")
                    inst.components.locomotor:WalkForward()

                    if inst.prefab == "ancient_robot_clawp" then
                        inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/arm/servo")
                        inst.SoundEmitter:SetParameter("servo", "intensity", math.random())
                        inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/arm/drag")
                        inst.SoundEmitter:SetParameter("servo", "intensity", math.random())                        
                    end
                end ),

                TimeEvent(1*FRAMES, function(inst)                                          
                    if inst.prefab == "ancient_robot_headp" then
                        -- inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/head/step")
                        -- inst.SoundEmitter:SetParameter("steps", "intensity", math.random())
                        inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/head/servo")
                        inst.SoundEmitter:SetParameter("servo", "intensity", math.random())
                    end
                end),                

                TimeEvent(5*FRAMES, function(inst)    
                    if inst.prefab == "ancient_robot_legp" then
                       inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/leg/servo")
                    end
                end),

                TimeEvent(6*FRAMES, function(inst)   
                    if inst.prefab == "ancient_robot_clawp" then
                        inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/arm/step")
                        inst.SoundEmitter:SetParameter("steps", "intensity", math.random())
                    end                                                       
                    if inst.prefab == "ancient_robot_spiderp" then
                        inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/ribs/step")
                        inst.SoundEmitter:SetParameter("steps", "intensity", math.random())
                        inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/ribs/servo")
                        inst.SoundEmitter:SetParameter("servo", "intensity", math.random())
                    end
                end),

                TimeEvent(14*FRAMES, function(inst)       
                    if inst.prefab == "ancient_robot_clawp" then
                        inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/arm/servo")
                        inst.SoundEmitter:SetParameter("servo", "intensity", math.random())
                    end                    
                    if inst.prefab == "ancient_robot_legp" then
                       inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/leg/step") 
                       inst.SoundEmitter:SetParameter("steps", "intensity", math.random())
                    end
     
                end),

                TimeEvent(16*FRAMES, function(inst)                                         
                    if inst.prefab == "ancient_robot_spiderp" then
                        inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/ribs/step_wires")
                        inst.SoundEmitter:SetParameter("steps", "intensity", math.random())
                        inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/ribs/servo")
                        inst.SoundEmitter:SetParameter("servo", "intensity", math.random())
                    end
                end),

                TimeEvent(17*FRAMES, function(inst)                                          
                    if inst.prefab == "ancient_robot_headp" then
                        inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/head/step")
                        inst.SoundEmitter:SetParameter("steps", "intensity", math.random())
                    end
                end),

                TimeEvent(21*FRAMES, function(inst)        
                    if inst.prefab == "ancient_robot_spiderp" then
                        inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/ribs/step")
                        inst.SoundEmitter:SetParameter("steps", "intensity", math.random())
                        inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/ribs/servo")
                        inst.SoundEmitter:SetParameter("servo", "intensity", math.random())
                    end
                end),

                TimeEvent(25*FRAMES, function(inst)                                          
                    if inst.prefab == "ancient_robot_clawp" then
                        inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/arm/step")
                        inst.SoundEmitter:SetParameter("steps", "intensity", math.random())
                    end
                    if inst.prefab == "ancient_robot_spiderp" then
                    inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/ribs/step")
                    inst.SoundEmitter:SetParameter("steps", "intensity", math.random())
                    inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/ribs/servo")
                    inst.SoundEmitter:SetParameter("servo", "intensity", math.random())
                    end                    
                end),

                TimeEvent(28*FRAMES, function(inst)                                          
                    if inst.prefab == "ancient_robot_clawp" then
                       inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/arm/servo")
                        inst.SoundEmitter:SetParameter("servo", "intensity", math.random())
                    end
                end),

                TimeEvent(38*FRAMES, function(inst)       
                    if inst.prefab == "ancient_robot_spiderp" then
                        inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/ribs/step")
                        inst.SoundEmitter:SetParameter("steps", "intensity", math.random())
                        inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/ribs/servo")
                        inst.SoundEmitter:SetParameter("servo", "intensity", math.random())
                    end
                end),


                
                TimeEvent(48*FRAMES, function(inst) 
                    --inst.Physics:Stop() 
                end ),
		},
		
		onexit = function(inst)
			inst.SoundEmitter:KillSound("robo_walk_LP")
		end,
		
        events=
			{
				EventHandler("animqueueover", function(inst) 
					inst.sg:GoToState("run")  
				end ),
			},

    },
	
	State
    {
        name = "run_stop",
        tags = { "idle" },

        onenter = function(inst) 
            inst.components.locomotor:StopMoving()
			inst.AnimState:PlayAnimation("walk_pst")
			--inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/step")            
        end,

		timeline= 
        {
                TimeEvent(3*FRAMES, function(inst)       
                    if inst.prefab == "ancient_robot_spiderp" then
                    inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/ribs/step")
                    inst.SoundEmitter:SetParameter("steps", "intensity", math.random())
                    inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/ribs/servo")
                    inst.SoundEmitter:SetParameter("servo", "intensity", math.random())
                    end
                end),
--------------------------------------------- arm/claw                
                TimeEvent(33*FRAMES, function(inst) 
					if inst.prefab == "ancient_robot_clawp" then
						inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/arm/servo")
					end
				end),

                TimeEvent(48*FRAMES, function(inst) 
                    inst.Physics:Stop() 
                end ),

        },
		
		onexit = function(inst)
			inst.SoundEmitter:KillSound("robo_walk_LP")
		end,
		
        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end ),
        },
    },
	
	--=========================
	-----wormhole states----
	--=========================
	
	State{
		name = "jumpin_pre",
        tags = {"busy"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
			inst:PerformBufferedAction()
            inst.AnimState:PlayAnimation("walk_pst")
			----print("DEBUG: JUMPIN PRE ran!")
        end,

		timeline=
        {
			
        },

        events=
        {
			EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    if inst.bufferedaction ~= nil then
                        inst:PerformBufferedAction()
						----print("DEBUG: Buffered Action did not return nil! "..inst.bufferedaction)
                    else
						----print("DEBUG: Buffered Action did return nil!")
                        inst.sg:GoToState("idle")
                    end
                end
            end),
        },
    },
	
	State{
		name = "jumpin",
        tags = {"doing", "busy"},

        onenter = function(inst, data)
            inst.Physics:Stop()
			----print("DEBUG: Jumpin State ran")
			inst:PerformBufferedAction()
            inst.AnimState:PlayAnimation("walk_pst")
			
			inst.components.locomotor:Stop()

            inst.sg.statemem.target = data.teleporter
            inst.sg.statemem.heavy = inst.components.inventory:IsHeavyLifting()

            if data.teleporter ~= nil and data.teleporter.components.teleporter ~= nil then
				----print("DEBUG: DATA IS NOT NIL!")
                data.teleporter.components.teleporter:RegisterTeleportee(inst)
            end
			
            local pos = data ~= nil and data.teleporter and data.teleporter:GetPosition() or nil
			
			inst.sg.statemem.teleportarrivestate = "idle"
        end,

		timeline=
        {
			--TimeEvent(13*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/hound/bark") end),
			--TimeEvent(24*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/hound/bark") end),
        },

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    if inst.sg.statemem.target ~= nil and
                        inst.sg.statemem.target:IsValid() and
                        inst.sg.statemem.target.components.teleporter ~= nil then
                        --Unregister first before actually teleporting
                        inst.sg.statemem.target.components.teleporter:UnregisterTeleportee(inst)
                        if inst.sg.statemem.target.components.teleporter:Activate(inst) then
                            inst.sg.statemem.isteleporting = true
                            inst.components.health:SetInvincible(true)
                            if inst.components.playercontroller ~= nil then
                                inst.components.playercontroller:Enable(false)
                            end
                            inst:Hide()
                            inst.DynamicShadow:Enable(false)
                            return
                        end
                    end
                    inst.sg:GoToState("idle")
                end
            end),
        },

        onexit = function(inst)
            if inst.sg.statemem.isphysicstoggle then
                ToggleOnPhysics(inst)
            end

            if inst.sg.statemem.isteleporting then
                inst.components.health:SetInvincible(false)
                if inst.components.playercontroller ~= nil then
                    inst.components.playercontroller:Enable(true)
                end
                inst:Show()
                inst.DynamicShadow:Enable(true)
            elseif inst.sg.statemem.target ~= nil
                and inst.sg.statemem.target:IsValid()
                and inst.sg.statemem.target.components.teleporter ~= nil then
                inst.sg.statemem.target.components.teleporter:UnregisterTeleportee(inst)
            end
        end,
    },
	
	State{
		name = "jumpout", --unused but is functional
        tags = {"busy"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
			inst:PerformBufferedAction()
            inst.AnimState:PlayAnimation("walk_pst")
			----print("DEBUG: JUMPOUT ran!")
        end,

		timeline=
        {
			
        },

        events=
        {
			EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                        inst.sg:GoToState("idle")
                end
            end),
        },
    },
	--==============================
	-------------------Forge States------------------------	
	State
    {
        name = "corpse",
        tags = { "busy", "noattack", "nopredict", "nomorph", "nodangle" },

        onenter = function(inst, fromload)
            if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:Enable(false)
            end
			inst.SoundEmitter:KillSound("robo_walk_LP")
			inst.SoundEmitter:KillSound("gears")
            inst:PushEvent("playerdied", { loading = fromload, skeleton = false })

            inst:ShowActions(false)
            inst.components.health:SetInvincible(true)
			
			

            if inst.components.locomotor then
				inst.components.locomotor:StopMoving()
			end
			--inst:RemoveTag("corpse") -- we don't want to be revived. We auto revive.
			inst.AnimState:PlayAnimation("deactivate")
			--inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/thunderbird/death")
			--if inst.deathcount then
				--inst.sg:SetTimeout((inst.deathcount and inst.deathcount >= 1) and 30 or (15 * (1 + inst.deathcount)))
			--else
				inst.sg:SetTimeout(15)
			--end	
			
			--inst.deathcount = (iinst.deathcount and (inst.deathcount + 0.1) or 0.1
        end,
		
		timeline=
		{
			TimeEvent(0*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_clawp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/arm/green")
                end
            end),
            TimeEvent(9*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_clawp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/arm/green")
                end
            end),
            TimeEvent(14*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_clawp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/arm/green")
                end
            end),
            TimeEvent(21*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_clawp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/arm/green")
                end
            end),
            TimeEvent(38*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_clawp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/arm/green")
                end
            end),
            TimeEvent(36*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_clawp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/arm/servo")
                end
            end),
--------------------------------------------- head
            TimeEvent(5*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_headp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/head/green")
                end
            end),
            TimeEvent(13*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_headp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/head/green")
                end
            end),
            TimeEvent(19*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_headp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/head/green")
                end
            end),
            TimeEvent(23*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_headp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/head/green")
                end
            end),
            TimeEvent(38*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_headp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/head/green")
                end
            end),
--------------------------------------------- leg
            TimeEvent(3*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_legp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/leg/green")
                end
            end),
            TimeEvent(12*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_legp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/leg/green")
                end
            end),
            TimeEvent(16*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_legp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/leg/green")
                end
            end),
            TimeEvent(23*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_legp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/leg/green")
                end
            end),
            TimeEvent(31*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_legp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/leg/green")
                end
            end),
            TimeEvent(43*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_legp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/leg/step")
                end
            end),
    --------------------------------------------- ribs
            TimeEvent(0*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_spiderp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/ribs/green")
                end
            end),
            TimeEvent(9*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_spiderp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/ribs/green")
                end
            end),
            TimeEvent(14*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_spiderp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/ribs/green")
                end
            end),
            TimeEvent(21*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_spiderp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/ribs/green")
                end
            end),
		},
		
		ontimeout = function(inst)
			if not TheWorld.components.lavaarenaevent.lost then
				inst.components.health:SetPercent(0.5)
				inst:PushEvent("respawnfromcorpse", { source = nil, user = nil })
				inst.sg:GoToState("corpse_rebirth")
			end	
		end,

        onexit = function(inst)
            if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:Enable(true)
            end
            inst:ShowActions(true)
            inst.components.health:SetInvincible(false)
        end,
    },
	
	State{
        name = "corpse_rebirth",
        tags = { "busy", "noattack", "nopredict", "nomorph" },

        onenter = function(inst)
            if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:RemotePausePrediction()
                inst.components.playercontroller:Enable(false)
            end

			
			inst.sg:SetTimeout(107 * FRAMES)
			
			inst.Physics:CollidesWith(COLLISION.WORLD)
            inst.Physics:CollidesWith(COLLISION.OBSTACLES)
            inst.Physics:CollidesWith(COLLISION.SMALLOBSTACLES)
            inst.Physics:CollidesWith(COLLISION.CHARACTERS)
            inst.Physics:CollidesWith(COLLISION.GIANTS)

            inst.components.health:SetInvincible(true)
            inst:ShowActions(false)
            inst:SetCameraDistance(14)
        end,

        timeline =
        {
            TimeEvent(53 * FRAMES, function(inst)
                inst.components.bloomer:PushBloom("corpse_rebirth", "shaders/anim.ksh", -2)
                inst.sg.statemem.fadeintime = (86 - 53) * FRAMES
                inst.sg.statemem.fadetime = 0
            end),
            TimeEvent(86 * FRAMES, function(inst)
                inst.sg.statemem.physicsrestored = true
                inst.Physics:ClearCollisionMask()
                inst.Physics:CollidesWith(COLLISION.WORLD)
                inst.Physics:CollidesWith(COLLISION.OBSTACLES)
                inst.Physics:CollidesWith(COLLISION.SMALLOBSTACLES)
                inst.Physics:CollidesWith(COLLISION.CHARACTERS)
                inst.Physics:CollidesWith(COLLISION.GIANTS)

                
                if inst.sg.statemem.fade ~= nil then
                    inst.sg.statemem.fadeouttime = 20 * FRAMES
                    inst.sg.statemem.fadetotal = inst.sg.statemem.fade
                end
                inst.sg.statemem.fadeintime = nil
            end),
			TimeEvent((96) * FRAMES, function(inst)
				local pos = inst:GetPosition()
				--local fire = SpawnPrefab("lavaarena_portal_player_fx")
				--fire.Transform:SetPosition(pos.x, 0, pos.z)
				--if inst:HasTag("largecreature") or inst:HasTag("epic") then --totally not for PP I swear! --Fid: Hmmmm are you sure?
					--fire.Transform:SetScale(3, 3, 3)
				--end
            end),
            TimeEvent((86 + 20) * FRAMES, function(inst)
				inst:Hide()
                inst.components.bloomer:PopBloom("corpse_rebirth")
            end),
        },
		
		
        onupdate = function(inst, dt)
            if inst.sg.statemem.fadeouttime ~= nil then
                inst.sg.statemem.fade = math.max(0, inst.sg.statemem.fade - inst.sg.statemem.fadetotal * dt / inst.sg.statemem.fadeouttime)
                if inst.sg.statemem.fade > 0 then
                    inst.components.colouradder:PushColour("corpse_rebirth", inst.sg.statemem.fade, inst.sg.statemem.fade, inst.sg.statemem.fade, 0)
                else
                    inst.components.colouradder:PopColour("corpse_rebirth")
                    inst.sg.statemem.fadeouttime = nil
                end
            elseif inst.sg.statemem.fadeintime ~= nil then
                local k = 1 - inst.sg.statemem.fadetime / inst.sg.statemem.fadeintime
                inst.sg.statemem.fade = .8 * (1 - k * k)
                inst.components.colouradder:PushColour("corpse_rebirth", inst.sg.statemem.fade, inst.sg.statemem.fade, inst.sg.statemem.fade, 0)
                inst.sg.statemem.fadetime = inst.sg.statemem.fadetime + dt
            end
        end,
		
		ontimeout = function(inst)
			inst.components.bloomer:PopBloom("corpse_rebirth")
            inst.sg:GoToState("corpse_taunt")
        end,
		
        onexit = function(inst)
			inst.components.health:SetPercent(0.5)
            inst:ShowActions(true)
            inst:SetCameraDistance()
			inst:Show()
            if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:Enable(true)
            end
            inst.components.health:SetInvincible(false)

            inst.Physics:ClearCollisionMask()
            inst.Physics:CollidesWith(COLLISION.WORLD)
            inst.Physics:CollidesWith(COLLISION.OBSTACLES)
            inst.Physics:CollidesWith(COLLISION.SMALLOBSTACLES)
            inst.Physics:CollidesWith(COLLISION.CHARACTERS)
            inst.Physics:CollidesWith(COLLISION.GIANTS)

            SerializeUserSession(inst)
        end,
    },
	
	State{
        name = "corpse_taunt",
        tags = { "busy", "noattack", "nopredict", "nomorph" },

        onenter = function(inst)
            if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:RemotePausePrediction()
                inst.components.playercontroller:Enable(false)
            end
			
			inst.Physics:CollidesWith(COLLISION.WORLD)
            inst.Physics:CollidesWith(COLLISION.OBSTACLES)
            inst.Physics:CollidesWith(COLLISION.SMALLOBSTACLES)
            inst.Physics:CollidesWith(COLLISION.CHARACTERS)
            inst.Physics:CollidesWith(COLLISION.GIANTS)

			inst.AnimState:PlayAnimation("activate")
			
            inst.components.health:SetInvincible(true)
            inst:ShowActions(false)
            inst:SetCameraDistance(14)
        end,

		timeline=
		{
			--------------------------------------------- arm/claw
            TimeEvent(3*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_clawp" then
                    inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/arm/active")
                    inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/arm/start")
                end
            end),
            TimeEvent(4*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_clawp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/arm/active")
                end
            end),
            TimeEvent(6*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_clawp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/arm/active") 
                end
            end),
            TimeEvent(8*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_clawp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/arm/active") 
                end
            end),
            TimeEvent(9*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_clawp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/arm/active") 
                end
            end),
            TimeEvent(12*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_clawp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/arm/active") 
                end
            end),

            TimeEvent(16*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_clawp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/arm/active") 
                end
            end),
            TimeEvent(30*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_clawp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/electro")
                end
            end),
            TimeEvent(37*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_clawp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/arm/servo") 
                end
            end),
            TimeEvent(30*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_clawp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/electro")
                end
            end),
            TimeEvent(41*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_clawp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/electro")
                end
            end),
            TimeEvent(50*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_clawp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/electro")
                end
            end),
            TimeEvent(51*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_clawp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/arm/servo") 
                end
            end),
            TimeEvent(53*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_clawp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/electro")
                end
            end),
            TimeEvent(62*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_clawp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/arm/servo") 
                end
            end),
            TimeEvent(70*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_clawp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/arm/servo") 
                end
            end),
--------------------------------------------- head
            TimeEvent(0*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_headp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/head/active")
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/head/start")
                end
            end),

            TimeEvent(1*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_headp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/head/active")
                end
            end),
            TimeEvent(3*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_headp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/head/active")
                end
            end),
            TimeEvent(5*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_headp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/head/active")
                end
            end),
            TimeEvent(6*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_headp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/head/active")
                end
            end),
            TimeEvent(9*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_headp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/head/active")
                end
            end),
            TimeEvent(12*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_headp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/head/active")
                end
            end),
            TimeEvent(14*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_headp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/electro")
                end
            end),
            TimeEvent(16*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_headp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/head/active")
                end
            end),
            TimeEvent(17*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_headp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/electro")
                end
            end),
            TimeEvent(27*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_headp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/electro")
                end
            end),
            TimeEvent(30*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_headp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/electro")
                end
            end),
            TimeEvent(37*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_headp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/electro")
                end
            end),
            TimeEvent(40*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_headp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/electro")
                end
            end),
            TimeEvent(54*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_headp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/electro")
                end
            end),
            TimeEvent(57*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_headp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/electro")
                end
            end),
            
            --------------------------------------------- leg
            TimeEvent(0*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_legp" then
                    inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/leg/active")
                    inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/leg/start")
                end
            end),
            
            TimeEvent(4*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_legp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/leg/active")
                end
            end),
            TimeEvent(5*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_legp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/leg/active")
                end
            end),
            TimeEvent(9*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_legp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/leg/active")
                end
            end),
            TimeEvent(13*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_legp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/leg/active")
                end
            end),
             TimeEvent(14*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_legp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/electro")
                end
            end),
            TimeEvent(17*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_legp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/electro")
                end
            end),
            TimeEvent(27*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_legp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/leg/servo")
                end
            end),
            TimeEvent(44*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_legp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/leg/servo")
                end
            end),
            TimeEvent(58*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_legp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/leg/step",nil,.06)
                end
            end),
            --------------------------------------------- ribs
            TimeEvent(2*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_spiderp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/ribs/active")
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/ribs/start")
                end
            end),
            
            TimeEvent(4*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_spiderp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/ribs/active")
                end
            end),
            TimeEvent(6*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_spiderp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/ribs/active")
                end
            end),
            TimeEvent(8*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_spiderp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/ribs/active")
                end
            end),
            TimeEvent(9*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_spiderp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/electro")
                end
            end),
            TimeEvent(10*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_spiderp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/ribs/active")
                end
            end),
            TimeEvent(12*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_spiderp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/ribs/active")
                end
            end),
            TimeEvent(13*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_spiderp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/electro")
                end
            end),
            TimeEvent(14*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_spiderp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/ribs/active")
                end
            end),
            TimeEvent(16*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_spiderp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/ribs/active")
                end
            end),
            TimeEvent(18*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_spiderp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/electro")
                end
            end),
            TimeEvent(21*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_spiderp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/electro")
                end
            end),
            TimeEvent(30*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_spiderp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/electro")
                end
            end),
            TimeEvent(33*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_spiderp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/electro")
                end
            end),
            TimeEvent(36*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_spiderp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/electro")
                end
            end),
            TimeEvent(39*FRAMES, function(inst) 
                if inst.prefab == "ancient_robot_spiderp" then
                   inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/metal_robot/electro")
                end
            end),
		},

        events =
        {
            EventHandler("animqueueover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("idle")
                end
            end),
        },

        onexit = function(inst)
            inst:ShowActions(true)
            inst:SetCameraDistance()
            if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:Enable(true)
            end
            inst.components.health:SetInvincible(false)

            inst.Physics:ClearCollisionMask()
            inst.Physics:CollidesWith(COLLISION.WORLD)
            inst.Physics:CollidesWith(COLLISION.OBSTACLES)
            inst.Physics:CollidesWith(COLLISION.SMALLOBSTACLES)
            inst.Physics:CollidesWith(COLLISION.CHARACTERS)
            inst.Physics:CollidesWith(COLLISION.GIANTS)

            SerializeUserSession(inst)
        end,
    },
	
	State
    {
        name = "revivecorpse",

        onenter = function(inst)
            --inst.components.talker:Say(GetString(inst, "ANNOUNCE_REVIVING_CORPSE"))
            local buffaction = inst:GetBufferedAction()
            local target = buffaction ~= nil and buffaction.target or nil
            inst.sg:GoToState("action_loop",
                TUNING.REVIVE_CORPSE_ACTION_TIME *
                (inst.components.corpsereviver ~= nil and inst.components.corpsereviver:GetReviverSpeedMult() or 1) *
                (target ~= nil and target.components.revivablecorpse ~= nil and target.components.revivablecorpse:GetReviveSpeedMult() or 1)
            )
        end,
    },
	
}
PP_CommonStates.AddActionStates(states, 
	{
		TimeEvent(0 * FRAMES, function(inst)
            inst:PerformBufferedAction()
        end),
	}, 
	"walk_pst", nil, nil, "taunt", "walk_pst") --(states, timelines, anim, anim2, enteranim, loopanim, exitanim, noanim, events)
PP_CommonStates.AddSailStates(states, {}, "walk_pst", "idle")
local simpleanim = "walk_pst"
local simpleidle = "idle"
local simplemove = "walk"
if CurrentRelease.GreaterOrEqualTo(ReleaseID.IDs.R08_ROT_TURNOFTIDES) then
CommonStates.AddHopStates(states, false, {pre = simplemove.."_pre", loop = simplemove.."_loop", pst = simplemove.."_pst"}, nil, "death")
end
PP_CommonStates.AddSailorStates(states, nil, 
{
	plank_idle = simpleanim,
	plank_idle_loop = simpleidle,
	plank_idle_pst = simpleanim,
	
	plank_hop_pre = simplemove.."_pre",
	plank_hop = simplemove.."_loop",
	
	steer_pre = simpleanim,
	steer_idle = simpleidle,
	steer_turning = simpleanim,
	stop_steering = simpleanim,
	
	row = simpleanim,
}
)

    
return StateGraph("ancient_robotp", states, events, "activate", actionhandlers)

