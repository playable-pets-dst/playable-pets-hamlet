require("stategraphs/commonstates")

local function FixAngle( target_angle)
    if target_angle > 360 then
        return target_angle % 360
    elseif target_angle < 0 then
        while target_angle < 0 do
            target_angle = target_angle + 360
        end
        return target_angle
    end
    return target_angle
end

local longaction = "action"
local shortaction = "action"
local workaction = "action"
local otheraction = "action"

local actionhandlers = PP_CommonStates.GetCommonActions(longaction, shortaction, workaction, otheraction)

local actionhandler_overrides = 
{ 
	ActionHandler(ACTIONS.ATTACK, "thunder_attack"),
}

if #actionhandler_overrides > 0 then
	for i, v in ipairs(actionhandler_overrides) do
		table.insert(actionhandlers, v)
	end
end

local events=
{
	EventHandler("attacked", function(inst, data) 
		if data.stimuli and data.stimuli == "strong" and not (inst.sg:HasStateTag("transform") or inst.sg:HasStateTag("nointerrupt")) and not inst.components.health:IsDead() then
			inst.sg:GoToState("hit")
		elseif data.stimuli and (data.stimuli == "electric" or data.stimuli == "explosive") and not inst.components.health:IsDead() then
			inst.sg:GoToState("stun", data.stimuli)
		elseif not inst.components.health:IsDead() and not inst.sg:HasStateTag("busy") and not inst.sg:HasStateTag("transform") then 
			inst.sg:GoToState("hit") 
		end 
	end),
	PP_CommonHandlers.OnKnockback(),
    EventHandler("death", function(inst, data)
        if inst.sleepingbag ~= nil and (inst.sg:HasStateTag("bedroll") or inst.sg:HasStateTag("tent")) then -- wakeup on death to "consume" sleeping bag first
            inst.sleepingbag.components.sleepingbag:DoWakeUp()
            inst.sleepingbag = nil
        end

        if (data ~= nil and data.cause == "file_load") or inst.components.revivablecorpse ~= nil then
            inst.sg:GoToState("corpse", true)
        else
            inst.sg:GoToState("death")
        end
    end),
	PP_CommonHandlers.AddCommonHandlers(),
    CommonHandlers.OnSleep(),
    --CommonHandlers.OnLocomote(true,false),
	EventHandler("locomote", function(inst, data)
        if inst.sg:HasStateTag("busy") then
            return
        end
        local is_moving = inst.sg:HasStateTag("moving")
        local should_move = inst.components.locomotor:WantsToMoveForward()
		local is_looking = inst.sg:HasStateTag("looking")

        if inst.sg:HasStateTag("bedroll") or inst.sg:HasStateTag("tent") or inst.sg:HasStateTag("waking") then -- wakeup on locomote
            if inst.sleepingbag ~= nil and inst.sg:HasStateTag("sleeping") then
                inst.sleepingbag.components.sleepingbag:DoWakeUp()
                inst.sleepingbag = nil
				inst:Show()
				--inst.AnimState:PushAnimation("pig_pickup")
				SetSleeperAwakeState(inst)
				--inst.AnimState:PushAnimation("pig_pickup")
				--inst.sg:GoToState("idle", true)
				
            end 
			elseif is_moving and not should_move then
            inst.sg:GoToState("run_stop")
        elseif not is_moving and should_move and not is_looking then
            inst.sg:GoToState("run_start")
        elseif data.force_idle_state and not (is_moving or should_move or inst.sg:HasStateTag("idle") or is_looking) then
            inst.sg:GoToState("idle")
        end
    end),
    CommonHandlers.OnFreeze(),
	
	 EventHandler("ms_opengift",
        function(inst)
            if not inst.sg:HasStateTag("busy") then
                inst.sg:GoToState("opengift")
            end
        end),
		
	EventHandler("respawnfromghost", function(inst)  
			if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:Enable(true)
            end

            inst.components.health:SetInvincible(false)
            inst:ShowHUD(true)
            inst:SetCameraDistance()

            SerializeUserSession(inst) end),	
}

if CurrentRelease.GreaterOrEqualTo(ReleaseID.IDs.R08_ROT_TURNOFTIDES) then
	table.insert(events, PP_CommonHandlers.OnSink())
	table.insert(events, CommonHandlers.OnHop())
end

 local states=
{

    State{
        name= "idle",
        tags = {"idle", "canrotate"},
        
        onenter = function(inst)

            if inst.fx and math.random() < 0.1 then
                inst.fx.AnimState:PlayAnimation("idle")
            end

            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("idle")
            
            end,
        
        
        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end ),
        },        
    },
	
	State{
        name = "charge_pre",
        tags = {"busy"},
        
        onenter = function(inst)
            
            if inst.fx then
                inst.fx.AnimState:PlayAnimation("charge_pre")
                inst.fx.AnimState:PushAnimation("charge_loop", true)
            end

            inst.AnimState:PlayAnimation("charge_pre")
            inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/lightninggoat/jacobshorn",nil,.25)
            inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/lightninggoat/shocked_electric",nil,.25)
            inst.Physics:Stop()
        end,

        
        
        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("charge") end ),
        },        
    },

    State{
        name = "charge",
        tags = {"busy"},
        
        onenter = function(inst)
            inst.AnimState:PlayAnimation("charge_loop", true)
            inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/thunderbird/hum_LP","hum")            
        end,

        onexit = function(inst)
            inst.SoundEmitter:KillSound("hum")           
        end,
    },

    State{
        name = "charge_pst",
        tags = {"busy"},
        
        onenter = function(inst)

            if inst.fx then
                inst.fx.AnimState:PlayAnimation("charge_pst")
            end
            
            inst.AnimState:PlayAnimation("charge_pst")
        end,
        
        events=
        {
            EventHandler("animover", function(inst) 
                inst.charging = false
                inst.sg:GoToState("idle") 
            end ),
        },        
    },

    State{
        name = "thunder_attack",
        tags = {"busy", "attack"},
        
        onenter = function(inst)
			if not inst.thunder_cd then
				inst.sg:GoToState("idle")
			else
			
				local buffaction = inst:GetBufferedAction()
				local target = buffaction ~= nil and buffaction.target or nil
				if target ~= nil then
					if target:IsValid() then
						inst:ForceFacePoint(target:GetPosition())
						inst.sg.statemem.attacktarget = target
					end
				end
				inst.components.combat:StartAttack()
			
				if inst.fx then
					inst.fx.AnimState:PlayAnimation("shoot")
				end
				
				inst.thunder_cd = false
				if inst.thunder_cd_task then
					inst.thunder_cd_task:Cancel()
					inst.thunder_cd_task = nil
				end
				inst.thunder_cd_task = inst:DoTaskInTime(10, function(inst) inst.thunder_cd = true end)
           
				inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/thunderbird/attack_swipe")
				inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/thunderbird/shoot")
				inst.Physics:Stop()
				inst.AnimState:PlayAnimation("shoot")
			
				inst.DoLightning(inst, inst.sg.statemem.attacktarget)
			end
        end,
        
        timeline =
        {
            TimeEvent(10*FRAMES, function(inst)
				inst.components.combat:DoAttack(inst.sg.statemem.attacktarget, nil, nil, "electric")
				local lightning = SpawnPrefab("lightning")
				lightning.Transform:SetPosition(inst.sg.statemem.attacktarget:GetPosition():Get())
			end),
        },
        
        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },	
	
	
	
	State{
		name = "hit",
        tags = {"busy", "hit"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("hit")
			inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/thunderbird/hit")
        end,

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	
	State{
		name = "special_atk1",
        tags = {"busy"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("idle")
			--Lets try being spooky and stare at people.
			if inst.lookingmode and inst.lookingmode == true then
				inst.lookingmode = false
			elseif not inst.lookingmode or inst.lookingmode == false then
				inst.lookingmode = true
			end
        end,

		timeline=
        {
			
        },

        events=
        {
			EventHandler("animover", function(inst) 
				if inst.lookingmode and inst.lookingmode == true then
					inst.sg:GoToState("lookmode")
				else
					inst.sg:GoToState("idle")
				end
			end),
        },
    },
	
	State{
		name = "lookmode",
        tags = {"looking", "canrotate", "caninterrupt"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
			--inst.AnimState:PlayAnimation("CCW")
			local origin_percents = {
                    [FACING_DOWN] = 0,
                    [FACING_RIGHT] = 0.25,
                    [FACING_UP] = 0.5,
                    [FACING_LEFT] = 0.75
                }

                inst.origin = origin_percents[inst.AnimState:GetCurrentFacing()]
                inst.current_percent = inst.origin

                inst.Transform:SetNoFaced()
        end,
		
		
		onexit = function(inst)
			inst.Transform:SetFourFaced()
		end,
		
		onupdate = function(inst)
		local angle = -inst.Transform:GetRotation() * DEGREES
		----print(angle)

		if angle < 0 then
			angle = (2*PI + angle)
		end

		angle = angle * 360 / (2*PI)

		angle = FixAngle(angle - TheCamera:GetHeadingTarget())
		local percent = angle / 360
		inst.AnimState:SetPercent("CCW", percent)
		end
		--[[
		local diff = percent > inst.current_percent and percent - inst.current_percent or inst.current_percent - percent
		-- Minimum percent for us to change frames
		if diff > 0.02 then

			-- Determines the orientation we're supposed to rotate towards
			if percent > inst.origin then
				if percent - inst.origin > 0.5 then
					inst.current_percent = inst.current_percent - 0.02
				elseif percent - inst.origin < 0.5 then
					inst.current_percent = inst.current_percent + 0.02
				end

			elseif percent < inst.origin then
				if inst.origin - percent > 0.5 then
					inst.current_percent = inst.current_percent + 0.02
				elseif inst.origin - percent < 0.5 then
					inst.current_percent = inst.current_percent - 0.02
				end
			end

			-- if we've gone over the edge of the animation this fixes it before setting it
			if inst.current_percent < 0 then
				inst.current_percent = 1 + inst.current_percent
			elseif inst.current_percent > 1 then
				inst.current_percent = inst.current_percent - 1
			end

				inst.AnimState:SetPercent("CCW", inst.current_percent)
			else
				-- We've reached the target, so this is our new origin
				inst.origin = inst.current_percent
			end
		end,]]
    },
	
	State{
		name = "action",
        tags = {"busy"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
			inst:PerformBufferedAction()
            inst.AnimState:PlayAnimation("walk_pst")
        end,

		timeline=
        {

        },

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
    State{
        name = "death",
        tags = {"busy"},

        onenter = function(inst)
			inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/thunderbird/death")
            inst.AnimState:PlayAnimation("death")
            inst.Physics:Stop()
            RemovePhysicsColliders(inst)     
			inst.components.lootdropper:DropLoot(Vector3(inst.Transform:GetWorldPosition()))
			inst.components.inventory:DropEverything(true)
        end,
		
		timeline=
        {

        },

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
					PlayablePets.DoDeath(inst)
                end
            end),
        },

    },
	
	State {
        name = "sleep",
        tags = { "sleeping", "busy" }, --add tag "busy" if you hate sliding

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pre")
            --if fns ~= nil and fns.onsleep ~= nil then
                --fns.onsleep(inst)
            --end
        end,

        --timeline = timelines ~= nil and timelines.starttimeline or nil,

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("sleeping") end ),
            EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "sleeping",
        tags = { "sleeping", "busy" },

        --onenter = onentersleeping,
		
		onenter = function(inst)
				inst.components.locomotor:StopMoving()
				PlayablePets.SleepHeal(inst)
				inst.AnimState:PlayAnimation("sleep_loop")
			end,
			
		onexit = function(inst)
		--inst.components.sanity.dapperness = -0.5
		end,
        --timeline = timelines ~= nil and timelines.sleeptimeline or nil,
		timeline=
        {
			--TimeEvent(20*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/spidermonkey/sleep")
			--inst.SoundEmitter:PlaySound("dontstarve/creatures/hound/sleep")
			--end),
        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("sleeping") end ),
			EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "wake",
        tags = { "busy", "waking" },

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pst")
            if inst.components.sleeper ~= nil and inst.components.sleeper:IsAsleep() then
                inst.components.sleeper:WakeUp()
            end
            --if fns ~= nil and fns.onwake ~= nil then
                --fns.onwake(inst)
            --end
        end,

        --timeline = timelines ~= nil and timelines.waketimeline or nil,

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State
    {
        name = "run_start",
        tags = { "moving", "running", "canrotate" },

        onenter = function(inst)
			inst.components.locomotor:RunForward()
            inst.AnimState:PlayAnimation("run_pre")
			
        end,
		
		timeline = 
		{
			--TimeEvent(0*FRAMES, function(inst) inst.Physics:Stop() end ),
		},
		
        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("run") end ),
        },
    },
	
	State
    {
        name = "run",
        tags = { "moving", "running", "canrotate" },

        onenter = function(inst)
			--inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/run")
			inst.components.locomotor:RunForward()
			inst.AnimState:PlayAnimation("run_loop")
        end,
		
		timeline = {

			TimeEvent(2*FRAMES, PlayFootstep ),
        -- TimeEvent(12*FRAMES, PlayFootstep ),
			TimeEvent(2*FRAMES, function(inst) 
            if inst.should_play_idle then
                    inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/thunderbird/run")
                    inst.should_play_idle = false
                else
                    inst.should_play_idle = true
                end
            end),
			
			--TimeEvent(23*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/spiderqueen/walk_spiderqueen") inst.SoundEmitter:PlaySound("dontstarve/creatures/rocklobster/footstep") end),
		},
		
        events=
			{
				EventHandler("animqueueover", function(inst) 
					inst.sg:GoToState("run")  
				end ),
			},

    },
	
	State
    {
        name = "run_stop",
        tags = { "idle" },

        onenter = function(inst) 
            inst.components.locomotor:StopMoving()
			inst.AnimState:PlayAnimation("walk_pst")
			--inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/step")            
        end,

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end ),
        },
    },	
}

CommonStates.AddFrozenStates(states)
PP_CommonStates.AddKnockbackState(states, nil, "hit") --(states, timeline, anim, sounds, fns)
PP_CommonStates.AddActionStates(states, 
	{
		TimeEvent(0 * FRAMES, function(inst)
            inst:PerformBufferedAction()
        end),
	}, 
	"walk_pst", nil, nil, "shoot", "walk_pst") --(states, timelines, anim, anim2, enteranim, loopanim, exitanim, noanim, events)
PP_CommonStates.AddCorpseStates(states, nil, 
	--timelines = 
	{
		corpse =
		{
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/thunderbird/death") end),
		},
		
		corpse_taunt =
		{

		},
	
	},
	--anims = 
	{
		corpse = "death",
		corpse_taunt = "idle"
	},
	--sounds =
	{
		--corpse = "dontstarve_DLC003/creatures/enemy/grabbing_vine/death"
	},
	--fns =
	{

	}
	) ---(states, isflying, timelines, anims, sounds, fns, events, nofx)
PP_CommonStates.AddJumpInStates(states, nil, "walk_pst")
PP_CommonStates.AddOpenGiftStates(states, "shoot")
PP_CommonStates.AddSailStates(states, {}, "walk_pst", "idle")
local simpleanim = "walk_pst"
local simpleidle = "idle"
local simplemove = "walk"
if CurrentRelease.GreaterOrEqualTo(ReleaseID.IDs.R08_ROT_TURNOFTIDES) then
CommonStates.AddHopStates(states, false, {pre = simplemove.."_pre", loop = simplemove.."_loop", pst = simplemove.."_pst"}, nil, "death")
end
PP_CommonStates.AddSailorStates(states, nil, 
{
	plank_idle = simpleanim,
	plank_idle_loop = simpleidle,
	plank_idle_pst = simpleanim,
	
	plank_hop_pre = simplemove.."_pre",
	plank_hop = simplemove.."_loop",
	
	steer_pre = simpleanim,
	steer_idle = simpleidle,
	steer_turning = simpleanim,
	stop_steering = simpleanim,
	
	row = "walk_pst",
}
)
    
return StateGraph("thunderbirdp", states, events, "idle", actionhandlers)

