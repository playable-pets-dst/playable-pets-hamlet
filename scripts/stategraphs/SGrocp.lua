require("stategraphs/commonstates")

local function GetExcludeTags(inst)
	if TheNet:GetPVPEnabled() then
		return {"INLIMBO", "playerghost", "notarget", "shadow", "groundpoundimmune"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion", "playerghost", "INLIMBO", "groundpoundimmune"}
	else	
		return {"player", "companion", "INLIMBO", "playerghost", "notarget", "shadow", "groundpoundimmune"}
	end
end

local function ShakeIfClose(inst)
    ShakeAllCameras(CAMERASHAKE.VERTICAL, .8, .02, .9, inst, 40)
end

local function CheckWaterTile(pos)
	if SW_PP_MODE == true and pos then
		if IsOnWater(pos:Get()) then
			return true
		else
			return false
		end
	elseif CurrentRelease.GreaterOrEqualTo(ReleaseID.IDs.R08_ROT_TURNOFTIDES) and TheNet:GetServerGameMode() ~= "lavaarena" then
		return false --everything is ocean, so who cares.
	else
		return false
	end
end

local function SetFlying(inst)
	inst.isflying = true
	inst.AnimState:SetBank("roc")
	inst.AnimState:SetBuild("roc_shadow")
	inst.AnimState:PlayAnimation("shadow")
	inst.Transform:SetNoFaced()
	inst.Transform:SetScale(1,1,1)
	inst.AnimState:SetOrientation( ANIM_ORIENTATION.OnGround )
	inst.AnimState:SetLayer( LAYER_BACKGROUND )
	inst.AnimState:SetSortOrder( 1 )	
	
	inst.components.locomotor.runspeed = 10
	
	if inst.sg.statemem.takeoff_pos then
		inst.Transform:SetPosition(inst.sg.statemem.takeoff_pos:Get())
	end
	
	if inst.sg.statemem.takeoff_rotation then
		inst.Transform:SetRotation(inst.sg.statemem.takeoff_rotation)
	end
	
	inst.components.health:SetInvincible(true)
	--remove tags first to ensure that we don't run into the taglimit
	inst:RemoveTag("epic")
	inst:RemoveTag("monster")
	inst:AddTag("notarget")
	inst:AddTag("flying")
	inst:AddTag("NOCLICK")
	
	inst.acidimmune = true
	inst.inkimmune = true
	inst.poisonimmune = true
	inst.debuffimmune = true

	inst.AnimState:SetMultColour(1, 1, 1, 0.5)
end

local function SetLand(inst)
	inst.isflying = nil
	inst.AnimState:SetBank("head")
	inst.AnimState:SetBuild("roc_head_build")
	inst.AnimState:PlayAnimation("idle_pre")
	inst.AnimState:SetOrientation(ANIM_ORIENTATION.Default )
	inst.AnimState:SetLayer(LAYER_WORLD)
	inst.AnimState:SetSortOrder( 0 )
	inst.Transform:SetEightFaced()
	inst.Transform:SetScale(0.8,0.8,0.8)
	
	inst.components.health:SetInvincible(false)
	--remove tags first to ensure that we don't run into the taglimit
	inst:RemoveTag("flying")
	inst:RemoveTag("notarget")
	inst:RemoveTag("NOCLICK")
	inst:AddTag("monster")
	inst:AddTag("epic")
	
	inst.acidimmune = nil
	inst.inkimmune = nil
	inst.poisonimmune = nil
	inst.debuffimmune = nil
	
	inst.components.locomotor.runspeed = 6*1.2
	
	inst.AnimState:SetMultColour(1, 1, 1, 1)
end

local longaction = "grab"
local shortaction = "grab"
local workaction = "grab"
local otheraction = "action_fast"

local actionhandlers = PP_CommonStates.GetCommonActions(longaction, shortaction, workaction, otheraction)

local actionhandler_overrides = 
{ 
	ActionHandler(ACTIONS.REVIVE_CORPSE, "failaction"), --no reviving
    ActionHandler(ACTIONS.PICK, "grab"),
    ActionHandler(ACTIONS.HARVEST, "grab"),
}

if #actionhandler_overrides > 0 then
	for i, v in ipairs(actionhandler_overrides) do
		table.insert(actionhandlers, v)
	end
end

--for action_loop state
local function StopActionMeter(inst, flash)
    if inst.HUD ~= nil then
        inst.HUD:HideRingMeter(flash)
    end
    if inst.sg.mem.actionmetertask ~= nil then
        inst.sg.mem.actionmetertask:Cancel()
        inst.sg.mem.actionmetertask = nil
        inst.player_classified.actionmeter:set(flash and 1 or 0)
    end
end

local function UpdateActionMeter(inst, starttime)
    inst.player_classified.actionmeter:set_local(math.min(255, math.floor((GetTime() - starttime) * 10 + 2.5)))
end

local function StartActionMeter(inst, duration)
    if inst.HUD ~= nil then
        inst.HUD:ShowRingMeter(inst:GetPosition(), duration)
    end
    inst.player_classified.actionmetertime:set(math.min(255, math.floor(duration * 10 + .5)))
    inst.player_classified.actionmeter:set(2)
    if inst.sg.mem.actionmetertask == nil then
        inst.sg.mem.actionmetertask = inst:DoPeriodicTask(.1, UpdateActionMeter, nil, GetTime())
    end
end

local events=
{
	EventHandler("attacked", function(inst, data) 
		
	end),
    EventHandler("death", function(inst, data)
        if inst.sleepingbag ~= nil and (inst.sg:HasStateTag("bedroll") or inst.sg:HasStateTag("tent")) then -- wakeup on death to "consume" sleeping bag first
            inst.sleepingbag.components.sleepingbag:DoWakeUp()
            inst.sleepingbag = nil
        end

        if (data ~= nil and data.cause == "file_load") or inst.components.revivablecorpse ~= nil then
            inst.sg:GoToState("corpse", true)
        else
            inst.sg:GoToState("death")
        end
    end),
	PP_CommonHandlers.AddCommonHandlers(),
    EventHandler("doattack", function(inst, data) if not inst.components.health:IsDead() and (inst.sg:HasStateTag("hit") or not inst.sg:HasStateTag("busy")) then inst.sg:GoToState("attack", data.target) end end),
    CommonHandlers.OnSleep(),
    CommonHandlers.OnLocomote(true,false),
    CommonHandlers.OnFreeze(),
	
	 EventHandler("ms_opengift",
        function(inst)
            if not inst.sg:HasStateTag("busy") then
                inst.sg:GoToState("opengift")
            end
        end),
		
	EventHandler("respawnfromghost", function(inst)  
			if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:Enable(true)
            end

            inst.components.health:SetInvincible(false)
            inst:ShowHUD(true)
            inst:SetCameraDistance()

            SerializeUserSession(inst) end),	
}

if CurrentRelease.GreaterOrEqualTo(ReleaseID.IDs.R08_ROT_TURNOFTIDES) then
	table.insert(events, PP_CommonHandlers.OnSink())
end


 local states=
{

    State{
        
        name = "idle",
        tags = {"idle", "canrotate"},
        onenter = function(inst, playanim)
            inst.Physics:Stop()
			if inst.isflying then
				inst.AnimState:PlayAnimation("shadow_flap_loop")
			else
				inst.AnimState:PlayAnimation("idle_loop", true)
			end
        end,
        
       timeline = 
        {
			TimeEvent(16*FRAMES, function(inst) 
				if inst.isflying then
					inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/roc/flap")
				end	
            end),
            
            TimeEvent(1*FRAMES, function(inst) --if math.random() < 0.5 then
                --inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/roc/call","calls") end
                ----inst.SoundEmitter:SetParameter("calls", "intensity", inst.sounddistance)
            end),
        },
        
        events=
        {
            EventHandler("animover", function(inst) 
                inst.sg:GoToState("idle")                 
            end),
        },
    },
	
	 State
    {
        name = "attack",
        tags = {"busy" },

        onenter = function(inst)
			if inst.isflying then
				inst.sg:GoToState("idle") 
			else
				inst.components.combat:StartAttack()
				inst.AnimState:PlayAnimation("bash_pre")           
				inst.AnimState:PushAnimation("bash_loop",false)           
				inst.AnimState:PushAnimation("bash_pst",false)
			end
        end,
        

        timeline =
        {
            TimeEvent(37*FRAMES, function(inst) 
				inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/bearger/groundpound")
                inst.components.groundpounder:GroundPound()
				inst.components.combat:DoAreaAttack(inst, 5, nil, nil, "strong", GetExcludeTags())
				ShakeIfClose(inst)
            end)
        },

        events =
        {
            EventHandler("animqueueover", function(inst, data)
                inst.sg:GoToState("idle")
            end),
        }
    },

   State
    {
        name = "grab",
        tags = {"busy", "grab" },

        onenter = function(inst)
			if inst.isflying then
				inst.sg:GoToState("idle") 
			else
				inst.AnimState:PlayAnimation("grab_pre")           
				inst.AnimState:PushAnimation("grab_loop",false)           
				inst.AnimState:PushAnimation("grab_pst",false)
			end
        end,
        
        timeline =
        {
            TimeEvent(14*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/roc/attack_1") end),
            TimeEvent(20*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/roc/attack_2") end),
            TimeEvent(24*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/roc/attack_3") end),
            TimeEvent(42*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/roc/close_whoosh") end),
            TimeEvent(54*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/roc/close") end),
            TimeEvent(31*FRAMES, function(inst) 
                inst:PerformBufferedAction()
            end)
        },

        onexit = function(inst)

        end,

        events =
        {
            EventHandler("animqueueover", function(inst, data)
                inst.sg:GoToState("idle")
            end),
        }
    },
	
	State
    {
        name = "action2",
        tags = {"idle","canrotate","busy"},

        onenter = function(inst)
			if inst.isflying then
				inst.sg:GoToState("idle") 
			else
				inst.AnimState:PlayAnimation("taunt")
			end	
        end,
        
        timeline=
        {
            TimeEvent(14*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/roc/attack_1") end),
            TimeEvent(20*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/roc/attack_2") end),
            TimeEvent(24*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/roc/attack_3") inst:PerformBufferedAction() end),
            TimeEvent(36*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/roc/close_whoosh") end),
            TimeEvent(48*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/roc/close") end),
        },
        
        events =
        {
            EventHandler("animover", function(inst, data)
                inst.sg:GoToState("idle")
            end),
        }
    },
	
	State
    {
        name = "special_atk1",
        tags = {"idle","canrotate","busy"},

        onenter = function(inst)
			if inst.isflying then
				inst.sg:GoToState("idle") 
			else
				inst.AnimState:PlayAnimation("taunt")
			end	
        end,
        
        timeline=
        {
            TimeEvent(14*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/roc/attack_1") end),
            TimeEvent(20*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/roc/attack_2") end),
            TimeEvent(24*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/roc/attack_3") end),
            TimeEvent(36*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/roc/close_whoosh") end),
            TimeEvent(48*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/roc/close") end),
        },
        
        events =
        {
            EventHandler("animover", function(inst, data)
                inst.sg:GoToState("idle")
            end),
        }
    },
	
	State
    {
        name = "taunt",
        tags = {"idle","canrotate","busy"},

        onenter = function(inst)
			if inst.isflying then
				inst.sg:GoToState("idle") 
			else
				inst.AnimState:PlayAnimation("taunt")
			end
        end,
        
        timeline=
        {
            TimeEvent(14*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/roc/attack_1") end),
            TimeEvent(20*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/roc/attack_2") end),
            TimeEvent(24*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/roc/attack_3") end),
            TimeEvent(36*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/roc/close_whoosh") end),
            TimeEvent(48*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/roc/close") end),
        },
        
        events =
        {
            EventHandler("animover", function(inst, data)
                inst.sg:GoToState("idle")
            end),
        }
    },    
	
	State
    {
        name = "enter",
        tags = {"idle","canrotate","busy"},

        onenter = function(inst)    
			inst:Show()
            inst.AnimState:PlayAnimation("idle_pre")
        end,
    
        events =
        {
            EventHandler("animover", function(inst, data)
                inst.sg:GoToState("taunt")
            end),
        }
    },
	
	State
    {
        name = "spawn",
        tags = {"busy"},

        onenter = function(inst)    
            inst.AnimState:PlayAnimation("idle_pre") --intentionally wrong animation, the body will send us to the correct state
			inst:Hide()
			if inst.isflying then
				inst.noactions = nil
				inst.taunt2 = false
				inst:DoTaskInTime(10, function(inst) inst.taunt2 = true end)
				SetLand(inst)
			end
        end,
		
		timeline=
        {
            TimeEvent(5*FRAMES, function(inst) 
				if inst.body then
					inst.body.components.roccontrollerp:Spawnbodyparts() 
				else
					local body = SpawnPrefab("roc_bodyp")
					body.Transform:SetPosition(inst:GetPosition():Get())
					body.Transform:SetRotation(inst.Transform:GetRotation())
					inst.body = body
					body.head = inst
					body:PushEvent("land")
					--inst.body.components.roccontrollerp:Spawnbodyparts()					
				end
			end),
        },
    
        events =
        {
            EventHandler("animover", function(inst, data)
                inst.sg:GoToState("taunt")
            end),
        }
    },
	
	State
    {
        name = "exit",
        tags = {"idle","canrotate"},

        onenter = function(inst)    
            inst.AnimState:PlayAnimation("idle_pst")
        end,

        events =
        {
            EventHandler("animover", function(inst, data)
				--should be flying now.
				inst.sg:GoToState("idle")
            end),
        }
    },  
	
	State{
        name = "opengift",
        tags = { "busy", "pausepredict" },

        onenter = function(inst)
            inst.components.locomotor:Stop()
            inst.components.locomotor:Clear()
            inst:ClearBufferedAction()

            --if IsNearDanger(inst) then
                --inst.sg.statemem.isdanger = true
                --inst.sg:GoToState("idle")
                --if inst.components.talker ~= nil then
                   -- inst.components.talker:Say(GetString(inst, "ANNOUNCE_NODANGERGIFT"))
                --end
                --return
           -- end

            inst.SoundEmitter:PlaySound("dontstarve/common/player_receives_gift")
            inst.AnimState:PlayAnimation("taunt", true)
            -- NOTE: the previously used ripping paper anim is called "giift_loop"

            if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:RemotePausePrediction()
                inst.components.playercontroller:EnableMapControls(false)
                inst.components.playercontroller:Enable(false)
            end
            inst.components.inventory:Hide()
            inst:PushEvent("ms_closepopups")
            inst:ShowActions(false)
            inst:ShowPopUp(POPUPS.GIFTITEM, true)

            if inst.components.giftreceiver ~= nil then
                inst.components.giftreceiver:OnStartOpenGift()
            end
        end,

        timeline =
        {
            -- Timing of the gift box opening animation on giftitempopup.lua
            TimeEvent(155 * FRAMES, function(inst)
               -- inst.AnimState:PlayAnimation("gift_open_pre")
                inst.AnimState:PushAnimation("taunt", true)
            end),
        },

        events =
        {
            EventHandler("firedamage", function(inst)
                inst.AnimState:PlayAnimation("taunt")
                inst.sg:GoToState("idle", true)
                if inst.components.talker ~= nil then
                    inst.components.talker:Say(GetString(inst, "ANNOUNCE_NODANGERGIFT"))
                end
            end),
            EventHandler("ms_doneopengift", function(inst, data)
				inst.sg:GoToState("idle", true)
                
            end),
        },

        onexit = function(inst)
            if inst.sg.statemem.isdanger then
                return
            elseif not inst.sg.statemem.isopeningwardrobe then
                if inst.components.playercontroller ~= nil then
                    inst.components.playercontroller:EnableMapControls(true)
                    inst.components.playercontroller:Enable(true)
                end
                inst.components.inventory:Show()
                inst:ShowActions(true)
            end
            inst:ShowPopUp(POPUPS.GIFTITEM, false)
        end,
    },
	
	State{
		name = "action_fast",
        tags = {"busy"},

        onenter = function(inst, cb)
			if inst.isflying then
				inst.sg:GoToState("idle") 
			else
				inst.Physics:Stop()
				inst:PerformBufferedAction()
				inst.AnimState:PlayAnimation("idle_loop", true)
				inst.sg:SetTimeout(0.3)
			end	
        end,

		ontimeout = function(inst)
			inst.sg:GoToState("idle") 
		end,

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State
    {
        name = "action_loop",
        tags = { "doing", "nodangle" },

        onenter = function(inst, timeout)
            inst.sg:SetTimeout(timeout or 1)
            inst.components.locomotor:Stop()
            inst.AnimState:PlayAnimation("taunt", true)
            if inst.bufferedaction ~= nil then
                inst.sg.statemem.action = inst.bufferedaction
                if inst.bufferedaction.action.actionmeter then
                    inst.sg.statemem.actionmeter = true
                    StartActionMeter(inst, timeout or 1)
                end
                if inst.bufferedaction.target ~= nil and inst.bufferedaction.target:IsValid() then
                    inst.bufferedaction.target:PushEvent("startlongaction")
                end
            end
        end,

        timeline =
        {
            TimeEvent(4 * FRAMES, function(inst)
                inst.sg:RemoveStateTag("busy")
            end),
        },

        ontimeout = function(inst)
            inst.AnimState:PlayAnimation("walk_pst")
            if inst.sg.statemem.actionmeter then
                inst.sg.statemem.actionmeter = nil
                StopActionMeter(inst, true)
            end
            inst:PerformBufferedAction()
        end,

        events =
        {
            EventHandler("animqueueover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("idle")
                end
            end),
        },

        onexit = function(inst)
            if inst.sg.statemem.actionmeter then
                StopActionMeter(inst, false)
            end
            if inst.bufferedaction == inst.sg.statemem.action then
                inst:ClearBufferedAction()
            end
        end,
    },
	
    State{
        name = "death",
        tags = {"busy"},

        onenter = function(inst)
			inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/roc/attack_1", "death")
			 --inst.SoundEmitter:SetParameter("death", "intensity", 2)
            inst.AnimState:PlayAnimation("idle_pst")
            inst.Physics:Stop()
            RemovePhysicsColliders(inst)
				
			inst.components.inventory:DropEverything(true)
        end,
		
		timeline=
        {
			TimeEvent(4 * FRAMES, function(inst)
				--local bodypos = inst:GetPosition()
				--inst.components.lootdropper:DropLoot(Vector3(bodypos.x, 20, bodypos.z))
            end),
        },

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
					PlayablePets.DoDeath(inst)
                end
            end),
        },

    },
	
	State {
        name = "sleep",
        tags = { "sleeping", "busy" }, --add tag "busy" if you hate sliding

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pre")
            --if fns ~= nil and fns.onsleep ~= nil then
                --fns.onsleep(inst)
            --end
        end,

        --timeline = timelines ~= nil and timelines.starttimeline or nil,

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("sleeping") end ),
            EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "sleeping",
        tags = { "sleeping", "busy" },

        --onenter = onentersleeping,
		
		onenter = function(inst)
				inst.components.locomotor:StopMoving()
				--if inst.components.sanity.current >= 50 then
				local hungerpercent = inst.components.hunger:GetPercent()
				if hungerpercent ~= nil and hungerpercent ~= 0 then -- We don't want players to heal out starvation.
					inst.components.health:DoDelta(20, false)
				end
				--end
				--inst.components.sanity:DoDelta(1, false)
				--inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/bearger/sleep")
				inst.AnimState:PlayAnimation("sleep_loop")
			end,
			
		onexit = function(inst)
		--inst.components.sanity.dapperness = -0.5
		end,
        --timeline = timelines ~= nil and timelines.sleeptimeline or nil,
		timeline=
        {
			TimeEvent(33*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/hippo/huff_in") end),
        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("sleeping") end ),
			EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "wake",
        tags = { "busy", "waking" },

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pst")
            if inst.components.sleeper ~= nil and inst.components.sleeper:IsAsleep() then
                inst.components.sleeper:WakeUp()
            end
            --if fns ~= nil and fns.onwake ~= nil then
                --fns.onwake(inst)
            --end
        end,

        --timeline = timelines ~= nil and timelines.waketimeline or nil,

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State
    {
        name = "run_start",
        tags = { "moving", "running", "canrotate" },

        onenter = function(inst)
			inst.components.locomotor:RunForward()
            inst.AnimState:PlayAnimation(inst.isflying and "shadow" or "idle_loop")
			
        end,
		
		timeline = 
		{
			--TimeEvent(0*FRAMES, function(inst) inst.Physics:Stop() end ),
		},
		
        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("run") end ),
        },
    },
	
	State
    {
        name = "run",
        tags = { "moving", "running", "canrotate" },

        onenter = function(inst)
			--inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/run")
			inst.components.locomotor:RunForward()
			if inst.isflying then
				if math.random() <= 0.2 then
					inst.flap = true
					inst.AnimState:PlayAnimation("shadow_flap_loop")
				else
					inst.flap = nil
					inst.AnimState:PlayAnimation("shadow")
				end
			else
				inst.AnimState:PlayAnimation("idle_loop")
			end	
        end,
		
		timeline = 
		{
			TimeEvent(16*FRAMES, function(inst) 
				if inst.isflying and inst.flap then
					inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/roc/flap")
				end	
            end),
			
			TimeEvent(1*FRAMES, function(inst) if math.random() < 0.5 and inst.isflying and inst.flap then
                inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/roc/call") end
            end),
		},
		
        events=
			{
				EventHandler("animqueueover", function(inst) 
					inst.sg:GoToState("run")  
				end ),
			},

    },
	
	State
    {
        name = "run_stop",
        tags = { "idle" },

        onenter = function(inst) 
            inst.components.locomotor:StopMoving()
			if inst.isflying then
				inst.sg:GoToState("idle")
			else
				inst.AnimState:PlayAnimation("idle_loop")
			end	
			--inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/step")            
        end,

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end ),
        },
    },
	
	-------------------Forge States------------------------	
	State
    {
        name = "corpse",
        tags = { "busy", "noattack", "nopredict", "nomorph", "nodangle" },

        onenter = function(inst, fromload)
            if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:Enable(false)
            end

            inst:PushEvent("playerdied", { loading = fromload, skeleton = false })

            inst:ShowActions(false)
            inst.components.health:SetInvincible(true)

            if inst.components.locomotor then
				inst.components.locomotor:StopMoving()
			end
			inst.AnimState:PlayAnimation("bash_pre")
			inst.AnimState:PushAnimation("bash_loop", false)
			--inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/roc/attack_1")
        end,
		
		timeline=
		{
			TimeEvent(37*FRAMES, function(inst) 
				inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/bearger/groundpound")
                inst.components.groundpounder:GroundPound()
				inst.components.combat:DoAreaAttack(inst, 3, nil, nil, "strong", GetExcludeTags())
				ShakeIfClose(inst)
            end)
		},

        onexit = function(inst)
            if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:Enable(true)
            end
            inst:ShowActions(true)
            inst.components.health:SetInvincible(false)
        end,
    },
	
	State{
        name = "corpse_rebirth",
        tags = { "busy", "noattack", "nopredict", "nomorph" },

        onenter = function(inst)
            if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:RemotePausePrediction()
                inst.components.playercontroller:Enable(false)
            end

			inst.sg:SetTimeout(107 * FRAMES)
			
			inst.Physics:CollidesWith(COLLISION.WORLD)

            inst.components.health:SetInvincible(true)
            inst:ShowActions(false)
            inst:SetCameraDistance(14)
        end,

        timeline =
        {
            TimeEvent(53 * FRAMES, function(inst)
                inst.components.bloomer:PushBloom("corpse_rebirth", "shaders/anim.ksh", -2)
                inst.sg.statemem.fadeintime = (86 - 53) * FRAMES
                inst.sg.statemem.fadetime = 0
            end),
            TimeEvent(86 * FRAMES, function(inst)
                inst.sg.statemem.physicsrestored = true
                inst.Physics:ClearCollisionMask()
                inst.Physics:CollidesWith(COLLISION.WORLD)

                
                if inst.sg.statemem.fade ~= nil then
                    inst.sg.statemem.fadeouttime = 20 * FRAMES
                    inst.sg.statemem.fadetotal = inst.sg.statemem.fade
                end
                inst.sg.statemem.fadeintime = nil
            end),
			TimeEvent((96) * FRAMES, function(inst)
				local pos = inst:GetPosition()
				--local fire = SpawnPrefab("lavaarena_portal_player_fx")
				--fire.Transform:SetPosition(pos.x, 0, pos.z)
				--if inst:HasTag("largecreature") or inst:HasTag("epic") then --totally not for PP I swear! --Fid: Hmmmm are you sure?
					--fire.Transform:SetScale(3, 3, 3)
				--end
            end),
            TimeEvent((86 + 20) * FRAMES, function(inst)
				inst:Hide()
                inst.components.bloomer:PopBloom("corpse_rebirth")
            end),
        },
		
		
        onupdate = function(inst, dt)
            if inst.sg.statemem.fadeouttime ~= nil then
                inst.sg.statemem.fade = math.max(0, inst.sg.statemem.fade - inst.sg.statemem.fadetotal * dt / inst.sg.statemem.fadeouttime)
                if inst.sg.statemem.fade > 0 then
                    inst.components.colouradder:PushColour("corpse_rebirth", inst.sg.statemem.fade, inst.sg.statemem.fade, inst.sg.statemem.fade, 0)
                else
                    inst.components.colouradder:PopColour("corpse_rebirth")
                    inst.sg.statemem.fadeouttime = nil
                end
            elseif inst.sg.statemem.fadeintime ~= nil then
                local k = 1 - inst.sg.statemem.fadetime / inst.sg.statemem.fadeintime
                inst.sg.statemem.fade = .8 * (1 - k * k)
                inst.components.colouradder:PushColour("corpse_rebirth", inst.sg.statemem.fade, inst.sg.statemem.fade, inst.sg.statemem.fade, 0)
                inst.sg.statemem.fadetime = inst.sg.statemem.fadetime + dt
            end
        end,
		
		ontimeout = function(inst)
			inst.components.bloomer:PopBloom("corpse_rebirth")
            inst.sg:GoToState("corpse_taunt")
        end,
		
        onexit = function(inst)
            inst:ShowActions(true)
            inst:SetCameraDistance()
			inst:Show()
            if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:Enable(true)
            end
            inst.components.health:SetInvincible(false)

            inst.Physics:ClearCollisionMask()
            inst.Physics:CollidesWith(COLLISION.WORLD)

            SerializeUserSession(inst)
        end,
    },
	
	State{
        name = "corpse_taunt",
        tags = { "busy", "noattack", "nopredict", "nomorph" },

        onenter = function(inst)
            if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:RemotePausePrediction()
                inst.components.playercontroller:Enable(false)
            end
			
			inst.Physics:CollidesWith(COLLISION.WORLD)

			inst.AnimState:PlayAnimation("bash_pst")
			
            inst.components.health:SetInvincible(true)
            inst:ShowActions(false)
            inst:SetCameraDistance(14)
			if inst.body then
				inst.body.components.roccontrollerp.leg1:RemoveTag("notarget")
				inst.body.components.roccontrollerp.leg2:RemoveTag("notarget")
			end
        end,

		timeline=
		{
		
		},

        events =
        {
            EventHandler("animqueueover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("idle")
                end
            end),
        },

        onexit = function(inst)
            inst:ShowActions(true)
            inst:SetCameraDistance()
            if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:Enable(true)
            end
            inst.components.health:SetInvincible(false)

            inst.Physics:ClearCollisionMask()
            inst.Physics:CollidesWith(COLLISION.WORLD)

            SerializeUserSession(inst)
        end,
    },
	
	State
    {
        name = "revivecorpse",

        onenter = function(inst)
            --inst.components.talker:Say(GetString(inst, "ANNOUNCE_REVIVING_CORPSE"))
            local buffaction = inst:GetBufferedAction()
            local target = buffaction ~= nil and buffaction.target or nil
            inst.sg:GoToState("action_loop",
                TUNING.REVIVE_CORPSE_ACTION_TIME *
                (inst.components.corpsereviver ~= nil and inst.components.corpsereviver:GetReviverSpeedMult() or 1) *
                (target ~= nil and target.components.revivablecorpse ~= nil and target.components.revivablecorpse:GetReviveSpeedMult() or 1)
            )
        end,
    },
	
	State
    {
        name = "land",
        tags = {"busy" },

        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("ground_pre")    
        end,
        
        
        timeline=
        {            
            TimeEvent(30*FRAMES, function(inst) inst.components.roccontrollerp:Spawnbodyparts() end),
            TimeEvent(5*FRAMES, function(inst) 
                inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/roc/flap","flaps")
                --inst.SoundEmitter:SetParameter("flaps", "intensity")
            end),
            TimeEvent(17*FRAMES, function(inst) 
                inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/roc/flap","flaps")
                --inst.SoundEmitter:SetParameter("flaps", "intensity")
            end),
        },
        
        events =
        {
            EventHandler("animover", function(inst, data)
                inst.sg:GoToState("idle")
            end),
        }
    },


    State
    {
        name = "takeoff",
        tags = {"busy" },

        onenter = function(inst)
            
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("ground_pst")    
        end,

        timeline=
        {            
            TimeEvent(15*FRAMES, function(inst) inst.components.locomotor:RunForward() end),
        },
        

        events =
        {
            EventHandler("animover", function(inst, data)
                inst.sg:GoToState("fly")
            end),
        }
    },


    State
    {
        name = "fly",
        tags = {"moving","canrotate"},

        onenter = function(inst)
            --inst.components.locomotor:RunForward()
            inst.sg:SetTimeout(1+2*math.random())
            inst.AnimState:PlayAnimation("shadow")      
        end,
        
        onupdate = function(inst)
           
        end,

        ontimeout=function(inst)
            inst.sg:GoToState("flap")
        end,
    },

    State
    {
        name = "flap",
        tags = {"moving","canrotate"},

        onenter = function(inst)
			inst.components.locomotor:RunForward()
            inst.AnimState:PlayAnimation("shadow_flap_loop")      
        end,

    timeline=
        {
            TimeEvent(16*FRAMES, function(inst) 
                inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/roc/flap","flaps")
                --inst.SoundEmitter:SetParameter("flaps", "intensity")
            end),
            
            TimeEvent(1*FRAMES, function(inst) if math.random() < 0.5 then
                inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/roc/call","calls") end
                --inst.SoundEmitter:SetParameter("calls", "intensity")
            end),
        },
        onupdate = function(inst)
           
        end,

        events=
        {
            EventHandler("animover", function(inst) 
                if not inst.flap then
                    inst.sg:GoToState("flap")
                    inst.flap = true
                else    
                    inst.sg:GoToState("fly")
                    inst.flap = nil
                end

            end),
        },
    },

	State
    {
        name = "special_atk2",
        tags = {"busy"},

        onenter = function(inst)    
            
			if inst.body and (CheckWaterTile(inst.body) == false or TheWorld.Map:IsAboveGroundAtPoint(inst.body:GetPosition():Get())) then
				inst.AnimState:PlayAnimation("idle_pst")
				inst.noactions = true
				inst.sg.statemem.takeoff_pos = inst.body:GetPosition()
				inst.sg.statemem.takeoff_rotation = inst.body.Transform:GetRotation()
				inst.body.components.roccontrollerp:doliftoff()
				inst.body = nil
			elseif inst.isflying and (CheckWaterTile(inst.body) == false or TheWorld.Map:IsAboveGroundAtPoint(inst.body:GetPosition():Get())) then
				inst.sg:GoToState("spawn")
			
			else
				inst.sg:GoToState("idle")					
			end
        end,

        events =
        {
            EventHandler("animover", function(inst, data)
				--should be flying now.
				SetFlying(inst)
				inst.sg:GoToState("idle")
            end),
        }
    },  
	
}

--CommonStates.AddFrozenStates(states)
PP_CommonStates.AddJumpInStates(states, nil, "idle_loop")
PP_CommonStates.AddOpenGiftStates(states, "idle_loop")
--PP_CommonStates.AddSailStates(states, {}, "idle_loop", "idle_loop")
PP_CommonStates.AddActionStates(states, 
	{
		TimeEvent(0 * FRAMES, function(inst)
            inst:PerformBufferedAction()
        end),
	}, 
	"idle_loop", nil, nil, "grab", "idle_loop") --(states, timelines, anim, anim2, enteranim, loopanim, exitanim, noanim, events)
local simpleanim = "idle_loop"
local simpleidle = "idle_loop"
local simplemove = "walk"
if CurrentRelease.GreaterOrEqualTo(ReleaseID.IDs.R08_ROT_TURNOFTIDES) then
CommonStates.AddHopStates(states, false, {pre = simpleidle, loop = simpleidle, pst = simpleidle}, nil, "death")
end
PP_CommonStates.AddSailorStates(states, nil, 
{
	plank_idle = simpleanim,
	plank_idle_loop = simpleidle,
	plank_idle_pst = simpleanim,
	
	plank_hop_pre = simpleidle,
	plank_hop = simpleidle,
	
	steer_pre = simpleanim,
	steer_idle = simpleidle,
	steer_turning = simpleanim,
	stop_steering = simpleanim,
	
	row = simpleanim,
}
)
    
return StateGraph("rocp", states, events, "spawn", actionhandlers)

