require("stategraphs/commonstates")

local pu = require ("prefabs/pugaliskp_util")

local actionhandlers = 
{
    ActionHandler(ACTIONS.GOHOME, "action"),
	ActionHandler(ACTIONS.ATTACK, "attack"),	
}

local events=
{
    EventHandler("death", function(inst, data)		
		if inst:HasTag("tail") then
            inst.sg:GoToState("tail_exit")
        end
    end),
    EventHandler("doattack", function(inst, data) if not inst.components.health:IsDead() and (inst.sg:HasStateTag("hit") or not inst.sg:HasStateTag("busy")) then inst.sg:GoToState("attack", data.target) end end),
}


 local states=
{

     State{
        name = "idle",
        tags = {"idle", "canrotate"},
        
        onenter = function(inst, start_anim)
            inst.Physics:Stop()
            if inst:HasTag("tail") then
                if start_anim then
                    inst.AnimState:PlayAnimation(start_anim)
                    inst.AnimState:PushAnimation("tail_idle_loop", true)
                else
                    inst.AnimState:PlayAnimation("tail_idle_loop", true)
                end
            else
                if start_anim then
                    inst.AnimState:PlayAnimation(start_anim)
                    inst.AnimState:PushAnimation("head_idle_loop", true)
                else
                    inst.AnimState:PlayAnimation("head_idle_loop", true)
                end
            end
        end,

        onupdate = function(inst)

            if not inst:HasTag("tail") then
                
                if inst.wantstogaze then
                    inst.sg:GoToState("gaze")
                elseif inst.wantstotaunt then
                    inst.sg:GoToState("toung")
                end  

                if inst.wantstopremove then
                    inst.wantstopremove = nil
                    inst:PushEvent("premove")
                end
            end

            if inst:HasTag("tail") and inst:HasTag("should_exit") then
                inst.sg:GoToState("tail_exit")
            end
        end,        
    

        events=
        {
            EventHandler("animover", function(inst)
                inst.sg:GoToState("idle")  
            end),
        },        
    },   

    State{
        name = "dirt_collapse",
        tags = {"busy"},

        onenter = function(inst, start_anim)
            inst.Physics:Stop()            
            inst.AnimState:PlayAnimation("dirt_collapse", false)            
        end,

        events=
        {
            EventHandler("animover", function(inst)
                inst:Remove()  
            end),
        },    
    },

    State{
        name = "tail_exit",
        tags = {"busy"},

        onenter = function(inst, start_anim)
            inst.Physics:Stop()            
            inst.AnimState:PlayAnimation("tail_idle_pst")            
        end,

        events=
        {
            EventHandler("animover", function(inst)
                inst.sg:GoToState("dirt_collapse")
            end),
        },    
    },

    State{
        name = "tail_ready",
        tags = {"busy"},

        onenter = function(inst, start_anim)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("tail_idle_pre")
        end,

        events=
        {
            EventHandler("animover", function(inst)
                inst.sg:GoToState("idle")                
            end),
        },    
    },
    
    State{
        name = "hole",
        tags = {"busy"},

        onenter = function(inst, start_anim)
            inst.AnimState:SetBank("giant_snake")
            inst.AnimState:SetBuild("python_test")

            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("dirt_static")
        end,  
    },   

    State{
        name = "attack",
        tags = {"attack", "canrotate", "busy",},
        
        onenter = function(inst, target)
            inst.components.combat:StartAttack()
            if inst:HasTag("tail") then
                inst.AnimState:PlayAnimation("tail_smack")
            else
                inst.AnimState:PlayAnimation("atk")
            end
            inst.sg.statemem.target = target
        end,
        
        timeline =
        {
            TimeEvent(17*FRAMES, function(inst) inst.components.combat:DoAttack(inst.sg.statemem.target) end),
            TimeEvent(3*FRAMES, function(inst)  if not inst:HasTag("tail") then inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/pugalisk/attack") end end),
            TimeEvent(6*FRAMES, function(inst)  if not inst:HasTag("tail") then inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/pugalisk/attack_pre") end end),
            TimeEvent(18*FRAMES, function(inst)  if not inst:HasTag("tail") then inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/pugalisk/bite") end end),
            TimeEvent(7*FRAMES, function(inst)  if inst:HasTag("tail") then inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/pugalisk/tail_attack") end end),
            --if inst:HasTag("tail") then inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/pugalisk/attack") end
        },
        
        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
    State{
        name = "death",
        tags = {"busy"},

        onenter = function(inst)
			if inst:HasTag("tail") then
                inst.AnimState:PlayAnimation("tail_idle_pst")
                inst.AnimState:PushAnimation("dirt_collapse_slow", false)
                
            else
                inst.AnimState:PlayAnimation("death")
            end
            inst.Physics:Stop()
            RemovePhysicsColliders(inst)     
        end,
		
		timeline=
        {
			--TimeEvent(20*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/pugalisk/death") end),
        },

        events =
        {
            EventHandler("animover", function(inst)
                inst:Remove()
            end),
        },

    },
	
}

--CommonStates.AddFrozenStates(states)
    
return StateGraph("pugaliskp_tail", states, events, "idle", actionhandlers)

