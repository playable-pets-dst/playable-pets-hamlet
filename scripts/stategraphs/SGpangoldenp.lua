require("stategraphs/commonstates")

local function GetExcludeTags(inst)
	if TheNet:GetPVPEnabled() then
		return {"INLIMBO", "playerghost", "notarget", "shadow", "groundpoundimmune"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion", "playerghost", "INLIMBO", "groundpoundimmune"}
	else	
		return {"player", "companion", "INLIMBO", "playerghost", "notarget", "shadow", "groundpoundimmune"}
	end
end

local function ShakeIfClose(inst)
    ShakeAllCameras(CAMERASHAKE.VERTICAL, .15, .02, .5, inst, 20)
end

local longaction = "action"
local shortaction = "action"
local workaction = "attack"
local otheraction = "action"

local actionhandlers = PP_CommonStates.GetCommonActions(longaction, shortaction, workaction, otheraction)

local actionhandler_overrides = 
{ 

}

if #actionhandler_overrides > 0 then
	for i, v in ipairs(actionhandler_overrides) do
		table.insert(actionhandlers, v)
	end
end

local events=
{
	EventHandler("attacked", function(inst, data) 
		if data.stimuli and data.stimuli == "strong" and not (inst.sg:HasStateTag("hiding") or inst.sg:HasStateTag("nointerrupt")) and not inst.components.health:IsDead() then
			inst.sg:GoToState("hit")
		elseif data.stimuli and (data.stimuli == "electric" or data.stimuli == "explosive") and not inst.components.health:IsDead() then
			inst.sg:GoToState("hit", data.stimuli)
		elseif not inst.components.health:IsDead() and not inst.sg:HasStateTag("busy") and not inst.sg:HasStateTag("hiding") then 
			inst.sg:GoToState("hit") 
		elseif not inst.components.health:IsDead() and (inst.sg:HasStateTag("hiding") and inst.sg:HasStateTag("busy")) then
			if TheNet:GetServerGameMode() == "lavaarena" then
				--don't want the timeout timer to keep resetting on hit, so mimic the hit state without actually leaving the shield state.
				inst.AnimState:PlayAnimation("ball_hit", false)
				inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/pangolden/ball_hit")
			else
				inst.sg:GoToState("hide_hit")
			end
		end 
	end),
	PP_CommonHandlers.AddCommonHandlers(),
    EventHandler("death", function(inst, data)
        if inst.sleepingbag ~= nil and (inst.sg:HasStateTag("bedroll") or inst.sg:HasStateTag("tent")) then -- wakeup on death to "consume" sleeping bag first
            inst.sleepingbag.components.sleepingbag:DoWakeUp()
            inst.sleepingbag = nil
        end

        if (data ~= nil and data.cause == "file_load") or inst.components.revivablecorpse ~= nil then
            inst.sg:GoToState("corpse", true)
        else
            inst.sg:GoToState("death")
        end
    end),
    EventHandler("doattack", function(inst, data) if not inst.components.health:IsDead() and (inst.sg:HasStateTag("hit") or not inst.sg:HasStateTag("busy")) then inst.sg:GoToState("attack", data.target) end end),
    CommonHandlers.OnSleep(),
   -- CommonHandlers.OnLocomote(true,false),
	EventHandler("locomote", function(inst, data)
        if inst.sg:HasStateTag("busy") then
            return
        end
        local is_moving = inst.sg:HasStateTag("moving")
        local should_move = inst.components.locomotor:WantsToMoveForward()

        if inst.sg:HasStateTag("bedroll") or inst.sg:HasStateTag("tent") or inst.sg:HasStateTag("waking") then -- wakeup on locomote
            if inst.sleepingbag ~= nil and inst.sg:HasStateTag("sleeping") then
                inst.sleepingbag.components.sleepingbag:DoWakeUp()
                inst.sleepingbag = nil
				inst:Show()
				--inst.AnimState:PushAnimation("pig_pickup")
				SetSleeperAwakeState(inst)
				--inst.AnimState:PushAnimation("pig_pickup")
				--inst.sg:GoToState("idle", true)
				
            end 
		elseif is_moving and not should_move then
			if inst.shouldwalk and inst.shouldwalk == true then
				inst.sg:GoToState("walk_stop")
			else
				inst.sg:GoToState("run_stop")
			end
        elseif not is_moving and should_move then
			if inst.shouldwalk and inst.shouldwalk == true then
				inst.sg:GoToState("walk_start")
			else
				inst.sg:GoToState("run_start")
			end
        elseif data.force_idle_state and not (is_moving or should_move or inst.sg:HasStateTag("idle_loop")) then
            inst.sg:GoToState("idle")
        end
    end),
    CommonHandlers.OnFreeze(),
	
	 EventHandler("ms_opengift",
        function(inst)
            if not inst.sg:HasStateTag("busy") then
                inst.sg:GoToState("opengift")
            end
        end),
		
	EventHandler("respawnfromghost", function(inst)  
			if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:Enable(true)
            end

            inst.components.health:SetInvincible(false)
            inst:ShowHUD(true)
            inst:SetCameraDistance()

            SerializeUserSession(inst) end),

	EventHandler("emote", function(inst, data)
        if not (inst.sg:HasStateTag("busy") or
                inst.sg:HasStateTag("nopredict") or
                inst.sg:HasStateTag("sleeping"))
            and not inst.components.inventory:IsHeavyLifting()
            and (data.mounted or not inst.components.rider:IsRiding())
            and (data.beaver or not inst:HasTag("beaver"))
            and (not data.requires_validation or TheInventory:CheckClientOwnership(inst.userid, data.item_type)) and data.anim and data.anim == "emoteXL_strikepose" then
            inst.sg:GoToState("preen")
        end
    end),
}

if CurrentRelease.GreaterOrEqualTo(ReleaseID.IDs.R08_ROT_TURNOFTIDES) then
	table.insert(events, PP_CommonHandlers.OnSink())
	table.insert(events, CommonHandlers.OnHop())
end

 local states=
{

	State{
        name = "idle",
        tags = {"idle", "canrotate"},
        
        onenter = function(inst, pushanim)
          
            inst.components.locomotor:StopMoving()
           
            if math.random() < 0.2 then
                if math.random() < 0.5 then
                    inst.sg:GoToState("shake") 
                else           
                    inst.sg:GoToState("preen")
                end
            else
                inst.AnimState:PlayAnimation("idle_loop", true)
                inst.sg:SetTimeout(2 + 2*math.random())
            end

        end,
        
        ontimeout=function(inst)
               
            inst.sg:GoToState("idle")

        end,
    },

    State{
        name = "shake",
        tags = {"idle"},
        
        onenter = function(inst)
            inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation("shake")
        end,

       timeline=
        {
            TimeEvent(17*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/pangolden/shake") end),
            TimeEvent(20*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/pangolden/movement") end),
            TimeEvent(24*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/pangolden/movement") end),
            TimeEvent(29*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/pangolden/movement") end),
            TimeEvent(37*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/pangolden/movement") end),
        },

        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },

    State{
        name = "preen",
        tags = {"idle"},
        
        onenter = function(inst)
            inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation("preen_pre")
            for i=1, math.random(1,3)  do 
                inst.AnimState:PushAnimation("preen_loop",false)
            end            
        end,
        
        timeline=
        {
            TimeEvent(5*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/movement/bodyfall_dirt",nil,.5) end), 
            TimeEvent(6*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/pangolden/movement") end),
            TimeEvent(15*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/pangolden/lick") end),
            TimeEvent(36*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/pangolden/lick") end),
            TimeEvent(57*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/pangolden/lick") end),
            -- TimeEvent(11*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/pangolden/movement") end),
            -- TimeEvent(11*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/pangolden/movement") end),
        }, 
        
        events=
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("preen_pst") end),
        },
    }, 

    State{
        name = "preen_pst",
        tags = {"idle"},
        
        onenter = function(inst)
            inst.AnimState:PlayAnimation("preen_pst")
        end,
        
        timeline=
        {
            TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/pangolden/lick") end),
            TimeEvent(11*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/pangolden/movement") end),
        }, 
        
        events=
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end),
        },
    },    


    State{
        name = "hide_pre",
        tags = {"ball", "busy"},
        
        onenter = function(inst, target)                
            inst.components.health:SetAbsorptionAmount(TheNet:GetServerGameMode() == "lavaarena" and 1 or 0.8)
            inst.components.locomotor:StopMoving()            
            inst.AnimState:PlayAnimation("ball_pre")           
        end,
         
        timeline=
        {
            TimeEvent(3*FRAMES, function(inst)                
                inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/pangolden/movement","scales") 
                inst.SoundEmitter:SetParameter("scales", "intensity", .01)  
            end),
            TimeEvent(6*FRAMES, function(inst) 
                inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/pangolden/movement","scales")
                inst.SoundEmitter:SetParameter("scales", "intensity", .33)  
            end),
            TimeEvent(9*FRAMES, function(inst) 
                inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/pangolden/movement","scales")
                inst.SoundEmitter:SetParameter("scales", "intensity", .66)  
            end),
             TimeEvent(20*FRAMES, function(inst) 
                inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/pangolden/ball_hit")
                inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/pangolden/walk")
             end),
             --TimeEvent(30*FRAMES, function(inst) inst:PerformBufferedAction() end),   
        },

        onexit = function(inst)
			if TheNet:GetServerGameMode() == "lavaarena" then
				inst.canhide_ham = false
				inst:DoTaskInTime(PPHAM_FORGE.PANGOLDEN.SPECIAL_CD, function(inst) inst.canhide_ham = true end)
			end
            inst.components.health:SetAbsorptionAmount(0)
        end,        
        

        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("ball") end),
        },
    }, 

    State{
        name = "ball",
        tags = {"hiding", "busy"},
        
        onenter = function(inst, target)                
            inst.components.health:SetAbsorptionAmount(TheNet:GetServerGameMode() == "lavaarena" and 1 or 0.8)
            inst.components.locomotor:StopMoving()            
            inst.AnimState:PlayAnimation("ball_idle",true)
			if TheNet:GetServerGameMode() == "lavaarena" then
				inst.sg:SetTimeout(10)
			end	
        end,
        
        onexit = function(inst)                
            inst.components.health:SetAbsorptionAmount(0)
        end,        
        
        ontimeout=function(inst)
            inst.sg:GoToState("hide_pst")
        end,

        timeline=
        {
         --   TimeEvent(15*FRAMES, function(inst) inst.components.combat:DoAttack(inst.sg.statemem.target) end),
        },        
    },    

    State{
        name = "hide_pst",
        tags = {"ball", "busy"},
        
        onenter = function(inst, target)                
            inst.AnimState:PlayAnimation("ball_pst")
        end,        

        timeline=
        {
            TimeEvent(11*FRAMES, function(inst)                
                inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/pangolden/movement","scales") 
                inst.SoundEmitter:SetParameter("scales", "intensity", .88)  
            end),

            TimeEvent(14*FRAMES, function(inst) 
                inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/pangolden/movement","scales")
                inst.SoundEmitter:SetParameter("scales", "intensity", .44)  
            end),

            TimeEvent(17*FRAMES, function(inst) 
                inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/pangolden/movement","scales")
                inst.SoundEmitter:SetParameter("scales", "intensity", .22)  
            end),

            TimeEvent(20*FRAMES, function(inst) 
                inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/pangolden/walk")  
             end),

            TimeEvent(21*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/pangolden/walk") end),
            
            TimeEvent(24*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/pangolden/movement") end),

        },

        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },      
    },


    State{
        name = "hide_hit",
        tags = {"ball", "busy"},
        
        onenter = function(inst, target)    
            inst.components.health:SetAbsorptionAmount(TheNet:GetServerGameMode() == "lavaarena" and 1 or 0.8)            
            inst.components.locomotor:StopMoving()            
            inst.AnimState:PlayAnimation("ball_hit")
            inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/pangolden/ball_hit")
        end,

        onexit = function(inst)                
            inst.components.health:SetAbsorptionAmount(0)
        end,   

        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("ball") end),
        },        
    },         

    State{
        name = "attack",
        tags = {"attack", "busy"},
        
        onenter = function(inst, target)    
			inst.sg.statemem.target = target
            inst.components.combat:StartAttack()
            inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation("atk", false)
        end,
        
        
        timeline=
        {
            TimeEvent(17*FRAMES, function(inst) inst:PerformBufferedAction() end),
        },
        
        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State{
        name = "action",
        tags = {"attack", "busy"},

        onenter = function(inst, target)
            inst.Physics:Stop()
            inst.components.combat:StartAttack()
            inst.AnimState:PlayAnimation("walk_pst")
        end,

        timeline=
        {

			TimeEvent(0 * FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/platapine_bill/attack") end),
            TimeEvent(4*FRAMES, function(inst) inst:PerformBufferedAction() end),
        },

        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State{
        name= "special_atk1",
        tags = {"idle", "busy"},
        
        onenter = function(inst)
			inst.Physics:Stop()
			if not inst.shouldwalk or inst.shouldwalk == false then
				inst.shouldwalk = true
			else
				inst.shouldwalk = false
			end
            --inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/city_pig/scream")
            inst.AnimState:PlayAnimation("walk_pst")
        end,
        
        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end ),
        },        
    },
	
	State{
        name = "drink_pre",
        tags = {"busy"},
        
        onenter = function(inst)         
            inst.Physics:Stop()           
            inst.AnimState:PlayAnimation("drink_pre")
        end,        

        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("drink_loop") end),
        },        
    },

     State{
        name = "drink_loop",
        tags = {"busy"},
        
        onenter = function(inst)
            inst.Physics:Stop()           
            inst.AnimState:PlayAnimation("drink_loop",true)
            inst.sg:SetTimeout(1)
           inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/pangolden/mouth")
           inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/pangolden/suck")         
        end,        

        timeline=
        {
            TimeEvent(12*FRAMES, function(inst) inst.components.health:DoDelta(20, false) inst.components.hunger:DoDelta(20, false) end),
        },

        ontimeout=function(inst)              
           inst.sg:GoToState("idle","drink_pst")
        end,
    },        

    State{
        name = "special_atk3",
        tags = {"busy"},
        
        onenter = function(inst)         
            inst.Physics:Stop()
			inst.sg.mem.wants_to_poop = nil
            inst.AnimState:PlayAnimation("poop")
            inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/pangolden/poop")
        end,        

        timeline=
        {
            TimeEvent(6*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/pangolden/movement") end),
            TimeEvent(9*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/pangolden/movement") end),
            TimeEvent(13*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/pangolden/movement") end),
            TimeEvent(18*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/pangolden/movement") end),
            TimeEvent(22*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/pangolden/movement") end),
            TimeEvent(25*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/pangolden/movement") end),
            TimeEvent(29*FRAMES, function(inst) 
                                inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/pangolden/fart") 
                                if inst.canpoop then
                                    SpawnPrefab("goldnugget").Transform:SetPosition(inst:GetPosition():Get())
                                    inst.canpoop = false
                                    if inst.canpoop_task then
                                        inst.canpoop_task:Cancel()
                                        inst.canpoop_task = nil
                                    end
                                    inst.canpoop_task = inst:DoTaskInTime(480/4, function(inst) inst.canpoop = true end)
                                end
                            end),
            TimeEvent(45*FRAMES, PlayFootstep ),             
            TimeEvent(43*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/pangolden/movement") end),
            TimeEvent(56*FRAMES, function(inst)inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/pangolden/step") end),
            TimeEvent(58*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/pangolden/movement") end),
            TimeEvent(59*FRAMES, PlayFootstep ), 
            TimeEvent(67*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/pangolden/movement") end),
            TimeEvent(68*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/pangolden/step") end),
            TimeEvent(70*FRAMES, PlayFootstep ),
            TimeEvent(68*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/pangolden/step") end),
            TimeEvent(81*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/pangolden/movement") end),
            
        },

        onexit = function(inst)
            
        end,
		
		events=
        {
            EventHandler("animover", function(inst)
                inst.sg:GoToState("idle") 
            end),
        },        
    },

    State{
        name = "eat",
        tags = {"busy"},
        
        onenter = function(inst)      
            inst.Physics:Stop()           
            inst.AnimState:PlayAnimation("drink_pre")
            inst.AnimState:PushAnimation("drink_loop",false)
			inst.AnimState:PushAnimation("drink_loop",false)
			inst.AnimState:PushAnimation("drink_pst",false)
        end,        

        timeline=
        {
            TimeEvent(12*FRAMES, function(inst) 
				inst.components.health:DoDelta(30, false)   
				inst.components.hunger:DoDelta(20, false)
			end),
			TimeEvent(40*FRAMES, function(inst) 
				inst.components.health:DoDelta(30, false)   
				inst.components.hunger:DoDelta(20, false)
			end), 
			
        },

        events=
        {
            EventHandler("animqueueover", function(inst)
                inst.sg:GoToState("idle","drink_pst") 
            end),
        },        
    },
	
	State{
        name = "opengift",
        tags = { "busy", "pausepredict" },

        onenter = function(inst)
            inst.components.locomotor:Stop()
            inst.components.locomotor:Clear()
            inst:ClearBufferedAction()

            inst.SoundEmitter:PlaySound("dontstarve/common/player_receives_gift")
            inst.AnimState:PlayAnimation("idle_loop", true)
            -- NOTE: the previously used ripping paper anim is called "giift_loop"

            if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:RemotePausePrediction()
                inst.components.playercontroller:EnableMapControls(false)
                inst.components.playercontroller:Enable(false)
            end
            inst.components.inventory:Hide()
            inst:PushEvent("ms_closepopups")
            inst:ShowActions(false)
            inst:ShowPopUp(POPUPS.GIFTITEM, true)

            if inst.components.giftreceiver ~= nil then
                inst.components.giftreceiver:OnStartOpenGift()
            end
        end,

        timeline =
        {
            -- Timing of the gift box opening animation on giftitempopup.lua
            TimeEvent(155 * FRAMES, function(inst)
               -- inst.AnimState:PlayAnimation("gift_open_pre")
                inst.AnimState:PushAnimation("idle_loop", true)
            end),
        },

        events =
        {
            EventHandler("firedamage", function(inst)
                inst.AnimState:PlayAnimation("idle_loop")
                inst.sg:GoToState("idle", true)
                if inst.components.talker ~= nil then
                    inst.components.talker:Say(GetString(inst, "ANNOUNCE_NODANGERGIFT"))
                end
            end),
            EventHandler("ms_doneopengift", function(inst, data)
				inst.sg:GoToState("idle", true)
                
            end),
        },

        onexit = function(inst)
            if inst.sg.statemem.isdanger then
                return
            elseif not inst.sg.statemem.isopeningwardrobe then
                if inst.components.playercontroller ~= nil then
                    inst.components.playercontroller:EnableMapControls(true)
                    inst.components.playercontroller:Enable(true)
                end
                inst.components.inventory:Show()
                inst:ShowActions(true)
            end
            inst:ShowPopUp(POPUPS.GIFTITEM, false)
        end,
    },
	
	State{
		name = "hit",
        tags = {"busy", "hit"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("hit")
			inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/pangolden/hit")
        end,

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State{
		name = "action",
        tags = {"busy"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
			inst:PerformBufferedAction()
            inst.AnimState:PlayAnimation("run_pst")
        end,

		timeline=
        {

        },

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState(inst.sg.mem.wants_to_poop and "poop" or "idle") end),
        },
    },
	
	State
    {
        name = "action_loop",
        tags = { "doing", "nodangle" },

        onenter = function(inst, timeout)
            inst.sg:SetTimeout(timeout or 1)
            inst.components.locomotor:Stop()
            inst.AnimState:PlayAnimation("drink_loop", true)
            if inst.bufferedaction ~= nil then
                inst.sg.statemem.action = inst.bufferedaction
                if inst.bufferedaction.action.actionmeter then
                    inst.sg.statemem.actionmeter = true
                    StartActionMeter(inst, timeout or 1)
                end
                if inst.bufferedaction.target ~= nil and inst.bufferedaction.target:IsValid() then
                    inst.bufferedaction.target:PushEvent("startlongaction")
                end
            end
        end,

        timeline =
        {
            TimeEvent(4 * FRAMES, function(inst)
                inst.sg:RemoveStateTag("busy")
            end),
        },

        ontimeout = function(inst)
            inst.AnimState:PlayAnimation("drink_pst")
            if inst.sg.statemem.actionmeter then
                inst.sg.statemem.actionmeter = nil
                StopActionMeter(inst, true)
            end
            inst:PerformBufferedAction()
        end,

        events =
        {
            EventHandler("animqueueover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("idle")
                end
            end),
        },

        onexit = function(inst)
            if inst.sg.statemem.actionmeter then
                StopActionMeter(inst, false)
            end
            if inst.bufferedaction == inst.sg.statemem.action then
                inst:ClearBufferedAction()
            end
        end,
    },
	
    State{
        name = "death",
        tags = {"busy"},

        onenter = function(inst)
			inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/pangolden/death")
            inst.AnimState:PlayAnimation("death")
            inst.Physics:Stop()
            RemovePhysicsColliders(inst)     
			inst.components.lootdropper:DropLoot(Vector3(inst.Transform:GetWorldPosition()))
			inst.components.inventory:DropEverything(true)
        end,
		
		timeline=
        {

        },

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
					PlayablePets.DoDeath(inst)
                end
            end),
        },

    },
	
	State {
        name = "sleep",
        tags = { "sleeping", "busy" }, --add tag "busy" if you hate sliding

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pre")
            --if fns ~= nil and fns.onsleep ~= nil then
                --fns.onsleep(inst)
            --end
        end,

        timeline = 
		{
			TimeEvent(11*FRAMES, function(inst)inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/pangolden/yawn") end),
		},

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("sleeping") end ),
            EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "sleeping",
        tags = { "sleeping", "busy" },

        --onenter = onentersleeping,
		
		onenter = function(inst)
				inst.components.locomotor:StopMoving()
				PlayablePets.SleepHeal(inst)
				inst.AnimState:PlayAnimation("sleep_loop")
			end,
			
		onexit = function(inst)
		--inst.components.sanity.dapperness = -0.5
		end,
        --timeline = timelines ~= nil and timelines.sleeptimeline or nil,
		timeline=
        {
			TimeEvent(1*FRAMES, function(inst)inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/pangolden/sleep") end),
        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("sleeping") end ),
			EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "wake",
        tags = { "busy", "waking" },

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pst")
            if inst.components.sleeper ~= nil and inst.components.sleeper:IsAsleep() then
                inst.components.sleeper:WakeUp()
            end
            --if fns ~= nil and fns.onwake ~= nil then
                --fns.onwake(inst)
            --end
        end,

        timeline = 
		{
			TimeEvent(22*FRAMES, function(inst)inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/pangolden/movement") end),
			TimeEvent(28*FRAMES, function(inst)inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/pangolden/step") end),
		},  

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State
    {
        name = "run_start",
        tags = { "moving", "running", "canrotate" },

        onenter = function(inst)
			inst.components.locomotor:RunForward()
            inst.AnimState:PlayAnimation("run_pre")
			
        end,
		
		timeline = 
		{
			TimeEvent(5*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/pangolden/scared") end),
		},
		
        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("run") end ),
        },
    },
	
	State
	{
		name = "run",
		tags = { "moving", "running", "canrotate" },

		onenter = function(inst)
			inst.AnimState:PlayAnimation("run_loop")
			inst.components.locomotor:RunForward()
		end,
		
		timeline = 
        { 
            TimeEvent(0*FRAMES, function(inst)
            inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/pangolden/walk") 
                
            end), 

            TimeEvent(2*FRAMES, function(inst)
            inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/pangolden/walk") 
                
            end),

            TimeEvent(0*FRAMES, PlayFootstep ),

            TimeEvent(1*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/pangolden/movement") end),
            TimeEvent(3*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/pangolden/movement") end),
            TimeEvent(7*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/pangolden/scared") end),
        },

        onexit = function(inst)
           
        end,

		events=
		{
			EventHandler("animover", function(inst) 
				inst.sg:GoToState("run")  
			end ),
		},
	},
	
	State
    {
        name = "run_stop",
        tags = { "idle" },

        onenter = function(inst) 
            inst.components.locomotor:StopMoving()
			inst.AnimState:PlayAnimation("run_pst")
			--inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/step")            
        end,

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end ),
        },
    },
	
	State
    {
        name = "walk_start",
        tags = { "moving", "running", "canrotate" },

        onenter = function(inst)
			inst.components.locomotor:WalkForward()
            inst.AnimState:PlayAnimation("walk_pre")
			
        end,
		
		timeline = 
		{
			--TimeEvent(0*FRAMES, function(inst) inst.Physics:Stop() end ),
		},
		
        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("walk") end ),
        },
    },
	
	State
    {
        name = "walk",
        tags = { "moving", "running", "canrotate" },

        onenter = function(inst)
			--inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/run")
			inst.components.locomotor:WalkForward()
			inst.AnimState:PlayAnimation("walk_loop")
			
        end,
		
		timeline =
		{
			TimeEvent(0*FRAMES, function(inst) 
                inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/pangolden/walk","steps") 
                inst.SoundEmitter:SetParameter("steps", "intensity", .9 )
                inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/pangolden/movement")  
            end),

            TimeEvent(2*FRAMES, function(inst) 
                inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/pangolden/walk","steps") 
                inst.SoundEmitter:SetParameter("steps", "intensity", math.random())  
            end),

            TimeEvent(2*FRAMES, PlayFootstep ), 

            TimeEvent(12*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/pangolden/movement") end),
            
            TimeEvent(20*FRAMES, function(inst) 
                inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/pangolden/walk","steps") 
                inst.SoundEmitter:SetParameter("steps", "intensity", .9 )  
            end),

            TimeEvent(21*FRAMES, function(inst) 
                inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/pangolden/walk","steps") 
                inst.SoundEmitter:SetParameter("steps", "intensity", math.random())  
            end),

            TimeEvent(21*FRAMES, PlayFootstep ),         
            
            TimeEvent(33*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/pangolden/movement") end),
		},		
		
		onexit = function(inst)

		end,
		
        events=
		{
			EventHandler("animqueueover", function(inst) 
				inst.sg:GoToState("walk")  
			end ),
		},

    },
	
	State
    {
        name = "walk_stop",
        tags = { "idle" },

        onenter = function(inst) 
            inst.components.locomotor:StopMoving()
			inst.AnimState:PlayAnimation("walk_pst")
			--inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/step")            
        end,
		
		timeline = 
        {             
            TimeEvent(6*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/pangolden/movement") end),
            TimeEvent(2*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/pangolden/walk") end),
        },
		
        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
}

CommonStates.AddFrozenStates(states)
PP_CommonStates.AddCorpseStates(states, nil, 
	--timelines = 
	{
		corpse =
		{
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/pangolden/death") end),
		},
		
		corpse_taunt =
		{
		
		},
	
	},
	--anims = 
	{
		corpse = "death",
		corpse_taunt = "idle"
	},
	--sounds =
	{
		--corpse = inst.sounds.scream
	},
	--fns =
	{
	
	}
	) --(states, isflying, timelines, anims, sounds, fns, events, nofx)
PP_CommonStates.AddKnockbackState(states, nil, "hit") --(states, timeline, anim, sounds, fns)
PP_CommonStates.AddActionStates(states, 
	{
		TimeEvent(0 * FRAMES, function(inst)
            inst:PerformBufferedAction()
        end),
	}, 
	"walk_pst", nil, "drink_pre", "drink_loop", "drink_pst") --(states, timelines, anim, anim2, enteranim, loopanim, exitanim, noanim, events)
PP_CommonStates.AddSailStates(states, {}, "run_pst", "idle")
local simpleanim = "run_pst"
local simpleidle = "idle"
local simplemove = "run"
if CurrentRelease.GreaterOrEqualTo(ReleaseID.IDs.R08_ROT_TURNOFTIDES) then
CommonStates.AddHopStates(states, false, {pre = simplemove.."_pre", loop = simplemove.."_loop", pst = simplemove.."_pst"}, nil, "death")
end
PP_CommonStates.AddJumpInStates(states, nil, simpleanim)
PP_CommonStates.AddSailorStates(states, nil, 
{
	plank_idle = simpleanim,
	plank_idle_loop = simpleidle,
	plank_idle_pst = simpleanim,
	
	plank_hop_pre = simplemove.."_pre",
	plank_hop = simplemove.."_loop",
	
	steer_pre = simpleanim,
	steer_idle = simpleidle,
	steer_turning = simpleanim,
	stop_steering = simpleanim,
	
	row = "run_pst",
}
)

PP_CommonStates.AddAoeStates(states, nil, 
{
	generic = "idle",
	
	leap_pre = simplemove.."_pre",
	leap_loop = simplemove.."_loop",
	leap_pst = simplemove.."_pst",
	
	lunge_pre = simpleanim,
	lunge_loop = simpleanim,
	lunge_pst = simpleanim,
	
	superjump_pre = simpleanim,
	superjump_loop = simpleanim,
	superjump_pst = simpleanim,
	
	castspelltime = 10,
})
    
return StateGraph("pangoldenp", states, events, "idle", actionhandlers)

