require("stategraphs/commonstates")
require("stategraphs/ppstates")

local NATURESTATCAP = 500

local possiblefoods = {
	"dragonfruit",
	"cave_banana",
	"asparagus",
	"carrot",
	"dragonfruit",
	"durian",
	"eggplant",
	"garlic",
	"potato",
	"pomegranate",
	"pepper",
	"tomato",
	"onion",
	"corn",
	"watermelon",
	"pumpkin"
}

local function GetExcludeTags(inst)
	if TheNet:GetPVPEnabled() then
		return {"INLIMBO", "playerghost", "notarget", "shadow", "groundpoundimmune"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion", "playerghost", "INLIMBO", "groundpoundimmune"}
	else	
		return {"player", "companion", "INLIMBO", "playerghost", "notarget", "shadow", "groundpoundimmune"}
	end
end

local function launchitem(item)
	local angle = math.random(1, 360)
    local speed = math.random() * 4 + 2
    angle = (angle + math.random() * 60 - 30) * DEGREES
    item.Physics:SetVel(speed * math.cos(angle), math.random() * 2 + 8, speed * math.sin(angle))
end


local function ShakeIfClose(inst)
    ShakeAllCameras(CAMERASHAKE.VERTICAL, .15,  inst.naturestat * (.02/NATURESTATCAP),  inst.naturestat * (.5/NATURESTATCAP), inst,  inst.naturestat * (20/NATURESTATCAP))
end

local function ShakeIfCloseYell(inst)
    ShakeAllCameras(CAMERASHAKE.VERTICAL, inst.naturestat * (.75/NATURESTATCAP),  inst.naturestat * (.03/NATURESTATCAP),  inst.naturestat * (0.6/NATURESTATCAP), inst,  inst.naturestat * (30/NATURESTATCAP))
end

local longaction = "action"
local shortaction = "action"
local workaction = "attack"
local otheraction = "eat"

local actionhandlers = PP_CommonStates.GetCommonActions(longaction, shortaction, workaction, otheraction)

local actionhandler_overrides = 
{ 
	
}

if #actionhandler_overrides > 0 then
	for i, v in ipairs(actionhandler_overrides) do
		table.insert(actionhandlers, v)
	end
end

local events=
{
	EventHandler("attacked", function(inst, data) 
		if data.stimuli and data.stimuli == "strong" and not (inst.sg:HasStateTag("transform") or inst.sg:HasStateTag("nointerrupt")) and not inst.components.health:IsDead() then
			inst.sg:GoToState("hit")
		elseif data.stimuli and (data.stimuli == "electric" or data.stimuli == "explosive") and not inst.components.health:IsDead() then
			inst.sg:GoToState("hit", data.stimuli)
		elseif not inst.components.health:IsDead() and not inst.sg:HasStateTag("busy") and not inst.sg:HasStateTag("nointerrupt") then 
			inst.sg:GoToState("hit") 
		end 
	end),
	PP_CommonHandlers.AddCommonHandlers(),
    EventHandler("doattack", function(inst, data) if not inst.components.health:IsDead() and (inst.sg:HasStateTag("hit") or not inst.sg:HasStateTag("busy")) then inst.sg:GoToState("attack", data.target) end end),
    CommonHandlers.OnSleep(),
    --CommonHandlers.OnLocomote(true,false),
    CommonHandlers.OnFreeze(),
	---
	PP_CommonHandlers.OpenGift(),
	PP_CommonHandlers.OnKnockback(),
	PP_CommonHandlers.OnLocomoteAdvanced(), --if mob uses toggeable walk or homes
	PP_CommonHandlers.OnDeath(),
		
	EventHandler("respawnfromghost", function(inst)  
			if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:Enable(true)
            end

            inst.components.health:SetInvincible(false)
            inst:ShowHUD(true)
            inst:SetCameraDistance()

            SerializeUserSession(inst) end),	
}

if CurrentRelease.GreaterOrEqualTo(ReleaseID.IDs.R08_ROT_TURNOFTIDES) then
	table.insert(events, PP_CommonHandlers.OnSink())
	table.insert(events, CommonHandlers.OnHop())
end

 local states=
{
	State{
        name = "idle",
        tags = {"idle", "canrotate"},
        
        onenter = function(inst)
            inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation("idle", true)
        end,
        
        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
    
    State{
        name = "attack",
        tags = {"attack", "busy"},
        
        onenter = function(inst, target)    
			inst.sg.statemem.eatingtarget = nil
			local buffaction = inst:GetBufferedAction()
			local target = buffaction ~= nil and buffaction.target or nil
			if target and target:IsValid() and target:HasTag("insect") and not (target:HasTag("largecreature") or target:HasTag("epic")) and not target.components.health:IsInvincible() then
				inst.sg.statemem.eatingtarget = target
			end
			
            inst.components.combat:StartAttack()
            inst.components.locomotor:StopMoving()
			if inst.sg.statemem.eatingtarget then
				inst.AnimState:PlayAnimation("eat")
			else
				inst.SoundEmitter:PlaySound(inst.sounds.angry)
				inst.AnimState:PlayAnimation("atk_pre")
				inst.AnimState:PushAnimation("atk", false)
			end
        end,
        
        
        timeline=
        {
            TimeEvent(15*FRAMES, function(inst)
				if not inst.sg.statemem.eatingtarget then
					PlayablePets.DoWork(inst, 3)
				end
			end),
			TimeEvent(20*FRAMES, function(inst) 
				if inst.sg.statemem.eatingtarget and inst.sg.statemem.eatingtarget:IsValid() and inst:IsNear(inst.sg.statemem.eatingtarget, 3 + (inst.naturestat * (2/NATURESTATCAP))) then
					local target = inst.sg.statemem.eatingtarget
					inst.convertingintofood = true
					inst.components.health:DoDelta(target:HasTag("player") and 30 or 5, false)
					inst.components.hunger:DoDelta(target:HasTag("player") and 10 or 5, false)
					inst.components.sanity:DoDelta(target:HasTag("player") and 10 or 1, false)
					if target:HasTag("player") then
						target:PushEvent("oneaten", {eater = inst})
					else
						target:Remove()
					end
					
				end
			end),
        },
        
        events=
        {
            EventHandler("animqueueover", function(inst) 
				if inst.convertingintofood then
					inst.convertingintofood = nil
					inst.sg:GoToState("spit")
				else
					inst.sg:GoToState("idle") 
				end
			end),
        },
    },

    State{
        name = "eat",
        tags = {"busy"},
        
        onenter = function(inst)
			inst.sg.statemem.eatingtarget = nil
			local buffaction = inst:GetBufferedAction()
			local target = buffaction ~= nil and buffaction.target or nil
			inst.sg.statemem.eatingtarget = target
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("eat")
        end,

        timeline=
        {
            TimeEvent(20*FRAMES, function(inst) 
				if inst.sg.statemem.eatingtarget and inst.sg.statemem.eatingtarget.components.edible.foodtype == FOODTYPE.INSECT then
					inst.convertingintofood = true
				else
					inst.convertingintofood = nil
				end
				if inst.sg.statemem.eatingtarget and inst.sg.statemem.eatingtarget:HasTag("player") and inst:IsNear(inst.sg.statemem.eatingtarget, 3) then
					inst.sg.statemem.eatingtarget:PushEvent("oneaten", {eater = inst})
				else
					inst:PerformBufferedAction() 
				end
			end),
        },

        events=
        {
            EventHandler("animqueueover", function(inst) 
				if inst.convertingintofood then
					inst.convertingintofood = nil
					inst.sg:GoToState("spit")
				else
					inst.sg:GoToState("idle") 
				end
			end),
        },
    },
	
	State{
        name = "hit",
        tags = {"busy"},
        
        onenter = function(inst)
            inst.AnimState:PlayAnimation("hit")
            inst.Physics:Stop()
        end,
        
        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end ),
        },        
    },
	
	State{            
        name = "spit",
        tags = {"busy"},
        
        onenter = function(inst) 
			inst.AnimState:PlayAnimation("taunt")
			inst.SoundEmitter:PlaySound(inst.sounds.yell)
        end,
		
		timeline = {
			TimeEvent(8*FRAMES, function(inst) 
				local pos = inst:GetPosition()
				local food = SpawnPrefab(possiblefoods[math.random(1, #possiblefoods)])
				food.Transform:SetPosition(pos.x, 6 + (inst.naturestat * (4/NATURESTATCAP)), pos.z)
				launchitem(food)
				--
				if inst.naturestat > (NATURESTATCAP/2) then
					ShakeIfCloseYell(inst)
				end
			end),
		},
		
		events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end ),
        },
    }, 
	
	State{            
        name = "special_atk1",
        tags = {"busy"},
        
        onenter = function(inst) 
			inst.components.locomotor:StopMoving()
			inst.AnimState:PlayAnimation("taunt")
			inst.SoundEmitter:PlaySound(inst.sounds.yell)
        end,
		
		timeline = {
			TimeEvent(8*FRAMES, function(inst)
				if inst.naturestat > (NATURESTATCAP/2) then
					ShakeIfCloseYell(inst)
				end
			end),
		},
		
		events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end ),
        },
    }, 
	
	State{
        name= "special_atk2",
        tags = {"idle", "busy"},
        
        onenter = function(inst)
			inst.Physics:Stop()
            --inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/city_pig/scream")
            inst.AnimState:PlayAnimation("run_pst")
        end,
        
        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end ),
        },        
    },
	
    State{
        name = "death",
        tags = {"busy"},

        onenter = function(inst)
			inst.SoundEmitter:PlaySound(inst.sounds.yell)
            inst.Physics:Stop()
            RemovePhysicsColliders(inst)     
			inst.components.lootdropper:DropLoot(Vector3(inst.Transform:GetWorldPosition()))
			inst.components.inventory:DropEverything(true)
            inst.AnimState:PlayAnimation("death")
        end,
		
		timeline=
        {
		
        },

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
					PlayablePets.DoDeath(inst)
                end
            end),
        },

    },
}

CommonStates.AddFrozenStates(states)
CommonStates.AddWalkStates(
    states,
    {
        walktimeline = 
        { 
            --TimeEvent(5*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.bigstep) end),
			TimeEvent(10*FRAMES, function(inst) 
				if inst.naturestat > (NATURESTATCAP/2) then 
					inst.SoundEmitter:PlaySound(inst.sounds.bigstep) 
					ShakeIfClose(inst)
				else
					PlayFootstep(inst)
				end				
			end),
			TimeEvent(24*FRAMES, function(inst) 
				if inst.naturestat > (NATURESTATCAP/2)  then 
					inst.SoundEmitter:PlaySound(inst.sounds.bigstep) 
					ShakeIfClose(inst)
				else
					PlayFootstep(inst)
				end
			end),
        }
    })
    
CommonStates.AddRunStates(
    states,
    {
        runtimeline = 
        { 
            TimeEvent(5*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.walk) end),
        }
    })

CommonStates.AddSleepStates(states,
{    
	starttimeline = {
		TimeEvent(20*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.bigstep) end)
	},
	sleeptimeline = {
        TimeEvent(20*FRAMES, function(inst) 
			PlayablePets.SleepHeal(inst)
		end),
		TimeEvent(20*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.grunt) end)
	},
	waketimeline = {
		TimeEvent(6*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.curious) end)
	},
})

PP_CommonStates.AddKnockbackState(states, nil, "hit") --(states, timeline, anim, sounds, fns)
PP_CommonStates.AddActionStates(states, 
	{
		TimeEvent(0 * FRAMES, function(inst)
            inst:PerformBufferedAction()
        end),
	}, 
	"walk_pst", nil, nil, "idle", "walk_pst") --(states, timelines, anim, anim2, enteranim, loopanim, exitanim, noanim, events)
PP_CommonStates.AddCorpseStates(states, nil, 
	--timelines = 
	{
		corpse =
		{
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.yell) end),
		},
		
		corpse_taunt =
		{
			TimeEvent(0*FRAMES, function(inst) inst.UpdateStats(inst) end)
		},
	
	},
	--anims = 
	{
		corpse = "death",
		corpse_taunt = "idle"
	},
	--sounds =
	{
		--corpse = "dontstarve_DLC003/creatures/enemy/grabbing_vine/death"
	},
	--fns =
	{

	}
	) ---(states, isflying, timelines, anims, sounds, fns, events, nofx)
PP_CommonStates.AddJumpInStates(states, nil, "walk_pst")
PP_CommonStates.AddOpenGiftStates(states, "idle")
PP_CommonStates.AddSailStates(states, {}, "walk_pst", "idle")
local simpleanim = "walk_pst"
local simpleidle = "idle"
local simplemove = "walk"
if CurrentRelease.GreaterOrEqualTo(ReleaseID.IDs.R08_ROT_TURNOFTIDES) then
CommonStates.AddHopStates(states, false, {pre = simplemove.."_pre", loop = simplemove.."_loop", pst = simplemove.."_pst"}, nil, "death")
end
PP_CommonStates.AddSailorStates(states, nil, 
{
	plank_idle = simpleanim,
	plank_idle_loop = simpleidle,
	plank_idle_pst = simpleanim,
	
	plank_hop_pre = simplemove.."_pre",
	plank_hop = simplemove.."_loop",
	
	steer_pre = simpleanim,
	steer_idle = simpleidle,
	steer_turning = simpleanim,
	stop_steering = simpleanim,
	
	row = "walk_pst",
}
)

    
return StateGraph("snapdragonp", states, events, "idle", actionhandlers)

