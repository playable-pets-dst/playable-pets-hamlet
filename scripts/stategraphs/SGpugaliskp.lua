require("stategraphs/commonstates")

local pu = require ("prefabs/pugaliskp_util")
local GAZE_CD = 20

local function GetExcludeTags(inst)
	if TheNet:GetPVPEnabled() then
		return {"INLIMBO", "playerghost", "notarget", "shadow", "pugalisk", "wall", "groundpoundimmune"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion", "playerghost", "INLIMBO", "notarget", "groundpoundimmune", "pugalisk"}
	else	
		return {"player", "companion", "INLIMBO", "playerghost", "notarget", "shadow", "pugalisk", "wall", "groundpoundimmune"}
	end
end

local function ShakeIfClose(inst)
    ShakeAllCameras(CAMERASHAKE.VERTICAL, .5, .03, .7, inst, 30)
end

local function UndergroundPhysics(inst)
    --inst.Physics:SetCollisionGroup(COLLISION.CHARACTERS)
	inst.Physics:ClearCollisionMask()
	if CurrentRelease.GreaterOrEqualTo(ReleaseID.IDs.R08_ROT_TURNOFTIDES) then
		RaiseFlyingCreature(inst)
		if TheNet:GetServerGameMode() ~= "lavaarena" then
			inst.Physics:ClearCollidesWith(COLLISION.LIMITS)
		end
	end
	
    inst.Physics:CollidesWith(COLLISION.WORLD)
	inst.DynamicShadow:Enable(false)
    --inst.Physics:CollidesWith(COLLISION.OBSTACLES)
end

local function AbovegroundPhysics(inst)
	if inst:HasTag("swimming") then
		LandFlyingCreature(inst)
		inst.AnimState:Hide("wormmovefx")
	else
		inst.AnimState:Show("wormmovefx")
	end
    ChangeToCharacterPhysics(inst)
	inst.Physics:ClearCollisionMask()
    inst.Physics:CollidesWith(COLLISION.WORLD)
	inst.Physics:SetCollisionGroup(COLLISION.CHARACTERS)
	if TheNet:GetServerGameMode() ~= "lavaarena" and CurrentRelease.GreaterOrEqualTo(ReleaseID.IDs.R08_ROT_TURNOFTIDES) then
		inst.Physics:ClearCollidesWith(COLLISION.LIMITS)
	end	
	inst.DynamicShadow:Enable(false)
end

local SHAKE_DIST = 40

local function dogroundpound(inst)
    inst.components.groundpounder:GroundPound()
    local player = GetClosestInstWithTag("player", inst, SHAKE_DIST)
    if player then    
        player.components.playercontroller:ShakeCamera(inst, "VERTICAL", 0.5, 0.03, 2, SHAKE_DIST)
    end
end

local function spawngaze(inst)
    local beam = SpawnPrefab("gaze_beamp")
    local pt = Vector3(inst.Transform:GetWorldPosition())
    local angle = inst.Transform:GetRotation() * DEGREES
    local radius = 4 
    local offset = Vector3(radius * math.cos( angle ), 0, -radius * math.sin( angle ))
    local newpt = pt+offset


    beam.Transform:SetPosition(newpt.x,newpt.y,newpt.z)
    beam.host = inst
    beam.Transform:SetRotation(inst.Transform:GetRotation())
end

local function endgaze(inst)
    if inst.gazetask then
        inst.gazetask:Cancel()
        inst.gazetask = nil
    end
end


local function dogaze(inst)
    if inst.gazetask then
        endgaze(inst)
    end
    inst.gazetask = inst:DoPeriodicTask(0.4,function() spawngaze(inst) end) 
end

function TableConcat(t1,t2)
    for i=1,#t2 do
        t1[#t1+1] = t2[i]
    end
    return t1
end

local longaction = "action"
local shortaction = "action"
local workaction = "action"
local otheraction = "action"

local actionhandlers = PP_CommonStates.GetCommonActions(longaction, shortaction, workaction, otheraction)

local actionhandler_overrides = 
{ 
	ActionHandler(ACTIONS.ATTACK, function(inst)
		if inst.wantstogaze then
			return "gaze"
		else
			return "attack"
		end	
	end),
	ActionHandler(ACTIONS.REVIVE_CORPSE, "failaction"), --no reviving
}

if #actionhandler_overrides > 0 then
	for i, v in ipairs(actionhandler_overrides) do
		table.insert(actionhandlers, v)
	end
end

--for action_loop state
local function StopActionMeter(inst, flash)
    if inst.HUD ~= nil then
        inst.HUD:HideRingMeter(flash)
    end
    if inst.sg.mem.actionmetertask ~= nil then
        inst.sg.mem.actionmetertask:Cancel()
        inst.sg.mem.actionmetertask = nil
        inst.player_classified.actionmeter:set(flash and 1 or 0)
    end
end

local function UpdateActionMeter(inst, starttime)
    inst.player_classified.actionmeter:set_local(math.min(255, math.floor((GetTime() - starttime) * 10 + 2.5)))
end

local function StartActionMeter(inst, duration)
    if inst.HUD ~= nil then
        inst.HUD:ShowRingMeter(inst:GetPosition(), duration)
    end
    inst.player_classified.actionmetertime:set(math.min(255, math.floor(duration * 10 + .5)))
    inst.player_classified.actionmeter:set(2)
    if inst.sg.mem.actionmetertask == nil then
        inst.sg.mem.actionmetertask = inst:DoPeriodicTask(.1, UpdateActionMeter, nil, GetTime())
    end
end
--

local events=
{
	EventHandler("attacked", function(inst, data) 
		if data.weakspot and data.weakspot == true then
			if data.stimuli and data.stimuli == "strong" and not (inst.sg:HasStateTag("transform") or inst.sg:HasStateTag("nointerrupt")) and not inst.components.health:IsDead() then
				inst.sg:GoToState("hit")
			elseif data.stimuli and (data.stimuli == "electric" or data.stimuli == "explosive") and not inst.sg:HasStateTag("nointerrupt") and not inst.components.health:IsDead() then
				inst.sg:GoToState("stun", data.stimuli)
			elseif not inst.components.health:IsDead() and not inst.sg:HasStateTag("busy") and not inst.sg:HasStateTag("underground") and not inst.sg:HasStateTag("nointerrupt") and (inst.sg.mem.last_hit_time or 0) + 2 < GetTime() then 
				inst.sg:GoToState("hit") 
			end 
		end
	end),
	PP_CommonHandlers.AddCommonHandlers(),
    EventHandler("death", function(inst, data)
        if inst.sleepingbag ~= nil and (inst.sg:HasStateTag("bedroll") or inst.sg:HasStateTag("tent")) then -- wakeup on death to "consume" sleeping bag first
            inst.sleepingbag.components.sleepingbag:DoWakeUp()
            inst.sleepingbag = nil
        end
		
		if inst:HasTag("tail") then
            inst.sg:GoToState("tail_exit")
        elseif (data ~= nil and data.cause == "file_load") and inst.components.revivablecorpse ~= nil then
            inst.sg:GoToState("corpse", true)
        else
			
            if inst.sg:HasStateTag("underground") then
                inst.sg:GoToState("death") 
            else
                inst.sg:GoToState("death") 
            end
        end
    end),
	 EventHandler("backup", function(inst) 
        if not inst.sg:HasStateTag("backup") and not inst.components.health:IsDead() then
            inst.sg:GoToState("backup") 
        end
    end),

    EventHandler("premove", function(inst) 
        if not inst.sg:HasStateTag("backup") and not inst.components.health:IsDead() and  not inst.sg:HasStateTag("busy") then
            inst.sg:GoToState("startmove") 
        end
    end),

    EventHandler("emerge", function(inst) 
        if not inst.components.health:IsDead() then
            inst.sg:GoToState("emerge") 
        end
    end),
    EventHandler("doattack", function(inst, data) if not inst.components.health:IsDead() and (inst.sg:HasStateTag("hit") or not inst.sg:HasStateTag("busy")) then inst.sg:GoToState("attack", data.target) end end),
    CommonHandlers.OnSleep(),
    --CommonHandlers.OnLocomote(true,false),
	EventHandler("locomote", function(inst, data)
        if inst.sg:HasStateTag("busy") then
            return
        end
        local is_moving = inst.sg:HasStateTag("moving")
        local should_move = inst.components.locomotor:WantsToMoveForward()
		local is_looking = inst.sg:HasStateTag("looking")

        if inst.sg:HasStateTag("bedroll") or inst.sg:HasStateTag("tent") or inst.sg:HasStateTag("waking") then -- wakeup on locomote
            if inst.sleepingbag ~= nil and inst.sg:HasStateTag("sleeping") then
                inst.sleepingbag.components.sleepingbag:DoWakeUp()
                inst.sleepingbag = nil
				inst:Show()
				--inst.AnimState:PushAnimation("pig_pickup")
				SetSleeperAwakeState(inst)
				--inst.AnimState:PushAnimation("pig_pickup")
				--inst.sg:GoToState("idle", true)
				
            end 
		elseif is_moving and not should_move then
            inst.sg:GoToState("run_stop")
        elseif not is_moving and should_move and not is_looking then
            inst.sg:GoToState("run_start")
        elseif data.force_idle_state and not (is_moving or should_move or inst.sg:HasStateTag("idle") or is_looking) then
            inst.sg:GoToState("idle")
        end
    end),
    CommonHandlers.OnFreeze(),
	
	 EventHandler("ms_opengift",
        function(inst)
            if not inst.sg:HasStateTag("busy") then
                inst.sg:GoToState("opengift")
            end
        end),
		
	EventHandler("respawnfromghost", function(inst)  
			if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:Enable(true)
            end

            inst.components.health:SetInvincible(false)
            inst:ShowHUD(true)
            inst:SetCameraDistance()

            SerializeUserSession(inst) end),	
}


 local states=
{

     State{
        name = "idle",
        tags = {"idle", "canrotate"},
        
        onenter = function(inst, start_anim)
            inst.Physics:Stop()
			inst.components.multibodyp:OnStopMove()
            if inst:HasTag("tail") then
                if start_anim then
                    inst.AnimState:PlayAnimation(start_anim)
                    inst.AnimState:PushAnimation("tail_idle_loop", true)
                else
                    inst.AnimState:PlayAnimation("tail_idle_loop", true)
                end
            else
                if start_anim then
                    inst.AnimState:PlayAnimation(start_anim)
                    inst.AnimState:PushAnimation("head_idle_loop", true)
                else
                    inst.AnimState:PlayAnimation("head_idle_loop", true)
                end
            end
        end,

        onupdate = function(inst)

            if not inst:HasTag("tail") then

                if inst.wantstopremove then
                    inst.wantstopremove = nil
                    inst:PushEvent("premove")
                end
            end

            if inst:HasTag("tail") and inst:HasTag("should_exit") then
                inst.sg:GoToState("tail_exit")
            end
        end,        
    

        events=
        {
            EventHandler("animover", function(inst)
                inst.sg:GoToState("idle")  
            end),
        },        
    },   
	
	State{
        name = "opengift",
        tags = { "busy", "pausepredict" },

        onenter = function(inst)
			inst.components.multibodyp:OnStopMove()
            inst.components.locomotor:Stop()
            inst.components.locomotor:Clear()
            inst:ClearBufferedAction()

            --if IsNearDanger(inst) then
                --inst.sg.statemem.isdanger = true
                --inst.sg:GoToState("idle")
                --if inst.components.talker ~= nil then
                   -- inst.components.talker:Say(GetString(inst, "ANNOUNCE_NODANGERGIFT"))
                --end
                --return
           -- end

            inst.SoundEmitter:PlaySound("dontstarve/common/player_receives_gift")
            inst.AnimState:PlayAnimation("taunt")
            inst.AnimState:PushAnimation("taunt", true)
            -- NOTE: the previously used ripping paper anim is called "giift_loop"

            if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:RemotePausePrediction()
                inst.components.playercontroller:EnableMapControls(false)
                inst.components.playercontroller:Enable(false)
            end
            inst.components.inventory:Hide()
            inst:PushEvent("ms_closepopups")
            inst:ShowActions(false)
            inst:ShowPopUp(POPUPS.GIFTITEM, true)

            if inst.components.giftreceiver ~= nil then
                inst.components.giftreceiver:OnStartOpenGift()
            end
        end,

        timeline =
        {
            -- Timing of the gift box opening animation on giftitempopup.lua
            TimeEvent(155 * FRAMES, function(inst)
               -- inst.AnimState:PlayAnimation("gift_open_pre")
                inst.AnimState:PushAnimation("taunt", true)
            end),
        },

        events =
        {
            EventHandler("firedamage", function(inst)
                inst.AnimState:PlayAnimation("taunt")
                inst.sg:GoToState("idle")
                if inst.components.talker ~= nil then
                    inst.components.talker:Say(GetString(inst, "ANNOUNCE_NODANGERGIFT"))
                end
            end),
            EventHandler("ms_doneopengift", function(inst, data)
				inst.sg:GoToState("idle")
                
            end),
        },

        onexit = function(inst)
            if inst.sg.statemem.isdanger then
                return
            elseif not inst.sg.statemem.isopeningwardrobe then
                if inst.components.playercontroller ~= nil then
                    inst.components.playercontroller:EnableMapControls(true)
                    inst.components.playercontroller:Enable(true)
                end
                inst.components.inventory:Show()
                inst:ShowActions(true)
            end
            inst:ShowPopUp(POPUPS.GIFTITEM, false)
        end,
    },
	
	State{
		name = "hit",
        tags = {"busy", "hit"},

        onenter = function(inst, cb)
			inst.components.multibodyp:OnStopMove()
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("hit")
			inst.sg.mem.last_hit_time = GetTime()
        end,
		
		timeline=
        {
            TimeEvent(2*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/pugalisk/hit") end),
        },

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State{
        name = "taunt",
        tags = {"canrotate", "busy"},

        onenter = function(inst, start_anim)
			assert(not inst:HasTag("tail"))
			inst.components.multibodyp:OnStopMove()
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("taunt")
            inst.wantstotaunt = nil
			inst:PerformBufferedAction()
        end,

        timeline=
        {
            TimeEvent(15*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/pugalisk/taunt") end),
        },

        events=
        {
            EventHandler("animover", function(inst)
                inst.sg:GoToState("idle")  
            end),
        },        
    },

	State{
        name = "special_atk1",
        tags = {"canrotate", "busy"},

        onenter = function(inst, start_anim)
			inst.components.multibodyp:OnStopMove()
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("taunt")
            inst.wantstotaunt = nil
			inst:PerformBufferedAction()
        end,

        timeline=
        {
            TimeEvent(15*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/pugalisk/taunt") end),
        },

        events=
        {
            EventHandler("animover", function(inst)
                inst.sg:GoToState("idle")  
            end),
        },        
    },   
	
	State{
        name = "emerge_taunt",
        tags = {"busy"},

        onenter = function(inst, start_anim)
			--inst.components.multibodyp:OnStopMove()
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("emerge_taunt")
            inst.wantstotaunt = nil
        end,

        timeline=
        {
            TimeEvent(1*FRAMES, function(inst) 
                    inst.SoundEmitter:PlaySound("dontstarve_DLC002/common/volcano/volcano_erupt")      
                end),
            TimeEvent(15*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/pugalisk/taunt") end),
            TimeEvent(15*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/pugalisk/attack") end),
        },

        events=
        {
            EventHandler("animover", function(inst)
                inst.sg:GoToState("idle")  
            end),
        },        
    },   



    State{
        name = "dirt_collapse",
        tags = {"busy"},

        onenter = function(inst, start_anim)
            inst.Physics:Stop()            
            inst.AnimState:PlayAnimation("dirt_collapse", false)            
        end,

        events=
        {
            EventHandler("animover", function(inst)
                inst:Remove()  
            end),
        },    
    },

    State{
        name = "tail_exit",
        tags = {"busy"},

        onenter = function(inst, start_anim)
            inst.Physics:Stop()            
            inst.AnimState:PlayAnimation("tail_idle_pst")            
        end,

        events=
        {
            EventHandler("animover", function(inst)
                inst.sg:GoToState("dirt_collapse")
            end),
        },    
    },

    State{
        name = "tail_ready",
        tags = {"busy"},

        onenter = function(inst, start_anim)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("tail_idle_pre")
        end,

        events=
        {
            EventHandler("animover", function(inst)
                inst.sg:GoToState("idle")                
            end),
        },    
    },

    State{
        name = "gaze",
        tags = {"busy"},

        onenter = function(inst, target)
			local buffaction = inst:GetBufferedAction()
			local target = buffaction ~= nil and buffaction.target or nil
			if target then
				if target:IsValid() then
					inst:FacePoint(target:GetPosition())
					inst.sg.mem.attacktarget = target
				end
			end
			
			inst.components.multibodyp:OnStopMove()
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("gaze_pre")
        end,



        events=
        {
            EventHandler("animover", function(inst)              
                inst.sg:GoToState("gaze_loop")
                inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/pugalisk/gaze_start")                
            end),
        },    
    },

    State{
        name = "gaze_loop",
        tags = {"busy","canrotate","gazing", "nointerrupt"},

        onenter = function(inst, target)   
            dogaze(inst)         
            inst.AnimState:PlayAnimation("gaze_loop", true)
            inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/pugalisk/gaze_LP","gazor")   
			inst.gaze_loops = inst.gaze_loops and inst.gaze_loops + 1 or 1
        end,

        timeline =
        {
            TimeEvent(45*FRAMES, function(inst) dogaze(inst) end),
        },

        onupdate = function(inst)
            local target = inst.sg.mem.attacktarget
            if target then
                local pt = Vector3(target.Transform:GetWorldPosition())
                local angle = inst:GetAngleToPoint(pt)
                inst.Transform:SetRotation(angle)   
            end       
        end,

        onexit = function(inst)
            inst.SoundEmitter:KillSound("gazor")             
        end,

        events=
        {
            EventHandler("animover", function(inst)     
				if inst.gaze_loops and inst.gaze_loops >= 2 then
					inst.sg:GoToState("gaze_pst") 
				else
					inst.sg:GoToState("gaze_loop")        
				end
            end),
        },    
    },

    State{
        name = "gaze_pst",
        tags = {"busy"},

        onenter = function(inst, start_anim) 
			endgaze(inst)
            inst.AnimState:PlayAnimation("gaze_pst")
        end,
		
		onexit = function(inst)
			inst.wantstogaze = false
			inst.gaze_loops = nil
			inst.sg.statemem.attacktarget = nil
			inst.components.combat:SetRange(TheNet:GetServerGameMode() == "lavaarena" and PPHAM_FORGE.PUGALISK.HIT_RANGE or 6)
			inst:DoTaskInTime(GAZE_CD, function(inst) inst.wantstogaze = true inst.components.combat:SetRange(20, 0) end)
		end,

        events=
        {
            EventHandler("animover", function(inst)
                inst.sg:GoToState("idle")                
            end),
        },    
    },    

    State{
        name = "emerge",
        tags = {"busy"},

        onenter = function(inst, start_anim)
            inst.emerged = true
			inst.components.multibodyp:OnStopMove()
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("head_idle_pre")
        end,

        events=
        {
            EventHandler("animover", function(inst)
                inst.sg:GoToState("idle")                
            end),
        },    
    },

    State{
        name = "underground",
        tags = {"underground","invisible"},

        onenter = function(inst, start_anim)
            inst:Hide()
            inst.Physics:SetActive(false)
            inst.AnimState:PlayAnimation("head_idle_pre")
        end,

        onexit = function(inst)
            inst:Show()
            inst.Physics:SetActive(true)
            inst.movecommited = nil
        end,        
  
    },


    State{
        name = "startmove",
        tags = {"busy","backup"},

        onenter = function(inst, start_anim)
            inst:PushEvent("startmove")   
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("head_idle_pst")
        end,

        events=
        {
            EventHandler("animover", function(inst)
                local pos = Vector3(inst.Transform:GetWorldPosition())
                inst.components.multibodyp:SpawnBody(-inst.Transform:GetRotation() * DEGREES,0.3,pos)
                inst.sg:GoToState("underground")
            end),
        },    
    },

    State{
        name = "backup",
        tags = {"busy","backup"},

        onenter = function(inst, start_anim)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("head_idle_pst")
        end,

        events=
        {
            EventHandler("animover", function(inst)
                local hole = SpawnPrefab("pugalisk_body")  
                hole:AddTag("exithole")    
                hole.Physics:SetActive(false)
                hole.Transform:SetPosition(inst.Transform:GetWorldPosition())
                hole.AnimState:PlayAnimation("dirt_collapse", false)
                hole:ListenForEvent("animover", function(inst, data)
                       hole:Remove()
                    end)                                    
                inst:DoTaskInTime(0.75, function() 
                    pu.recoverfrombadangle(inst) 
                    inst.movecommited = false

                    dogroundpound(inst)
                    --inst.components.groundpounder:GroundPound()

                    inst.sg:GoToState("emerge")
                end)       
                inst.movecommited = true         
                inst.sg:GoToState("underground")

            end),
        },    
    },
    
    State{
        name = "hole",
        tags = {"busy"},

        onenter = function(inst, start_anim)
            inst.AnimState:SetBank("giant_snake")
            inst.AnimState:SetBuild("python_test")

            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("dirt_static")
        end,  
    },   

    State{
        name = "attack",
        tags = {"attack", "canrotate", "busy",},
        
        onenter = function(inst, target)
			inst.components.multibodyp:OnStopMove()
            inst.components.combat:StartAttack()
            if inst:HasTag("tail") then
                inst.AnimState:PlayAnimation("tail_smack")
            else
                inst.AnimState:PlayAnimation("atk")
            end
            inst.sg.statemem.target = target
        end,
        
        timeline =
        {
            TimeEvent(17*FRAMES, function(inst) inst:PerformBufferedAction() end),
            TimeEvent(3*FRAMES, function(inst)  if not inst:HasTag("tail") then inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/pugalisk/attack") end end),
            TimeEvent(6*FRAMES, function(inst)  if not inst:HasTag("tail") then inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/pugalisk/attack_pre") end end),
            TimeEvent(18*FRAMES, function(inst)  if not inst:HasTag("tail") then inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/pugalisk/bite") end end),
            TimeEvent(7*FRAMES, function(inst)  if inst:HasTag("tail") then inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/pugalisk/tail_attack") end end),
            --if inst:HasTag("tail") then inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/pugalisk/attack") end
        },
        
        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
			EventHandler("bodycomplete", function(inst) inst.components.multibodyp:OnStopMove() end),
        },
    },
	
	State{
		name = "action",
        tags = {"busy"},

        onenter = function(inst, cb)
			inst.components.multibodyp:OnStopMove()
            inst.Physics:Stop()
			inst:PerformBufferedAction()
            inst.AnimState:PlayAnimation("emerge_taunt")
			
			
        end,

		timeline=
        {
			TimeEvent(1*FRAMES, function(inst) 

                    --GetPlayer().components.dynamicmusic:OnStartDanger()
                    
                    --inst.components.groundpounder.numRings = 3

                     --dogroundpound(inst)

                    --inst.components.groundpounder:GroundPound()

                    --inst.components.groundpounder.numRings = 2
                    inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/pugalisk/taunt")
                    --inst.SoundEmitter:PlaySound("dontstarve_DLC002/common/volcano/volcano_erupt")            

                end),
            TimeEvent(15*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/pugalisk/taunt") end),
            TimeEvent(15*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/pugalisk/attack") end),
        },

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
			EventHandler("bodycomplete", function(inst) inst.components.multibodyp:OnStopMove() end),
        },
    },
	
	State
    {
        name = "action_loop",
        tags = { "doing", "nodangle" },

        onenter = function(inst, timeout)
            inst.sg:SetTimeout(timeout or 1)
            inst.components.locomotor:Stop()
            inst.AnimState:PlayAnimation("taunt", true)
            if inst.bufferedaction ~= nil then
                inst.sg.statemem.action = inst.bufferedaction
                if inst.bufferedaction.action.actionmeter then
                    inst.sg.statemem.actionmeter = true
                    StartActionMeter(inst, timeout or 1)
                end
                if inst.bufferedaction.target ~= nil and inst.bufferedaction.target:IsValid() then
                    inst.bufferedaction.target:PushEvent("startlongaction")
                end
            end
        end,

        timeline =
        {
            TimeEvent(4 * FRAMES, function(inst)
                inst.sg:RemoveStateTag("busy")
            end),
        },

        ontimeout = function(inst)
            inst.AnimState:PlayAnimation("walk_pst")
            if inst.sg.statemem.actionmeter then
                inst.sg.statemem.actionmeter = nil
                StopActionMeter(inst, true)
            end
            inst:PerformBufferedAction()
        end,

        events =
        {
            EventHandler("animqueueover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("idle")
                end
            end),
        },

        onexit = function(inst)
            if inst.sg.statemem.actionmeter then
                StopActionMeter(inst, false)
            end
            if inst.bufferedaction == inst.sg.statemem.action then
                inst:ClearBufferedAction()
            end
        end,
    },
	
    State{
        name = "death",
        tags = {"busy"},

        onenter = function(inst)
			if inst:HasTag("tail") then
                inst.AnimState:PlayAnimation("tail_idle_pst")
                inst.AnimState:PushAnimation("dirt_collapse_slow", false)
                
            else
                inst.AnimState:PlayAnimation("death")
            end
			endgaze(inst)
            inst.Physics:Stop()
            RemovePhysicsColliders(inst)     
			inst.components.lootdropper:DropLoot(Vector3(inst.Transform:GetWorldPosition()))
			inst.components.inventory:DropEverything(true)
        end,
		
		timeline=
        {
			TimeEvent(20*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/pugalisk/death") end),
        },

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
					PlayablePets.DoDeath(inst)
                end
            end),
        },

    },
	
	State {
        name = "sleep",
        tags = { "sleeping", "busy" }, --add tag "busy" if you hate sliding

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pre")
            --if fns ~= nil and fns.onsleep ~= nil then
                --fns.onsleep(inst)
            --end
        end,

        timeline = 
		{
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("fallAsleep") end ),
		},

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("sleeping") end ),
            EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "sleeping",
        tags = { "sleeping", "busy" },

        --onenter = onentersleeping,
		
		onenter = function(inst)
				inst.components.locomotor:StopMoving()
				--if inst.components.sanity.current >= 50 then
				local hungerpercent = inst.components.hunger:GetPercent()
				if hungerpercent ~= nil and hungerpercent ~= 0 then -- We don't want players to heal out starvation.
				inst.components.health:DoDelta(20, false)
				end
				--end
				--inst.components.sanity:DoDelta(1, false)
				--inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/bearger/sleep")
				inst.AnimState:PlayAnimation("sleep_loop")
			end,
			
		onexit = function(inst)
		--inst.components.sanity.dapperness = -0.5
		end,
        --timeline = timelines ~= nil and timelines.sleeptimeline or nil,
		timeline=
        {
			TimeEvent(35*FRAMES, function(inst) inst.SoundEmitter:PlaySound("sleeping") end ),
        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("sleeping") end ),
			EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "wake",
        tags = { "busy", "waking" },

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pst")
            if inst.components.sleeper ~= nil and inst.components.sleeper:IsAsleep() then
                inst.components.sleeper:WakeUp()
            end
        end,

        timeline = 
		{
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("wakeUp") end ),
		},

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State
    {
        name = "run_start",
        tags = { "moving", "running", "canrotate" },

        onenter = function(inst)
			----print("runstart ran")
			--inst.components.locomotor:RunForward()
			--inst:Hide()
            --inst.AnimState:PlayAnimation("run_pre")
			inst.AnimState:PlayAnimation("head_idle_pst")
			
        end,
		
		timeline = 
		{
			--TimeEvent(0*FRAMES, function(inst) inst.Physics:Stop() end ),
		},
		
        events =
        {
            EventHandler("animover", function(inst) 
				----print("runstart ended")
                inst.sg:GoToState("run")
			end ),
        },
    },
	
	State
    {
        name = "run",
        tags = { "moving", "running", "busy", "underground" },

        onenter = function(inst)
			----print("run ran")
			local pos = Vector3(inst.Transform:GetWorldPosition())
            inst.components.multibodyp:SpawnBody(inst.Transform:GetRotation() * DEGREES,0.3,pos)
			if inst:HasTag("swimming") then
				inst.SoundEmitter:PlaySound("turnoftides/common/together/water/splash/large")
				local splash = SpawnPrefab(CurrentRelease.GreaterOrEqualTo(ReleaseID.IDs.R09_ROT_SALTYDOG) and "splash_green" or "splash_water")
				splash.Transform:SetScale(2, 2, 2)
				splash.Transform:SetPosition(inst:GetPosition():Get())
			else
				inst.components.groundpounder:GroundPound()
				ShakeIfClose(inst)
				inst.components.combat:DoAreaAttack(inst, 5, nil, nil, nil, GetExcludeTags())
			end
			local tail = inst.components.multibodyp.tail or nil
			if tail then
				tail.sg:GoToState("tail_exit")
			end
			inst.components.multibodyp:OnStartMove()
			UndergroundPhysics(inst)
			--inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/run")
			
			--inst.components.segmentedp:StartMove()
			inst.components.locomotor:WalkForward()
			inst:Hide()
            --inst.Physics:SetActive(false)
            inst.AnimState:PlayAnimation("tail_smack")
        end,
		
		timeline = 
		{

		},
		
		onexit = function(inst)
			AbovegroundPhysics(inst)
			inst:Show()
		end,
		
        events=
			{
				EventHandler("animover", function(inst) 
					inst.sg:GoToState("run2")  
				end ),
			},

    },
	
	State
    {
        name = "run2",
        tags = { "moving", "running", "canrotate", "underground" },

        onenter = function(inst)
			--inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/run")
			inst.components.locomotor:RunForward()
			--inst.components.segmentedp:StartMove()
            inst.sg:SetTimeout(1)
			inst:Hide()
        end,
		
		timeline = 
		{

		},
		
		ontimeout = function(inst)
			inst.sg:GoToState("run") 
		end,
		
		onexit = function(inst)
			inst:Show()
		end,
		
        events=
			{
				EventHandler("animover", function(inst) 
					inst.sg:GoToState("run")  
				end ),
			},

    },
	
	State
    {
        name = "run_stop",
        tags = { "idle" },

        onenter = function(inst) 
            inst.components.locomotor:StopMoving()
			inst.components.multibodyp:OnStopMove()
			--inst.components.segmentedp:StopMove()
			inst.AnimState:PlayAnimation("head_idle_pre")
			--inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/step")            
        end,
		
		onexit = function(inst)
			inst.components.multibodyp:OnStopMove()
		end,

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end ),
        },
    },
	
	--=========================
	-----wormhole states----
	--=========================
	
	State{
		name = "jumpin_pre",
        tags = {"busy"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
			inst:PerformBufferedAction()
            inst.AnimState:PlayAnimation("head_idle_pst")
			----print("DEBUG: JUMPIN PRE ran!")
        end,

		timeline=
        {
			
        },

        events=
        {
			EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    if inst.bufferedaction ~= nil then
                        inst:PerformBufferedAction()
						----print("DEBUG: Buffered Action did not return nil! "..inst.bufferedaction)
                    else
						----print("DEBUG: Buffered Action did return nil!")
                        inst.sg:GoToState("idle")
                    end
                end
            end),
        },
    },
	
	State{
		name = "jumpin",
        tags = {"doing", "busy"},

        onenter = function(inst, data)
            inst.Physics:Stop()
			----print("DEBUG: Jumpin State ran")
			inst:PerformBufferedAction()
            inst.AnimState:PlayAnimation("walk_pst")
			
			inst.components.locomotor:Stop()

            inst.sg.statemem.target = data.teleporter
            inst.sg.statemem.heavy = inst.components.inventory:IsHeavyLifting()

            if data.teleporter ~= nil and data.teleporter.components.teleporter ~= nil then
				----print("DEBUG: DATA IS NOT NIL!")
                data.teleporter.components.teleporter:RegisterTeleportee(inst)
            end
			
            local pos = data ~= nil and data.teleporter and data.teleporter:GetPosition() or nil
			
			inst.sg.statemem.teleportarrivestate = "emerge_taunt"
        end,

		timeline=
        {
			--TimeEvent(13*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/hound/bark") end),
			--TimeEvent(24*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/hound/bark") end),
        },

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    if inst.sg.statemem.target ~= nil and
                        inst.sg.statemem.target:IsValid() and
                        inst.sg.statemem.target.components.teleporter ~= nil then
                        --Unregister first before actually teleporting
                        inst.sg.statemem.target.components.teleporter:UnregisterTeleportee(inst)
                        if inst.sg.statemem.target.components.teleporter:Activate(inst) then
                            inst.sg.statemem.isteleporting = true
                            inst.components.health:SetInvincible(true)
                            if inst.components.playercontroller ~= nil then
                                inst.components.playercontroller:Enable(false)
                            end
                            inst:Hide()
                            inst.DynamicShadow:Enable(false)
                            return
                        end
                    end
                    inst.sg:GoToState("idle")
                end
            end),
        },

        onexit = function(inst)
            if inst.sg.statemem.isphysicstoggle then
                ToggleOnPhysics(inst)
            end

            if inst.sg.statemem.isteleporting then
                inst.components.health:SetInvincible(false)
                if inst.components.playercontroller ~= nil then
                    inst.components.playercontroller:Enable(true)
                end
                inst:Show()
                inst.DynamicShadow:Enable(true)
            elseif inst.sg.statemem.target ~= nil
                and inst.sg.statemem.target:IsValid()
                and inst.sg.statemem.target.components.teleporter ~= nil then
                inst.sg.statemem.target.components.teleporter:UnregisterTeleportee(inst)
            end
        end,
    },
	
	State{
		name = "jumpout", --unused but is functional
        tags = {"busy"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
			inst:PerformBufferedAction()
            inst.AnimState:PlayAnimation("walk_pst")
			----print("DEBUG: JUMPOUT ran!")
        end,

		timeline=
        {
			
        },

        events=
        {
			EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                        inst.sg:GoToState("idle")
                end
            end),
        },
    },
	--==============================
	-------------------Forge States------------------------	
	State
    {
        name = "corpse",
        tags = { "busy", "noattack", "nopredict", "nomorph", "nodangle" },

        onenter = function(inst, fromload)
            if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:Enable(false)
            end

			endgaze(inst)
			
			
            inst:PushEvent("playerdied", { loading = fromload, skeleton = false })

            inst:ShowActions(false)
            inst.components.health:SetInvincible(true)

            if inst.components.locomotor then
				inst.components.locomotor:StopMoving()
			end
			inst.AnimState:PlayAnimation("death")
        end,
		
		timeline=
		{
			TimeEvent(20*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/pugalisk/death") end),
		},

        onexit = function(inst)
            if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:Enable(true)
            end
            inst:ShowActions(true)
            inst.components.health:SetInvincible(false)
        end,
    },
	
	State{
        name = "corpse_rebirth",
        tags = { "busy", "noattack", "nopredict", "nomorph" },

        onenter = function(inst)
            if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:RemotePausePrediction()
                inst.components.playercontroller:Enable(false)
            end

			inst.sg:SetTimeout(107 * FRAMES)
			
			inst.Physics:CollidesWith(COLLISION.WORLD)
            inst.Physics:CollidesWith(COLLISION.OBSTACLES)
            inst.Physics:CollidesWith(COLLISION.SMALLOBSTACLES)
            inst.Physics:CollidesWith(COLLISION.CHARACTERS)
            inst.Physics:CollidesWith(COLLISION.GIANTS)

            inst.components.health:SetInvincible(true)
            inst:ShowActions(false)
            inst:SetCameraDistance(14)
        end,

        timeline =
        {
            TimeEvent(53 * FRAMES, function(inst)
                inst.components.bloomer:PushBloom("corpse_rebirth", "shaders/anim.ksh", -2)
                inst.sg.statemem.fadeintime = (86 - 53) * FRAMES
                inst.sg.statemem.fadetime = 0
            end),
            TimeEvent(86 * FRAMES, function(inst)
                inst.sg.statemem.physicsrestored = true
                inst.Physics:ClearCollisionMask()
                inst.Physics:CollidesWith(COLLISION.WORLD)
                inst.Physics:CollidesWith(COLLISION.OBSTACLES)
                inst.Physics:CollidesWith(COLLISION.SMALLOBSTACLES)
                inst.Physics:CollidesWith(COLLISION.CHARACTERS)
                inst.Physics:CollidesWith(COLLISION.GIANTS)

                
                if inst.sg.statemem.fade ~= nil then
                    inst.sg.statemem.fadeouttime = 20 * FRAMES
                    inst.sg.statemem.fadetotal = inst.sg.statemem.fade
                end
                inst.sg.statemem.fadeintime = nil
            end),
			TimeEvent((96) * FRAMES, function(inst)
				local pos = inst:GetPosition()
				local fire = SpawnPrefab("lavaarena_portal_player_fx")
				fire.Transform:SetPosition(pos.x, 0, pos.z)
				if inst:HasTag("largecreature") or inst:HasTag("epic") then --totally not for PP I swear! --Fid: Hmmmm are you sure?
					fire.Transform:SetScale(3, 3, 3)
				end
            end),
            TimeEvent((86 + 20) * FRAMES, function(inst)
				inst:Hide()
                inst.components.bloomer:PopBloom("corpse_rebirth")
            end),
        },
		
		
        onupdate = function(inst, dt)
            if inst.sg.statemem.fadeouttime ~= nil then
                inst.sg.statemem.fade = math.max(0, inst.sg.statemem.fade - inst.sg.statemem.fadetotal * dt / inst.sg.statemem.fadeouttime)
                if inst.sg.statemem.fade > 0 then
                    inst.components.colouradder:PushColour("corpse_rebirth", inst.sg.statemem.fade, inst.sg.statemem.fade, inst.sg.statemem.fade, 0)
                else
                    inst.components.colouradder:PopColour("corpse_rebirth")
                    inst.sg.statemem.fadeouttime = nil
                end
            elseif inst.sg.statemem.fadeintime ~= nil then
                local k = 1 - inst.sg.statemem.fadetime / inst.sg.statemem.fadeintime
                inst.sg.statemem.fade = .8 * (1 - k * k)
                inst.components.colouradder:PushColour("corpse_rebirth", inst.sg.statemem.fade, inst.sg.statemem.fade, inst.sg.statemem.fade, 0)
                inst.sg.statemem.fadetime = inst.sg.statemem.fadetime + dt
            end
        end,
		
		ontimeout = function(inst)
			inst.components.bloomer:PopBloom("corpse_rebirth")
            inst.sg:GoToState("corpse_taunt")
        end,
		
        onexit = function(inst)
            inst:ShowActions(true)
            inst:SetCameraDistance()
			inst:Show()
            if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:Enable(true)
            end
            inst.components.health:SetInvincible(false)

            inst.Physics:ClearCollisionMask()
            inst.Physics:CollidesWith(COLLISION.WORLD)
            inst.Physics:CollidesWith(COLLISION.OBSTACLES)
            inst.Physics:CollidesWith(COLLISION.SMALLOBSTACLES)
            inst.Physics:CollidesWith(COLLISION.CHARACTERS)
            inst.Physics:CollidesWith(COLLISION.GIANTS)

            SerializeUserSession(inst)
        end,
    },
	
	State{
        name = "corpse_taunt",
        tags = { "busy", "noattack", "nopredict", "nomorph" },

        onenter = function(inst)
            if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:RemotePausePrediction()
                inst.components.playercontroller:Enable(false)
            end
			inst.components.multibodyp:Setup(PUGSEG,"pugaliskp_body") 
			inst.Physics:CollidesWith(COLLISION.WORLD)
            inst.Physics:CollidesWith(COLLISION.OBSTACLES)
            inst.Physics:CollidesWith(COLLISION.SMALLOBSTACLES)
            inst.Physics:CollidesWith(COLLISION.CHARACTERS)
            inst.Physics:CollidesWith(COLLISION.GIANTS)

			inst.AnimState:PlayAnimation("emerge_taunt")
			
            inst.components.health:SetInvincible(true)
            inst:ShowActions(false)
            inst:SetCameraDistance(14)
        end,

		timeline=
		{
			TimeEvent(15*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/pugalisk/taunt") end),
            TimeEvent(15*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/pugalisk/attack") end),
		},

        events =
        {
            EventHandler("animqueueover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("idle")
                end
            end),
        },

        onexit = function(inst)
            inst:ShowActions(true)
            inst:SetCameraDistance()
            if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:Enable(true)
            end
            inst.components.health:SetInvincible(false)

            inst.Physics:ClearCollisionMask()
            inst.Physics:CollidesWith(COLLISION.WORLD)
            inst.Physics:CollidesWith(COLLISION.OBSTACLES)
            inst.Physics:CollidesWith(COLLISION.SMALLOBSTACLES)
            inst.Physics:CollidesWith(COLLISION.CHARACTERS)
            inst.Physics:CollidesWith(COLLISION.GIANTS)

            SerializeUserSession(inst)
        end,
    },
	
	State
    {
        name = "revivecorpse",

        onenter = function(inst)
            --inst.components.talker:Say(GetString(inst, "ANNOUNCE_REVIVING_CORPSE"))
            local buffaction = inst:GetBufferedAction()
            local target = buffaction ~= nil and buffaction.target or nil
            inst.sg:GoToState("action_loop",
                TUNING.REVIVE_CORPSE_ACTION_TIME *
                (inst.components.corpsereviver ~= nil and inst.components.corpsereviver:GetReviverSpeedMult() or 1) *
                (target ~= nil and target.components.revivablecorpse ~= nil and target.components.revivablecorpse:GetReviveSpeedMult() or 1)
            )
        end,
    },
	
}

CommonStates.AddFrozenStates(states)



    
return StateGraph("pugaliskp", states, events, "emerge_taunt", actionhandlers)

