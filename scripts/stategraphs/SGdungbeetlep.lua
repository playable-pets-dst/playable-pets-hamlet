require("stategraphs/commonstates")
require("stategraphs/ppstates")

local function processAnim(inst,anim)
	if inst:HasTag("hasdung") then
		return "ball_"..anim
	else
		return anim
	end
end

local function GetExcludeTags(inst)
	if TheNet:GetPVPEnabled() then
		return {"INLIMBO", "playerghost", "notarget", "shadow", "groundpoundimmune"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion", "playerghost", "INLIMBO", "groundpoundimmune"}
	else	
		return {"player", "companion", "INLIMBO", "playerghost", "notarget", "shadow", "groundpoundimmune"}
	end
end

local function ShakeIfClose(inst)
    ShakeAllCameras(CAMERASHAKE.VERTICAL, .15, .02, .5, inst, 20)
end

local function GetOnBall(inst, target)
	ChangeToCharacterPhysics(inst)
	inst.components.health:SetInvincible(false)
	if target and target.poopball_hp then
		inst.components.locomotor:SetExternalSpeedMultiplier(inst, 1.2, 1.2)
		inst.poopball_hp = target.poopball_hp
		inst.dung_target = nil
		target:Remove()
		inst:AddTag("hasdung")
	else
		--print("ERROR: No dung target on GetOnBall fn!")
	end	
end

local function GetOffBall(inst, forced)
	ChangeToCharacterPhysics(inst)
	inst.components.health:SetInvincible(false)
	inst.shouldwalk = true --dungbeetle shouldn't be able to walk without his dungball
    local ball = SpawnPrefab("dungballp")
	ball.Transform:SetPosition(inst:GetPosition():Get())
	ball.poopball_hp = inst.poopball_hp or 300
	inst.poopball_hp = 0
	inst:RemoveTag("hasdung")
	inst.components.locomotor:RemoveExternalSpeedMultiplier(inst, 1.2)
	if forced then
		ball.AnimState:PlayAnimation("break")
		ball.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/dungbeetle/dungball_break")
		ball.DynamicShadow:Enable(false)
		ball:ListenForEvent("animover", function(inst) inst:Remove() end)
		if TheNet:GetServerGameMode() == "lavaarena" then
			inst:DoTaskInTime(PPHAM_FORGE.DUNGBEETLE.SPECIAL_CD, function(inst)
				if not inst.components.health:IsDead() then
					local ball = SpawnPrefab("dungballp")
					ball.Transform:SetPosition(inst:GetPosition():Get())
					inst.dung_target = ball
					inst.shouldwalk = false
					inst.sg:GoToState("jump_ball", ball)
				end
			end)
		end
	end
end

--for action_loop state
local function StopActionMeter(inst, flash)
    if inst.HUD ~= nil then
        inst.HUD:HideRingMeter(flash)
    end
    if inst.sg.mem.actionmetertask ~= nil then
        inst.sg.mem.actionmetertask:Cancel()
        inst.sg.mem.actionmetertask = nil
        inst.player_classified.actionmeter:set(flash and 1 or 0)
    end
end

local function UpdateActionMeter(inst, starttime)
    inst.player_classified.actionmeter:set_local(math.min(255, math.floor((GetTime() - starttime) * 10 + 2.5)))
end

local function StartActionMeter(inst, duration)
    if inst.HUD ~= nil then
        inst.HUD:ShowRingMeter(inst:GetPosition(), duration)
    end
    inst.player_classified.actionmetertime:set(math.min(255, math.floor(duration * 10 + .5)))
    inst.player_classified.actionmeter:set(2)
    if inst.sg.mem.actionmetertask == nil then
        inst.sg.mem.actionmetertask = inst:DoPeriodicTask(.1, UpdateActionMeter, nil, GetTime())
    end
end
--

local longaction = "action"
local shortaction = "action"
local workaction = "action"
local otheraction = "action"

local actionhandlers = PP_CommonStates.GetCommonActions(longaction, shortaction, workaction, otheraction)

local actionhandler_overrides = 
{ 
	ActionHandler(ACTIONS.ATTACK, "walk_pst"),
	ActionHandler(ACTIONS.BUILD, shortaction),
	ActionHandler(ACTIONS.DEPLOY, shortaction),
}

if #actionhandler_overrides > 0 then
	for i, v in ipairs(actionhandler_overrides) do
		table.insert(actionhandlers, v)
	end
end

local events=
{
	EventHandler("attacked", function(inst, data) 
		if not inst:HasTag("dung") then
			if data.stimuli and data.stimuli == "strong" and not (inst.sg:HasStateTag("transform") or inst.sg:HasStateTag("nointerrupt")) and not inst.components.health:IsDead() then
				inst.sg:GoToState("hit")
			elseif data.stimuli and (data.stimuli == "electric" or data.stimuli == "explosive") and not inst.components.health:IsDead() then
				inst.sg:GoToState("hit", data.stimuli)
			elseif not inst.components.health:IsDead() and not inst.sg:HasStateTag("busy") and not inst.sg:HasStateTag("nointerrupt") then 
				inst.sg:GoToState("hit") 
			end
		end
	end),
	EventHandler("oneaten", function(inst) inst.sg:GoToState("eaten") end),
	EventHandler("bumped", 
		function(inst) 
			inst.SoundEmitter:KillSound("dungroll") 
			inst.sg:GoToState("bumped") 
		end),
	PP_CommonHandlers.AddCommonHandlers(),
	EventHandler("balldestroyed", 
		function(inst) 
			inst.SoundEmitter:KillSound("dungroll")
			GetOffBall(inst, true)
			inst.sg:GoToState("bumped") 
		end),	
    EventHandler("doattack", function(inst, data) if not inst.components.health:IsDead() and (inst.sg:HasStateTag("hit") or not inst.sg:HasStateTag("busy")) then inst.sg:GoToState("attack", data.target) end end),
    CommonHandlers.OnSleep(),
    --CommonHandlers.OnLocomote(true,false),
    CommonHandlers.OnFreeze(),
	---
	PP_CommonHandlers.OpenGift(),
	PP_CommonHandlers.OnKnockback(),
	PP_CommonHandlers.OnLocomoteAdvanced(), --if mob uses toggeable walk or homes
	PP_CommonHandlers.OnDeath(),
		
	EventHandler("respawnfromghost", function(inst)  
			if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:Enable(true)
            end

            inst.components.health:SetInvincible(false)
            inst:ShowHUD(true)
            inst:SetCameraDistance()

            SerializeUserSession(inst) end),	
}

if CurrentRelease.GreaterOrEqualTo(ReleaseID.IDs.R08_ROT_TURNOFTIDES) then
	table.insert(events, PP_CommonHandlers.OnSink())
	table.insert(events, CommonHandlers.OnHop())
end

 local states=
{
    State{
		name = "idle",
		tags = {"idle", "canrotate"},
		onenter = function(inst, playanim)
			inst.Physics:Stop()
			inst.AnimState:PlayAnimation(processAnim(inst,"idle"), true)
			inst.SoundEmitter:PlaySound ("dontstarve_DLC003/creatures/dungbeetle/idle")                               
			inst.sg:SetTimeout(1 + math.random()*1)
		end,
	},
	
	State{
        name = "attack",
        tags = {"attack"},
        
        onenter = function(inst, cb)
			if inst.isflying then
				inst.sg:GoToState("idle")
			else
				inst.Physics:Stop()
				inst.components.combat:StartAttack()
				inst.AnimState:PlayAnimation("atk_pre")
				inst.AnimState:PushAnimation("atk",false)
			end
        end,
        
        timeline=
        {
            TimeEvent(8*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/grabbing_vine/attack_pre") end),
            TimeEvent(11*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/grabbing_vine/attack") end),
            TimeEvent(15*FRAMES, function(inst) inst:PerformBufferedAction() end),
        },
        
        events=
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State{
		name = "dig",
		
		onenter = function(inst) 
			inst.Physics:Stop()
			inst.AnimState:PlayAnimation("dig_pre")
		end,
		
		events=
		{
			EventHandler("animover", function (inst, data) inst.sg:GoToState("dig_loop") end),
		} 
	}, 
		
	State{
		name = "dig_loop",
		tags = {"busy"},
		
		onenter = function(inst) 
			inst.AnimState:PlayAnimation( "dig_loop", true )                
			inst.sg:SetTimeout(2*math.random()+.5)
		end,    
		
		ontimeout= function(inst)
			inst.sg:GoToState("dig_pst")
		end,
	},

	State{
		name = "dig_pst",
		
		onenter = function(inst) 
			inst.AnimState:PlayAnimation("dig_pst")
			inst:PerformBufferedAction()
		end,
		
		events=
		{
			EventHandler("animover", function (inst, data) inst.sg:GoToState("idle") end),
		} 
	},  
	
	State{
        name= "special_atk1",
        tags = {"idle", "busy"},
        
        onenter = function(inst)
			inst.Physics:Stop()
			if TheNet:GetServerGameMode() ~= "lavaarena" and inst:HasTag("hasdung") then
				GetOffBall(inst)
				inst.AnimState:PlayAnimation("walk_pst")
			else
				inst.SoundEmitter:PlaySound(inst.sounds.scream)
				inst.AnimState:PlayAnimation("emote_surprise")
			end            
        end,
        
        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end ),
        },        
    },
	
	State{
        name= "special_atk2",
        tags = {"idle", "busy"},
        
        onenter = function(inst)
			inst.Physics:Stop()
			if inst:HasTag("hasdung") then
				if not inst.shouldwalk or inst.shouldwalk == false then
					inst.shouldwalk = true
				else
					inst.shouldwalk = false
				end
			end
			inst.AnimState:PushAnimation(processAnim(inst,"walk_pst"))
        end,
        
        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end ),
        },        
    },
	
	State{
		name = "jump_ball",
		tags = {"busy", "moving", "canrotate", "nointerrupt"},
		onenter = function(inst, target) 
			if inst:HasTag("hasdung") then
				GetOffBall(inst)
			end
			local buffaction = inst:GetBufferedAction()
			local target = buffaction ~= nil and buffaction.target or nil
			RemovePhysicsColliders(inst)   
			inst.components.health:SetInvincible(true)
			inst.Physics:Stop()
			inst.AnimState:PlayAnimation("ball_get_on")
			inst.SoundEmitter:PlaySound("dontstarve/common/craftable/tent_sleep")
			--the only time this shouldn't be nil is if FF spawned a ball and wants the player to get on it immediately
			if not inst.dung_target then
				inst.dung_target = target
			end
			local pos = Point(inst.dung_target.Transform:GetWorldPosition())

			local MAX_JUMPIN_DIST = 3
			local MAX_JUMPIN_DIST_SQ = MAX_JUMPIN_DIST*MAX_JUMPIN_DIST
			local MAX_JUMPIN_SPEED = 6     
			local dist
			if pos ~= nil then
				inst:ForceFacePoint(pos)
				local distsq = inst:GetDistanceSqToPoint(pos)
				if distsq <= 0.25*0.25 then
					dist = 0
					inst.sg.statemem.speed = 0
				elseif distsq >= MAX_JUMPIN_DIST_SQ then
					dist = MAX_JUMPIN_DIST
					inst.sg.statemem.speed = MAX_JUMPIN_SPEED
				else
					dist = math.sqrt(distsq)
					inst.sg.statemem.speed = MAX_JUMPIN_SPEED * dist / MAX_JUMPIN_DIST
				end
			else
				inst.sg.statemem.speed = 0
				dist = 0
			end                   
			inst.Physics:SetMotorVel(inst.sg.statemem.speed * .5, 0, 0)
		end,
		
		onexit = function(inst) 
			inst.components.health:SetInvincible(false)
		end,

		timeline =
		{
			TimeEvent(1 * FRAMES, function(inst)
				inst.Physics:SetMotorVel(inst.sg.statemem.speed, 0, 0)
			end),

			--Normal
			TimeEvent(12 * FRAMES, function(inst)
				inst.Physics:Stop()
			end),
		},

		events=
		{
			EventHandler("animover", function (inst) 
				inst.sg:GoToState("jump_ball_pst")                 
			end),
		} 
	}, 

	State{
		name = "jump_ball_pst",
		
		onenter = function(inst)            
			inst.Physics:Stop()
			GetOnBall(inst, inst.dung_target)
			inst.AnimState:PlayAnimation("ball_get_on_pst")
		end,
		
		onexit = function(inst)
			inst.components.health:SetInvincible(false)
			
		end,
		
		events=
		{
			EventHandler("animover", function (inst) inst.sg:GoToState("idle") end),
		} 
	},
	
	State{
		name = "fall",
		tags = {"busy", "stunned"},
		onenter = function(inst)
			inst.Physics:SetDamping(0)
			inst.Physics:SetMotorVel(0,-20+math.random()*10,0)
			inst.AnimState:PlayAnimation("stunned_loop", true)
			inst:CheckTransformState()
		end,
		
		onupdate = function(inst)
			local pt = Point(inst.Transform:GetWorldPosition())
			if pt.y < 2 then
				inst.Physics:SetMotorVel(0,0,0)
			end
			
			if pt.y <= .1 then
				pt.y = 0

				inst.Physics:Stop()
				inst.Physics:SetDamping(5)
				inst.Physics:Teleport(pt.x,pt.y,pt.z)
				inst.DynamicShadow:Enable(true)
				inst.sg:GoToState("stunned")                
			end
		end,

		onexit = function(inst)
			local pt = inst:GetPosition()
			pt.y = 0
			inst.Transform:SetPosition(pt:Get())
		end,
	},    
	
	State{
		name = "stunned",
		tags = {"busy", "stunned"},
		
		onenter = function(inst) 
			inst.Physics:Stop()
			inst.AnimState:PlayAnimation("stunned_loop", true)
			inst.sg:SetTimeout(GetRandomWithVariance(6, 2) )
			if inst.components.inventoryitem then
				inst.components.inventoryitem.canbepickedup = true
			end
		end,
		
		onexit = function(inst)
			if inst.components.inventoryitem then
				inst.components.inventoryitem.canbepickedup = false
			end
		end,
		
		ontimeout = function(inst) inst.sg:GoToState("idle") end,
	},
	
	State{
		name = "bumped",
		tags = {"busy"},
		
		onenter = function(inst, dead) 

			if inst:HasTag("hasdung") then
				GetOffBall(inst)
			end
			
			if dead then
				inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/dungbeetle/death")
			end

			inst.Physics:Stop()
			inst:ClearBufferedAction()
			inst.AnimState:PlayAnimation( "fall_off_pre" )
			inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/dungbeetle/crash")
			inst.components.locomotor.runspeed = -6
			inst.components.locomotor:RunForward()

		end,

		events=
		{
			EventHandler("animover", function(inst) inst.sg:GoToState("bumped_loop") end ),
		},  

		onexit = function(inst)
			inst.components.locomotor.runspeed = 6
			inst.Physics:Stop()
		end,        
	},

	State{
		name = "bumped_loop",
		tags = {"busy"},
		
		onenter = function(inst) 
			inst.AnimState:PlayAnimation( processAnim(inst,"fall_off_loop"), true )
			inst.SoundEmitter:PlaySound("dontstarve/movement/bodyfall_dirt")
			inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/dungbeetle/fall_off_LP","fallen")
			inst.sg:SetTimeout(2)
		end,

		onexit = function(inst)
			inst.SoundEmitter:KillSound("fallen")
		end,
		
		ontimeout = function(inst) inst.sg:GoToState("bumped_pst") end,
	},    

	State{
		name = "bumped_pst",
		tags = {"busy"},
		
		onenter = function(inst) 
			inst.AnimState:PlayAnimation( processAnim(inst,"fall_off_pst"))
			inst.SoundEmitter:KillSound("fallen")
		end,

        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end ),
        },                 
    },


	State{
		name = "hit",
		tags = {"busy"},
		
		onenter = function(inst)
			if not inst:HasTag("hasdung") then                
				inst.SoundEmitter:PlaySound(inst.sounds.hurt)
				inst.SoundEmitter:KillSound("fallen")
				inst.AnimState:PlayAnimation("hit")
				inst.Physics:Stop()
			else
				inst.SoundEmitter:PlaySound(inst.sounds.hurt)
				inst.AnimState:PlayAnimation( processAnim(inst,"emote_surprise"))
			end        
		end,
		
		events=
		{
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end ),
		},        
	}, 
	
    State{
        name = "death",
        tags = {"busy"},

        onenter = function(inst)
			inst.poopball_hp = 0 --incase something went wrong
			inst.SoundEmitter:PlaySound(inst.sounds.scream)
            inst.AnimState:PlayAnimation("death")
            inst.Physics:Stop()
            RemovePhysicsColliders(inst)     
			inst.components.lootdropper:DropLoot(Vector3(inst.Transform:GetWorldPosition()))
			inst.components.inventory:DropEverything(true)
        end,
		
		timeline=
        {
			TimeEvent(3*FRAMES,function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/dungbeetle/death") end ),
        },

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
					PlayablePets.DoDeath(inst)
                end
            end),
        },

    },
	
	State {
        name = "sleep",
        tags = { "sleeping", "busy" }, --add tag "busy" if you hate sliding

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation(processAnim(inst,"sleep_pre"))
            inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/dungbeetle/yawn")
        end,

        --timeline = timelines ~= nil and timelines.starttimeline or nil,

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("sleeping") end ),
            EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "sleeping",
        tags = { "sleeping", "busy" },

        --onenter = onentersleeping,
		
		onenter = function(inst)
				inst.components.locomotor:StopMoving()
				PlayablePets.SleepHeal(inst)
				inst.AnimState:PlayAnimation(processAnim(inst,"sleep_loop"))
				inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/dungbeetle/breath_out")
			end,
			
		onexit = function(inst)
		--inst.components.sanity.dapperness = -0.5
		end,
        --timeline = timelines ~= nil and timelines.sleeptimeline or nil,
		timeline=
        {
			--TimeEvent(33*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/hippo/huff_in") end),
        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("sleeping") end ),
			EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "wake",
        tags = { "busy", "waking" },

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation(processAnim(inst,"sleep_pst"))
            if inst.components.sleeper ~= nil and inst.components.sleeper:IsAsleep() then
                inst.components.sleeper:WakeUp()
            end
        end,

        --timeline = timelines ~= nil and timelines.waketimeline or nil,

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State
    {
        name = "run_start",
        tags = { "moving", "running", "canrotate" },

        onenter = function(inst)
			inst.components.locomotor:RunForward()
            inst.AnimState:PlayAnimation(processAnim(inst,"run_pre"))
			if inst:HasTag("hasdung") then
                inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/dungbeetle/rollingbball_LP","dungroll") 
                inst.SoundEmitter:SetParameter("dungroll", "speed", 1 )
            end 
        end,
		
		onexit = function(inst)
			inst.SoundEmitter:KillSound("dungroll")
		end,
		
		timeline = 
		{
			--TimeEvent(0*FRAMES, function(inst) inst.Physics:Stop() end ),
		},
		
        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("run") end ),
        },
    },
	
	State
	{
		name = "run",
		tags = { "moving", "running", "canrotate" },

		onenter = function(inst)
			inst.AnimState:PlayAnimation(processAnim(inst,"run_loop"), true)
			inst.components.locomotor:RunForward()
			if inst:HasTag("hasdung") then
                inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/dungbeetle/rollingbball_LP","dungroll") 
                inst.SoundEmitter:SetParameter("dungroll", "speed", 1 )
            end  
		end,

        onexit = function(inst)
			inst.SoundEmitter:KillSound("dungroll")
		end,
		
		timeline=
		{
			TimeEvent(10*FRAMES, function(inst) 
				if inst:HasTag("hasdung") then 
					inst.SoundEmitter:PlaySound("dontstarve/movement/run_marsh_small") 
				else
					PlayFootstep(inst)
				end 
			end ),
			TimeEvent(11*FRAMES, function(inst) 
				if inst:HasTag("hasdung") then 
					inst.SoundEmitter:PlaySound("dontstarve/movement/run_marsh_small") 
				else
					PlayFootstep(inst)
				end 
			end ),
		},

		events=
		{
			EventHandler("animover", function(inst) 
				inst.sg:GoToState("run")  
			end ),
		},
	},
	
	State
    {
        name = "run_stop",
        tags = { "idle" },

        onenter = function(inst) 
            inst.components.locomotor:StopMoving()
			inst.AnimState:PlayAnimation(processAnim(inst,"run_pst"))
			--inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/step")            
        end,

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end ),
        },
    },	
	
	State
    {
        name = "walk_start",
        tags = { "moving", "canrotate" },

        onenter = function(inst)
			if inst:HasTag("hasdung") then
				inst.components.locomotor:WalkForward()
				inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/dungbeetle/rollingbball_LP","dungroll") 
                inst.SoundEmitter:SetParameter("dungroll", "speed", 0 )
			else
				inst.components.locomotor:RunForward()
			end
			inst.AnimState:PlayAnimation(processAnim(inst,"walk_pre"))
			
        end,
		
		onexit = function(inst)
			inst.SoundEmitter:KillSound("dungroll")
		end,
		
		timeline = 
		{
			--TimeEvent(0*FRAMES, function(inst) inst.Physics:Stop() end ),
		},
		
        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("walk") end ),
        },
    },
	
	State
    {
        name = "walk",
        tags = { "moving", "canrotate" },

        onenter = function(inst)
			--inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/run")
			if inst:HasTag("hasdung") then
				inst.components.locomotor:WalkForward()
                inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/dungbeetle/rollingbball_LP","dungroll") 
                inst.SoundEmitter:SetParameter("dungroll", "speed", 0 )
			else
				inst.components.locomotor:RunForward()
			end
			inst.AnimState:PlayAnimation(processAnim(inst,"walk_loop", true))			
        end,
		
		timeline =
		{
			TimeEvent(10*FRAMES, function(inst) if not inst:HasTag("hasdung") then PlayFootstep(inst) end end ),
            TimeEvent(11*FRAMES, function(inst) if not inst:HasTag("hasdung") then PlayFootstep(inst) end end ),
		},		
		
		onexit = function(inst)
			inst.SoundEmitter:KillSound("dungroll")
		end,
		
        events=
		{
			EventHandler("animqueueover", function(inst) 
				inst.sg:GoToState("walk")  
			end ),
		},

    },
	
	State
    {
        name = "walk_stop",
        tags = { "idle" },

        onenter = function(inst) 
            inst.components.locomotor:StopMoving()
			inst.AnimState:PlayAnimation(processAnim(inst,"walk_pst"))
			--inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/step")            
        end,

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end ),
        },
    },
	
	State
    {
        name = "action",
        tags = { "busy" },

        onenter = function(inst) 
            inst.components.locomotor:StopMoving()
			inst.AnimState:PlayAnimation(processAnim(inst,"walk_pst"))
			inst:PerformBufferedAction()
			--inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/step")            
        end,

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end ),
        },
    },
	
	State
    {
        name = "action_loop",
        tags = { "doing", "nodangle" },

        onenter = function(inst, timeout)
            inst.sg:SetTimeout(timeout or 1)
            inst.components.locomotor:Stop()
			if inst:HasTag("hasdung") then
				inst.AnimState:PlayAnimation(processAnim(inst,"idle"), true)
			else
				inst.AnimState:PlayAnimation("dig_pre", false)
				inst.AnimState:PushAnimation("dig_loop", true)
			end
            if inst.bufferedaction ~= nil then
                inst.sg.statemem.action = inst.bufferedaction
                if inst.bufferedaction.action.actionmeter then
                    inst.sg.statemem.actionmeter = true
                    StartActionMeter(inst, timeout or 1)
                end
                if inst.bufferedaction.target ~= nil and inst.bufferedaction.target:IsValid() then
                    inst.bufferedaction.target:PushEvent("startlongaction")
                end
            end
        end,

        timeline =
        {
            TimeEvent(4 * FRAMES, function(inst)
                inst.sg:RemoveStateTag("busy")
            end),
        },

        ontimeout = function(inst)
            inst.AnimState:PlayAnimation(inst:HasTag("hasdung") and processAnim(inst,"walk_pst") or "dig_pst")
            if inst.sg.statemem.actionmeter then
                inst.sg.statemem.actionmeter = nil
                StopActionMeter(inst, true)
            end
            inst:PerformBufferedAction()
        end,

        events =
        {
            EventHandler("animqueueover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("idle")
                end
            end),
        },

        onexit = function(inst)
            if inst.sg.statemem.actionmeter then
                StopActionMeter(inst, false)
            end
            if inst.bufferedaction == inst.sg.statemem.action then
                inst:ClearBufferedAction()
            end
        end,
    },
	
	State
    {
        name = "revivecorpse",

        onenter = function(inst)
            --inst.components.talker:Say(GetString(inst, "ANNOUNCE_REVIVING_CORPSE"))
            local buffaction = inst:GetBufferedAction()
            local target = buffaction ~= nil and buffaction.target or nil
            inst.sg:GoToState("action_loop",
                TUNING.REVIVE_CORPSE_ACTION_TIME *
                (inst.components.corpsereviver ~= nil and inst.components.corpsereviver:GetReviverSpeedMult() or 1) *
                (target ~= nil and target.components.revivablecorpse ~= nil and target.components.revivablecorpse:GetReviveSpeedMult() or 1)
            )
        end,
    },
	
	-- FROZEN STATES
	State{
		name = "frozen",
		tags = {"busy", "frozen"},
		
		onenter = function(inst)
			if inst.components.locomotor then
				inst.components.locomotor:StopMoving()
			end
			inst.AnimState:PlayAnimation(processAnim(inst,"frozen"), true)
			inst.SoundEmitter:PlaySound("dontstarve/common/freezecreature")
			inst.AnimState:OverrideSymbol("swap_frozen", "frozen", "frozen")
		end,
		
		onexit = function(inst)
			inst.AnimState:ClearOverrideSymbol("swap_frozen")
		end,
		
		events=
		{   
			EventHandler("onthaw", function(inst) inst.sg:GoToState("thaw") end ),        
		}
	},

	State{
		name = "thaw",
		tags = {"busy", "thawing"},
		
		onenter = function(inst) 
			if inst.components.locomotor then
				inst.components.locomotor:StopMoving()
			end
			inst.AnimState:PlayAnimation(processAnim(inst,"frozen_loop_pst"), true)
			inst.SoundEmitter:PlaySound("dontstarve/common/freezethaw", "thawing")
			inst.AnimState:OverrideSymbol("swap_frozen", "frozen", "frozen")
		end,
		
		onexit = function(inst)
			inst.SoundEmitter:KillSound("thawing")
			inst.AnimState:ClearOverrideSymbol("swap_frozen")
		end,

		events =
		{   
			EventHandler("unfreeze", function(inst)
				inst.sg:GoToState("idle")
			end ),
		}
	}
}

--CommonStates.AddFrozenStates(states)
--[[
PP_CommonStates.AddActionStates(states, 
	{
		TimeEvent(0 * FRAMES, function(inst)
            inst:PerformBufferedAction()
        end),
	}, 
	"walk_pst", nil, nil, "idle", "walk_pst") --(states, timelines, anim, anim2, enteranim, loopanim, exitanim, noanim, events)]]
PP_CommonStates.AddCorpseStates(states, true, 
	--timelines = 
	{
		corpse =
		{
			TimeEvent(3*FRAMES,function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/dungbeetle/death") end ),
		},
		
		corpse_taunt =
		{
		
		},
	
	},
	--anims = 
	{
		corpse = "death",
		corpse_taunt = "fall_off_pst"
	},
	--sounds =
	{
		--corpse = inst.sounds.scream
	},
	--fns =
	{
	
	},
	nil,
	true
	) ---(states, isflying, timelines, anims, sounds, fns, events, nofx)
PP_CommonStates.AddJumpInStates(states, nil, "walk_pst")
PP_CommonStates.AddOpenGiftStates(states, "idle")
PP_CommonStates.AddSailStates(states, {}, "walk_pst", "idle")
local simpleanim = "walk_pst"
local simpleidle = "idle"
local simplemove = "walk"
if CurrentRelease.GreaterOrEqualTo(ReleaseID.IDs.R08_ROT_TURNOFTIDES) then
CommonStates.AddHopStates(states, false, {pre = simplemove.."_pre", loop = simplemove.."_loop", pst = simplemove.."_pst"}, nil, "death")
end
PP_CommonStates.AddSailorStates(states, nil, 
{
	plank_idle = simpleanim,
	plank_idle_loop = simpleidle,
	plank_idle_pst = simpleanim,
	
	plank_hop_pre = simplemove.."_pre",
	plank_hop = simplemove.."_loop",
	
	steer_pre = simpleanim,
	steer_idle = simpleidle,
	steer_turning = simpleanim,
	stop_steering = simpleanim,
	
	row = "walk_pst",
}
)

    
return StateGraph("dungbeetlep", states, events, "idle", actionhandlers)

