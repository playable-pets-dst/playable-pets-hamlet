require "behaviours/wander"
require "behaviours/follow"
require "behaviours/faceentity"
require "behaviours/chaseandattack"
require "behaviours/runaway"
require "behaviours/doaction"
--require "behaviours/choptree"
require "behaviours/findlight"
require "behaviours/panic"
require "behaviours/chattynode"
require "behaviours/leash"

local BrainCommon = require "brains/braincommon"

local MIN_FOLLOW_DIST = 2
local TARGET_FOLLOW_DIST = 5
local MAX_FOLLOW_DIST = 9
local MAX_WANDER_DIST = 20

local LEASH_RETURN_DIST = 10
local LEASH_MAX_DIST = 30

local START_RUN_DIST = 3
local STOP_RUN_DIST = 5
local MAX_CHASE_TIME = 10
local MAX_CHASE_DIST = 30
local SEE_LIGHT_DIST = 20
local TRADE_DIST = 20
local SEE_TREE_DIST = 15
local SEE_TARGET_DIST = 20
local SEE_FOOD_DIST = 10

local SEE_BURNING_HOME_DIST_SQ = 20*20

local COMFORT_LIGHT_LEVEL = 0.3

local KEEP_CHOPPING_DIST = 10

local RUN_AWAY_DIST = 5
local STOP_RUN_AWAY_DIST = 8

local function ShouldRunAway(inst, target)
    return not inst.components.trader:IsTryingToTradeWithMe(target)
end

local function GetTraderFn(inst)
    local x, y, z = inst.Transform:GetWorldPosition()
    local players = FindPlayersInRange(x, y, z, TRADE_DIST, true)
    for i, v in ipairs(players) do
        if inst.components.trader:IsTryingToTradeWithMe(v) then
            return v
        end
    end
end

local function KeepTraderFn(inst, target)
    return inst.components.trader:IsTryingToTradeWithMe(target)
end

local FINDFOOD_CANT_TAGS = { "outofreach" }
local function FindFoodAction(inst)
    if inst.sg:HasStateTag("busy") then
        return
    end

    if inst.components.inventory ~= nil and inst.components.eater ~= nil then
        local target = inst.components.inventory:FindItem(function(item) return inst.components.eater:CanEat(item) end)
        if target ~= nil then
            return BufferedAction(inst, target, ACTIONS.EAT)
        end
    end

	if inst.components.minigame_spectator ~= nil then
		return
	end

    local time_since_eat = inst.components.eater:TimeSinceLastEating()
    if time_since_eat ~= nil and time_since_eat <= TUNING.PIG_MIN_POOP_PERIOD * 2 then
        return
    end

    local noveggie = time_since_eat ~= nil and time_since_eat < TUNING.PIG_MIN_POOP_PERIOD * 4

    local target = FindEntity(inst,
        SEE_FOOD_DIST,
        function(item)
            return item:GetTimeAlive() >= 8
                and item.prefab ~= "mandrake"
                and item.components.edible ~= nil
                and (not noveggie or item.components.edible.foodtype == FOODTYPE.MEAT)
                and item:IsOnPassablePoint()
                and inst.components.eater:CanEat(item)
        end,
        nil,
        FINDFOOD_CANT_TAGS
    )
    if target ~= nil then
        return BufferedAction(inst, target, ACTIONS.EAT)
    end

    target = FindEntity(inst,
        SEE_FOOD_DIST,
        function(item)
            return item.components.shelf ~= nil
                and item.components.shelf.itemonshelf ~= nil
                and item.components.shelf.cantakeitem
                and item.components.shelf.itemonshelf.components.edible ~= nil
                and (not noveggie or item.components.shelf.itemonshelf.components.edible.foodtype == FOODTYPE.MEAT)
                and item:IsOnPassablePoint()
                and inst.components.eater:CanEat(item.components.shelf.itemonshelf)
        end,
        nil,
        FINDFOOD_CANT_TAGS
    )
    if target ~= nil then
        return BufferedAction(inst, target, ACTIONS.TAKEITEM)
    end
end

local function IsDeciduousTreeMonster(guy)
    return guy.monster and guy.prefab == "deciduoustree"
end

local CHOP_MUST_TAGS = { "CHOP_workable" }
local function FindDeciduousTreeMonster(inst)
    return FindEntity(inst, SEE_TREE_DIST / 3, IsDeciduousTreeMonster, CHOP_MUST_TAGS)
end

local function KeepChoppingAction(inst)
    return inst.tree_target ~= nil
        or (inst.components.follower.leader ~= nil and
            inst:IsNear(inst.components.follower.leader, KEEP_CHOPPING_DIST))
        or FindDeciduousTreeMonster(inst) ~= nil
end

local function StartChoppingCondition(inst)
    return inst.tree_target ~= nil
        or (inst.components.follower.leader ~= nil and
            inst.components.follower.leader.sg ~= nil and
            inst.components.follower.leader.sg:HasStateTag("chopping"))
        or FindDeciduousTreeMonster(inst) ~= nil
end

local function FindTreeToChopAction(inst)
    local target = FindEntity(inst, SEE_TREE_DIST, nil, CHOP_MUST_TAGS)
    if target ~= nil then
        if inst.tree_target ~= nil then
            target = inst.tree_target
            inst.tree_target = nil
        else
            target = FindDeciduousTreeMonster(inst) or target
        end
        return BufferedAction(inst, target, ACTIONS.CHOP)
    end
end

local function HasValidHome(inst)
    local home = inst.components.homeseeker ~= nil and inst.components.homeseeker.home or nil
    return home ~= nil
        and home:IsValid()
        and not (home.components.burnable ~= nil and home.components.burnable:IsBurning())
        and not home:HasTag("burnt")
end

local function GoHomeAction(inst)
    if not inst.components.follower.leader and
        HasValidHome(inst) and
        not inst.components.combat.target then
            return BufferedAction(inst, inst.components.homeseeker.home, ACTIONS.GOHOME)
    end
end

local function GetLeader(inst)
    return inst.components.follower.leader
end

local function GetHomePos(inst)
    return HasValidHome(inst) and inst.components.homeseeker:GetHomePos()
end

local function GetNoLeaderHomePos(inst)
    if GetLeader(inst) then
        return nil
    end
    return GetHomePos(inst)
end

local LIGHTSOURCE_TAGS = {"lightsource"}
local function GetNearestLightPos(inst)
    local light = GetClosestInstWithTag(LIGHTSOURCE_TAGS, inst, SEE_LIGHT_DIST)
    if light then
        return Vector3(light.Transform:GetWorldPosition())
    end
    return nil
end

local function GetNearestLightRadius(inst)
    local light = GetClosestInstWithTag(LIGHTSOURCE_TAGS, inst, SEE_LIGHT_DIST)
    if light then
        return light.Light:GetCalculatedRadius()
    end
    return 1
end

local function RescueLeaderAction(inst)
    return BufferedAction(inst, GetLeader(inst), ACTIONS.UNPIN)
end

local function WantsToGivePlayerPigTokenAction(inst)
	local leader = GetLeader(inst)
	return leader ~= nil and inst.components.follower:GetLoyaltyPercent() >= TUNING.PIG_FULL_LOYALTY_PERCENT and inst.components.inventory:Has(inst._pig_token_prefab, 1)
end

local function GivePlayerPigTokenAction(inst)
	local leader = GetLeader(inst)
	if leader ~= nil then
		local note = next(inst.components.inventory:GetItemByName(inst._pig_token_prefab, 1))
		if note ~= nil then
			return BufferedAction(inst, leader, ACTIONS.DROP, note)
		end
	end
end

local function GetFaceTargetFn(inst)
    return inst.components.follower.leader
end

local function KeepFaceTargetFn(inst, target)
    return inst.components.follower.leader == target
end

local function GetFaceTargetNearestPlayerFn(inst)
	local x, y, z = inst.Transform:GetWorldPosition()
	return FindClosestPlayerInRange(x, y, z, START_RUN_DIST + 1, true)
end

local function KeepFaceTargetNearestPlayerFn(inst, target)
    return GetFaceTargetNearestPlayerFn(inst) == target
end

local function SafeLightDist(inst, target)
    return (target:HasTag("player") or target:HasTag("playerlight")
            or (target.inventoryitem and target.inventoryitem:GetGrandOwner() and target.inventoryitem:GetGrandOwner():HasTag("player")))
        and 4
        or target.Light:GetCalculatedRadius() / 3
end

local function IsHomeOnFire(inst)
    return inst.components.homeseeker
        and inst.components.homeseeker.home
        and inst.components.homeseeker.home.components.burnable
        and inst.components.homeseeker.home.components.burnable:IsBurning()
        and inst:GetDistanceSqToInst(inst.components.homeseeker.home) < SEE_BURNING_HOME_DIST_SQ
end

local function WatchingMinigame(inst)
	return inst.components.minigame_spectator ~= nil and inst.components.minigame_spectator:GetMinigame()
end

local function WatchingMinigame_MinDist(inst)
	return inst.components.minigame_spectator:GetMinigame().components.minigame.watchdist_min
end
local function WatchingMinigame_TargetDist(inst)
	return inst.components.minigame_spectator:GetMinigame().components.minigame.watchdist_target
end
local function WatchingMinigame_MaxDist(inst)
	return inst.components.minigame_spectator:GetMinigame().components.minigame.watchdist_max
end

local function WatchingCheaters(inst)
    local minigame = WatchingMinigame(inst) or nil
    if minigame ~= nil and minigame._minigame_elites ~= nil then
        for k, v in pairs(minigame._minigame_elites) do
            if k:WasCheated() then
                return minigame
            end
        end
    end
end

local function CurrentContestTarget(inst)
    local stage = inst.npc_stage
    if stage.current_contest_target then
        return stage.current_contest_target
    else
        return stage
    end
end

local function MarkPost(inst)
    if inst.yotb_post_to_mark ~= nil then
        return BufferedAction(inst, inst.yotb_post_to_mark, ACTIONS.MARK)
    end
end

local function CollctPrize(inst)
    if inst.yotb_prize_to_collect ~= nil then
        local x,y,z = inst.yotb_prize_to_collect.Transform:GetWorldPosition()
        if y < 0.1 and y > -0.1 and not inst.yotb_prize_to_collect:HasTag("INLIMBO") then
            return BufferedAction(inst, inst.yotb_prize_to_collect, ACTIONS.PICKUP)
        end
    end
end

local function IsWatchingMinigameIntro(inst)
	local minigame = inst.components.minigame_spectator ~= nil and inst.components.minigame_spectator:GetMinigame() or nil
	return minigame ~= nil and minigame.sg ~= nil and minigame.sg:HasStateTag("intro")
end

local function GetGameLocation(inst)
	return inst.components.knownlocations:GetLocation("pigking")
end
---

local MIN_FOLLOW_DIST = 2
local TARGET_FOLLOW_DIST = 5
local MAX_FOLLOW_DIST = 9
local MAX_WANDER_DIST = 20

local LEASH_RETURN_DIST = 10
local LEASH_MAX_DIST = 30

local GO_HOME_DIST = 10

local START_FACE_DIST = 4
local KEEP_FACE_DIST = 8
local START_RUN_DIST = 3
local STOP_RUN_DIST = 5
local MAX_CHASE_TIME = 10  
local MAX_CHASE_DIST = 30 

local SEE_LIGHT_DIST = 20
local TRADE_DIST = 20
local SEE_TREE_DIST = 15
local SEE_TARGET_DIST = 20
local SEE_FOOD_DIST = 10
local SEE_MONEY_DIST = 6
local SEE_LIGHT_DIST = 20
local KEEP_CHOPPING_DIST = 10

local RUN_AWAY_DIST = 5
local STOP_RUN_AWAY_DIST = 8

local STOP_CHASE_CHAT = 35

local FAR_ENOUGH = 40

local BIG_NUMBER = 9999

local function getSpeechType(inst, speech)
    local line = speech.DEFAULT

    if inst.talkertype and speech[inst.talkertype] then
        line = speech[inst.talkertype]
        --print("Talkertype is not nil - "..inst.talkertype)
    end
    return line
end

local function getString(speech)
    if type(speech) == "table" then
        return speech[math.random(#speech)]
    else
        return speech
    end    
end

local function GetFaceTargetFn(inst)
    if inst.components.follower.leader then
        return inst.components.follower.leader
    end
    local x, y, z = inst.Transform:GetWorldPosition()
	return FindClosestPlayerInRange(x, y, z, START_RUN_DIST + 1, true)
end

local function KeepFaceTargetFn(inst, target)
    if inst.components.follower.leader then
        return inst.components.follower.leader == target
    end
    --print(GetFaceTargetFn(inst) == target)
    return GetFaceTargetFn(inst) == target
end


local function ShouldRunAway(inst, target)
    return not inst.components.trader:IsTryingToTradeWithMe(target)
end

local function GetTraderFn(inst)
    return FindEntity(inst, TRADE_DIST, function(target) return inst.components.trader:IsTryingToTradeWithMe(target) end, {"player"})
end

local function KeepTraderFn(inst, target)
    return inst.components.trader:IsTryingToTradeWithMe(target)
end

local function GreetAction(inst)
    if GetClosestInstWithTag("player", inst, START_FACE_DIST) then
        inst.sg:GoToState("alert")
        return true
    end
end

local function FindFoodAction(inst)
    local target = nil

	if inst.sg:HasStateTag("busy") or inst:HasTag("shopkeep") then
		return
	end
    
    if inst.components.inventory and inst.components.eater then
        target = inst.components.inventory:FindItem(function(item) return inst.components.eater:CanEat(item) end)
    end
    
    local time_since_eat = inst.components.eater:TimeSinceLastEating()
    local noveggie = time_since_eat and time_since_eat < TUNING.PIG_MIN_POOP_PERIOD*4
    
    if not target and (not time_since_eat or time_since_eat > TUNING.PIG_MIN_POOP_PERIOD*2) then
        target = FindEntity(inst, SEE_FOOD_DIST, function(item)
				if item:GetTimeAlive() < 8 then return false end
				if item.prefab == "mandrake" then return false end
				if noveggie and item.components.edible and item.components.edible.foodtype ~= "MEAT" then
					return false
				end
				if not item:IsOnValidGround() then
					return false
				end
				return inst.components.eater:CanEat(item) 
			end)
    end
    if target then
        return BufferedAction(inst, target, ACTIONS.EAT)
    end

    if not target and (not time_since_eat or time_since_eat > TUNING.PIG_MIN_POOP_PERIOD*2) then
        target = FindEntity(inst, SEE_FOOD_DIST, 
            function(item) 
                if not item.components.shelf then return false end
                if not item.components.shelf.itemonshelf or not item.components.shelf.cantakeitem then return false end
                if noveggie and item.components.shelf.itemonshelf.components.edible and item.components.shelf.itemonshelf.components.edible.foodtype ~= "MEAT" then
                    return false
                end
                if not item:IsOnValidGround() then
                    return false
                end
                return inst.components.eater:CanEat(item.components.shelf.itemonshelf) 
            end)
    end

    if target then
        return BufferedAction(inst, target, ACTIONS.TAKEITEM)
    end
end

local function KeepChoppingAction(inst)
    local keep_chop = inst.components.follower.leader and inst.components.follower.leader:GetDistanceSqToInst(inst) <= KEEP_CHOPPING_DIST*KEEP_CHOPPING_DIST
    local target = FindEntity(inst, SEE_TREE_DIST/3, function(item)
        return item.prefab == "deciduoustree" and item.monster and item.components.workable and item.components.workable.action == ACTIONS.CHOP 
    end)    
    if inst.tree_target ~= nil then target = inst.tree_target end

    return (keep_chop or target ~= nil)
end

local function StartChoppingCondition(inst)
    local start_chop = inst.components.follower.leader and inst.components.follower.leader.sg and inst.components.follower.leader.sg:HasStateTag("chopping")
    local target = FindEntity(inst, SEE_TREE_DIST/3, function(item) 
        return item.prefab == "deciduoustree" and item.monster and item.components.workable and item.components.workable.action == ACTIONS.CHOP 
    end)
    if inst.tree_target ~= nil then target = inst.tree_target end
    
    return (start_chop or target ~= nil)
end


local function FindTreeToChopAction(inst)
    local target = FindEntity(inst, SEE_TREE_DIST, function(item) return item.components.workable and item.components.workable.action == ACTIONS.CHOP end)
    if target then
        local decid_monst_target = FindEntity(inst, SEE_TREE_DIST/3, function(item)
            return item.prefab == "deciduoustree" and item.monster and item.components.workable and item.components.workable.action == ACTIONS.CHOP 
        end)
        if decid_monst_target ~= nil then 
            target = decid_monst_target 
        end
        if inst.tree_target then 
            target = inst.tree_target
            inst.tree_target = nil 
        end
        return BufferedAction(inst, target, ACTIONS.CHOP)
    end
end

local function HasValidHome(inst)
    return inst.components.homeseeker and inst.components.homeseeker.home and 
           not inst.components.homeseeker.home:HasTag("fire") and not inst.components.homeseeker.home:HasTag("burnt") and
           inst.components.homeseeker.home:IsValid()
end


local function GoHomeAction(inst)
    if not inst.components.follower.leader and HasValidHome(inst) and not inst.components.combat.target then
        return BufferedAction(inst, inst.components.homeseeker.home, ACTIONS.GOHOME)
    end
end

local function GetLeader(inst)
    return inst.components.follower.leader 
end

local function GetHomePos(inst)
    return HasValidHome(inst) and inst.components.homeseeker:GetHomePos()
end

local function GetNoLeaderHomePos(inst)
    if GetLeader(inst) then
        return nil
    end
    return GetHomePos(inst)
end

local CityPigpBrain = Class(Brain, function(self, inst)
    Brain._ctor(self, inst)
end)

local function FixStructure(inst)
    if inst.components.fixer then
        return BufferedAction(inst, inst.components.fixer.target, ACTIONS.FIX)
    end   
end

local function PoopTip(inst)
    inst.tipping = true
    return BufferedAction(inst, GetPlayer(), ACTIONS.SPECIAL_ACTION)
end


local function PayTax(inst)
    inst.taxing = true
    return BufferedAction(inst, GetPlayer(), ACTIONS.SPECIAL_ACTION)
end



local function DailyGift(inst)
    inst.daily_gifting = true
    return BufferedAction(inst, GetPlayer(), ACTIONS.SPECIAL_ACTION)
end

local function ShopkeeperSitAtDesk(inst)
    if inst.components.homeseeker then
        return BufferedAction(inst, inst.components.homeseeker.home, ACTIONS.SPECIAL_ACTION)
    end
end

local function shouldPanic(inst)
    local x,y,z = inst.Transform:GetWorldPosition()
    local ents = TheSim:FindEntities(x,y,z, 20, {"hostile"},{"city_pig"},{"LIMBO"}) 
    if #ents > 0 then
        --print("CAUSE PANIC")
        --dumptable(ents,1,1,1)
        return true
    end
        
    if inst.components.combat.target then
        local threat = inst.components.combat.target
        if threat then
            local myPos = Vector3(inst.Transform:GetWorldPosition() )
            local threatPos = Vector3(threat.Transform:GetWorldPosition() )
            local dist = distsq(threatPos, myPos)
            if dist < FAR_ENOUGH*FAR_ENOUGH then
                if dist > STOP_RUN_AWAY_DIST*STOP_RUN_AWAY_DIST then
                    return true
                end
            else
                inst.components.combat:GiveUp()
            end
        end
    end
    return false
end

local function shouldpanicwithspeech(inst)
    if shouldPanic(inst) then
        if math.random()<0.01 then                            
            local speechset = getSpeechType(inst, STRINGS.CITY_PIG_TALK_FLEE)
            local str = speechset[math.random(#speechset)]
            inst.components.talker:Say(str)               
        end
        return true
    end
end

local function needlight(inst)

    if inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS) and inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS).prefab == "torch" then
        return false
    end
    
    return true
end

local function ShouldGoHome(inst)
    local homePos = inst.components.knownlocations:GetLocation("home")
    local myPos = Vector3(inst.Transform:GetWorldPosition() )
    return (homePos and distsq(homePos, myPos) > GO_HOME_DIST*GO_HOME_DIST )
end

local function inCityLimits(inst)
    local x,y,z = inst.Transform:GetWorldPosition()
    local ents = TheSim:FindEntities(x,y,z, FAR_ENOUGH, {"citypossession"},{"city_pig"}) 
    if #ents > 0 then
        return true
    end
    if inst.components.combat.target then

        local speechset = getSpeechType(inst,STRINGS.CITY_PIG_TALK_STAYOUT)
        local str = speechset[math.random(#speechset)]
        inst.components.talker:Say(str)

        inst.components.combat:GiveUp()
    end
    return false
end

local function ReplaceStockCondition(inst)
    if not inst:HasTag("shopkeep") then
        return false
    end

    local x,y,z = inst.Transform:GetWorldPosition()
    local ents = TheSim:FindEntities(x,y,z, FAR_ENOUGH/2, {"shop_pedestal"},{"INTERIOR_LIMBO"}) 
    if #ents == 0 then
        return false
    end

    local changestock = nil

    for i,ent in ipairs(ents)do
        if ent.imagename and ent.imagename == "" and not ent:HasTag("justsellonce") and 
            (not ent.costimagename or ent.costimagename ~= "cost-nil") then
            changestock = ent
            break
        end
    end
    if not changestock then
        return false
    end

    inst.changestock = changestock    
    return true
end

local function ExtinguishfireAction(inst)

    if not inst:HasTag("guard") then
        return false
    end

    -- find fire
    local x,y,z = inst.Transform:GetWorldPosition()
    local ents = TheSim:FindEntities(x,y,z, FAR_ENOUGH/2, {"campfire"}) 
    if #ents == 0 then
        return false
    end

    local target = nil

    if target then
        return BufferedAction(inst, target, ACTIONS.MANUALEXTINGUISH)
    end
end

local function ReplenishStockAction(inst)

    if inst.changestock and inst.changestock:IsValid() then
        inst.sg:GoToState("idle")
        return BufferedAction(inst, inst.changestock, ACTIONS.STOCK)
    end
end

local function SafeLightDist(inst, target)
    return (target:HasTag("player") or target:HasTag("playerlight")
            or (target.inventoryitem and target.inventoryitem:GetGrandOwner() and target.inventoryitem:GetGrandOwner():HasTag("player")))
        and 4
        or target.Light:GetCalculatedRadius() / 3
end

--[[
function getfacespeechp(inst)
    --local econ = TheWorld.components.economy
	local speech = getSpeechType(inst, STRINGS.CITY_PIG_TALK_LOOKATWILSON)

    return speech        
end
]]
local function getfacespeechp(inst)

    local econ = PIG_TRADER

    local econprefab = inst.prefab
    if inst.econprefab then
        econprefab = inst.econprefab
    end  
    local desc = STRINGS.CITY_PIG_TALK_LOOKATWILSON_TRADER[inst.prefab]
    if desc then
        local speech = deepcopy(getSpeechType(inst, STRINGS.CITY_PIG_TALK_LOOKATWILSON_TRADER))

        for i,line in ipairs(speech)do
            speech[i] = string.format( line, desc )
        end

        return speech        
    else
        local speech = getSpeechType(inst, STRINGS.CITY_PIG_TALK_LOOKATWILSON)

        return speech
    end
end

local function RescueLeaderAction(inst)
    return BufferedAction(inst, GetLeader(inst), ACTIONS.UNPIN)
end

--[[
function CityPigpBrain:OnStart()
    --print(self.inst, "PigBrain:OnStart")
    local in_contest = WhileNode( function() return self.inst:HasTag("NPC_contestant") end, "In contest",
        PriorityNode({
--            IfNode(function() return self.inst.yotb_post_to_mark end, "mark post",
                DoAction(self.inst, CollctPrize, "collect prize", true ),
                DoAction(self.inst, MarkPost, "mark post", true ),   --)
            WhileNode( function() return self.inst.components.timer and self.inst.components.timer:TimerExists("contest_panic") end, "Panic Contest",
                ChattyNode(self.inst, "PIG_TALK_CONTEST_PANIC",
                    Panic(self.inst))),
            ChattyNode(self.inst, "PIG_TALK_CONTEST_OOOH",
                FaceEntity(self.inst, CurrentContestTarget, CurrentContestTarget ), 5, 15),
        }, 0.1))

	local watch_game = WhileNode( function() return WatchingMinigame(self.inst) end, "Watching Game",
        PriorityNode({
			IfNode(function() return WatchingMinigame(self.inst).components.minigame.gametype == "pigking_wrestling" end, "Is Pig King Wrestling",
				PriorityNode({
					ChattyNode(self.inst, "PIG_TALK_GAME_GOTO",
						Follow(self.inst, WatchingMinigame, WatchingMinigame_MinDist, WatchingMinigame_TargetDist, WatchingMinigame_MaxDist)),
					WhileNode(function() return IsWatchingMinigameIntro(self.inst) end, "Is Intro",
						PriorityNode({
							RunAway(self.inst, "minigame_participator", 5, 7),
							ChattyNode(self.inst, "PIG_TALK_FIND_MEAT",
								DoAction(self.inst, FindFoodAction )),
							FaceEntity(self.inst, WatchingMinigame, WatchingMinigame),
						}, 0.1)),
					ChattyNode(self.inst, "PIG_TALK_GAME_CHEER",
						RunAway(self.inst, "minigame_participator", 5, 7)),
					ChattyNode(self.inst, "PIG_TALK_FIND_MEAT",
						DoAction(self.inst, FindFoodAction )),
					ChattyNode(self.inst, "PIG_ELITE_SALTY",
						FaceEntity(self.inst, WatchingCheaters, WatchingCheaters ), 5, 15),
					ChattyNode(self.inst, "PIG_TALK_GAME_CHEER",
						FaceEntity(self.inst, WatchingMinigame, WatchingMinigame ), 5, 15),
				}, 0.1)
			),
			PriorityNode({
				ChattyNode(self.inst, "PIG_TALK_MISC_GAME_GOTO",
					Follow(self.inst, WatchingMinigame, WatchingMinigame_MinDist, WatchingMinigame_TargetDist, WatchingMinigame_MaxDist)),
				ChattyNode(self.inst, "PIG_TALK_MISC_GAME_CHEER",
					RunAway(self.inst, "minigame_participator", 5, 7)),
				ChattyNode(self.inst, "PIG_TALK_FIND_MEAT",
					DoAction(self.inst, FindFoodAction )),
				ChattyNode(self.inst, "PIG_TALK_MISC_GAME_CHEER",
					FaceEntity(self.inst, WatchingMinigame, WatchingMinigame ), 5, 15),
			}, 0.1),
        }, 0.1)
	)

    local day = WhileNode( function() return TheWorld.state.isday end, "IsDay",
        PriorityNode{
            ChattyNode(self.inst, "PIG_TALK_FOLLOWWILSON",
                Follow(self.inst, GetLeader, MIN_FOLLOW_DIST, TARGET_FOLLOW_DIST, MAX_FOLLOW_DIST)),
            IfNode(function() return GetLeader(self.inst) end, "has leader",
                ChattyNode(self.inst, "PIG_TALK_FOLLOWWILSON",
                    FaceEntity(self.inst, GetFaceTargetFn, KeepFaceTargetFn ))),

            Leash(self.inst, GetNoLeaderHomePos, LEASH_MAX_DIST, LEASH_RETURN_DIST),

            --ChattyNode(self.inst, "PIG_TALK_RUNAWAY_WILSON",
                --RunAway(self.inst, "player", START_RUN_DIST, STOP_RUN_DIST)),
            ChattyNode(self.inst, getSpeechType(self.inst, STRINGS.CITY_PIG_TALK_LOOKATWILSON),
                FaceEntity(self.inst, GetFaceTargetNearestPlayerFn, KeepFaceTargetNearestPlayerFn)),
            Wander(self.inst, GetNoLeaderHomePos, MAX_WANDER_DIST)
        }, .5)

    local night = WhileNode( function() return not TheWorld.state.isday end, "IsNight",
        PriorityNode{
            ChattyNode(self.inst, "PIG_TALK_RUN_FROM_SPIDER",
                RunAway(self.inst, "spider", 4, 8)),
            ChattyNode(self.inst, "PIG_TALK_FIND_MEAT",
                DoAction(self.inst, FindFoodAction )),
            RunAway(self.inst, "player", START_RUN_DIST, STOP_RUN_DIST, function(target) return ShouldRunAway(self.inst, target) end ),
            ChattyNode(self.inst, "PIG_TALK_GO_HOME",
                WhileNode( function() return not TheWorld.state.iscaveday or not self.inst:IsInLight() end, "Cave nightness",
                    DoAction(self.inst, GoHomeAction, "go home", true ))),
            WhileNode(function() return TheWorld.state.isnight and self.inst:IsLightGreaterThan(COMFORT_LIGHT_LEVEL) end, "IsInLight", -- wants slightly brighter light for this
                Wander(self.inst, GetNearestLightPos, GetNearestLightRadius, {
                    minwalktime = 0.6,
                    randwalktime = 0.2,
                    minwaittime = 5,
                    randwaittime = 5
                })
            ),
            ChattyNode(self.inst, "PIG_TALK_FIND_LIGHT",
                FindLight(self.inst, SEE_LIGHT_DIST, SafeLightDist)),
            ChattyNode(self.inst, "PIG_TALK_PANIC",
                Panic(self.inst)),
        }, 1)

    local root =
        PriorityNode(
        {
            BrainCommon.PanicWhenScared(self.inst, .25, "PIG_TALK_PANICBOSS"),
            WhileNode( function() return self.inst.components.hauntable and self.inst.components.hauntable.panic end, "PanicHaunted",
                ChattyNode(self.inst, "PIG_TALK_PANICHAUNT",
                    Panic(self.inst))),
            WhileNode(function() return self.inst.components.health.takingfiredamage end, "OnFire",
                ChattyNode(self.inst, "PIG_TALK_PANICFIRE",
                    Panic(self.inst))),
            ChattyNode(self.inst, "PIG_TALK_FIGHT",
                WhileNode( function() return self.inst.components.combat.target == nil or not self.inst.components.combat:InCooldown() end, "AttackMomentarily",
                    ChaseAndAttack(self.inst, MAX_CHASE_TIME, MAX_CHASE_DIST) )),
            ChattyNode(self.inst, "PIG_TALK_RESCUE",
                WhileNode( function() return GetLeader(self.inst) and GetLeader(self.inst).components.pinnable and GetLeader(self.inst).components.pinnable:IsStuck() end, "Leader Phlegmed",
                    DoAction(self.inst, RescueLeaderAction, "Rescue Leader", true) )),
            ChattyNode(self.inst, "PIG_TALK_FIGHT",
                WhileNode( function() return self.inst.components.combat.target and self.inst.components.combat:InCooldown() end, "Dodge",
                    RunAway(self.inst, function() return self.inst.components.combat.target end, RUN_AWAY_DIST, STOP_RUN_AWAY_DIST) )),
            WhileNode(function() return IsHomeOnFire(self.inst) end, "OnFire",
                ChattyNode(self.inst, "PIG_TALK_PANICHOUSEFIRE",
                    Panic(self.inst))),
            RunAway(self.inst, function(guy) return guy:HasTag("pig") and guy.components.combat and guy.components.combat.target == self.inst end, RUN_AWAY_DIST, STOP_RUN_AWAY_DIST ),
            ChattyNode(self.inst, "PIG_TALK_ATTEMPT_TRADE",
                FaceEntity(self.inst, GetTraderFn, KeepTraderFn)),
            ChattyNode(self.inst, "PIG_TALK_GIVE_GIFT",
                WhileNode( function() return WantsToGivePlayerPigTokenAction(self.inst) end, "Wants To Give Token", -- todo: check for death and valid
                    DoAction(self.inst, GivePlayerPigTokenAction, "Giving Token", true) )),
            in_contest,
			watch_game,
            day,
            night,
        }, .5)

    self.bt = BT(self.inst, root)
end]]

function CityPigpBrain:OnStart()
    --print(self.inst, "CityPigpBrain:OnStart")
      
    local day = WhileNode( function() return TheWorld.state.isday end, "IsDay",
        PriorityNode{
            Leash(self.inst, GetNoLeaderHomePos, LEASH_MAX_DIST, LEASH_RETURN_DIST),

            ChattyNode(self.inst, getfacespeechp(self.inst),
                    FaceEntity(self.inst, GetFaceTargetFn, KeepFaceTargetFn)),
				
            Wander(self.inst, GetNoLeaderHomePos, MAX_WANDER_DIST)
        },.5)
    
    local night = WhileNode( function() return not TheWorld.state.isday end, "IsNight",
        PriorityNode{
            ChattyNode(self.inst, getSpeechType(self.inst, STRINGS.CITY_PIG_TALK_RUN_FROM_SPIDER),
                RunAway(self.inst, "spider", 4, 8)),

            IfNode(function() return not self.inst:HasTag("guard") end, "gohome",
                ChattyNode(self.inst, getSpeechType(self.inst, STRINGS.CITY_PIG_TALK_GO_HOME),
                    DoAction(self.inst, GoHomeAction, "go home", true, 6 ))),
            
            WhileNode(function() return needlight(self.inst) end, "NeedLight",
                ChattyNode(self.inst, getSpeechType(self.inst, STRINGS.CITY_PIG_TALK_FIND_LIGHT),
                    FindLight(self.inst, SEE_LIGHT_DIST, SafeLightDist))),
            
            IfNode(function() return not self.inst:HasTag("guard") end, "panic",
                ChattyNode(self.inst, getSpeechType(self.inst, STRINGS.CITY_PIG_TALK_PANIC),                                
                Panic(self.inst))),
        },1)

    local root = 
        PriorityNode(
        {
            WhileNode(function() return self.inst.components.health.takingfiredamage and not self.inst.components.health:IsDead() end, "OnFire",
				ChattyNode(self.inst, getSpeechType(self.inst, STRINGS.CITY_PIG_TALK_PANICFIRE),
					Panic(self.inst))),

            -- FOLLOWER CODE
            ChattyNode(self.inst, getSpeechType(self.inst, STRINGS.CITY_PIG_TALK_FOLLOWWILSON), 
                Follow(self.inst, GetLeader, MIN_FOLLOW_DIST, TARGET_FOLLOW_DIST, MAX_FOLLOW_DIST)),
            IfNode(function() return GetLeader(self.inst) and not self.inst.components.health:IsDead() end, "has leader",
                ChattyNode(self.inst, getSpeechType(self.inst, STRINGS.CITY_PIG_TALK_FOLLOWWILSON),
                    FaceEntity(self.inst, GetFaceTargetFn, KeepFaceTargetFn ))),
            -- END FOLLOWER CODE

            ChattyNode(self.inst, getSpeechType(self.inst, STRINGS.CITY_PIG_TALK_FLEE),
                WhileNode(function() return shouldpanicwithspeech(self.inst) and not self.inst.components.health:IsDead() end, "Threat Panic",
                    Panic(self.inst) )),

            ChattyNode(self.inst, getSpeechType(self.inst, STRINGS.CITY_PIG_TALK_FLEE),
                WhileNode( function() return (self.inst.components.combat.target and not self.inst:HasTag("guard")) end, "Dodge",
                    RunAway(self.inst, function() return self.inst.components.combat.target end, RUN_AWAY_DIST, STOP_RUN_AWAY_DIST) ) ),                                

            ChattyNode(self.inst, getSpeechType(self.inst, STRINGS.CITY_PIG_TALK_FLEE),
                RunAway(self.inst, function(guy) return guy:HasTag("pig") and guy.components.combat and guy.components.combat.target == self.inst end, RUN_AWAY_DIST, STOP_RUN_AWAY_DIST )),


            IfNode(
                function()
                    if self.inst:HasTag("shopkeep") then
                        return false
                    end

                    local target = GetFaceTargetFn(self.inst)
                    
                    if target and target:HasTag("pigroyalty")and
                       (not self.inst.daily_gift or (GetTime() - self.inst.daily_gift > (TUNING.TOTAL_DAY_TIME * 1.5))) then
                            self.inst.daily_gift = GetTime()
                            return math.random() < 0.3
                    end
                    return false
                
                end, "daily_gift",
                DoAction(self.inst, DailyGift, "daily_gift", true, 6)
            ),

            -- for the mechanic pigs when they fix stuff
            ChattyNode(self.inst, getSpeechType(self.inst, STRINGS.CITY_PIG_TALK_FIX),
                WhileNode( function() return self.inst.components.fixer and self.inst.components.fixer.target end, "RepairStructure",
                     DoAction(self.inst, FixStructure, false, 6))),

            ChattyNode(self.inst, getSpeechType(self.inst, STRINGS.CITY_PIG_TALK_ATTEMPT_TRADE),
                FaceEntity(self.inst, GetTraderFn, KeepTraderFn)),
            
            day,

            night
        }, .5)
    
    self.bt = BT(self.inst, root) 
end

return CityPigpBrain