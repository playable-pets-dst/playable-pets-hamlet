require "behaviours/wander"
require "behaviours/follow"
require "behaviours/faceentity"
require "behaviours/chaseandattack"
require "behaviours/runaway"
require "behaviours/doaction"
--require "behaviours/choptree"
require "behaviours/findlight"
require "behaviours/panic"
require "behaviours/chattynode"
require "behaviours/leash"

-------------------------------------------------
--      Variables
-------------------------------------------------
local MIN_FOLLOW_DIST = 2
local TARGET_FOLLOW_DIST = 5
local MAX_FOLLOW_DIST = 9
local MAX_WANDER_DIST = 20

local LEASH_RETURN_DIST = 10
local LEASH_MAX_DIST = 30

local GO_HOME_DIST = 10

local START_FACE_DIST = 4
local KEEP_FACE_DIST = 8

-- local START_RUN_DIST = 3
-- local STOP_RUN_DIST = 5

local MAX_CHASE_TIME = 10  
local MAX_CHASE_DIST = 30 

local TRADE_DIST = 20
local SEE_TREE_DIST = 15
local SEE_FOOD_DIST = 10
local SEE_MONEY_DIST = 6

local KEEP_CHOPPING_DIST = 10

local RUN_AWAY_DIST = 5
local STOP_RUN_AWAY_DIST = 8

local FAR_ENOUGH = 40

-------------------------------------------------
--      Getters
-------------------------------------------------

local function GetHome(inst)
    return inst.components.homeseeker ~= nil and inst.components.homeseeker.home or nil
end

local function getSpeechType(inst,speech)
    local line = speech.DEFAULT

    if inst.talkertype and speech[inst.talkertype] then
        line = speech[inst.talkertype]
    end
    return line
end

local function getString(speech)
    if type(speech) == "table" then
        return speech[math.random(#speech)]
    else
        return speech
    end 
end

local function getfacespeechp(inst)
    local econ = PIG_TRADER

    local econprefab = inst.prefab
    if inst.econprefab then
        econprefab = inst.econprefab
    end  
    local desc = STRINGS.CITY_PIG_TALK_LOOKATWILSON_TRADER[inst.prefab]
    if desc then
        local speech = deepcopy(getSpeechType(inst, STRINGS.CITY_PIG_TALK_LOOKATWILSON_TRADER))

        for i,line in ipairs(speech)do
            speech[i] = string.format( line, desc )
        end

        return speech        
    else
        local speech = getSpeechType(inst, STRINGS.CITY_PIG_TALK_LOOKATWILSON)

        return speech
    end
end

local function GetFaceTargetFn(inst)
    if inst.components.follower.leader then
        return inst.components.follower.leader
    end
    local target = GetClosestInstWithTag("player", inst, START_FACE_DIST)
    if target and not target:HasTag("notarget") then    
        inst.sg:GoToState("alert")
        return target
    end
end

local function KeepFaceTargetFn(inst, target)
    if inst.components.follower.leader then
        return inst.components.follower.leader == target
    end
    return inst:IsNear(target, KEEP_FACE_DIST) and not target:HasTag("notarget")
end

local function ShouldRunAway(inst, target)
    return not inst.components.trader:IsTryingToTradeWithMe(target)
end

local function GetTraderFn(inst)
    return FindEntity(inst, TRADE_DIST, function(target) return inst.components.trader:IsTryingToTradeWithMe(target) end, {"player"})
end

local function KeepTraderFn(inst, target)
    return inst.components.trader:IsTryingToTradeWithMe(target)
end

local function GreetAction(inst)
    if GetClosestInstWithTag("player", inst, START_FACE_DIST) then
        inst.sg:GoToState("alert")
        return true
    end
end

local function FindFoodAction(inst)
    local target = nil

    if inst.sg:HasStateTag("busy") then
        return
    end
    
    if inst.components.inventory and inst.components.eater then
        target = inst.components.inventory:FindItem(function(item) return inst.components.eater:CanEat(item) end)
    end
    
    local time_since_eat = inst.components.eater:TimeSinceLastEating()
    local noveggie = time_since_eat and time_since_eat < TUNING.PIG_MIN_POOP_PERIOD*4
    
    if not target and (not time_since_eat or time_since_eat > TUNING.PIG_MIN_POOP_PERIOD*2) then
        target = FindEntity(inst, SEE_FOOD_DIST, function(item) 
                if item:GetTimeAlive() < 8 then return false end
                if item.prefab == "mandrake" then return false end
                if noveggie and item.components.edible and item.components.edible.foodtype ~= "MEAT" then
                    return false
                end
                if not item:IsOnValidGround() then
                    return false
                end
                return inst.components.eater:CanEat(item) 
            end)
    end
    if target then
        return BufferedAction(inst, target, ACTIONS.EAT)
    end

    if not target and (not time_since_eat or time_since_eat > TUNING.PIG_MIN_POOP_PERIOD*2) then
        target = FindEntity(inst, SEE_FOOD_DIST, function(item) 
                if not item.components.shelf then return false end
                if not item.components.shelf.itemonshelf or not item.components.shelf.cantakeitem then return false end
                if noveggie and item.components.shelf.itemonshelf.components.edible and item.components.shelf.itemonshelf.components.edible.foodtype ~= "MEAT" then
                    return false
                end
                if not item:IsOnValidGround() then
                    return false
                end
                return inst.components.eater:CanEat(item.components.shelf.itemonshelf) 
            end)
    end

    if target then
        return BufferedAction(inst, target, ACTIONS.PICKUP)
    end
end

local function FindMoneyAction(inst)
    local target = FindEntity(inst, SEE_MONEY_DIST, function(item)
                if not item:IsOnValidGround() then
                    return false
                end            
               -- local itempos = Vector3(item.Transform:GetWorldPosition())
               -- local instpos = Vector3(inst.Transform:GetWorldPosition())
                --and GetWorld().Pathfinder:IsClear(itempos.x, itempos.y, itempos.z, instpos.x, instpos.y, instpos.z,  {ignorewalls = false})
                return item.prefab == "oincp" or item.prefab == "oinc10p"
            end)    
    if target then        
        return BufferedAction(inst, target, ACTIONS.PICKUP)
    end
end

local function checknotangry(inst)
    return not inst:HasTag("angry_at_player") or inst:GetDistanceSqToInst(GetPlayer()) > 4*4  
end

local function KeepChoppingAction(inst)
    local keep_chop = inst.components.follower.leader and inst.components.follower.leader:GetDistanceSqToInst(inst) <= KEEP_CHOPPING_DIST*KEEP_CHOPPING_DIST
    local target = FindEntity(inst, SEE_TREE_DIST/3, function(item)
        return item.prefab == "deciduoustree" and item.monster and item.components.workable and item.components.workable.action == ACTIONS.CHOP 
    end)    
    if inst.tree_target ~= nil then target = inst.tree_target end

    return (keep_chop or target ~= nil)
end

local function StartChoppingCondition(inst)
    local start_chop = inst.components.follower.leader and inst.components.follower.leader.sg and inst.components.follower.leader.sg:HasStateTag("chopping")
    local target = FindEntity(inst, SEE_TREE_DIST/3, function(item) 
        return item.prefab == "deciduoustree" and item.monster and item.components.workable and item.components.workable.action == ACTIONS.CHOP 
    end)
    if inst.tree_target ~= nil then target = inst.tree_target end
    
    return (start_chop or target ~= nil)
end


local function FindTreeToChopAction(inst)
    local target = FindEntity(inst, SEE_TREE_DIST, function(item) return item.components.workable and item.components.workable.action == ACTIONS.CHOP end)
    if target then
        local decid_monst_target = FindEntity(inst, SEE_TREE_DIST/3, function(item)
            return item.prefab == "deciduoustree" and item.monster and item.components.workable and item.components.workable.action == ACTIONS.CHOP 
        end)
        if decid_monst_target ~= nil then 
            target = decid_monst_target 
        end
        if inst.tree_target then 
            target = inst.tree_target
            inst.tree_target = nil 
        end
        return BufferedAction(inst, target, ACTIONS.CHOP)
    end
end

local function HasValidHome(inst)
    return inst.components.homeseeker and 
       inst.components.homeseeker.home and 
       not inst.components.homeseeker.home:HasTag("fire") and
       not inst.components.homeseeker.home:HasTag("burnt") and
       inst.components.homeseeker.home:IsValid()
end


local function GuardGoHomeAction(inst)

    local homePos = inst.components.knownlocations:GetLocation("home")
    if homePos and 
       not inst.components.combat.target then
        return BufferedAction(inst, nil, ACTIONS.WALKTO, nil, homePos)
    end
end

local function GetLeader(inst)
    return inst.components.follower.leader 
end

local function GetHomePos(inst)
    return HasValidHome(inst) and inst.components.homeseeker:GetHomePos()
end

local function GetNoLeaderHomePos(inst)
    if GetLeader(inst) then
        return nil
    end
    return GetHomePos(inst)
end


local PigSoldierBrain = Class(Brain, function(self, inst)
    Brain._ctor(self, inst)
end)


local function shouldPanic(inst)

    local x,y,z = inst.Transform:GetWorldPosition()
    local ents = TheSim:FindEntities(x,y,z, 20, {"hostile"},{"city_pig"}) 
    if #ents > 0 then
        return true
    end

    if inst.components.combat.target then
        local threat = inst.components.combat.target
        if threat then
            local myPos = Vector3(inst.Transform:GetWorldPosition() )
            local threatPos = Vector3(threat.Transform:GetWorldPosition() )
            local dist = distsq(threatPos, myPos)
            if dist < FAR_ENOUGH*FAR_ENOUGH then
                if dist > STOP_RUN_AWAY_DIST*STOP_RUN_AWAY_DIST then
                    return true
                end
            else
                inst.components.combat:GiveUp()
            end
        end
    end
    return false
end

local function ShouldGoHome(inst)
    local homePos = inst.components.knownlocations:GetLocation("home")
    local myPos = Vector3(inst.Transform:GetWorldPosition() )

    return (homePos and distsq(homePos, myPos) > GO_HOME_DIST*GO_HOME_DIST )
end

local function inCityLimits(inst)
    local x,y,z = inst.Transform:GetWorldPosition()
    local ents = TheSim:FindEntities(x,y,z, FAR_ENOUGH, {"citypossession"},{"city_pig"}) 
    if #ents > 0 then
        return true
    end
    if inst.components.combat.target then

        local speechset = getSpeechType(inst,STRINGS.CITY_PIG_TALK_STAYOUT)
        local str = speechset[math.random(#speechset)]
        
        inst.sayline(inst, str)
        --inst.components.talker:Say(str)

        inst.components.combat:GiveUp()
    end
    return false
end

local function ExtinguishfireAction(inst)
    if not (inst:HasTag("guard") or inst:HasTag("footsoldier")) then
        return false
    end

    -- find fire
    local x,y,z = inst.Transform:GetWorldPosition()
    local valid_ents = {}
    local ents = TheSim:FindEntities(x,y,z, FAR_ENOUGH/2, nil, {"campfire"}, {"smolder", "fire"}) 
    if #ents > 0 then
        for i, v in ipairs(ents) do
            if not (v.components.inventoryitem and v.components.inventoryitem.owner) then
                table.insert(valid_ents, v)
            end
        end
    end

    if #valid_ents == 0 then
        return false
    end

    local target = valid_ents[1]

    if target then
        inst._extinguish_target = target --gross but I don't want to make a new action
        return BufferedAction(inst, target, ACTIONS.SMOTHER)
    end
end

local function playersproblem(inst)
    return false        
end

local function ShouldStandStill(inst)
    return (inst.type == "guard" and inst:IsNear(GetHome(inst), 1))
end

local function RescueLeaderAction(inst)
    return BufferedAction(inst, GetLeader(inst), ACTIONS.UNPIN)
end

function PigSoldierBrain:OnStart()
    --print(self.inst, "PigSoldierBrain:OnStart")
    --TODO figure out whats wrong with this code, they go braindead at dusk
    local day = WhileNode( function() return TheWorld.state.isday end, "IsDay",
        PriorityNode{
            --ChattyNode(self.inst, getSpeechType(self.inst,STRINGS.CITY_PIG_TALK_FIND_MEAT),
               -- DoAction(self.inst, FindFoodAction )),
            IfNode(function() return StartChoppingCondition(self.inst) end, "chop", 
                WhileNode(function() return KeepChoppingAction(self.inst) end, "keep chopping",
                    LoopNode{ 
                        ChattyNode(self.inst, getSpeechType(self.inst,STRINGS.CITY_PIG_TALK_HELP_CHOP_WOOD),
                            DoAction(self.inst, FindTreeToChopAction ))})),

            Leash(self.inst, GetNoLeaderHomePos, LEASH_MAX_DIST, LEASH_RETURN_DIST),

            IfNode(function() return not self.inst.alerted end, "greet",
                ChattyNode(self.inst, getfacespeechp(self.inst),
                    FaceEntity(self.inst, GetFaceTargetFn, KeepFaceTargetFn))),
            StandStill(self.inst, ShouldStandStill),
            Wander(self.inst, GetNoLeaderHomePos, MAX_WANDER_DIST)
        },.5)
        
    
    local night = WhileNode( function() return not TheWorld.state.isday end, "IsNight",
        PriorityNode{
            
            --ChattyNode(self.inst, getSpeechType(self.inst,STRINGS.CITY_PIG_TALK_FIND_MEAT),
                --DoAction(self.inst, FindFoodAction )),
            --RunAway(self.inst, "player", START_RUN_DIST, STOP_RUN_DIST, function(target) return ShouldRunAway(self.inst, target) end ),
            StandStill(self.inst, ShouldStandStill),
            Wander(self.inst, GetNoLeaderHomePos, MAX_WANDER_DIST)
        },1)

    local root = 
        PriorityNode(
        {
            --WhileNode(function() return self.inst.components.health.takingfiredamage end, "OnFire",
                --ChattyNode(self.inst, getSpeechType(self.inst,STRINGS.CITY_PIG_TALK_PANICFIRE),
                   -- Panic(self.inst))),

            --AttackWall(self.inst),
            -- GUARD SECTION
            WhileNode(function() return checknotangry(self.inst) end, "not angry",
                ChattyNode(self.inst, getSpeechType(self.inst,STRINGS.CITY_PIG_TALK_FIND_MONEY),
                    DoAction(self.inst, FindMoneyAction ))),

            ChattyNode(self.inst, getSpeechType(self.inst,STRINGS.CITY_PIG_TALK_PROTECT),
                WhileNode( function() return (self.inst.components.combat.target == nil or not self.inst.components.combat:InCooldown()) and not playersproblem(self.inst) end, "AttackMomentarily", -- and inCityLimits(self.inst)
                    ChaseAndAttack(self.inst, MAX_CHASE_TIME, MAX_CHASE_DIST) )),

            ChattyNode(self.inst, getSpeechType(self.inst,STRINGS.CITY_PIG_GUARD_TALK_RESCUE),
                WhileNode( function() return GetLeader(self.inst) and GetLeader(self.inst).components.pinnable and GetLeader(self.inst).components.pinnable:IsStuck() end, "Leader Phlegmed",
                    DoAction(self.inst, RescueLeaderAction, "Rescue Leader", true) )),

            ChattyNode(self.inst, getSpeechType(self.inst,STRINGS.CITY_PIG_TALK_EXTINGUISH),                
                    DoAction(self.inst, ExtinguishfireAction,"extinguish", true, 6 )),

            ChattyNode(self.inst, getSpeechType(self.inst,STRINGS.CITY_PIG_TALK_FIGHT),
                WhileNode( function() return self.inst.components.combat.target and self.inst.components.combat:InCooldown() end, "Dodge",
                    RunAway(self.inst, function() return self.inst.components.combat.target end, RUN_AWAY_DIST, STOP_RUN_AWAY_DIST) )),         

            -- FOLLOWER CODE
            ChattyNode(self.inst, getSpeechType(self.inst,STRINGS.CITY_PIG_TALK_FOLLOWWILSON), 
                Follow(self.inst, GetLeader, MIN_FOLLOW_DIST, TARGET_FOLLOW_DIST, MAX_FOLLOW_DIST)),
            IfNode(function() return GetLeader(self.inst) end, "has leader",
                ChattyNode(self.inst, getSpeechType(self.inst,STRINGS.CITY_PIG_TALK_FOLLOWWILSON),
                    FaceEntity(self.inst, GetFaceTargetFn, KeepFaceTargetFn ))),
            -- END FOLLOWER CODE

            WhileNode(function() return ShouldGoHome(self.inst) and self.inst:HasTag("guard") end, "ShouldGoHome",
                ChattyNode(self.inst, getSpeechType(self.inst, STRINGS.CITY_PIG_GUARD_TALK_GOHOME),
                    DoAction(self.inst, GuardGoHomeAction, "Go Home", true ) ) ),     

            -- END GUARD SECTION
            day,
            night
        }, .5)
    
    self.bt = BT(self.inst, root)
    
end

return PigSoldierBrain