return {
	--Grocery
	"onion",
	"tomato",
	"potato",
	"berries",
	"ice",
	"watermelon",
	"carrot",
	"drumstick",
	"eggplant",
	"corn",
	"pumpkin",
	"meat",
	"pomegranate",
	"cave_banana",
	"froglegs",
	"sweet_potato", --IA only
	"coconut", --IA only
	
	--Deli
	"ratatouille",
	"monsterlasagna",
	"pumpkincookie",
	"stuffedeggplant",
	"frogglebunwich",
	"honeynuggets",
	"perogies",
	"waffles",
	"meatballs",
	"honeyham",
	"turkeydinner",
	"dragonpie",
	
	--Bank
	"goldnugget",
	"oincp",
	"oinc10p",
	"oinc100p",
	
	--Arcane
	"icestaff",
	"firestaff",
	"amulet",
	"blueamulet",
	"purpleamulet",
	"livinglog",
	"armorslurper",
	"nightsword",
	"armor_sanity",
	"onemanband",
	
	--Oddities
	"silk",
	"gears",
	"steelwool",
	"mandrake",
	"wormlight",
	"deerclops_eyeball",
	"walrus_task",
	"bearger_fur",
	"goose_feather",
	"dragon_scales",
	"houndstooth",
	"coontail",
	"bamboo",
	
	--Spa
	"blue_cap",
	"green_cap",
	"bandage",
	"healingsalve",
	"petals",
	"antidotep",
	
	--Hat
	"winterhat",
	"tophat",
	"earmuffshat",
	"dragonheadhat",
	"dragonbodyhat",
	"dragontailhat",
	"walrushat",
	"molehat",
	"catcoonhat",
	"captainhat",
	"featherhat",
	"strawhat",
	"beefalohat",
	"rainhat",
	
	--Weapon
	"spear",
	"halberdp",
	"cutlass",
	"trap_teeth",
	"birdtrap",
	"trap",
	"coconade",
	"blowdart_pipe",
	"blowdart_sleep",
	"boomerang",
	
	--Floralist
	"poop",
	"pumpkin_seeds",
	--"asparagus_seeds",
	"eggplant_seeds",
	"durian_seeds",
	"corn_seeds",
	"dragonfruit_seeds",
	"watermelon_seeds",
	"seeds",
	"flowerhat",
	"acorn",
	"pinecone",
	"dug_berrybush",
	"dug_berrybush2",
	
	--General Store
	"boat_item",
	"anchor_item",
	"mast_item",
	"shovel",
	"pickaxe",
	"axe",
	"flint",
	"rocks",
	"cutgrass",
	"machete",
	"minerhat",
	"razor",
	"umbrella",
	"frabric",
	"bugnet",
	"fishingrod",
}


	
	
	
	
	


