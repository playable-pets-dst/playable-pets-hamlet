
return	{
	FOUNTAIN = {
		name = "pugalisk_fountain",
		bank = "fountain",
		build = "python_fountain",
		anim = "flow_loop",
		animbool = true,
		
		physics_type = "obstacle",
		physics_radius = 2,
		--physics_mass = 9999,
		
		sound = "dontstarve_DLC003/creatures/boss/pugalisk/fountain_LP",
		soundname = "burble",
		
		tags = {
			"structure",
			"pugalisk_fountain",
		},
	},
	LAWN_DECOR1 = {
		name = "lawndecor1",
		bank = "topiary",
		build = "topiary01_build",
		anim = "idle",
		
		physics_type = "obstacle",
		physics_radius = 0.5,
		--physics_mass = 9999,
		
		tags = {
			"structure",
		},
		
		hide_symbols =
		{
			"SNOW",
		},
		
		workable = true,
		works = 2,
		
		onhit = function(inst, worker)
			inst.AnimState:PlayAnimation("hit")
			inst.AnimState:PushAnimation("idle", false)
		end,
		
		onhammered = function(inst, worker)
			SpawnPrefab("collapse_small").Transform:SetPosition(inst.Transform:GetWorldPosition())
			inst.SoundEmitter:PlaySound("dontstarve/common/destroy_wood")
			inst:Remove()
		end,
		
		onbuilt = function(inst)
			inst.AnimState:PlayAnimation("place")
			inst.AnimState:PushAnimation("idle", false)
			inst.SoundEmitter:PlaySound("dontstarve_DLC003/common/objects/lawnornaments/repair")
		end,
		
		placerbank = "topiary",
		placerbuild = "topiary01_build",
		placeranim = "idle"
	},
	LAWN_DECOR2 = {
		name = "lawndecor2",
		bank = "topiary",
		build = "topiary02_build",
		anim = "idle",
		
		hide_symbols =
		{
			"SNOW",
		},
		
		physics_type = "obstacle",
		physics_radius = 0.5,
		--physics_mass = 9999,
		
		tags = {
			"structure",
		},
		
		workable = true,
		works = 2,
		
		onhit = function(inst, worker)
			inst.AnimState:PlayAnimation("hit")
			inst.AnimState:PushAnimation("idle", false)
		end,
		
		onhammered = function(inst, worker)
			SpawnPrefab("collapse_small").Transform:SetPosition(inst.Transform:GetWorldPosition())
			inst.SoundEmitter:PlaySound("dontstarve/common/destroy_wood")
			inst:Remove()
		end,
		
		onbuilt = function(inst)
			inst.AnimState:PlayAnimation("place")
			inst.AnimState:PushAnimation("idle", false)
			inst.SoundEmitter:PlaySound("dontstarve_DLC003/common/objects/lawnornaments/repair")
		end,
		
		placerbank = "topiary",
		placerbuild = "topiary02_build",
		placeranim = "idle"
	},
	LAWN_DECOR3 = {
		name = "lawndecor3",
		bank = "topiary",
		build = "topiary03_build",
		anim = "idle",

		hide_symbols =
		{
			"SNOW",
		},
		
		physics_type = "obstacle",
		physics_radius = 0.5,
		--physics_mass = 9999,
		
		tags = {
			"structure",
		},
		
		workable = true,
		works = 2,
		
		onhit = function(inst, worker)
			inst.AnimState:PlayAnimation("hit")
			inst.AnimState:PushAnimation("idle", false)
		end,
		
		onhammered = function(inst, worker)
			SpawnPrefab("collapse_small").Transform:SetPosition(inst.Transform:GetWorldPosition())
			inst.SoundEmitter:PlaySound("dontstarve/common/destroy_wood")
			inst:Remove()
		end,
		
		onbuilt = function(inst)
			inst.AnimState:PlayAnimation("place")
			inst.AnimState:PushAnimation("idle", false)
			inst.SoundEmitter:PlaySound("dontstarve_DLC003/common/objects/lawnornaments/repair")
		end,
		

		placerbank = "topiary",
		placerbuild = "topiary03_build",
		placeranim = "idle"
	},
	LAWN_DECOR4 = {
		name = "lawndecor4",
		bank = "topiary",
		build = "topiary04_build",
		anim = "idle",
		
		hide_symbols =
		{
			"SNOW",
		},
		
		physics_type = "obstacle",
		physics_radius = 0.5,
		--physics_mass = 9999,
		
		tags = {
			"structure",
		},
		
		workable = true,
		works = 2,
		
		onhit = function(inst, worker)
			inst.AnimState:PlayAnimation("hit")
			inst.AnimState:PushAnimation("idle", false)
		end,
		
		onhammered = function(inst, worker)
			SpawnPrefab("collapse_small").Transform:SetPosition(inst.Transform:GetWorldPosition())
			inst.SoundEmitter:PlaySound("dontstarve/common/destroy_wood")
			inst:Remove()
		end,
		
		onbuilt = function(inst)
			inst.AnimState:PlayAnimation("place")
			inst.AnimState:PushAnimation("idle", false)
			inst.SoundEmitter:PlaySound("dontstarve_DLC003/common/objects/lawnornaments/repair")
		end,
		
		placerbank = "topiary",
		placerbuild = "topiary04_build",
		placeranim = "idle"
	},
	LAWN_DECOR5 = {
		name = "lawndecor5",
		bank = "topiary",
		build = "topiary05_build",
		anim = "idle",
		
		hide_symbols =
		{
			"SNOW",
		},
		
		physics_type = "obstacle",
		physics_radius = 0.5,
		--physics_mass = 9999,
		
		tags = {
			"structure",
		},
		
		workable = true,
		works = 2,
		
		onhit = function(inst, worker)
			inst.AnimState:PlayAnimation("hit")
			inst.AnimState:PushAnimation("idle", false)
		end,
		
		onhammered = function(inst, worker)
			SpawnPrefab("collapse_small").Transform:SetPosition(inst.Transform:GetWorldPosition())
			inst.SoundEmitter:PlaySound("dontstarve/common/destroy_wood")
			inst:Remove()
		end,
		
		onbuilt = function(inst)
			inst.AnimState:PlayAnimation("place")
			inst.AnimState:PushAnimation("idle", false)
			inst.SoundEmitter:PlaySound("dontstarve_DLC003/common/objects/lawnornaments/repair")
		end,
		
		placerbank = "topiary",
		placerbuild = "topiary05_build",
		placeranim = "idle"
	},
	LAWN_DECOR6 = {
		name = "lawndecor6",
		bank = "topiary",
		build = "topiary06_build",
		anim = "idle",
		
		hide_symbols =
		{
			"SNOW",
		},
		
		physics_type = "obstacle",
		physics_radius = 0.5,
		--physics_mass = 9999,
		
		tags = {
			"structure",
		},
		
		workable = true,
		works = 2,
		
		onhit = function(inst, worker)
			inst.AnimState:PlayAnimation("hit")
			inst.AnimState:PushAnimation("idle", false)
		end,
		
		onhammered = function(inst, worker)
			SpawnPrefab("collapse_small").Transform:SetPosition(inst.Transform:GetWorldPosition())
			inst.SoundEmitter:PlaySound("dontstarve/common/destroy_wood")
			inst:Remove()
		end,
		
		onbuilt = function(inst)
			inst.AnimState:PlayAnimation("place")
			inst.AnimState:PushAnimation("idle", false)
			inst.SoundEmitter:PlaySound("dontstarve_DLC003/common/objects/lawnornaments/repair")
		end,
		
		placerbank = "topiary",
		placerbuild = "topiary06_build",
		placeranim = "idle"
	},
	LAWN_DECOR7 = {
		name = "lawndecor7",
		bank = "topiary",
		build = "topiary07_build",
		anim = "idle",
		
		hide_symbols =
		{
			"SNOW",
		},
		
		physics_type = "obstacle",
		physics_radius = 0.5,
		--physics_mass = 9999,
		
		tags = {
			"structure",
		},
		
		workable = true,
		works = 2,
		
		onhit = function(inst, worker)
			inst.AnimState:PlayAnimation("hit")
			inst.AnimState:PushAnimation("idle", false)
		end,
		
		onhammered = function(inst, worker)
			SpawnPrefab("collapse_small").Transform:SetPosition(inst.Transform:GetWorldPosition())
			inst.SoundEmitter:PlaySound("dontstarve/common/destroy_wood")
			inst:Remove()
		end,
		
		onbuilt = function(inst)
			inst.AnimState:PlayAnimation("place")
			inst.AnimState:PushAnimation("idle", false)
			inst.SoundEmitter:PlaySound("dontstarve_DLC003/common/objects/lawnornaments/repair")
		end,
		
		placerbank = "topiary",
		placerbuild = "topiary07_build",
		placeranim = "idle"
	},
	PIG_STATUE_HEAD = {
		name = "pig_ruins_head",
		bank = "pig_ruins_head",
		build = "ruins_giant_head",
		anim = "full",
		
		common_postint = function(inst)
			local color = 0.75 + math.random() * 0.25
			inst.AnimState:SetMultColour(color, color, color, 1)  
		end,
		
		physics_type = "obstacle",
		physics_radius = 1,
		--physics_mass = 9999,
		
		tags = {
			"structure",
		},
		
		workable = true,
		worktype = ACTIONS.MINE,
		works = TUNING.ROCKS_MINE,
		
		onhit = function(inst, worker, workleft)

			local pt = Point(inst.Transform:GetWorldPosition())
			if workleft <= 0 then
				if inst:HasTag("trggerdarttraps") then
					triggerdarts(inst)
				end

				inst.SoundEmitter:PlaySound("dontstarve/wilson/rock_break")
				if inst.components.lootdropper then
					inst.components.lootdropper:DropLoot(pt)
				end	
				inst:Remove()
			else
				if workleft < TUNING.ROCKS_MINE*(1/3) then
					inst.AnimState:PlayAnimation("low")
				elseif workleft < TUNING.ROCKS_MINE*(2/3) then
					inst.AnimState:PlayAnimation("med")
				else
					inst.AnimState:PlayAnimation("full")
				end
			end
		end,
		
		onhammered = function(inst, worker)
			inst.SoundEmitter:PlaySound("dontstarve/wilson/rock_break")
		end,
	},
	PIG_STATUE_PIGMAN= {
		name = "pig_ruins_pigman",
		bank = "statue_pig_ruins_pig",
		build = "statue_pig_ruins_pig",
		anim = "full",
		
		common_postint = function(inst)
			local color = 0.75 + math.random() * 0.25
			inst.AnimState:SetMultColour(color, color, color, 1)  
		end,
		
		physics_type = "obstacle",
		physics_radius = 1,
		--physics_mass = 9999,
		
		tags = {
			"structure",
		},
		
		workable = true,
		worktype = ACTIONS.MINE,
		works = TUNING.ROCKS_MINE,
		
		onhit = function(inst, worker, workleft)

			local pt = Point(inst.Transform:GetWorldPosition())
			if workleft <= 0 then
				if inst:HasTag("trggerdarttraps") then
					triggerdarts(inst)
				end

				inst.SoundEmitter:PlaySound("dontstarve/wilson/rock_break")
				if inst.components.lootdropper then
					inst.components.lootdropper:DropLoot(pt)
				end	
				inst:Remove()
			else
				if workleft < TUNING.ROCKS_MINE*(1/3) then
					inst.AnimState:PlayAnimation("low")
				elseif workleft < TUNING.ROCKS_MINE*(2/3) then
					inst.AnimState:PlayAnimation("med")
				else
					inst.AnimState:PlayAnimation("full")
				end
			end
		end,
		
		onhammered = function(inst, worker)
			inst.SoundEmitter:PlaySound("dontstarve/wilson/rock_break")
		end,
	},
	LAWN_DECOR_PIGKING = {
		name = "lawndecor_pigking",
		bank = "topiary",
		build = "topiary_pigking_build",
		anim = "idle",
		
		hide_symbols =
		{
			"SNOW",
		},
		
		physics_type = "obstacle",
		physics_radius = 2,
		--physics_mass = 9999,
		
		tags = {
			"structure",
		},
	},
	LAWN_DECOR_BEEFALO = {
		name = "lawndecor_beefalo",
		bank = "topiary",
		build = "topiary_beefalo_build",
		anim = "idle",
		
		hide_symbols =
		{
			"SNOW",
		},
		
		physics_type = "obstacle",
		physics_radius = 2,
		--physics_mass = 9999,
		
		tags = {
			"structure",
		},
	},
	LAWN_DECOR_WEREPIG = {
		name = "lawndecor_werepig",
		bank = "topiary",
		build = "topiary_werepig_build",
		anim = "idle",
		
		hide_symbols =
		{
			"SNOW",
		},
		
		physics_type = "obstacle",
		physics_radius = 2,
		--physics_mass = 9999,
		
		tags = {
			"structure",
		},
	},
	LAWN_DECOR_PIGMAN = {
		name = "lawndecor_pigman",
		bank = "topiary",
		build = "topiary_pigman_build",
		anim = "idle",
		
		hide_symbols =
		{
			"SNOW",
		},
		
		physics_type = "obstacle",
		physics_radius = 2,
		--physics_mass = 9999,
		
		tags = {
			"structure",
		},
	},
	WALL_HEDGE1 = {
		name = "wall_hedge1",
		bank = "hedge",
		build = "hedge1_build",
		anim = "growth1",
		
		face = 8,
		
		physics_type = "obstacle",
		physics_radius = 0.5,
		--physics_mass = 9999,
		
		tags = {
			"structure",
			"wall",
		},
		
		workable = true,
		works = 3,
		
		onhit = function(inst, worker)
			inst.SoundEmitter:PlaySound("dontstarve_DLC002/common/vine_hack")
		end,
		
		onhammered = function(inst, worker)
			SpawnPrefab("collapse_small").Transform:SetPosition(inst.Transform:GetWorldPosition())
			inst.SoundEmitter:PlaySound("dontstarve/common/destroy_wood")
			inst:Remove()
		end,
		
		gridnudger = true,
		
		placerbank = "hedge",
		placerbuild = "hedge1_build",
		placeranim = "growth1",
		placergridlock = true,
		placerface = "eight",
		placeritem = true
	},
	WALL_HEDGE2 = {
		name = "wall_hedge2",
		bank = "hedge",
		build = "hedge2_build",
		anim = "growth1",
		
		face = 8,
		
		physics_type = "obstacle",
		physics_radius = 0.5,
		--physics_mass = 9999,
		
		tags = {
			"structure",
			"wall",
		},
		
		workable = true,
		works = 3,
		
		onhit = function(inst, worker)
			inst.SoundEmitter:PlaySound("dontstarve_DLC002/common/vine_hack")
		end,
		
		onhammered = function(inst, worker)
			SpawnPrefab("collapse_small").Transform:SetPosition(inst.Transform:GetWorldPosition())
			inst.SoundEmitter:PlaySound("dontstarve/common/destroy_wood")
			inst:Remove()
		end,
		
		gridnudger = true,
		
		placerbank = "hedge",
		placerbuild = "hedge2_build",
		placeranim = "growth1",
		placergridlock = true,
		placerface = "eight",
		placeritem = true
	},
	WALL_HEDGE3 = {
		name = "wall_hedge3",
		bank = "hedge",
		build = "hedge3_build",
		anim = "growth1",
		
		face = 8,
		
		gridnudger = true,
		
		physics_type = "obstacle",
		physics_radius = 0.5,
		--physics_mass = 9999,
		
		tags = {
			"structure",
			"wall",
		},
		
		workable = true,
		works = 3,
		
		onhit = function(inst, worker)
			inst.SoundEmitter:PlaySound("dontstarve_DLC002/common/vine_hack")
		end,
		
		onhammered = function(inst, worker)
			SpawnPrefab("collapse_small").Transform:SetPosition(inst.Transform:GetWorldPosition())
			inst.SoundEmitter:PlaySound("dontstarve/common/destroy_wood")
			inst:Remove()
		end,
		
		placerbank = "hedge",
		placerbuild = "hedge3_build",
		placeranim = "growth1",
		placergridlock = true,
		placerface = "eight",
		placeritem = true
	},
}