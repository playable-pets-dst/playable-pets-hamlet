local SKINS = {
	--[[
	prefab = {
		skinname = {
			name = "", --this is for the purpose of printing
			fname = "Name of Skin",
			build = "build name",
			teen_build = "",
			adult_build = "adult build name",
			etc_build = "blah blah build", --add as many as these as you need
			fn = functionhere,
			owners = {}, --userids go here
			locked = true, --this skin can't be used.
		},
	},
	]]
	ancient_robot_spiderp = {
		rgb = {
			name = "rgb",
			fname = "RGB Ribs",
			build = "metal_spider",
			on_equip = function(inst)
				PlayablePets.MakeRainbow(inst)
			end,
			on_remove = function(inst)
				PlayablePets.MakeRainbow(inst, true)
			end,
			locked = true,
		},
	},
	ancient_robot_clawp = {
		rgb = {
			name = "rgb",
			fname = "RGB Arm",
			build = "metal_claw",
			on_equip = function(inst)
				PlayablePets.MakeRainbow(inst)
			end,
			on_remove = function(inst)
				PlayablePets.MakeRainbow(inst, true)
			end,
			locked = true,
		},
	},
	ancient_robot_headp = {
		rgb = {
			name = "rgb",
			fname = "RGB Head",
			build = "metal_head",
			on_equip = function(inst)
				PlayablePets.MakeRainbow(inst)
			end,
			on_remove = function(inst)
				PlayablePets.MakeRainbow(inst, true)
			end,
			locked = true,
		},
	},
	ancient_robot_legp = {
		rgb = {
			name = "rgb",
			fname = "RGB Leg",
			build = "metal_leg",
			on_equip = function(inst)
				PlayablePets.MakeRainbow(inst)
			end,
			on_remove = function(inst)
				PlayablePets.MakeRainbow(inst, true)
			end,
			locked = true,
		},
	},
	gnatp = {
		rgb = {
			name = "rgb",
			fname = "RGB Gnats",
			build = "gnat",
			on_equip = function(inst)
				PlayablePets.MakeRainbow(inst)
			end,
			on_remove = function(inst)
				PlayablePets.MakeRainbow(inst, true)
			end,
			locked = true,
		},
	},
	frog2p = {
		rgb = {
			name = "rgb",
			fname = "RGB Poison Dartfrog",
			build = "frog_treefrog_build",
			on_equip = function(inst)
				PlayablePets.MakeRainbow(inst)
			end,
			on_remove = function(inst)
				PlayablePets.MakeRainbow(inst, true)
			end,
			locked = true,
		},
	},
	pogp = {
		rgb = {
			name = "rgb",
			fname = "RGB Pog",
			build = "pog_actions",
			on_equip = function(inst)
				PlayablePets.MakeRainbow(inst)
			end,
			on_remove = function(inst)
				PlayablePets.MakeRainbow(inst, true)
			end,
			locked = true,
		},
	},
	antqueenp = {
		rgb = {
			name = "rgb",
			fname = "RGB Mant Queen",
			build = "crickant_queen_basics",
			on_equip = function(inst)
				PlayablePets.MakeRainbow(inst)
			end,
			on_remove = function(inst)
				PlayablePets.MakeRainbow(inst, true)
			end,
			locked = true,
		},
	},
	ancient_hulkp = {
		love = {
			name = "love",
			fname = "Love Puppy Hulk",
			build = "metal_hulk_shiny_build_06",
			mine_build = "metal_hulk_bomb_shiny_build_06",
		},
		rf = {
			name = "rf",
			fname = "Reforged Hulk",
			build = "metal_hulk_shiny_build_08",
			--mine_build = "metal_hulk_bomb_shiny_build_08", --TODO
		},
		rgb = {
			name = "rgb",
			fname = "RGB Hulk",
			build = "metal_hulk_build",
			on_equip = function(inst)
				PlayablePets.MakeRainbow(inst)
			end,
			on_remove = function(inst)
				PlayablePets.MakeRainbow(inst, true)
			end,
			locked = true,
		},
	},
	dungbeetlep = {
		jeremy = {
			name = "jeremy",
			fname = "Jeremy's Skin",
			build = "dung_beetle_jeremy_build",
		},
	},
	mandrakemanp = {
		odd = {
			name = "odd",
			fname = "Turnip Mandrake",
			build = "elderdrake_shiny_build_01",
		},
		rgb = {
			name = "rgb",
			fname = "RGB Mandrake",
			build = "elderdrake_build",
			on_equip = function(inst)
				PlayablePets.MakeRainbow(inst)
			end,
			on_remove = function(inst)
				PlayablePets.MakeRainbow(inst, true)
			end,
			locked = true,
		},
	},
	scorpionp = {
		dark = {
			name = "dark",
			fname = "Dark Scorpion",
			build = "scorpion_shiny_build_03",
		},
		rgb = {
			name = "rgb",
			fname = "RGB Scorpion",
			build = "scorpion_build",
			on_equip = function(inst)
				PlayablePets.MakeRainbow(inst)
			end,
			on_remove = function(inst)
				PlayablePets.MakeRainbow(inst, true)
			end,
			locked = true,
		},
	},
	heraldp = {
		odd = {
			name = "odd",
			fname = "Odd Herald",
			build = "herald_shiny_build_01",
		},
		gray = {
			name = "gray",
			fname = "Gray Herald",
			build = "herald_shiny_build_03",
		},
		dark = {
			name = "dark",
			fname = "Dark Herald",
			build = "herald_shiny_build_06",
			override = "herald_shiny_override_build_6",
		},
		rgb = {
			name = "rgb",
			fname = "RGB Herald",
			build = "ancient_spirit",
			on_equip = function(inst)
				PlayablePets.MakeRainbow(inst)
			end,
			on_remove = function(inst)
				PlayablePets.MakeRainbow(inst, true)
			end,
			locked = true,
		},
	},
	thunderbirdp = {
		odd = {
			name = "odd",
			fname = "Odd Thunderbird",
			build = "thunderbird_shiny_build_01",
		},
		dark = {
			name = "dark",
			fname = "Dark Thunderbird",
			build = "thunderbird_shiny_build_03",
		},
		rgb = {
			name = "rgb",
			fname = "RGB Thunderbird",
			build = "thunderbird",
			on_equip = function(inst)
				PlayablePets.MakeRainbow(inst)
			end,
			on_remove = function(inst)
				PlayablePets.MakeRainbow(inst, true)
			end,
			locked = true,
		},
	},
	vampirebatp = {
		dark = {
			name = "dark",
			fname = "Dark Vampire Bat",
			build = "bat_vamp_shiny_build_03",
		},
		rgb = {
			name = "rgb",
			fname = "RGB Vampire Bat",
			build = "bat_vamp_build",
			on_equip = function(inst)
				PlayablePets.MakeRainbow(inst)
			end,
			on_remove = function(inst)
				PlayablePets.MakeRainbow(inst, true)
			end,
			locked = true,
		},
	},
	citypig_malep = {
		banker = {
			name = "banker",
			fname = "Banker",
			build = "pig_banker",
		},
		collector = {
			name = "collector",
			fname = "Collector",
			build = "pig_collector"
		},
		hunter = {
			name = "hunter",
			fname = "Hunter",
			build = "pig_hunter",
		},
		mechanic = {
			name = "mechanic",
			fname = "Mechanic",
			build = "pig_mechanic"
		},
		professor = {
			name = "professor",
			fname = "Professor",
			build = "pig_professor"
		},
		usher = {
			name = "usher",
			fname = "Usher",
			build = "pig_usher"
		},
		farmer = {
			name = "farmer",
			fname = "Farmer",
			build = "pig_farmer"
		},
		miner = {
			name = "miner",
			fname = "Collector",
			build = "pig_miner"
		},
	},
	citypig_femalep = {
		florist = {
			name = "florist",
			fname = "Florist",
			build = "pig_florist"
		},
		beauty = {
			name = "beauty",
			fname = "Beautician",
			build = "pig_beautician"
		},
		storeowner = {
			name = "storeowner",
			fname = "Store owner",
			build = "pig_storeowner"
		},
		hatmaker = {
			name = "hatmaker",
			fname = "Hatmaker",
			build = "pig_hatmaker"
		},
		erudite = {
			name = "erudite",
			fname = "Erudite",
			build = "pig_erudite"
		},
	},
	citypig_guardp = {
		city = {
			name = "city",
			fname = "City Guard",
			build = "pig_royalguard_2",
		},
		royal = {
			name = "royal",
			fname = "Royal Guard",
			build = "pig_royalguard_3",
		},
		rich = {
			name = "rich",
			fname = "Rich Guard",
			build = "pig_royalguard_rich",
		},
		richcity = {
			name = "richcity",
			fname = "Rich City Guard",
			build = "pig_royalguard_rich_2",
		},
	},
	pangoldenp = {
		blue = {
			name = "blue",
			fname = "Blue Pangolden",
			build = "pango_action",
			hue = 0.5,
		},
		green = {
			name = "green",
			fname = "Green Pangolden",
			build = "pango_action",
			hue = 0.2,
		},
		purple = {
			name = "purple",
			fname = "Purple Pangolden",
			build = "pango_action",
			hue = 0.6,
		},
		pink = {
			name = "pink",
			fname = "Pink Pangolden",
			build = "pango_action",
			hue = 0.8,
		},
		rgb = {
			name = "rgb",
			fname = "RGB Pangolden",
			build = "pango_action",
			on_equip = function(inst)
				PlayablePets.MakeRainbow(inst)
			end,
			on_remove = function(inst)
				PlayablePets.MakeRainbow(inst, true)
			end,
			locked = true,
		},
	},	
	glowflyp = {
		rf = {
			name = "rf",
			fname = "Forged Glowfly",
			build = "glowfly_shiny_build_08",
			adult_build = "rabid_beetle_shiny_build_08"
		},
		rgb = {
			name = "rgb",
			fname = "RGB Glowfly",
			build = "lantern_fly",
			adult_build = "rabid_beetle",
			on_equip = function(inst)
				PlayablePets.MakeRainbow(inst)
			end,
			on_remove = function(inst)
				PlayablePets.MakeRainbow(inst, true)
			end,
			locked = true,
		},
	},	
	rabidbeetlep = {
		rf = {
			name = "rf",
			fname = "Forged Rabid Beetle",
			build = "rabid_beetle_shiny_build_08"
		},
		val = {
			name = "val",
			fname = "Valentine Rabid Beetle",
			build = "rabid_beetle_valentines"
		},
		polar = {
			name = "polar",
			fname = "Polar Flea",
			build = "polar_flea"
		},
		rgb = {
			name = "rgb",
			fname = "RGB Rabid Beetle",
			build = "rabid_beetle",
			on_equip = function(inst)
				PlayablePets.MakeRainbow(inst)
			end,
			on_remove = function(inst)
				PlayablePets.MakeRainbow(inst, true)
			end,
			locked = true,
		},
	},
	weevolep = {
		aphid = {
			name = "aphid",
			fname = "Aphid",
			build = "aphid"
		},
		rgb = {
			name = "rgb",
			fname = "RGB Weevole",
			build = "weevole",
			on_equip = function(inst)
				PlayablePets.MakeRainbow(inst)
			end,
			on_remove = function(inst)
				PlayablePets.MakeRainbow(inst, true)
			end,
			locked = true,
		},
	},
	chickenp = {
		blue = {
			name = "blue",
			fname = "Blue Chicken",
			build = "chicken",
			hue = 0.6,
		},
		rgb = {
			name = "rgb",
			fname = "RGB Chicken",
			build = "chicken",
			on_equip = function(inst)
				PlayablePets.MakeRainbow(inst)
			end,
			on_remove = function(inst)
				PlayablePets.MakeRainbow(inst, true)
			end,
			locked = true,
		},
	},
	hippop = {
		badlands = {
			name = "badlands",
			fname = "Badlands Hippo",
			build = "hippo_badlands",
		},
	},

}

return SKINS