local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--
---------------------------

--Don't add assets unless absolutely needed.
local assets = 
{
	Asset("ANIM", "anim/snake_basic.zip"),
	Asset("ANIM", "anim/snake_water.zip"),	
	Asset("ANIM", "anim/snake_scaly_build.zip"),	
}
--Might be completely pointless here. I don't know.
local prefabs = 
{	

}
	
--Note: Becareful when adding starting items.
--People are likely to spam changecharacter to get more of them.	
local start_inv = 
{
	--'prefab',
	--"spear",
}

local start_inv2 =
{
   --"beehome",
}

if MOBHOUSE == "Enable1" or MOBHOUSE == "Enable3" then
	start_inv = start_inv2
end
-----------------------
--Stats--
local mob = 
{
	health = 250,
	hunger = 150,
	hungerrate = TUNING.WILSON_HUNGER_RATE, 
	sanity = 100,
	runspeed = 5,
	walkspeed = 5,
	damage = 30*2,
	range = 3,
	bank = "snake",
	build = "snake_scaly_build",
	shiny = "snake_scaly",
	--build2 = "bee_guard_puffy_build",
	scale = 1,
	stategraph = "SGsnake2p",
	minimap = "snake2p.tex",
	
}
-----------------------
--Loot that drops when you die, duh.
SetSharedLootTable('snakep',
{
    {'monstermeat',  1.00},
})

local function GetExcludeTags(inst)
	if TheNet:GetPVPEnabled() then
		return {"lightningimmune", "lightningrod", "notarget"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion", "lightningimmune", "lightningrod", "notarget"}
	else	
		return {"player", "companion", "lightningimmune", "lightningrod", "notarget"}
	end
end

local sounds = {
	default = {
		idle = "dontstarve_DLC002/creatures/snake/idle",
		pre_attack = "dontstarve_DLC002/creatures/snake/pre-attack",
		attack = "dontstarve_DLC002/creatures/snake/attack",
		hurt = "dontstarve_DLC002/creatures/snake/hurt",
		taunt = "dontstarve_DLC002/creatures/snake/taunt",
		death = "dontstarve_DLC002/creatures/snake/death",
		sleep = "dontstarve_DLC002/creatures/snake/sleep",
		move = "dontstarve_DLC002/creatures/snake/move",
	},
	amphibious = {
		idle = "dontstarve_DLC002/creatures/snake/idle",
		pre_attack = "dontstarve_DLC002/creatures/snake/pre-attack",
		attack = "dontstarve_DLC002/creatures/snake/attack",
		hurt = "dontstarve_DLC002/creatures/snake/hurt",
		taunt = "dontstarve_DLC002/creatures/snake/taunt",
		death = "dontstarve_DLC002/creatures/snake/death",
		sleep = "dontstarve_DLC002/creatures/snake/sleep",
		move = "dontstarve_DLC002/creatures/snake/move",	
	},
}

local function onattacked(inst, data)
    inst.components.combat:SetTarget(data.attacker)
    inst.components.combat:ShareTarget(data.attacker, 30, function(dude) return dude:HasTag("snake") and not dude.components.health:IsDead() end, 30)
end

local function onhitother2(inst, other)
	if inst.isaltattacking then
		PlayablePets.SetPoison(inst, other)
	end
    inst.components.combat:SetTarget(other)
    inst.components.combat:ShareTarget(other, 30, function(dude) return dude:HasTag("snake") and not dude.components.health:IsDead() end, 30)
end
------------------------------------------------------
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
end
-------------------------------------------------------
local common_postinit = function(inst) 
	inst.MiniMapEntity:SetIcon(mob.minimap)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false)
	  --ThePlayer.HUD.controls.status.brain:Hide()
		if MONSTERHUNGER == "Disable" then
			--ThePlayer.HUD.controls.status.stomach:Hide()
		end
   end
end)
	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)

	PlayablePets.SetNightVision(inst)
	
end

-----------------------------
--Forge

local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	inst.components.health:SetMaxHealth(PPHAM_FORGE.SNAKE2.HEALTH)
	
	inst.components.combat:SetDefaultDamage(PPHAM_FORGE.SNAKE2.DAMAGE)
	inst.components.combat:SetAttackPeriod(PPHAM_FORGE.SNAKE2.ATTACKPERIOD)
	
	inst.components.locomotor.walkspeed = PPHAM_FORGE.SNAKE2.WALKSPEED
    inst.components.locomotor.runspeed = PPHAM_FORGE.SNAKE2.RUNSPEED
	
	inst.components.combat:SetRange(PPHAM_FORGE.SNAKE2.ATTACK_RANGE, PPHAM_FORGE.SNAKE2.HIT_RANGE)
	--inst.components.combat:SetAreaDamage(PPHAM_FORGE.TREEGUARD.AOE_RANGE, PPHAM_FORGE.TREEGUARD.AOE_DMGMULT)
	
	inst.mobsleep = false
	
	--inst.acidmult = 1.25
	
	inst:DoTaskInTime(0, function(inst)
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"books", "staves"})
	end)
	
	--inst.components.combat.onhitotherfn = OnForgeHitOther
	
	inst.components.combat:SetDamageType(1)
	
	inst.components.revivablecorpse.revivespeedmult = 1
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end
-----------------------------------------------------------------------

local function OnWaterChange(inst, onwater)
  if not inst:HasTag("playerghost") then

  if onwater then
    if inst.DynamicShadow then
      inst.DynamicShadow:Enable(false)
    end

    inst.onwater = true
    inst.sg:GoToState("submerge")
    inst.DynamicShadow:Enable(false)
    
  else
    --if inst.onwater then
        inst.sg:GoToState("emerge")
    --end
    inst.onwater = false 
  end

  local splash = SpawnPrefab("splash_water")
  local ent_pos = Vector3(inst.Transform:GetWorldPosition())
  splash.Transform:SetPosition(ent_pos.x, ent_pos.y, ent_pos.z)

  if inst.sg then
    inst.sg:GoToState("idle")
  end
  end
end

local master_postinit = function(inst)
	------------------------------------------
	--Stats--
    PlayablePets.SetCommonStats(inst, mob, nil, true)
    PlayablePets.SetCommonWeatherResistances(inst, 60, 20, .25) --heat, cold, wetness
	----------------------------------
	--Loot drops--
	PlayablePets.SetCommonLootdropper(inst, "snake2p")
	----------------------------------
	--Tags--
    inst:AddTag("monster")
	inst:AddTag("snake")
	inst:AddTag("amphibious")
	
	inst.mobsleep = true
	inst.taunt = true
	--inst.taunt2 = true
	
	inst.mobplayer = true
	inst.ghostbuild = "ghost_monster_build"
	
	inst.sounds = sounds.amphibious
	--inst.altattack_ham = true

	inst.isshiny = 0
	
	inst:DoTaskInTime(3, function(inst)
		if SW_PP_MODE == true then		
			inst:AddComponent("tiletracker")
			inst.components.tiletracker:SetOnWaterChangeFn(OnWaterChange)
			inst.components.tiletracker:Start()
		end
	end)
	
	local body_symbol = "hound_body"
	inst.poisonsymbol = body_symbol
	MakeMediumBurnableCharacter(inst, body_symbol)
    MakeMediumFreezableCharacter(inst, body_symbol)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)

	PlayablePets.SetCommonStatResistances(inst, nil, 0.5, 0) --fire, acid, poison
	----------------------------------
	--Eater--

	inst.components.eater:SetDiet({ FOODTYPE.MEAT, FOODTYPE.MEAT }, { FOODTYPE.MEAT, FOODTYPE.MEAT }) 
    inst.components.eater:SetAbsorptionModifiers(1,1,1) --This multiplies food stats.
	inst.components.eater:SetCanEatHorrible() --check to make them eat monster meat.
	inst.components.eater.strongstomach = true -- can eat monster meat!
	--inst.components.eater.eatwholestack = true --should eat the entire stack and get all the benefits.
	inst.components.eater.ignoresspoilage = true -- Can eat spoiled food without consequence. Might keep food stats as if fresh.
	---------------------------------
	--Physics and Scale--
	
	inst.DynamicShadow:SetSize(1.5, 0.75)
	inst.DynamicShadow:Enable(false)
    MakeCharacterPhysics(inst, 10, .5)
    inst.Transform:SetFourFaced()	
	PlayablePets.SetAmphibious(inst, "snake", "snake_water")
	---------------------------------
	--Listeners--
	
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	inst:ListenForEvent("attacked", onattacked)
	--inst.components.combat.onhitotherfn = onhitother2
	---------------------------------
	--Functions that saves and loads data.

    inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
    ------------------------------------------------------
	--Light and Character Functions. Don't Touch.--
	
	inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob, nil, nil, nil, nil, true) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman, amphibious)
    end)
	
    return inst
	
end

return MakePlayerCharacter("snake2p", prefabs, assets, common_postinit, master_postinit, start_inv)
