local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--
---------------------------

--Don't add assets unless absolutely needed.
local assets = 
{
	Asset("ANIM", "anim/frog.zip"),
	Asset("ANIM", "anim/frog_water.zip"),	
	Asset("ANIM", "anim/frog_treefrog_build.zip"),
}
--Might be completely pointless here. I don't know.
local prefabs = 
{	

}
	
--Note: Becareful when adding starting items.
--People are likely to spam changecharacter to get more of them.	
local start_inv = 
{
	--'prefab',
	--"spear",
}

local start_inv2 =
{
   --"beehome",
}

if MOBHOUSE == "Enable1" or MOBHOUSE == "Enable3" then
	start_inv = start_inv2
end
-----------------------
--Stats--
local mob = 
{
	health = 100,
	hunger = 50,
	hungerrate = TUNING.WILSON_HUNGER_RATE, 
	sanity = 100,
	runspeed = 8,
	walkspeed = 4,
	damage = 15*2,
	range = 4,
	bank = "frog",
	build = "frog_treefrog_build",
	shiny = "frog_treefrog",
	--build2 = "bee_guard_puffy_build",
	scale = 1,
	stategraph = "SGfrog2p",
	minimap = "frogp.tex",
	
}

--Loot that drops when you die, duh.
SetSharedLootTable('frog2p',
{
    {'froglegs',             1.00},
	{'venomglandp',             1.00},  
})

local sounds = {
	base = {
		grunt = "dontstarve/frog/grunt",
		walk = "dontstarve/frog/walk",
		spit = "dontstarve/frog/attack_spit",
		voice = "dontstarve/frog/attack_voice",
		splat = "dontstarve/frog/splat",
		die = "dontstarve/frog/die",	
		wake = "dontstarve/frog/wake",
	},
	poison = {
		grunt = "dontstarve_DLC003/creatures/enemy/frog_poison/grunt",
		walk = "dontstarve/frog/walk",
		spit = "dontstarve_DLC003/creatures/enemy/frog_poison/attack_spit",
		voice = "dontstarve_DLC003/creatures/enemy/frog_poison/attack_spit",		
		splat = "dontstarve/frog/splat",
		die = "dontstarve_DLC003/creatures/enemy/frog_poison/death",
		wake = "dontstarve/frog/wake",
	},	
}

local function OnWaterChange(inst, onwater)
	if not inst:HasTag("playerghost") then
		if onwater then
			inst.onwater = true
			inst.sg:GoToState("submerge")
			inst.DynamicShadow:Enable(false)
			inst.components.locomotor.walkspeed = 3
		else
			inst.onwater = false		
			inst.sg:GoToState("emerge")
			inst.DynamicShadow:Enable(true)
			inst.components.locomotor.walkspeed = 4
		end
	end	
end

local function GetExcludeTags(inst)
	if TheNet:GetPVPEnabled() then
		return {"lightningimmune", "lightningrod", "notarget"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion", "lightningimmune", "lightningrod", "notarget"}
	else	
		return {"player", "companion", "lightningimmune", "lightningrod", "notarget"}
	end
end

local function onattacked(inst, data)
    inst.components.combat:SetTarget(data.attacker)
    inst.components.combat:ShareTarget(data.attacker, 30, function(dude) return dude:HasTag("frog") and not dude.components.health:IsDead() end, 30)
end

local function OnHitOther(inst, other)
	PlayablePets.SetPoison(inst, other)
    inst.components.combat:SetTarget(other)
    inst.components.combat:ShareTarget(other, 30, function(dude) return dude:HasTag("frog") and not dude.components.health:IsDead() end, 30)
    if other.components.inventory ~= nil then
		inst.components.thief:StealItem(other)
	end
end
------------------------------------------------------
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
end
-------------------------------------------------------

local common_postinit = function(inst) 
	inst.MiniMapEntity:SetIcon(mob.minimap)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false)
	  --ThePlayer.HUD.controls.status.brain:Hide()
		if MONSTERHUNGER == "Disable" then
			--ThePlayer.HUD.controls.status.stomach:Hide()
		end
   end
end)
	--inst:WatchWorldState( "onphasechanged", function() SetNightVision(inst) end)
	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst)
	
end

-------------Forge------------------------------------------
local ex_fns = require "prefabs/player_common_extensions"

local function OnAttacked_Forge(inst, data)
	local attacker = data.attacker
    if attacker and inst:IsNear(attacker, 4) and attacker.components.combat and attacker.components.health and not attacker.components.health:IsDead() then
		PlayablePets.SetPoison(inst, data.attacker)
	end
end

local function SetForge(inst)
	inst.components.health:SetMaxHealth(PPHAM_FORGE.FROG2.HEALTH)
	
	inst.components.combat:SetDefaultDamage(PPHAM_FORGE.FROG2.DAMAGE)
	inst.components.combat:SetAttackPeriod(PPHAM_FORGE.FROG2.ATTACKPERIOD)
	
	inst.components.locomotor.walkspeed = PPHAM_FORGE.FROG2.WALKSPEED
    inst.components.locomotor.runspeed = PPHAM_FORGE.FROG2.RUNSPEED
	
	inst.components.combat:SetRange(PPHAM_FORGE.FROG2.ATTACK_RANGE, PPHAM_FORGE.FROG2.HIT_RANGE)
	--inst.components.combat:SetAreaDamage(PPHAM_FORGE.HIPPO.AOE_RANGE, PPHAM_FORGE.HIPPO.AOE_DMGMULT)
	
	inst.mobsleep = false
	
	--inst.acidmult = 1.25
	
	inst:DoTaskInTime(0, function(inst)
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"books", "staves", "darts"})
	end)
	
	inst:ListenForEvent("attacked", OnAttacked_Forge) --Shows head when hats make heads disappear.
	
	--inst.components.combat.onhitotherfn = OnForgeHitOther
	
	inst.components.combat:SetDamageType(1)
	
	inst.components.revivablecorpse.revivespeedmult = 0.5
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end
-----------------------------------------------------------------------

local master_postinit = function(inst)
	------------------------------------------
	--Stats--
    PlayablePets.SetCommonStats(inst, mob, nil, true) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20, 1) --heat, cold, wetness
	----------------------------------
	--Loot drops--
	PlayablePets.SetCommonLootdropper(inst, "frog2p") --inst, prefaboverride
	----------------------------------
	--Tags--
    inst:AddTag("frog")
	inst:AddTag("amphibious")
	
	inst.mobsleep = true
	--inst.taunt = true
	inst.taunt2 = true
	
	inst.mobplayer = true
	inst.ghostbuild = "ghost_monster_build"

	inst.shouldwalk = false
	inst.isshiny = 0
	
	inst.poisonimmune = true
	
	MakeSmallBurnableCharacter(inst, "frogsack")
	MakeTinyFreezableCharacter(inst, "frogsack")
	
	inst:AddComponent("thief")
	
	inst.sounds = sounds.poison	
	----------------------------------
	--Eater--
    inst.components.eater:SetAbsorptionModifiers(4,3,3) --This multiplies food stats.
	inst.components.eater:SetCanEatHorrible() --check to make them eat monster meat.
	inst.components.eater.strongstomach = true -- can eat monster meat!
	---------------------------------
	--Physics and Scale--
	MakeCharacterPhysics(inst, 1, .3)
    inst.DynamicShadow:SetSize(1.5, .75)
    inst.Transform:SetFourFaced()
	
	PlayablePets.SetAmphibious(inst, "frog", "frog_water")
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	inst:ListenForEvent("attacked", onattacked)
	inst.components.combat.onhitotherfn = OnHitOther
	---------------------------------
	--Functions that saves and loads data.

    inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
	--Forge--
	if inst.components.revivablecorpse then
		inst.forge_fn = SetForge(inst)
	end
    ------------------------------------------------------
	--Light and Character Functions. Don't Touch.--
    if MOBNIGHTVISION ~= "Disable2" then inst.components.grue:AddImmunity("mobplayer") end
	
	--IA's tile tracker--
	inst:DoTaskInTime(3, function(inst)
	if SW_PP_MODE == true then		
		inst:AddComponent("tiletracker")
		inst.components.tiletracker:SetOnWaterChangeFn(OnWaterChange)
		inst.components.tiletracker:Start()
	end
	end)
    inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob) end)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob, nil, nil, nil, nil, true) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman, amphibious)
    end)
	
    return inst
	
end

return MakePlayerCharacter("frog2p", prefabs, assets, common_postinit, master_postinit, start_inv)
