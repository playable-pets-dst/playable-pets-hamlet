local assets =
{
	Asset( "IMAGE", "images/inventoryimages/monster_wpnham.tex" ),
    Asset( "ATLAS", "images/inventoryimages/monster_wpnham.xml" ),
	Asset("ANIM", "anim/monkey_projectile.zip"),
	Asset("ANIM", "anim/poop.zip"),
}

local prefabs = 
{
	"reticulelong",
    "reticulelongping",
	"splash_lavafx",
}

local tuning_values = PPHAM_FORGE.POOPWEAPON

local function ReticuleTargetFn()
    return Vector3(ThePlayer.entity:LocalToWorldSpace(6.5, 0, 0))
end

local function ReticuleMouseTargetFn(inst, mousepos)
    if mousepos ~= nil then
        local x, y, z = inst.Transform:GetWorldPosition()
        local dx = mousepos.x - x
        local dz = mousepos.z - z
        local l = dx * dx + dz * dz
        if l <= 0 then
            return inst.components.reticule.targetpos
        end
        l = 6.5 / math.sqrt(l)
        return Vector3(x + dx * l, 0, z + dz * l)
    end
end

local function ReticuleUpdatePositionFn(inst, pos, reticule, ease, smoothing, dt)
    local x, y, z = inst.Transform:GetWorldPosition()
    reticule.Transform:SetPosition(x, 0, z)
    local rot = -math.atan2(pos.z - z, pos.x - x) / DEGREES
    if ease and dt ~= nil then
        local rot0 = reticule.Transform:GetRotation()
        local drot = rot - rot0
        rot = Lerp((drot > 180 and rot0 + 360) or (drot < -180 and rot0 - 360) or rot0, rot, dt * smoothing)
    end
    reticule.Transform:SetRotation(rot)
end

local function MakeNormal(inst)
	inst:RemoveTag("NOCLICK")
	inst.ismoving = false
	inst.Physics:Stop()
	inst.Physics:SetCollisionGroup(COLLISION.OBSTACLES)
    inst.Physics:CollidesWith(COLLISION.WORLD)
    inst.Physics:CollidesWith(COLLISION.OBSTACLES)
    inst.Physics:CollidesWith(COLLISION.SMALLOBSTACLES)
	inst.components.inventoryitem.canbepickedup = true
	inst.AnimState:SetBank("poop")
    inst.AnimState:SetBuild("poop")
    inst.AnimState:PlayAnimation("dump")
	inst.AnimState:PushAnimation("idle", false)
	inst.AnimState:SetMultColour(1,1,1,1)
end

local function Return(inst, attacker, inocean)
	attacker.components.combat.ignorehitrange = false
	if not inocean then 
		inst.Physics:SetMotorVel(0, 0, 0) 
		inst.AnimState:SetMultColour(0,0,0,0)
	end
	inst:DoTaskInTime(FRAMES*15, function()
		if (inocean and not inst:IsOnValidGround()) or inst:IsOnValidGround() then
			if attacker and (attacker:HasTag("monkey") or attacker:HasTag("spider_monkey")) then
				inst.Physics:Stop()
				inst.AnimState:SetMultColour(0,0,0,0)
				inst.returnfrompos = inst:GetPosition()
				inst.projectileowner = nil
				inst:DoTaskInTime(FRAMES*12, function()
                        if attacker and attacker.entity and attacker.entity:IsValid() and attacker.entity:IsVisible() then
                            attacker.components.inventory:Equip(inst)
                        end
                        MakeNormal(inst)
                    inst.returnfrompos = nil
                end)
			else
				if inocean then
					inst:DoTaskInTime(3, function(inst)
						inst:PutBackOnGround()
						local pos = inst:GetPosition()
						inst.Transform:SetPosition(pos.x, 12, pos.z)
						MakeNormal(inst)
					end)
				else
					inst:DoTaskInTime(FRAMES*12, function() MakeNormal(inst) end)
				end
			end
		end
	end)
end

local function OnUpdate(inst, attacker)
	inst:DoTaskInTime(0, function(inst)
		if not inst:IsOnValidGround() then
			inst.ismoving = false
			inst.Physics:Stop()
			inst.components.aimedprojectile:Stop()
			inst.AnimState:SetMultColour(0,0,0,0)
			inst.AnimState:PlayAnimation("idle")
			SpawnPrefab("splash_lavafx").Transform:SetPosition(inst:GetPosition():Get())
			Return(inst, attacker, true)
		end
		if inst.ismoving then OnUpdate(inst, attacker) end
	end)
end

local function OnThrown(inst, owner, attacker, targetpos)
	attacker.components.inventory:DropItem(inst)
	inst:AddTag("NOCLICK")
	inst.Physics:ClearCollisionMask()
	inst.components.inventoryitem.canbepickedup = false
	inst.AnimState:SetBank("monkey_projectile")
    inst.AnimState:SetBuild("monkey_projectile")
	inst.AnimState:PlayAnimation("idle", true)
	inst.ismoving = true
	attacker.components.combat.ignorehitrange = true
	OnUpdate(inst, attacker)
end

local function OnHit(inst, owner, attacker, target)
	if target.components.sanity ~= nil then
        target.components.sanity:DoDelta(-TUNING.SANITY_SMALL)
    end
    inst.SoundEmitter:PlaySound("dontstarve/creatures/monkey/poopsplat")
	if target.components.combat and attacker ~= nil and not (target.aggrotimer and target.components.combat.lastwasattackedtime <= target.aggrotimer ) then 
		local oldtarget = target.components.combat.target or nil
		target:PushEvent("newcombattarget", {target=attacker, oldtarget=oldtarget})
		--desperate attempt to force slam on whoever chucked lucy
		if target.prefab == "boarrior" then
			local x, y, z = attacker.Transform:GetWorldPosition()
			local distsq = target:GetDistanceSqToPoint(x, y, z)
			if distsq and distsq > 30 and distsq < 300 and target.components.health:GetPercent() <= TUNING.FORGE.BOARRIOR.PHASE2_TRIGGER and not target.slamhardcooldown then
				target.wants_to_slam = attacker
			end
		end
		target.components.combat:SetTarget(attacker) 
		target.aggrotimer = GetTime() + TUNING.FORGE.AGGROTIMER_LUCY 
	end
	inst.components.aimedprojectile:RotateToTarget(attacker:GetPosition())
	if inst:IsOnValidGround() then Return(inst, attacker) end
   --- inst:Remove()
end

local function OnMiss(inst, owner, attacker)
	if inst:IsOnValidGround() then Return(inst, attacker) end
end

local function ChuckLucy(inst, caster, pos)
	if caster:HasTag("monkey") or caster:HasTag("spider_monkey") then
		inst.components.aimedprojectile:Throw(inst, caster, pos)
	end
end

local function fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
	inst.entity:AddSoundEmitter()
    inst.entity:AddNetwork()

    MakeInventoryPhysics(inst)

    inst.AnimState:SetBank("poop")
    inst.AnimState:SetBuild("poop")
    inst.AnimState:PlayAnimation("dump")
	inst.AnimState:PushAnimation("idle", false)

    inst:AddTag("poopweaon")
	inst:AddTag("throw_line")
	inst:AddTag("rechargeable")
	inst:AddTag("sharp")
	
	inst:AddComponent("aoetargeting")
    inst.components.aoetargeting:SetAlwaysValid(true)
    inst.components.aoetargeting.reticule.reticuleprefab = "reticulelong"
    inst.components.aoetargeting.reticule.pingprefab = "reticulelongping"
    inst.components.aoetargeting.reticule.targetfn = ReticuleTargetFn
    inst.components.aoetargeting.reticule.mousetargetfn = ReticuleMouseTargetFn
    inst.components.aoetargeting.reticule.updatepositionfn = ReticuleUpdatePositionFn
    inst.components.aoetargeting.reticule.validcolour = { 1, .75, 0, 1 }
    inst.components.aoetargeting.reticule.invalidcolour = { .5, 0, 0, 1 }
    inst.components.aoetargeting.reticule.ease = true
    inst.components.aoetargeting.reticule.mouseenabled = true

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end
	inst:AddComponent("rechargeable")
	-------
	inst:AddComponent("aoespell")
	inst.components.aoespell:SetAOESpell(ChuckLucy)
	
    inst:AddComponent("weapon")
	inst.components.weapon:SetDamage(tuning_values.DAMAGE)
	inst.components.weapon:SetDamageType(1)
	--inst.components.weapon:SetAltDamage(tuning_values.DAMAGE)
	inst.components.weapon:SetAltAttack(tuning_values.ALT_DAMAGE, 20, nil, 1) -- TODO tuning
    -------
	inst:AddComponent("itemtype")
	inst.components.itemtype:SetType("melees")
	
    inst:AddComponent("inspectable")

    inst:AddComponent("inventoryitem")
	--inst.components.inventoryitem.atlasname = "images/inventoryitems.xml"
    inst.components.inventoryitem.imagename = "poop"
	
	inst:AddComponent("aimedprojectile")
	inst.components.aimedprojectile:SetRange(20)
	inst.components.aimedprojectile.speed = 25
	inst.components.aimedprojectile:SetHitDistance(2)
	inst.components.aimedprojectile:SetStimuli("strong")
	--inst.components.aimedprojectile:SetDamage(TUNING.FORGE.RILEDLUCY.ALT_DAMAGE)
	inst.components.aimedprojectile:SetOnThrownFn(OnThrown)
    inst.components.aimedprojectile:SetOnHitFn(OnHit)
	inst.components.aimedprojectile:SetOnMissFn(OnMiss)
	
    inst:AddComponent("equippable")
    --inst.components.equippable:SetOnEquip(onequip)
    --inst.components.equippable:SetOnUnequip(onunequip)

    return inst
end

return Prefab("poopweapon_forge", fn, assets)