local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--
---------------------------

--Don't add assets unless absolutely needed.
local assets = 
{
	
}
--Might be completely pointless here. I don't know.
local prefabs = 
{	
	"groundpound_fx",
	"groundpoundring_fx",
}
	
--Note: Becareful when adding starting items.
--People are likely to spam changecharacter to get more of them.	
local start_inv = 
{
	--'prefab',
	--"spear",
}

local start_inv2 =
{
   --"beehome",
}

if MOBHOUSE == "Enable1" or MOBHOUSE == "Enable3" then
	start_inv = start_inv2
end
-----------------------
--Stats--
local mob = 
{
	health = 15000,
	hunger = 800,
	hungerrate = TUNING.WILSON_HUNGER_RATE, 
	sanity = 100,
	runspeed = 6*1.2,
	walkspeed = 5,
	damage = 300*2,
	range = 3,
	bank = "head",
	build = "roc_head_build",
	shiny = "roc_head",
	--build2 = "bee_guard_puffy_build",
	scale = 0.8,
	stategraph = "SGrocp",
	minimap = "rocp.tex",
	
}
-----------------------
local sounds = 
{
    emerge = "ia/creatures/seacreature_movement/water_emerge_med",
    submerge = "ia/creatures/seacreature_movement/water_submerge_med",
}

--Loot that drops when you die, duh.
SetSharedLootTable('rocp',
{
    {'meat',  1.00},
	{'meat',  1.00},
	{'meat',  1.00},
	{'meat',  1.00},
	{'meat',  1.00},
	{'meat',  1.00},
	{'meat',  1.00},
	{'meat',  1.00},
	{'meat',  1.00},
	{'meat',  1.00},
	{'meat',  1.00},
	{'meat',  1.00},
	{'meat',  1.00},
	{'meat',  1.00},
	{'meat',  1.00},
	{'meat',  1.00},
	{'feather_robin',  1.00},
	{'feather_robin',  1.00},
	{'feather_robin',  1.00},
	{'feather_robin',  0.50},
	{'feather_crow',  0.50},
	{'feather_crow',  0.50},
	{'feather_crow',  0.50},
	{'feather_crow',  0.50},
	{'feather_canary',  0.10},
	{'feather_canary',  0.10},
	{'feather_canary',  0.10},
	{'feather_canary',  0.10},
})

local function GetExcludeTags(inst)
	if TheNet:GetPVPEnabled() then
		return {"lightningimmune", "lightningrod", "notarget"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion", "lightningimmune", "lightningrod", "notarget"}
	else	
		return {"player", "companion", "lightningimmune", "lightningrod", "notarget"}
	end
end

------------------------------------------------------
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
end
-------------------------------------------------------
local common_postinit = function(inst) 
	inst.MiniMapEntity:SetIcon(mob.minimap)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false)
	  --ThePlayer.HUD.controls.status.brain:Hide()
		if MONSTERHUNGER == "Disable" then
			--ThePlayer.HUD.controls.status.stomach:Hide()
		end
   end
end)
	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)

	PlayablePets.SetNightVision(inst)	
end

-----------------------------------------------------------------------
local function RemoveBody(inst)
	if inst.body and TheNet:GetServerGameMode() ~= "lavaarena" then
		if inst.components.health:IsDead() then
			local bodypos = inst.body:GetPosition()
			inst.components.lootdropper:DropLoot(Vector3(bodypos.x, 20, bodypos.z))
		end
			
		inst.body.components.roccontrollerp:doliftoff()
		inst.body = nil
	elseif inst.body and inst.components.health:IsDead() and TheNet:GetServerGameMode() == "lavaarena" then
		inst.body.components.roccontrollerp.leg1:AddTag("notarget")
		inst.body.components.roccontrollerp.leg2:AddTag("notarget")
	end
end

local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	inst.components.health:SetMaxHealth(PPHAM_FORGE.ROC.HEALTH)
	
	inst.components.combat:SetDefaultDamage(PPHAM_FORGE.ROC.DAMAGE)
	inst.components.combat:SetAttackPeriod(PPHAM_FORGE.ROC.ATTACKPERIOD)
	
	inst.components.locomotor.walkspeed = PPHAM_FORGE.ROC.WALKSPEED
    inst.components.locomotor.runspeed = PPHAM_FORGE.ROC.RUNSPEED
	
	inst.components.combat:SetRange(PPHAM_FORGE.ROC.ATTACK_RANGE, PPHAM_FORGE.ROC.HIT_RANGE)
	--inst.components.combat:SetAreaDamage(PPHAM_FORGE.HIPPO.AOE_RANGE, PPHAM_FORGE.HIPPO.AOE_DMGMULT)
	
	inst.mobsleep = false
	inst.taunt2 = false
	
	--inst.acidmult = 1.25
	inst.components.health:StopRegen()
	
	inst:DoTaskInTime(0, function(inst)
	inst:ListenForEvent("onremove", function(inst) 
		if inst.body then
			inst.body.components.roccontrollerp:doliftoff()
			inst.body = nil 
		end
	end)
	inst:ListenForEvent("death", RemoveBody)
	
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"books", "staves", "darts", "melees"})
	end)
	
	inst:DoTaskInTime(5, function(inst) 
		inst.Physics:ClearCollisionMask()
		inst.Physics:CollidesWith(COLLISION.WORLD)  
	end)
	--inst.components.combat.onhitotherfn = OnForgeHitOther
	
	
	
	inst.components.combat:SetDamageType(1)
	
	inst.components.revivablecorpse.revivespeedmult = 4
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end



local master_postinit = function(inst)
	------------------------------------------
	--Stats--
	inst.components.health:StartRegen(10, 5)
    PlayablePets.SetCommonStats(inst, mob, nil, true) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20) --heat, cold, wetness
	----------------------------------
	--Loot drops--
	inst:AddComponent("lootdropper")
    inst.components.lootdropper:SetChanceLootTable("rocp")
	
	inst:AddComponent("roccontrollerp")
	----------------------------------
	--Tags--
	inst:AddTag("epic")
	inst:AddTag("monster")
	inst:AddTag("noteleport")
	inst:AddTag("groundpoundimmune")
	
	inst.roc_head = true
	--inst.mobsleep = true
	inst.taunt = true
	inst.taunt2 = true
	
	inst.mobplayer = true
	inst.ghostbuild = "ghost_monster_build"

	inst.isshiny = 0
	inst.sounds = sounds
	
    MakeLargeBurnableCharacter(inst, "nose_tree01")
    MakeLargeFreezableCharacter(inst, "spring")
	inst.poisonsymbol = "nose_tree01"
	inst.components.debuffable:SetFollowSymbol(inst.poisonsymbol, 0, 0, 0)
	
	inst:ListenForEvent("onremove", RemoveBody)
	inst:ListenForEvent("death", RemoveBody)
	PlayablePets.SetCommonStatResistances(inst, 1, 1, 1, 9999) --fire, acid, poison
	PlayablePets.SetStormImmunity(inst)
	----------------------------------
	--Eater--

	--inst.components.eater:SetDiet({ FOODTYPE.MEAT, FOODTYPE.MEAT }, { FOODTYPE.MEAT, FOODTYPE.MEAT }) 
    inst.components.eater:SetAbsorptionModifiers(1,1.75,1) --This multiplies food stats.
	inst.components.eater:SetCanEatHorrible() --check to make them eat monster meat.
	inst.components.eater.strongstomach = true -- can eat monster meat!
	inst.components.eater.eatwholestack = true --should eat the entire stack and get all the benefits.
	inst.components.eater.ignoresspoilage = true -- Can eat spoiled food without consequence. Might keep food stats as if fresh.
	---------------------------------
	--Physics and Scale--
	MakeGhostPhysics(inst, .5, .5)
	inst.Physics:ClearCollisionMask()
    inst.Physics:CollidesWith(COLLISION.WORLD)
	inst.DynamicShadow:SetSize(0, 0)

    inst.Transform:SetEightFaced()
	
	PlayablePets.SetAmphibious(inst, nil, nil, true)
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	---------------------------------
	--Functions that saves and loads data.

    inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
	--Forge--
	if inst.components.revivablecorpse then
		inst.forge_fn = SetForge(inst)
	end
	
	inst:AddComponent("groundpounder")
    inst.components.groundpounder.destroyer = true
    inst.components.groundpounder.damageRings = 0
    inst.components.groundpounder.destructionRings = 2
    inst.components.groundpounder.numRings = 3
    ------------------------------------------------------
	
	inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob, true, true, true) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob, true, true, true) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob, true, true, true, nil, true) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman, amphibious)
    end)
	
    return inst
	
end

return MakePlayerCharacter("rocp", prefabs, assets, common_postinit, master_postinit, start_inv)
