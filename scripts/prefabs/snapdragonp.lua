local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--
---------------------------

local assets = 
{
	Asset("ANIM", "anim/snapdragon.zip"),
    Asset("ANIM", "anim/snapdragon_build.zip"),
	Asset("SOUND", "sound/beefalo.fsb"),
}

local prefabs = 
{	

}

local start_inv = 
{
	--'prefab',
	--"spear",
}

local start_inv2 =
{
   --"beehome",
}

if MOBHOUSE == "Enable1" or MOBHOUSE == "Enable3" then
	start_inv = start_inv2
end

local prefabname = "snapdragonp"
-----------------------
--Stats--
local mob = 
{
	health = TUNING.BEEFALO_HEALTH,
	hunger = 200,
	hungerrate = TUNING.WILSON_HUNGER_RATE, 
	sanity = 150,
	
	runspeed = 4,
	walkspeed = 4,
	
	attackperiod = 0,
	damage = 35,
	range = 2.5,
	hit_range = 3,
	
	bank = "snapdragon",
	build = "snapdragon_build",
	shiny = "snapdragon",
	
	scale = 1.22,
	stategraph = "SG"..prefabname,
	minimap = prefabname..".tex",	
}

--Loot that drops when you die, duh.
SetSharedLootTable( 'snapdragonp',
{
    {'plantmeat',                 1.00},
	{'plantmeat',                 1.00},
	{'plantmeat',                 1.00},
	{'plantmeat',                 1.00},
	{'flower',                    1.00},
})

local FORGE_STATS = PPHAM_FORGE.SNAPDRAGON

local sounds = 
{
    walk = "dontstarve/beefalo/walk",
    grunt = "dontstarve/beefalo/grunt",
    yell = "dontstarve/beefalo/yell",
    swish = "dontstarve/beefalo/tail_swish",
    curious = "dontstarve/beefalo/curious",
    angry = "dontstarve/beefalo/angry",
	bigstep = "dontstarve/creatures/leif/footstep",
}
--==============================================
--					Mob Functions
--==============================================

--==============================================
--				Custom Common Functions
--==============================================
--TODO this is currently disabled because of the new system which allow
--prefabs to act as hats. The new hats like the Void Hood will crash with this code.
--Find a way to fix it...
local function OnEquipHat(inst, data)
	local head = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HEAD)
	if head then
		inst.AnimState:Show("hair")
		local fname = "hat_"..string.sub(head.prefab, 0, -4) --strip the "hat" from the name so we can automate most hats in the game
		local skin_build = head:GetSkinBuild()
		if skin_build ~= nil then
			inst:PushEvent("equipskinneditem", inst:GetSkinName())
			inst.AnimState:OverrideItemSkinSymbol("hair", skin_build, "swap_hat", head.GUID, fname)
		else
			inst.AnimState:OverrideSymbol("hair", fname, "swap_hat")
		end
	else
		inst.AnimState:Hide("hair")
	end
end

local function OnUnequip(inst, data)
	local head = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HEAD)
	if not head then
		inst.AnimState:Show("hair")
		inst.AnimState:OverrideSymbol("hair", mob.build, "hair")
	end
end

--Below table is what will count towards the nature stat, this is because the actual prefab itself
--is removed before the deployed event pushes, so you can't check for it's tags.
local plantables = 
{
	"acorn",
	"pinecone",
	"twiggy_nut",
	"butterfly",
	"seeds",
	"asparagus_seeds",
	"carrot_seeds",
	"dragonfruit_seeds",
	"durian_seeds",
	"eggplant_seeds",
	"garlic_seeds",
	"potato_seeds",
	"onion_seeds",
	"corn_seeds",
	"pomegranate_seeds",
	"pepper_seeds",
	"tomato_seeds",
	"watermelon_seeds",
	"pumpkin_seeds"
}

local NATURESTATCAP = 500

local function UpdateStats(inst)
	local curhealth = inst.components.health:GetPercent()
	local curhunger = inst.components.health:GetPercent()
	if inst.naturestat > NATURESTATCAP then --incase the cap gets reduced in an update
		inst.naturestat = NATURESTATCAP
	end
	if inst.naturestat then
		inst.components.health:SetMaxHealth((TheNet:GetServerGameMode() == "lavaarena" and FORGE_STATS.HEALTH or mob.health) + (inst.naturestat * (2000/NATURESTATCAP)))
		inst.components.hunger:SetMax(mob.hunger + (inst.naturestat * (500/NATURESTATCAP)))
		local scale = mob.scale + (inst.naturestat * (0.88/NATURESTATCAP))
		inst.Transform:SetScale(scale, scale, scale)
		inst.components.combat:SetDefaultDamage((TheNet:GetServerGameMode() == "lavaarena" and FORGE_STATS.DAMAGE or mob.damage) + (inst.naturestat * (45/NATURESTATCAP)))
		inst.components.combat:SetRange((TheNet:GetServerGameMode() == "lavaarena" and FORGE_STATS.RANGE or mob.range) + (inst.naturestat * (2/NATURESTATCAP)), (TheNet:GetServerGameMode() == "lavaarena" and FORGE_STATS.HIT_RANGE or mob.hit_range) + (inst.naturestat * (2/NATURESTATCAP)))
		inst.components.locomotor.walkspeed = mob.walkspeed/(1 + inst.naturestat * (0.5/NATURESTATCAP))
		inst.components.combat:SetAttackPeriod((TheNet:GetServerGameMode() == "lavaarena" and FORGE_STATS.ATTACKPERIOD or mob.attackperiod) + (inst.naturestat * (2/NATURESTATCAP)))
	end	
	inst.components.health:SetPercent(curhealth)
	inst.components.hunger:SetPercent(curhunger)
end

local function IsPlantable(item)
	local check = false
	for i, v in ipairs(plantables) do
		if item == v then
			check = true
			break
		end
	end
	--print("DEBUG: IsPlantable returned "..check)
	return check
end

local function OnDeploy(inst, data)
	--print("DEBUG: OnDeploy ran, prefab is "..data.prefab)
	if data and data.prefab and IsPlantable(data.prefab) and inst.naturestat < NATURESTATCAP then
		inst.naturestat = inst.naturestat + 1
		UpdateStats(inst)
	end
end

local function OnDeath(inst)
	inst.naturestat = 0
	if inst.saved_damage then
		inst.saved_damage = 0
	end
end
--==============================================
--					Loading/Saving
--==============================================
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
		inst.naturestat = data.naturestat or 0
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
	data.naturestat = inst.naturestat or 0
end

--==============================================
--					Forged Forge
--==============================================

local function OnAttackedForge(inst, data)
	if inst.components.health and not inst.components.health:IsDead() then
		if data.damage then
			inst.saved_damage = inst.saved_damage + data.damage
			inst.naturestat = inst.saved_damage/150
			UpdateStats(inst)
		end
	end
end

local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, FORGE_STATS)
	
	inst.mobsleep = false	
	inst.healmult = 3
	inst.naturestat = 0 --just incase
	inst.saved_damage = 0
	inst:ListenForEvent("attacked", OnAttackedForge)
	
	inst:DoTaskInTime(0, function(inst)	
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"staves"})
	end)
	
	inst.components.combat:SetDamageType(1)
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

--==============================================
--					Common/Master
--==============================================

local common_postinit = function(inst) 
	inst.MiniMapEntity:SetIcon(mob.minimap)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false) --PP doesn't work with movement prediction enabled, due to use of custom stategraphs
   end
end)
	----------------------------------
	--Tags--
    inst:AddTag("snapdragon")
	inst:AddTag("plantkin")
	----------------------------------

	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst)
end

local master_postinit = function(inst)
	--Stats--
	PlayablePets.SetCommonStats(inst, mob, nil, true) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20, 1) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst, 3, 1, 1) --fire, acid, poison
	----------------------------------
	--Variables	
	inst.mobsleep = true
	inst.taunt = true
	--inst.taunt2 = true
	inst.shouldwalk = true
	inst.can_eat_insects = true
	inst.naturestat = 0
	inst.sounds = sounds
	
	inst.UpdateStats = UpdateStats
	
	local body_symbol = "swap_fire"
	inst.poisonsymbol = body_symbol
	MakeMediumBurnableCharacter(inst, body_symbol)
    MakeMediumFreezableCharacter(inst, body_symbol)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	----------------------------------
	--Components
	PlayablePets.SetCommonLootdropper(inst, prefabname) --inst, prefaboverride
	----------------------------------
	--Eater--
    inst.components.eater:SetAbsorptionModifiers(1,1,1) --This multiplies food stats.
	inst.components.eater:SetDiet({ FOODTYPE.SEEDS, FOODTYPE.GENERIC ,FOODTYPE.VEGGIE, FOODTYPE.INSECT }, { FOODTYPE.SEEDS, FOODTYPE.GENERIC ,FOODTYPE.VEGGIE, FOODTYPE.INSECT }) 
	---------------------------------
	--Physics and Shadows--
	MakeCharacterPhysics(inst, 100, .5)
	inst.Transform:SetFourFaced()
	
	inst.DynamicShadow:SetSize(6, 2)
	--inst.DynamicShadow:Enable(false)  
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	--inst:ListenForEvent("equip", OnEquipHat) --disabled, check note above this function.
	--inst:ListenForEvent("unequip", OnUnequip) 
	inst:ListenForEvent("deployitem", OnDeploy)
	inst:ListenForEvent("death", OnDeath)
	---------------------------------
	--Functions

	---------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
    ------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)
    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) UpdateStats(inst) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob) UpdateStats(inst) end)
    end)
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
    return inst	
end

return MakePlayerCharacter(prefabname, prefabs, assets, common_postinit, master_postinit, start_inv)