require "brains/citypigpbrain"
require "brains/pigguardbrain"
require "brains/werepigbrain"
require "stategraphs/SGpig_cityp"
require "stategraphs/SGwerepig"

local assets =
{
	Asset("SOUND", "sound/pig.fsb"),
    Asset("ANIM", "anim/pig_usher.zip"),
    Asset("ANIM", "anim/pig_mayor.zip"),
    Asset("ANIM", "anim/pig_miner.zip"),
    Asset("ANIM", "anim/pig_queen.zip"),
    Asset("ANIM", "anim/pig_farmer.zip"),
    Asset("ANIM", "anim/pig_hunter.zip"),
    Asset("ANIM", "anim/pig_banker.zip"),
    Asset("ANIM", "anim/pig_florist.zip"),
    Asset("ANIM", "anim/pig_erudite.zip"),
    Asset("ANIM", "anim/pig_hatmaker.zip"),
    Asset("ANIM", "anim/pig_mechanic.zip"),
    Asset("ANIM", "anim/pig_professor.zip"),
    Asset("ANIM", "anim/pig_collector.zip"),
    Asset("ANIM", "anim/townspig_basic.zip"),
    Asset("ANIM", "anim/pig_beautician.zip"),
    Asset("ANIM", "anim/pig_royalguard.zip"),
    Asset("ANIM", "anim/pig_storeowner.zip"),
    Asset("ANIM", "anim/townspig_attacks.zip"),
    Asset("ANIM", "anim/townspig_actions.zip"),
    Asset("ANIM", "anim/townspig_extra_actions.zip"),
    Asset("ANIM", "anim/pig_royalguard_2.zip"),
    Asset("ANIM", "anim/townspig_shop_wip.zip"),
}

local prefabs =
{
    "meat",
    "poop",
    "tophat",
    "pigskin",
    "halberdp",
    "strawhat",
    "monstermeat",
    --"pigman_shopkeeper_deskp",
}

local MAX_TARGET_SHARES = 5
local SHARE_TARGET_DIST = 30

local function getSpeechType(inst,speech)
    local line = speech.DEFAULT

    if inst.talkertype and speech[inst.talkertype] then
        line = speech[inst.talkertype]
    end

    if type(line) == "table" then
        line = line[math.random(#line)]
    end

    return line
end

local function shopkeeper_speech(inst, speech )
    if inst:IsValid() and not inst:IsAsleep() and not inst.components.combat.target and not inst:IsInLimbo() then
        inst.sayline(inst, speech)
        --inst.components.talker:Say(speech, 1.5, nil, true)
    end
end

local function sayline(inst, line, mood)
	inst.SoundEmitter:PlaySound("dontstarve/pig/grunt")
    inst.components.talker:Say(line, 1.5, nil, true, mood)
end

local function ontalk(inst, script, mood)
    inst.SoundEmitter:PlaySound("dontstarve/pig/grunt")
end

local function CalcSanityAura(inst, observer)
	if inst.components.werebeast
       and inst.components.werebeast:IsInWereState() then
		return -TUNING.SANITYAURA_LARGE
	end
	
	if inst.components.follower and inst.components.follower.leader == observer then
		return TUNING.SANITYAURA_SMALL
	end
	
	return 0
end

local function ShouldAcceptItem(inst, item)
	if inst.components.sleeper:IsAsleep() then
        return false
    end
	local list = PIG_TRADER[inst.prefab]
	local desc =  PIG_TRADER[inst.prefab].desc
	
	if list[item.prefab] and not inst.unwanted[item.prefab] then
		return true
	elseif list[item.prefab] and inst.unwanted[item.prefab] then
		inst.sayline(inst, getSpeechType(inst,STRINGS.CITY_PIG_TALK_REFUSE_GIFT_DELAY_TOMORROW))
		return false
	end
	return false
end

local meal_rewards_T2 = {
	"meatballs",
	"leafymeatburger",
	"frogglebunwich",
	"seafoodgumbo",
	"fishsticks",
	"butterflymuffin",
	"fishtacos",
}

local meal_rewards_T1 = {
	"meat",
	"berries",
	"corn",
	"carrot",
	"pumpkin",
	"watermelon",
	"eggplant",
	"fishmeat_small",
	"fishmeat",
}

local function OnGetItemFromPlayer(inst, giver, item)
	local list = PIG_TRADER[inst.prefab]
	local desc =  PIG_TRADER[inst.prefab].desc
	
	if list[item.prefab] and not inst.unwanted[item.prefab] then   
		inst.sg:GoToState("interact", giver)

        local reward = list[item.prefab].reward
		local qty = list[item.prefab].rewardqty
        if inst:HasTag("guard") then
            item:Remove() --We don't want guards to keep anything in their actual inventory besides the usual
        end
		
        if reward then
            if giver.components.inventory then
                inst.sayline(inst, string.format(getSpeechType(inst,STRINGS.CITY_PIG_TALK_GIVE_REWARD), tostring(1), desc ))
                    --inst.components.talker:Say( string.format(getSpeechType(inst,STRINGS.CITY_PIG_TALK_GIVE_REWARD), tostring(1), desc ))--econ:GetNumberWanted(econprefab,city) ), desc ) )

                for i=1,qty do
					if reward == "random_meal" then
						local list = math.random() > 0.90 and meal_rewards_T2 or meal_rewards_T1
						local rewarditem = SpawnPrefab(list[math.random(1, #list)])
						giver.components.inventory:GiveItem(rewarditem, nil,  Vector3(inst.Transform:GetWorldPosition()))
					else
						local rewarditem = SpawnPrefab(reward)
						giver.components.inventory:GiveItem(rewarditem, nil,  Vector3(inst.Transform:GetWorldPosition()))
					end
                end
				if list[item.prefab].cooldown then
					inst.unwanted[item.prefab] = true
				end
            end
        else
            inst.sayline(inst, string.format(getSpeechType(inst,STRINGS.CITY_PIG_TALK_TAKE_GIFT), tostring(1), desc ))
            --inst.components.talker:Say( string.format(getSpeechType(inst,STRINGS.CITY_PIG_TALK_TAKE_GIFT), tostring(1), desc ))--econ:GetNumberWanted(econprefab,city) ), desc ) )
        end
    end
end

local function OnRefuseItem(inst, item)
    inst.sg:GoToState("refuse")
    if inst.components.sleeper:IsAsleep() then
        inst.components.sleeper:WakeUp()
    end
end

local function OnEat(inst, food)    
    if food.components.edible and (food.components.edible.foodtype == "VEGGIE") then --or food.components.edible.foodtype == "SEEDS") then
        local poop = SpawnPrefab("poop")
		poop.Transform:SetPosition(inst.Transform:GetWorldPosition())
        poop.owner = inst
	end
end

local function OnAttackedByDecidRoot(inst, attacker)
    local fn = function(dude) return dude:HasTag("pig") and not dude:HasTag("werepig") and not dude:HasTag("guard") end

    local x,y,z = inst.Transform:GetWorldPosition()
    local ents = nil
    ents = TheSim:FindEntities(x,0,z, SHARE_TARGET_DIST / 2)
    
    if ents then
        local num_helpers = 0
        for k,v in pairs(ents) do
            if v ~= inst and v.components.combat and not (v.components.health and v.components.health:IsDead()) and fn(v) then
                if v:PushEvent("suggest_tree_target", {tree=attacker}) then
                    num_helpers = num_helpers + 1
                end
            end
            if num_helpers >= MAX_TARGET_SHARES then
                break
            end     
        end
    end
end

local function spawnguardtasks(inst, attacker)
    
end

local function OnAttacked(inst, data)

    ----print(inst, "OnAttacked")
    local attacker = data.attacker
    if attacker then
        inst:ClearBufferedAction()

        if attacker.prefab == "deciduous_root" and attacker.owner then 
            OnAttackedByDecidRoot(inst, attacker.owner)
        elseif attacker.prefab ~= "deciduous_root" then
            inst.components.combat:SetTarget(attacker)

            if inst:HasTag("guard") then
                if attacker:HasTag("player") then
					--					
                end
                inst.components.combat:ShareTarget(attacker, SHARE_TARGET_DIST, function(dude) return dude:HasTag("pig") and (dude:HasTag("guard") or not attacker:HasTag("pig")) end, MAX_TARGET_SHARES)
            else
                if not (attacker:HasTag("pig") and attacker:HasTag("guard") ) then
                    inst.components.combat:ShareTarget(attacker, SHARE_TARGET_DIST, function(dude) return dude:HasTag("pig") end, MAX_TARGET_SHARES)
                end
            end
        end
    end
end

local builds = {"pig_build", "pigspotted_build"}
local guardbuilds = {"pig_guard_build"}

local function NormalRetargetFn(inst)
    return FindEntity(inst, 10,
        function(guy)
            if not guy.LightWatcher or guy.LightWatcher:IsInLight() then

                if guy:HasTag("player") and guy.components.health and not guy.components.health:IsDead() and inst.components.combat:CanTarget(guy) and guy:HasTag("iswanted") then
                    --inst.sayline(inst, getSpeechType(inst,STRINGS.CITY_PIG_GUARD_TALK_ANGRY_PLAYER))
                    --inst.components.talker:Say( getSpeechType(inst,STRINGS.CITY_PIG_GUARD_TALK_ANGRY_PLAYER) )
                end

                return (((guy:HasTag("monster") or guy:HasTag("hostile") or guy:HasTag("penguin")) and not guy:HasTag("player"))) and guy.components.health and not guy.components.health:IsDead() and inst.components.combat:CanTarget(guy) and not 
                (inst.components.follower.leader ~= nil and guy:HasTag("abigail"))
            end
        end)
end

local function CitizenRetargetFn(inst)
    return FindEntity(inst, 10,
        function(guy)
            if not guy.LightWatcher or guy.LightWatcher:IsInLight() then

                if guy:HasTag("player") and guy.components.health and not guy.components.health:IsDead() and inst.components.combat:CanTarget(guy) and guy:HasTag("iswanted") then
                    inst.sayline(inst, getSpeechType(inst, STRINGS.CITY_PIG_GUARD_TALK_ANGRY_PLAYER))
                    --inst.components.talker:Say( getSpeechType(inst,STRINGS.CITY_PIG_GUARD_TALK_ANGRY_PLAYER) )
                end

                return (((guy:HasTag("monster") or guy:HasTag("hostile") or guy:HasTag("penguin")) and not guy:HasTag("player"))) and guy.components.health and not guy.components.health:IsDead() and inst.components.combat:CanTarget(guy) and not 
                (inst.components.follower.leader ~= nil and guy:HasTag("abigail"))
            end
        end)
end

local function NormalKeepTargetFn(inst, target)
    --give up on dead guys, or guys in the dark, or werepigs
    return inst.components.combat:CanTarget(target)
           and (not target.LightWatcher or target.LightWatcher:IsInLight())
           and not (target.sg and target.sg:HasStateTag("transform") )
end

local function NormalShouldSleep(inst)
    if inst.components.follower and inst.components.follower.leader then
        local fire = FindEntity(inst, 6, function(ent)
            return ent.components.burnable
                   and ent.components.burnable:IsBurning()
        end, {"campfire"})
        return DefaultSleepTest(inst) and fire and (not inst.LightWatcher or inst.LightWatcher:IsInLight())
    else
        return DefaultSleepTest(inst)
    end
end

local function SetNormalPig(inst, brain_id)
    inst:RemoveTag("werepig")
    inst:RemoveTag("guard")
    local brain = require "brains/citypigpbrain"
    inst:SetBrain(brain)
    inst:SetStateGraph("SGpig_cityp")
--	inst.AnimState:SetBuild(inst.build)
    
	--inst.components.werebeast:SetOnNormalFn(SetNormalPig)
    inst.components.sleeper:SetResistance(2)

    inst.components.combat:SetDefaultDamage(TUNING.PIG_DAMAGE)
    inst.components.combat:SetAttackPeriod(TUNING.PIG_ATTACK_PERIOD)
    inst.components.combat:SetKeepTargetFunction(NormalKeepTargetFn)
    inst.components.locomotor.runspeed = TUNING.PIG_RUN_SPEED
    inst.components.locomotor.walkspeed = TUNING.PIG_WALK_SPEED
    
    inst.components.sleeper:SetSleepTest(NormalShouldSleep)
    inst.components.sleeper:SetWakeTest(DefaultWakeTest)
    
    inst.components.lootdropper:SetLoot({})
    inst.components.lootdropper:AddRandomLoot("meat",3)
    inst.components.lootdropper:AddRandomLoot("pigskin",1)
    inst.components.lootdropper.numrandomloot = 1

    inst.components.health:SetMaxHealth(TUNING.PIG_HEALTH)
    inst.components.combat:SetRetargetFunction(3, NormalRetargetFn)
    inst.components.combat:SetTarget(nil)
    inst:ListenForEvent("suggest_tree_target", function(inst, data)
        if data and data.tree and inst:GetBufferedAction() ~= ACTIONS.CHOP then
            inst.tree_target = data.tree
        end
    end)

    inst.components.trader:Enable()
    inst.components.talker:StopIgnoringAll()
end

local function normalizetorch(torch)
    ----print("normalizing torch")
	torch:RemoveTag("nosteal")
    torch.components.fueled.rate = 1 
end

local function normalizehalberd(halberd)
    ----print("normalizing halberd")
	halberd:RemoveTag("nosteal")
    halberd.components.finiteuses.unlimited_uses = nil   
end

local function makefn(name, build, fixer, guard_pig, shopkeeper, tags, sex, econprefab)
    local function make_common()
	
    	local inst = CreateEntity()
    	local trans = inst.entity:AddTransform()
    	local anim = inst.entity:AddAnimState()
    	local sound = inst.entity:AddSoundEmitter()
    	local shadow = inst.entity:AddDynamicShadow()
		inst.entity:AddNetwork()

    	shadow:SetSize( 1.5, .75 )

        inst.Transform:SetFourFaced()

        inst.entity:AddLightWatcher()
        
        inst:AddComponent("talker")
        inst.components.talker.ontalk = ontalk
        inst.components.talker.fontsize = 35
        inst.components.talker.font = TALKINGFONT
        inst.components.talker.offset = Vector3(0,-600,0)
        inst.talkertype = name

        inst.sayline = sayline

        --inst.components.talker.colour = Vector3(133/255, 140/255, 167/255)

        MakeCharacterPhysics(inst, 50, .5)
        
        
		
		anim:SetBank("townspig")
        anim:SetBuild(build)

        anim:PlayAnimation("idle_loop",true)
        anim:Hide("hat")
        anim:Hide("desk")
        anim:Hide("ARM_carry")

        inst:AddTag("character")
        inst:AddTag("pig")
        inst:AddTag("civilized")
        inst:AddTag("scarytoprey")

        inst:AddTag("city_pig")
        


        if tags then
            for i,tag in ipairs(tags)do
                inst:AddTag(tag)
            end
        end
		
		inst.entity:SetPristine()

		if not TheWorld.ismastersim then
			return inst
		end
		
		inst.unwanted = {}

		inst:AddComponent("locomotor") -- locomotor must be constructed before the stategraph
        inst.components.locomotor.runspeed = TUNING.PIG_RUN_SPEED --5
        inst.components.locomotor.walkspeed = TUNING.PIG_WALK_SPEED --3

        ------------------------------------------
        inst:AddComponent("eater")
    	inst.components.eater:SetCanEatHorrible()
        inst.components.eater.strongstomach = true -- can eat monster meat!
        inst.components.eater:SetOnEatFn(OnEat)
		-------------------------------------------
        
        ------------------------------------------
        inst:AddComponent("combat")
        inst.components.combat.hiteffectsymbol = "pig_torso"

        MakeMediumBurnableCharacter(inst, "pig_torso")

        inst:AddComponent("named")

        local names = {}
        for i, name in ipairs(STRINGS.CITYPIGNAMES["UNISEX"]) do
            table.insert(names, name)
        end

        if sex then
            if sex == "MALE" then
                inst.female = false
            else
                inst.female = true
            end

            for i,name in ipairs(STRINGS.CITYPIGNAMES[sex]) do
                table.insert(names, name)
            end
        end

		if name == "pigman_mayorp" then
			inst.components.named.possiblenames = STRINGS.MAYORPIGNAMES
		elseif name == "pigman_queenp" then
			inst.components.named.possiblenames = STRINGS.QUEENPIGNAMES
		else
			inst.components.named.possiblenames = names
		end
        inst.components.named:PickNewName()

        inst:AddComponent("follower")
        inst.components.follower.maxfollowtime = TUNING.PIG_LOYALTY_MAXTIME

        ------------------------------------------
        inst:AddComponent("health")
		if name == "pigman_royalguard_2p" then
			inst.components.health:SetMaxHealth(250)
		end
        inst:AddComponent("sleeper")
        inst:AddComponent("inventory")
        inst:AddComponent("lootdropper")
        inst:AddComponent("knownlocations")
       -- inst:AddComponent("citypossession")
        --inst:AddComponent("citypooper")
        ------------------------------------------

        inst:AddComponent("trader")
        inst.components.trader:SetAcceptTest(ShouldAcceptItem)
        inst.components.trader.onaccept = OnGetItemFromPlayer
        inst.components.trader.onrefuse = OnRefuseItem
        
        ------------------------------------------

        inst:AddComponent("sanityaura")
        inst.components.sanityaura.aurafn = CalcSanityAura
        
        ------------------------------------------
        MakeMediumFreezableCharacter(inst, "pig_torso")
        --------------------------------------------
		
		inst.delay = 0
		
		inst:WatchWorldState( "isnight", function() inst.unwanted = {} end)

        inst:AddComponent("inspectable")
        inst.components.inspectable.getstatus = function(inst)
            if inst:HasTag("guard") then
                return "GUARD"
            elseif inst.components.follower.leader ~= nil then
                return "FOLLOWER"
            end
        end

        if econprefab then
            inst.econprefab = econprefab
            inst.components.inspectable.nameoverride = econprefab
        end
        ------------------------------------------  
        
        inst.special_action = function (act)
            if inst.daily_gifting then
                inst.sg:GoToState("daily_gift")
            elseif inst.poop_tip then
                inst.sg:GoToState("poop_tip")
            elseif inst:HasTag("paytax") then
                inst.sg:GoToState("pay_tax")
            end
        end

        ------------------------------------------
        inst.OnSave = function(inst, data)
            data.build = inst.build      

            data.children = {}
            -- for the shopkeepers if they have spawned their desk            
            if inst.desk then
                --print("SAVING THE DESK")
                table.insert(data.children, inst.desk.GUID)  
                data.desk = inst.desk.GUID              
            end

            if inst.torch then
                table.insert(data.children, inst.torch.GUID)
                data.torch = inst.torch.GUID
            end
            if inst.axe then
                table.insert(data.children, inst.axe.GUID)
                data.axe = inst.axe.GUID
            end

            if inst:HasTag("atdesk") then
                data.atdesk = true
            end
            if inst:HasTag("guards_called") then
                data.guards_called = true
            end
            if inst.task_guard1 or inst.task_guard2 then
                --print("SAVING GUARD TASKS")
                data.doSpawnGuardTask = true
            end             
            -- end shopkeeper stuff
            
            if inst:HasTag("angry_at_player") then
                data.angryatplayer = true
            end   

            if inst.equipped then
                data.equipped = true
            end

            if inst:HasTag("recieved_trinket") then
                data.recieved_trinket = true
            end
            
            if inst:HasTag("paytax") then
                data.paytax = true 
            end

            if data.children and #data.children > 0 then
                return data.children
            end    
        end        
        
        inst.OnLoad = function(inst, data)    
    		if data then                
    			inst.build = data.build or builds[1]
                if data.atdesk then
                    inst.sg:GoToState("desk_pre")
                end 

                if data.guards_called then
                    inst:AddTag("guards_called")
                end

                if data.doSpawnGuardTask then
                    spawnguardtasks(inst,GetPlayer())
                end

                if data.equipped then
                    inst.equipped = true
                    inst.equiptask:Cancel()
                    inst.equiptask = nil
                end

                if data.angryatplayer then
                    inst:AddTag("angry_at_player") 
                end 
                if data.recieved_trinket then
                    inst:AddTag("recieved_trinket")
                end  
                if data.paytax then
                    inst:AddTag("paytax")
                end              
    		end
        end           
        
        inst.OnLoadPostPass = function(inst, ents, data)
            if data then
                if data.children then                    
                    for k,v in pairs(data.children) do
                        local item = ents[v]            
                        if item then
                            if data.desk and data.desk == v then
                                inst.desk = item.entity
                                inst:AddComponent("homeseeker") 
                                inst.components.homeseeker:SetHome(inst.desk)  
                            end
                        end                    
                    end
                end
            end
        end    

        inst:ListenForEvent("attacked", OnAttacked)    

        --inst.throwcrackers = throwcrackers
        
        SetNormalPig(inst)

        if fixer then
            --inst:AddComponent("fixer")
        end

        return inst
    end

    ----------------------------------------------------------------------------------------

    local function make_pig_guard ()
        local inst = make_common()

        inst:AddTag("emote_nocurtsy")
        inst:AddTag("guard")
        inst:AddTag("extinguisher")

		if not TheWorld.ismastersim then
			return inst
		end
		
        inst.components.burnable:SetBurnTime(2)

        inst.equiptask = inst:DoTaskInTime(0,function()
            if not inst.equipped then
                inst.equipped = true

                local torch = SpawnPrefab("minerhat")
                inst.components.inventory:GiveItem(torch)
				torch:AddTag("nosteal")

                local axe =SpawnPrefab("halberdp")
                inst.components.inventory:GiveItem(axe)            
                inst.components.inventory:Equip(axe)
                axe.components.finiteuses.unlimited_uses = true
				axe:AddTag("nosteal")
            end
        end)
		
		inst.components.health:SetAbsorptionAmount(0.8)

        inst:WatchWorldState("isday", function()                
            inst:DoTaskInTime(0.5+(math.random()*1),function()
                    local axe = inst.components.inventory:FindItem(
                        function(item)
                            if item.prefab == "halberdp" then
                                return true
                            end
                        end)
                    if axe then 
                        inst.components.inventory:Equip(axe) 
                    end 
					local hat = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HEAD)
					if hat then 
                        inst.components.inventory:Unequip(EQUIPSLOTS.HEAD) 
                    end 
                end)
        end)

        inst:WatchWorldState("isdusk", 
            function() 
                local function getspeech()
                    return STRINGS.CITY_PIG_GUARD_LIGHT_TORCH.DEFAULT[math.random(#STRINGS.CITY_PIG_GUARD_LIGHT_TORCH.DEFAULT)]
                end
                inst.sayline(inst, getspeech())
                --inst.components.talker:Say(getspeech(), 1.5, nil, true)
                inst:DoTaskInTime(0.5+(math.random()*1),function() 
                        local hat = inst.components.inventory:FindItem(
                        function(item)
                            if item.prefab == "minerhat" then
                                return true
                            end
                        end)
						if hat then 
							hat.components.fueled.currentfuel = TUNING.MINERHAT_LIGHTTIME
							inst.components.inventory:Equip(hat) 
						end 
                    end)
            end)

        inst:ListenForEvent("dropitem", 
            function(it, data)
                if data.item.prefab == "minerhat" then
                    data.item:Remove()
                end
                if data.item.prefab == "halberdp" then
                    normalizehalberd(data.item)
                end
            end)
		inst:ListenForEvent("unequip", function(inst)
			local hat = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HEAD)
			if not hat then
				inst.AnimState:Show("HAT")
			end
		end)
		inst:ListenForEvent("equip", function(inst)
			local hat = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HEAD)
			if not hat then
				inst.AnimState:Show("HAT")
			end
		end)

        inst:ListenForEvent("death", 
            function()
                local torch = inst.components.inventory:FindItem(
                    function(item)
                        if item.prefab == "torch" then
                            return true
                        end
                    end)
                if torch then 
                    normalizetorch(torch)
                end

                local axe = inst.components.inventory:FindItem(
                    function(item)
                        if item.prefab == "halberdp" and item.components.finiteuses and item.components.finiteuses.unlimited_uses then
                            return true
                        end
                    end)
                if axe then
                    normalizehalberd(axe)
                end
            end)

        local brain = require "brains/royalpigguardpbrain"
        inst:SetBrain(brain)
        return inst
    end

    local function make_shopkeeper()
        local inst = make_common()

		if not TheWorld.ismastersim then
			return inst
		end
        inst.components.sleeper.onlysleepsfromitems= true
        
        inst.AnimState:AddOverrideBuild("townspig_shop_wip")
        inst:AddTag("shopkeep")
        inst.separatedesk = separatedesk
        inst.shopkeeper_speech = shopkeeper_speech  
        
        --GetWorld():ListenForEvent("enterroom", function(data) shopkeeper_speech(inst, getSpeechType(inst,STRINGS.CITY_PIG_SHOPKEEPER_GREETING) ) end ) --  getSpeechType(inst,speech) -- [math.random(1,#STRINGS.CITY_PIG_SHOPKEEPER_GREETING)]
        inst:ListenForEvent("nighttime", function() closeshop(inst) end, GetWorld())

        inst.special_action = function (act)
            inst.sg:GoToState("desk_pre")
        end


        inst:ListenForEvent("death", 
            function()
                local x,y,z = inst.Transform:GetWorldPosition()
                local shops = TheSim:FindEntities(x,y,z, 30, {"shop_pedestal"},{"INLIMBO"}) 
                for i,ent in ipairs(shops)do
                    ent:AddTag("nodailyrestock")
                end
            end)        

        return inst
    end

    local function make_mechanic()
        local inst = make_common()
		inst:DoTaskInTime(0, function()
			-- Get rid of any hammers we have, cuz bugs
			local numHammers = inst.components.inventory:Count("hammer")
			local hammers = inst.components.inventory:GetItemByName("hammer", numHammers)
			for k,v in pairs(hammers) do
				local thisHammer = inst.components.inventory:RemoveItem(k, true)
				thisHammer:Remove()
			end
			-- and give us a brand new one
			local tool = SpawnPrefab("hammer")
			if tool then
				inst.components.inventory:GiveItem(tool)
				inst.components.inventory:Equip(tool)
			end
		end)

        return inst
    end

    local function make_queen()
        local inst = make_common()
		
		
		inst.entity:SetPristine()

		if not TheWorld.ismastersim then
			return inst
		end
        inst.components.named.possiblenames = STRINGS.QUEENPIGNAMES
        inst.components.named:PickNewName()
        MakeCharacterPhysics(inst, 50, 0.75)

        return inst
    end

    --------------------------------------------------------------------------

    if name == "pigman_queen" then
        return make_queen
    end

    if name == "pigman_mechanic" then
        return make_mechanic
    end

    if shopkeeper or name == "pigman_shopkeep" then
        return make_shopkeeper
    end 

    if guard_pig then
        return make_pig_guard
    end

    return make_common
end

local function makepigman(name, build, fixer, guard_pig, shopkeeper, tags, sex, econprefab)   
    return Prefab(name, makefn(name, build, fixer, guard_pig, shopkeeper, tags, sex, econprefab), assets, prefabs )  
end

--                      name                        build         fixer  guard shop tags               sex
return  makepigman("pigman_beauticianp",         "pig_beautician",   nil,  nil, nil, nil,             "FEMALE" ),
        makepigman("pigman_floristp",            "pig_florist",      nil,  nil, nil, nil,             "FEMALE" ),
        makepigman("pigman_eruditep",            "pig_erudite",      nil,  nil, nil, nil,             "FEMALE" ),
        makepigman("pigman_hatmakerp",           "pig_hatmaker",     nil,  nil, nil, nil,             "FEMALE" ),
        makepigman("pigman_storeownerp",         "pig_storeowner",   nil,  nil, nil, {"emote_nohat"}, "FEMALE" ),
        makepigman("pigman_bankerp",             "pig_banker",       nil,  nil, nil, {"emote_nohat"}, "MALE"   ),
        makepigman("pigman_collectorp",          "pig_collector",    nil,  nil, nil, nil,             "MALE"   ),
        makepigman("pigman_hunterp",             "pig_hunter",       nil,  nil, nil, nil,             "MALE"   ),
        makepigman("pigman_mayorp",              "pig_mayor",        nil,  nil, nil, nil,             "MALE"   ),
        makepigman("pigman_mechanicp",           "pig_mechanic",     true, nil, nil, nil,             "MALE"   ),
        makepigman("pigman_professorp",          "pig_professor",    nil, nil, nil, nil,              "MALE"   ),
        makepigman("pigman_usherp",              "pig_usher",        nil, nil, nil, nil,              "MALE"   ),
        
        makepigman("pigman_royalguardp",         "pig_royalguard",   nil,  true, nil, nil, "MALE" ),
        makepigman("pigman_royalguard_2p",       "pig_royalguard_2", nil,  true, nil, nil, "MALE" ),       
        makepigman("pigman_farmerp",             "pig_farmer",       nil,  nil,  nil, nil, "MALE" ),
        makepigman("pigman_minerp",              "pig_miner",        nil,  nil,  nil, nil, "MALE" ),
        makepigman("pigman_queenp",              "pig_queen",        nil,  nil,  nil, {"pigqueen","emote_nohat"})