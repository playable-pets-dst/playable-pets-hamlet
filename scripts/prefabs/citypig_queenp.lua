local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--
---------------------------
local prefabname = "citypig_queenp"
--Don't add assets unless absolutely needed.
local assets = 
{
	
}
--Might be completely pointless here. I don't know.
local prefabs = 
{	
	"pigcrownp",
}
	
--Note: Becareful when adding starting items.
--People are likely to spam changecharacter to get more of them.	
local start_inv = 
{
	--'prefab',
	--"spear",
}

local start_inv2 =
{
   --"beehome",
}

if MOBHOUSE == "Enable1" or MOBHOUSE == "Enable3" then
	start_inv = start_inv2
end
-----------------------
--Stats--
local mob = 
{
	health = TUNING[string.upper(prefabname.."_HEALTH")],
	hunger = TUNING[string.upper(prefabname.."_HUNGER")],
	hungerrate = TUNING.WILSON_HUNGER_RATE, 
	sanity = TUNING[string.upper(prefabname.."_SANITY")],
	runspeed = 5,
	walkspeed = 3,
	damage = 20*2,
	range = 3,
	bank = "townspig",
	build = "pig_queen",
	shiny = "pig_queen",
	--build2 = "bee_guard_puffy_build",
	scale = 1,
	stategraph = "SGcitypigp",
	minimap = "citypig_queenp.tex",
	
}
-----------------------
--Loot that drops when you die, duh.
SetSharedLootTable('citypig_queenp',
{
    {'meat',  1.00},
	{'meat',  1.00},
	{'pigskin',  1.00},
})


local function OnDoneTalking(inst)
    if inst.talktask ~= nil then
        inst.talktask:Cancel()
        inst.talktask = nil
    end
    inst.SoundEmitter:KillSound("talk")
end

local function OnTalk(inst, script, mood)
	----print("DEBUG: OnTalk ran")
    inst.SoundEmitter:PlaySound("dontstarve/pig/grunt")
	--[[
    if inst:HasTag("guard") then
        if mood and mood == "alarmed" then
            inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/city_pig/guard_alert")
        else
            inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/city_pig/conversational_talk_gaurd","talk")
        end
    else
        if inst.female and inst.female == true then
            if mood and mood == "alarmed" then
                inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/city_pig/scream_female")
            else
               inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/city_pig/conversational_talk_female","talk")
            end
        else
            if mood and mood == "alarmed" then
                inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/city_pig/scream")
            else
				----print("DEBUG: OnTalk2 ran")
    	       inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/city_pig/conversational_talk","talk")
            end
        end
		inst.talktask = inst:DoTaskInTime(1.5 + math.random() * .5, OnDoneTalking)
    end]]
end

local function GetExcludeTags(inst)
	if TheNet:GetPVPEnabled() then
		return {"lightningimmune", "lightningrod", "notarget"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion", "lightningimmune", "lightningrod", "notarget"}
	else	
		return {"player", "companion", "lightningimmune", "lightningrod", "notarget"}
	end
end

local function onattacked(inst, data)
    inst.components.combat:SetTarget(data.attacker)
    inst.components.combat:ShareTarget(data.attacker, 30, function(dude) return dude:HasTag("pig") and not dude.components.health:IsDead() end, 30)
end

local function onhitother2(inst, other)
    inst.components.combat:SetTarget(other)
    inst.components.combat:ShareTarget(other, 30, function(dude) return dude:HasTag("pig") and not dude.components.health:IsDead() end, 30)
end

local function EquipHat(inst) --If a hat removes the head, this fixes that.
    local head = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HEAD)
	if head then
		inst.AnimState:Show("HEAD")
		if inst.hashat and inst.hashat == true then
			inst.AnimState:Hide("headtopper")
		end
	end
end

local function UnEquipHat(inst) --If a hat removes the head, this fixes that.
    local head = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HEAD) or nil
	if not head then
		inst.AnimState:Show("HEAD")
		if inst.hashat and inst.hashat == true then
			inst.AnimState:Show("headtopper")
		end
	end
end
------------------------------------------------------
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
end
-------------------------------------------------------
local common_postinit = function(inst) 
	inst.MiniMapEntity:SetIcon(mob.minimap)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false)
	  --ThePlayer.HUD.controls.status.brain:Hide()
		if MONSTERHUNGER == "Disable" then
			--ThePlayer.HUD.controls.status.stomach:Hide()
		end
   end
end)
	------------------------
	--Tags--
    inst:AddTag("pig")
	inst:AddTag("pig_queen")
	inst:AddTag("civilised")
	------------------------
	inst.components.talker.symbol = "head_base"
	inst.components.talker.mod_str_fn = string.utf8upper
	--inst.components.talker.ontalk = ontalk
	--inst.components.talker.offset = Vector3(0,-900,0)	
end
-------------Forge------------------------------------------
local function DoAttackBuff(target)
    if target:IsValid() and not target.components.health:IsDead() and not target.buffimmune and not target.defbuffed and target.components.debuffable then
		target.components.debuffable:AddDebuff("guard_buff", "guard_buff")	
	end
end

local function DoSelfBuff(inst)
	--OnPulse(inst)
	if inst.components.health and not inst.components.health:IsDead() then
    local x, y, z = inst.Transform:GetWorldPosition()
    local ents = TheSim:FindEntities(x, y, z, 10, nil, { "playerghost", "ghost", "battlestandard", "INLIMBO" }, {"player", "guard", "pig"} )
		if #ents > 1 then
			DoAttackBuff(inst)
		end
	end
end

local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, PPHAM_FORGE.CITYPIG)
	
	inst.mobsleep = false
	--inst.acidmult = 1.25
	
	inst:DoTaskInTime(0, function(inst)
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"melees", "darts"})
	end)
	
	inst.DoAura = inst:DoPeriodicTask(TUNING.FORGE.BATTLESTANDARD.PULSE_TICK, DoSelfBuff)
	
	inst.components.combat:SetDamageType(1)
	
	inst.components.revivablecorpse.revivespeedmult = 1
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end
-----------------------------------------------------------------------

local master_postinit = function(inst)
	------------------------------------------
	--Stats--
    PlayablePets.SetCommonStats(inst, mob, nil, true) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst) --heat, cold, wetness	
	----------------------------------
	--Loot drops--
	inst:AddComponent("lootdropper")
    inst.components.lootdropper:SetChanceLootTable("citypig_queenp")
	----------------------------------	
	inst.AnimState:Hide("hat")
	inst.AnimState:Hide("desk")
	inst.AnimState:Hide("ARM_carry")
	
	inst.mobsleep = true
	inst.taunt = true
	inst.taunt2 = true
	
	--inst.mobplayer = true
	inst.ghostbuild = "ghost_monster_build"

	inst.isshiny = 0
	
	inst.female = true
	inst.hashat = false
	
    local body_symbol = "pig_torso"
	inst.poisonsymbol = body_symbol
	MakeMediumBurnableCharacter(inst, body_symbol)
    MakeMediumFreezableCharacter(inst, body_symbol)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	PlayablePets.SetCommonStatResistances(inst) --fire, acid, poison
	----------------------------------
	--Eater--

	---------------------------------
	--Physics and Scale--
	
	inst.DynamicShadow:SetSize(1.5, 0.75)

    MakeCharacterPhysics(inst, 50, .5)
    inst.Transform:SetFourFaced()
	---------------------------------
	--Listeners--
	inst.components.talker.symbol = "head_base"
	inst:ListenForEvent("equip", EquipHat) --Shows head when hats make heads disappear.
	inst:ListenForEvent("unequip", UnEquipHat) --Shows head when hats make heads disappear.
	
	inst:ListenForEvent("ontalk", OnTalk)
    inst:ListenForEvent("donetalking", OnDoneTalking)
	inst:ListenForEvent("attacked", onattacked)
	inst.components.combat.onhitotherfn = onhitother2
	---------------------------------
	--Functions that saves and loads data.

    inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	-------------------------------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
	-------------------------------------------------------
	inst:DoTaskInTime(0, function(inst)
		if not TheSim:FindFirstEntityWithTag("pigcrown") then
			inst.components.inventory:GiveItem(SpawnPrefab("pigcrownp"))
		end
	end)
    ------------------------------------------------------	
	
	inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob, nil, nil, nil, true) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob, nil, nil, nil, true) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob, nil, nil, nil, true) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman, amphibious)
    end)
	
    return inst
	
end

return MakePlayerCharacter("citypig_queenp", prefabs, assets, common_postinit, master_postinit, start_inv)
