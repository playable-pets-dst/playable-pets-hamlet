local assets=
{
	Asset("ANIM", "anim/blunderbuss_projectile.zip"),
}

local prefabs = {}

--[[
function RotateToTarget(dest)
    local direction = (dest - inst:GetPosition()):GetNormalized()
    local angle = math.acos(direction:Dot(Vector3(1, 0, 0))) / DEGREES
    inst.Transform:SetRotation(angle)
    inst:FacePoint(dest)
end

function OnUpdate(dt)
    local target = target
    if homing and target ~= nil and target:IsValid() and not target:IsInLimbo() then
        dest = target:GetPosition()
    end
    local current = inst:GetPosition()
    if range ~= nil and distsq(start, current) > range * range then
        Miss(target)
    elseif not homing then
        if target ~= nil and target:IsValid() and not target:IsInLimbo() then
            local range = target:GetPhysicsRadius(0) + hitdist
            -- V2C: this is 3D distsq (since combat range checks use 3D distsq as well)
            -- NOTE: used < here, whereas combat uses <=, just to give us tiny bit of room for error =)
            if distsq(current, target:GetPosition()) < range * range then
                Hit(target)
            end
        end
    elseif target ~= nil
        and target:IsValid()
        and not target:IsInLimbo()
        and target.entity:IsVisible()
        and (target.sg == nil or
            not (target.sg:HasStateTag("flight") or
                target.sg:HasStateTag("invisible"))) then
        local range = target:GetPhysicsRadius(0) + hitdist
        -- V2C: this is 3D distsq (since combat range checks use 3D distsq as well)
        -- NOTE: used < here, whereas combat uses <=, just to give us tiny bit of room for error =)
        if distsq(current, target:GetPosition()) < range * range then
            Hit(target)
        else
            local direction = (dest - current):GetNormalized()
            local projectedSpeed = speed * TheSim:GetTickTime() * TheSim:GetTimeScale()
            local projected = current + direction * projectedSpeed
            if direction:Dot(dest - projected) >= 0 then
                RotateToTarget(dest)
            else
                Hit(target)
            end
        end
    elseif owner == nil or
        not owner:IsValid() or
        owner.components.combat == nil or
        inst.components.weapon == nil or
        inst.components.weapon.attackrange == nil then
        -- Lost our target, e.g. bird flew away
        Miss(target)
    else
        -- We have enough info to make our weapon fly to max distance before "missing"
        local range = owner.components.combat.attackrange + inst.components.weapon.attackrange
        if distsq(owner:GetPosition(), current) > range * range then
            Miss(target)
        end
    end
end]]

local function OnHit(inst, owner, target)
    if target ~= nil and target:IsValid() then
        local fx = SpawnPrefab("impact")
        fx.Transform:SetPosition(target:GetPosition():Get())
        target.components.combat:GetAttacked(owner, 50)
        inst:Remove()
    end
end

local function OnMiss(inst)
    inst:Remove()
end

local function OnThrown(inst)
    inst.Physics:ClearCollisionMask()
	inst.Physics:CollidesWith(COLLISION.CHARACTERS)
end

local function onremove(inst)

end

local function fn()
	local inst = CreateEntity()
	local trans = inst.entity:AddTransform()
	inst.entity:AddNetwork()
	inst.entity:AddAnimState()
	inst.entity:AddSoundEmitter()
	
	MakeInventoryPhysics(inst)

	inst.AnimState:SetBank("blunderbuss_projectile")
	inst.AnimState:SetBuild("blunderbuss_projectile")
	inst.AnimState:PlayAnimation("idle_loop", true)
	inst:SetPrefabNameOverride("pig_footsoldier_blue")

    inst:AddTag("thrown")
	inst:AddTag("projectile")
	
	inst.entity:SetPristine()
	
	if not TheWorld.ismastersim then
        return inst
    end

	inst:AddComponent("projectile")
	inst.components.projectile:SetSpeed(40)
	inst.components.projectile:SetRange(50)
    inst.components.projectile:SetOnThrownFn(OnThrown)
    inst.components.projectile:SetOnHitFn(OnHit)
    inst.components.projectile:SetOnMissFn(OnMiss)
    inst.components.projectile.homing = false
	
    inst:DoTaskInTime(10, inst.Remove)

	inst.OnRemoveEntity = onremove

	inst.persists = false

	return inst
end

return Prefab("blunderbuss_projectile", fn, assets, prefabs)