local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--
---------------------------

local assets = 
{
	Asset("ANIM", "anim/pog_basic.zip"),
	Asset("ANIM", "anim/pog_actions.zip"),
	Asset("ANIM", "anim/pog_feral_build.zip"),
}

local prefabs = 
{	

}

local start_inv = 
{
	--'prefab',
	--"spear",
}

local start_inv2 =
{
   --"beehome",
}

if MOBHOUSE == "Enable1" or MOBHOUSE == "Enable3" then
	start_inv = start_inv2
end
-----------------------
--Stats--
local mob = 
{
	health = 200,
	hunger = 150,
	hungerrate = TUNING.WILSON_HUNGER_RATE, 
	sanity = 150,
	
	runspeed = 6,
	walkspeed = 3,
	
	attackperiod = 0,
	damage = 30,
	range = 2.5,
	hit_range = 3,
	
	bank = "pog",
	build = "pog_actions",
	build2 = "pog_feral_build",
	shiny = "pog",
	
	scale = 1,
	stategraph = "SGpogp",
	minimap = "pogp.tex",	
}

--Loot that drops when you die, duh.
SetSharedLootTable( 'pogp',
{
    {'smallmeat',  1},
})

local FORGE_HP_TRIGGER = 0.3
local FORGE_STATS = PPHAM_FORGE.POG
--==============================================
--					Mob Functions
--==============================================

--==============================================
--				Custom Common Functions
--==============================================
local function SetNormal(inst)
	
	if TheNet:GetServerGameMode() ~= "lavaarena" then
		inst.transformed = nil
		local healthpercent = inst.components.health:GetPercent()
		inst.components.health:SetMaxHealth(mob.health)
		inst.components.health:SetPercent(healthpercent)
	end	
	
	inst.components.hunger:SetRate(mob.hungerrate)
	
	inst.components.combat:SetDefaultDamage(TheNet:GetServerGameMode() == "lavaarena" and FORGE_STATS.DAMAGE or mob.damage)

	inst.AnimState:SetBuild(inst.isshiny ~= 0 and "pog_shiny_build_0"..inst.isshiny or mob.build)
	
	inst.components.locomotor:RemoveExternalSpeedMultiplier(inst, 1.2)
end

local function SetAporkalypse(inst)
	if (TheWorld.state.isfullmoon and TheWorld.state.isnight) or (TheNet:GetServerGameMode() == "lavaarena" and inst.components.health:GetPercent() <= FORGE_HP_TRIGGER) then
		inst.transformed = true
	
		inst.components.hunger:SetRate(0.25)
	
		inst.components.combat:SetDefaultDamage(TheNet:GetServerGameMode() == "lavaarena" and FORGE_STATS.DAMAGE * 1.5 or mob.damage * 1.5)
	
		inst.AnimState:SetBuild(inst.isshiny ~= 0 and "pog_feral_shiny_build_0"..inst.isshiny or mob.build2)
	
		inst.components.locomotor:SetExternalSpeedMultiplier(inst, 1.2, 1.2)
		
		if TheNet:GetServerGameMode() ~= "lavaarena" then
			local healthpercent = inst.components.health:GetPercent()
			inst.components.health:SetMaxHealth(mob.health * 2)
			inst.components.health:SetPercent(healthpercent)
			inst:WatchWorldState("startday", SetNormal)
		elseif TheNet:GetServerGameMode() == "lavaarena" then
			inst.cursetask = inst:DoTaskInTime(15, SetNormal)
		end
	else
		SetNormal(inst)
	end
end
--==============================================
--					Loading/Saving
--==============================================
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
end

--==============================================
--					Forged Forge
--==============================================

local function OnAttacked_Forge(inst, data)
	if data.attacker and data.attacker.components.combat then
		if math.random(1, 10) >= 3 then
			data.attacker.components.combat:SetTarget(nil)
		end
	end
end

local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, PPHAM_FORGE.POG)
	
	inst.mobsleep = false	
	
	inst:DoTaskInTime(0, function(inst)	
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"books", "staves", "darts"})
	end)
	
	
	inst:ListenForEvent("attacked", OnAttacked_Forge)
	--inst.components.combat.onhitotherfn = OnForgeHitOther
	
	inst:ListenForEvent("healthdelta", function(inst)
		local health = inst.components.health:GetPercent()
		if health >= 0.5 then
			inst.transformed = nil
		elseif health <= FORGE_HP_TRIGGER and not inst.transformed then
			SetAporkalypse(inst)
		end	
	end)
	
	inst.components.combat:SetDamageType(1)
	
	inst.components.revivablecorpse.revivespeedmult = 0.75
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

--==============================================
--					Common/Master
--==============================================

local common_postinit = function(inst) 
	inst.MiniMapEntity:SetIcon(mob.minimap)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false) --PP doesn't work with movement prediction enabled, due to use of custom stategraphs
   end
end)
	--[[
	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst)]]
end



local master_postinit = function(inst)
	--Stats--
	PlayablePets.SetCommonStats(inst, mob, nil, true) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst, 1, 1, 1) --fire, acid, poison
	inst.components.combat:SetHurtSound("dontstarve_DLC003/creatures/pog/hit")
	----------------------------------
	--Tags--
    inst:RemoveTag("character")
	inst:AddTag("pog")
	
	----------------------------------
	--Variables	
	inst.mobsleep = true
	inst.taunt = true
	inst.taunt2 = true
	inst.shouldwalk = false
	
	local body_symbol = "pog_chest"
	inst.poisonsymbol = body_symbol
	MakeSmallBurnableCharacter(inst, body_symbol)
    MakeTinyFreezableCharacter(inst, body_symbol)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	----------------------------------
	--Components
	PlayablePets.SetCommonLootdropper(inst) --inst, prefaboverride
	----------------------------------
	--Eater--
    inst.components.eater:SetAbsorptionModifiers(1,1,1) --This multiplies food stats.
	inst.components.eater:SetCanEatHorrible() --check to make them eat monster meat.
	inst.components.eater.strongstomach = true -- can eat monster meat!
	---------------------------------
	--Physics and Shadows--
	MakeCharacterPhysics(inst, 1, .5)
	inst.Transform:SetFourFaced()
	
	inst.DynamicShadow:SetSize(2, 0.75)
	--inst.DynamicShadow:Enable(false)  
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	
	---------------------------------
	--Functions
	inst:WatchWorldState("isfullmoon", SetAporkalypse)
    inst:DoTaskInTime(2, SetAporkalypse)
	---------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
    ------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob) end)
    end)
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
    return inst	
end

return MakePlayerCharacter("pogp", prefabs, assets, common_postinit, master_postinit, start_inv)