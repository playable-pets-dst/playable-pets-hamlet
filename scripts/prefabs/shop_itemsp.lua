require "prefabutil"
require "recipes"

local assets =
{
	
}

local function Transform(inst, original, owner, method)
	----print("DEBUG: Shop item is mimicking "..original)
	if original then
		--if method and method == "onputininventory" then
			if owner then
				
				inst:DoTaskInTime(0, function(inst) 
					local getowner = inst.components.inventoryitem.owner
					local owner = getowner.components.inventory and getowner.components.inventory or getowner.components.container --if "owner" isn't the character itself, then assume its a bag
					--local owner_get = (getowner.components and getowner.components.inventory) and getowner:GetDisplayName() or (owner.components and owner.components.inventoryitem.owner:GetDisplayName()) and  --This is just for the --printing
					local slot = owner:GetItemSlot(inst)
					local pos = getowner:GetPosition()
					----print("Shopping Log: "..owner_get.." bought "..original.." at coordinates: "..pos.x..", "..pos.y..", "..pos.z)
					inst:Remove()
					if inst.amount and inst.amount > 1 then
						for i = 1, inst.amount or 1 do
							if owner then
								owner:GiveItem(SpawnPrefab(original))
							end
						end
					else
						owner:GiveItem(SpawnPrefab(original))
					end
				end)
				
			end
		--end
	end		
end

local function OnDrop(inst, original)
	inst:DoTaskInTime(1, function(inst)
		if inst.original then
			SpawnPrefab(inst.original).Transform:SetPosition(inst:GetPosition():Get())
			inst:Remove()
		end
	end)		
end

local function makefn(name, original, amount)

    local function fn(Sim)
        local inst = CreateEntity()
        local trans = inst.entity:AddTransform()
        local anim = inst.entity:AddAnimState()
        local light = inst.entity:AddLight()
        inst.entity:AddSoundEmitter()
		inst.entity:AddNetwork()
	
		MakeInventoryPhysics(inst)
		
		inst.AnimState:SetBank("nightmarefuel")
		inst.AnimState:SetBuild("nightmarefuel")
		inst.AnimState:PlayAnimation("idle_loop", true)
		inst.AnimState:SetMultColour(1, 1, 1, 0.5)
		
		inst.entity:SetPristine()
	
		if not TheWorld.ismastersim then
			return inst
		end
		
		inst.original = original
		
		inst.amount = amount and amount or nil
		
		inst:AddComponent("inspectable")
		
		inst:AddComponent("inventoryitem")
		inst.components.inventoryitem:SetOnDroppedFn(OnDrop)
		
		inst:ListenForEvent("onputininventory", function(inst, owner) Transform(inst, inst.original, owner, "onputininventory") end)
		
		inst:DoTaskInTime(3, function(inst)
			--print("ERROR: Something went horrible wrong with shop_itemp, attempted mimicry is "..inst.original)
			inst:Remove()
		end)
		

        return inst
    end
    return fn
end

local function makeshopitem(name, original, amount)   
    return Prefab(name, makefn(name, original, amount), assets, prefabs )
end

local prefs = {}
local items = require("ppham_shoplist")
for i, v in ipairs(items) do
	local item = makeshopitem(v.."_shopp", v)
	table.insert(prefs, item)
end
table.insert(prefs, makeshopitem("oincp_10_shopp", "oincp", 10))
table.insert(prefs, makeshopitem("oinc10p_10_shopp", "oinc10p", 10))

return unpack(prefs)