local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--
---------------------------

local assets = 
{
	Asset("ANIM", "anim/peagawk_basic.zip"),
    Asset("ANIM", "anim/peagawk_actions.zip"),
    Asset("ANIM", "anim/peagawk_charge.zip"),
    Asset("ANIM", "anim/peagawk_build.zip"),
    Asset("ANIM", "anim/peagawk_prism_build.zip"),
    Asset("ANIM", "anim/eyebush.zip"),
    Asset("ANIM", "anim/eyebush_prism_build.zip"),
}

local prefabs = 
{	

}

local start_inv = 
{
	--'prefab',
	--"spear",
}

local start_inv2 =
{
   --"beehome",
}

if MOBHOUSE == "Enable1" or MOBHOUSE == "Enable3" then
	start_inv = start_inv2
end

local prefabname = "peagawkp"
-----------------------
--Stats--
local mob = 
{
	health = 125,
	hunger = 150,
	hungerrate = TUNING.WILSON_HUNGER_RATE, 
	sanity = 150,
	
	runspeed = 8,
	walkspeed = 3,
	
	attackperiod = 0,
	damage = 50,
	range = 2.5,
	hit_range = 3,
	
	bank = "peagawk",
	build = "peagawk_build",
	build2 = "peagawk_prism_build",
	shiny = "peagawk",
	
	scale = 1,
	stategraph = "SG"..prefabname,
	minimap = prefabname..".tex",	
}

--Loot that drops when you die, duh.
SetSharedLootTable( 'peagawkp',
{
    {'drumstick',  1},
	{'drumstick',  1},
})

local FORGE_HP_TRIGGER = 0.5
local FORGE_STATS = PPHAM_FORGE.PEAGAWK
--==============================================
--					Mob Functions
--==============================================
local function TransformToRainbow(inst)
    if inst.is_bush then
        inst.AnimState:SetBuild("eyebush_prism_build")
    else
        inst.AnimState:SetBuild("peagawk_prism_build")
    end

    inst.prism = true
    inst.prismdroptask = inst:DoPeriodicTask(1, function() inst:PrismDrop() end)
    inst.prismdropped = 0

    inst.components.pickable:SetUp("peagawkfeather_prism", TUNING.PEAGAWK_REGROW_TIME)
    inst:SetupPrismTimer(PRISM_STOP_TIMER)
end 

local function UnRainbow(inst)
    inst.prism = false

    if inst.is_bush then
        inst.AnimState:SetBuild("eyebush")
    else
        inst.AnimState:SetBuild("peagawk_build")
    end

    if inst.prismdroptask ~= nil then
        inst.prismdroptask:Stop()
    end 
end 

local function PrismDrop(inst)
    local pos = inst.Transform:GetWorldPosition()
    if distsq(Vector3(pos), Vector3(inst.lastpos)) >= 10*10 and inst.prismdropped < MAX_PRISM_DROPS then
        local feather = SpawnPrefab("peagawkfeather_prism")
        local x,y,z = inst.Transform:GetWorldPosition()
        feather.Transform:SetPosition(x,y,z)
        inst.lastpos = pos 
        inst.prismdropped = inst.prismdropped + 1
    end
end

local function SetupPrismTimer(inst, prismtimer)
    inst.prismtimer = prismtimer
    inst.prismtask = inst:DoTaskInTime(inst.prismtimer, function() inst:UnRainbow() end)
end

local function TransformToAnimal(inst, ignore_state)
    inst.AnimState:SetBank("peagawk")
    inst.components.inspectable.nameoverride = nil

    if inst.prism then
        inst.AnimState:SetBuild("peagawk_prism_build")
    else
        inst.AnimState:SetBuild("peagawk_build")
    end

    inst.is_bush = false

    if not ignore_state then
        inst.sg:GoToState("appear")
    end
end

local function TransformToBush(inst)
    inst.AnimState:SetBank("eyebush")
    inst.components.inspectable.nameoverride = "peagawk_bush"

    if inst.prism then
        inst.AnimState:SetBuild("eyebush_prism_build")
    else
        inst.AnimState:SetBuild("eyebush")
    end

    inst.is_bush = true
end
--==============================================
--				Custom Common Functions
--==============================================
local function SetNormal(inst)
	
	if TheNet:GetServerGameMode() ~= "lavaarena" then
		inst.transformed = nil
		local healthpercent = inst.components.health:GetPercent()
		inst.components.health:SetMaxHealth(mob.health)
		inst.components.health:SetPercent(healthpercent)
	end	
	inst.prism = false
	inst.debuffimmune = nil
	inst.inkimmune = nil
		
	PlayablePets.SetCommonStatResistances(inst, 1, 1, 1) --fire, acid, poison

	if inst.is_bush then
		inst.AnimState:SetBuild(inst.isshiny ~= 0 and "eyebush_shiny_build_0"..inst.isshiny or "eyebush")
	else
		inst.AnimState:SetBuild(inst.isshiny ~= 0 and "peagawk_shiny_build_0"..inst.isshiny or mob.build)
	end
	
	inst.components.locomotor:RemoveExternalSpeedMultiplier(inst, 1.2)
end

local function SetAporkalypse(inst)
	if (TheWorld.state.isfullmoon and TheWorld.state.isnight) or (TheNet:GetServerGameMode() == "lavaarena" and inst.components.health:GetPercent() <= FORGE_HP_TRIGGER) then
		inst.transformed = true
		inst.prism = true
		inst.debuffimmune = true
		inst.inkimmune = true
	
		PlayablePets.SetCommonStatResistances(inst, 0, 0, 0) --fire, acid, poison
	
		if inst.is_bush then
			inst.AnimState:SetBuild(inst.isshiny ~= 0 and "eyebush_prism_shiny_build_0"..inst.isshiny or "eyebush_prism_build")
		else
			inst.AnimState:SetBuild(inst.isshiny ~= 0 and "peagawk_prism_shiny_build_0"..inst.isshiny or mob.build2)
		end	
		
		if TheNet:GetServerGameMode() ~= "lavaarena" then
			inst:WatchWorldState("startday", SetNormal)
		end
	else
		SetNormal(inst)
	end
end
--==============================================
--					Loading/Saving
--==============================================
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
end

--==============================================
--					Forged Forge
--==============================================

local function OnAttacked_Forge(inst, data)
	if data.attacker and data.attacker.components.combat then
		if math.random(1, 10) >= 3 then
			data.attacker.components.combat:SetTarget(nil)
		end
	end
end

local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, PPHAM_FORGE.PEAGAWK)
	
	inst.mobsleep = false	
	
	inst:DoTaskInTime(0, function(inst)	
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"books", "staves"})
	end)
	
	
	inst:ListenForEvent("attacked", OnAttacked_Forge)
	--inst.components.combat.onhitotherfn = OnForgeHitOther
	
	inst:ListenForEvent("healthdelta", function(inst)
		local health = inst.components.health:GetPercent()
		if health > 0.5 then
			inst.transformed = nil
		elseif health <= FORGE_HP_TRIGGER and not inst.transformed then
			SetAporkalypse(inst)
		end	
	end)
	
	inst.components.combat:SetDamageType(1)
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

--==============================================
--					Common/Master
--==============================================

local common_postinit = function(inst) 
	inst.MiniMapEntity:SetIcon(mob.minimap)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false) --PP doesn't work with movement prediction enabled, due to use of custom stategraphs
   end
end)
	----------------------------------
	--Tags--
    inst:AddTag("berrythief")
    inst:AddTag("smallcreature")
	----------------------------------

	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst)
end



local master_postinit = function(inst)
	--Stats--
	PlayablePets.SetCommonStats(inst, mob, nil, true) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst, 1, 1, 1) --fire, acid, poison
	----------------------------------
	--Variables	
	inst.mobsleep = true
	inst.taunt = true
	inst.taunt2 = true
	inst.shouldwalk = false
	
	inst.TransformToRainbow = TransformToRainbow
    inst.UnRainbow = UnRainbow
    inst.SetupPrismTimer = SetupPrismTimer
    inst.prism = false 
    inst.prismtimer = 0
    inst.prismdropped = 0 
    inst.lastpos = inst.Transform:GetWorldPosition()
	inst.is_bush = false
    inst.TransformToBush = TransformToBush
    inst.TransformToAnimal = TransformToAnimal
	
	local body_symbol = "pig_torso"
	inst.poisonsymbol = body_symbol
	MakeMediumBurnableCharacter(inst, body_symbol)
    MakeMediumFreezableCharacter(inst, body_symbol)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	----------------------------------
	--Components
	PlayablePets.SetCommonLootdropper(inst, prefabname) --inst, prefaboverride
	----------------------------------
	--Eater--
    inst.components.eater:SetAbsorptionModifiers(1,1.75,1) --This multiplies food stats.
	inst.components.eater:SetDiet({ FOODTYPE.SEEDS, FOODTYPE.GENERIC ,FOODTYPE.VEGGIE }, { FOODTYPE.SEEDS, FOODTYPE.GENERIC ,FOODTYPE.VEGGIE }) 
	---------------------------------
	--Physics and Shadows--
	MakeCharacterPhysics(inst, 50, .5)
	inst.Transform:SetFourFaced()
	
	inst.DynamicShadow:SetSize(1.5, 0.75)
	--inst.DynamicShadow:Enable(false)  
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	
	---------------------------------
	--Functions
	inst:WatchWorldState("isfullmoon", SetAporkalypse)
    inst:DoTaskInTime(2, SetAporkalypse)
	---------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
    ------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob) end)
    end)
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
    return inst	
end

return MakePlayerCharacter(prefabname, prefabs, assets, common_postinit, master_postinit, start_inv)