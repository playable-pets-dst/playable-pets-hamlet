local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--
---------------------------
local prefabname = "gnatp"
--Don't add assets unless absolutely needed.
local assets = 
{
	Asset("ANIM", "anim/gnat.zip"),
}
--Might be completely pointless here. I don't know.
local prefabs = 
{	

}
	
--Note: Becareful when adding starting items.
--People are likely to spam changecharacter to get more of them.	
local start_inv = 
{
	--'prefab',
	--"spear",
}

local start_inv2 =
{
   --"beehome",
}

if MOBHOUSE == "Enable1" or MOBHOUSE == "Enable3" then
	start_inv = start_inv2
end
-----------------------
--Stats--
local mob = 
{
	health = 300,
	hunger = 300,
	hungerrate = 0.20, 
	sanity = 100,
	runspeed = 9,
	walkspeed = 3,
	damage = 10*2,
	range = 2,
	bank = "gnat",
	build = "gnat",
	shiny = "gnat",
	scale = 1,
	stategraph = "SGgnatp",
	minimap = prefabname..".tex",
	
}

--Loot that drops when you die, duh.
SetSharedLootTable(prefabname,
{

})

--==============================================
--				Custom Common Functions
--==============================================

local function GetExcludeTags(inst)
	if TheNet:GetPVPEnabled() then
		return {"gnat", "shadow", "notarget", "INLIMBO", "playerghost", "wall", "structure"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"gnat", "shadow", "notarget", "INLIMBO", "playerghost", "player", "companion", "wall"}
	else	
		return {"gnat", "shadow", "notarget", "INLIMBO", "playerghost", "player", "companion", "wall", "structure"}
	end
end

local function OnEat(inst, food)
	if food and food.components.perishable then
		local rot = SpawnPrefab("spoiled_food")
		local pos = inst:GetPosition()
		rot.Transform:SetPosition(pos.x, 2.5, pos.z)
	end
end

local function OnAttacked(inst, data)
	--we don't want to dodge non-living entities
	if data.attacker and data.attacker:IsValid() and data.attacker.components.health and not data.attacker:HasTag("structure") and not inst.sg.statemem.dodge_next_attack then 
		if math.random(1, 10) == 1 and not (inst.defbuffed or inst.defdebuffed) then
			inst.sg.statemem.dodge_next_attack = true
			inst.components.health:SetAbsorptionAmount(0)
		end
	else
		--just to ensure that dodge_next_attack doesn't mess with attacks from non-living entities, disable dodge_next_attack everytime it gets hit by them
		inst.sg.statemem.dodge_next_attack = nil
		if not (inst.defbuffed or inst.defdebuffed) then
			inst.components.health:SetAbsorptionAmount(1)
		end
	end
end

local function OnHitOther(inst, other)
	if other and other:IsValid() and other.components.health and not other.components.health:IsDead() then
		inst.components.hunger:DoDelta(1, false)
	end
end
--==============================================
--					Loading/Saving
--==============================================
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
end
--==============================================
--					Forged Forge
--==============================================
local FORGE_STATS = PPHAM_FORGE.GNAT
local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, FORGE_STATS)
	
	inst.mobsleep = false
	inst.taunt = false
	
	--inst.acidmult = 1.25
	
	inst:DoTaskInTime(0, function(inst)
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"books", "staves", "darts", "melees"})
	end)
	
	inst:DoTaskInTime(5, function(inst) 
		inst.Physics:ClearCollisionMask()
		inst.Physics:CollidesWith(COLLISION.WORLD)  
	end)
	
	inst.components.health:StartRegen(1, 1)
	
	--inst:ListenForEvent("healthdelta", function(inst) inst.components.combat:SetDefaultDamage(inst.components.health.currenthealth) end)
	
	inst.components.combat:SetDamageType(1)
	inst.components.revivablecorpse.revivespeedmult = 1

	inst:ListenForEvent("attacked", OnAttacked)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

--==============================================
--					Common/Master
--==============================================
local common_postinit = function(inst) 
	inst.MiniMapEntity:SetIcon(mob.minimap)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false) --PP doesn't work with movement prediction enabled, due to use of custom stategraphs
   end
end)
	----------------------------------
	--Tags--
	inst:AddTag("gnat")
	inst:AddTag("insect")
    inst:AddTag("flying")
	----------------------------------

	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst)
end

local master_postinit = function(inst)
	------------------------------------------
	--Stats--
    PlayablePets.SetCommonStats(inst, mob) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst, 10, 10, 10) --fire, acid, poison
	inst.components.locomotor:EnableGroundSpeedMultiplier(false)
    inst.components.locomotor:SetTriggersCreep(false)
	----------------------------------
	--Variables	
	inst.mobsleep = true
	--inst.taunt = true
	inst.taunt2 = true
	inst.shouldwalk = false
	----------------------------------
	--Loot drops--
	PlayablePets.SetCommonLootdropper(inst, "gnatp") --inst, prefaboverride
	----------------------------------
	--Tags--
    inst:AddTag("gnat")
	inst:AddTag("insect")
    inst:AddTag("flying")
	--inst:AddTag("groundpoundimmune")
	
	inst.mobsleep = true
	--inst.taunt = true
	inst.taunt2 = true
	
	inst.mobplayer = true
	inst.ghostbuild = "ghost_monster_build"

	inst.shouldwalk = false
	inst.isshiny = 0
	
	inst.AnimState:SetRayTestOnBB(true)
	local body_symbol = "fx_texture"
	inst.poisonsymbol = body_symbol
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	MakeLargeBurnableCharacter(inst, "fx_texture")
    MakeLargeFreezableCharacter(inst, "fx_texture")
	
	inst:AddComponent("aura")
    inst.components.aura.radius = 2
    inst.components.aura.tickperiod = 0.75
	inst.components.aura:Enable(true)
	inst.components.aura.auraexcludetags = GetExcludeTags(inst)
	----------------------------------
	--Eater--

	inst.components.eater:SetOnEatFn(OnEat)
	--inst.components.eater:SetDiet({ FOODTYPE.SEEDS, FOODTYPE.GENERIC ,FOODTYPE.VEGGIE }, { FOODTYPE.SEEDS, FOODTYPE.GENERIC ,FOODTYPE.VEGGIE }) 
    inst.components.eater:SetAbsorptionModifiers(4,3,3) --This multiplies food stats.
	inst.components.eater:SetCanEatHorrible() --check to make them eat monster meat.
	inst.components.eater.strongstomach = true -- can eat monster meat!
	---------------------------------
	--Physics and Scale--
	
	MakeFlyingCharacterPhysics(inst, 1, .25)
    inst.Transform:SetScale(mob.scale, mob.scale, mob.scale)
    inst.DynamicShadow:SetSize(2, 0.6)
	--inst.DynamicShadow:Enable(false) --Disables shadows.
    inst.Transform:SetFourFaced()
	
	PlayablePets.SetAmphibious(inst, nil, nil, true)
	---------------------------------
	inst:AddComponent("sanityaura")
    inst.components.sanityaura.aura = -TUNING.SANITYAURA_TINY * 2
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.EquipHat) --Shows head when hats make heads disappear.
	inst:ListenForEvent("attacked", OnAttacked) --Shows head when hats make heads disappear.
	inst.components.combat.onhitotherfn = OnHitOther
	---------------------------------
	--Functions that saves and loads data.

    inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
	--Forge--
	if inst.components.revivablecorpse then
		inst.forge_fn = SetForge(inst)
	end
    ------------------------------------------------------
	--Light and Character Functions. Don't Touch.--
    inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob, true, true) end)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob, true, true, nil, nil, true) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman, amphibious)
    end)
	
    return inst
	
end

return MakePlayerCharacter("gnatp", prefabs, assets, common_postinit, master_postinit, start_inv)
