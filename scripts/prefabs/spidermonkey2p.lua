local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--
---------------------------

--Don't add assets unless absolutely needed.
local assets = 
{
    Asset("ANIM", "anim/kiki_basic.zip"),
	Asset("ANIM", "anim/spidermonkey_build.zip"),
}
--Might be completely pointless here. I don't know.
local prefabs = 
{	

}
	
--Note: Becareful when adding starting items.
--People are likely to spam changecharacter to get more of them.	
local start_inv = 
{
	--'prefab',
	--"spear",
}

local start_inv2 =
{
   --"beehome",
}

if MOBHOUSE == "Enable1" or MOBHOUSE == "Enable3" then
	start_inv = start_inv2
end
-----------------------
--Stats--
local mob = 
{
	health = 175,
	hunger = 175,
	hungerrate = TUNING.WILSON_HUNGER_RATE, 
	sanity = 100,
	runspeed = TUNING.MONKEY_MOVE_SPEED,
	walkspeed = TUNING.MONKEY_MOVE_SPEED,
	damage = 32*2,
	range = 3,
	bank = "kiki",
	build = "spidermonkey_build",
	shiny = "spidermonkey2",
	--build2 = "bee_guard_puffy_build",
	scale = 1,
	stategraph = "SGspidermonkey2p",
	minimap = "spidermonkey2p.tex",
	
}
-----------------------
--Loot that drops when you die, duh.
SetSharedLootTable('spidermonkey2p',
{
    {'monstermeat',     1.0},
    {'beardhair',      0.75},
    {'silk',           0.25},
})

local function onattacked(inst, data)
    inst.components.combat:SetTarget(data.attacker)
    inst.components.combat:ShareTarget(data.attacker, 30, function(dude) return dude:HasTag("spidermonkey") and not dude.components.health:IsDead() end, 30)
end

local function onhitother2(inst, other)
    inst.components.combat:SetTarget(other)
    inst.components.combat:ShareTarget(other, 30, function(dude) return dude:HasTag("spidermonkey") and not dude.components.health:IsDead() end, 30)
end
-------

local function IsPoop(item)
    return item.prefab == "poop"
end

local function FireProjectile(inst, target)
    if target then
		local pos = inst:GetPosition()
		local proj = SpawnPrefab("monkeyprojectile")
		proj.Transform:SetPosition(pos.x, 0, pos.z)
		inst:DoTaskInTime(0, function(inst) proj.components.projectile.owner = inst end)
		proj.components.projectile.owner = inst
		proj.components.projectile:Throw(inst, target)
    end
end

local function OnEat(inst)
    --Eating food might cause pooping!
    if inst.components.inventory ~= nil then
        local maxpoop = 20
		local pooper = math.random()
        local poopstack = inst.components.inventory:FindItem(IsPoop)
        if poopstack == nil or poopstack.components.stackable.stacksize < maxpoop then
			if math.random() < 0.33 then
            inst.components.inventory:GiveItem(SpawnPrefab("poop"))
			end
        end
    end
end
------------------------------------------------------
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
end
-------------------------------------------------------
local common_postinit = function(inst) 
	inst.MiniMapEntity:SetIcon(mob.minimap)
	inst.soundtype = ""

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false)
	  --ThePlayer.HUD.controls.status.brain:Hide()
		if MONSTERHUNGER == "Disable" then
			--ThePlayer.HUD.controls.status.stomach:Hide()
		end
   end
end)

	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)

	PlayablePets.SetNightVision(inst)	
end

-------------Forge------------------------------------------
local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	inst.components.health:SetMaxHealth(PPHAM_FORGE.SPIDERMONKEY2.HEALTH)
	
	inst.components.combat:SetDefaultDamage(PPHAM_FORGE.SPIDERMONKEY2.DAMAGE)
	inst.components.combat:SetAttackPeriod(PPHAM_FORGE.SPIDERMONKEY2.ATTACKPERIOD)
	
	inst.components.locomotor.walkspeed = PPHAM_FORGE.SPIDERMONKEY2.WALKSPEED
    inst.components.locomotor.runspeed = PPHAM_FORGE.SPIDERMONKEY2.RUNSPEED
	
	inst.components.combat:SetRange(PPHAM_FORGE.SPIDERMONKEY2.ATTACK_RANGE, PPHAM_FORGE.SPIDERMONKEY2.HIT_RANGE)
	--inst.components.combat:SetAreaDamage(PPHAM_FORGE.TREEGUARD.AOE_RANGE, PPHAM_FORGE.TREEGUARD.AOE_DMGMULT)
	
	inst.mobsleep = false
	
	inst:RemoveComponent("periodicspawner")
	
	inst:DoTaskInTime(0, function(inst)
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"books", "staves"})
	inst.components.inventory:Equip(SpawnPrefab("poopweapon_forge"))
	end)
	
	inst.components.combat:SetDamageType(1)
	
	inst:AddComponent("corpsereviver")
	if inst.components.corpsereviver then
		inst.components.corpsereviver.reviver_speed_mult = 0.75
		inst.components.corpsereviver.additional_revive_health_percent = 0.2
	end
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end
-----------------------------------------------------------------------

local master_postinit = function(inst)
	------------------------------------------
	--Stats--
    PlayablePets.SetCommonStats(inst, mob, nil, true) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst) --fire, acid, poison
	----------------------------------
	--Loot drops--
	PlayablePets.SetCommonLootdropper(inst, "spidermonkey2p") --inst, prefaboverride
	----------------------------------
	--Tags--
	inst:AddTag("spider_monkey")
	inst:AddTag("spider")
	inst:AddTag("monster")
	
	inst.soundtype = ""
	
	inst.mobsleep = true
	inst.taunt = true
	inst.taunt2 = true
	inst._ismonster = true
	inst.soundtype = ""
	inst.poopmode = false
	inst.isadult = false
	inst.foodcount = 0

	inst.FireProjectile = FireProjectile

	
	local body_symbol = "kiki_lowerbody"
	inst.poisonsymbol = body_symbol
	MakeLargeBurnableCharacter(inst, body_symbol)
    MakeLargeFreezableCharacter(inst, body_symbol)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)

	inst.ghostbuild = "ghost_monster_build"
	----------------------------------
	--Eater--
	inst.components.eater:SetCanEatHorrible() --check to make them eat monster meat.
	inst.components.eater.strongstomach = true -- can eat monster meat!
	inst.components.eater:SetOnEatFn(OnEat) --Runs a function when the player eats something.
	---------------------------------
	--Physics and Scale--
	MakeCharacterPhysics(inst, 10, .25)
    inst.DynamicShadow:SetSize(2, 1.25)
    inst.Transform:SetSixFaced()
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	inst:ListenForEvent("attacked", onattacked)
	---------------------------------
	--Functions that saves and loads data.

    inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
    ------------------------------------------------------
	--Light and Character Functions. Don't Touch.--
	
	inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob, nil, true) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob, nil, true) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob, nil, true) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman, amphibious)
    end)
	
    return inst
end

return MakePlayerCharacter("spidermonkey2p", prefabs, assets, common_postinit, master_postinit, start_inv)
