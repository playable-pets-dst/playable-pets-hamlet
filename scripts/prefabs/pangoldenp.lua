local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--
---------------------------

--Don't add assets unless absolutely needed.
local assets = 
{
	Asset("ANIM", "anim/pango_basic.zip"),
    Asset("ANIM", "anim/pango_action.zip"),
}
--Might be completely pointless here. I don't know.
local prefabs = 
{	

}
	
--Note: Becareful when adding starting items.
--People are likely to spam changecharacter to get more of them.	
local start_inv = 
{
	--'prefab',
	--"spear",
}

local start_inv2 =
{
   --"beehome",
}

if MOBHOUSE == "Enable1" or MOBHOUSE == "Enable3" then
	start_inv = start_inv2
end
-----------------------
--Stats--
local mob = 
{
	health = 500,
	hunger = 175,
	hungerrate = TUNING.WILSON_HUNGER_RATE, 
	sanity = 100,
	runspeed = 8,
	walkspeed = 3,
	damage = 20*2,
	range = 4,
	bank = "pango",
	build = "pango_action",
	shiny = "pango",
	--build2 = "bee_guard_puffy_build",
	scale = 1,
	stategraph = "SGpangoldenp",
	minimap = "pangoldenp.tex",
	
}

--Loot that drops when you die, duh.
SetSharedLootTable('pangoldenp',
{
    {'meat',             1.00},
	{'meat',             1.00},
	{'meat',             1.00},
})

local function GetExcludeTags(inst)
	if TheNet:GetPVPEnabled() then
		return {"lightningimmune", "lightningrod", "notarget"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion", "lightningimmune", "lightningrod", "notarget"}
	else	
		return {"player", "companion", "lightningimmune", "lightningrod", "notarget"}
	end
end
------------------------------------------------------
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
end
-------------------------------------------------------

local common_postinit = function(inst) 
	inst.MiniMapEntity:SetIcon(mob.minimap)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false)
	  --ThePlayer.HUD.controls.status.brain:Hide()
		if MONSTERHUNGER == "Disable" then
			--ThePlayer.HUD.controls.status.stomach:Hide()
		end
   end
end)

	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)

	PlayablePets.SetNightVision(inst)
	
end

local function OnEat(inst)
	if math.random(1, 10) == 1 then
		inst.sg.mem.wants_to_poop = true
	end
end

-------------Forge------------------------------------------
local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	inst.components.health:SetMaxHealth(PPHAM_FORGE.PANGOLDEN.HEALTH)
	
	inst.components.combat:SetDefaultDamage(PPHAM_FORGE.PANGOLDEN.DAMAGE)
	inst.components.combat:SetAttackPeriod(PPHAM_FORGE.PANGOLDEN.ATTACKPERIOD)
	
	inst.components.locomotor.walkspeed = PPHAM_FORGE.PANGOLDEN.WALKSPEED
    inst.components.locomotor.runspeed = PPHAM_FORGE.PANGOLDEN.RUNSPEED
	
	inst.components.combat:SetRange(PPHAM_FORGE.PANGOLDEN.ATTACK_RANGE, PPHAM_FORGE.PANGOLDEN.HIT_RANGE)
	--inst.components.combat:SetAreaDamage(PPHAM_FORGE.HIPPO.AOE_RANGE, PPHAM_FORGE.HIPPO.AOE_DMGMULT)
	
	inst.mobsleep = false
	inst.taunt = false
	
	--inst.acidmult = 1.25
	
	inst:DoTaskInTime(0, function(inst)
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"books", "staves"})
	end)
	
	--inst.components.combat.onhitotherfn = OnForgeHitOther
	
	inst.components.combat:SetDamageType(1)
	
	inst.components.revivablecorpse.revivespeedmult = 1.5
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end
-----------------------------------------------------------------------

local master_postinit = function(inst)
	------------------------------------------
	--Stats--
    PlayablePets.SetCommonStats(inst, mob) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20, 1) --heat, cold, wetness
	----------------------------------
	--Loot drops--
	inst:AddComponent("lootdropper")
    PlayablePets.SetCommonLootdropper(inst, "pangoldenp")
	----------------------------------
	--Tags--
    inst:AddTag("pangolden")
    inst:AddTag("largecreature")
	--inst:AddTag("groundpoundimmune")
	
	inst.mobsleep = true
	inst.taunt = true
	inst.canhide = true
	inst.taunt3 = true
	inst.canpoop = true
	
	inst.mobplayer = true
	inst.ghostbuild = "ghost_monster_build"

	inst.shouldwalk = false
	inst.isshiny = 0

	local body_symbol = "swap_fire"
	inst.poisonsymbol = body_symbol
	MakeLargeBurnableCharacter(inst, "swap_fire")
    MakeLargeFreezableCharacter(inst, "pang_bod")
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	PlayablePets.SetCommonStatResistances(inst, 3, 1, 1) --fire, acid, poison, freeze (flat value, not a multiplier)
	----------------------------------
	--Eater--

	--inst.components.eater:SetDiet({ FOODTYPE.SEEDS, FOODTYPE.GENERIC ,FOODTYPE.VEGGIE }, { FOODTYPE.SEEDS, FOODTYPE.GENERIC ,FOODTYPE.VEGGIE }) 
	inst.components.eater:SetOnEatFn(OnEat)
    --inst.components.eater:SetAbsorptionModifiers(4,3,3) --This multiplies food stats.
	inst.components.eater:SetCanEatHorrible() --check to make them eat monster meat.
	inst.components.eater.strongstomach = true -- can eat monster meat!
	---------------------------------
	--Physics and Scale--
	
	MakeCharacterPhysics(inst, 100, .5)
    inst.DynamicShadow:SetSize(6, 2)
	--inst.DynamicShadow:Enable(false) --Disables shadows.
    inst.Transform:SetFourFaced()
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	---------------------------------
	--Functions that saves and loads data.

    inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
	--Forge--
	if inst.components.revivablecorpse then
		inst.forge_fn = SetForge(inst)
	end
    ------------------------------------------------------
	--Light and Character Functions. Don't Touch.--
    inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob) end)
    end)
	
    return inst
	
end

return MakePlayerCharacter("pangoldenp", prefabs, assets, common_postinit, master_postinit, start_inv)
