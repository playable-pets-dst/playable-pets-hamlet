local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--
---------------------------
local prefabname = "antmanp"
--Don't add assets unless absolutely needed.
local assets = 
{
	Asset("ANIM", "anim/antman_basic.zip"),
	Asset("ANIM", "anim/antman_attacks.zip"),
	Asset("ANIM", "anim/antman_actions.zip"),

    Asset("ANIM", "anim/antman_translucent_build.zip"),    
	--Asset("ANIM", "anim/antman_build.zip"),
	Asset("SOUND", "sound/pig.fsb"),
}
--Might be completely pointless here. I don't know.
local prefabs = 
{	

}
	
--Note: Becareful when adding starting items.
--People are likely to spam changecharacter to get more of them.	
local start_inv = 
{
	--'prefab',
	--"spear",
}

local start_inv2 =
{
   --"beehome",
}

if MOBHOUSE == "Enable1" or MOBHOUSE == "Enable3" then
	start_inv = start_inv2
end
-----------------------
--Stats--
local mob = 
{
	health = TUNING[string.upper(prefabname.."_HEALTH")],
	hunger = TUNING[string.upper(prefabname.."_HUNGER")],
	hungerrate = TUNING.WILSON_HUNGER_RATE, 
	sanity = TUNING[string.upper(prefabname.."_SANITY")],
	runspeed = 6,
	walkspeed = 3,
	damage = 30*2,
	range = 3,
	bank = "antman",
	build = "antman_translucent_build",
	shiny = "antman",
	--build2 = "bee_guard_puffy_build",
	scale = 1,
	stategraph = "SGantmanp",
	minimap = "antmanp.tex",
	
}
-----------------------
--Loot that drops when you die, duh.
SetSharedLootTable('antmanp',
{
    {'monstermeat',  1.00},
})

local function OnTalk(inst, script)
	inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/crickant/abandon")
end

local function onattacked(inst, data)
    inst.components.combat:SetTarget(data.attacker)
    inst.components.combat:ShareTarget(data.attacker, 30, function(dude) return dude:HasTag("ant") and not dude.components.health:IsDead() end, 30)
end

local function onhitother2(inst, other)
	if inst.isaltattacking then
		PlayablePets.SetPoison(inst, other)
	end
    inst.components.combat:SetTarget(other)
    inst.components.combat:ShareTarget(other, 30, function(dude) return dude:HasTag("ant") and not dude.components.health:IsDead() end, 30)
end
------------------------------------------------------
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
end
-------------------------------------------------------

local common_postinit = function(inst) 
	inst.MiniMapEntity:SetIcon(mob.minimap)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false)
	  --ThePlayer.HUD.controls.status.brain:Hide()
		if MONSTERHUNGER == "Disable" then
			--ThePlayer.HUD.controls.status.stomach:Hide()
		end
   end
end)
	-------------------------------------------------------
	--Tags--
	inst:AddTag("ant")
	-------------------------------------------------------
	--Worker--
	inst:AddComponent("worker")
	inst.components.worker:SetAction(ACTIONS.CHOP, 2)
	inst.components.worker:SetAction(ACTIONS.MINE, 2)
	--inst:WatchWorldState( "onphasechanged", function() SetNightVision(inst) end)
	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)

	PlayablePets.SetNightVision(inst)		
end
-------------Forge------------------------------------------
local ex_fns = require "prefabs/player_common_extensions"

local function DoAttackBuff(target)
    if target:IsValid() and not target.components.health:IsDead() and not target.buffimmune and not target.defbuffed and target.components.debuffable then
		target.components.debuffable:AddDebuff("guard_buff", "guard_buff")	
	end
end

local function DoSelfBuff(inst)
	--OnPulse(inst)
	if inst.components.health and not inst.components.health:IsDead() then
    local x, y, z = inst.Transform:GetWorldPosition()
    local ents = TheSim:FindEntities(x, y, z, 40, nil, { "playerghost", "ghost", "battlestandard", "INLIMBO" }, {"antguard"} )
		if #ents > 0 then
			DoAttackBuff(inst)
		end
	end
end

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, PPHAM_FORGE.ANTMAN)
	
	inst.mobsleep = false
	
	--inst.acidmult = 1.25
	
	inst:DoTaskInTime(0, function(inst)
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({})
	end)
	
	inst.DoAura = inst:DoPeriodicTask(TUNING.FORGE.BATTLESTANDARD.PULSE_TICK, DoSelfBuff)
	
	inst.components.combat:SetDamageType(1)
	
	inst.components.revivablecorpse.revivespeedmult = 1
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end
-----------------------------------------------------------------------

local master_postinit = function(inst)
	------------------------------------------
	--Stats--
    PlayablePets.SetCommonStats(inst, mob) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20, 1) --heat, cold, wetness
	------------------------------------------
	--Loot drops--
	inst:AddComponent("lootdropper")
    inst.components.lootdropper:SetChanceLootTable("antmanp")
	----------------------------------	
	inst.mobsleep = true
	inst.taunt = true
	inst.taunt2 = true
	
	inst.mobplayer = true
	inst.ghostbuild = "ghost_monster_build"

	inst.isshiny = 0
	
    local body_symbol = "pig_torso"
	inst.poisonsymbol = body_symbol
	MakeMediumBurnableCharacter(inst, body_symbol)
    MakeLargeFreezableCharacter(inst, body_symbol)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	PlayablePets.SetCommonStatResistances(inst, 3, 3, 3) --fire, acid, poison
	----------------------------------
	--Worker--
	inst:AddComponent("worker")
	inst.components.worker:SetAction(ACTIONS.CHOP, 2)
	inst.components.worker:SetAction(ACTIONS.MINE, 2)
	----------------------------------
	--Eater--
	--inst.components.eater:SetDiet({ FOODTYPE.MEAT, FOODTYPE.MEAT }, { FOODTYPE.MEAT, FOODTYPE.MEAT }) 
	inst.components.eater:SetCanEatHorrible() --check to make them eat monster meat.
	inst.components.eater.strongstomach = true -- can eat monster meat!
	inst.components.eater.ignoresspoilage = true -- Can eat spoiled food without consequence. Might keep food stats as if fresh.
	---------------------------------
	--Physics and Scale--
	
	inst.DynamicShadow:SetSize(1.5, 0.75)
    MakeCharacterPhysics(inst, 10, .5)
    inst.Transform:SetFourFaced()
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip)
	inst:ListenForEvent("attacked", onattacked)
	
	inst:ListenForEvent("ontalk", OnTalk)
	---------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
    ------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob, nil, true) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob, nil, true)
   		end)
	end)
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
    return inst
	
end

return MakePlayerCharacter("antmanp", prefabs, assets, common_postinit, master_postinit, start_inv)
