local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--
---------------------------

local getskins = {"8"}	

--Don't add assets unless absolutely needed.
local assets = 
{
	Asset("ANIM", "anim/rabid_beetle.zip"),
	--Skins--
	Asset("ANIM", "anim/rabid_beetle_shiny_build_08.zip"),
	Asset("ANIM", "anim/rabid_beetle_valentines.zip"),
	Asset("ANIM", "anim/polar_flea.zip"), --from "The Winterlands" mod
}
--Might be completely pointless here. I don't know.
local prefabs = 
{	

}
	
--Note: Becareful when adding starting items.
--People are likely to spam changecharacter to get more of them.	
local start_inv = 
{
	--'prefab',
	--"spear",
}

local start_inv2 =
{
   --"beehome",
}

if MOBHOUSE == "Enable1" or MOBHOUSE == "Enable3" then
	start_inv = start_inv2
end
-----------------------
--Stats--
local mob = 
{
	health = 125,
	hunger = 100,
	hungerrate = TUNING.WILSON_HUNGER_RATE, 
	sanity = 50,
	runspeed = 12,
	walkspeed = 12,
	damage = 20*2,
	range = 2,
	hit_range = 3,
	attackperiod = 0,
	bank = "rabid_beetle",
	build = "rabid_beetle",
	shiny = "rabid_beetle",
	--build2 = "bee_guard_puffy_build",
	scale = 0.6,
	stategraph = "SGrabidbeetlep",
	minimap = "rabidbeetlep.tex",
	
}

--Loot that drops when you die, duh.
SetSharedLootTable('rabidbeetlep',
{
    {'lightbulb', 1},
})


local function onattacked(inst, data)
    inst.components.combat:SetTarget(data.attacker)
    inst.components.combat:ShareTarget(data.attacker, 30, function(dude) return dude:HasTag("rabidbeetle") and not dude.components.health:IsDead() end, 30)
end

local function onhitother2(inst, other)
	if inst.isaltattacking then
		PlayablePets.SetPoison(inst, other)
	end
    inst.components.combat:SetTarget(other)
    inst.components.combat:ShareTarget(other, 30, function(dude) return dude:HasTag("rabidbeetle") and not dude.components.health:IsDead() end, 30)
end
------------------------------------------------------
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
end
-------------------------------------------------------

local common_postinit = function(inst) 
	inst.MiniMapEntity:SetIcon(mob.minimap)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false)
	  --ThePlayer.HUD.controls.status.brain:Hide()
		if MONSTERHUNGER == "Disable" then
			--ThePlayer.HUD.controls.status.stomach:Hide()
		end
   end
end)
	inst:AddTag("monster")
	inst:AddTag("rabidbeetle") 

	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)

	PlayablePets.SetNightVision(inst)	
end

-------------Forge------------------------------------------

local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, PPHAM_FORGE.RABIDBEETLE)
	inst.mobsleep = false	

	inst:DoTaskInTime(0, function(inst)
		inst:AddComponent("itemtyperestrictions")
		inst.components.itemtyperestrictions:SetRestrictions({"books", "staves", "darts"})
	end)
	inst.components.combat:SetDamageType(1)
	
	inst.components.revivablecorpse.revivespeedmult = 0.3
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end
-----------------------------------------------------------------------

local master_postinit = function(inst)
	------------------------------------------
	--Stats--
    PlayablePets.SetCommonStats(inst, mob) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20, 1) --heat, cold, wetness
	----------------------------------
	--Loot drops--
	inst:AddComponent("lootdropper")
    inst.components.lootdropper:SetChanceLootTable("rabidbeetlep")
	----------------------------------
	--Tags--
    
	--inst:AddTag("groundpoundimmune")
	
	inst.mobsleep = true
	inst.taunt = true
	inst.taunt2 = true
	
	inst.mobplayer = true
	inst.ghostbuild = "ghost_monster_build"

	inst.isshiny = 0
	
    MakeLargeBurnableCharacter(inst, "shell01")
    MakeSmallFreezableCharacter(inst, "shell01")
	inst.poisonsymbol = "shell01"

	local body_symbol = "shell01"
	inst.poisonsymbol = body_symbol
    MakeSmallFreezableCharacter(inst, body_symbol)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	MakeLargeBurnableCharacter(inst, body_symbol)	
	PlayablePets.SetCommonStatResistances(inst, 3, 3, 3)
	----------------------------------
	--Eater--

	inst.components.eater:SetDiet({ FOODTYPE.MEAT, FOODTYPE.MEAT }, { FOODTYPE.MEAT, FOODTYPE.MEAT }) 
    inst.components.eater:SetAbsorptionModifiers(1,1.75,1) --This multiplies food stats.
	inst.components.eater:SetCanEatHorrible() --check to make them eat monster meat.
	inst.components.eater.strongstomach = true -- can eat monster meat!
	--inst.components.eater.eatwholestack = true --should eat the entire stack and get all the benefits.
	inst.components.eater.ignoresspoilage = true -- Can eat spoiled food without consequence. Might keep food stats as if fresh.
	---------------------------------
	--Physics and Scale--
	inst.DynamicShadow:SetSize(2.5, 1.5)
    MakeCharacterPhysics(inst, 5, .5)
    inst.Transform:SetScale(mob.scale, mob.scale, mob.scale)
    inst.Transform:SetFourFaced()
	---------------------------------
	--Listeners--
	
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	--inst:ListenForEvent("attacked", onattacked)
	--inst.components.combat.onhitotherfn = onhitother2
	------------------------------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
	------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob) end)
    end)
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
    return inst	
end

return MakePlayerCharacter("rabidbeetlep", prefabs, assets, common_postinit, master_postinit, start_inv)
