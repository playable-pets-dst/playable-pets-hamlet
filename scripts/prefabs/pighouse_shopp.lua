require "prefabutil"
require "recipes"

local assets =
{

    Asset("ANIM", "anim/pig_shop.zip"),
    
    Asset("ANIM", "anim/pig_shop.zip"),    
    Asset("ANIM", "anim/pig_shop_florist.zip"),
    Asset("ANIM", "anim/pig_shop_hoofspa.zip"),
    Asset("ANIM", "anim/pig_shop_produce.zip"),
    Asset("ANIM", "anim/pig_shop_general.zip"),
    Asset("ANIM", "anim/pig_shop_deli.zip"),    
    Asset("ANIM", "anim/pig_shop_antiquities.zip"),       

    --Asset("ANIM", "anim/flag_post_duster_build.zip"),    
    --Asset("ANIM", "anim/flag_post_wilson_build.zip"),    

    Asset("ANIM", "anim/pig_cityhall.zip"),      
    Asset("ANIM", "anim/pig_shop_arcane.zip"),
    Asset("ANIM", "anim/pig_shop_weapons.zip"),
    Asset("ANIM", "anim/pig_shop_accademia.zip"),
    Asset("ANIM", "anim/pig_shop_millinery.zip"),
    Asset("ANIM", "anim/pig_shop_bank.zip"),           

}

local prefabs = 
{
    "pigman_royalguardp",
    "pigman_royalguard_2p",
}

local function LightsOn(inst)
    if not inst:HasTag("burnt") then
        inst.Light:Enable(true)
        inst.AnimState:PlayAnimation("lit", true)
        inst.SoundEmitter:PlaySound("dontstarve/pig/pighut_lighton")
        inst.lightson = true
        if inst.components.pigshopping then
            inst.components.pigshopping:GetNewInventory(inst.components.pigshopping.type)
            inst.components.pigshopping:SetOpen(true)
        end
    end
end

local function LightsOff(inst)
    if not inst:HasTag("burnt") then
        inst.Light:Enable(false)
        inst.AnimState:PlayAnimation("idle", true)
        inst.SoundEmitter:PlaySound("dontstarve/pig/pighut_lightoff")
        inst.lightson = false
        if inst.components.pigshopping then
            inst.components.pigshopping:SetOpen(false)
        end
    end
end

local function OnDayChange(inst)
    inst:DoTaskInTime(0.5, function(inst)
        if (TheWorld.state.isday or TheWorld.state.isdusk or TheWorld:HasTag("cave")) and not inst.lightson then
            inst:DoTaskInTime(1 + (5*math.random()), LightsOn)
        elseif TheWorld.state.isnight and not inst.latehours then
            inst:DoTaskInTime(1 + (5*math.random()), LightsOff)
        end
    end)    
end

local function getstatus(inst)
    if inst:HasTag("burnt") then
        return "BURNT"
    elseif inst.components.spawner and inst.components.spawner:IsOccupied() then
        if inst.lightson then
            return "FULL"
        else
            return "LIGHTSOUT"
        end
    end
end
        
local function onhammered(inst, worker)
    if inst:HasTag("fire") and inst.components.burnable then
        inst.components.burnable:Extinguish()
    end

    inst.reconstruction_project_spawn_state = {
        bank = "pig_house",
        build = "pig_house",
        anim = "unbuilt",
    }

    if not inst.components.fixable and inst.components.lootdropper then
        inst.components.lootdropper:DropLoot()
    end

    if inst.doortask then
        inst.doortask:Cancel()
        inst.doortask = nil
    end
	if inst.components.spawner then inst.components.spawner:ReleaseChild() end

	SpawnPrefab("collapse_big").Transform:SetPosition(inst.Transform:GetWorldPosition())
	inst.SoundEmitter:PlaySound("dontstarve/common/destroy_wood")
	inst:Remove()
end

local function ongusthammerfn(inst)
    onhammered(inst, nil)
end

local function onhit(inst, worker)
    if not inst:HasTag("burnt") then
    	inst.AnimState:PlayAnimation("hit")
    	inst.AnimState:PushAnimation(inst.lightson and "lit" or"idle")
    end
	
end

local function OnDay(inst)
    ----print(inst, "OnDay")
    if not inst:HasTag("burnt") then
        LightsOn(inst)
    end
end

local function OnDusk(inst)
    ----print(inst, "OnDay")
    if not inst:HasTag("burnt") and not inst.latehours then
        LightsOff(inst)
    end
end

local function citypossessionfn(inst)      
        if inst:HasTag("palacetower") then
            inst.AnimState:AddOverrideBuild("flag_post_royal_build")
            local spawned = {"pigman_royalguard_2p"}
            inst.components.spawner:Configure( spawned[math.random(1,#spawned)], 480,1)              
        elseif inst.id == 2 then
            inst.AnimState:AddOverrideBuild("flag_post_perdy_build")
            local spawned = {"pigman_royalguard_2p"}
            inst.components.spawner:Configure( spawned[math.random(1,#spawned)], 480,1)            
        elseif inst.id == 1 then
            inst.AnimState:AddOverrideBuild("flag_post_duster_build")
            local spawned = {"pigman_royalguardp"}
            inst.components.spawner:Configure( spawned[math.random(1,#spawned)], 480,1)
		else
			inst.AnimState:AddOverrideBuild("flag_post_wilson_build")
			local spawned = {"pigman_royalguardp"}
			inst.components.spawner:Configure( spawned[math.random(1,#spawned)], 480,1) 
        end    
end

local function onbuilt(inst)
	inst._isbuilt = true
	inst.AnimState:PlayAnimation("place")
	inst.AnimState:PushAnimation("idle")
    --citypossessionfn( inst )
end

local function onsave(inst, data)
	data._isbuilt = inst._isbuilt or nil
	data.indestructable = inst.indestructable or nil
	data.id = inst.id or nil
	data.latehours = inst.latehours or nil
end

local function onload(inst, data)
	if data then
		inst.id = data.id and data.id or nil
		inst.latehours = data.latehours or nil
		inst._isbuilt = data._isbuilt or nil
		inst.indestructable = data.indestructable or nil
	end
end

local function OnEntityWake(inst)
    if TheWorld.state.isnight and not inst.laterhours and inst.lightson then
        LightsOff(inst)
	elseif not TheWorld.state.isnight and not inst.lightson then
		LightsOn(inst)		
    end
end

local function BankAcceptCheck(inst, item, giver)
    if (item:HasTag("ppcurrency") and giver.components.ppcurrency and giver.components.ppcurrency.current ~= giver.components.ppcurrency.max) then
        local value = item.components.stackable and (item.currencyvalue*item.components.stackable:StackSize()) or item.currencyvalue
        item:Remove()
        if giver.components.ppcurrency then
            giver.components.ppcurrency:DoDelta(value)
        end
        return false
    end
    return false
end

local function BankOnAccept(inst, giver, item)
    if item and item.currencyvalue then
        local value = item.components.stackable and (item.currencyvalue*item.components.stackable:StackSize()) or item.currencyvalue
        item:Remove()
        if giver.components.ppcurrency then
            giver.components.ppcurrency:DoDelta(value)
        end
    end
end

local function makefn(name,build, bank, data)

    local function fn(Sim)
        local inst = CreateEntity()
        local trans = inst.entity:AddTransform()
        local anim = inst.entity:AddAnimState()
        local light = inst.entity:AddLight()
        inst.entity:AddSoundEmitter()
		inst.entity:AddNetwork()

        local minimap = inst.entity:AddMiniMapEntity()
        minimap:SetIcon( name .. ".tex" )

        light:SetFalloff(1)
        light:SetIntensity(.5)
        light:SetRadius(1)
        light:Enable(false)
        light:SetColour(180/255, 195/255, 50/255)
		
		anim:SetBuild(build)
        anim:PlayAnimation("idle", true)
        anim:Hide("YOTP")
        anim:Hide("YOTP")

        inst:AddTag(name)

        inst:AddTag("structure")
        inst:AddTag("city_hammerable")
        if data.isbank then
            inst:AddTag("currencybank")
        else
            inst:AddTag("pig_shop")
        end
        
        MakeObstaclePhysics(inst, 1.25)

        if bank then
            anim:SetBank(bank)
        else
            anim:SetBank("pig_shop")
        end
		
		inst.entity:SetPristine()
	
		if not TheWorld.ismastersim then
			return inst
		end
	
        if data.isbank then
            inst.isbank = true
            
            inst:AddComponent("trader")
            inst.components.trader:SetAbleToAcceptTest(BankAcceptCheck)
            inst.components.trader:SetAcceptTest(BankAcceptCheck)
            inst.components.trader.onaccept = BankOnAccept
        else
            inst:AddComponent("pigshopping")
            inst.components.pigshopping:SetType(data.type or "GENERAL")
            inst.components.pigshopping:Initialize()
        end
        
        --inst:AddComponent("lootdropper")
		
		if data and data.latehours then
			inst.latehours = true
		end
			
        inst:WatchWorldState("isnight", function() OnDayChange(inst) end)    
        inst:WatchWorldState("isdusk", function() OnDayChange(inst) end) 
		inst:WatchWorldState("isday", function() OnDayChange(inst) end)
		OnDayChange(inst)
  
        inst:AddComponent("inspectable")    

		--inst:AddComponent("pigshopping")
		
        --inst.components.inspectable.getstatus = getstatus
        
        MakeSnowCovered(inst, .01)

        inst:ListenForEvent("burntup", function(inst)
            if inst.doortask then
                inst.doortask:Cancel()
                inst.doortask = nil
            end
            --inst:Remove()
        end)
        inst:ListenForEvent("onignite", function(inst, data)
            if inst.components.spawner then
                inst.components.spawner:ReleaseChild()
            end
        end)

        inst.OnSave = onsave 
        inst.OnLoad = onload
		inst.OnEntityWake = OnEntityWake

        inst:ListenForEvent( "onbuilt", onbuilt)
		
		inst:DoTaskInTime(0, function(inst)
			if not (data and data.indestructable) and not inst.indestructable and inst._isbuilt then
				if not data or not data.unburnable then
					MakeMediumBurnable(inst, nil, nil, true)
					MakeLargePropagator(inst)
				end			
				inst:AddComponent("workable")
				inst.components.workable:SetWorkAction(ACTIONS.HAMMER)
				inst.components.workable:SetWorkLeft(4)
				inst.components.workable:SetOnFinishCallback(onhammered)
				inst.components.workable:SetOnWorkCallback(onhit)			
			end
		end)
		
		
        if TheWorld.state.isday or TheWorld.state.isdusk then 
            OnDay(inst)
        end 

        if data and data.sounds then
            --inst.usesounds = data.sounds
        end

        return inst
    end
    return fn
end

local function makeshop(name, build, bank, data)   
    return Prefab("common/objects/" .. name, makefn(name, build, bank, data), assets, prefabs )
end

local function placetestfn(inst)
    inst.AnimState:Hide("YOTP")
    inst.AnimState:Hide("SNOW")
    return true
end

return makeshop("pig_shop_delip",        "pig_shop_deli", nil, {type = "DELI"}),
       makeshop("pig_shop_generalp",     "pig_shop_general",     nil,  {type = "GENERAL"} ),
       makeshop("pig_shop_hoofspap",     "pig_shop_hoofspa",     nil,    {type = "SPA"}),        
       makeshop("pig_shop_producep",     "pig_shop_produce",     nil,   {type = "PRODUCE"}),
       makeshop("pig_shop_floristp",     "pig_shop_florist",     nil,    {type = "FLORIST"}),
       makeshop("pig_shop_antiquitiesp", "pig_shop_antiquities", nil,    {type = "ODDITIES"}), 

	   --makeshop("pig_shop_academyp",     "pig_shop_accademia",   nil,  nil ),
       makeshop("pig_shop_arcanep",      "pig_shop_arcane",      nil,    {type = "ARCANE"} ),
       makeshop("pig_shop_weaponsp",     "pig_shop_weapons",     nil,    {type = "WEAPONS"}),
       makeshop("pig_shop_hatshopp",     "pig_shop_millinery",   nil,    {type = "HAT"}),

       makeshop("pig_shop_bankp",        "pig_shop_bank",        nil,    {isbank = true, latehours = true} ),
       --Prefab("common/objects/pig_shop_spawner", makespawnerfn, assets, spawnprefabs ),


       MakePlacer("common/pig_shop_delip_placer", "pig_shop", "pig_shop_deli", "idle", false, false, true, nil, nil, nil, nil, nil, nil, placetestfn),
       MakePlacer("common/pig_shop_generalp_placer", "pig_shop", "pig_shop_general", "idle", false, false, true, nil, nil, nil, nil, nil, nil, placetestfn),
       MakePlacer("common/pig_shop_hoofspap_placer", "pig_shop", "pig_shop_hoofspa", "idle", false, false, true, nil, nil, nil, nil, nil, nil, placetestfn),
       MakePlacer("common/pig_shop_producep_placer", "pig_shop", "pig_shop_produce", "idle", false, false, true, nil, nil, nil, nil, nil, nil, placetestfn),
       MakePlacer("common/pig_shop_floristp_placer", "pig_shop", "pig_shop_florist", "idle", false, false, true, nil, nil, nil, nil, nil, nil, placetestfn),
       MakePlacer("common/pig_shop_antiquitiesp_placer", "pig_shop", "pig_shop_antiquities", "idle", false, false, true, nil, nil, nil, nil, nil, nil, placetestfn),
       MakePlacer("common/pig_shop_arcanep_placer", "pig_shop", "pig_shop_arcane", "idle", false, false, true, nil, nil, nil, nil, nil, nil, placetestfn),
       MakePlacer("common/pig_shop_weaponsp_placer", "pig_shop", "pig_shop_weapons", "idle", false, false, true, nil, nil, nil, nil, nil, nil, placetestfn),
       MakePlacer("common/pig_shop_hatshopp_placer", "pig_shop", "pig_shop_millinery", "idle", false, false, true, nil, nil, nil, nil, nil, nil, placetestfn),
       MakePlacer("common/pig_shop_city_player_cityhall_placer", "pig_cityhall", "pig_cityhall", "idle", false, false, true, nil, nil, nil, nil, nil, nil, placetestfn),
       MakePlacer("common/pig_shop_bankp_placer", "pig_shop", "pig_shop_bank", "idle", false, false, true, nil, nil, nil, nil, nil, nil, placetestfn)
