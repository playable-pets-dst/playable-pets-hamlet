local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--
---------------------------

--Don't add assets unless absolutely needed.
local assets = 
{
	Asset("ANIM", "anim/giant_grub.zip")
}
--Might be completely pointless here. I don't know.
local prefabs = 
{	

}
	
--Note: Becareful when adding starting items.
--People are likely to spam changecharacter to get more of them.	
local start_inv = 
{
	--'prefab',
	--"spear",
}

local start_inv2 =
{
   --"beehome",
}

if MOBHOUSE == "Enable1" or MOBHOUSE == "Enable3" then
	start_inv = start_inv2
end
-----------------------
--Stats--
local mob = 
{
	health = 1800,
	hunger = 300,
	hungerrate = TUNING.WILSON_HUNGER_RATE, 
	sanity = 100,
	runspeed = 2.5,
	walkspeed = 2.5,
	damage = 44*2,
	range = 3,
	bank = "giant_grub",
	build = "giant_grub",
	shiny = "giant_grub",
	--build2 = "bee_guard_puffy_build",
	scale = 3,
	stategraph = "SGgiantgrubp",
	minimap = "giantgrubp.tex",
	
}
-----------------------
--Loot that drops when you die, duh.
SetSharedLootTable('giantgrubp',
{
    {'monstermeat',  1.00},
	{'monstermeat',  1.00},
	{'monstermeat',  1.00},
})

local function GetExcludeTags(inst)
	if TheNet:GetPVPEnabled() then
		return {"lightningimmune", "lightningrod", "notarget"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion", "lightningimmune", "lightningrod", "notarget"}
	else	
		return {"player", "companion", "lightningimmune", "lightningrod", "notarget"}
	end
end

local function CanBeAttacked(inst, attacker)
	return inst.State == "above"
end
------------------------------------------------------
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
end
-------------------------------------------------------

local common_postinit = function(inst) 
	inst.MiniMapEntity:SetIcon(mob.minimap)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false)
	  --ThePlayer.HUD.controls.status.brain:Hide()
		if MONSTERHUNGER == "Disable" then
			--ThePlayer.HUD.controls.status.stomach:Hide()
		end
   end
end)
	--inst:WatchWorldState( "onphasechanged", function() SetNightVision(inst) end)
	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst)
	
end

local function DontTriggerCreep(inst)
    inst.components.locomotor.runspeed = (mob.runspeed)
	inst.components.locomotor.walkspeed = (mob.walkspeed)
end

--CONSUME THY ENEMIES
local function OnKill(inst, data) 
	if data.victim and data.victim:HasTag("mant") and not (data.victim:HasTag("epic") or data.victim:HasTag("largecreature")) then
		--mants are my favorite.
		inst.components.health:DoDelta(100, false)
		inst.components.hunger:DoDelta(data.victim and data.victim:HasTag("smallcreature") and 30 or 75, false)
	elseif data.victim ~= nil and not data.victim:HasTag("structure") and not data.victim:HasTag("ghost") and not (data.victim:HasTag("epic") or data.victim:HasTag("largecreature")) then
		inst.components.health:DoDelta(inst.components.revivablecorpse and 50 or 20, false)	
		inst.components.hunger:DoDelta(data.victim and data.victim:HasTag("smallcreature") and 10 or 40, false)
	end
end

-------------Forge------------------------------------------
local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	inst.components.health:SetMaxHealth(PPHAM_FORGE.GIANTGRUB.HEALTH)
	
	inst.components.combat:SetDefaultDamage(PPHAM_FORGE.GIANTGRUB.DAMAGE)
	inst.components.combat:SetAttackPeriod(PPHAM_FORGE.GIANTGRUB.ATTACKPERIOD)
	
	inst.components.locomotor.walkspeed = PPHAM_FORGE.GIANTGRUB.WALKSPEED
    inst.components.locomotor.runspeed = PPHAM_FORGE.GIANTGRUB.RUNSPEED
	
	inst.components.combat:SetRange(PPHAM_FORGE.GIANTGRUB.ATTACK_RANGE, PPHAM_FORGE.GIANTGRUB.HIT_RANGE)
	--inst.components.combat:SetAreaDamage(PPHAM_FORGE.TREEGUARD.AOE_RANGE, PPHAM_FORGE.TREEGUARD.AOE_DMGMULT)
	
	inst.mobsleep = false
	
	--inst.acidmult = 1.25
	
	inst:DoTaskInTime(0, function(inst)
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"books", "staves", "darts"})
	end)
	
	--inst.components.combat.onhitotherfn = OnForgeHitOther
	
	inst.components.combat:SetDamageType(1)
	
	inst.components.revivablecorpse.revivespeedmult = 1
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

local function SetState(inst, state)
	--"under" or "above"
    inst.State = string.lower(state)
    if inst.State == "under" then
		if not inst.components.revivablecorpse then
			inst:AddTag("notarget")
		else
			--instead of being immune to targeting, lets just take less damage
			inst.components.health:SetAbsorptionAmount(0.20)
		end
        inst.Physics:ClearCollisionMask()
		inst.Physics:CollidesWith(COLLISION.WORLD)
		inst.DynamicShadow:Enable(false)
    elseif inst.State == "above" then
		inst:RemoveTag("notarget")
        ChangeToCharacterPhysics(inst)
    end
end

local function IsState(inst, state)
    return inst.State == string.lower(state)
end
-----------------------------------------------------------------------

local master_postinit = function(inst)
	------------------------------------------
	--Stats--
	PlayablePets.SetCommonStats(inst, mob, nil, true) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20, 1) --heat, cold, wetness
	PlayablePets.SetStormImmunity(inst)	
	----------------------------------
	--Loot drops--
	PlayablePets.SetCommonLootdropper(inst, "giantgrubp") --inst, prefaboverride
	----------------------------------
	--Tags--
    inst:AddTag("monster")
    inst:AddTag("giantgrub")
	inst:AddTag("largecreature")
	
	inst.mobsleep = true
	--inst.taunt = true
	
	inst.mobplayer = true
	inst.ghostbuild = "ghost_monster_build"

	inst.isshiny = 0

	local body_symbol = "chest"
	inst.poisonsymbol = body_symbol
	MakeLargeBurnableCharacter(inst, body_symbol)
    MakeLargeFreezableCharacter(inst)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	PlayablePets.SetCommonStatResistances(inst, nil, 1.5, 3) --fire, acid, poison	
	
	SetState(inst, "under")
    inst.SetState = SetState
    inst.IsState = IsState
	----------------------------------
	inst:AddComponent("groundpounder")
  	--inst.components.groundpounder.destroyer = true
	inst.components.groundpounder.damageRings = 0
	inst.components.groundpounder.destructionRings = 0
	inst.components.groundpounder.numRings = 2
	----------------------------------
	--Eater--

	--inst.components.eater:SetDiet({ FOODTYPE.MEAT, FOODTYPE.MEAT }, { FOODTYPE.MEAT, FOODTYPE.MEAT }) 
    inst.components.eater:SetAbsorptionModifiers(1,0.8,1) --I'd much more prefer to eat the living.
	inst.components.eater:SetCanEatHorrible() --check to make them eat monster meat.
	inst.components.eater.strongstomach = true -- can eat monster meat!
	inst.components.eater.eatwholestack = true --should eat the entire stack and get all the benefits.
	inst.components.eater.ignoresspoilage = true -- Can eat spoiled food without consequence. Might keep food stats as if fresh.
	---------------------------------
	--Physics and Scale--
	inst.DynamicShadow:SetSize(1, 0.75)
    MakeCharacterPhysics(inst, 1, .5)
	inst.Physics:SetMass(9999)
    inst.Transform:SetFourFaced()
	---------------------------------
	--Listeners--
	
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	inst:ListenForEvent("killed", OnKill)
	---------------------------------
	--Functions that saves and loads data.

    inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
    ------------------------------------------------------
	--Light and Character Functions. Don't Touch.--	
	inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob, nil, true, true) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob, nil, true, true) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob, nil, true, true) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman, amphibious)
    end)
	
    return inst
	
end
--[[
if SKIN_RARITY_COLORS.ModMade ~= nil then
AddModCharacterSkin("beeguardp", 1, {normal_skin = "bee_guard_shiny_build_01", ghost_skin = "ghost_monster_build"}, {"bearger_shiny_build_01", "ghost_monster_build"}, {"BEARPLAYER", "FORMAL"})
end]]

return MakePlayerCharacter("giantgrubp", prefabs, assets, common_postinit, master_postinit, start_inv)
