local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--
---------------------------

--Don't add assets unless absolutely needed.
local assets = 
{
	Asset("ANIM", "anim/hippo_basic.zip"),
    Asset("ANIM", "anim/hippo_attacks.zip"),    
    Asset("ANIM", "anim/hippo_water.zip"),
    Asset("ANIM", "anim/hippo_water_attacks.zip"),    
	Asset("ANIM", "anim/hippo_build.zip"),
	Asset("ANIM", "anim/hippo_badlands.zip"),
	Asset("SOUND", "sound/perd.fsb"),
}
--Might be completely pointless here. I don't know.
local prefabs = 
{	
	"groundpound_fx",
	"groundpoundring_fx",
}
	
--Note: Becareful when adding starting items.
--People are likely to spam changecharacter to get more of them.	
local start_inv = 
{
	--'prefab',
	--"spear",
}

local start_inv2 =
{
   --"beehome",
}

if MOBHOUSE == "Enable1" or MOBHOUSE == "Enable3" then
	start_inv = start_inv2
end
-----------------------
--Stats--
local mob = 
{
	health = TUNING.HIPPOP_HEALTH,
	hunger = TUNING.HIPPOP_HUNGER,
	hungerrate = TUNING.WILSON_HUNGER_RATE, 
	sanity = TUNING.HIPPOP_SANITY,
	runspeed = 6,
	walkspeed = 5,
	damage = 50*2,
	range = 6,
	hit_range = 3,
	attackperiod = 3,
	bank = "hippo",
	build = "hippo_build",
	shiny = "hippo",
	scale = 1,
	stategraph = "SGhippop",
	minimap = "hippop.tex",
	
}
-----------------------
local sounds = 
{
    emerge = "ia/creatures/seacreature_movement/water_emerge_med",
    submerge = "ia/creatures/seacreature_movement/water_submerge_med",
}

--Loot that drops when you die, duh.
SetSharedLootTable('hippop',
{
    {'meat',  1.00},
	{'meat',  1.00},
	{'meat',  1.00},
	{'meat',  1.00},
})

local function GetExcludeTags(inst)
	if TheNet:GetPVPEnabled() then
		return {"lightningimmune", "lightningrod", "notarget"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion", "lightningimmune", "lightningrod", "notarget"}
	else	
		return {"player", "companion", "lightningimmune", "lightningrod", "notarget"}
	end
end

------------------------------------------------------
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
end
-------------------------------------------------------

local common_postinit = function(inst) 
	inst.MiniMapEntity:SetIcon(mob.minimap)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false)
	  --ThePlayer.HUD.controls.status.brain:Hide()
		if MONSTERHUNGER == "Disable" then
			--ThePlayer.HUD.controls.status.stomach:Hide()
		end
   end
end)
	--Tags--
    inst:AddTag("animal")
    inst:AddTag("hippopotamoose") 
    inst:AddTag("wavemaker")        
    inst:AddTag("groundpoundimmune")
	inst:AddTag("amphibious")
	
	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst)	
end


-------------Forge------------------------------------------
local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	inst.components.health:SetMaxHealth(PPHAM_FORGE.HIPPO.HEALTH)
	
	inst.components.combat:SetDefaultDamage(PPHAM_FORGE.HIPPO.DAMAGE)
	inst.components.combat:SetAttackPeriod(PPHAM_FORGE.HIPPO.ATTACKPERIOD)
	
	inst.components.locomotor.walkspeed = PPHAM_FORGE.HIPPO.WALKSPEED
    inst.components.locomotor.runspeed = PPHAM_FORGE.HIPPO.RUNSPEED
	
	inst.components.combat:SetRange(PPHAM_FORGE.HIPPO.ATTACK_RANGE, PPHAM_FORGE.HIPPO.HIT_RANGE)
	--inst.components.combat:SetAreaDamage(PPHAM_FORGE.HIPPO.AOE_RANGE, PPHAM_FORGE.HIPPO.AOE_DMGMULT)
	
	inst.mobsleep = false
	
	--inst.acidmult = 1.25
	
	inst:DoTaskInTime(0, function(inst)
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"books", "staves", "darts", "melees"})
	end)
	
	--inst.components.combat.onhitotherfn = OnForgeHitOther
	
	inst.components.combat:SetDamageType(1)
	
	inst.components.revivablecorpse.revivespeedmult = 1.5
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

local function OnWaterChange(inst, onwater)
    if onwater then
        inst.onwater = true
        inst.sg:GoToState("submerge")
        inst.DynamicShadow:Enable(false)
    --        inst.components.locomotor.walkspeed = 3
    else
        inst.onwater = false        
        inst.sg:GoToState("emerge")
        inst.DynamicShadow:Enable(true)
    --        inst.components.locomotor.walkspeed = 4
    end
end
-----------------------------------------------------------------------

local master_postinit = function(inst)
	------------------------------------------
	--Stats--
    PlayablePets.SetCommonStats(inst, mob) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20) --heat, cold, wetness
	----------------------------------
	--Loot drops--
	inst:AddComponent("lootdropper")
    inst.components.lootdropper:SetChanceLootTable("hippop")
	----------------------------------
	
	--inst:AddTag("groundpoundimmune")
	
	inst.mobsleep = true
	inst.taunt = true
	inst.taunt2 = true
	
	inst.mobplayer = true
	inst.ghostbuild = "ghost_monster_build"

	inst.isshiny = 0
	inst.sounds = sounds

	local body_symbol = "swap_fire"
	inst.poisonsymbol = body_symbol
	MakeSmallBurnableCharacter(inst, body_symbol)
    MakeSmallFreezableCharacter(inst, body_symbol)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	PlayablePets.SetCommonStatResistances(inst, 0) --fire, acid, poison
	----------------------------------
	--Eater--

    inst.components.eater:SetAbsorptionModifiers(1,1,1) --This multiplies food stats.
	inst.components.eater:SetCanEatHorrible() --check to make them eat monster meat.
	inst.components.eater.strongstomach = true -- can eat monster meat!
	inst.components.eater.ignoresspoilage = true -- Can eat spoiled food without consequence. Might keep food stats as if fresh.
	---------------------------------
	--Physics and Scale--
    MakeCharacterPhysics(inst, 50, 1.5)
	inst.DynamicShadow:SetSize(3, 1.25)
    inst.Transform:SetFourFaced()
	
	PlayablePets.SetAmphibious(inst, "hippo", "hippo_water", nil, nil, true)
	---------------------------------
	--Listeners--
	
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	---------------------------------
	--Functions that saves and loads data.

    inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
	
	inst:AddComponent("groundpounder")
    inst.components.groundpounder.destroyer = false
    inst.components.groundpounder.damageRings = 0
    inst.components.groundpounder.destructionRings = 0
    inst.components.groundpounder.numRings = 2
    ------------------------------------------------------

    inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob) end)
    end)
	
    return inst
	
end

return MakePlayerCharacter("hippop", prefabs, assets, common_postinit, master_postinit, start_inv)
