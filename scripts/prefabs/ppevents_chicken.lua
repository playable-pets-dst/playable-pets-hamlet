local assets =
{
    Asset("ANIM", "anim/chicken.zip"),
	Asset("ANIM", "anim/chicken_altar.zip"),
	Asset("ANIM", "anim/golden_egg_build.zip"),
}

local prefabs =
{

}

SetSharedLootTable( 'chickenp',
{
    {'drumstick',  1.00},
})

local function SpawnGoldenEgg(inst)
	local ents = {}
	
end

local function fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddDynamicShadow()
    inst.entity:AddNetwork()

    inst.DynamicShadow:SetSize(1, .75)
    inst.Transform:SetFourFaced()
	MakeCharacterPhysics(inst, 1, 0.12)

    inst.AnimState:SetBank("chicken")
    inst.AnimState:SetBuild("chicken")
    inst.AnimState:PlayAnimation("idle", true)

    inst:AddTag("animal")
	inst:AddTag("prey")
	inst:AddTag("chicken")
	inst:AddTag("smallcreature")

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

	inst:AddComponent("health")
	inst.components.health:SetMaxHealth(150)
	
	inst:AddComponent("combat")
	inst.components.combat:SetDefaultDamage(10)
	
	

    return inst
end

local function spawner_fn()
	local inst = CreateEntity()

	inst.entity:AddTransform()
    --[[Non-networked entity]]

    inst:AddTag("CLASSIFIED")
	
	inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end
	
	
	
	return inst
end

local function egg_fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddNetwork()

	MakeInventoryPhysics(inst)

    inst.AnimState:SetBank("golden_egg")
    inst.AnimState:SetBuild("golden_egg_build")
    inst.AnimState:PlayAnimation("idle", true)
	
	MakeInventoryFloatable(inst)

    inst:AddTag("goldenegg")

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

	inst:AddComponent("inspectable")

    inst:AddComponent("inventoryitem")
    inst.components.inventoryitem.atlasname = "images/inventoryimages/golden_egg.xml"
	
	inst:AddComponent("edible")
    inst.components.edible.foodtype = GLOBAL.FOODTYPE.MEAT
    inst.components.edible.hungervalue = 99999
    inst.components.edible.healthvalue = 99999
	
	inst:AddComponent("perishable")
    inst.components.perishable:SetPerishTime(TUNING.PERISH_FAST)
    inst.components.perishable:StartPerishing()
    inst.components.perishable.onperishreplacement = "spoiled_food"
    
    inst:AddComponent("tradable")

    return inst
end

local function altar_fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
	inst.entity:AddSoundEmitter()
    inst.entity:AddNetwork()

	MakeObstaclePhysics(inst, 1)

    inst.AnimState:SetBank("chicken_altar")
    inst.AnimState:SetBuild("chicken_altar")
    inst.AnimState:PlayAnimation("idle", true)

    inst:AddTag("chicken_altar")

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

	inst.Transform:SetScale(1.5, 1.5, 1.5)
	
	inst:AddComponent("inspectable")
	
	inst.AnimState:Hide("swap_object")

    return inst
end

return Prefab("chicken_npc", fn, assets, prefabs),
Prefab("chicken_npc_spawner", spawner_fn, assets, prefabs),
Prefab("chicken_altar", altar_fn, assets, prefabs),
Prefab("golden_egg", egg_fn, assets, prefabs)
