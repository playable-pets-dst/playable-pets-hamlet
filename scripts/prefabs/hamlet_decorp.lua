require "prefabutil"
require "recipes"

local assets =
{
	Asset("ANIM", "anim/python_fountain.zip"),
	Asset("ANIM", "anim/rugs.zip"),
	Asset("ANIM", "anim/ruins_artichoke.zip"),
	Asset("ANIM", "anim/ruins_giant_head.zip"),
	Asset("ANIM", "anim/statue_pig_ruins_pig.zip"),
	Asset("ANIM", "anim/statue_pig_ruins_ant.zip"),
	Asset("ANIM", "anim/statue_pig_ruins_idol.zip"),
	Asset("ANIM", "anim/statue_pig_ruins_plaque.zip"),
}

local function onhammeredcommon(inst, worker)
	if inst.components.sleepingbag then
		inst.components.sleepingbag:DoWakeUp()
	end	
    if inst.components.burnable ~= nil and inst.components.burnable:IsBurning() then
        inst.components.burnable:Extinguish()
    end
    inst:RemoveComponent("childspawner")
	if inst.components.lootdropper then
		inst.components.lootdropper:DropLoot()
	end	
    local fx = SpawnPrefab("collapse_big")
    fx.Transform:SetPosition(inst.Transform:GetWorldPosition())
    fx:SetMaterial("wood")
    inst:Remove()
end

local function onhitcommon(inst, worker)
    if not inst:HasTag("burnt") then
        if inst.components.childspawner ~= nil then
            inst.components.childspawner:ReleaseAllChildren(worker)
        end
        inst.AnimState:PlayAnimation("idle")
    end
end

local function placetestfn(inst)    
    inst.AnimState:Hide("SNOW")
    return true
end

local function MakeDecor(data)
    local function fn(Sim)
        local inst = CreateEntity()
        inst.entity:AddTransform()
        inst.entity:AddAnimState()
		if data.light then
			inst.entity:AddLight()
		end	
        inst.entity:AddSoundEmitter()
		inst.entity:AddNetwork()
	
		if data.physics_type and data.physics_type == "obstacle" then
			MakeObstaclePhysics(inst, data.physics_radius)
		else
			MakeInventoryPhysics(inst)
		end	
		
		if data.face and data.face == 8 then
			inst.Transform:SetEightFaced()
		elseif data.face and data.face == 6 then
			inst.Transform:SetSixFaced()
		elseif data.face and data.face == 4 then
			inst.Transform:SetFourFaced()
		elseif data.face and data.face == 2 then
			inst.Transform:SetTwoFaced()
		end
		
		inst.AnimState:SetBank(data.bank)
		inst.AnimState:SetBuild(data.build)
		inst.AnimState:PlayAnimation(data.anim, data.animbool or false)
		
		if data.hide_symbols then
			for i, v in ipairs(data.hide_symbols) do
				inst.AnimState:Hide(v)
			end
		end
		
		if data.sound then
			inst.SoundEmitter:PlaySound(data.sound, data.soundname or nil)
		end
		
		if data.tags then
			for i, v in ipairs(data.tags) do
				inst:AddTag(v)
			end
		end
		
		if data.nameoverride then
			inst.nameoverride = data.nameoverride
		end
		
		if data.common_postinit then
			data.common_postinit(inst)
		end
		
		inst.entity:SetPristine()
	
		if not TheWorld.ismastersim then
			return inst
		end
		
		if data.gridnudger then
			inst:AddComponent("gridnudgerp")
		end
		
		if not data.noinspect then
			inst:AddComponent("inspectable")
		end
		
		if data.loot then
			inst:AddComponent("lootdropper")
			inst.components.lootdropper:SetLoot(data.loot)
		end
		
		if data.onbuilt then
			inst:ListenForEvent("onbuilt", data.onbuilt)
		end
		
		if data.workable then
			inst:AddComponent("workable")
			inst.components.workable:SetWorkAction(data.worktype or ACTIONS.HAMMER)
			inst.components.workable:SetWorkLeft(data.works or 3)
			inst.components.workable:SetOnFinishCallback(data.onhammered or onhammeredcommon)
			inst.components.workable:SetOnWorkCallback(data.onhit or onhitcommon)
		end
		
		if data.master_postinit then
			data.master_postinit(inst)
		end

        return inst
    end

	return Prefab(data.name.."p", fn, assets)

end

local prefs = {}

local decors = require("ppham_decor")
for k, v in pairs(decors) do
    table.insert(prefs, MakeDecor(v))
end

return unpack(prefs)
--MakePlacer("wall_hedge1_itemp_placer", "hedge", "hedge1_build", "growth1", nil, nil, true, nil, nil, "eight"),
--MakePlacer("wall_hedge2_itemp_placer", "hedge", "hedge2_build", "growth1", nil, nil, true, nil, nil, "eight"),
--MakePlacer("wall_hedge3_itemp_placer", "hedge", "hedge3_build", "growth1", nil, nil, true, nil, nil, "eight"),
--MakePlacer("lawndecor1p_placer", "topiary", "topiary01_build", "idle", nil, nil, nil, nil, nil, nil, nil, nil, placetestfn),
--MakePlacer("lawndecor2p_placer", "topiary", "topiary02_build", "idle", nil, nil, nil, nil, nil, nil, nil, nil, placetestfn),
--MakePlacer("lawndecor3p_placer", "topiary", "topiary03_build", "idle", nil, nil, nil, nil, nil, nil, nil, nil, placetestfn),
--MakePlacer("lawndecor4p_placer", "topiary", "topiary04_build", "idle", nil, nil, nil, nil, nil, nil, nil, nil, placetestfn),
--MakePlacer("lawndecor5p_placer", "topiary", "topiary05_build", "idle", nil, nil, nil, nil, nil, nil, nil, nil, placetestfn),
--MakePlacer("lawndecor6p_placer", "topiary", "topiary06_build", "idle", nil, nil, nil, nil, nil, nil, nil, nil, placetestfn),
--MakePlacer("lawndecor7p_placer", "topiary", "topiary07_build", "idle", nil, nil, nil, nil, nil, nil, nil, nil, placetestfn)
	
	
	
	
	


