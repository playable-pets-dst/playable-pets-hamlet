local CANOPY_SHADOW_DATA = require("prefabs/canopyshadows")

local prefabs = 
{
}

local assets =
{
    Asset("ANIM", "anim/pillar_tree.zip"),
	Asset("ANIM", "anim/tree_shadowp.zip"),
	Asset("ANIM", "anim/vines_rainforest_border.zip"),  
	Asset("ANIM", "anim/fern_plant.zip"),
    Asset("ANIM", "anim/fern2_plant.zip"),
	Asset("ANIM", "anim/lightrays.zip"),
	Asset("ANIM", "anim/mist_fx.zip"),
	Asset("ANIM", "anim/mist_fxp.zip"),
	Asset("ANIM", "anim/lamp_post2.zip"),
    Asset("ANIM", "anim/lamp_post2_city_build.zip"),    
    Asset("ANIM", "anim/lamp_post2_yotp_build.zip"),
}
---------------------------------------
--			Tree Pillar			     --
---------------------------------------
local MIN_SHADE = TUNING.SHADE_CANOPY_RANGE
local MAX_SHADE = MIN_SHADE + TUNING.WATERTREE_PILLAR_CANOPY_BUFFER

local DROP_ITEMS_DIST_MIN = 8
local DROP_ITEMS_DIST_VARIANCE = 12

local NUM_OCEANTREENUTS_MAX = 2
local START_NUM_OCEANTREENUTS = 1

local NUM_DROP_SMALL_ITEMS_MIN = 10
local NUM_DROP_SMALL_ITEMS_MAX = 14

local DROPPED_ITEMS_SPAWN_HEIGHT = 10

local RAM_ALERT_COCOONS_RADIUS = 25

local NEW_VINES_SPAWN_RADIUS_MIN = 6

local ATTEMPT_DROP_OCEANTREENUT_MAX_ATTEMPTS = 5
local ATTEMPT_DROP_OCEANTREENUT_RADIUS = 6

local function removecanopyshadow(inst)
    if inst.canopy_data ~= nil then
        for _, shadetile_key in ipairs(inst.canopy_data.shadetile_keys) do
            if TheWorld.shadetiles[shadetile_key] ~= nil then
                TheWorld.shadetiles[shadetile_key] = TheWorld.shadetiles[shadetile_key] - 1

                if TheWorld.shadetiles[shadetile_key] <= 0 then
                    if TheWorld.shadetile_key_to_leaf_canopy_id[shadetile_key] ~= nil then
                        DespawnLeafCanopy(TheWorld.shadetile_key_to_leaf_canopy_id[shadetile_key])
                        TheWorld.shadetile_key_to_leaf_canopy_id[shadetile_key] = nil
                    end
                end
            end
        end

        for _, ray in ipairs(inst.canopy_data.lightrays) do
            ray:Remove()
        end
    end
end

local function OnFar(inst, player)
    if player.canopytrees then   
        player.canopytrees = player.canopytrees - 1
        player:PushEvent("onchangecanopyzone", player.canopytrees > 0)
    end
    inst.players[player] = nil
end

local function OnNear(inst,player)
    inst.players[player] = true

    player.canopytrees = (player.canopytrees or 0) + 1

    player:PushEvent("onchangecanopyzone", player.canopytrees > 0)
end

local UpdateShadowSize = function(shadow, height)
    local scaleFactor = Lerp(.5, 1.5, height / 35)
    shadow.Transform:SetScale(scaleFactor, scaleFactor, scaleFactor)
end 

local function RemoveCanopy(inst)
    for player in pairs(inst.players) do
        if player:IsValid() then
            if player.canopytrees then
                player.canopytrees = player.canopytrees - 1
                player:PushEvent("onchangecanopyzone", player.canopytrees > 0)
            end
        end
    end
    inst._hascanopy:set(false)
end

local function OnRemove_Tree(inst)
    RemoveCanopy(inst)
end

local FIREFLY_MUST = {"firefly"}
local function OnPhaseChanged(inst, phase)
   if phase == "day" then

        local x, y, z = inst.Transform:GetWorldPosition()

        if TheSim:CountEntities(x,y,z, TUNING.SHADE_CANOPY_RANGE, FIREFLY_MUST) < 10 then
            if math.random()<0.7 then
                local pos = nil
                local offset = nil
                local count = 0
                while offset == nil and count < 10 do
                    local angle = 2*PI*math.random()
                    local radius = math.random() * (TUNING.SHADE_CANOPY_RANGE -4)
                    offset = {x= math.cos(angle) * radius, y=0, z=math.sin(angle) * radius}   
                    count = count + 1

                    pos = {x=x+offset.x,y=0,z=z+offset.z}

                    if TheSim:CountEntities(pos.x, pos.y, pos.z, 5) > 0 then
                        offset = nil
                    end
                end

                if offset then
                    local firefly = SpawnPrefab("fireflies")
                    firefly.Transform:SetPosition(x+offset.x,0,z+offset.z)
                end
            end
        end
   end
end

local function fn(Sim)
	local inst = CreateEntity()
	inst.entity:AddTransform()
	inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
	inst.entity:AddNetwork()

    MakeObstaclePhysics(inst, 3, 24)

    inst:AddTag("tree_pillar")    
	inst:AddTag("shadecanopy")

	inst.AnimState:SetBank("pillar_tree")-- flash inst.AnimStateation .fla 
	inst.AnimState:SetBuild("pillar_tree")   -- art files
    inst.AnimState:PlayAnimation("idle",true)
	
	if not TheNet:IsDedicated() then
        inst:AddComponent("distancefade")
        inst.components.distancefade:Setup(15,25)
    end
	
	inst._hascanopy = net_bool(inst.GUID, "oceantree_pillar._hascanopy", "hascanopydirty")
    inst._hascanopy:set(true)  
    inst:ListenForEvent("hascanopydirty", function()
        if not inst._hascanopy:value() then 
            RemoveCanopyShadow(inst) 
        end
    end)

    inst:DoTaskInTime(0,function()
        inst.canopy_data = CANOPY_SHADOW_DATA.spawnshadow(inst, math.floor(TUNING.SHADE_CANOPY_RANGE/4))
    end)

	
	inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end
	------------------------------------------------------
	------------------------------------------------------
	inst.players = {}
   
    inst:AddComponent("playerprox")
    inst.components.playerprox:SetTargetMode(inst.components.playerprox.TargetModes.AllPlayers)
    inst.components.playerprox:SetDist(MIN_SHADE, MAX_SHADE)
    inst.components.playerprox:SetOnPlayerFar(OnFar)
    inst.components.playerprox:SetOnPlayerNear(OnNear)
	------------------------------------------------------
	inst:ListenForEvent("onremove", OnRemove_Tree)
    --inst:ListenForEvent("phasechanged", function(src, phase) OnPhaseChanged(inst,phase) end, TheWorld)
    
   return inst
end

local function SetTransparency(inst)
	if inst.components.colourtweener then
		if TheWorld.state.isnight and not TheWorld.state.isfullmoon then
			inst.components.colourtweener:StartTween({1, 1, 1, 0}, 3)
		elseif TheWorld.state.isdusk and inst.prefab == "light_rays_junglep" then	
			inst.components.colourtweener:StartTween({1, 1, 1, 0.2}, 3)
		else
			inst.components.colourtweener:StartTween({1, 1, 1, 0.5}, 3)
		end
	end
end

local function shadowfn(Sim)
	local inst = CreateEntity()
	local trans = inst.entity:AddTransform()
	local anim = inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
	inst.entity:AddNetwork()

    --MakeObstaclePhysics(inst, 3, 24)

    -- THIS WAS COMMENTED OUT BECAUSE THE ROC WAS BUMPING INTO IT. BUT I'M NOT SURE WHY IT WAS SET THAT WAY TO BEGIN WITH.
    --inst.Physics:SetCollisionGroup(COLLISION.GROUND)
    trans:SetScale(3,3,3)
    inst:AddTag("tree_shadow")
	inst:AddTag("NOCLICK") 
	inst:AddTag("NOBLOCK")

	anim:SetBank("tree_shadowp")
	anim:SetBuild("tree_shadowp") 

	if math.random(1, 3) > 2 then
		anim:PlayAnimation("idle3", true)
	else
		anim:PlayAnimation("idle1", true)
	end
	
    inst.AnimState:SetOrientation(ANIM_ORIENTATION.OnGround )

	----print(LAYER_WORLD)
	----print(LAYER_BACKGROUND)
	inst.AnimState:SetLayer(LAYER_WORLD)
	inst.AnimState:SetSortOrder(1)
	
	inst.AnimState:SetMultColour(1, 1, 1, 0.5)
	
	inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end
	
	inst:AddComponent("colourtweener")
	inst:WatchWorldState("isnight", SetTransparency)
	inst:WatchWorldState("startday", SetTransparency)
	
	trans:SetRotation(math.random() * 360)
    -------------------
    
   return inst
end

local function onvinesave(inst, data)
	data.animchoice = inst.animchoice
end

local function onvineload(inst, data)
    if data and data.animchoice then
        inst.animchoice = data.animchoice
	    inst.AnimState:PlayAnimation("idle_"..inst.animchoice)
	end
end

local function vinefn(Sim)
	local inst = CreateEntity()
	local trans = inst.entity:AddTransform()
	local anim = inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
	inst.entity:AddNetwork()

	inst.AnimState:SetBank("vine_rainforest_border")
    inst:AddTag("NOCLICK")
	inst:AddTag("NOBLOCK")
    inst.AnimState:SetBuild("vines_rainforest_border")
    -- anim:SetMultColour(.2, 1, .2, 1.0)

    anim:PlayAnimation("idle", true)
	
	inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end
    -------------------
	local color = 0.7 + math.random() * 0.3
    inst.AnimState:SetMultColour(color, color, color, 1)    

    inst.animchoice = math.random(1,6)
    inst.AnimState:PlayAnimation("idle_"..inst.animchoice)

    --------SaveLoad
    inst.OnSave = onvinesave 
    inst.OnLoad = onvineload 
    
   return inst
end

local function grassfn(Sim)
	local inst = CreateEntity()
	local trans = inst.entity:AddTransform()
	local anim = inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
	inst.entity:AddNetwork()

	inst.AnimState:SetBank("fern_plant")
    inst.AnimState:SetBuild("fern_plant")
    inst:AddTag("NOCLICK")
    -- anim:SetMultColour(.2, 1, .2, 1.0)

    if math.random()<0.5 then
        inst.AnimState:PlayAnimation("idle")
    else
        inst.AnimState:PlayAnimation("idle2")
    end
	
	inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end
    -------------------
	local color = 0.7 + math.random() * 0.3
    inst.AnimState:SetMultColour(color, color, color, 1)    
    
   return inst
end

local function grassspawnerfn(Sim)
	local inst = CreateEntity()
	local trans = inst.entity:AddTransform()
	local anim = inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
	inst.entity:AddNetwork()

	inst.AnimState:SetBank("fern_plant")
    inst.AnimState:SetBuild("fern_plant")
    inst:AddTag("NOCLICK")
    
	inst.AnimState:PlayAnimation("idle")
	
	inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end
    
	inst:DoTaskInTime(0, function(inst)
		local offset = 50
		local number = 30
		local pos = inst:GetPosition()
		for i=0, number do
			local grass = SpawnPrefab("junglegrassp")
			grass.Transform:SetPosition(pos.x + math.random(-offset, offset), 0, pos.z + math.random(-offset, offset))
			if not PlayablePets.IsAboveLand(grass:GetPosition():Get()) then
				grass:Remove()
			end
		end	
		inst:Remove()
	end)
    
   return inst
end

--ThePlayer:DoPeriodicTask(0.5, function(inst) if not inst.nospawn then local pos = inst:GetPosition() local theta = math.random() * 2 * PI local pt = inst:GetPosition() local radius = math.random(3, 6) local offset = FindWalkableOffset(pt, theta, radius, 3, true, true) if offset ~= nil then pos.x = pos.x + offset.x pos.z = pos.z + offset.z end local fx = SpawnPrefab("junglegrassp") fx.Transform:SetPosition(pos.x, 0, pos.z) end end)

local function lightfn(Sim)
	local inst = CreateEntity()
	local trans = inst.entity:AddTransform()
	local anim = inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
	inst.entity:AddNetwork()

	inst.AnimState:SetBank("light_rays")
    inst.AnimState:SetBuild("light_rays")
    inst:AddTag("NOCLICK")
	inst:AddTag("NOBLOCK")
    -- anim:SetMultColour(.2, 1, .2, 1.0)
	inst.AnimState:SetLayer(LAYER_WORLD)
	inst.AnimState:SetSortOrder(2)

    inst.AnimState:PlayAnimation("idle_loop")
	
	inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end
    -------------------
	
	 inst.Transform:SetRotation(45)

	local rays = {1,2,3,4,5,6,7,8,9,10,11}
        for i=1,#rays,1 do
            inst.AnimState:Hide("lightray"..i)
        end

        for i=1,math.random(4,6),1 do
            local selection =math.random(1,#rays)
            inst.AnimState:Show("lightray"..rays[selection]) 
            table.remove(rays,selection)
        end
	inst:AddComponent("colourtweener")
	inst:WatchWorldState("isnight", SetTransparency)
	inst:WatchWorldState("isdusk", SetTransparency)
	inst:WatchWorldState("startday", SetTransparency)	
    
   return inst
end

local function onfogload(inst, data)
	inst.colour = data.colour
	inst.scale = data.scale
end

local function onfogsave(inst, data)
	data.colour = inst.colour
	data.scale = inst.scale
end

local function fogfn(Sim)
	local inst = CreateEntity()
	local trans = inst.entity:AddTransform()
	local anim = inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
	inst.entity:AddNetwork()

	inst:AddTag("NOCLICK") 
	inst:AddTag("NOBLOCK")

	anim:SetBank("mist_fx")
	anim:SetBuild("mist_fxp")  
    -- anim:SetMultColour(.2, 1, .2, 1.0)

    anim:PlayAnimation("idle", true)
	trans:SetScale(2,2,2)
	inst.AnimState:SetFinalOffset(4)
	inst.AnimState:SetRayTestOnBB(true)
    inst.AnimState:SetOrientation(ANIM_ORIENTATION.OnGround )

	----print(LAYER_WORLD)
	----print(LAYER_BACKGROUND)
	inst.AnimState:SetLayer(LAYER_WORLD)
	inst.AnimState:SetSortOrder(1)
	
	inst.AnimState:SetMultColour(1, 1, 1, 0.5)
	
	anim:SetTime(math.random() * anim:GetCurrentAnimationLength())
	
	inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end
	
	inst.scale = 2
	inst.colour = {r = 0.3, g = 0.6, b = 0.2, a = 0.2}
	inst:DoTaskInTime(0, function(inst)
		inst.Transform:SetScale(inst.scale, inst.scale, inst.scale)
		inst.AnimState:SetMultColour(inst.colour.r, inst.colour.g, inst.colour.b, inst.colour.a )
	end)

    --------SaveLoad
    inst.OnSave = onfogsave 
    inst.OnLoad = onfogload 
    
   return inst
end

local function onpoisonareaload(inst, data)
	inst.range = data.range
	inst.damage = data.damage
	inst.immunetags = data.immunetags
	inst.poisonous = data.poisonous
	inst.inflictpoison = data.inflictpoison
end

local function onpoisonareasave(inst, data)
	data.range = inst.range
	data.damage = inst.damage
	data.immunetags = inst.immunetags
	data.poisonous = inst.poisonous
	data.inflictpoison = inst.inflictpoison
end

local function DoDamage(inst)
	if inst.range and inst.damage and inst.immunetags and inst.poisonous then
		local pos = inst:GetPosition()
		local ents = TheSim:FindEntities(pos.x,0,pos.z, inst.range, nil, inst.immunetags)
		if ents and #ents > 0 then
			for i, v in ipairs(ents) do
				if v:IsValid() and v.components.health and not v.components.health:IsDead() and v.components.combat and v ~= inst and (inst.poisonous == true and not v.poisonimmune) and not v.isbeenhit and not (v:HasTag("structure") or v:HasTag("wall")) then
					v.components.health:DoDelta(-inst.damage*(v.poisonmult or 1), nil, nil, nil, inst, true)
					v.isbeenhit = true
					v:DoTaskInTime(0.8, function(inst) inst.isbeenhit = nil end)
					if inst.inflictpoison and inst.inflictpoison == true then
						PlayablePets.SetPoison(inst, v)
					end
				end
			end
		end	
	end
end

local function poisonareafn(Sim)
	local inst = CreateEntity()
	local trans = inst.entity:AddTransform()
	local anim = inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
	inst.entity:AddNetwork()

	inst:AddTag("NOCLICK") 
	inst:AddTag("notarget")

	anim:SetBank("mist_fx")
	anim:SetBuild("mist_fxp")  
    -- anim:SetMultColour(.2, 1, .2, 1.0)

    anim:PlayAnimation("idle", true)
	trans:SetScale(2,2,2)
	inst.AnimState:SetFinalOffset(4)
	inst.AnimState:SetRayTestOnBB(true)
    inst.AnimState:SetOrientation(ANIM_ORIENTATION.OnGround )

	----print(LAYER_WORLD)
	----print(LAYER_BACKGROUND)
	inst.AnimState:SetLayer(LAYER_WORLD)
	inst.AnimState:SetSortOrder(1)
	
	inst.AnimState:SetMultColour(1, 1, 1, 0)
	
	anim:SetTime(math.random() * anim:GetCurrentAnimationLength())
	
	inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end
	
	inst:Hide()
	
	inst.range = 20
	inst.damage = 20
	inst.immunetags = {"notarget", "playerghost", "shadow", "flytrap"}
	inst.poisonous = true
	inst.inflictpoison = false
	inst:AddComponent("combat")
	--inst.components.combat:SetRange(60, 60)
	
	inst:DoPeriodicTask(2, DoDamage)

    --------SaveLoad
    inst.OnSave = onpoisonareasave 
    inst.OnLoad = onpoisonareaload 
    
   return inst
end

-----
local function onnear(inst)
	inst.AnimState:PlayAnimation("down")
    inst.AnimState:PushAnimation("idle_loop", true)
    inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/enemy/grabbing_vine/drop")
    inst.shadow:SetSize( 1.5, .75 )
end

local function onfar(inst)
    inst.AnimState:PlayAnimation("up")
    inst.SoundEmitter:PlaySound("dontstarve/cave/rope_up")
    inst.shadow:SetSize( 0,0 )
end


local function AnyNonVinePlayer(inst, self)
    local x, y, z = inst.Transform:GetWorldPosition()
    if not self.isclose then
        local player = FindClosestPlayerInRange(x, y, z, self.near, self.alivemode)
        if player ~= nil and not player:HasTag("vine") then
            self.isclose = true
            if self.onnear ~= nil then
                self.onnear(inst, player)
            end
        end
    elseif not IsAnyPlayerInRange(x, y, z, self.far, self.alivemode) then
        self.isclose = false
        if self.onfar ~= nil then
            self.onfar(inst)
        end
    end
end

local function vine2fn(Sim)
	local inst = CreateEntity()
	local trans = inst.entity:AddTransform()
	local anim = inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
	inst.entity:AddNetwork()
	inst.shadow = inst.entity:AddDynamicShadow()

	inst.shadow:SetSize( 1.5, .75 )
	inst.shadow:SetSize( 0,0 )
	inst:AddTag("NOCLICK")

	inst.AnimState:SetBank("exitrope")
	inst:AddTag("hangingvine")
	
	if math.random() < 0.5 then
		inst.AnimState:SetBuild("vine01_build")
	else
		inst.AnimState:SetBuild("vine02_build")
	end
	
	inst.AnimState:PlayAnimation("up")
	
	inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end
	
	inst:AddComponent("playerprox")
    inst.components.playerprox:SetOnPlayerNear(onnear)
    inst.components.playerprox:SetOnPlayerFar(onfar)
    inst.components.playerprox:SetDist(10,16)
	inst.components.playerprox:SetTargetMode(AnyNonVinePlayer, nil, true)

	inst:AddComponent("inspectable")
    
   return inst
end

-----

return Prefab( "tree_pillarp", fn, assets, prefabs ),
Prefab( "tree_shadowp", shadowfn, assets, prefabs ),
Prefab( "vinesp", vinefn, assets, prefabs ),
Prefab( "junglegrassp", grassfn, assets, prefabs ),
Prefab( "junglegrass_group", grassspawnerfn, assets, prefabs ),
Prefab( "light_rays_junglep", lightfn, assets, prefabs ),
Prefab( "mistp", fogfn, assets, prefabs ),
Prefab( "poisonareap", poisonareafn, assets, prefabs ),
Prefab( "hangingvinep", vine2fn, assets, prefabs )
