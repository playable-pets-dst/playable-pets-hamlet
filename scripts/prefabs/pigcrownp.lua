local assets=
{
	Asset("ANIM", "anim/hat_pigcrown.zip"),

}

local function onequip(inst, owner)
	local build = "hat_pigcrown"
	owner.AnimState:OverrideSymbol("swap_hat", build, "swap_hat")
	owner.AnimState:Show("HAT")
	owner.AnimState:Show("HAIR_HAT")
	owner.AnimState:Hide("HAIR_NOHAT")
	owner.AnimState:Hide("HAIR")

	if owner:HasTag("player") then
		--owner.AnimState:Hide("HEAD")
		owner.AnimState:Show("HEAD_HAIR")
		owner.AnimState:Hide("HAIRFRONT")
	end

	owner:AddTag("pig_royalty")
	if owner:HasTag("civilised") then
		owner._crown_originally_civilized = true
	else
		owner:AddTag("civilised")
	end
	if owner:HasTag("monster") then
		owner:AddTag("originally_monster")
		owner:RemoveTag("monster")
	end
end

local function hideHat(inst, owner)
	owner.AnimState:ClearOverrideSymbol("swap_hat")
	owner.AnimState:Hide("HAT")
	owner.AnimState:Hide("HAIR_HAT")
	owner.AnimState:Show("HAIR_NOHAT")
	owner.AnimState:Show("HAIR")

	if owner:HasTag("player") then
		owner.AnimState:Hide("HEAD_HAIR")
		owner.AnimState:Show("HAIRFRONT")
	end
end

local function onunequip(inst, owner)
	hideHat(inst, owner)
	owner:RemoveTag("pig_royalty")
	owner:RemoveTag("civilised")
	if owner:HasTag("originally_monster") then
		owner:AddTag("monster")
	end
	if owner._crown_originally_civilized then
		owner:AddTag("civilised")
		owner._crown_originally_civilized = nil
	end
end

local function fn()
	local inst = CreateEntity()
	local trans = inst.entity:AddTransform()
	local anim = inst.entity:AddAnimState()
	inst.entity:AddNetwork()
	
	inst.entity:AddSoundEmitter()
	MakeInventoryPhysics(inst)
	
	if CurrentRelease.GreaterOrEqualTo(ReleaseID.IDs.R08_ROT_TURNOFTIDES) then
		MakeInventoryFloatable(inst)
	end

	anim:SetBank("pigcrownhat")
	anim:SetBuild("hat_pigcrown")
	anim:PlayAnimation("anim")

	inst:AddTag("pigcrown")
	inst:AddTag("irreplaceable")

	inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end
	
	inst:AddComponent("equippable")
	inst.components.equippable.equipslot = EQUIPSLOTS.HEAD
	inst.components.equippable.dapperness = TUNING.DAPPERNESS_MED_LARGE
	-------
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst:AddComponent("itemtype")
		inst.components.itemtype:SetType("hats")
	end
	-------
	inst:AddComponent("inventoryitem")
	inst.components.inventoryitem.atlasname = "images/inventoryimages/ppham_icons.xml"
	inst.components.inventoryitem:SetSinks(false)
	
	inst:AddComponent("inspectable")

	--inst:AddComponent("tradable")

	

	inst.components.equippable:SetOnEquip( onequip )
	inst.components.equippable:SetOnUnequip( onunequip)

	return inst
end

return Prefab( "common/inventory/pigcrownp", fn, assets)
