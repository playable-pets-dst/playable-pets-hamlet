local MakePlayerCharacter = require "prefabs/player_common"
local easing = require("easing")
---------------------------
----------==Notes==--------
--
---------------------------

local assets = 
{
	Asset("ANIM", "anim/snake_scales_fx.zip"),
	Asset("ANIM", "anim/gaze_beam.zip"),
}

local prefabs = 
{	

}

local function OnSave_Mine(inst, data)
	data.permanent = inst.permanent or nil
end

local function OnLoad_Mine(inst, data)
    if data then
        inst.permanent = data.permanent or nil
	end
end

local function GetExcludeTags(inst)
	if TheNet:GetPVPEnabled() then
		return {"notarget", "INLIMBO", "shadow", "playerghost"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion", "playerghost", "INLIMBO", "notarget", "shadow"}
	else	
		return {"player", "companion", "playerghost", "INLIMBO", "notarget", "LA_mob", "battlestandard"}
	end
end


local function oncollide(inst)
	local posx, posy, posz = inst.Transform:GetWorldPosition()
	local ents = TheSim:FindEntities(posx, 0, posz, 4, { "_combat"}, GetExcludeTags(inst))
    
	for _,other in ipairs(ents) do
		if TheNet:GetServerGameMode() == "lavaarena" then
			if other.components.sleeper then
				other.components.sleeper:GoToSleep(6)
			end
		else
			if other.components.freezable and not other.components.freezable:IsFrozen() and other ~= inst.host then
				if inst.host and other.components.combat then
					other:PushEvent("attacked", {attacker = inst.host, damage = 0, weapon = inst})
				end
				other.components.freezable:AddColdness(5)
				other.components.freezable:SpawnShatterFX()
			end
		end
	end
end

local function fn(Sim)
	local inst = CreateEntity()
	local trans = inst.entity:AddTransform()
	local anim = inst.entity:AddAnimState()
	local physics = inst.entity:AddPhysics()
	local sound = inst.entity:AddSoundEmitter()
	inst.entity:AddNetwork()
    MakeGhostPhysics(inst, 0.5, 0.5)
    RemovePhysicsColliders(inst)


	anim:SetBank("gaze_beam")
	anim:SetBuild("gaze_beam")
	anim:PlayAnimation("loop")
	
	inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/pugalisk/gaze_LP","gaze")

	inst:AddTag("projectile")

	inst.entity:SetPristine()

        if not TheWorld.ismastersim then
            return inst
        end

	if TheNet:GetServerGameMode()  == "lavaarena" then
		--inst:AddComponent("fossilizer")
	end
	
	inst.timeremainingMax = 2
	inst.timeremaining = inst.timeremainingMax

	inst.gazetask = inst:DoPeriodicTask(0.3, oncollide)

	inst.Physics:SetMotorVelOverride(10,0,0)
	
	

	inst:ListenForEvent("animover", function(inst, data)
						if inst.gazetask then
							inst.gazetask:Cancel()
							inst.gazetask = nil
							inst.AnimState:PlayAnimation("loop_pst")
							inst.SoundEmitter:KillSound("gaze")
						else
							inst:Remove()
						end
                    end)    
                    

	return inst
end

local function fx_fn(inst)
        local inst = CreateEntity()

        inst.entity:AddTransform()
        inst.entity:AddNetwork()
		inst.entity:AddAnimState()
		inst.entity:AddSoundEmitter()
		
		inst.AnimState:SetBank("snake_scales_fx")
		inst.AnimState:SetBuild("snake_scales_fx")
		inst.AnimState:PlayAnimation("idle")

        inst:AddTag("FX")

        inst.entity:SetPristine()

        if not TheWorld.ismastersim then
            return inst
        end

        inst.persists = false
        inst:DoTaskInTime(2, inst.Remove)

    return inst
end

return Prefab( "snake_scales_fxp", fx_fn, assets, prefabs),
	Prefab("gaze_beamp", fn, assets, prefabs)