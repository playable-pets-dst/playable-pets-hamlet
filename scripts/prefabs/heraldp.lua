local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--
---------------------------

--Don't add assets unless absolutely needed.
local assets = 
{

	Asset("ANIM", "anim/herald_shiny_build_01.zip"),
	Asset("ANIM", "anim/herald_shiny_build_03.zip"),
	Asset("ANIM", "anim/herald_shiny_build_06.zip"),
	Asset("ANIM", "anim/herald_shiny_override_build_6.zip"),
	Asset("ANIM", "anim/ancient_spirit.zip"),
	--Asset("ANIM", "anim/ancient_spirit2.zip"),
}
--Might be completely pointless here. I don't know.
local prefabs = 
{	
	--
}
	
local getskins = {"1", "3","6","7"}	
--Note: Becareful when adding starting items.
--People are likely to spam changecharacter to get more of them.	
local start_inv = 
{
	--'prefab',
	--"spear",
}

local start_inv2 =
{
   --"beehome",
}

if MOBHOUSE == "Enable1" or MOBHOUSE == "Enable3" then
	start_inv = start_inv2
end
-----------------------
--Stats--
local mob = 
{
	health = 4000,
	hunger = 350,
	hungerrate = TUNING.WILSON_HUNGER_RATE, 
	sanity = 800,
	runspeed = 3.5,
	walkspeed = 3.5 ,
	damage = 50*2,
	range = 15,
	hit_range = 6,
	attackperiod = 5,
	bank = "ancient_spirit",
	build = "ancient_spirit",
	shiny = "herald",
	--build2 = "bee_guard_puffy_build",
	scale = 1.25,
	stategraph = "SGheraldp",
	minimap = "heraldp.tex",
	
}
-----------------------

--Loot that drops when you die, duh.
SetSharedLootTable('heraldp',
{
    {'nightmarefuel',  1.00},
	{'nightmarefuel',  1.00},
	{'nightmarefuel',  1.00},
	{'nightmarefuel',  1.00},
	{'nightmarefuel',  1.00},
	{'nightmarefuel',  1.00},
	{'nightmarefuel',  1.00},
	{'nightmarefuel',  1.00},
})

local FORGE_STATS = PPHAM_FORGE.HERALD

local function GetExcludeTags(inst)
	if TheNet:GetPVPEnabled() then
		return {"lightningimmune", "lightningrod", "notarget"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion", "lightningimmune", "lightningrod", "notarget"}
	else	
		return {"player", "companion", "lightningimmune", "lightningrod", "notarget"}
	end
end

local function onattacked(inst, data)
    inst.components.combat:SetTarget(data.attacker)
    inst.components.combat:ShareTarget(data.attacker, 30, function(dude) return dude:HasTag("shadow") and not dude.components.health:IsDead() end, 30)
end

local function OnEat(inst, food)
    if food and food.components.edible then
		if food.prefab == "glowberrymousse" then
			inst.components.health:DoDelta(-mob.health, false)
		end
    end
end

------------------------------------------------------
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
end
-------------------------------------------------------

local function SetSkinDefault(inst, data)
	if data then
		if data.override then
			inst.AnimState:OverrideSymbol("spirit_head", data.override, "spirit_head")
		else
			inst.AnimState:OverrideSymbol("spirit_head", mob.build, "spirit_head")
		end
		inst.AnimState:SetBuild(data.build)
	else
		inst.AnimState:OverrideSymbol("spirit_head", mob.build, "spirit_head")
		inst.AnimState:SetBuild(mob.build)
	end
end

local common_postinit = function(inst) 
	inst.MiniMapEntity:SetIcon(mob.minimap)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false)
	  --ThePlayer.HUD.controls.status.brain:Hide()
		if MONSTERHUNGER == "Disable" then
			--ThePlayer.HUD.controls.status.stomach:Hide()
		end
   end
end)
	-------------------------------
	--Tags--
	-------------------------------
	inst:AddTag("epic")
	inst:AddTag("monster") 
	inst:AddTag("laserimmune")
	inst:AddTag("flying")
	inst:AddTag("shadow_aligned")
	-------------------------------
	--inst:WatchWorldState( "onphasechanged", function() SetNightVision(inst) end)
	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst)
	
end

--==============================================
--					Forged Forge
--==============================================

local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, FORGE_STATS)
	inst.components.health:StopRegen()
	
	inst.healmult = 2
	
	inst:DoTaskInTime(0, function(inst)
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"darts", "melees"})
	end)
	
	inst.components.combat:SetDamageType(2)
	
	inst.components.revivablecorpse.revivespeedmult = 2
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

local function OnWaterChange(inst, onwater)
    if onwater then
        inst.onwater = true
        inst.sg:GoToState("submerge")
        inst.DynamicShadow:Enable(false)
    --        inst.components.locomotor.walkspeed = 3
    else
        inst.onwater = false        
        inst.sg:GoToState("emerge")
        inst.DynamicShadow:Enable(true)
    --        inst.components.locomotor.walkspeed = 4
    end
end

local function KillFollowers(inst)
	if inst.minions and #inst.minions > 0 then
		for i, v in ipairs(inst.minions) do
			if v.components.health and not v.components.health:IsDead() then
				if v.components.lootdropper then
					v.components.lootdropper:SetLoot(nil)
				end	
				v.components.health:Kill()
			end		
		end
	end
end
-----------------------------------------------------------------------

local master_postinit = function(inst)
	------------------------------------------
	--Stats--
	PlayablePets.SetCommonStats(inst, mob, nil, true) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20, 1) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst, 0, 0, 0, 9999) --fire, acid, poison
	inst.components.health:StartRegen(10, 5)
	----------------------------------
	--Loot drops--
	PlayablePets.SetCommonLootdropper(inst, "heraldp") --inst, prefaboverride
	----------------------------------	
	--inst.altattack = true
	--inst.mobsleep = true
	inst.inkimmune = true
	inst.debuffimmune = true
	inst.poisonimmune = true
	inst.acidimmune = true
	inst.taunt = true
	inst.taunt2 = true
	inst.taunt3 = true
	inst.maxminions = 4
	inst.minions = {}
	inst.minion_target = nil

	inst.components.ppskin_manager:SetSkinDefaultFn(SetSkinDefault)
	
	inst.mobplayer = true
	inst.ghostbuild = "ghost_monster_build"
	
	inst:ListenForEvent("onremove", KillFollowers)
	inst:ListenForEvent("death", KillFollowers)
	
    MakeLargeBurnableCharacter(inst, "swap_fire")
    --MakeLargeFreezableCharacter(inst, "spring")
	----------------------------------
	--Eater--

	--inst.components.eater:SetDiet({ FOODTYPE.MEAT, FOODTYPE.MEAT }, { FOODTYPE.MEAT, FOODTYPE.MEAT }) 
    inst.components.eater:SetAbsorptionModifiers(1,1.75,1) --This multiplies food stats.
	inst.components.eater:SetCanEatHorrible() --check to make them eat monster meat.
	inst.components.eater.strongstomach = true -- can eat monster meat!
	inst.components.eater:SetOnEatFn(OnEat)
	--inst.components.eater.eatwholestack = true --should eat the entire stack and get all the benefits.
	inst.components.eater.ignoresspoilage = true -- Can eat spoiled food without consequence. Might keep food stats as if fresh.
	---------------------------------
	--Physics and Scale--
	
	inst.DynamicShadow:SetSize(3, 1.25)

    MakeFlyingCharacterPhysics(inst, 1000, 1.5)
    inst.Transform:SetScale(mob.scale, mob.scale, mob.scale)
    inst.Transform:SetSixFaced()
	
	PlayablePets.SetAmphibious(inst, nil, nil, true)
	---------------------------------
	--Listeners--
	
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	---------------------------------
	--Functions that saves and loads data.

    inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
	--Forge--
	if inst.components.revivablecorpse then
		inst.forge_fn = SetForge(inst)
	end
    ------------------------------------------------------
	--Light and Character Functions. Don't Touch.--
    if MOBNIGHTVISION ~= "Disable2" then inst.components.grue:AddImmunity("mobplayer") end
	
	inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob, true, true, true) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob, true, true, true) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob, true, true, true, nil, true) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman, amphibious)
    end)
	
    return inst
	
end
--[[
if SKIN_RARITY_COLORS.ModMade ~= nil then
AddModCharacterSkin("beeguardp", 1, {normal_skin = "bee_guard_shiny_build_01", ghost_skin = "ghost_monster_build"}, {"bearger_shiny_build_01", "ghost_monster_build"}, {"BEARPLAYER", "FORMAL"})
end]]

return MakePlayerCharacter("heraldp", prefabs, assets, common_postinit, master_postinit, start_inv)
