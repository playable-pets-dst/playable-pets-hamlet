local assets =
{
    Asset("ANIM", "anim/cowbell.zip"),
}

local function onequip(inst, owner)
    owner.AnimState:OverrideSymbol("swap_object", "swap_fishingrod_ocean", "swap_fishingrod_ocean")

    owner.AnimState:Show("ARM_carry")
    owner.AnimState:Hide("ARM_normal")

    if inst.components.container ~= nil then
        inst.components.container:Open(owner)
    end
end

local function onunequip(inst, owner)
    owner.AnimState:Hide("ARM_carry")
    owner.AnimState:Show("ARM_normal")
    owner.AnimState:ClearOverrideSymbol("swap_object")

    if inst.components.container ~= nil then
        inst.components.container:Close()
    end
end

local function onequiptomodel(inst, owner, from_ground)
    if inst.components.container ~= nil then
        inst.components.container:Close()
    end
end

local function IsAsh(item, slot)
    return item and item:HasTag("spirit_ash")
end

local function GetCurrentAsh(inst)
	return inst.components.container ~= nil and inst.components.container.slots[1] or nil
end

local function OnSpiritAshChanged(inst, data)
	--remove all active spirits?
    local owner = inst.components.inventoryitem.owner
    if owner and owner.components.leader and #owner.components.leader.followers > 0 then
        for i, v in ipairs(owner.components.leader.followers) do
            if v and v:HasTag("spirit_summon") then
                owner.components.leader:RemoveFollower(v)
                v:DoSpiritFadeOut()
            end
        end
    end
end

--------------------------------------------------------
--Code for Spirits specfically

local function IsMyAlly(inst, target)
    if target.components.pigfaction then
        return inst.components.pigfaction.faction == target.components.pigfaction.faction
    else
        return not (target:HasTag("hostile") or target:HasTag("monster"))
    end
end

local TARGET_DIST = 20
local RETARGET_CANT_TAGS = {"wall", "insect", "civilian"}
--TODO commonize this.
local function spirit_retargetfn(inst)
    local leader = inst.components.follower.leader or nil
    if leader and leader.components.combat.target and leader.components.combat.target:IsValid() then
        return leader.components.combat.target
    end
    return FindEntity(
                inst,
                TARGET_DIST,
                function(guy)
                    return guy ~= leader and (not IsMyAlly(inst, guy) or (guy.components.combat and guy.components.combat.target == leader))
                end,
                {},
                RETARGET_CANT_TAGS
            )
        or nil
end

local function spirit_keeptargetfn(inst, target)
    --give up on dead guys, or guys in the dark, or werepigs
    return inst.components.combat:CanTarget(target)
           and not IsMyAlly(inst, target)
           and not (target.sg and target.sg:HasStateTag("transform"))
end

local function DoFadeOut(inst)
    inst.brain:Stop()
    inst.components.colourtweener:StartTween({1, 1, 1, 0}, 1, inst.Remove)
end

local function SummonSpirit(inst, owner)
    local ash = GetCurrentAsh(inst)
    if ash then
        local pos = owner:GetPosition()
        for i = 1, ash.count do
            local mob = SpawnPrefab(ash.ashprefab)
            mob.Transform:SetPosition(pos.x + 4*(math.random(-2,2)), 0, pos.z + 4*(math.random(-2,2)))
            mob.AnimState:SetSaturation(0)
            mob.AnimState:SetMultColour(1, 1, 1, 0)
            mob.AnimState:SetBloomEffectHandle("shaders/anim_bloom_ghost.ksh")
	        mob.AnimState:SetLightOverride(1)	
	        mob.AnimState:SetAddColour(1/7, 1/7, 1/7, 1)

            mob:AddTag("spirit_summon")
            mob.persists = false
            if mob.brain then
                mob.brain:Stop()
            end
            mob.components.health:SetInvincible(true)
            mob.components.health.nofadeout = true
            mob.DoFadeOut = DoFadeOut

            owner.components.leader:AddFollower(mob)

            if not mob.components.colourtweener then
                mob:AddComponent("colourtweener")
            end
            mob.components.colourtweener:StartTween({1, 1, 1, 0.8}, 1.5, function(mob) mob.brain:Start() end)

            if mob.components.combat then
                inst.components.combat:SetRetargetFunction(1, spirit_retargetfn)
                inst.components.combat:SetKeepTargetFunction(spirit_keeptargetfn)
            end

            if mob.components.lootdropper then
                mob.components.lootdropper:SetLoot({})
            end

            if not mob.components.pigfaction then
                mob:AddComponent("pigfaction")
            end

            if owner.components.pigfaction then
                mob.components.pigfaction.faction = owner.components.pigfaction.faction
            end

            mob:ListenForEvent("death", mob.DoFadeOut)
        end
    end
end


--------------------------------------------------------

local function fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddNetwork()

    MakeInventoryPhysics(inst)

    inst.AnimState:SetBank("cowbell")
    inst.AnimState:SetBuild("cowbell")
    inst.AnimState:PlayAnimation("idle1", true)

    MakeInventoryFloatable(inst)

    inst:AddTag("bell")

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    inst:AddComponent("inspectable")

    inst:AddComponent("inventoryitem")

    --Unsure if I want to make this an equippable weapon or an inventoryitem thing.
    inst:AddComponent("weapon")
    inst.components.weapon:SetDamage(15)

    inst:AddComponent("equippable")
    inst.components.equippable:SetOnEquip(onequip)
    inst.components.equippable:SetOnUnequip(onunequip)
    inst.components.equippable:SetOnEquipToModel(onequiptomodel)

    inst:AddComponent("instrument")
    inst.components.instrument.onplayed = SummonSpirit

    inst:AddComponent("tool")
    inst.components.tool:SetAction(ACTIONS.PLAY)
    
    inst:AddComponent("container")
    inst.components.container:WidgetSetup("spirit_bellp")
	inst.components.container.canbeopened = false
    inst.components.container.itemtestfn = IsAsh
    inst:ListenForEvent("itemget", OnSpiritAshChanged)
    inst:ListenForEvent("itemlose", OnSpiritAshChanged)

    return inst
end

return Prefab("spirit_bellp", fn, assets)
