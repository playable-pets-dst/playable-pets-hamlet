local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--
---------------------------

local assets = 
{
	Asset("ANIM", "anim/ds_squirrel_basic.zip"),
	Asset("ANIM", "anim/squirrel_cheeks_build.zip"),
	Asset("ANIM", "anim/squirrel_build.zip"),

	Asset("ANIM", "anim/orange_squirrel_cheeks_build.zip"),
	Asset("ANIM", "anim/orange_squirrel_build.zip"),
}

local prefabs = 
{	

}

local start_inv = 
{
	--'prefab',
	--"spear",
}

local start_inv2 =
{
   --"beehome",
}

if MOBHOUSE == "Enable1" or MOBHOUSE == "Enable3" then
	start_inv = start_inv2
end
-----------------------
--Stats--
local mob = 
{
	health = 50,
	hunger = 100,
	hungerrate = TUNING.WILSON_HUNGER_RATE, 
	sanity = 150,
	
	runspeed = 7,
	walkspeed = 3,
	
	attackperiod = 0,
	damage = 10,
	range = 2,
	hit_range = 2,
	
	bank = "squirrel",
	build = "squirrel_build",
	build2 = "orange_squirrel_build",
	shiny = "squirrel",
	
	scale = 1,
	stategraph = "SGpikop",
	minimap = "pikop.tex",	
}

--Loot that drops when you die, duh.
SetSharedLootTable( 'pikop',
{
    {'smallmeat',  1},
})

local FORGE_HP_TRIGGER = 0.5
local FORGE_STATS = PPHAM_FORGE.PIKO
--==============================================
--					Mob Functions
--==============================================
local INTENSITY = .5

local pikosounds = 
{
	scream = "dontstarve_DLC003/creatures/piko/scream",
	hurt = "dontstarve_DLC003/creatures/piko/scream",
}

local function fadein(inst)
    inst.components.fader:StopAll()
	inst.AnimState:Show("eye_red")
	inst.AnimState:Show("eye2_red")  
    inst.Light:Enable(true)
	inst.Light:SetIntensity(0)
	inst.components.fader:Fade(0, INTENSITY, 3+math.random()*2, function(v) inst.Light:SetIntensity(v) end)
end

local function fadeout(inst)
    inst.components.fader:StopAll()
	inst.AnimState:Hide("eye_red")
	inst.AnimState:Hide("eye2_red")
	inst.components.fader:Fade(INTENSITY, 0, 0.75+math.random()*1, function(v) inst.Light:SetIntensity(v) end)
end

local function updatelight(inst)
    if inst.currentlyRabid then
        if not inst.lighton then
            inst:DoTaskInTime(math.random()*2, function() 
                fadein(inst)
            end)

        else            
            inst.Light:Enable(true)
            inst.Light:SetIntensity(INTENSITY)
        end
		inst.AnimState:Show("eye_red")
		inst.AnimState:Show("eye2_red")      
        inst.lighton = true
    else
        if inst.lighton then
            inst:DoTaskInTime(math.random()*2, function() 
                fadeout(inst)
            end)            
        else
            inst.Light:Enable(false)
            inst.Light:SetIntensity(0)
        end

		inst.AnimState:Hide("eye_red")
		inst.AnimState:Hide("eye2_red")      

        inst.lighton = false
    end
end

local function OnDeath(inst)
	inst.Light:Enable(false)
end

local function SetAsRabid(inst, rabid)	
 	inst.currentlyRabid = rabid
 	updatelight(inst)
end
--==============================================
--				Custom Common Functions
--==============================================
local function OnHitOther(inst, other, damage)
    if other.components.inventory ~= nil then
		inst.components.thief:StealItem(other)
	end
end

local function SetVariation(inst, var)
	if inst.isshiny == 0 then
		if var > 5 then
			inst.AnimState:SetBuild(mob.build)
		else
			inst.AnimState:SetBuild(mob.build2)
		end
	end
end

local function SetNormal(inst)
	
	if TheNet:GetServerGameMode() ~= "lavaarena" then
		inst.transformed = nil
		local healthpercent = inst.components.health:GetPercent()
		inst.components.health:SetMaxHealth(mob.health)
		inst.components.health:SetPercent(healthpercent)
	end	
	
	SetAsRabid(inst, false)
	
	inst.components.hunger:SetRate(mob.hungerrate)
	
	inst.components.combat:SetDefaultDamage(TheNet:GetServerGameMode() == "lavaarena" and FORGE_STATS.DAMAGE or mob.damage)
	
	inst.components.locomotor:RemoveExternalSpeedMultiplier(inst, 1.2)
end

local function SetAporkalypse(inst)
	if (TheWorld.state.isfullmoon and TheWorld.state.isnight) or (TheNet:GetServerGameMode() == "lavaarena" and inst.components.health:GetPercent() <= FORGE_HP_TRIGGER) then
		inst.transformed = true
	
		SetAsRabid(inst, true)
		inst.components.hunger:SetRate(0.25)
	
		inst.components.combat:SetDefaultDamage(TheNet:GetServerGameMode() == "lavaarena" and FORGE_STATS.DAMAGE * 3 or mob.damage * 3)
	
		inst.components.locomotor:SetExternalSpeedMultiplier(inst, 1.2, 1.2)
		
		if TheNet:GetServerGameMode() ~= "lavaarena" then
			local healthpercent = inst.components.health:GetPercent()
			inst.components.health:SetMaxHealth(mob.health * 2)
			inst.components.health:SetPercent(healthpercent)
			inst:WatchWorldState("startday", SetNormal)
		elseif TheNet:GetServerGameMode() == "lavaarena" then
			inst.cursetask = inst:DoTaskInTime(15, SetNormal)
		end
	else
		SetNormal(inst)
	end
end
--==============================================
--					Loading/Saving
--==============================================
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
		inst.var = data.var or 1
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
	data.var = inst.var or 1
end

--==============================================
--					Forged Forge
--==============================================

local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, PPHAM_FORGE.PIKO)
	
	inst.mobsleep = false	
	
	inst:DoTaskInTime(0, function(inst)	
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"books", "staves", "darts"})
	end)
	
	inst:AddTag("moreaggro")
	
	inst:ListenForEvent("healthdelta", function(inst)
		local health = inst.components.health:GetPercent()
		if health > 0.75 then
			inst.transformed = nil
		elseif health <= FORGE_HP_TRIGGER and not inst.transformed then
			SetAporkalypse(inst)
		end	
	end)
	
	inst.components.combat:SetDamageType(1)
	
	inst.components.revivablecorpse.revivespeedmult = 0.33
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

--==============================================
--					Common/Master
--==============================================

local common_postinit = function(inst) 
	inst.MiniMapEntity:SetIcon(mob.minimap)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false) --PP doesn't work with movement prediction enabled, due to use of custom stategraphs
   end
end)
	--[[
	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst)]]
end



local master_postinit = function(inst)
	--Stats--
	PlayablePets.SetCommonStats(inst, mob, nil, true) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst, 1, 1, 1) --fire, acid, poison
	----------------------------------
	--Tags--
    inst:RemoveTag("scarytoprey")
	inst:RemoveTag("character")
	inst:AddTag("squirrel")
	
	----------------------------------
	--Variables	
	inst.mobsleep = true
	--inst.taunt = true
	inst.taunt2 = true
	inst.shouldwalk = false
	
	inst.currentlyRabid = false
	
	inst.var = math.random(1,10)
	
	inst.sounds = pikosounds
	
	local body_symbol = "chest"
	inst.poisonsymbol = body_symbol
	MakeSmallBurnableCharacter(inst, body_symbol)
    MakeTinyFreezableCharacter(inst, body_symbol)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	----------------------------------
	--Components
	PlayablePets.SetCommonLootdropper(inst) --inst, prefaboverride
	
	inst:AddComponent("thief")
	----------------------------------
	--Eater--
    inst.components.eater:SetAbsorptionModifiers(1,1,1) --This multiplies food stats.
	local pikofoodprefs = {"MEAT", "VEGGIE", "SEEDS", "ELEMENTAL", "WOOD", "ROUGHAGE", "GENERIC", "POOP"}
	inst.components.eater.caneat = pikofoodprefs
	inst.components.eater.preferseating = pikofoodprefs
	---------------------------------
	--Physics and Shadows--
	MakeCharacterPhysics(inst, 1, .12)
	inst.Transform:SetFourFaced()
	
	inst.DynamicShadow:SetSize(1, 0.75)
	--inst.DynamicShadow:Enable(false)  
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	inst.components.combat.onhitotherfn = OnHitOther
	---------------------------------
	--Functions
	inst:WatchWorldState("isfullmoon", SetAporkalypse)
    inst:DoTaskInTime(2, SetAporkalypse)
	---------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
    ------------------------------------------------------
	--Respawning and Initializing functions--
	
	local light = inst.entity:AddLight()
    light:SetFalloff(1)
    light:SetIntensity(INTENSITY)
    inst.Light:SetColour(150/255, 40/255, 40/255)  --197 10 10
    inst.Light:SetFalloff(0.9)
    inst.Light:SetRadius(2)    
    light:Enable(false)
    inst:AddComponent("fader")
	
	inst:ListenForEvent("death", OnDeath)
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(0, function(inst) SetVariation(inst, inst.var) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.05, function(inst) SetVariation(inst, inst.var) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob) end)
    end)
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
    return inst	
end

return MakePlayerCharacter("pikop", prefabs, assets, common_postinit, master_postinit, start_inv)