local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--
---------------------------

--Don't add assets unless absolutely needed.
local assets = 
{
	Asset("ANIM", "anim/weevole.zip"),
	Asset("ANIM", "anim/aphid.zip"),
}
--Might be completely pointless here. I don't know.
local prefabs = 
{	

}
	
--Note: Becareful when adding starting items.
--People are likely to spam changecharacter to get more of them.	
local start_inv = 
{
	--'prefab',
	--"spear",
}

local start_inv2 =
{
   --"beehome",
}

if MOBHOUSE == "Enable1" or MOBHOUSE == "Enable3" then
	start_inv = start_inv2
end
-----------------------
--Stats--
local mob = 
{
	health = 300,
	hunger = 150,
	hungerrate = TUNING.WILSON_HUNGER_RATE, 
	sanity = 100,
	runspeed = 5,
	walkspeed = 5,
	damage = 30*2,
	hit_range = 3,
	range = 4,
	attackperiod = 0,
	bank = "weevole",
	build = "weevole",
	shiny = "weevole",
	--build2 = "bee_guard_puffy_build",
	scale = 1,
	stategraph = "SGweevolep",
	minimap = "weevolep.tex",
	
}
-----------------------
--Loot that drops when you die, duh.
SetSharedLootTable('weevolep',
{
    {'monstermeat',  1.00},
})

------------------------------------------------------
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
end
-------------------------------------------------------


local common_postinit = function(inst) 
	inst.MiniMapEntity:SetIcon(mob.minimap)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false)
	  --ThePlayer.HUD.controls.status.brain:Hide()
		if MONSTERHUNGER == "Disable" then
			--ThePlayer.HUD.controls.status.stomach:Hide()
		end
   end
end)
	--Tags--
	inst:AddTag("monster")
	inst:AddTag("insect")
	inst:AddTag("weevole")
	
	--Worker--
	inst:AddComponent("worker")
	inst.components.worker:SetAction(ACTIONS.CHOP, 4)

	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst)
	
end
-------------Forge------------------------------------------


local ex_fns = require "prefabs/player_common_extensions"

local function OnForgeHitOther(inst, other)
	if other and other.components.armorbreak_debuff then
		other.components.armorbreak_debuff:ApplyDebuff()
	end
    inst.components.combat:SetTarget(other)
    inst.components.combat:ShareTarget(other, 30, function(dude) return dude:HasTag("weevole") and not dude.components.health:IsDead() end, 30)
end

local function DoAttackBuff(target)
    if target:IsValid() and not target.components.health:IsDead() and not target.buffimmune and not target.atkbuffed and target.components.debuffable then
		target.components.debuffable:AddDebuff("weevole_swarm_buff", "weevole_swarm_buff")	
	end
end

local function DoSelfBuff(inst)
	--OnPulse(inst)
	if inst.components.health and not inst.components.health:IsDead() then
    local x, y, z = inst.Transform:GetWorldPosition()
    local ents = TheSim:FindEntities(x, y, z, 10, nil, { "playerghost", "ghost", "battlestandard", "INLIMBO" }, {"player", "weevole"} )
		if #ents > 1 then
			DoAttackBuff(inst)
		end
	end
end

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, FORGE_STATS)
	
	inst.mobsleep = false	
	inst.specialsleep = false
	
	inst.components.health:SetAbsorptionAmount(0.8)
	
	inst:DoTaskInTime(0, function(inst)
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"books", "staves", "armors"})
	end)
	
	inst.components.combat.onhitotherfn = OnForgeHitOther
	
	inst.components.combat:SetDamageType(1)
	
	inst.components.revivablecorpse.revivespeedmult = 1
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

-----------------------------------------------------------------------
local function OnEat(inst, food)
    if food and food.components.edible and food.components.edible.foodtype == FOODTYPE.WOOD then
		inst.components.hunger:DoDelta(10, false)
    end
end

local master_postinit = function(inst)
	------------------------------------------
	--Stats--
    PlayablePets.SetCommonStats(inst, mob, nil, true) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20, 1) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst, 3, 3, 3) --fire, acid, poison
	----------------------------------
	--Loot drops--
	inst:AddComponent("lootdropper")
    inst.components.lootdropper:SetChanceLootTable("weevolep")	
	----------------------------------
	--Tags--
    inst:AddTag("monster")
	inst:AddTag("insect")
	inst:AddTag("weevole")
	
	inst.specialsleep = true
	inst.taunt = true
	--inst.taunt2 = true
	
	inst.mobplayer = true
	inst.ghostbuild = "ghost_monster_build"

	inst.isshiny = 0
	
	local body_symbol = "body"
	inst.poisonsymbol = body_symbol
	MakeMediumBurnableCharacter(inst, body_symbol)
    MakeMediumFreezableCharacter(inst, body_symbol)
	----------------------------------
	--Worker--
	inst:AddComponent("worker")
	inst.components.worker:SetAction(ACTIONS.CHOP, 3)
	----------------------------------
	--Eater--
    inst.components.eater.caneat = { "WOOD","SEEDS", "ROUGHAGE" }
	inst.components.eater.preferseating = { "WOOD","SEEDS", "ROUGHAGE" }
	inst.components.eater.oneatfn = OnEat
    inst.components.eater:SetAbsorptionModifiers(1,0.4,1) --This multiplies food stats.
	inst.components.eater:SetCanEatHorrible() --check to make them eat monster meat.
	inst.components.eater.strongstomach = true -- can eat monster meat!
	--inst.components.eater.eatwholestack = true --should eat the entire stack and get all the benefits.
	inst.components.eater.ignoresspoilage = true -- Can eat spoiled food without consequence. Might keep food stats as if fresh.
	---------------------------------
	--Physics and Scale--
	
	inst.DynamicShadow:SetSize(1.5, 0.5)
	inst.DynamicShadow:Enable(false)
    MakeCharacterPhysics(inst, 10, .5)
    inst.Transform:SetScale(mob.scale, mob.scale, mob.scale)
    inst.Transform:SetSixFaced()
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip)
	---------------------------------
	--Functions that saves and loads data.

    inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
	--Forge--
	if inst.components.revivablecorpse then
		inst.forge_fn = SetForge(inst)
	end
    ------------------------------------------------------
	--Light and Character Functions. Don't Touch.--
	
	inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman, amphibious)
    end)
	
    return inst
	
end

return MakePlayerCharacter("weevolep", prefabs, assets, common_postinit, master_postinit, start_inv)
