local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--
---------------------------

local getskins = {}
local getspecialskins = {}

local assets = 
{
	Asset("ANIM", "anim/chicken.zip"),
}

local prefabs = 
{	

}

local start_inv = 
{
	--'prefab',
	--"spear",
}

local start_inv2 =
{
   --"beehome",
}

if MOBHOUSE == "Enable1" or MOBHOUSE == "Enable3" then
	start_inv = start_inv2
end
-----------------------
--Stats--
local mob = 
{
	health = 200,
	hunger = 150,
	hungerrate = TUNING.WILSON_HUNGER_RATE, 
	sanity = 50,
	
	runspeed = 8,
	walkspeed = 8,
	
	attackperiod = 0,
	damage = 20,
	range = 2.5,
	hit_range = 3,
	
	bank = "chicken",
	build = "chicken",
	shiny = "chicken",
	
	scale = 1,
	stategraph = "SGchickenp",
	minimap = "chickenp.tex",	
}

--Loot that drops when you die, duh.
SetSharedLootTable( 'chickenp',
{
    {'drumstick',  1},
})

local FORGE_STATS = PPHAM_FORGE.CHICKEN
--==============================================
--					Mob Functions
--==============================================

--==============================================
--				Custom Common Functions
--==============================================

--==============================================
--					Loading/Saving
--==============================================
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
end

--==============================================
--					Forged Forge
--==============================================

local ex_fns = require "prefabs/player_common_extensions"

local function UpdateStealth(inst)
	local players = AllPlayers
	local aliveplayers = {}
	for i, v in ipairs(players) do
		if v.components.health and not v.components.health:IsDead() and v ~= inst then
			if not v:HasTag("worthless_target") then
				table.insert(aliveplayers, v)
			end
		end
	end
	if #aliveplayers > 0 then
		if not inst:HasTag("notarget") then
			inst:AddTag("notarget")
		end
	else
		if inst:HasTag("notarget") then
			inst:RemoveTag("notarget")
		end
	end
end

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, PPHAM_FORGE.CHICKEN)
	
	inst:AddTag("worthless_target")
	
	inst.mobsleep = false	
	
	inst:DoTaskInTime(0, function(inst)	
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"melees", "books", "staves", "darts"})
	end)
	
	inst.dummy_task = inst:DoPeriodicTask(1, UpdateStealth)
	
	
	inst.components.combat:SetDamageType(1)
	
	inst.components.revivablecorpse.revivespeedmult = 0.5
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

--==============================================
--					Common/Master
--==============================================

local common_postinit = function(inst) 
	inst.MiniMapEntity:SetIcon(mob.minimap)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false) --PP doesn't work with movement prediction enabled, due to use of custom stategraphs
   end
end)

end

local master_postinit = function(inst)
	--Stats--
	PlayablePets.SetCommonStats(inst, mob, nil, true) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst) --fire, acid, poison
	----------------------------------
	--Tags--
	inst:AddTag("chicken")
	----------------------------------
	--Variables	
	inst.mobsleep = true
	inst.taunt = true
	--inst.taunt2 = true
	
	local body_symbol = "chest"
	inst.poisonsymbol = body_symbol
	MakeSmallBurnableCharacter(inst, body_symbol)
    MakeTinyFreezableCharacter(inst, body_symbol)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	----------------------------------
	--Components
	PlayablePets.SetCommonLootdropper(inst, "chickenp") --inst, prefaboverride
	----------------------------------
	--Eater--
    inst.components.eater:SetAbsorptionModifiers(1,1,1) --This multiplies food stats.
	---------------------------------
	--Physics and Shadows--
	MakeCharacterPhysics(inst, 1, .12)
	inst.Transform:SetFourFaced()
	
	inst.DynamicShadow:SetSize(1, 0.75)
	--inst.DynamicShadow:Enable(false)  
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	---------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
    ------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob) end)
    end)
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
    return inst	
end

return MakePlayerCharacter("chickenp", prefabs, assets, common_postinit, master_postinit, start_inv)