local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--
---------------------------

local flytrap_health = 
{
	200,
	350,
	700,
}

local flytrap_hunger = 
{
	125,
	175,
	200,
}

local flytrap_speed = 
{
	6,
	5,
	4,
}

local flytrap_damage = 
{
	30*2,
	50*2,
	90*2,
}

local flytrap_range = 
{
	3,
	3.5,
	4,
}

--Don't add assets unless absolutely needed.
local assets=
{
	Asset("ANIM", "anim/venus_flytrap_sm_build.zip"),
	Asset("ANIM", "anim/venus_flytrap_lg_build.zip"),
	Asset("ANIM", "anim/venus_flytrap_build.zip"),
	Asset("ANIM", "anim/venus_flytrap.zip"),
	--Asset("ANIM", "anim/snapdragon.zip"),
	Asset("SOUND", "sound/hound.fsb"),
}
--Might be completely pointless here. I don't know.
local prefabs = 
{	

}
	
--Note: Becareful when adding starting items.
--People are likely to spam changecharacter to get more of them.	
local start_inv = 
{
	--'prefab',
	--"spear",
}

local start_inv2 =
{
   --"beehome",
}

if MOBHOUSE == "Enable1" or MOBHOUSE == "Enable3" then
	start_inv = start_inv2
end
-----------------------
--Stats--
local mob = 
{
	health = flytrap_health[1],
	hunger = flytrap_hunger[1],
	hungerrate = TUNING.WILSON_HUNGER_RATE, 
	sanity = 100,
	runspeed = flytrap_speed[1],
	walkspeed = 2,
	damage = flytrap_damage[1],
	hit_range = flytrap_range[1],
	range = flytrap_range[1],
	bank = "venus_flytrap",
	build = "venus_flytrap_sm_build",
	--build2 = "bee_guard_puffy_build",
	scale = 1,
	shiny = "flytrap",
	stategraph = "SGflytrapp",
	minimap = "flytrapp.tex",
	
}

local mob_teen = 
{
	health = flytrap_health[2],
	hunger = flytrap_hunger[2],
	hungerrate = TUNING.WILSON_HUNGER_RATE, 
	sanity = 100,
	runspeed = flytrap_speed[2],
	walkspeed = 2,
	damage = flytrap_damage[2],
	hit_range = flytrap_range[2],
	range = flytrap_range[2],
	bank = "venus_flytrap",
	build = "venus_flytrap_build",
	--build2 = "bee_guard_puffy_build",
	scale = 1.2,
	shiny = "flytrap",
	stategraph = "SGflytrapp",
	minimap = "flytrapp.tex",
	
}

local mob_adult = 
{
	health = flytrap_health[3],
	hunger = flytrap_hunger[3],
	hungerrate = TUNING.WILSON_HUNGER_RATE, 
	sanity = 100,
	runspeed = flytrap_speed[3],
	walkspeed = 2,
	damage = flytrap_damage[3],
	hit_range = flytrap_range[3],
	range = flytrap_range[3],
	bank = "venus_flytrap",
	build = "venus_flytrap_lg_build",
	--build2 = "bee_guard_puffy_build",
	scale = 1.6,
	shiny = "flytrap",
	stategraph = "SGflytrapp",
	minimap = "flytrapp.tex",
	
}
-----------------------
--Loot that drops when you die, duh.
SetSharedLootTable('flytrapp',
{
    {'plantmeat',     1.0},   
})

local function onattacked(inst, data)
    inst.components.combat:SetTarget(data.attacker)
    inst.components.combat:ShareTarget(data.attacker, 30, function(dude) return dude:HasTag("flytrap") and not dude.components.health:IsDead() end, 30)
end

local function onhitother2(inst, other)
    inst.components.combat:SetTarget(other)
    inst.components.combat:ShareTarget(other, 30, function(dude) return dude:HasTag("flytrap") and not dude.components.health:IsDead() end, 30)
end

local LVL2_REQ = 25
local LVL3_REQ = 75

--GROOOOW-------
local function SetAdult(inst)
	inst.isteen = false
	inst.isadult = true
	inst:RemoveTag("usefastrun")
	
	inst.currentTransform = 3
	
	PlayablePets.SetCommonStats(inst, mob_adult)

	inst.components.combat:SetRange(inst.components.revivablecorpse and PPHAM_FORGE.FLYTRAP_ADULT.HIT_RANGE or mob_adult.hit_range)
    inst.components.locomotor.runspeed = inst.components.revivablecorpse and PPHAM_FORGE.FLYTRAP_ADULT.RUNSPEED or mob_adult.runspeed
	inst.components.combat:SetDefaultDamage(inst.components.revivablecorpse and PPHAM_FORGE.FLYTRAP_ADULT.DAMAGE or mob_adult.damage)
	--inst.sg:GoToState("taunt")
	inst.AnimState:Hide("dirt")
	
	MakeCharacterPhysics(inst, 10, 1.33)
    inst.DynamicShadow:SetSize( 6, 3 )
    inst.DynamicShadow:Enable(false)
end

local function SetTeen(inst)
		inst.currentTransform = 2
		inst.isteen = true
		inst:RemoveTag("usefastrun")

		PlayablePets.SetCommonStats(inst, mob_teen)

		inst.components.combat:SetRange(inst.components.revivablecorpse and PPHAM_FORGE.FLYTRAP_TEEN.HIT_RANGE or mob_teen.hit_range)
		inst.components.locomotor.runspeed = inst.components.revivablecorpse and PPHAM_FORGE.FLYTRAP_TEEN.RUNSPEED or mob_teen.runspeed
		inst.components.combat:SetDefaultDamage(inst.components.revivablecorpse and PPHAM_FORGE.FLYTRAP_TEEN.DAMAGE or mob_teen.damage)

		inst.AnimState:Hide("dirt")
end

local function OnAdult(inst)
	inst.Transform:SetPosition(inst.Transform:GetWorldPosition())
	inst.new_build = "venus_flytrap_lg_build"
	inst.start_scale = 1.20

	inst.inc_scale = (1.40 - 1.20) /5
	inst.currentTransform = 3
	inst.sg:GoToState("grow")	
end

local function OnTeen(inst)
	inst.Transform:SetPosition(inst.Transform:GetWorldPosition())
	inst.new_build = "venus_flytrap_build"
	inst.start_scale = 1

	inst.inc_scale = (1.20 - 1) /5
	inst.currentTransform = 2
	inst.sg:GoToState("grow")
end

local LVL2_FORGEREQ = 50
local LVL3_FORGEREQ = 150

local function applyupgrades(inst, forced)

    local health_percent = inst.components.health:GetPercent()
	local hunger_percent = inst.components.hunger:GetPercent()
	--local sanity_percent = inst.components.sanity:GetPercent()

	if inst.foodcount < LVL2_REQ then
		inst.components.health.maxhealth = inst.components.revivablecorpse and PPHAM_FORGE.FLYTRAP.HEALTH or flytrap_health[1]
		inst.components.hunger.max = flytrap_hunger[1]
	end
	
    if inst.foodcount >= LVL2_REQ and inst.foodcount < LVL3_REQ or (forced and forced == "teen") then
    inst.components.health.maxhealth = inst.components.revivablecorpse and PPHAM_FORGE.FLYTRAP_TEEN.HEALTH or flytrap_health[2]
	inst.components.hunger.max = flytrap_hunger[2]
	
	inst.components.health:SetPercent(health_percent)
	
	
    end
	
	if inst.foodcount >= LVL3_REQ or (forced and forced == "adult") then
    inst.components.health.maxhealth = inst.components.revivablecorpse and PPHAM_FORGE.FLYTRAP_ADULT.HEALTH or flytrap_health[3]
	inst.components.hunger.max = flytrap_hunger[3]
	
	inst.components.health:SetPercent(health_percent)
    end
	
    inst.components.health:SetPercent(health_percent)
end

local function OnEat(inst, food)
    if food and food.components.edible then
		if food.components.edible.foodtype == FOODTYPE.MEAT  then
			--food helps you grow!
			if inst.foodcount < 100 then --add a cap.
			inst.foodcount = inst.foodcount + 1
			end
			if inst.foodcount >= LVL2_REQ and inst.foodcount < LVL3_REQ and inst.isteen == false and inst.isadult == false then
				--inst.sg:GoToState("transform")
				OnTeen(inst)
				applyupgrades(inst)
			end
		
			if inst.foodcount >= LVL3_REQ and inst.isadult == false then
				--inst.sg:GoToState("transform2")
				OnAdult(inst)
				applyupgrades(inst)
			end
		end
    end
end

local function ondeath(inst)
    inst.foodcount = 0
    applyupgrades(inst)
end

------------------------------------------------------
local function OnPreload(inst, data)
    if data ~= nil and data.foodcount ~= nil then
        inst.foodcount = data.foodcount
        applyupgrades(inst)
        --re-set these from the save data, because of load-order clipping issues
        if data.health and data.health.health then inst.components.health:SetCurrentHealth(data.health.health) end
        if data.hunger and data.hunger.hunger then inst.components.hunger.current = data.hunger.hunger end
        if data.sanity and data.sanity.current then inst.components.sanity.current = data.sanity.current end
        inst.components.health:DoDelta(0)
        inst.components.hunger:DoDelta(0)
        inst.components.sanity:DoDelta(0)
    end
end
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
		inst.foodcount = data.foodcount or 0
		inst.growpoints = data.growpoints or 0
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
	data.foodcount = inst.foodcount or 0
	data.growpoints = inst.growpoints or 0
end
-------------------------------------------------------
local function SetSkinDefault(inst, data)
	if data then
		if inst.currentTransform == 1 then
			inst.AnimState:SetBuild(data.build)
		elseif inst.currentTransform == 2 then
			inst.AnimState:SetBuild(data.teen_build)
		elseif inst.currentTransform == 3 then
			inst.AnimState:SetBuild(data.adult_build)
		end
	else
		if inst.currentTransform == 1 then
			inst.AnimState:SetBuild(mob.build)
		elseif inst.currentTransform == 2 then
			inst.AnimState:SetBuild(mob_teen.build)
		elseif inst.currentTransform == 3 then
			inst.AnimState:SetBuild(mob_adult.build)
		end
	end
end

local common_postinit = function(inst) 
	inst.MiniMapEntity:SetIcon(mob.minimap)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false)
	  --ThePlayer.HUD.controls.status.brain:Hide()
		if MONSTERHUNGER == "Disable" then
			--ThePlayer.HUD.controls.status.stomach:Hide()
		end
   end
end)

	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)

	PlayablePets.SetNightVision(inst)
	
end

-------------Forge------------------------------------------
local ex_fns = require "prefabs/player_common_extensions"

local function OnForgeHitOther(inst, other)
	if other and other:IsValid() and (other.sg and not other.sg:HasStateTag("hiding")) then
		inst.growpoints = inst.growpoints + 1
		if inst.growpoints >= LVL2_FORGEREQ and inst.growpoints < LVL3_FORGEREQ and inst.isteen == false and inst.isadult == false then
			OnTeen(inst)
			applyupgrades(inst, "teen")
		end	
		if inst.growpoints >= LVL3_FORGEREQ and inst.isteen == true and inst.isadult == false then
			inst.force_adult = true
			OnAdult(inst)
			applyupgrades(inst, "adult")
		end
	end
end

local function SetForge(inst)
	inst.components.health:SetMaxHealth(PPHAM_FORGE.FLYTRAP.HEALTH)
	
	inst.components.combat:SetDefaultDamage(PPHAM_FORGE.FLYTRAP.DAMAGE)
	inst.components.combat:SetAttackPeriod(PPHAM_FORGE.FLYTRAP.ATTACKPERIOD)
	
	inst.components.locomotor.walkspeed = PPHAM_FORGE.FLYTRAP.WALKSPEED
    inst.components.locomotor.runspeed = PPHAM_FORGE.FLYTRAP.RUNSPEED
	
	inst.components.combat:SetRange(PPHAM_FORGE.FLYTRAP.ATTACK_RANGE, PPHAM_FORGE.FLYTRAP.HIT_RANGE)
	--inst.components.combat:SetAreaDamage(PPHAM_FORGE.TREEGUARD.AOE_RANGE, PPHAM_FORGE.TREEGUARD.AOE_DMGMULT)
	
	inst.mobsleep = false
	
	inst.growpoints = 0
	
	inst.acidmult = 1.25
	
	inst.components.combat.onhitotherfn = OnForgeHitOther
	
	inst:DoTaskInTime(0, function(inst)
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"books", "staves", "darts"})
	end)
	
	inst.components.combat:SetDamageType(1)
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end
-----------------------------------------------------------------------
local function GetMobTable(inst)
	if inst.foodcount and inst.foodcount >= LVL2_REQ and inst.foodcount < LVL3_REQ then
		SetTeen(inst)
		inst.currentTransform = 2
		return mob_teen
	elseif inst.foodcount and inst.foodcount >= LVL3_REQ then
		SetAdult(inst)
		inst.currentTransform = 3
		return mob_adult
	else
		inst.currentTransform = 1
		return mob
	end
end

local master_postinit = function(inst)
	------------------------------------------
	--Stats--
    PlayablePets.SetCommonStats(inst, mob, nil, true) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20, 1) --heat, cold, wetness
	----------------------------------
	--Loot drops--
	inst:AddComponent("lootdropper")
    inst.components.lootdropper:SetChanceLootTable("flytrapp")
	----------------------------------
	--Tags--
	inst.AnimState:Hide("dirt")
	
	inst:AddTag("monster")
	inst:AddTag("flytrap")
	inst:AddTag("usefastrun")

	inst.setskin_defaultfn = SetSkinDefault
	
	inst.mobsleep = true
	inst.taunt = true
	--inst.taunt2 = true
	inst.foodcount = 0
	inst.isteen = false
	inst.isadult = false
	inst.isshiny = 0
	inst.currentTransform = 1

	inst.poison_symbol = "mane"
	local body_symbol = "mane"
	inst.poisonsymbol = body_symbol
	MakeMediumBurnableCharacter(inst, body_symbol)
    MakeMediumFreezableCharacter(inst, body_symbol)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	PlayablePets.SetCommonStatResistances(inst, 3, 1, 1) --fire, acid, poison, freeze (flat value, not a multiplier)
	----------------------------------
	--Eater--

	inst.components.eater:SetDiet({ FOODTYPE.MEAT, FOODTYPE.MEAT }, { FOODTYPE.MEAT, FOODTYPE.MEAT }) 
    inst.components.eater:SetAbsorptionModifiers(1,1,1) --This multiplies food stats.
	inst.components.eater.oneatfn = OnEat
	inst.components.eater:SetCanEatHorrible() --check to make them eat monster meat.
	inst.components.eater.strongstomach = true -- can eat monster meat!
	--inst.components.eater.eatwholestack = true --should eat the entire stack and get all the benefits.
	inst.components.eater.ignoresspoilage = true -- Can eat spoiled food without consequence. Might keep food stats as if fresh.
	---------------------------------
	--Physics and Scale--
	
	inst.DynamicShadow:SetSize(2.5, 1.5)

    MakeCharacterPhysics(inst, 10, .5)
    inst.Transform:SetFourFaced()
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	---------------------------------
	--Functions that saves and loads data.

	inst.OnPreload = OnPreload
    inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
	inst.userfunctions =
    {
        SetTeen = SetTeen,
        SetAdult = SetAdult,
    }
	
	inst.mobplayer = true
	inst.ghostbuild = "ghost_monster_build"
	
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
    ------------------------------------------------------
	--Light and Character Functions. Don't Touch.--
    inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, GetMobTable(inst)) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, GetMobTable(inst)) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, GetMobTable(inst)) end)
    inst:ListenForEvent("respawnfromghost", function() 
		inst:DoTaskInTime(0, function(inst)
			if inst.mob_scale and inst.mob_scale < 2 then
				inst.Transform:SetScale(inst.mob_scale, inst.mob_scale, inst.mob_scale)
			end
		end)
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, GetMobTable(inst)) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, GetMobTable(inst)) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, GetMobTable(inst)) ondeath(inst) end)
    end)
	
    return inst
	
end

return MakePlayerCharacter("flytrapp", prefabs, assets, common_postinit, master_postinit, start_inv)
