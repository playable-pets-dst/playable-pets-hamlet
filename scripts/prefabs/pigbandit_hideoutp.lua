local assets =
{
	Asset("ANIM", "anim/x_marks_spot_bandit.zip"),
	Asset( "IMAGE", "images/inventoryimages/pigbandit_hideoutp.tex" ),
	Asset( "ATLAS", "images/inventoryimages/pigbandit_hideoutp.xml" ),
}

local prefabs =
{
   
}

SetSharedLootTable('pigbandit_hideoutp',
{
    {"cutgrass", 1.00},
	{"cutgrass", 1.00},
})

local function onsave(inst, data)
	data.owner = inst.owner or nil
end

local function ondug(inst, worker)
	if inst.owner ~= nil then
		for i, v in ipairs(AllPlayers) do
			if TheWorld:HasTag("Caves") and v.userid == inst.owner and v.hideyhole_caves then
				v.hideyhole_caves = nil			
			elseif v.userid == inst.owner and v.hideyhole then
				--print("HIDEOUT DEBUG: Owner found, telling him we've been dug!")
				v.hideyhole = nil
			else
				--print("HIDEOUT DEBUG: Owner was not found, hopefully he'll get this onload")
			end
		end
	end
	inst.owner = nil
    if inst.components.burnable ~= nil and inst.components.burnable:IsBurning() then
        inst.components.burnable:Extinguish()
    end
    inst.components.lootdropper:DropLoot()
    if inst.components.container ~= nil then
        inst.components.container:DropEverything()
    end
	if inst.components.childspawner ~= nil then
			inst.components.childspawner:ReleaseAllChildren()
	end
    local fx = SpawnPrefab("collapse_small")
    fx.Transform:SetPosition(inst.Transform:GetWorldPosition())
    fx:SetMaterial("wood")
    inst:Remove()
end

local function onload(inst, data)
	inst.owner = data.owner or nil
end

local function onopen(inst)
    if not inst:HasTag("burnt") and inst.components.container.opener.prefab == "citypig_thiefp" then
        
	else
		inst.components.container:Close()
    end
end 



local function onclose(inst)

end

local function afterblink(inst)
	if inst.blinktask then
		inst.blinktask:Cancel()
		inst.blinktask = nil
	end
--	inst:RemoveEventCallback("animover", function() inst.afterblink(inst) end)
	inst.AnimState:PlayAnimation("idle")
	inst.blinktask = inst:DoTaskInTime(math.random()*2+1,function() inst.blink(inst) end)
end

local function blink(inst)
	if inst.blinktask then
		inst.blinktask:Cancel()
		inst.blinktask = nil
	end
	inst.AnimState:PlayAnimation("blink")
	inst.blinktask = inst:DoTaskInTime(30/10,function() inst.afterblink(inst) end)
end

local function onbuilt(inst, data)
    if data.builder then
		inst.owner = data.builder.userid
		if TheWorld:HasTag("Caves") then 
			data.builder.hideyhole_caves = inst:GetPosition()
		else
			data.builder.hideyhole = inst:GetPosition()
		end
	end
end

local function fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddMiniMapEntity()
    inst.entity:AddNetwork()

    MakeObstaclePhysics(inst, 0.2)

    inst.AnimState:SetBank("x_marks_spot_bandit")
    inst.AnimState:SetBuild("x_marks_spot_bandit")
    inst.AnimState:PlayAnimation("idle")

    inst:AddTag("structure")
	inst:AddTag("hideout")
	
    MakeSnowCoveredPristine(inst)

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    inst:AddComponent("inspectable")
	
	inst:AddComponent("container")
    inst.components.container:WidgetSetup("pigbandit_hideoutp")
	
    inst:AddComponent("workable")
    inst.components.workable:SetWorkAction(ACTIONS.DIG)
    inst.components.workable:SetMaxWork(1)
    inst.components.workable:SetWorkLeft(1)
    inst.components.workable:SetOnWorkCallback(ondug)

    inst:AddComponent("lootdropper")
    inst.components.lootdropper:SetChanceLootTable('pigbandit_hideoutp')
	
	inst:ListenForEvent("onbuilt", onbuilt)

    MakeHauntableWork(inst)

    inst.blink = blink
    inst.afterblink = afterblink
    afterblink(inst)

    inst.OnSave = onsave
    inst.OnLoad = onload

    MakeSnowCovered(inst)

    return inst
end

return Prefab("pigbandit_hideoutp", fn, assets, prefabs),
MakePlacer("pigbandit_hideoutp_placer", "x_marks_spot_bandit", "x_marks_spot_bandit", "idle")
