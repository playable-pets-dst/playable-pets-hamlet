local MakePlayerCharacter = require "prefabs/player_common"
local easing = require("easing")
---------------------------
----------==Notes==--------
--
---------------------------
local prefabname = "ancient_hulkp"

local assets = 
{
	Asset("ANIM", "anim/metal_hulk_build.zip"),
	Asset("ANIM", "anim/metal_hulk_basic.zip"),
    Asset("ANIM", "anim/metal_hulk_attacks.zip"),
    Asset("ANIM", "anim/metal_hulk_actions.zip"),
    Asset("ANIM", "anim/metal_hulk_barrier.zip"),
    Asset("ANIM", "anim/metal_hulk_explode.zip"),    
    Asset("ANIM", "anim/metal_hulk_bomb.zip"),    
    Asset("ANIM", "anim/metal_hulk_projectile.zip"),    

    Asset("ANIM", "anim/laser_explode_sm.zip"),  
    Asset("ANIM", "anim/smoke_aoe.zip"),
    Asset("ANIM", "anim/laser_explosion.zip"),
    --Asset("ANIM", "anim/ground_chunks_breaking.zip"),
    Asset("ANIM", "anim/ground_chunks_breaking_brown.zip"),
}

local prefabs = 
{	
	"groundpound_fx",
    "groundpoundring_fx",
	"ancient_hulk_minep",
	"ancient_hulk_orbp",
	"ancient_hulk_markerp",
    --"ancient_robots_assemblyp",
    "rock_basaltp"
}

local start_inv = 
{
	--'prefab',
	--"spear",
}

local start_inv2 =
{
   --"beehome",
}

local getskins = {"6", "8"}

if MOBHOUSE == "Enable1" or MOBHOUSE == "Enable3" then
	start_inv = start_inv2
end
-----------------------
--Stats--
local mob = 
{
    health = TUNING[string.upper(prefabname.."_HEALTH")],
	hunger = TUNING[string.upper(prefabname.."_HUNGER")],
	hungerrate = TUNING.WILSON_HUNGER_RATE, 
	sanity = TUNING[string.upper(prefabname.."_SANITY")],
	
	runspeed = TUNING.BEARGER_RUN_SPEED,
	walkspeed = 4,
	
	attackperiod = TUNING.BEARGER_ATTACK_PERIOD,
	damage = 200,
	range = 30,
	hit_range = 5.5,
	
	bank = "metal_hulk",
	build = "metal_hulk_build",
	shiny = "metal_hulk",
	
	scale = 1,
	stategraph = "SGancient_hulkp",
	minimap = "ancient_hulkp.tex",	
}

--Loot that drops when you die, duh.
SetSharedLootTable( 'ancient_hulkp',
{
    {'gears',  1.00},
	{'gears',  1.00},
	{'gears',  1.00},
	{'gears',  1.00},
	{'gears',  1.00},
	{'gears',  1.00},
	{'gears',  1.00},
	{'gears',  1.00},
	{'gears',  0.50},
	{'gears',  0.50},
})

local FORGE_HP_TRIGGER = 0.3
local FORGE_STATS = PPHAM_FORGE.ANCIENT_HULK
--==============================================
--					Mob Functions
--==============================================
local INTENSITY = .75
local function SetLightValue(inst, val1, val2, time)
    ----print("LIGHT VALUE", val1, val2, time)
    inst.components.fader:StopAll()
    if val1 and val2 and time then
        inst.Light:Enable(true)
        inst.components.fader:Fade(val1, val2, time, function(v) inst.Light:SetIntensity(v) end)
--[[
        if inst.Light ~= nil then
            inst.Light:Enable(true)
            inst.Light:SetIntensity(.6 * val)
            inst.Light:SetRadius(5 * val)
            inst.Light:SetFalloff(3 * val)
        end
        ]]
    else    
        inst.Light:Enable(false)
    end
end

local function GetExcludeTags(inst)
	if TheNet:GetPVPEnabled() then
		return {"playerghost", "notarget", "shadow", "laserimmune", "laser", "DECOR", "INLIMBO"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion", "playerghost", "laserimmune", "laser", "DECOR", "INLIMBO"}
	else	
		return {"player", "companion", "playerghost", "notarget", "laserimmune", "laser", "DECOR", "INLIMBO"}
	end
end

local function SetFires(x,y,z, rad)
    for i, v in ipairs(TheSim:FindEntities(x, 0, z, rad, nil, GetExcludeTags())) do 
        if v.components.burnable and MOBFIRE == "Disable" then
            v.components.burnable:Ignite()
        end
    end
end

local function DoDamage(inst, rad, startang, endang, spawnburns)
    local targets = {}
    local x, y, z = inst.Transform:GetWorldPosition()
    local angle = nil
    if startang and endang then
        startang = startang + 90
        endang = endang + 90
        
        local down = TheCamera:GetDownVec()             
        angle = math.atan2(down.z, down.x)/DEGREES
    end

    SetFires(x,y,z, rad)
    for i, v in ipairs(TheSim:FindEntities(x, 0, z, rad, nil, GetExcludeTags(inst), { "_combat", "pickable", "campfire", "CHOP_workable", "HAMMER_workable", "MINE_workable", "DIG_workable" })) do  --  { "_combat", "pickable", "campfire", "CHOP_workable", "HAMMER_workable", "MINE_workable", "DIG_workable" }
        local dodamage = true
        if startang and endang then
            local dir = inst:GetAngleToPoint(Vector3(v.Transform:GetWorldPosition())) 

            local dif = angle - dir         
            while dif > 450 do
                dif = dif - 360 
            end
            while dif < 90 do
                dif = dif + 360
            end                       
            if dif < startang or dif > endang then                
                dodamage = nil
            end
        end
        if dodamage and not targets[v] and v:IsValid() and not v:IsInLimbo() and not (v.components.health ~= nil and v.components.health:IsDead()) and not v:HasTag("laser_immune") then            
            local vradius = 0
            if v.Physics then
                vradius = v.Physics:GetRadius()
            end

            local range = rad + vradius
            if v:GetDistanceSqToPoint(Vector3(x, y, z)) < range * range then
                local isworkable = false
                if v.components.workable ~= nil then
                    local work_action = v.components.workable:GetWorkAction()
                    --V2C: nil action for campfires
                    isworkable =
                        (   work_action == nil and v:HasTag("campfire")    ) or
                        
                            (   work_action == ACTIONS.CHOP or
                                work_action == ACTIONS.HAMMER or
                                work_action == ACTIONS.MINE or   
                                work_action == ACTIONS.DIG
                            )
                end
                if isworkable then
                    targets[v] = true
                    v:DoTaskInTime(0.6, function() 
                        if v.components.workable then
                            v.components.workable:Destroy(inst) 
                            local vx,vy,vz = v.Transform:GetWorldPosition()
                            v:DoTaskInTime(0.3, function() SetFires(vx,vy,vz,1) end)
                        end
                     end)
                    if v:IsValid() and v:HasTag("stump") then
                       -- v:Remove()
                    end
                elseif v.components.health then              
                    inst.components.combat:DoAttack(v)                                    
                    if v:IsValid() then
                        if not v.components.health or not v.components.health:IsDead() then
                            if v.components.freezable ~= nil then
                                if v.components.freezable:IsFrozen() then
                                    v.components.freezable:Unfreeze()
                                elseif v.components.freezable.coldness > 0 then
                                    v.components.freezable:AddColdness(-2)
                                end
                            end
                            if v.components.temperature ~= nil then
                                local maxtemp = math.min(v.components.temperature:GetMax(), 10)
                                local curtemp = v.components.temperature:GetCurrent()
                                if maxtemp > curtemp then
                                    v.components.temperature:DoDelta(math.min(10, maxtemp - curtemp))
                                end
                            end
                        end
                    end                   
                end
                if v:IsValid() and v.AnimState then
                    SpawnPrefab("laserhitp"):SetTarget(v)
                end
            end
        end
    end
end

---------------------------------------------------------------------------------------

local function color(x,y,tiles,islands,value)
    tiles[y][x] = false
    islands[y][x] = value
end

local function check_validity(x,y,w,h,tiles,stack)
    if x >= 1 and y >= 1 and x <= w and y <= h and tiles[y][x] then
        stack[#stack+1] = {x=x,y=y}
    end
end

local function floodfill(x,y,w,h,tiles,islands,value)
--    Queue q
    local q = {}
--    q.push((x,y))
    q[#q+1] = {x=x,y=y}
--    while (q is not empty)
    while #q > 0 do
--       (x1,y1) = q.pop()
        local el = q[#q] 
        table.remove(q)
        local x1,y1 = el.x, el.y
--       color(x1,y1)         -- islandmap[x,y] = color
----print("Color",x1,y1)
        color(x1,y1,tiles,islands,value)
                            
        check_validity(x1+1,y1,w,h,tiles,q)
        check_validity(x1-1,y1,w,h,tiles,q)
        check_validity(x1,y1+1,w,h,tiles,q)
           check_validity(x1,y1-1,w,h,tiles,q)
        -- diagonals
        check_validity(x1-1,y1-1,w,h,tiles,q)
        check_validity(x1-1,y1+1,w,h,tiles,q)
        check_validity(x1+1,y1-1,w,h,tiles,q)
            check_validity(x1+1,y1+1,w,h,tiles,q)

--            q.push(x1,y1-1)    
    end
end

local function dofloodfillfromcoord(x,y,w, h, tiles, islands)
    local index = 3
    local rescan = true
    local val = tiles[y][x]
    if val then
        floodfill(x,y,w,h,tiles,islands,index)
        index = index + 1
    end
end

function getDropLocations(inst)
   local islands = {}
   local tiles = {}
   local map = GetWorld().Map
   local w,h = map:GetSize()

   for y = 1,h do
       tiles[y] = {}
       islands[y] = {}
       for x = 1, w do
           local tile = map:GetTile(x-1,y-1)

           tiles[y][x] = tile ~= GROUND.IMPASSABLE and tile ~= GROUND.LILYPOND
       end
   end
   local x,y,z = inst.Transform:GetWorldPosition()

   x = math.floor(x/4+ (w/2))
   z = math.floor(z/4 + (h/2))
   dofloodfillfromcoord(x,z,w, h, tiles, islands)

   local locations = {}
   for z=1,h do
       for x=1,w do
           if islands[z][x] then
               table.insert(locations,{x=x,z=z})
           end
       end
   end

   return locations
end

local function LaunchProjectile(inst, targetpos)
    local x, y, z = inst.Transform:GetWorldPosition()

    local projectile = SpawnPrefab("ancient_hulk_minep")
	projectile.owner = inst
    projectile.primed = false
    projectile.AnimState:PlayAnimation("spin_loop",true)
    projectile.Transform:SetPosition(x, 2.5, z)
	if inst.components.ppskin_manager:GetSkin() then
		local skindata = inst.components.ppskin_manager:GrabSkin(inst.components.ppskin_manager:GetSkin())
		if skindata and skindata.mine_build then
			projectile.AnimState:SetBuild(skindata.mine_build)
		end
	end

    --V2C: scale the launch speed based on distance
    --     because 15 does not reach our max range.
    local dx = targetpos.x - x
    local dz = targetpos.z - z
    local rangesq = dx * dx + dz * dz
    local maxrange = TUNING.FIRE_DETECTOR_RANGE
    local speed = easing.linear(rangesq, 15, 3, maxrange * maxrange)
    projectile.components.complexprojectile:SetHorizontalSpeed(speed)
    projectile.components.complexprojectile:SetGravity(-25)
    projectile.components.complexprojectile:Launch(targetpos, inst, inst)
    projectile.owner = inst
end


local function ShootProjectile(inst, targetpos)
    local x, y, z = inst.Transform:GetWorldPosition()

    local projectile = SpawnPrefab("ancient_hulk_orbp")

    projectile.primed = false
    projectile.AnimState:PlayAnimation("spin_loop",true)

    projectile.Transform:SetPosition(x, 4, z)

    local speed =  60 --  easing.linear(rangesq, 15, 3, maxrange * maxrange)
    projectile.components.complexprojectile:SetHorizontalSpeed(speed)
    projectile.components.complexprojectile:SetGravity(-25)
    projectile.components.complexprojectile:Launch(targetpos, inst, inst)
    projectile.owner = inst
end

local function CheckWaterTile(inst, pos)
	if SW_PP_MODE == true or CurrentRelease.GreaterOrEqualTo(ReleaseID.IDs.R08_ROT_TURNOFTIDES) then
		return IsOceanTile(TheWorld.Map:GetTileAtPoint(pos:Get()))
	else
		return false
	end
end

local function spawnbarrier(inst,pt)
    local angle = 0
    local radius = 13
    local number = 32
    for i=1,number do        
        local offset = Vector3(radius * math.cos( angle ), 0, -radius * math.sin( angle ))
        local newpt = pt + offset
        local tile = TheWorld.Map:GetTileAtPoint(newpt.x, newpt.y, newpt.z)

        if tile ~= GROUND.IMPASSABLE and tile ~= GROUND.INVALID and not CheckWaterTile(inst, newpt) then
            inst:DoTaskInTime(math.random()*0.3, function()            
                local rock = SpawnPrefab("rock_basaltp")
                rock.AnimState:PlayAnimation("emerge")
                inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/rock")
                rock.AnimState:PushAnimation("full")

                rock.Transform:SetPosition(newpt.x,newpt.y,newpt.z)
				rock:DoTaskInTime(TheNet:GetServerGameMode() == "lavaarena" and 10 or 30, function(inst) inst.components.health:DoDelta(-9999) end)
            end)
        end
        angle = angle + (PI*2/number)
    end
end

--==============================================
--				Custom Common Functions
--==============================================

local function ClearRecentlyCharged(inst, other)
    inst.recentlycharged[other] = nil
end

local function OnDestroyOther(inst, other)
	if other ~= nil and other:HasTag("wall") then
		local isname = inst:GetDisplayName()
		local selfpos = inst:GetPosition()
		print("LOGGED: "..isname.." destroyed a wall at "..selfpos.x..","..selfpos.y..","..selfpos.z.." as "..inst.prefab)
	end	
    if other:IsValid() and
        other.components.workable ~= nil and
        other.components.workable:CanBeWorked() and
        other.components.workable.action ~= ACTIONS.DIG and
        other.components.workable.action ~= ACTIONS.NET and other.prefab ~= "fossil_stalker" and
        not inst.recentlycharged[other] then
        SpawnPrefab("collapse_small").Transform:SetPosition(other.Transform:GetWorldPosition())
        other.components.workable:Destroy(inst)
        if other:IsValid() and other.components.workable ~= nil and other.components.workable:CanBeWorked() then
            inst.recentlycharged[other] = true
            inst:DoTaskInTime(3, ClearRecentlyCharged, other)
        end
    end
end

local function OnCollide(inst, other)
    if other ~= nil and (other:HasTag("boulder") or other:HasTag("tree")) and
        other:IsValid() and
        other.components.workable ~= nil and
        other.components.workable:CanBeWorked() and
        other.components.workable.action ~= ACTIONS.DIG and
        other.components.workable.action ~= ACTIONS.NET and
        not inst.recentlycharged[other] then
        inst:DoTaskInTime(2 * FRAMES, OnDestroyOther, other)
    end
end

local function NoHoles(pt)
    return not TheWorld.Map:IsGroundTargetBlocked(pt) 
end

local function ReticuleTargetFn(inst)
    local rotation = inst.Transform:GetRotation() * DEGREES
    local pos = inst:GetPosition()
    pos.y = 0
    for r = 13, 4, -.5 do
        local offset = FindWalkableOffset(pos, rotation, r, 1, false, true, NoHoles)
        if offset ~= nil then
            pos.x = pos.x + offset.x
            pos.z = pos.z + offset.z
            return pos
        end
    end
    for r = 13.5, 16, .5 do
        local offset = FindWalkableOffset(pos, rotation, r, 1, false, true, NoHoles)
        if offset ~= nil then
            pos.x = pos.x + offset.x
            pos.z = pos.z + offset.z
            return pos
        end
    end
    pos.x = pos.x + math.cos(rotation) * 13
    pos.z = pos.z - math.sin(rotation) * 13
    return pos
end

local function GetPointSpecialActions(inst, pos, useitem, right)
    if right and useitem == nil then
        local rider = inst.replica.rider
        if rider == nil or not rider:IsRiding() then
            return { ACTIONS.HULK_TELEPORT}
        end
    end
    return {}
end

local function OnSetOwner(inst)
    if inst.components.playeractionpicker ~= nil and TheNet:GetServerGameMode() ~= "lavaarena" then
        inst.components.playeractionpicker.pointspecialactionsfn = GetPointSpecialActions
    end
end
--==============================================
--					Loading/Saving
--==============================================
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
end

--==============================================
--					Forged Forge
--==============================================

local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, PPHAM_FORGE.ANCIENT_HULK)
	inst.components.health:SetAbsorptionAmount(0.9)
	inst.components.health:StopRegen()
	
	inst.acidmult = 2
	
	inst:DoTaskInTime(0, function(inst)	
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"books", "staves", "darts", "melees", "armors"})
	end)
	
	inst.components.combat:SetDamageType(1)
	
	inst.components.revivablecorpse.revivespeedmult = 2
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

--==============================================
--					Common/Master
--==============================================

local common_postinit = function(inst) 
	inst.MiniMapEntity:SetIcon(mob.minimap)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false) --PP doesn't work with movement prediction enabled, due to use of custom stategraphs
   end
end)
	
	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst)
	
	inst:ListenForEvent("setowner", OnSetOwner)
	--if inst.components.playeractionpicker ~= nil then
        --inst.components.playeractionpicker.pointspecialactionsfn = GetPointSpecialActions
   -- end
	
	inst:AddComponent("reticule")
    inst.components.reticule.targetfn = ReticuleTargetFn
    inst.components.reticule.ease = true
end

local master_postinit = function(inst)
	--Stats--
	PlayablePets.SetCommonStats(inst, mob) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20, 1) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst, 0, 0.5, 0) --fire, acid, poison
	inst.components.health:StartRegen(5, 2)
	
	inst.recentlycharged = {}
    inst.Physics:SetCollisionCallback(OnCollide)
	
	inst.AnimState:AddOverrideBuild("laser_explode_sm")
    inst.AnimState:AddOverrideBuild("smoke_aoe")    
    inst.AnimState:AddOverrideBuild("laser_explosion")   
    inst.AnimState:AddOverrideBuild("ground_chunks_breaking") 
	
	inst.components.health.destroytime = 5
	inst.components.combat:SetAreaDamage(5.5, 0.8)
	----------------------------------
	--Tags--
	inst:AddTag("epic")
	inst:AddTag("ancient_hulk")        
    inst:AddTag("laserimmune")
	----------------------------------
	--Variables	
	inst.specialsleep = true
	inst.taunt = true
	inst.taunt2 = true
	
	inst.can_mine = true
	inst.can_tele = true	
	
	inst.getskins = getskins
	
	local body_symbol = "segment01"
	inst.poisonsymbol = body_symbol
	MakeSmallBurnableCharacter(inst, body_symbol)
    MakeTinyFreezableCharacter(inst, body_symbol)
	
	PlayablePets.SetStormImmunity(inst)
	----------------------------------
	--Components
	PlayablePets.SetCommonLootdropper(inst, "ancient_hulkp") --inst, prefaboverride
	
	inst:AddComponent("groundpounder")
    inst.components.groundpounder.destroyer = true
    inst.components.groundpounder.damageRings = 0 --handle damage in stategraph instead
    inst.components.groundpounder.destructionRings = MOBFIRE == "Disable" and 3 or 0
    inst.components.groundpounder.numRings = 3
    inst.components.groundpounder.groundpoundfx = "groundpound_fx_hulkp"
	
	inst:AddComponent("fader")
    inst.glow = inst.entity:AddLight()    
    inst.glow:SetIntensity(.6)
    inst.glow:SetRadius(5)
    inst.glow:SetFalloff(3)
    inst.glow:SetColour(1, 0.3, 0.3)
    inst.glow:Enable(false)
	----------------------------------
	--Eater--
    inst.components.eater:SetAbsorptionModifiers(1,1,1) --This multiplies food stats.
	inst.components.eater:SetCanEatHorrible() --check to make them eat monster meat.
	inst.components.eater.strongstomach = true -- can eat monster meat!
	---------------------------------
	--Physics and Shadows--
	MakeCharacterPhysics(inst, 1000, 1.5)
	inst.Transform:SetSixFaced()
	
	inst.DynamicShadow:SetSize(6, 3.5)  
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	inst:ListenForEvent("onremove", function() inst.SoundEmitter:KillSound("gears") end, inst)
	---------------------------------
	--Functions
	inst.LaunchProjectile = LaunchProjectile
    inst.ShootProjectile = ShootProjectile
    inst.DoDamage = DoDamage
    inst.spawnbarrier = spawnbarrier
    inst.SetLightValue = SetLightValue
	---------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
    ------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob) end)
    end)
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
    return inst	
end

return MakePlayerCharacter("ancient_hulkp", prefabs, assets, common_postinit, master_postinit, start_inv)