local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--
---------------------------

--Don't add assets unless absolutely needed.
local assets =
{
	Asset("ANIM", "anim/elderdrake_basic.zip"),
	Asset("ANIM", "anim/elderdrake_actions.zip"),
	Asset("ANIM", "anim/elderdrake_attacks.zip"),
    Asset("ANIM", "anim/elderdrake_build.zip"),   

	Asset("SOUND", "sound/bunnyman.fsb"),
}

local getskins = {"1"}
--Might be completely pointless here. I don't know.
local prefabs = 
{	

}
	
--Note: Becareful when adding starting items.
--People are likely to spam changecharacter to get more of them.	
local start_inv = 
{
	--'prefab',
	--"spear",
}

local start_inv2 =
{
   --"beehome",
}

if MOBHOUSE == "Enable1" or MOBHOUSE == "Enable3" then
	start_inv = start_inv2
end
-----------------------
--Stats--
local mob = 
{
	health = 250,
	hunger = 200,
	hungerrate = TUNING.WILSON_HUNGER_RATE, 
	sanity = 250,
	runspeed = 6,
	walkspeed = 6,
	damage = 40,
	range = 2,
	hit_range = 3.5,
	attackperiod = 0,
	bank = "elderdrake",
	build = "elderdrake_build",
	shiny = "elderdrake",
	--build2 = "bee_guard_puffy_build",
	scale = 1.25,
	stategraph = "SGmandrakemanp",
	minimap = "mandrakemanp.tex",
	
}
-----------------------
--Loot that drops when you die, duh.
SetSharedLootTable('mandrakemanp',
{
    {'livinglog',     1.00},
    {'livinglog',     1.00},  
})

local function ontalk(inst, script)
	inst.SoundEmitter:PlaySound("dontstarve/creatures/bunnyman/idle_med")
end

--Temporary
local talkstuff = {"ROT!", "SOIL!", "COMPOST!", "MOLD"}

------------------------------------------------------
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
end
-------------------------------------------------------

local function test_talk(inst)
    return talkstuff[math.random(#talkstuff)]
end

local common_postinit = function(inst) 
	inst.MiniMapEntity:SetIcon(mob.minimap)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false)
	  --ThePlayer.HUD.controls.status.brain:Hide()
		if MONSTERHUNGER == "Disable" then
			--ThePlayer.HUD.controls.status.stomach:Hide()
		end
   end
end)
	inst.components.talker.ontalk = ontalk
    inst.components.talker.fontsize = 24
    inst.components.talker.offset = Vector3(0,-500,0)
	inst.components.talker.mod_str_fn = test_talk

	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst)
	
end

-------------Forge------------------------------------------

local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, PPHAM_FORGE.MANDRAKEMAN)
	
	--inst.mobsleep = false
	inst.components.health:StopRegen()
	inst.components.health:StartRegen(2, 1)
	inst.acidmult = 0.75
	
	inst:DoTaskInTime(0, function(inst)
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"melees", "darts"})
	end)
	
	inst.components.combat:SetDamageType(1)
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end
-----------------------------------------------------------------------

local function SetMood(inst)
	if TheWorld.state.isnight and TheWorld.state.isfullmoon then
		inst.AnimState:Show("head_happy")
		inst.AnimState:Hide("head_angry")
	else
		inst.AnimState:Show("head_angry")
		inst.AnimState:Hide("head_happy")
	end
end

local master_postinit = function(inst)
	------------------------------------------
	--Stats--
	PlayablePets.SetCommonStats(inst, mob, nil, true)
    PlayablePets.SetCommonWeatherResistances(inst, 60, 20, .25) --heat, cold, wetness
	----------------------------------
	--Loot drops--
	inst:AddComponent("lootdropper")
    inst.components.lootdropper:SetChanceLootTable("mandrakemanp")

	----------------------------------
	--Tags--
	inst:AddTag("pig")
    inst:AddTag("mandrakeman")
    inst:AddTag("scarytoprey")
	
	local body_symbol = "elderdrake_torso"
	inst.poisonsymbol = body_symbol
	MakeMediumBurnableCharacter(inst, body_symbol)
    MakeMediumFreezableCharacter(inst, body_symbol)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	inst.components.combat.hiteffectsymbol = body_symbol
	PlayablePets.SetCommonStatResistances(inst, 0.75, nil, nil, 5) --fire, acid, poison
	
	inst.mobsleep = true
	inst.taunt = true
	inst.taunt2 = true
	inst.getskins = getskins
	
	inst.mobplayer = true
	inst.ghostbuild = "ghost_monster_build"
	
	inst.AnimState:Hide("head_happy")
	inst.AnimState:Hide("hat")

	inst.isshiny = 0
	----------------------------------
	--Eater--
	inst.components.eater:SetDiet({ FOODTYPE.SEEDS, FOODTYPE.GENERIC ,FOODTYPE.VEGGIE }, { FOODTYPE.SEEDS, FOODTYPE.GENERIC, FOODTYPE.VEGGIE }) 
    inst.components.eater:SetAbsorptionModifiers(1,1.5,1) --This might multiply food stats.
	inst.components.eater:SetCanEatHorrible() --check to make them eat monster meat.
	inst.components.eater.strongstomach = true -- can eat monster meat!
	---------------------------------
	--Physics and Scale--
	
	inst.DynamicShadow:SetSize(1.5, 1.75)

    MakeCharacterPhysics(inst, 50, .5)
    inst.Transform:SetFourFaced()
	---------------------------------
	--Listeners--
	
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	inst:WatchWorldState("isfullmoon", SetMood)
	inst:WatchWorldState("startday", SetMood)
	---------------------------------
	--Functions that saves and loads data.

    inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
	--Forge--
	if inst.components.revivablecorpse then
		inst.forge_fn = SetForge(inst)
	end
    ------------------------------------------------------
	--Light and Character Functions. Don't Touch.--
	
	inst.components.talker.ontalk = ontalk
    inst.components.talker.fontsize = 24
    inst.components.talker.offset = Vector3(0,-500,0)
	inst.components.talker.mod_str_fn = test_talk
	
	inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob, nil, nil, nil, true) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob, nil, nil, nil, true) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob, nil, nil, nil, true) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman, amphibious)
    end)
	
    return inst
	
end

return MakePlayerCharacter("mandrakemanp", prefabs, assets, common_postinit, master_postinit, start_inv)
