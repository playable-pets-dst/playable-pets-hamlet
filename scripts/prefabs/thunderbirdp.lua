local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--
---------------------------
local getskins = {"3"}

--Don't add assets unless absolutely needed.
local assets = 
{
	Asset("ANIM", "anim/thunderbird.zip"),
    Asset("ANIM", "anim/thunderbird_fx.zip"),
	Asset("SOUND", "sound/perd.fsb"),
}
--Might be completely pointless here. I don't know.
local prefabs = 
{	
	"thunderbirdp_fx",
	"lightning"
}
	
--Note: Becareful when adding starting items.
--People are likely to spam changecharacter to get more of them.	
local start_inv = 
{
	--'prefab',
	--"spear",
}

local start_inv2 =
{
   --"beehome",
}

if MOBHOUSE == "Enable1" or MOBHOUSE == "Enable3" then
	start_inv = start_inv2
end
-----------------------
--Stats--
local mob = 
{
	health = 200,
	hunger = 150,
	hungerrate = TUNING.WILSON_HUNGER_RATE, 
	sanity = 100,
	runspeed = 7,
	walkspeed = 2,
	damage = 30*2,
	attackperiod = 5,
	range = 6,
	hit_range = 3,
	bank = "thunderbird",
	build = "thunderbird",
	shiny = "thunderbird",
	--build2 = "bee_guard_puffy_build",
	scale = 1,
	stategraph = "SGthunderbirdp",
	minimap = "thunderbirdp.tex",
	
}
-----------------------
--Loot that drops when you die, duh.
SetSharedLootTable('thunderbirdp',
{
    {'drumstick',     1.0},
    {'drumstick',     1.0},    
})

local function GetExcludeTags(inst)
	if TheNet:GetPVPEnabled() then
		return {"lightningimmune", "lightningrod", "notarget"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion", "lightningimmune", "lightningrod", "notarget"}
	else	
		return {"player", "companion", "lightningimmune", "lightningrod", "notarget"}
	end
end

local function DoLightning(inst, target)
    local LIGHTNING_COUNT = 3
    local COOLDOWN = 60
	local lightningtarget = target and target or inst

    for i=1, LIGHTNING_COUNT do
        inst:DoTaskInTime(0.4*i, function ()
            local rad = math.random(4, 8)
            local angle = i*((4*PI)/LIGHTNING_COUNT)
            local pos = Vector3(lightningtarget.Transform:GetWorldPosition()) + Vector3(rad*math.cos(angle), 0, rad*math.sin(angle))
            --TheWorld:PushEvent("ms_sendlightningstrike", pos)
			local lightning = SpawnPrefab("lightning")
			lightning.Transform:SetPosition(pos:Get())
			inst.components.combat:DoAreaAttack(lightning, 3,nil, nil, "electric", GetExcludeTags())
        end)
    end

    inst.cooling_down = true
    inst:DoTaskInTime(COOLDOWN, function () inst.cooling_down = false end)
end

local function spawnfx(inst)
    if not inst.fx then
        inst.fx = SpawnPrefab("thunderbirdp_fx")
        local x,y,z = inst.Transform:GetWorldPosition()
        inst.fx.Transform:SetPosition(x, y, z)

        local follower = inst.fx.entity:AddFollower()
        follower:FollowSymbol(inst.GUID, inst.components.combat.hiteffectsymbol, 0, 0, 0 )
        inst.fx:FacePoint(inst.Transform:GetWorldPosition())
   end
end
------------------------------------------------------
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
end
-------------------------------------------------------

local common_postinit = function(inst) 
	inst.MiniMapEntity:SetIcon(mob.minimap)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false)
	  --ThePlayer.HUD.controls.status.brain:Hide()
		if MONSTERHUNGER == "Disable" then
			--ThePlayer.HUD.controls.status.stomach:Hide()
		end
   end
end)

    local light = inst.entity:AddLight()
    light:SetFalloff(.7)
    light:SetIntensity(.75)
    light:SetRadius(2.5)
    light:SetColour(120/255, 120/255, 120/255)
    light:Enable(true)

    inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
    inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
    inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
    inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
    inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
    inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)

    PlayablePets.SetNightVision(inst)	
end
-------------Forge------------------------------------------
local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	inst.components.health:SetMaxHealth(PPHAM_FORGE.THUNDERBIRD.HEALTH)
	
	inst.components.combat:SetDefaultDamage(PPHAM_FORGE.THUNDERBIRD.DAMAGE)
	inst.components.combat:SetAttackPeriod(PPHAM_FORGE.THUNDERBIRD.ATTACKPERIOD)
	
	inst.components.locomotor.walkspeed = PPHAM_FORGE.THUNDERBIRD.WALKSPEED
    inst.components.locomotor.runspeed = PPHAM_FORGE.THUNDERBIRD.RUNSPEED
	
	inst.components.combat:SetRange(PPHAM_FORGE.THUNDERBIRD.ATTACK_RANGE, PPHAM_FORGE.THUNDERBIRD.HIT_RANGE)
	--inst.components.combat:SetAreaDamage(PPHAM_FORGE.TREEGUARD.AOE_RANGE, PPHAM_FORGE.TREEGUARD.AOE_DMGMULT)
	
	inst.components.combat:AddDamageBuff("passive_electric", {buff = 1.5, stimuli = "electric"})
	inst:AddComponent("passive_shock")
	
	inst.mobsleep = false
	
	--inst.acidmult = 1.25
	
	inst:DoTaskInTime(0, function(inst)
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"melees", "books", "staves", "darts"})
	end)
	
	inst.components.combat.onhitotherfn = OnForgeHitOther
	
	inst.components.combat:SetDamageType(DAMAGETYPES.MAGIC)
	
	inst.components.revivablecorpse.revivespeedmult = 1.5
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end
-----------------------------------------------------------------------

local master_postinit = function(inst)
	------------------------------------------
	--Stats--
    PlayablePets.SetCommonStats(inst, mob, nil, true) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20, 1) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst) --fire, acid, poison
	inst.components.burnable.lightningimmune = true
	----------------------------------
	--Loot drops--
	inst:AddComponent("lootdropper")
    inst.components.lootdropper:SetChanceLootTable("thunderbirdp")
	----------------------------------
	--Tags--
    inst:AddTag("berrythief")
	
	inst.AnimState:Hide("hat")
	
	inst.mobsleep = true
	inst.lookingmode = true
	inst.getskins = getskins
	inst.thunder_cd = true --TEMP FIX FOR ATTACKPERIOD BEING IGNORED
	
	inst.poison_symbol = "pig_torso" --for poison bubbles
	inst.taunt = true
	--inst.taunt2 = true
	
	inst.mobplayer = true
	inst.ghostbuild = "ghost_monster_build"

	inst.isshiny = 0
	inst.current_percent = 0
	
	--inst.special_action = function (act)
    --    inst.sg:GoToState("thunder_attack")
    --end

    inst:DoTaskInTime(0.1, function() spawnfx(inst) end)

    inst.DoLightning = DoLightning
    MakeMediumFreezableCharacter(inst, "pig_torso")
    MakeMediumBurnableCharacter(inst, "pig_torso")
	
	PlayablePets.SetStormImmunity(inst)
	----------------------------------
	--Eater--

	inst.components.eater:SetDiet({ FOODTYPE.SEEDS, FOODTYPE.GENERIC ,FOODTYPE.VEGGIE }, { FOODTYPE.SEEDS, FOODTYPE.GENERIC ,FOODTYPE.VEGGIE }) 
    inst.components.eater:SetAbsorptionModifiers(1,1.75,1) --This multiplies food stats.
	---------------------------------
	--Physics and Scale--
	
	inst.DynamicShadow:SetSize(1.5, 0.75)

    MakeCharacterPhysics(inst, 50, .5)
    inst.Transform:SetScale(mob.scale, mob.scale, mob.scale)
    inst.Transform:SetFourFaced()
	---------------------------------
	--Listeners--
	
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	--inst:ListenForEvent("attacked", onattacked)
	--inst.components.combat.onhitotherfn = onhitother2
	---------------------------------
	--Functions that saves and loads data.

    inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
	inst.forge_fn = SetForge(inst)
	end
    ------------------------------------------------------
	--Light and Character Functions. Don't Touch.--
	
	inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman, amphibious)
    end)
	
    return inst
	
end

local function fx_fn()
    local inst = CreateEntity()
    local trans = inst.entity:AddTransform()
    local anim = inst.entity:AddAnimState()
	inst.entity:AddNetwork()

    inst:AddTag("NOCLICK")

    anim:SetBank("thunderbird_fx")
    anim:SetBuild("thunderbird_fx")
	
	inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    return inst
end

return MakePlayerCharacter("thunderbirdp", prefabs, assets, common_postinit, master_postinit, start_inv),
Prefab("thunderbirdp_fx", fx_fn)
