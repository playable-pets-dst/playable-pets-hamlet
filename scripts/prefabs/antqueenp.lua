local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--
---------------------------
local prefabname = "antqueenp"
--Don't add assets unless absolutely needed.
local assets = 
{
	Asset("ANIM", "anim/crickant_queen_basics.zip"),
}
--Might be completely pointless here. I don't know.
local prefabs = 
{	

}
	
--Note: Becareful when adding starting items.
--People are likely to spam changecharacter to get more of them.	
local start_inv = 
{
	--'prefab',
	--"spear",
}

local start_inv2 =
{
   --"beehome",
}

if MOBHOUSE == "Enable1" or MOBHOUSE == "Enable3" then
	start_inv = start_inv2
end
-----------------------
--Stats--
local mob = 
{
	health = TUNING[string.upper(prefabname.."_HEALTH")],
	hunger = TUNING[string.upper(prefabname.."_HUNGER")],
	hungerrate = TUNING.WILSON_HUNGER_RATE, 
	sanity = TUNING[string.upper(prefabname.."_SANITY")],
	runspeed = 7,
	walkspeed = 2,
	damage = 250*2,
	range = 30,
	hit_range = 0,
	bank = "crick_crickantqueen",
	build = "crickant_queen_basics",
	shiny  = "antqueen",
	scale = 0.9,
	stategraph = "SGantqueenp",
	minimap = "antqueenp.tex",
	
}
-----------------------
--Loot that drops when you die, duh.
SetSharedLootTable('antqueenp',
{
    {'monstermeat',  1.00},
	{'monstermeat',  1.00},
	{'monstermeat',  1.00},
	{'monstermeat',  1.00},
	{'monstermeat',  1.00},
	{'honey',  		 1.00},
	{'honey',  		 1.00},
	{'honey',  		 1.00},
	{'honey',  		 1.00},
	{'honey',  		 1.00},
})

local function onattacked(inst, data)
    inst.components.combat:SetTarget(data.attacker)
    inst.components.combat:ShareTarget(data.attacker, 30, function(dude) return dude:HasTag("antman") and not dude.components.health:IsDead() end, 30)
end

local function onhitother2(inst, other)
	if inst.isaltattacking then
		PlayablePets.SetPoison(inst, other)
	end
    inst.components.combat:SetTarget(other)
    inst.components.combat:ShareTarget(other, 30, function(dude) return dude:HasTag("antman") and not dude.components.health:IsDead() end, 30)
end
------------------------------------------------------
local function SpawnWarrior(inst)
    
    local theta = math.random() * 2 * PI
	local pt = inst:GetPosition()
	local radius = math.random(3, 8)
	local offset = FindWalkableOffset(pt, theta, radius, 3, true, true)
	if offset ~= nil then
		pt.x = pt.x + offset.x
		pt.z = pt.z + offset.z
	end

    local egg = SpawnPrefab("antman_warrior_eggp")
    egg.queen = inst
    egg.Transform:SetPosition(pt.x, 35, pt.z)

    --local shadow = SpawnPrefab("warningshadow")
    --shadow.Transform:SetPosition(x, 0.2, z)
    --shadow:shrink(1.5, 1.5, 0.25)

    inst.warrior_count = inst.warrior_count + 1
end

local function WarriorKilled(inst, warrior)
	if warrior and inst.warriors[warrior] then
		inst.warriors[warrior] = nil
	end
    inst.warrior_count = inst.warrior_count - 1
end


------------------------------------------------------
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
end
-------------------------------------------------------
local common_postinit = function(inst) 
	inst.MiniMapEntity:SetIcon(mob.minimap)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false)
	  --ThePlayer.HUD.controls.status.brain:Hide()
		if MONSTERHUNGER == "Disable" then
			--ThePlayer.HUD.controls.status.stomach:Hide()
		end
   end
end)
	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)

	PlayablePets.SetNightVision(inst)		
end
-------------Forge------------------------------------------
local ex_fns = require "prefabs/player_common_extensions"

local function DoAttackBuff(target)
    if target:IsValid() and not target.components.health:IsDead() and not target.buffimmune and not target.defbuffed and target.components.debuffable then
		target.components.debuffable:AddDebuff("guard_buff", "guard_buff")	
	end
end

local function DoSelfBuff(inst)
	--OnPulse(inst)
	if inst.components.health and not inst.components.health:IsDead() then
    local x, y, z = inst.Transform:GetWorldPosition()
    local ents = TheSim:FindEntities(x, y, z, 40, nil, { "playerghost", "ghost", "battlestandard", "INLIMBO" }, {"antguard"} )
		if #ents > 0 then
			DoAttackBuff(inst)
		end
	end
end

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, PPHAM_FORGE.ANTQUEEN)
	
	inst.mobsleep = false
	
	inst.acidmult = 3
	
	inst.DoAura = inst:DoPeriodicTask(TUNING.FORGE.BATTLESTANDARD.PULSE_TICK, DoSelfBuff)
	
	inst:DoTaskInTime(0, function(inst)
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"books", "staves", "darts", "melees"})
	end)
	------------------------
	--Tags--
	inst:AddTag("antqueen")
	inst:AddTag("epic")
	------------------------

	--inst.components.combat.onhitotherfn = OnForgeHitOther
	
	inst.components.combat:SetDamageType(1)
	
	inst.components.revivablecorpse.revivespeedmult = 2.5
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end
-----------------------------------------------------------------------

local function KillFollowers(inst)
	if inst.warriors and #inst.warriors > 0 then
		for i, v in ipairs(inst.warriors) do
			if v.components.health and not v.components.health:IsDead() then
				if v.components.lootdropper then
					v.components.lootdropper:SetLoot(nil)
				end	
				v.components.health:Kill()
			end		
		end
	end
end

--------------------------------------------------------------------------
local function ClearRecentlyCharged(inst, other)
    inst.recentlycharged[other] = nil
end

local function OnDestroyOther(inst, other)
	if other ~= nil and other:HasTag("wall") then
		local isname = inst:GetDisplayName()
		local selfpos = inst:GetPosition()
		print("LOGGED: "..isname.." destroyed a wall at "..selfpos.x..","..selfpos.y..","..selfpos.z.." as "..inst.prefab)
	end	
    if other:IsValid() and
        other.components.workable ~= nil and
        other.components.workable:CanBeWorked() and
        other.components.workable.action ~= ACTIONS.DIG and
        other.components.workable.action ~= ACTIONS.NET and other.prefab ~= "fossil_stalker" and
        not inst.recentlycharged[other] then
        SpawnPrefab("collapse_small").Transform:SetPosition(other.Transform:GetWorldPosition())
        other.components.workable:Destroy(inst)
        if other:IsValid() and other.components.workable ~= nil and other.components.workable:CanBeWorked() then
            inst.recentlycharged[other] = true
            inst:DoTaskInTime(3, ClearRecentlyCharged, other)
        end
    end
end

local function OnCollide(inst, other)
    if other ~= nil and (other:HasTag("boulder") or other:HasTag("tree")) and
        other:IsValid() and
        other.components.workable ~= nil and
        other.components.workable:CanBeWorked() and
        other.components.workable.action ~= ACTIONS.DIG and
        other.components.workable.action ~= ACTIONS.NET and
        not inst.recentlycharged[other] then
        inst:DoTaskInTime(2 * FRAMES, OnDestroyOther, other)
    end
end
----------------------------------------

local master_postinit = function(inst)
	------------------------------------------
	--Stats--
	PlayablePets.SetCommonStats(inst, mob) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20, 1) --heat, cold, wetness
	------------------------------------------
	inst.components.locomotor:SetSlowMultiplier( 1 )
    inst.components.locomotor:SetTriggersCreep(false)
    --inst.components.locomotor.pathcaps = { ignorecreep = true }
	inst.components.locomotor.pathcaps = { ignorewalls = true }
	
	inst.recentlycharged = {}
    inst.Physics:SetCollisionCallback(OnCollide)
	
	----------------------------------
	--Loot drops--
	inst:AddComponent("lootdropper")
    inst.components.lootdropper:SetChanceLootTable("antqueenp")
	----------------------------------	
	inst.mobsleep = true
	inst.taunt = true
	inst.taunt2 = true
	
	inst.mobplayer = true
	inst.ghostbuild = "ghost_monster_build"
	
	inst.altattack = true

	inst.isshiny = 0
	
	-- Used in SGantqueen
	inst.warrior_count = 0
	
    inst.jump_count = 1
    inst.jump_attack_count = 0
    inst.max_jump_attack_count = 3

    inst.sanity_attack_count = 0
    inst.max_sanity_attack_count = 2

    inst.summon_count = 3
    inst.current_summon_count = 0

    inst.min_combat_cooldown = 5
    inst.max_combat_cooldown = 7
	
	inst.warriors = {}
	
	inst.SpawnWarrior = SpawnWarrior
	inst.WarriorKilled = WarriorKilled
	
	inst:AddComponent("groundpounder")
	inst.components.groundpounder.destroyer = false
	inst.components.groundpounder.groundpounddamagemult = 1
	inst.components.groundpounder.damageRings = 0
	inst.components.groundpounder.destructionRings = 0
	inst.components.groundpounder.numRings = 4
	--inst.components.groundpounder.groundpoundFn = OnGroundPound
	
	inst:ListenForEvent("onremove", KillFollowers)
	inst:ListenForEvent("death", KillFollowers)
	----------------------------
	local body_symbol = "crick_headbase"
	inst.poisonsymbol = body_symbol
	MakeLargeBurnableCharacter(inst, body_symbol)
    MakeLargeFreezableCharacter(inst, body_symbol)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	PlayablePets.SetCommonStatResistances(inst, 3, 3, 3) --fire, acid, poison
	----------------------------------
	--Eater--
	--inst.components.eater:SetDiet({ FOODTYPE.MEAT, FOODTYPE.MEAT }, { FOODTYPE.MEAT, FOODTYPE.MEAT }) 
	inst.components.eater:SetCanEatHorrible() --check to make them eat monster meat.
	inst.components.eater.strongstomach = true -- can eat monster meat!
	inst.components.eater.eatwholestack = true --should eat the entire stack and get all the benefits.
	inst.components.eater.ignoresspoilage = true -- Can eat spoiled food without consequence. Might keep food stats as if fresh.
	---------------------------------
	--Physics and Scale--
	inst.DynamicShadow:SetSize(1.5, 0.75)
	inst.DynamicShadow:Enable(false)
	if TheNet:GetServerGameMode() == "lavaarena" then
		MakeFlyingCharacterPhysics(inst, 1000, 2)
	else
		MakeCharacterPhysics(inst, 1000, 2) --TODO, use GiantCharacterPhysics and give the collides with COLLISION.LIMITS when RoT comes out.
	end	
	--inst.Physics:SetMass(9999)
    inst.Transform:SetTwoFaced()
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip)
	--inst:ListenForEvent("attacked", onattacked)
	inst.components.combat.onhitotherfn = onhitother2
	---------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
    ------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob, nil, true) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob, nil, true)
   		end)
	end)
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
    return inst
	
end

return MakePlayerCharacter("antqueenp", prefabs, assets, common_postinit, master_postinit, start_inv)
