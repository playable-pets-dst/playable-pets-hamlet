local assets =
{
	Asset("ANIM", "anim/antman_basic.zip"),
	Asset("ANIM", "anim/antman_attacks.zip"),
	Asset("ANIM", "anim/antman_actions.zip"),
    Asset("ANIM", "anim/antman_egghatch.zip"),
    Asset("ANIM", "anim/antman_guard_build.zip"),

    Asset("ANIM", "anim/antman_translucent_build.zip"),
}
--TODO: make eggs die when on water

local function dohatch(inst, hatch_time)
	if not inst.components.health:IsDead() then
	inst.updatetask = inst:DoTaskInTime(hatch_time, function()
		local pos = inst:GetPosition()
		if not inst.inlimbo then
			inst.AnimState:PlayAnimation("hatch")
			inst.components.health:SetInvincible(true)
			
			inst.updatetask = inst:DoTaskInTime(11 * FRAMES, 
				function()
					if not inst.inlimbo and not inst.components.health:IsDead() then
						ChangeToInventoryPhysics(inst)
						local warrior = SpawnPrefab("antman_warriorp_npc")
						warrior.Transform:SetPosition(  inst.Transform:GetWorldPosition() )
						warrior.sg:GoToState("hatch")

						if inst.queen and inst.queen:IsValid() and inst.queen.components.health and not inst.queen.components.health:IsDead() then
							warrior.queen = inst.queen
							if inst.queen.warriors then
							table.insert(inst.queen.warriors, warrior)
							end
							inst.queen.components.leader:AddFollower(warrior)
						end


						if warrior.queen then
							warrior:ListenForEvent("death", function(warrior, data) 
								warrior.queen:WarriorKilled(warrior)
							end)
						end
					end
				end
			)
		end
	end)
	end
end

local function ground_detection(inst)
	local pos = inst:GetPosition()
	
	if pos.y <= 0.2 and not inst.islanded then
		inst.islanded = true
		if CurrentRelease.GreaterOrEqualTo(ReleaseID.IDs.R08_ROT_TURNOFTIDES) then
			if IsOceanTile(TheWorld.Map:GetTileAtPoint(pos:Get())) and not TheWorld.Map:GetPlatformAtPoint(pos.x, pos.z) then
				local splash = SpawnPrefab("splash_green").Transform:SetPosition(pos:Get())
				inst:Remove()
			end
		end	
		--ChangeToObstaclePhysics(inst)
		inst.AnimState:PlayAnimation("land", false)
		inst.AnimState:PushAnimation("idle", true)

		if inst.updatetask then
			inst.updatetask:Cancel()
			inst.updatetask = nil
		end
		
		dohatch(inst, math.random(2, 6))
	end
end

local function start_grounddetection(inst)
	inst.updatetask = inst:DoPeriodicTask(FRAMES, ground_detection)
end

local function onremove(inst)	
	if inst.updatetask then
		inst.updatetask:Cancel()
		inst.updatetask = nil
	end
end

local function OnSave(inst, data)
	if inst.queen then
		--data.queen = inst.queen
	end
end

local function OnLoadPostPass(inst, ents, data)
    if data.queen_guid then
        --local queen = ents[data.queen_guid].entity
        --queen.WarriorKilled()
    end

	inst:Remove()
end

local function fn()
	local inst = CreateEntity()
	inst.entity:AddTransform()
	inst.entity:AddAnimState()
	inst.entity:AddSoundEmitter()
	inst.entity:AddNetwork()
	
	--inst.AnimState:SetRayTestOnBB(true);

	if TheNet:GetServerGameMode() == "lavaarena" then
		inst:AddTag("companion")
	end
	inst:AddTag("ant")
	
	inst.AnimState:SetBank("antman_egg")
	inst.AnimState:SetBuild("antman_guard_build")
	inst.AnimState:AddOverrideBuild("antman_egghatch")

	inst.AnimState:PlayAnimation("flying", true)
	
	inst.entity:SetPristine()
	
	if not TheWorld.ismastersim then
        return inst
    end	
	
	MakeInventoryPhysics(inst)

	inst:AddComponent("inspectable")
	inst:AddComponent("health")
	inst.components.health:SetMaxHealth(200)
	
	inst:AddComponent("combat")
	inst.components.combat:SetOnHit(function()
		if inst.components.health:IsDead() then
			inst.AnimState:PlayAnimation("break")
			if inst.queen then
				inst.queen:WarriorKilled()
			end
			onremove(inst)
		elseif not inst.components.health:IsInvincible() then
			inst.AnimState:PlayAnimation("hit", false)
		end
	end)

	inst.OnRemoveEntity = onremove
	inst.Transform:SetScale(1.15, 1.15, 1.15)

	inst:ListenForEvent("animover", function (inst) 
		if inst.AnimState:IsCurrentAnimation("hatch") then
			inst:Remove()
		end
	end)

	inst:DoTaskInTime(10, function(inst) inst:Remove() end) --incase eggs stay forever
	inst.eggify = function (inst)
		inst.AnimState:PlayAnimation("eggify", false)
		inst.AnimState:PushAnimation("idle", false)
		dohatch(inst, 1)
	end
	
	inst.updatetask = inst:DoPeriodicTask(0.2, start_grounddetection)
	inst.OnSave = OnSave
	inst.OnLoadPostPass = OnLoadPostPass

	return inst
end

return Prefab("antman_warrior_eggp", fn, assets, prefabs)