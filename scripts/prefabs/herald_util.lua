Herald = {}

local mobs = 
{
	--{prefab = "frog", summoncount = 20, max = 20},

}

local function ReTargetFn(inst)
	return FindEntity(inst, 30, function(guy) return inst.components.combat:CanTarget(guy) and not guy.components.health:IsDead() and not inst.components.follower.leader == guy end, nil, { "wall", "LA_mob", "battlestandard" })
end

local function KeepTargetFn(inst)


end

Herald.SummonMinions = function(inst)
	local pos = inst:GetPosition()
	local target = mobs[math.random(1, #mobs)]
	local summonee = SpawnPrefab(target.prefab)
	
	if sumonee then
		sumonee.Transform:SetPosition(pos:Get())
		
		summonee:AddComponent("follower")
		summonee.components.follower.keepleader = true
		inst.components.leader:AddFollower(summonee)
		
		
		
		if sumonee.components.combat and not sumonee:HasTag("meteor") then
			sumonee.components.combat:SetRetargetFn(0.5, ReTargetFn)
			sumonee.components.combat:SetKeepTargetFn(KeepTargetFn)		
		end
	end
end