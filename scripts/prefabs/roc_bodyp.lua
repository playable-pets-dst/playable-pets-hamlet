local assets=
{
	
}

--This is shit and so are you.

local prefabs =
{
	"roc_legp",
	"roc_tailp",
}


local function setstage(inst,stage)
	if stage == 1 then
		inst.Transform:SetScale(1,1,1)
        inst.components.locomotor.runspeed = 5
	elseif stage == 2 then
		inst.Transform:SetScale(1,1,1)
        inst.components.locomotor.runspeed = 7.5
    else
		inst.Transform:SetScale(1,1,1)
        inst.components.locomotor.runspeed = 10
	end
end


local function scalefn(inst,scale)
	inst.components.locomotor.runspeed = 20 * scale
	--inst.components.shadowcaster:setrange(TUNING.ROC_SHADOWRANGE*scale)	
end

local function OnRemoved(inst)     
    --GetWorld().components.rocmanager:RemoveRoc(inst)
end

local function redirecthealth(inst, amount, overtime, cause, ignore_invincible, afflicter, ignore_absorb)
	--print("DEBUG: Running redirect health for "..inst.prefab)

    if amount < 0 and inst.body and inst.body:IsValid() then
		local head = inst.body.head or nil
		----print("DEBUG: redirect passed for "..inst.prefab)
		if head and head.components.health and not head.components.health:IsDead() then
			head.components.health:DoDelta(amount, overtime, cause, ignore_invincible, afflicter, ignore_absorb)
			head:PushEvent("attacked", { attacker = afflicter})
		end		      
    end    
end

local function body_fn()
	local inst = CreateEntity()
	local trans = inst.entity:AddTransform()
	local anim = inst.entity:AddAnimState()
	local physics = inst.entity:AddPhysics()
	local sound = inst.entity:AddSoundEmitter()
	inst.entity:AddNetwork()
--	local shadow = inst.entity:AddDynamicShadow()
--	shadow:SetSize( 6.5, 6.5 )

	inst:AddTag("roc")
	inst:AddTag("roc_body")
	inst:AddTag("noteleport")
	inst:AddTag("notarget")
	inst:AddTag("NOCLICK")

	anim:SetBank("roc")
	anim:SetBuild("roc_shadow")
	anim:PlayAnimation("ground_loop")
	
	inst.entity:SetPristine()
	
	if not TheWorld.ismastersim then
		return inst
	end
	inst.persists = false

	--inst.Transform:SetSixFaced()
	MakeCharacterPhysics(inst, 10, 1.5)
	--MakeGhostPhysics(inst, 10, 1.5)
    --MakeCharacterPhysics(inst, 10, 1.5)
    RemovePhysicsColliders(inst)

    inst.Transform:SetScale(1.5,1.5,1.5)
	anim:SetOrientation( ANIM_ORIENTATION.OnGround )
	anim:SetLayer( LAYER_BACKGROUND )
	anim:SetSortOrder( 1 )	

	anim:SetMultColour(1, 1, 1, 0.5)

	inst:AddComponent("colourtweener")

	inst:AddComponent("knownlocations")

    inst:ListenForEvent("onremove", OnRemoved)

	inst:AddComponent("locomotor") -- locomotor must be constructed before the stategraph
	inst.components.locomotor.runspeed = 6

	inst:AddComponent("roccontrollerp")	
	inst.components.roccontrollerp:Setup(20, 0.35, 3)
	inst.components.roccontrollerp:Start()
	inst.components.roccontrollerp.scalefn = scalefn

	inst:SetStateGraph("SGroc_bodyp")

	inst.setstage = setstage

	return inst
end

local function leg_fn()
	local inst = CreateEntity()
	local trans = inst.entity:AddTransform()
	local anim = inst.entity:AddAnimState()
	local physics = inst.entity:AddPhysics()
	local sound = inst.entity:AddSoundEmitter()
	inst.entity:AddNetwork()
	--local shadow = inst.entity:AddDynamicShadow()
	--shadow:SetSize( 2.5, 1.5 )
	

	inst:AddTag("scarytoprey")
	inst:AddTag("monster")
	inst:AddTag("hostile")
	inst:AddTag("roc")
	inst:AddTag("roc_leg")
	inst:AddTag("noteleport")
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst:AddTag("companion")
	end

   

	anim:SetBank("foot")
	anim:SetBuild("roc_leg")
	anim:PlayAnimation("stomp_loop")
	--inst.AnimState:SetRayTestOnBB(true)
	
	inst.entity:SetPristine()
	
	if not TheWorld.ismastersim then
		return inst
	end
	
	inst.persists = false
	
	inst.Transform:SetSixFaced()
	MakeObstaclePhysics(inst, 2)
	
	inst:AddComponent("health")
    inst.components.health:SetMaxHealth(99999)
    inst.components.health.destroytime = 5
    inst.components.health.redirect = redirecthealth
	
	inst.poisonmult = 0.001
	
	inst:AddComponent("named")
	inst:DoTaskInTime(0, function(inst)
		inst.components.named:SetName(inst.body and inst.body.head and inst.body.head:GetDisplayName() or "BFB")
	end)
	
	inst:AddComponent("knownlocations")

	--inst:AddComponent("locomotor") -- locomotor must be constructed before the stategraph
	--inst.components.locomotor.runspeed = TUNING.SNAKE_SPEED

	inst:SetStateGraph("SGroc_legp")

--	inst:AddComponent("health")
--	inst.components.health:SetMaxHealth(TUNING.SNAKE_HEALTH)
	--inst.components.health.poison_damage_scale = 0 -- immune to poison

	inst:AddComponent("groundpounder")	
    inst.components.groundpounder.destroyer = false
    inst.components.groundpounder.damageRings = 0
    inst.components.groundpounder.destructionRings = 0
    inst.components.groundpounder.numRings = 2	
    
	inst:AddComponent("combat")
	inst.components.combat:SetDefaultDamage(200)
	
	inst:AddComponent("inspectable")

	inst:AddComponent("sanityaura")
	inst.components.sanityaura.aurafn = function() return -TUNING.SANITYAURA_LARGE end

	--inst:ListenForEvent("attacked", OnAttacked)
	--inst:ListenForEvent("onattackother", OnAttackOther)

	return inst
end

local function tail_fn()
	local inst = CreateEntity()
	local trans = inst.entity:AddTransform()
	local anim = inst.entity:AddAnimState()
	local physics = inst.entity:AddPhysics()
	local sound = inst.entity:AddSoundEmitter()
    inst.entity:AddNetwork()

	local shadow = inst.entity:AddDynamicShadow()

	inst:AddTag("scarytoprey")
	inst:AddTag("monster")
	inst:AddTag("hostile")
	inst:AddTag("roc")
	inst:AddTag("roc_tail")
	inst:AddTag("noteleport")
	inst:AddTag("notarget")

   -- MakeObstaclePhysics(inst, 2)

	anim:SetBank("tail")
	anim:SetBuild("roc_tail")
	anim:PlayAnimation("tail_loop")
	--inst.AnimState:SetRayTestOnBB(true)

	inst.entity:SetPristine()
	
	if not TheWorld.ismastersim then
		return inst
	end
	
	inst.persists = false
	
	shadow:SetSize( 8, 4 )
	
	inst.Transform:SetSixFaced()
	
	inst:AddComponent("knownlocations")
	
	inst:AddComponent("named")
	inst:DoTaskInTime(0, function(inst)
		inst.components.named:SetName(inst.body and inst.body.head and inst.body.head:GetDisplayName() or "BFB")
	end)

	--inst:AddComponent("locomotor") -- locomotor must be constructed before the stategraph
	--inst.components.locomotor.runspeed = TUNING.SNAKE_SPEED

	inst:SetStateGraph("SGroc_tailp")

--	inst:AddComponent("health")
--	inst.components.health:SetMaxHealth(TUNING.SNAKE_HEALTH)
	--inst.components.health.poison_damage_scale = 0 -- immune to poison


	inst:AddComponent("combat")
	inst.components.combat:SetDefaultDamage(300)
	inst:AddComponent("inspectable")

	inst:AddComponent("sanityaura")
	inst.components.sanityaura.aurafn = function() return -TUNING.SANITYAURA_LARGE end

	--inst:ListenForEvent("attacked", OnAttacked)
	--inst:ListenForEvent("onattackother", OnAttackOther)

	return inst
end

return Prefab("roc_bodyp", body_fn, assets, prefabs),
Prefab("roc_legp", leg_fn, assets, prefabs),
Prefab("roc_tailp", tail_fn, assets, prefabs)
