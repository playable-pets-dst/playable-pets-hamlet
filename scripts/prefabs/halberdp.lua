local assets=
{
	Asset("ANIM", "anim/halberd.zip"),
	Asset("ANIM", "anim/swap_halberd.zip"),
	Asset( "IMAGE", "images/inventoryimages/halberdp.tex" ),
    Asset( "ATLAS", "images/inventoryimages/halberdp.xml" ),

}

local function onfinished(inst)
	inst:Remove()
end

local function OnAttack(inst, attacker, target)
	--SpawnPrefab("weaponsparks_fx"):SetPosition(attacker, target)
	if target and target.components.armorbreak_debuff then
		target.components.armorbreak_debuff:ApplyDebuff()
	end
end


local function onequip(inst, owner)
	owner.AnimState:OverrideSymbol("swap_object", "swap_halberd", "swap_halberd")
	owner.AnimState:Show("ARM_carry")
	owner.AnimState:Hide("ARM_normal")
	if owner:HasTag("guard") then
		inst.components.weapon.attackwear = 0
	end
end

local function onunequip(inst, owner)
	owner.AnimState:Hide("ARM_carry")
	owner.AnimState:Show("ARM_normal")
	inst.components.weapon.attackwear = 1
end

local function fn()
	local inst = CreateEntity()
	local trans = inst.entity:AddTransform()
	local anim = inst.entity:AddAnimState()
	inst.entity:AddNetwork()
	
	inst.entity:AddSoundEmitter()
	MakeInventoryPhysics(inst)
	
	if CurrentRelease.GreaterOrEqualTo(ReleaseID.IDs.R08_ROT_TURNOFTIDES) then
		MakeInventoryFloatable(inst)
	end

	anim:SetBank("halberd")
	anim:SetBuild("halberd")
	anim:PlayAnimation("idle")

	inst:AddTag("halberd")
	inst:AddTag("sharp")
	inst:AddTag("pointy")

	inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end
	
	inst:AddComponent("weapon")
	inst.components.weapon:SetDamage(32*1.3)

	-----
	inst:AddComponent("tool")
	inst.components.tool:SetAction(ACTIONS.CHOP)
	-------
	if TheNet:GetServerGameMode() ~= "lavaarena" then
		inst:AddComponent("finiteuses")
		inst.components.finiteuses:SetMaxUses(100)
		inst.components.finiteuses:SetUses(100)
		inst.components.finiteuses:SetOnFinished(onfinished)
		inst.components.finiteuses:SetConsumption(ACTIONS.CHOP, 1)
	else
		inst:AddComponent("itemtype")
		inst.components.itemtype:SetType("melees")
		inst.components.weapon:SetOnAttack(OnAttack)
		inst.components.weapon:SetDamageType(1)
		inst.components.weapon:SetDamage(30)
	end
	-------
	inst:AddComponent("inventoryitem")
	inst.components.inventoryitem.atlasname = "images/inventoryimages/halberdp.xml"
	inst.components.inventoryitem:SetSinks(true)
	
	inst:AddComponent("inspectable")

	inst:AddComponent("tradable")

	inst:AddComponent("equippable")

	inst.components.equippable:SetOnEquip( onequip )
	inst.components.equippable:SetOnUnequip( onunequip)

	return inst
end

local function normal()
	local inst = fn()

	if not TheWorld.ismastersim then
        return inst
    end
	
	inst:AddComponent("inventoryitem")
	inst.components.inventoryitem.atlasname = "images/inventoryimages/halberdp.xml"

	if HAMLET_PP_MODE then
		PlayablePets.ReplacePrefab(inst, "halberd")
	end

	return inst
end

local function black(Sim)
	local inst = fn(Sim)

	if not TheWorld.ismastersim then
        return inst
    end

	inst:AddComponent("inventoryitem")

	return inst
end

return Prefab( "common/inventory/halberdp", fn, assets)
