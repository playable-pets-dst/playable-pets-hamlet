local MakePlayerCharacter = require "prefabs/player_common"
local easing = require("easing")
---------------------------
----------==Notes==--------
--
---------------------------

local assets = 
{
	Asset("ANIM", "anim/metal_hulk_build.zip"),
	Asset("ANIM", "anim/metal_hulk_basic.zip"),
    Asset("ANIM", "anim/metal_hulk_attacks.zip"),
    Asset("ANIM", "anim/metal_hulk_actions.zip"),
    Asset("ANIM", "anim/metal_hulk_barrier.zip"),
    Asset("ANIM", "anim/metal_hulk_explode.zip"),    
    Asset("ANIM", "anim/metal_hulk_bomb.zip"),    
	Asset("ANIM", "anim/metal_hulk_ring_fx.zip"),
    Asset("ANIM", "anim/metal_hulk_projectile.zip"),    

    Asset("ANIM", "anim/laser_explode_sm.zip"),  
    Asset("ANIM", "anim/smoke_aoe.zip"),
    Asset("ANIM", "anim/laser_explosion.zip"),
    --Asset("ANIM", "anim/ground_chunks_breaking.zip"),
    Asset("ANIM", "anim/ground_chunks_breaking_brown.zip"),
	
	Asset("ANIM", "anim/rock_basalt.zip"),
	Asset("MINIMAP_IMAGE", "rock"),
}

local prefabs = 
{	

}

local INTENSITY = .75
local function SetLightValue(inst, val1, val2, time)
    ----print("LIGHT VALUE", val1, val2, time)
    inst.components.fader:StopAll()
    if val1 and val2 and time then
        inst.Light:Enable(true)
        inst.components.fader:Fade(val1, val2, time, function(v) inst.Light:SetIntensity(v) end)
--[[
        if inst.Light ~= nil then
            inst.Light:Enable(true)
            inst.Light:SetIntensity(.6 * val)
            inst.Light:SetRadius(5 * val)
            inst.Light:SetFalloff(3 * val)
        end
        ]]
    else    
        inst.Light:Enable(false)
    end
end

local function ShakeifClose(inst)
	--inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/trails/bodyfall") 
    ShakeAllCameras(CAMERASHAKE.FULL, 0.5, .03, .5, inst, 30)
end

local function GetExcludeTags(inst)
	if TheNet:GetPVPEnabled() then
		return  {"notarget", "wall", "laserimmune", "playerghost"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion", "INLIMBO", "notarget", "laserimmune", "playerghost"}
	else	
		return {"player", "companion", "INLIMBO", "notarget", "wall", "laserimmune", "playerghost"}
	end
end

local function OnMineCollide(inst, other)
    -- may want to do some charging damage?
end

local function OnHit(inst, dist)    
    inst.AnimState:PlayAnimation("land")
    inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/ribs/step_wires")
    inst.AnimState:PushAnimation("open")
    inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/rust")    
    inst:ListenForEvent("animover", function() 
        if inst.AnimState:IsCurrentAnimation("open") then
            inst.primed  = true
            inst.AnimState:PlayAnimation("green_loop",true)
        end
    end)
end

--TODO: Make this a common fn
local function DoManualAoe(inst, damage, range, stimuli)
	local posx, posy, posz = inst.Transform:GetWorldPosition()
	local ents = TheSim:FindEntities(posx, 0, posz, range and range or 5, { "locomotor"}, GetExcludeTags(inst))
	local targets = {}
	if ents and #ents > 0 then
		for i, v in ipairs(ents) do
			if v:IsValid() and v.components.health and not v.components.health:IsDead() and v.components.combat and v ~= inst then
				table.insert(targets, v)
			end
		end
	end	
	if targets and #targets > 0 then
		for i, v in ipairs(targets) do
			if v:IsValid() and v.components.health and not v.components.health:IsDead() and v.components.combat then
				v.components.combat:GetAttacked(inst.owner and inst.owner or inst, inst.components.combat:CalcDamage(v), nil, stimuli)
				inst:PushEvent("onattackother", { target = v, stimuli = stimuli })
			end
		end	
	end
end

local function onnearmine(inst, ents) 
	if ents then
		for i,ent in ipairs(ents)do
			if not ent:HasTag("ancient_hulk") then
				inst.detonate = true
				break
			end
		end
	end	
    if inst.primed and inst.detonate then
        inst.SetLightValue(inst, 0,0.75,0.2 )
        inst.AnimState:PlayAnimation("red_loop", true)
        --start beep
        inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/active_LP","boom_loop")
        inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/electro")
        inst:DoTaskInTime(0.8,function() 
            --explode, end beep
			inst.SoundEmitter:KillSound("boom_loop")
            ShakeifClose(inst)
            local ring = SpawnPrefab("laser_ringp")
            ring.Transform:SetPosition(inst.Transform:GetWorldPosition())
            inst:DoTaskInTime(0.3,function() DoManualAoe(inst, 100, 5, "explosive") inst:Remove() end)    
            
            local explosion = SpawnPrefab("laser_explosionp")
            explosion.Transform:SetPosition(inst.Transform:GetWorldPosition())
            explosion.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/smash_3")                          
        end)
    end
end

local function OnSave_Mine(inst, data)
	data.permanent = inst.permanent or nil
end

local function OnLoad_Mine(inst, data)
    if data then
        inst.permanent = data.permanent or nil
	end
end

local function minefn(Sim)
    local inst = CreateEntity()
    local trans = inst.entity:AddTransform()
    local anim = inst.entity:AddAnimState()
    local sound = inst.entity:AddSoundEmitter()
	inst.entity:AddNetwork()

    MakeInventoryPhysics(inst, 75, 0.5)

    anim:SetBank("metal_hulk_mine")
    anim:SetBuild("metal_hulk_bomb")
    anim:PlayAnimation("green_loop", true)

    inst:AddTag("ancient_hulk_mine")

    inst.glow = inst.entity:AddLight()    
    inst.glow:SetIntensity(.6)
    inst.glow:SetRadius(2)
    inst.glow:SetFalloff(1)
    inst.glow:SetColour(1, 0.3, 0.3)
    inst.glow:Enable(false)
	
	inst.entity:SetPristine()
	
	if not TheWorld.ismastersim then
        return inst
    end
	
    inst.primed = true
	
	--inst.Physics:SetCollisionCallback(OnMineCollide)

    inst:AddComponent("locomotor")
    inst:AddComponent("complexprojectile")
	inst.SetSource = PlayablePets.SetProjectileSource --needed for Reforged compatibility
    inst.components.complexprojectile:SetOnHit(OnHit)
    inst.components.complexprojectile.yOffset = 4

    inst:AddComponent("combat")
    inst.components.combat:SetDefaultDamage(100)
    --inst.components.combat.playerdamagepercent = .5

    inst:AddComponent("fader")

    inst.SetLightValue = SetLightValue
	
	inst:DoTaskInTime(30, function(inst) if not inst.permanent then inst.detonate = true onnearmine(inst) end end)

    inst:AddComponent("creatureproxp")   
    inst.components.creatureproxp.period = 0.01
    inst.components.creatureproxp:SetDist(3.5,5) 
    inst.components.creatureproxp:SetOnPlayerNear(onnearmine)
	
	inst.OnSave = OnSave_Mine
	inst.OnLoad = OnLoad_Mine

    return inst
end

local function OnHitOrb(inst, dist)    
    ShakeifClose(inst)
    inst.AnimState:PlayAnimation("impact")  
    inst:ListenForEvent("animover", function() 
        if inst.AnimState:IsCurrentAnimation("impact") then
           inst:Remove()
        end
    end)
    local ring = SpawnPrefab("laser_ringp")
    ring.Transform:SetPosition(inst.Transform:GetWorldPosition())     
    inst:DoTaskInTime(0,function() inst.components.combat:DoAreaAttack(inst, 4, nil, nil, nil, GetExcludeTags(inst)) end)
    ring.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/smash_2")    
end

local function orbfn()
    local inst = CreateEntity()
    local trans = inst.entity:AddTransform()
    local anim = inst.entity:AddAnimState()
    local sound = inst.entity:AddSoundEmitter()
	inst.entity:AddNetwork()

    MakeInventoryPhysics(inst, 75, 0.5)
    inst.Physics:ClearCollisionMask()
    inst.Physics:CollidesWith(COLLISION.WORLD)
    --inst.Physics:SetCapsule(physics.radius,physics.radius)

    anim:SetBank("metal_hulk_projectile")
    anim:SetBuild("metal_hulk_projectile")
    anim:PlayAnimation("spin_loop", true)
    inst:AddTag("ancient_hulk_orb")

    inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/gears_LP","gears")   
    inst.SoundEmitter:SetParameter( "gears", "intensity", 1 )

    inst.glow = inst.entity:AddLight()    
    inst.glow:SetIntensity(.6)
    inst.glow:SetRadius(3)
    inst.glow:SetFalloff(1)
    inst.glow:SetColour(1, 0.3, 0.3)
    inst.glow:Enable(true)

	inst.entity:SetPristine()
	
	if not TheWorld.ismastersim then
        return inst
    end

    --inst.Physics:SetCollisionCallback(OnMineCollide)
	
    inst.persists = false

    inst:AddComponent("locomotor")
    inst:AddComponent("complexprojectile")
    inst.components.complexprojectile:SetOnHit(OnHitOrb)
    inst.components.complexprojectile.yOffset = 4
	inst.components.complexprojectile.usehigharc = false

    inst:AddComponent("combat")
    inst.components.combat:SetDefaultDamage(100)
    inst.components.combat.playerdamagepercent = 0.5

    inst:AddComponent("fader")
	
    inst.SetLightValue = SetLightValue
	
	inst.SetSource = PlayablePets.SetProjectileSource --needed for Reforged compatibility

    return inst
end

------------Rock Basalt-----------------------
SetSharedLootTable( 'rock_basaltp',
{
    {'rocks',  1.00},
    {'rocks',  1.00},
    {'rocks',  0.50},
    {'flint',  1.00},
    {'flint',  0.30},
})

local function OnWork(inst, worker, workleft)
    if workleft <= 0 then
        local pt = inst:GetPosition()
        SpawnPrefab("rock_break_fx").Transform:SetPosition(pt:Get())
        inst.components.lootdropper:DropLoot(pt)

        if inst.showCloudFXwhenRemoved then
            local fx = SpawnPrefab("collapse_small")
            fx.Transform:SetPosition(inst.Transform:GetWorldPosition())
        end

		if not inst.doNotRemoveOnWorkDone then
	        inst:Remove()
		end
    else
        inst.AnimState:PlayAnimation(
            (workleft < TUNING.ROCKS_MINE / 3 and "low") or
            (workleft < TUNING.ROCKS_MINE * 2 / 3 and "med") or
            "full"
        )
    end
end

local function OnHit_Rock(inst)
	local health = inst.components.health:GetPercent()
	if health > 0.5 then
		inst.AnimState:PlayAnimation("full", false)
	elseif health > 0.25 and health <= 0.5 then
		inst.AnimState:PlayAnimation("med", false)
	elseif health < 0.25 then
		inst.AnimState:PlayAnimation("low", false)
	end
end

local function OnDeath_Rock(inst)
	RemovePhysicsColliders(inst)
end

local function OnSave_Rock(inst, data)
	
end

local function OnLoad_Rock(inst, data)
    if inst.components.health then
        OnHit_Rock(inst)
	end
end

local function basaltfn(inst)
	local inst = CreateEntity()
	local trans = inst.entity:AddTransform()
	local anim = inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
	inst.entity:AddNetwork()
	local minimap = inst.entity:AddMiniMapEntity()
	minimap:SetIcon( "rock.png" )
	
    MakeObstaclePhysics(inst, 1)
	
    inst:AddTag("rock_basalt")    

	anim:SetBank("rock_basalt")
	anim:SetBuild("rock_basalt")
    -- anim:SetMultColour(.2, 1, .2, 1.0)

    anim:PlayAnimation("full")
	
	inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end
	inst:AddComponent("lootdropper") 
	
	inst:AddComponent("health")
	inst.components.health:SetMaxHealth(300)
	
	inst:AddComponent("combat")

    inst:AddComponent("inspectable")
	
	inst.persists = false
	
	inst:ListenForEvent("attacked", OnHit_Rock)
	inst:ListenForEvent("death", OnDeath_Rock)
	-------------------
	PlayablePets.SetCommonStatResistances(inst, 0, 0, 0) --fire, acid, poison
	inst.debuffimmune = true
    -------------------
	inst.OnLoad = OnLoad_Rock
	inst.OnSave = OnSave_Rock
    
   return inst
end

local function markerfn(Sim)
    local inst = CreateEntity()
    local trans = inst.entity:AddTransform()
	inst.entity:AddNetwork()
	
	inst.entity:SetPristine()
	
	if not TheWorld.ismastersim then
        return inst
    end

    inst.persists = false
    return inst
end

local function fx_fn(inst)
        local inst = CreateEntity()

        inst.entity:AddTransform()
        inst.entity:AddNetwork()
		inst.entity:AddAnimState()
		inst.entity:AddSoundEmitter()
		
		inst.AnimState:SetBank("metal_hulk_ring_fx")
		inst.AnimState:SetBuild("metal_hulk_ring_fx")
		inst.AnimState:PlayAnimation("idle")

        inst:AddTag("FX")

        inst.entity:SetPristine()

        if not TheWorld.ismastersim then
            return inst
        end

        inst.persists = false
        inst:DoTaskInTime(2, inst.Remove)

    return inst
end

local function fx2_fn(inst)
        local inst = CreateEntity()

        inst.entity:AddTransform()
        inst.entity:AddNetwork()
		inst.entity:AddAnimState()
		inst.entity:AddSoundEmitter()
		
		inst.AnimState:SetBank("bearger_ground_fx")
		inst.AnimState:SetBuild("bearger_ground_fx")
		inst.AnimState:PlayAnimation("idle")

        inst:AddTag("FX")

        inst.entity:SetPristine()

        if not TheWorld.ismastersim then
            return inst
        end
		
		inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/hulk_metal_robot/dust")

        inst.persists = false
        inst:DoTaskInTime(2, inst.Remove)

    return inst
end

return Prefab( "common/monsters/ancient_hulk_minep", minefn, assets, prefabs),
       Prefab( "common/monsters/ancient_hulk_orbp", orbfn, assets, prefabs),
       Prefab( "common/monsters/ancient_hulk_markerp", markerfn, assets, prefabs),
	   Prefab( "metal_hulk_ring_fxp", fx_fn, assets, prefabs),
	   Prefab( "groundpound_fx_hulkp", fx2_fn, assets, prefabs),
	   Prefab( "common/monsters/rock_basaltp", basaltfn, assets, prefabs)