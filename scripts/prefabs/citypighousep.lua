require "prefabutil"

--Fix the missing lights.

local assets =
{

}

local prefabs =
{
    --"merm",
    "collapse_big",

    --loot:
    "boards",
    "rocks",
    --"fish",
}

local loot =
{
    "boards",
    "rocks",
    
}

	--[[
	Asset("ANIM", "anim/pig_house_sale.zip"),	
    Asset("ANIM", "anim/player_small_house1.zip"),
    Asset("ANIM", "anim/player_large_house1.zip"),

    Asset("ANIM", "anim/player_large_house1_manor_build.zip"), 
    Asset("ANIM", "anim/player_large_house1_villa_build.zip"),    
    Asset("ANIM", "anim/player_small_house1_cottage_build.zip"),    
    Asset("ANIM", "anim/player_small_house1_tudor_build.zip"),   
    Asset("ANIM", "anim/player_small_house1_gothic_build.zip"),  
    Asset("ANIM", "anim/player_small_house1_brick_build.zip"),  
    Asset("ANIM", "anim/player_small_house1_turret_build.zip"),]]  

local SCALEBUILD ={}
SCALEBUILD["pig_townhouse1_pink_build"] = true
SCALEBUILD["pig_townhouse1_green_build"] = true
SCALEBUILD["pig_townhouse1_white_build"] = true
SCALEBUILD["pig_townhouse1_brown_build"] = true

local SETBANK ={}
SETBANK["pig_farmhouse_build"] = "pig_shop"
SETBANK["pig_townhouse1_pink_build"] = "pig_townhouse"
SETBANK["pig_townhouse1_green_build"] = "pig_townhouse"
SETBANK["pig_townhouse1_white_build"] = "pig_townhouse"
SETBANK["pig_townhouse1_brown_build"] = "pig_townhouse"
SETBANK["pig_townhouse5_beige_build"] = "pig_townhouse5"
SETBANK["pig_townhouse6_red_build"] = "pig_townhouse6"
SETBANK["player_large_house1_manor_build"] = "pig_house_sale"
SETBANK["player_large_house1_villa_build"] = "pig_house_sale"
SETBANK["player_small_house1_cottage_build"] = "pig_house_sale"
SETBANK["player_small_house1_tudor_build"] = "pig_house_sale"
SETBANK["player_small_house1_gothic_build"] = "pig_house_sale"
SETBANK["player_small_house1_brick_build"] = "pig_house_sale"
SETBANK["player_small_house1_turret_build"] = "pig_house_sale"

local house_builds = {
   "pig_farmhouse_build",
   "pig_townhouse1_pink_build",
   "pig_townhouse1_green_build",
   "pig_townhouse1_white_build",
   "pig_townhouse1_brown_build",
   "pig_townhouse5_beige_build",
   "pig_townhouse6_red_build",   
   "player_large_house1_manor_build",
   "player_large_house1_villa_build",
   "player_small_house1_cottage_build",
   "player_small_house1_tudor_build",
   "player_small_house1_gothic_build",
   "player_small_house1_brick_build",
   "player_small_house1_turret_build"
}

local function setScale(inst,build)
    if SCALEBUILD[build] then
        inst.AnimState:SetScale(0.75,0.75,0.75)
    else
        inst.AnimState:SetScale(1,1,1)
    end
end


local function setcolor(inst,num)
    if not num then
        num = math.random()
    end
    local color = 0.5 + num * 0.5
    inst.AnimState:SetMultColour(color, color, color, 1)
    return num
end

--Client update
local function OnUpdateWindow(window, inst, snow)
    if inst:HasTag("burnt") then
        inst._windowsnow = nil
        inst._window = nil
        snow:Remove()
        window:Remove()
    elseif inst.Light:IsEnabled() and inst.AnimState:IsCurrentAnimation("lit") then
        if not window._shown then
            window._shown = true
            window:Show()
            snow:Show()
        end
    elseif window._shown then
        window._shown = false
        window:Hide()
        snow:Hide()
    end
end

local function onhammered(inst, worker)
	if inst.components.sleepingbag then
		inst.components.sleepingbag:DoWakeUp()
	end	
    if inst.components.burnable ~= nil and inst.components.burnable:IsBurning() then
        inst.components.burnable:Extinguish()
    end
	
	 if inst.doortask ~= nil then
        inst.doortask:Cancel()
        inst.doortask = nil
    end
	
    inst:RemoveComponent("childspawner")
    inst.components.lootdropper:DropLoot()
    local fx = SpawnPrefab("collapse_big")
    fx.Transform:SetPosition(inst.Transform:GetWorldPosition())
    fx:SetMaterial("wood")
    inst:Remove()
end

local function onhit(inst, worker)
    if not inst:HasTag("burnt") then 
        inst.AnimState:PlayAnimation("hit")
        if inst.lightson then
            inst.AnimState:PushAnimation("lit")
            if inst._window ~= nil then
                inst._window.AnimState:PlayAnimation("windowlight_hit")
                inst._window.AnimState:PushAnimation("windowlight_idle")
            end
            if inst._windowsnow ~= nil then
                inst._windowsnow.AnimState:PlayAnimation("windowsnow_hit")
                inst._windowsnow.AnimState:PushAnimation("windowsnow_idle")
            end
        else
            inst.AnimState:PushAnimation("idle")
        end
    end
end

local function onbuilt(inst)
    --inst.AnimState:PlayAnimation("place")
    inst.AnimState:PlayAnimation("idle", true)
end

local function LightsOn(inst)
    if not inst:HasTag("burnt") and not inst.lightson then
        inst.Light:Enable(true)
        inst.AnimState:PlayAnimation("lit", true)
        inst.SoundEmitter:PlaySound("dontstarve/pig/pighut_lighton")
        inst.lightson = true
        if inst._window ~= nil then
            inst._window.AnimState:PlayAnimation("windowlight_idle", true)
            inst._window:Show()
        end
        if inst._windowsnow ~= nil then
            inst._windowsnow.AnimState:PlayAnimation("windowsnow_idle", true)
            inst._windowsnow:Show()
        end
    end
end

local function LightsOff(inst)
    if not inst:HasTag("burnt") and inst.lightson then
        inst.Light:Enable(false)
        inst.AnimState:PlayAnimation("idle", true)
        inst.SoundEmitter:PlaySound("dontstarve/pig/pighut_lightoff")
        inst.lightson = false
        if inst._window ~= nil then
            inst._window:Hide()
        end
        if inst._windowsnow ~= nil then
            inst._windowsnow:Hide()
        end
    end
end

local factions = 
{
	"flag_post_royal_build",
	"flag_post_perdy_build",
	"flag_post_duster_build",
	"flag_post_wilson_build",
	--custom ones are below here, mostly for PP Server memes--
}

local function SetFaction(inst, num) --for towers
	if num and factions[num] then
		inst.faction = num
		inst.AnimState:AddOverrideBuild(factions[num])
	end
end

local function onsave(inst, data)
    if inst:HasTag("burnt") or (inst.components.burnable ~= nil and inst.components.burnable:IsBurning()) then
        data.burnt = true
    end
	data.faction = inst.faction and inst.faction or nil
	data.build = inst.setbuild and inst.setbuild or inst.build
    data.animset = inst.setanimset and inst.setanimset or inst.animset
	if inst.illegal then
		data.illegal = inst.illegal
	end
end

local function onload(inst, data)
    if data ~= nil then
		if data.build and inst.prefab == "city_player_house" then
            inst.setbuild = data.build
			inst:DoTaskInTime(0, function(inst)
            inst.AnimState:SetBuild(inst.setbuild)
			setScale(inst,inst.setbuild)
			end)
           
        end

        if data.animset and inst.prefab == "city_player_house" then
            inst.setanimset = data.animset
			inst:DoTaskInTime(0, function(inst)
            inst.AnimState:SetBank(inst.setanimset)
			end)
        end
		
		if data.burnt then
        inst.components.burnable.onburnt(inst)
		end
		
		if data.faction then
			SetFaction(inst, data.faction)
		end
		
		if data.illegal then
			inst.illegal = data.illegal
		end
    end
end

local function onignite(inst)
    inst.components.sleepingbag:DoWakeUp()
end

local function onburntup(inst)
    if inst.doortask ~= nil then
        inst.doortask:Cancel()
        inst.doortask = nil
    end
    if inst.inittask ~= nil then
        inst.inittask:Cancel()
        inst.inittask = nil
    end
    if inst._window ~= nil then
        inst._window:Remove()
        inst._window = nil
    end
    if inst._windowsnow ~= nil then
        inst._windowsnow:Remove()
        inst._windowsnow = nil
    end
end

local function wakeuptest(inst, phase)
   -- if phase ~= inst.sleep_phase then
        --inst.components.sleepingbag:DoWakeUp()
    --end
end

local function onwake(inst, sleeper, nostatechange)
    if inst.sleeptask ~= nil then
        inst.sleeptask:Cancel()
        inst.sleeptask = nil
    end
	
	LightsOff(inst)

    inst:StopWatchingWorldState("phase", wakeuptest)
    sleeper:RemoveEventCallback("onignite", onignite, inst)

    if not nostatechange then
        if sleeper.sg:HasStateTag("tent") then
            sleeper.sg.statemem.iswaking = true
        end
		inst.SoundEmitter:PlaySound("dontstarve/common/pighouse_door")
        sleeper.sg:GoToState("wakeup")
    end

    if inst.sleep_anim ~= nil then
        --inst.AnimState:PushAnimation("idle", true)
    end

    --inst.components.finiteuses:Use()
end

local function onsleeptick(inst, sleeper)
    local isstarving = sleeper.components.beaverness ~= nil and sleeper.components.beaverness:IsStarving()
	
	
    if sleeper.components.hunger ~= nil then
        sleeper.components.hunger:DoDelta(inst.hunger_tick, true, true)
        isstarving = sleeper.components.hunger:IsStarving()
    end

    if sleeper.components.sanity ~= nil and sleeper.components.sanity:GetPercentWithPenalty() < 1 then
        sleeper.components.sanity:DoDelta(TUNING.SLEEP_SANITY_PER_TICK, true)
    end

    if not isstarving and sleeper.components.health ~= nil then
        sleeper.components.health:DoDelta(TUNING.SLEEP_HEALTH_PER_TICK * 2, true, inst.prefab, true)
    end

    if sleeper.components.temperature ~= nil then
        if inst.is_cooling then
            if sleeper.components.temperature:GetCurrent() > TUNING.SLEEP_TARGET_TEMP_TENT then
                sleeper.components.temperature:SetTemperature(sleeper.components.temperature:GetCurrent() - TUNING.SLEEP_TEMP_PER_TICK)
            end
        elseif sleeper.components.temperature:GetCurrent() < TUNING.SLEEP_TARGET_TEMP_TENT then
            sleeper.components.temperature:SetTemperature(sleeper.components.temperature:GetCurrent() + TUNING.SLEEP_TEMP_PER_TICK)
        end
    end

    if isstarving then
        inst.components.sleepingbag:DoWakeUp()
    end
end

local function onsleep(inst, sleeper)
    --inst:WatchWorldState("phase", wakeuptest)
    sleeper:ListenForEvent("onignite", onignite, inst)
	

    --if inst.sleep_anim ~= nil then
        --inst.AnimState:PlayAnimation(inst.sleep_anim, true)
    --end

    if inst.sleeptask ~= nil then
        inst.sleeptask:Cancel()
    end
	
	
	inst.SoundEmitter:PlaySound("dontstarve/common/pighouse_door")
    inst.sleeptask = inst:DoPeriodicTask(TUNING.SLEEP_TICK_PERIOD, onsleeptick, nil, sleeper)
	
end

local function MakeWindow()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()

    inst:AddTag("DECOR")
    inst:AddTag("NOCLICK")
    --[[Non-networked entity]]
    inst.persists = false

    inst.AnimState:SetBank("pig_house")
    inst.AnimState:SetBuild("pig_house")
    inst.AnimState:PlayAnimation("windowlight_idle")
    inst.AnimState:SetLightOverride(.6)
    inst.AnimState:SetBloomEffectHandle("shaders/anim.ksh")
    inst.AnimState:SetFinalOffset(1)

    inst:Hide()

    return inst
end

local function MakeWindowSnow()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()

    inst:AddTag("DECOR")
    inst:AddTag("NOCLICK")
    --[[Non-networked entity]]
    inst.persists = false

    inst.AnimState:SetBank("pig_house")
    inst.AnimState:SetBuild("pig_house")
    inst.AnimState:PlayAnimation("windowsnow_idle")
    inst.AnimState:SetFinalOffset(2)

    inst:Hide()

    MakeSnowCovered(inst)

    return inst
end

local function fn(animset, setbuild, spawnList)
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddMiniMapEntity()
    inst.entity:AddNetwork()
	inst.entity:AddLight()
	inst.entity:AddLightWatcher()
	
	inst.Light:SetFalloff(1)
    inst.Light:SetIntensity(.5)
    inst.Light:SetRadius(1)
    inst.Light:Enable(false)
    inst.Light:SetColour(180/255, 195/255, 50/255)

    MakeObstaclePhysics(inst, 1)

    inst.MiniMapEntity:SetIcon("pighouse.png")
	
	inst.hunger_tick = TUNING.SLEEP_HUNGER_PER_TICK
	
	inst:AddTag("tent")
    inst:AddTag("structure")
	
	
	setScale(inst, inst.build)
	
	MakeSnowCoveredPristine(inst)
	
	inst.entity:SetPristine()
	
	if not TheWorld.ismastersim then
        return inst
    end
	
	inst.sleep_phase = "night"
    
	
	inst:AddComponent("workable")
	inst:AddComponent("sleepingbag")
	--MakeHauntableWork(inst)
    inst.components.sleepingbag.onsleep = onsleep
    inst.components.sleepingbag.onwake = onwake
    --convert wetness delta to drying rate
    inst.components.sleepingbag.dryingrate = math.max(0, -TUNING.SLEEP_WETNESS_PER_TICK / TUNING.SLEEP_TICK_PERIOD)

	
    inst:AddComponent("lootdropper")
    inst.components.lootdropper:SetLoot(loot)
    inst:AddComponent("workable")
    inst.components.workable:SetWorkAction(ACTIONS.HAMMER)
    inst.components.workable:SetWorkLeft(6)
    inst.components.workable:SetOnFinishCallback(onhammered)
    inst.components.workable:SetOnWorkCallback(onhit)
	
	if not inst.build then
	local build = house_builds[math.random(1,#house_builds)]        
    if inst.setbuild then
        build = inst.setbuild
    end
	inst.build = build
	end
	inst.AnimState:SetBuild(inst.build)
	
	--inst.animset = nil

    if inst.setanimset then
		inst.AnimState:SetBank(inst.setanimset)
        inst.animset = inst.setanimset
    else            
        inst.AnimState:SetBank(SETBANK[inst.build])            
        inst.animset = SETBANK[inst.build]
    end
    inst.AnimState:PlayAnimation("idle", true)

	
	--inst:AddComponent("prototyper")
	--inst.components.prototyper.trees = TUNING.PROTOTYPER_TREES.PIG_SHOP_GROCERYP
    --inst:AddComponent("childspawner")
    --inst.components.childspawner.childname = "merm"
    --inst.components.childspawner:SetSpawnedFn(OnSpawned)
    --inst.components.childspawner:SetGoHomeFn(OnGoHome)
    --inst.components.childspawner:SetRegenPeriod(TUNING.TOTAL_DAY_TIME * 4)
    --inst.components.childspawner:SetSpawnPeriod(10)
    --inst.components.childspawner:SetMaxChildren(TUNING.MERMHOUSE_MERMS)

    --inst.components.childspawner.emergencychildname = "merm"
    --inst.components.childspawner:SetEmergencyRadius(TUNING.MERMHOUSE_EMERGENCY_RADIUS)
    --inst.components.childspawner:SetMaxEmergencyChildren(TUNING.MERMHOUSE_EMERGENCY_MERMS)

    inst:AddComponent("hauntable")
    inst.components.hauntable:SetHauntValue(TUNING.HAUNT_SMALL)
    --inst.components.hauntable:SetOnHauntFn(OnHaunt)

    --inst:WatchWorldState("isday", OnIsDay)

    --StartSpawning(inst)
	inst.OnSave = onsave
	inst.OnLoad = onload

    MakeMediumBurnable(inst, nil, nil, true)
    MakeLargePropagator(inst)
    inst:ListenForEvent("onignite", onignite)
    inst:ListenForEvent("burntup", onburntup)

    inst:AddComponent("inspectable")

    MakeSnowCovered(inst)

    return inst
end

local function guardtowerfn()
    local inst = fn()
	inst.AnimState:SetBank("pig_shop")
	inst.AnimState:SetBuild("pig_tower_build")
    
	if not TheWorld.ismastersim then
        return inst
    end
	
	inst:RemoveComponent("burnable")
	inst.SetFaction = SetFaction()
    inst.hunger_tick = TUNING.SLEEP_HUNGER_PER_TICK
    --inst.is_cooling = false

    --inst.components.finiteuses:SetMaxUses(TUNING.TENT_USES)
    --inst.components.finiteuses:SetUses(TUNING.TENT_USES)

    return inst
end

local function cityhallfn()
    local inst = fn()
	inst.AnimState:SetBank("pig_cityhall")
	inst.AnimState:SetBuild("pig_cityhall")
    
	if not TheWorld.ismastersim then
        return inst
    end
	
    inst.hunger_tick = 0
    --inst.is_cooling = false

    --inst.components.finiteuses:SetMaxUses(TUNING.TENT_USES)
    --inst.components.finiteuses:SetUses(TUNING.TENT_USES)

    return inst
end

local function palacefn()
    local inst = fn()
	inst.AnimState:SetBank("palace")
	inst.AnimState:SetBuild("palace")
    
	if not TheWorld.ismastersim then
        return inst
    end
	
    inst.hunger_tick = 0
    --inst.is_cooling = false

    --inst.components.finiteuses:SetMaxUses(TUNING.TENT_USES)
    --inst.components.finiteuses:SetUses(TUNING.TENT_USES)

    return inst
end

return Prefab("city_player_house", fn, assets, prefabs),
	Prefab("city_player_guardtower", guardtowerfn, assets, prefabs),
	Prefab("cityhall", cityhallfn, assets, prefabs),
	Prefab("palace_player", palacefn, assets, prefabs),
	--Prefab("pighousewindowp", windowfn, windowassets),
   -- Prefab("pighousewindowsnow", windowsnowfn, windowassets),
	MakePlacer("city_player_house_placer", "pig_townhouse", "pig_townhouse1_green_build", "idle"),
	MakePlacer("city_player_guardtower_placer", "pig_shop", "pig_tower_build", "idle")
