local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--
---------------------------

local assets = 
{
	Asset("ANIM", "anim/lantern_fly.zip"),
	Asset("ANIM", "anim/glowfly_shiny_build_08.zip"),
	Asset("ANIM", "anim/rabid_beetle_shiny_build_08.zip"),
}

local prefabs = 
{	

}

local start_inv = 
{
	--'prefab',
	--"spear",
}

local start_inv2 =
{
   --"beehome",
}

if MOBHOUSE == "Enable1" or MOBHOUSE == "Enable3" then
	start_inv = start_inv2
end
-----------------------
--Stats--
local mob = 
{
	health = 20,
	hunger = 60,
	hungerrate = 0.075, 
	sanity = 80,
	runspeed = 6,
	walkspeed = 5,
	damage = 20*2,
	attackperiod = 0,
	range = 2,
	hit_range = 3,
	bank = "lantern_fly",
	build = "lantern_fly",
	shiny = "glowfly",

	scale = 0.6,
	stategraph = "SGglowflyp",
	minimap = "glowflyp.tex",
	
}

local mob_adult = 
{
	health = 150,
	hunger = 100,
	hungerrate = TUNING.WILSON_HUNGER_RATE, 
	sanity = 50,
	runspeed = 12,
	walkspeed = 12,
	damage = 20*2,
	attackperiod = 0,
	range = 3,
	hit_range = 3,
	bank = "rabid_beetle",
	build = "rabid_beetle",
	shiny = "rabid_beetle",
	--build2 = "bee_guard_puffy_build",
	scale = 0.6,
	stategraph = "SGrabidbeetlep",
	minimap = "rabidbeetlep.tex",
	
}

--Loot that drops when you die, duh.
SetSharedLootTable('glowflyp',
{
    {'lightbulb', 1},
})

--==============================================
--					Mob Functions
--==============================================
local sounds =
{
	takeoff = "dontstarve/creatures/mosquito/mosquito_takeoff",
	attack = "dontstarve/creatures/mosquito/mosquito_attack",
	buzz = "dontstarve_DLC003/creatures/glowfly/buzz_LP",
	hit = "dontstarve_DLC003/creatures/glowfly/hit",
	death = "dontstarve_DLC003/creatures/glowfly/death",
	explode = "dontstarve/creatures/mosquito/mosquito_explo",
}

local function onattacked(inst, data)
    inst.components.combat:SetTarget(data.attacker)
    inst.components.combat:ShareTarget(data.attacker, 30, function(dude) return dude:HasTag("rabidbeetle") and not dude.components.health:IsDead() end, 30)
end

local function onhitother2(inst, other)
    inst.components.combat:SetTarget(other)
    inst.components.combat:ShareTarget(other, 30, function(dude) return dude:HasTag("rabidbeetle") and not dude.components.health:IsDead() end, 30)
end

--==============================================
--				Custom Common Functions
--==============================================
local function SetAdult(inst)
	PlayablePets.SetCommonStats(inst, mob_adult) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20, 1) --heat, cold, wetness
	inst.Transform:SetFourFaced()
	inst.components.ppskin_manager:LoadSkin(mob, true)
	if inst.Light then
		inst.Light:Enable(false)
	end

	inst.taunt2 = nil
	inst.DynamicShadow:SetSize(2.5, 1.5)
	ChangeToCharacterPhysics(inst)
	PlayablePets.RemoveAmphibious(inst)
end

local function SetSkinDefault(inst, data)
	if data then
		if inst.isadult then
			inst.AnimState:SetBuild(data.adult_build)
		else
			inst.AnimState:SetBuild(data.build)
		end
	else
		if inst.isadult then
			inst.AnimState:SetBuild(mob_adult.build)
		else
			inst.AnimState:SetBuild(mob.build)
		end
	end
end
--==============================================
--					Loading/Saving
--==============================================
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
		inst.isadult = data.isadult or nil
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
	data.isadult = inst.isadult or nil
end

--==============================================
--					Forged Forge
--==============================================
local function DoHealBuff(target)
    if target:IsValid() and not target.components.health:IsDead() and not target.buffimmune and not target.defbuffed and target.components.debuffable then
		
	end
end

local function DoHealPulse(inst)
	--OnPulse(inst)
	if inst.components.health and not inst.components.health:IsDead() then
    local x, y, z = inst.Transform:GetWorldPosition()
    local ents = TheSim:FindEntities(x, y, z, 4, nil, { "playerghost", "ghost", "battlestandard", "INLIMBO", "LA_MOB" } )
		if #ents > 1 then
			for i, v in ipairs(ents) do
				if v and v.components.health and not v.components.health:IsDead() then
					v.components.health:DoDelta(2, false)
				end
			end
		end
	end
end

local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, PPHAM_FORGE.GLOWFLY)
	
	inst.mobsleep = false

	inst.taunt2 = false
	
	inst:DoTaskInTime(0, function(inst)
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"books", "staves", "darts", "melees"})
	end)
	
	inst:DoPeriodicTask(0.5, DoHealPulse)
	
	--inst.components.combat.onhitotherfn = OnForgeHitOther
	
	inst.components.combat:SetDamageType(1)
	
	inst.components.revivablecorpse.revivespeedmult = 0.3
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

local function GetMobTable(inst)
	if inst.isadult then
		SetAdult(inst)
		return mob_adult
	else
		return mob
	end
end

local function ondeath(inst)
	inst.isadult = nil
end

--==============================================
--					Common/Master
--==============================================
local INTENSITY = .75

local common_postinit = function(inst) 
	inst.MiniMapEntity:SetIcon(mob.minimap)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false) --PP doesn't work with movement prediction enabled, due to use of custom stategraphs
   end
end)
	----------------------------------
	--Tags--
    inst:AddTag("glowfly")
	inst:AddTag("flying")	
	----------------------------------
	local light = inst.entity:AddLight()
    light:SetFalloff(.7)
    light:SetIntensity(INTENSITY)
    light:SetRadius(2)
    light:SetColour(120/255, 120/255, 120/255)
    light:Enable(true)

	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst)
	
end

local master_postinit = function(inst)
	--Stats--
	inst.sounds = sounds
	PlayablePets.SetCommonStats(inst, mob, nil, true) --mob table, ishuman, ignorepvpmultiplier
	inst.components.health:StartRegen(3, 2)
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst, 3, 1, 3) --fire, acid, poison
	
	----------------------------------
	--Variables	
	inst.mobsleep = true
	inst.taunt = true
	inst.taunt2 = true

	inst.components.ppskin_manager:SetSkinDefaultFn(SetSkinDefault)

	local body_symbol = "eyes"
	inst.poisonsymbol = body_symbol
	MakeSmallBurnableCharacter(inst, body_symbol)
    MakeTinyFreezableCharacter(inst, body_symbol)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	----------------------------------
	--Components
	PlayablePets.SetCommonLootdropper(inst, "glowflyp") --prefaboverride
	----------------------------------
	--Eater--
    inst.components.eater:SetAbsorptionModifiers(1,1,1) --This multiplies food stats.
	inst.components.eater:SetAbsorptionModifiers(1,1.75,1) --This multiplies food stats.
	inst.components.eater:SetCanEatHorrible() --check to make them eat monster meat.
	inst.components.eater.strongstomach = true -- can eat monster meat!
			--inst.components.eater.eatwholestack = true --should eat the entire stack and get all the benefits.
	inst.components.eater.ignoresspoilage = true -- Can eat spoiled food without consequence. Might keep food stats as if fresh.
	---------------------------------
	--Physics and Shadows--
	MakeCharacterPhysics(inst, 1, .2)
	inst.Transform:SetSixFaced()
	
	inst.DynamicShadow:SetSize(1, 0.75)
	--inst.DynamicShadow:Enable(false)
    
	inst.Physics:SetCollisionGroup(COLLISION.FLYERS)
	inst.Physics:CollidesWith(COLLISION.FLYERS)	    
	
	PlayablePets.SetAmphibious(inst, nil, nil, true)
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	inst:ListenForEvent("death", ondeath)
	---------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
    ------------------------------------------------------
	--Respawning and Initializing functions--

	inst.SoundEmitter:PlaySound(inst.sounds.buzz, "buzz")

	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, GetMobTable(inst), true, true, nil, nil, true) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, GetMobTable(inst)) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, GetMobTable(inst)) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, GetMobTable(inst)) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, GetMobTable(inst)) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, GetMobTable(inst), true, true, nil, nil, true) ondeath(inst) end)
    end)
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
    return inst	
end

return MakePlayerCharacter("glowflyp", prefabs, assets, common_postinit, master_postinit, start_inv)