local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--
---------------------------
local prefabname = "citypig_thiefp"
--Don't add assets unless absolutely needed.
local assets = 
{
	Asset("ANIM", "anim/townspig_sneaky.zip"),
}
--Might be completely pointless here. I don't know.
local prefabs = 
{	

}
	
--Note: Becareful when adding starting items.
--People are likely to spam changecharacter to get more of them.	
local start_inv = 
{
	--'prefab',
	--"spear",
}

local start_inv2 =
{
   --"beehome",
}

if MOBHOUSE == "Enable1" or MOBHOUSE == "Enable3" then
	start_inv = start_inv2
end
-----------------------
--Stats--
local mob = 
{
	health = TUNING[string.upper(prefabname.."_HEALTH")],
	hunger = TUNING[string.upper(prefabname.."_HUNGER")],
	hungerrate = TUNING.WILSON_HUNGER_RATE, 
	sanity = TUNING[string.upper(prefabname.."_SANITY")],
	runspeed = 7,
	walkspeed = 3,
	damage = 20*2,
	range = 2,
	hit_range = 3,
	attackperiod = 0,
	bank = "townspig",
	build = "pig_bandit",
	shiny = "pig_bandit",
	--build2 = "bee_guard_puffy_build",
	scale = 1,
	stategraph = "SGcitypig_theifp",
	minimap = "none.tex",	
}
-----------------------
--Loot that drops when you die, duh.
SetSharedLootTable('citypig_malep',
{
    {'meat',  1.00},
	{'pigskin',  1.00},
})

local function OnDoneTalking(inst)
    if inst.talktask ~= nil then
        inst.talktask:Cancel()
        inst.talktask = nil
    end
    inst.SoundEmitter:KillSound("talk")
end

local function OnTalk(inst, script, mood)
	inst.SoundEmitter:PlaySound("dontstarve/pig/grunt")
end

local function GetExcludeTags(inst)
	if TheNet:GetPVPEnabled() then
		return {"lightningimmune", "lightningrod", "notarget"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion", "lightningimmune", "lightningrod", "notarget"}
	else	
		return {"player", "companion", "lightningimmune", "lightningrod", "notarget"}
	end
end

local function onattacked(inst, data)
    inst.components.combat:SetTarget(data.attacker)
    inst.components.combat:ShareTarget(data.attacker, 30, function(dude) return dude:HasTag("guard") and not dude.components.health:IsDead() end, 30)
end

local function IsCurrency(item)
    return item:HasTag("currency")
end

local function onhitother(inst, other) 
	if other and other.components.inventory then
		local money = other.components.inventory:FindItems(IsCurrency)
		for k,v in pairs(money) do
			--Why are you even trying to get the owner's inventory? Just drop it!
			local owner = v.components.inventoryitem.owner
			other.components.inventory:DropItem(v, false, true)
			v:DoTaskInTime(0, function() Launch(v, owner, 5) end)
		end
	end
end

local function onhitother2(inst, other)
    inst.components.combat:SetTarget(other)
    inst.components.combat:ShareTarget(other, 30, function(dude) return dude:HasTag("pig") and not dude.components.health:IsDead() end, 30)
end

local function EquipHat(inst) --If a hat removes the head, this fixes that.
    local head = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HEAD)
	if head then
		inst.AnimState:Show("HEAD")
		if inst.hashat and inst.hashat == true then
			inst.AnimState:Hide("headtopper")
		end
	end
end

local function UnEquipHat(inst) --If a hat removes the head, this fixes that.
    local head = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HEAD) or nil
	if not head then
		inst.AnimState:Show("HEAD")
		if inst.hashat and inst.hashat == true then
			inst.AnimState:Show("headtopper")
		end
	end
end

local function FindHideyHoles(inst, pos) --verify if your hideyhole exists onload, if no then remove it
	if pos then
		local ents = TheSim:FindEntities(pos.x, 0, pos.z, 1, {"hideout"})
		if #ents == 0 then
			if TheWorld:HasTag("Caves") then
				inst.hideyhole_caves = nil
			else
				inst.hideyhole = nil
			end
		else
			--print("DEBUG: Hideyhole found! Keeping!")
		end
	end
end
------------------------------------------------------
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
		inst.hideyhole = data.hideyhole or nil
		inst.hideyhole_caves = data.hideyhole_caves or nil
		if inst.hideyhole or inst.hideyhole_caves then
			FindHideyHoles(inst, TheWorld:HasTag("Caves") and inst.hideyhole_caves or inst.hideyhole)
		end
	end
end

local function OnSave(inst, data)
	data.hideyhole = inst.hideyhole or nil
	data.hideyhole_caves = inst.hideyhole_caves or nil
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
end
-------------------------------------------------------
local common_postinit = function(inst) 
	inst.MiniMapEntity:SetIcon(mob.minimap)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false)
	  --ThePlayer.HUD.controls.status.brain:Hide()
		if MONSTERHUNGER == "Disable" then
			--ThePlayer.HUD.controls.status.stomach:Hide()
		end
   end
end)
	inst.components.talker.symbol = "head_base"
	inst.components.talker.mod_str_fn = string.utf8upper
	--inst.components.talker.ontalk = ontalk
	--inst.components.talker.offset = Vector3(0,-900,0)	
end

-------------Forge------------------------------------------
local ex_fns = require "prefabs/player_common_extensions"

local T3 =
{
	"bacontome",
	"splintmail",
	"steadfastarmor",
	"steadfastgrandarmor",
	"jaggedgrandarmor",
	"silkengrandarmor",
	"whisperinggrandarmor",
	"flowerheadband",
	"noxhelm",
	"resplendentnoxhelm",
	"blossomedwreath",
	"clairvoyantcrown",
	"spiralspear",
	"infernalstaff"

}

local T2 =
{
	"forginghammer",
	"moltendarts",
	"pithpike",
	"reedtunic",
	"featheredwreath",
	"jaggedarmor",
	"silkenarmor",	
	"barbedhelm",
	"wovengarland",
	
}

local T1 =
{
	"petrifyingtome",
	"forgedarts",
	"reedtunic",
	"crystaltiara",
	"forge_woodarmor",
}

local T1_CHANCE = 60
local T2_CHANCE = 90
local T3_CHANCE = 100

local function ForgeSpecialAttack(inst)
	local roll1 = math.random(1, 100)
	local roll2 = math.random(1, 100)
	local roll3 = math.random(1, 100)
	
	--Leo:okay, this is dumb. Why can't I think of the correct away to do this.
	if roll1 and math.random(1, 100) <= 75 then
		if roll1 <= T1_CHANCE then
			inst.item1 = T1[math.random(1, #T1)]
		elseif roll1 > T1_CHANCE and roll1 <= T2_CHANCE then
			inst.item1 = T2[math.random(1, #T2)]
		elseif roll1 > T2_CHANCE then
			inst.item1 = T3[math.random(1, #T3)]
		end	
	end	
	
	if roll2 and math.random(1, 100) <= 50 then
		if roll2 <= T1_CHANCE then
			inst.item2 = T1[math.random(1, #T1)]
		elseif roll2 > T1_CHANCE and roll1 <= T2_CHANCE then
			inst.item2 = T2[math.random(1, #T2)]
		elseif roll2 > T2_CHANCE then
			inst.item2 = T3[math.random(1, #T3)]
		end	
	end	
	
	if roll3 and math.random(1, 100) <= 10 then
		if roll3 <= T1_CHANCE then
			inst.item3 = T1[math.random(1, #T1)]
		elseif roll3 > T1_CHANCE and roll1 <= T2_CHANCE then
			inst.item3 = T2[math.random(1, #T2)]
		elseif roll3 > T2_CHANCE then
			inst.item3 = T3[math.random(1, #T3)]
		end	
	end	
	----print(inst.item1 or "FAILED"..", "..inst.item2 or "FAILED"..", "..inst.item3 or "FAILED")
	
	if inst.item1 then
		local item = SpawnPrefab(inst.item1)
		item.Transform:SetPosition(inst:GetPosition():Get())
		Launch(item, inst, 2)
	end
	if inst.item2 then
		local item = SpawnPrefab(inst.item2)
		item.Transform:SetPosition(inst:GetPosition():Get())
		Launch(item, inst, 2)
	end
	if inst.item3 then
		local item = SpawnPrefab(inst.item3)
		item.Transform:SetPosition(inst:GetPosition():Get())
		Launch(item, inst, 2)
	end
	
	inst.item1 = nil
	inst.item2 = nil
	inst.item3 = nil
end

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, PPHAM_FORGE.CITYPIG)
	
	inst.mobsleep = false
	inst.taunt = false --this prevents mobs from targeting you, we don't want that.
	--inst.acidmult = 1.25
	
	inst:DoTaskInTime(0, function(inst)
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"books", "staves"})
	end)
	
	inst.ForgeSpecialAttack = ForgeSpecialAttack
	
	inst.components.combat:SetDamageType(1)
	
	inst.components.revivablecorpse.revivespeedmult = 1
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end
-----------------------------------------------------------------------

local master_postinit = function(inst)
	------------------------------------------
	--Stats--
    PlayablePets.SetCommonStats(inst, mob, nil, true) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20, 1) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst) --fire, acid, poison	
	----------------------------------
	--Loot drops--
	inst:AddComponent("lootdropper")
    inst.components.lootdropper:SetChanceLootTable("citypig_thiefp")
	----------------------------------
	--Tags--
    inst:AddTag("pigtheif")
	inst:AddTag("noplayerindicator")
	inst:AddTag("civilised")
	
	inst.AnimState:Hide("hat")
	inst.AnimState:Hide("desk")
	inst.AnimState:Hide("ARM_carry")
	inst.AnimState:Show("headtopper")
	
	inst.mobsleep = true
	--inst.specialsleep = true
	inst.taunt = true
	inst.taunt2 = true
	inst.taunt3 = true
	
	--inst.mobplayer = true
	inst.ghostbuild = "ghost_monster_build"

	inst.isshiny = 0
	
	inst.female = false
	inst.hashat = true
	
    local body_symbol = "pig_torso"
	inst.poisonsymbol = body_symbol
	MakeMediumBurnableCharacter(inst, body_symbol)
    MakeMediumFreezableCharacter(inst, body_symbol)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	----------------------------------
	--Eater--

	---------------------------------
	--Physics and Scale--
	
	inst.DynamicShadow:SetSize(1.5, 0.75)

    MakeCharacterPhysics(inst, 50, .5)
    inst.Transform:SetScale(mob.scale, mob.scale, mob.scale)
    inst.Transform:SetFourFaced()
	---------------------------------
	--Listeners--
	inst.components.talker.symbol = "head_base"
	inst:ListenForEvent("equip", EquipHat) --Shows head when hats make heads disappear.
	inst:ListenForEvent("unequip", UnEquipHat) --Shows head when hats make heads disappear.
	
	inst:ListenForEvent("ontalk", OnTalk)
    inst:ListenForEvent("donetalking", OnDoneTalking)
	--inst:ListenForEvent("attacked", onattacked)
	inst.components.combat.onhitotherfn = onhitother
	---------------------------------
	--Functions that saves and loads data.

    inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
	--Forge--
	if inst.components.revivablecorpse then
		inst.forge_fn = SetForge(inst)
	end
    ------------------------------------------------------
	--Light and Character Functions. Don't Touch.--
	--inst.components.talker.ontalk = OnTalk
	--inst.components.talker.offset = Vector3(0,-900,0)	
	
	inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman, amphibious)
    end)
	
    return inst
	
end
--[[
if SKIN_RARITY_COLORS.ModMade ~= nil then
AddModCharacterSkin("beeguardp", 1, {normal_skin = "bee_guard_shiny_build_01", ghost_skin = "ghost_monster_build"}, {"bearger_shiny_build_01", "ghost_monster_build"}, {"BEARPLAYER", "FORMAL"})
end]]

return MakePlayerCharacter("citypig_thiefp", prefabs, assets, common_postinit, master_postinit, start_inv)
