local assets =
{
    Asset("ANIM", "anim/ash.zip"),
}

local function CreateSpiritAsh(prefab, fname, count, data)
	local data = data or {}
	local function fn()
		local inst = CreateEntity()

		inst.entity:AddTransform()
		inst.entity:AddAnimState()
		inst.entity:AddSoundEmitter()
		inst.entity:AddNetwork()

		MakeInventoryPhysics(inst)

		inst.AnimState:SetBank("ashes")
		inst.AnimState:SetBuild("ash")
		inst.AnimState:PlayAnimation("idle")

		inst:AddTag("spirit_ash")

		--Sneak these into pristine state for optimization
		inst:AddTag("_named")

		inst.entity:SetPristine()

		if not TheWorld.ismastersim then
			return inst
		end

		PlayablePets.Spirify(inst, 1)

		inst.count = count or 1
		inst.ashprefab = prefab
		inst.stat_mults = {
			damage = data.damage or 1,
			hp = data.hpmult or 1,
		}
		inst.onsummoned_fn = data.onsummoned_fn or nil

		--Remove these tags so that they can be added properly when replicating components below
		inst:RemoveTag("_named")
		---------------------------------

		inst:AddComponent("inspectable")

		inst:AddComponent("inventoryitem")

		inst:AddComponent("named")
		inst.components.named.nameformat = (fname or STRINGS.NAMES[prefab]).." Spirit Ashes"

		inst:AddComponent("tradable")

		inst:AddComponent("hauntable")

		return inst
	end
	return Prefab("spiritash_"..prefab, fn, assets)
end

return CreateSpiritAsh("hound", nil, 3),
CreateSpiritAsh("bearger")
