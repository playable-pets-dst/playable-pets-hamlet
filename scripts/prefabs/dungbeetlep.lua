local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--
---------------------------

local assets = 
{
	Asset("ANIM", "anim/dung_beetle_basic.zip"),
	Asset("ANIM", "anim/dung_beetle_build.zip"),
	--skins--
	Asset("ANIM", "anim/dung_beetle_jeremy_build.zip"),
}

local prefabs = 
{	

}

local start_inv = 
{
	--'prefab',
	--"spear",
}

local start_inv2 =
{
   --"beehome",
}

if MOBHOUSE == "Enable1" or MOBHOUSE == "Enable3" then
	start_inv = start_inv2
end
-----------------------
--Stats--
local prefabname = "dungbeetlep"

local mob = 
{
	health = TUNING[string.upper(prefabname.."_HEALTH")],
	hunger = TUNING[string.upper(prefabname.."_HUNGER")],
	hungerrate = TUNING.WILSON_HUNGER_RATE, 
	sanity = TUNING[string.upper(prefabname.."_SANITY")],
	
	runspeed = 6,
	walkspeed = 3.5,
	
	attackperiod = 0,
	damage = 50*2,
	range = 3,
	hit_range = 4,
	
	bank = "dung_beetle",
	build = "dung_beetle_build",
	--build2 = "bee_guard_puffy_build",
	shiny = "dungbeetle",
	
	scale = 1,
	stategraph = "SGdungbeetlep",
	minimap = "dungbeetlep.tex",	
}

--Loot that drops when you die, duh.
SetSharedLootTable( 'dungbeetlep',
{
    {'monstermeat',  1},
})

local POOPBALL_MAXHEALTH = 300

--==============================================
--					Mob Functions
--==============================================
local beetlesounds = 
{
    scream = "dontstarve_DLC003/creatures/dungbeetle/scream",
    hurt = "dontstarve_DLC003/creatures/dungbeetle/hit",
}

local function falloffdung(inst)
    inst:PushEvent("bumped")
end

local function OnAttacked(inst, data)
    local freezetask = inst:DoTaskInTime(1, function() 
        if inst:HasTag("hasdung") and not inst.components.freezable:IsFrozen() then
            falloffdung(inst)        
        end
    end)
end

local SHAKE_DIST = 40

local function HitShake(inst)
   ShakeAllCameras(CAMERASHAKE.SIDE, .5, .05, .1, inst, 40)
end

local function ClearRecentlyCharged(inst, other)
    inst.recentlycharged[other] = nil
end

local function onothercollide(inst, other)
    if not other:IsValid() then
        return
    elseif (other:HasTag("tree") or other:HasTag("wall")) and TheNet:GetServerGameMode() ~= "lavaarena" then
        HitShake(inst)    
        falloffdung(inst)
    elseif not inst.recentlycharged[other] and other.components.health ~= nil and not other.components.health:IsDead() and not (other:HasTag("tree") or other:HasTag("wall")) and not (TheNet:GetServerGameMode() == "lavaarena" and (other:HasTag("player") or other:HasTag("companion")))then
		ShakeAllCameras(CAMERASHAKE.SIDE, .5, .05, .1, inst, 40)
        inst.recentlycharged[other] = true
        inst:DoTaskInTime(1, ClearRecentlyCharged, other)
        other.components.combat:GetAttacked(inst, mob.damage/(other:HasTag("player") and 2 or 1))
		if other.components.sanity then
			other.components.sanity:DoDelta(-5, false)
		end
		if other:HasTag("flippable") then
			other:PushEvent("flipped")
		end	
		if (other:HasTag("epic") or other:HasTag("largecreature")) and TheNet:GetServerGameMode() ~= "lavaarena" then
			HitShake(inst)    
			falloffdung(inst)
		end
    end
end

local function oncollide(inst, other)
    if inst.sg:HasStateTag("running") and inst:HasTag("hasdung")  then
        if other then
			if not (other ~= nil and other:IsValid() and inst:IsValid()) and (TheNet:GetPVPEnabled()) and other:HasTag("player") then
				return
			else
				
				inst:DoTaskInTime(2 * FRAMES, onothercollide, other)
			end
        end 
    end

end

--==============================================
--				Custom Common Functions
--==============================================
local function redirecthealth(inst, amount, overtime, cause, ignore_invincible, afflicter, ignore_absorb)
    if amount < 0 then
		if inst:HasTag("hasdung") then
			inst.poopball_hp = inst.poopball_hp + amount
			----print("DEBUG: poopball now has "..inst.poopball_hp)
			if inst.poopball_hp <= 0 then
				inst:PushEvent("balldestroyed")
			end
			return true
		end		      
    end    
end

--==============================================
--					Loading/Saving
--==============================================
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
		if data._isonpoopball and data._isonpoopball == true then --I don't trust players to have the tag at this point
			inst:AddTag("hasdung")
			inst:DoTaskInTime(2, function(inst) inst.components.locomotor:SetExternalSpeedMultiplier(inst, 1.2, 1.2) end)			
			inst.poopball_hp = data.poopball_hp or POOPBALL_MAXHEALTH 
		end
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
	if inst:HasTag("hasdung") then
		data._isonpoopball = true
		data.poopball_hp = inst.poopball_hp or 0
	end
end

--==============================================
--					Forged Forge
--==============================================


local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, PPHAM_FORGE.DUNGBEETLE)
	
	inst.mobsleep = false
	inst.taunt = false
	
	--inst.acidmult = 1.25
	
	
	inst:DoTaskInTime(0, function(inst)	
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"books", "staves", "darts", "melees"})
	end)
	
	inst:DoTaskInTime(5, function(inst)
		local ball = SpawnPrefab("dungballp")
		ball.Transform:SetPosition(inst:GetPosition():Get())
		inst.dung_target = ball
		inst.shouldwalk = false
		inst.sg:GoToState("jump_ball", ball)	
	end)
	
	
	--inst:ListenForEvent("attacked", OnAttacked_Forge) --Shows head when hats make heads disappear.
	--inst.components.combat.onhitotherfn = OnForgeHitOther
	
	inst.components.combat:SetDamageType(1)
	
	--inst.components.revivablecorpse.revivespeedmult = 0.3
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

--==============================================
--					Common/Master
--==============================================

local common_postinit = function(inst) 
	inst.MiniMapEntity:SetIcon(mob.minimap)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false) --PP doesn't work with movement prediction enabled, due to use of custom stategraphs
   end
end)

	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst)
	
end



local master_postinit = function(inst)
	--Stats--
	PlayablePets.SetCommonStats(inst, mob, nil, true) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20) --heat, cold, wetness	
	inst.components.health.redirect = redirecthealth
	----------------------------------
	--Tags--
    inst:RemoveTag("character")
	inst:AddTag("dungbeetle")
	
	----------------------------------
	--Variables	
	inst.mobsleep = true
	inst.taunt = true
	inst.taunt2 = true
	inst.shouldwalk = true
	
	inst.poopball_hp = 0
	
	inst.recentlycharged = {}
	
	local body_symbol = "body"
	inst.poisonsymbol = body_symbol
	MakeSmallBurnableCharacter(inst, body_symbol)
    MakeTinyFreezableCharacter(inst, body_symbol)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	inst.sounds = beetlesounds
	PlayablePets.SetCommonStatResistances(inst, 3, 3, 3) --fire, acid, poison
	----------------------------------
	--Components
	PlayablePets.SetCommonLootdropper(inst, prefabname) --inst, prefaboverride
	

	----------------------------------
	--Eater--
	inst.components.eater:SetDiet({ FOODTYPE.POOP, FOODTYPE.POOP }, { FOODTYPE.POOP, FOODTYPE.POOP }) 
    inst.components.eater:SetAbsorptionModifiers(1,1,1) --This multiplies food stats.
	inst.components.eater:SetCanEatHorrible() --check to make them eat monster meat.
	inst.components.eater.strongstomach = true -- can eat monster meat!
	--inst.components.eater.eatwholestack = true --should eat the entire stack and get all the benefits.
	inst.components.eater.ignoresspoilage = true -- Can eat spoiled food without consequence. Might keep food stats as if fresh.
	---------------------------------
	--Physics and Shadows--
	MakeCharacterPhysics(inst, 1, .5)
	inst.Transform:SetSixFaced()
	
	inst.DynamicShadow:SetSize(2, 0.5)
	inst.Physics:SetCollisionCallback(oncollide)
	--inst.DynamicShadow:Enable(false)  
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	
	---------------------------------
	--Functions

    
	---------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
    ------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob) end)
    end)
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
    return inst	
end

return MakePlayerCharacter("dungbeetlep", prefabs, assets, common_postinit, master_postinit, start_inv)