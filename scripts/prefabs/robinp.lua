local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--
---------------------------

local assets = 
{
	Asset("ANIM", "anim/ro_bin.zip"),
    Asset("ANIM", "anim/ro_bin_water.zip"),
    Asset("ANIM", "anim/ro_bin_build.zip"),
}

local prefabs = 
{	

}

local start_inv = 
{
	--'prefab',
	--"spear",
}

local start_inv2 =
{
   --"beehome",
}

if MOBHOUSE == "Enable1" or MOBHOUSE == "Enable3" then
	start_inv = start_inv2
end

local prefabname = "robinp"
-----------------------
--Stats--
local mob = 
{
	health = 450,
	hunger = 300,
	hungerrate = 0.015, 
	sanity = 100,
	
	runspeed = 10 * 0.7,
	walkspeed = 5,
	
	attackperiod = 0,
	damage = 15,
	range = 3,
	hit_range = 3,
	
	bank = "ro_bin",
	build = "ro_bin_build",
	--build2 = "bee_guard_puffy_build",
	shiny = "ro_bin",
	
	scale = 1,
	stategraph = "SG"..prefabname,
	minimap = prefabname..".tex",	
}

--Loot that drops when you die, duh.
SetSharedLootTable( prefabname,
{
	{'meat',     1.0},
	{'meat',     1.0},
	{'meat',     1.0},
	{'meat',     1.0}
})


--==============================================
--					Mob Functions
--==============================================


--==============================================
--				Custom Common Functions
--==============================================

--==============================================
--					Loading/Saving
--==============================================
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
end

--==============================================
--					Forged Forge
--==============================================


local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, PPHAM_FORGE.ROBIN)
	
	inst.mobsleep = nil
	
	inst:DoTaskInTime(0, function(inst)
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({})
	end)
	
	inst.components.combat:AddDamageBuff("atk_banner", 0.5, false)
	
	inst.components.combat:SetDamageType(1)
	
	inst.components.revivablecorpse.revivespeedmult = 1.5
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

--==============================================
--					Common/Master
--==============================================

local common_postinit = function(inst) 
	inst.MiniMapEntity:SetIcon(mob.minimap)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false) --PP doesn't work with movement prediction enabled, due to use of custom stategraphs
   end
end)

	--inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	--inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	--inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	--inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	--inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	--inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	--PlayablePets.SetNightVision(inst)
	
end



local master_postinit = function(inst)
	--Stats--
	PlayablePets.SetCommonStats(inst, mob, nil, true) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst, 1, 1, 1) --fire, acid, poison
	----------------------------------
	--Tags--
	inst:AddTag("ro_bin")
	
	----------------------------------
	--Variables	
	inst.mobsleep = true
	inst.taunt = true
	--inst.taunt2 = true
	
	inst.shouldwalk = true
	
	inst.poisonsymbol = "robin_body"
	MakeMediumBurnableCharacter(inst, inst.poisonsymbol)
	MakeLargeFreezableCharacter(inst, inst.poisonsymbol)
	
	----------------------------------
	--Components
	PlayablePets.SetCommonLootdropper(inst, prefabname) --prefaboverride
	----------------------------------
	--Eater--

    inst.components.eater:SetAbsorptionModifiers(1,3,1) --This multiplies food stats.
	inst.components.eater.eatwholestack = true --should eat the entire stack and get all the benefits.
	inst.components.eater.ignoresspoilage = true -- Can eat spoiled food without consequence. Might keep food stats as if fresh.
	---------------------------------
	--Physics and Shadows--
	MakeCharacterPhysics(inst, 75, .5)
	inst.Transform:SetFourFaced()
	
	inst.DynamicShadow:SetSize(2, 1.5)
	--inst.DynamicShadow:Enable(false)

	PlayablePets.SetAmphibious(inst, mob.bank, mob.bank.."_water", nil, nil, true)
	if inst.components.amphibiouscreature then
		--fixes animation issue
		inst.components.amphibiouscreature:SetEnterWaterFn(function(inst) inst.sg:GoToState("idle") end)
		inst.components.amphibiouscreature:SetExitWaterFn(function(inst) inst.sg:GoToState("idle") end)
	end
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	---------------------------------
	--Functions

    
	---------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
    ------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman, amphibious)
    end)
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
    return inst	
end

return MakePlayerCharacter(prefabname, prefabs, assets, common_postinit, master_postinit, start_inv)