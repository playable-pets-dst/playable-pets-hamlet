local assets =
{
   Asset("ANIM", "anim/pig_coin_oinc.zip"),
   Asset("ANIM", "anim/pig_coin_silver.zip"),
   Asset("ANIM", "anim/pig_coin_jade.zip"),
   Asset( "IMAGE", "images/inventoryimages/oincp.tex" ),
   Asset( "ATLAS", "images/inventoryimages/oincp.xml" ),
   Asset( "IMAGE", "images/inventoryimages/oinc10p.tex" ),
   Asset( "ATLAS", "images/inventoryimages/oinc10p.xml" ),
   Asset( "IMAGE", "images/inventoryimages/oinc100p.tex" ),
   Asset( "ATLAS", "images/inventoryimages/oinc100p.xml" ),
}


local function shine(inst)
    inst.task = nil
    -- hacky, need to force a floatable anim change
	if inst.components.floatable then
		inst.components.floatable:UpdateAnimations("idle_water", "idle")
		inst.components.floatable:UpdateAnimations("sparkle_water", "sparkle")

		if inst.components.floatable.onwater then
			inst.AnimState:PushAnimation("idle_water")
		else
			inst.AnimState:PushAnimation("idle")
		end
	end
    
    if inst.entity:IsAwake() then
        inst:DoTaskInTime(4+math.random()*5, function() shine(inst) end)
    end
end

local function onwake(inst)
    inst.task = inst:DoTaskInTime(4+math.random()*5, function() shine(inst) end)
end

local function MakeCoin(id)
	local ids = tostring(id)
    local function fn()
        local inst = CreateEntity()

        inst.entity:AddTransform()
        inst.entity:AddAnimState()
        inst.entity:AddSoundEmitter()
        inst.entity:AddNetwork()
		
		if CurrentRelease.GreaterOrEqualTo(ReleaseID.IDs.R08_ROT_TURNOFTIDES) then
			MakeInventoryFloatable(inst)
		end	

        inst.AnimState:SetBank("coin")
		inst.AnimState:SetBuild(ids == "10" and "pig_coin_silver" or "pig_coin_oinc")
		if ids == "100" then
			inst.AnimState:SetBuild("pig_coin_jade")
		end	
		inst.AnimState:PlayAnimation("idle")
		
		--if id == 4 then
			--inst.AnimState:PlayAnimation("opal", true)
			--inst:Show("coin01")
		--end
		
		if ids == "10" then
			inst:AddTag("hightier")
		else
			inst:AddTag("commontier") --lowest tier	
		end

        inst:AddTag("hamlet_coin")
		inst:AddTag("currency")
		inst:AddTag("ppcurrency")

        MakeInventoryPhysics(inst)

        inst.entity:SetPristine()

        if not TheWorld.ismastersim then
            return inst
        end
		
		inst.currencyvalue = id

		inst:AddComponent("edible")
		inst.components.edible.foodtype = "ELEMENTAL"
		inst.components.edible.hungervalue = 10
		
		inst.AnimState:SetBloomEffectHandle( "shaders/anim.ksh" )  

		inst.OnEntityWake = onwake
		
		inst:AddComponent("stackable")
        inst.components.stackable.maxsize = TUNING.STACK_SIZE_SMALLITEM		

        inst:AddComponent("inspectable")
        inst:AddComponent("inventoryitem")
		inst.components.inventoryitem.atlasname = "images/inventoryimages/oinc"..(ids ~= "1" and ids or "").."p.xml"

		inst:AddComponent("tradable")

		if HAMLET_PP_MODE then
			PlayablePets.ReplacePrefab(inst, "oinc"..(ids ~= "1" and ids or ""))
		end
		
		--onupdate = onupdate

        return inst
    end

    return Prefab("oinc"..(ids ~= "1" and ids or "").."p", fn, assets)
end

return MakeCoin(1),
    MakeCoin(10),
	MakeCoin(100)
