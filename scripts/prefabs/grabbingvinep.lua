local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--
---------------------------

local assets = 
{
	Asset("ANIM", "anim/cave_exit_rope.zip"),
	Asset("ANIM", "anim/vine01_build.zip"),
	Asset("ANIM", "anim/vine02_build.zip"),	
	Asset("ANIM", "anim/copycreep_build.zip"),
}

local prefabs = 
{	

}

local start_inv = 
{
	--'prefab',
	--"spear",
}

local start_inv2 =
{
   --"beehome",
}

if MOBHOUSE == "Enable1" or MOBHOUSE == "Enable3" then
	start_inv = start_inv2
end
-----------------------
--Stats--
local mob = 
{
	health = 200,
	hunger = 150,
	hungerrate = 0.04, 
	sanity = 100,
	
	runspeed = 8,
	walkspeed = 4,
	
	attackperiod = 0,
	damage = 35*2,
	range = 3,
	hit_range = 4,
	
	bank = "exitrope",
	build = "copycreep_build",
	--build2 = "bee_guard_puffy_build",
	shiny = "vine",
	
	scale = 1,
	stategraph = "SGgrabbingvinep",
	minimap = "noiconlol.tex",	
}

--Loot that drops when you die, duh.
SetSharedLootTable( 'grabbingvinep',
{
    {'plantmeat',  1},
    {'rope',  0.4},
})


--==============================================
--					Mob Functions
--==============================================


--==============================================
--				Custom Common Functions
--==============================================
local function OnHitOther(inst, other, damage)
    if other.components.inventory ~= nil then
		inst.components.thief:StealItem(other)
	end
end
--==============================================
--					Loading/Saving
--==============================================
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
end

--==============================================
--					Forged Forge
--==============================================


local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, PPHAM_FORGE.VINE)
	
	inst.mobsleep = false
	
	--inst.acidmult = 1.25
	
	inst:DoTaskInTime(0, function(inst)
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"books", "staves", "darts"})
	end)
	
	inst:AddTag("lessaggro")
	
	inst:AddComponent("corpsereviver")
	if inst.components.corpsereviver then
		inst.components.corpsereviver.reviver_speed_mult = 0.5
		inst.components.corpsereviver.additional_revive_health_percent = 0.4
	end
	
	--because FF forces character physics on us on spawn
	inst:DoTaskInTime(3, function(inst)
		inst.Physics:SetCollisionGroup(COLLISION.FLYERS)
		inst.Physics:CollidesWith(COLLISION.FLYERS)	
	end)
	
	--inst:ListenForEvent("attacked", OnAttacked_Forge) --Shows head when hats make heads disappear.
	inst.components.health:StopRegen()
	--inst.components.combat.onhitotherfn = OnForgeHitOther
	
	inst.components.combat:SetDamageType(1)
	
	--inst.components.revivablecorpse.revivespeedmult = 0.3
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

--==============================================
--					Common/Master
--==============================================

local common_postinit = function(inst) 
	inst.MiniMapEntity:SetIcon(mob.minimap)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false) --PP doesn't work with movement prediction enabled, due to use of custom stategraphs
   end
end)

	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst)
	
end



local master_postinit = function(inst)
	--Stats--
	PlayablePets.SetCommonStats(inst, mob, nil, true) --mob table, ishuman, ignorepvpmultiplier
	inst.components.health:StartRegen(3, 2)
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst, 3, 1, 3) --fire, acid, poison
	----------------------------------
	--Tags--
    inst:RemoveTag("character")
	inst:AddTag("flying")
	inst:AddTag("noplayerindicator")
	inst:AddTag("vine")
	
	----------------------------------
	--Variables	
	--inst.mobsleep = true
	--inst.taunt = true
	inst.taunt2 = true
	
	inst.poisonsymbol = "exit_rope01"
	MakeSmallBurnableCharacter(inst, "exit_rope01")
	MakeTinyFreezableCharacter(inst, "exit_rope01")
	
	PlayablePets.SetStormImmunity(inst)
	----------------------------------
	--Components
	PlayablePets.SetCommonLootdropper(inst) --prefaboverride
	
	inst:AddComponent("thief")
	----------------------------------
	--Eater--

    inst.components.eater:SetAbsorptionModifiers(1,1,1) --This multiplies food stats.
	inst.components.eater:SetCanEatHorrible() --check to make them eat monster meat.
	inst.components.eater.strongstomach = true -- can eat monster meat!
	--inst.components.eater.eatwholestack = true --should eat the entire stack and get all the benefits.
	inst.components.eater.ignoresspoilage = true -- Can eat spoiled food without consequence. Might keep food stats as if fresh.
	---------------------------------
	--Physics and Shadows--
	MakeCharacterPhysics(inst, 1, .2)
	inst.Transform:SetNoFaced()
	
	inst.DynamicShadow:SetSize(1, 0.75)
	--inst.DynamicShadow:Enable(false)
    
	inst.Physics:SetCollisionGroup(COLLISION.FLYERS)
	inst.Physics:CollidesWith(COLLISION.FLYERS)	    
	
	PlayablePets.SetAmphibious(inst, nil, nil, true)
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	inst.components.combat.onhitotherfn = OnHitOther
	---------------------------------
	--Functions

    
	---------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
    ------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob, true, true) end)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob, true, true, nil, nil, true) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman, amphibious)
    end)
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
    return inst	
end

return MakePlayerCharacter("grabbingvinep", prefabs, assets, common_postinit, master_postinit, start_inv)