local prefabs =
{
    "nightmarefuel",
}

local brain = require( "brains/heraldminionbrain")

local function GetExcludeTags(inst)
	if TheNet:GetPVPEnabled() then
		return {"playerghost", "notarget", "DECOR", "INLIMBO", "structure", "wall", "shadow_aligned"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion", "playerghost", "DECOR", "INLIMBO", "notarget", "structure", "wall", "shadow_aligned"}
	else	
		return {"player", "companion", "playerghost", "notarget", "DECOR", "INLIMBO", "structure", "wall", "shadow_aligned"}
	end
end

local function retargetfn(inst)
    return FindEntity(
                inst,
                TUNING.PIG_TARGET_DIST,
                function(guy)
                    return guy and guy:IsValid()
                        and not (guy.sg ~= nil and (guy.sg:HasStateTag("transform") or (guy.sg:HasStateTag("sleeping") and TheNet:GetServerGameMode() == "lavaarena") or guy.sg:HasStateTag("fossilized"))) and inst.components.follower.leader ~= guy and not (guy.components.follower and guy.components.follower.leader and guy.components.follower.leader == inst.components.follower.leader) and not (TheNet:GetServerGameMode() == "lavaarena" and (guy:HasTag("player") or guy:HasTag("companion")))
                end,
                { "_combat"}, --See entityreplica.lua (re: "_combat" tag)
                GetExcludeTags()
            )
        or nil
end

local function KeepTargetFn(inst, target)
    return inst.components.combat:CanTarget(target)
           and inst.components.follower.leader ~= target
           and not target:HasTag("shadow") and not target:HasTag("shadow_aligned")
           and not (target.sg and ((target.sg:HasStateTag("sleeping") or target.sg:HasStateTag("fossilized")) and TheNet:GetServerGameMode() == "lavaarena"))
end

SetSharedLootTable('herald_minion',
{
    --{'nightmarefuel', 1.0},
    --{'nightmarefuel', 0.5},
})

local function DoBuff(inst, data)
	if data and data.buff then
		inst.sg:GoToState("taunt")
		inst.components.combat.damagemultiplier = 1.5
		inst.components.combat:SetAttackPeriod(0)
		inst.AnimState:SetMultColour(1, 1, 1, 0.8)
		inst.Transform:SetScale(1.15, 1.15, 1.15)
	end
end

local function UnBuff(inst, data)
	if data and data.buff then
		inst.sg:GoToState("taunt")
		inst.components.combat.damagemultiplier = 1
		inst.components.combat:SetAttackPeriod(inst.prefab == "herald_crawlingnightmare" and TUNING.CRAWLINGHORROR_ATTACK_PERIOD or TUNING.TERRORBEAK_ATTACK_PERIOD)
		inst.AnimState:SetMultColour(1, 1, 1, 0.5)
		inst.Transform:SetScale(1, 1, 1)
	end
end

local function CanShareTargetWith(dude)
    return dude:HasTag("nightmarecreature") and not dude.components.health:IsDead()
end

local function OnAttacked(inst, data)
    if data.attacker ~= nil then
        inst.components.combat:SetTarget(data.attacker)
        inst.components.combat:ShareTarget(data.attacker, 30, CanShareTargetWith, 1)
    end
end

local function ScheduleCleanup(inst)
    inst:DoTaskInTime(math.random() * TUNING.NIGHTMARE_SEGS.DAWN * TUNING.SEG_TIME, function()
        inst.components.lootdropper:SetLoot({})
        inst.components.lootdropper:SetChanceLootTable(nil)
        inst.components.health:Kill()
    end)
end

local function OnNightmareDawn(inst, dawn)
    if dawn then
        ScheduleCleanup(inst)
    end
end

local function MakeShadowCreature(data)
    local bank = data.bank 
    local build = data.build 

    local assets =
    {
        Asset("ANIM", "anim/"..data.build..".zip"),
    }

    local sounds = 
    {
        attack = "dontstarve/sanity/creature"..data.num.."/attack",
        attack_grunt = "dontstarve/sanity/creature"..data.num.."/attack_grunt",
        death = "dontstarve/sanity/creature"..data.num.."/die",
        idle = "dontstarve/sanity/creature"..data.num.."/idle",
        taunt = "dontstarve/sanity/creature"..data.num.."/taunt",
        appear = "dontstarve/sanity/creature"..data.num.."/appear",
        disappear = "dontstarve/sanity/creature"..data.num.."/dissappear",
    }

    local function fn()
        local inst = CreateEntity()

        inst.entity:AddTransform()
        inst.entity:AddAnimState()
        inst.entity:AddSoundEmitter()
        inst.entity:AddNetwork()

        inst.Transform:SetFourFaced()

        MakeCharacterPhysics(inst, 10, 1.5)
        RemovePhysicsColliders(inst)
        inst.Physics:SetCollisionGroup(COLLISION.SANITY)
        inst.Physics:CollidesWith(COLLISION.SANITY)      
         
        inst.AnimState:SetBank(bank)
        inst.AnimState:SetBuild(build)
        inst.AnimState:PlayAnimation("idle_loop")
        inst.AnimState:SetMultColour(1, 1, 1, 0.5)

        inst:AddTag("herald_minion")
        inst:AddTag("monster")
        inst:AddTag("hostile")
        inst:AddTag("notraptrigger")
		inst:AddTag("nospawnfx")
		if TheNet:GetServerGameMode() == "lavaarena" then
			inst:AddTag("companion")
		end

        inst.entity:SetPristine()

        if not TheWorld.ismastersim then
            return inst
        end
		
		inst.persists = false
		
		inst.acidimmune = true
		inst.debuffimmune = true
		inst.inkimmune = true
		inst.poisonimmune = true
		
		inst:AddComponent("follower")
		--inst.components.follower.keepleader = true
		inst.components.follower:KeepLeaderOnAttacked()
		inst.components.follower.keepdeadleader = true

        inst:AddComponent("locomotor") -- locomotor must be constructed before the stategraph
        inst.components.locomotor.walkspeed = data.speed
        inst.sounds = sounds

        inst:SetStateGraph("SGshadowcreature")
        inst:SetBrain(brain)

        inst:AddComponent("sanityaura")
        inst.components.sanityaura.aura = -TUNING.SANITYAURA_LARGE

        inst:AddComponent("health")
        inst.components.health:SetMaxHealth(data.health)

        inst:AddComponent("combat")
        inst.components.combat:SetDefaultDamage(data.damage)
        inst.components.combat:SetAttackPeriod(data.attackperiod)
        inst.components.combat:SetRetargetFunction(2, retargetfn)
		inst.components.combat:SetKeepTargetFunction(KeepTargetFn)
		

        inst:AddComponent("lootdropper")
        --inst.components.lootdropper:SetChanceLootTable('nightmare_creature')

        --inst:ListenForEvent("attacked", OnAttacked)

        --inst:WatchWorldState("isnightmaredawn", OnNightmareDawn)

        inst:AddComponent("knownlocations")
		
		inst:ListenForEvent("buffpets", DoBuff)
		inst:ListenForEvent("debuffpets", UnBuff)

        return inst
    end

    return Prefab(data.name, fn, assets, prefabs)
end

local data =
{
    {
        name = "herald_crawlingnightmare",
        build = "shadow_insanity1_basic",
        bank = "shadowcreature1",
        num = 1,
        speed = TUNING.CRAWLINGHORROR_SPEED,
        health = 600,
        damage = TheNet:GetServerGameMode() == "lavaarena" and 20 or 30,
        attackperiod = TUNING.CRAWLINGHORROR_ATTACK_PERIOD,
        sanityreward = 0,
    },
    {
        name = "herald_nightmarebeak",
        build = "shadow_insanity2_basic",
        bank = "shadowcreature2",
        num = 2,
        speed = TUNING.TERRORBEAK_SPEED,
        health = 500,
        damage = TheNet:GetServerGameMode() == "lavaarena" and 30 or 50,
        attackperiod = TUNING.TERRORBEAK_ATTACK_PERIOD,
        sanityreward = 0,
    },
}

local ret = {}
for i, v in ipairs(data) do
    table.insert(ret, MakeShadowCreature(v))
end

return unpack(ret) 
