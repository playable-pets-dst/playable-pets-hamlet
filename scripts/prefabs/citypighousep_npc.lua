require "prefabutil"
require "recipes"

local assets =
{
	Asset("ANIM", "anim/pig_townhouse1.zip"),
    Asset("ANIM", "anim/pig_townhouse5.zip"),
    Asset("ANIM", "anim/pig_townhouse6.zip"),    

    Asset("ANIM", "anim/pig_townhouse1_pink_build.zip"),
    Asset("ANIM", "anim/pig_townhouse1_green_build.zip"),

    Asset("ANIM", "anim/pig_townhouse1_brown_build.zip"),
    Asset("ANIM", "anim/pig_townhouse1_white_build.zip"),

    Asset("ANIM", "anim/pig_townhouse5_beige_build.zip"),
    Asset("ANIM", "anim/pig_townhouse6_red_build.zip"),
    
    Asset("ANIM", "anim/pig_farmhouse_build.zip"),
	
	Asset("ANIM", "anim/palace.zip"),

    Asset("SOUND", "sound/pig.fsb"),
    Asset("INV_IMAGE", "pighouse_city"),    
}

local prefabs = 
{
    "pigman_collectorp",
    "pigman_bankerp",
    "pigman_beauticianp",
    "pigman_floristp",
    "pigman_eruditep",
    "pigman_hunterp",
    "pigman_hatmakerp",
    "pigman_usherp",
    "pigman_mechanicp",
    "pigman_storeownerp",
    "pigman_professorp",
}

local city_1_citizens = {
    "pigman_bankerp",
    "pigman_beauticianp",
    "pigman_floristp",
    "pigman_usherp",
    "pigman_mechanicp",
    "pigman_storeownerp",
    "pigman_professorp",
	"pigman_collectorp",
    "pigman_eruditep",
    "pigman_hatmakerp",
    "pigman_hunterp",
}

local city_2_citizens = {
    "pigman_collectorp",
    "pigman_eruditep",
    "pigman_hatmakerp",
    "pigman_hunterp",
}

local city_citizens = {
    city_1_citizens,
    city_2_citizens,
}

local spawned_farm = {
    "pigman_farmerp"
}

local spawned_cityhall = {
    "pigman_mayorp"
}

local spawned_palace = {
    "pigman_queenp"
}

local spawned_mine = {
    "pigman_minerp"
}

local SCALEBUILD ={}
SCALEBUILD["pig_townhouse1_pink_build"] = true
SCALEBUILD["pig_townhouse1_green_build"] = true
SCALEBUILD["pig_townhouse1_white_build"] = true
SCALEBUILD["pig_townhouse1_brown_build"] = true

local SETBANK ={}
SETBANK["pig_townhouse1_pink_build"] = "pig_townhouse"
SETBANK["pig_townhouse1_green_build"] = "pig_townhouse"
SETBANK["pig_townhouse1_white_build"] = "pig_townhouse"
SETBANK["pig_townhouse1_brown_build"] = "pig_townhouse"
SETBANK["pig_townhouse5_beige_build"] = "pig_townhouse5"
SETBANK["pig_townhouse6_red_build"] = "pig_townhouse6"

local house_builds = {
   "pig_townhouse1_pink_build",
   "pig_townhouse1_green_build",
   "pig_townhouse1_white_build",
   "pig_townhouse1_brown_build",
   "pig_townhouse5_beige_build",
   "pig_townhouse6_red_build",   
}

local function setScale(inst,build)
    if SCALEBUILD[build] then
        inst.AnimState:SetScale(0.75,0.75,0.75)
    else
        inst.AnimState:SetScale(1,1,1)
    end
end

local function getScale(inst,build)
    if SCALEBUILD[build] then
        return {0.75,0.75,0.75}
    else
        return {1,1,1}
    end
end

local function LightsOn(inst)
    if not inst:HasTag("burnt") then
        inst.Light:Enable(true)
        inst.AnimState:PlayAnimation("lit", true)
        inst.SoundEmitter:PlaySound("dontstarve/pig/pighut_lighton")
        inst.lightson = true
    end
end

local function LightsOff(inst)
    if not inst:HasTag("burnt") then
        inst.Light:Enable(false)
        inst.AnimState:PlayAnimation("idle", true)
        inst.SoundEmitter:PlaySound("dontstarve/pig/pighut_lightoff")
        inst.lightson = false
    end
end

local function onfar(inst) 
    if not inst:HasTag("burnt") then
        if inst.components.spawner and inst.components.spawner:IsOccupied() then
            LightsOn(inst)
        end
    end
end

local function getstatus(inst)
    if inst:HasTag("burnt") then
        return "BURNT"
    elseif inst.components.spawner and inst.components.spawner:IsOccupied() then
        if inst.lightson then
            return "FULL"
        else
            return "LIGHTSOUT"
        end
    end
end

local function onnear(inst) 
    if not inst:HasTag("burnt") then
        if inst.components.spawner and inst.components.spawner:IsOccupied() then
            LightsOff(inst)
        end
    end
end

local function onwere(child)
    if child.parent and not child.parent:HasTag("burnt") then
        child.parent.SoundEmitter:KillSound("pigsound")
        child.parent.SoundEmitter:PlaySound("dontstarve/pig/werepig_in_hut", "pigsound")
    end
end

local function onnormal(child)
    if child.parent and not child.parent:HasTag("burnt") then
        child.parent.SoundEmitter:KillSound("pigsound")
        child.parent.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/city_pig/pig_in_house_LP", "pigsound")
    end
end

local function onburntup(inst)
    if inst.doortask ~= nil then
        inst.doortask:Cancel()
        inst.doortask = nil
    end
    if inst.inittask ~= nil then
        inst.inittask:Cancel()
        inst.inittask = nil
    end
    if inst._window ~= nil then
        inst._window:Remove()
        inst._window = nil
    end
    if inst._windowsnow ~= nil then
        inst._windowsnow:Remove()
        inst._windowsnow = nil
    end
end

local function onignite(inst)
    if inst.components.spawner ~= nil and inst.components.spawner:IsOccupied() then
        inst.components.spawner:ReleaseChild()
    end
end

local function onoccupied(inst, child)
    if not inst:HasTag("burnt") then
    	inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/city_pig/pig_in_house_LP", "pigsound")
        inst.SoundEmitter:PlaySound("dontstarve/common/pighouse_door")
    	
		if inst.illegal then
			child.illegal = inst.illegal
		end
		
        if inst.doortask then
            inst.doortask:Cancel()
            inst.doortask = nil
        end
    	--inst.doortask = inst:DoTaskInTime(1, function() if not inst.components.playerprox:IsPlayerClose() then LightsOn(inst) end end)
        inst.doortask = inst:DoTaskInTime(1, function() LightsOn(inst) end)
    	if child then
    	    inst:ListenForEvent("transformwere", onwere, child)
    	    inst:ListenForEvent("transformnormal", onnormal, child)
    	end
    end
end

local function onvacate(inst, child)
    if not inst:HasTag("burnt") then
        if inst.doortask then
            inst.doortask:Cancel()
            inst.doortask = nil
        end
		
		if inst.illegal then
			child.illegal = inst.illegal
		end
        inst.SoundEmitter:PlaySound("dontstarve/common/pighouse_door")
        inst.SoundEmitter:KillSound("pigsound")
    	
    	if child then
    	    inst:RemoveEventCallback("transformwere", onwere, child)
    	    inst:RemoveEventCallback("transformnormal", onnormal, child)
            if child.components.werebeast then
    		    child.components.werebeast:ResetTriggers()
    		end
    		if child.components.health then
    		    child.components.health:SetPercent(1)
    		end
          --  if child.components.citypossession
    	end    
    end
end
           
local function onhammered(inst, worker)
    if inst:HasTag("fire") and inst.components.burnable then
        inst.components.burnable:Extinguish()
    end

    inst.reconstruction_project_spawn_state = {
        bank = "pig_house",
        build = "pig_house",
        anim = "unbuilt",
    }

    if inst.doortask then
        inst.doortask:Cancel()
        inst.doortask = nil
    end
	if inst.components.spawner and inst.components.spawner:IsOccupied() then inst.components.spawner:ReleaseChild() end
	if not inst.components.fixable then
        inst.components.lootdropper:DropLoot()
    end
	SpawnPrefab("collapse_big").Transform:SetPosition(inst.Transform:GetWorldPosition())
	inst.SoundEmitter:PlaySound("dontstarve/common/destroy_wood")
	inst:Remove()
end

local function ongusthammerfn(inst)
    onhammered(inst, nil)
end

local function onhit(inst, worker)
    if not inst:HasTag("burnt") then
    	inst.AnimState:PlayAnimation("hit")
    	inst.AnimState:PushAnimation("idle")
    end
end

local function paytax(inst)
 
end
local function checktax(inst)

end

local function OnDay(inst)
    ----print(inst, "OnDay")
    if not inst:HasTag("burnt") then
        if inst.components.spawner:IsOccupied() then
            LightsOff(inst)
            if inst.doortask then
                inst.doortask:Cancel()
                inst.doortask = nil
            end
            inst.doortask = inst:DoTaskInTime(1 + math.random()*2, function() inst.components.spawner:ReleaseChild() end)
        end
    end   
end

local function setcolor(inst,num)
    if not num then
        num = math.random()
    end
    local color = 0.5 + num * 0.5
    inst.AnimState:SetMultColour(color, color, color, 1)
    return num
end

local function onsave(inst, data)
    if inst:HasTag("burnt") or (inst.components.burnable ~= nil and inst.components.burnable:IsBurning()) then
        data.burnt = true
    end
	data.faction = inst.faction and inst.faction or nil
	data.build = inst.setbuild and inst.setbuild or inst.build
    data.animset = inst.setanimset and inst.setanimset or inst.animset
	if inst.components.spawner and inst.components.spawner.childname then
        data.childname = inst.components.spawner.childname
    end
	
	if inst.illegal then
		data.illegal = inst.illegal
	end
end

local function onload(inst, data)
    if data ~= nil then
		if data.build then
            inst.setbuild = data.build
			inst:DoTaskInTime(0, function(inst)
            inst.AnimState:SetBuild(inst.setbuild)
			setScale(inst,inst.setbuild)
			end)
           
        end

        if data.animset then
            inst.setanimset = data.animset
			inst:DoTaskInTime(0, function(inst)
            inst.AnimState:SetBank(inst.setanimset)
			end)
        end
		
		if data.burnt then
        inst.components.burnable.onburnt(inst)
		end
		
		if data.childname and inst.components.spawner then
            inst.components.spawner:Configure(data.childname, 480*3)
        end
		
		if data.illegal then
			inst.illegal = data.illegal
		end
    end
end

local function ConfigureSpawner( inst, selected_citizens )
    inst.spawnlist = selected_citizens
    inst.components.spawner:Configure( selected_citizens[math.random(1,#selected_citizens)], 480*3, 1)
    
    inst.components.spawner.onoccupied = onoccupied
    inst.components.spawner.onvacate = onvacate
    inst:WatchWorldState( "isday", function() OnDay(inst) end)
end

local function citypossessionfn( inst )

    local selected_citizens = {}
    if inst.components.citypossession and inst.components.citypossession.cityID then
        for i=1, inst.components.citypossession.cityID do
            selected_citizens = JoinArrays(selected_citizens, city_citizens[i])
        end
    else
        for i=1, 2 do
            selected_citizens = JoinArrays(selected_citizens, city_citizens[i])
        end
    end

    ConfigureSpawner(inst, selected_citizens)    
end

local function onbuilt(inst)
    inst.AnimState:PlayAnimation("place")
    inst.AnimState:PushAnimation("idle")
    citypossessionfn( inst )
end

local function OnLoadPostPass(inst)
    --citypossessionfn( inst )
end

local function makefn(animset, setbuild, spawnList)

    local function fn(Sim)
    	local inst = CreateEntity()
    	local trans = inst.entity:AddTransform()
    	local anim = inst.entity:AddAnimState()
        local light = inst.entity:AddLight()
        inst.entity:AddSoundEmitter()
		inst.entity:AddNetwork()

    	local minimap = inst.entity:AddMiniMapEntity()
    	minimap:SetIcon( "pighouse.png" )
        --{anim="level1", sound="dontstarve/common/campfire", radius=2, intensity=.75, falloff=.33, colour = {197/255,197/255,170/255}},
        
        
        MakeObstaclePhysics(inst, 1)    

        anim:Hide("YOTP")

        inst:AddTag("bandit_cover")
        inst:AddTag("structure")
        inst:AddTag("city_hammerable")

		inst.entity:SetPristine()

		if not TheWorld.ismastersim then
			return inst
		end
		
		if not inst.build then
			local build = house_builds[math.random(1,#house_builds)]        
			if inst.setbuild then
				build = inst.setbuild
			end
			inst.build = setbuild and setbuild or build
		end
		inst.AnimState:SetBuild(inst.build)
	
	--inst.animset = nil

		if inst.setanimset then
			inst.AnimState:SetBank(animset and animset or inst.setanimset)
			inst.animset = animset and animset or inst.setanimset
		else            
			inst.AnimState:SetBank(animset and animset or SETBANK[inst.build])            
			inst.animset = animset and animset or SETBANK[inst.build]
		end
		inst.AnimState:PlayAnimation("idle", true)

		inst.colornum = setcolor(inst)
        local color = 0.5 + math.random() * 0.5
        anim:SetMultColour(color, color, color, 1)
		
        setScale(inst, inst.build)
		
		
        inst:AddComponent("lootdropper")
        
        inst:AddComponent("workable")
        inst.components.workable:SetWorkAction(ACTIONS.HAMMER)
        inst.components.workable:SetWorkLeft(4)
    	inst.components.workable:SetOnFinishCallback(onhammered)
    	inst.components.workable:SetOnWorkCallback(onhit)
    	
		light:SetFalloff(1)
        light:SetIntensity(.5)
        light:SetRadius(1)
        light:Enable(false)
        light:SetColour(180/255, 195/255, 50/255)
		
        inst:AddComponent("spawner")
        if spawnList then
            ConfigureSpawner(inst, spawnList)           
        else
            --inst.citypossessionfn = citypossessionfn 
            inst.OnLoadPostPass = OnLoadPostPass
        end

        inst:WatchWorldState( "isday", function() OnDay(inst) end)    

        inst:AddComponent("inspectable")
        
        inst.components.inspectable.getstatus = getstatus
    	
    	MakeSnowCovered(inst, .01)
        
        --inst:AddComponent("fixable")        
        --inst.components.fixable:AddRecinstructionStageData("rubble","pig_townhouse",build,nil,getScale(inst,build))
        --inst.components.fixable:AddRecinstructionStageData("unbuilt","pig_townhouse",build,nil,getScale(inst,build)) 



        MakeMediumBurnable(inst, nil, nil, true)
		MakeLargePropagator(inst)
		inst:ListenForEvent("burntup", onburntup)
		inst:ListenForEvent("onignite", onignite)

        inst.OnSave = onsave 
        inst.OnLoad = onload

    	inst:ListenForEvent( "onbuilt", onbuilt)
        inst:DoTaskInTime(math.random(), function() 
            ----print(inst, "spawn check day")
            if TheWorld.state.isday then 
                OnDay(inst)
            end 
        end)

        return inst
    end
    return fn
end

local function MakeDecor(name, anim)
	local function fn()
		local inst = CreateEntity()
		local trans = inst.entity:AddTransform()
		local anim = inst.entity:AddAnimState()
		local light = inst.entity:AddLight()
		inst.entity:AddSoundEmitter()
		inst.entity:AddNetwork()
	
		inst.AnimState:SetBank("pig_townhouse1")
		inst.AnimState:SetBuild("pig_townhouse1_green_build")
		inst.AnimState:PlayAnimation(anim)
	
		MakeObstaclePhysics(inst, 1)    
		
		if not TheWorld.ismastersim then
			return inst
		end
		
		return inst
	end
	return Prefab(name, fn, assets, prefabs)
end

local function placetestfn(inst)
    inst.AnimState:Hide("YOTP")
    inst.AnimState:Hide("SNOW")
    return true
end

local function house(name, anim, build, spawnList)
    return Prefab( "common/objects/"..name, makefn(anim, build, spawnList ), assets, prefabs)
end

return house("pighouse_cityp",nil,nil, city_1_citizens),
       house("pighouse_farmp","pig_shop","pig_farmhouse_build",spawned_farm),
       house("pighouse_minep","pig_shop","pig_farmhouse_build",spawned_mine),
	   house("pighouse_city_player_cityhall","pig_cityhall","pig_cityhall",spawned_cityhall),
	   house("pig_city_player_palace", "palace", "palace", spawned_palace),
	   MakeDecor("pig_rubblep", "rubble"),
	   MakeDecor("pig_unbuiltp", "unbuilt"),

       MakePlacer("common/pighouse_cityp_placer", "pig_shop", "pig_townhouse1_green_build", "idle", nil, nil, true, 0.75, nil, nil, nil, nil, nil, placetestfn)

	  -- MakePlacer("common/pighouse_placer", "pig_house", "pig_house", "idle")  
