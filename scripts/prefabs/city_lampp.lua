local assets =
{
	Asset("ANIM", "anim/lamp_post2.zip"),
    Asset("ANIM", "anim/lamp_post2_city_build.zip"),    
    Asset("ANIM", "anim/lamp_post2_yotp_build.zip"),
}

local INTENSITY = 0.6

local LAMP_DIST = 16
local LAMP_DIST_SQ = LAMP_DIST * LAMP_DIST

local function OnIsPathFindingDirty(inst)    
    local wall_x, wall_y, wall_z = inst.Transform:GetWorldPosition()
    if TheWorld.Map:GetPlatformAtPoint(wall_x, wall_z) == nil then        
        if inst._ispathfinding:value() then
            if inst._pfpos == nil then
                inst._pfpos = Point(wall_x, wall_y, wall_z)
                TheWorld.Pathfinder:AddWall(wall_x, wall_y, wall_z)
            end
        elseif inst._pfpos ~= nil then
            TheWorld.Pathfinder:RemoveWall(wall_x, wall_y, wall_z)
            inst._pfpos = nil
        end
    end
end

local function InitializePathFinding(inst)
    inst:ListenForEvent("onispathfindingdirty", OnIsPathFindingDirty)
    OnIsPathFindingDirty(inst)
end

local function makeobstacle(inst)
    inst.Physics:SetActive(true)
    inst._ispathfinding:set(true)
end

local function clearobstacle(inst)
    inst.Physics:SetActive(false)
    inst._ispathfinding:set(false)
end

local function UpdateAudio(inst)

    local instPosition = Vector3(inst.Transform:GetWorldPosition())

    if not TheWorld.state.isday and not inst.SoundEmitter:PlayingSound("onsound") then
        inst.SoundEmitter:PlaySound("dontstarve_DLC003/common/objects/city_lamp/on_LP", "onsound")
    elseif TheWorld.state.isday and inst.SoundEmitter:PlayingSound("onsound") then
        inst.SoundEmitter:KillSound("onsound")
    end
end

local function GetStatus(inst)
    return not inst.lighton and "ON" or nil
end

local function fadein(inst)
    inst.AnimState:PlayAnimation("on")
    inst.SoundEmitter:PlaySound("dontstarve_DLC003/common/objects/city_lamp/fire_on")
    inst.AnimState:PushAnimation("idle", true)
    inst.Light:Enable(true)
	inst.Light:SetIntensity(INTENSITY)
end

local function fadeout(inst)
    inst.AnimState:PlayAnimation("off")    
    inst.AnimState:PushAnimation("idle", true)
	inst.Light:SetIntensity(0)
end

local function updatelight(inst)
    if not TheWorld.state.isday then
        if not inst.lighton then
                fadein(inst)
        else            
            inst.Light:Enable(true)
            inst.Light:SetIntensity(INTENSITY)
        end
        inst.AnimState:Show("FIRE")
        inst.AnimState:Show("GLOW")        
        inst.lighton = true
    else
        if inst.lighton then
                fadeout(inst)            
        end

        inst.AnimState:Hide("FIRE")
        inst.AnimState:Hide("GLOW")        

        inst.lighton = false
    end
end

local function onhammered(inst, worker)

    inst.SoundEmitter:KillSound("onsound")

    if not inst.components.fixable then
        inst.components.lootdropper:DropLoot()
    end

    SpawnPrefab("collapse_small").Transform:SetPosition(inst.Transform:GetWorldPosition())
    inst.SoundEmitter:PlaySound("dontstarve/common/destroy_metal")

    inst:Remove()
end

local function onhit(inst, worker)
    inst.AnimState:PlayAnimation("hit")
    inst.AnimState:PushAnimation("idle", true)
    inst:DoTaskInTime(0.3, function() updatelight(inst) end)
end

local function onbuilt(inst)
    inst.AnimState:PlayAnimation("place")
    inst.AnimState:PushAnimation("idle", true)
    inst:DoTaskInTime(0, function() updatelight(inst) end)
end

local function OnEntitySleep(inst)
	if inst.audiotask then
		inst.audiotask:Cancel()
		inst.audiotask = nil
	end
end

local function OnEntityWake(inst)
	if inst.audiotask then
		inst.audiotask:Cancel()
	end
    inst.audiotask = inst:DoPeriodicTask(1.0, function() UpdateAudio(inst) end, math.random())
end

local function fn(Sim)
	local inst = CreateEntity()
    local sound = inst.entity:AddSoundEmitter()
	inst.entity:AddNetwork()    
	inst.entity:AddTransform()
	inst.entity:AddAnimState()
	
	local light = inst.entity:AddLight()
    inst.Light:SetIntensity(INTENSITY)
    inst.Light:SetColour(197/255, 197/255, 10/255)
    inst.Light:SetFalloff( 0.9 )
    inst.Light:SetRadius( 5 )
    inst.Light:Enable(false)
	
	inst.AnimState:SetBank("lamp_post")
    inst.AnimState:SetBuild("lamp_post2_city_build")
    inst.AnimState:PlayAnimation("idle", true)
 
    MakeObstaclePhysics(inst, 0.25)   
	inst.Physics:SetDontRemoveOnSleep(true)

	inst._ispathfinding = net_bool(inst.GUID, "_ispathfinding", "onispathfindingdirty")
    makeobstacle(inst)
        --Delay this because makeobstacle sets pathfinding on by default
        --but we don't to handle it until after our position is set
    inst:DoTaskInTime(0, InitializePathFinding)
	
	inst:AddTag("CITY_LAMP")
	inst:AddTag("structure")
	inst.entity:SetPristine()
	
    if not TheWorld.ismastersim then
        return inst
    end
    
    --inst.AnimState:SetBloomEffectHandle( "shaders/anim.ksh" )
    
    inst.build = "lamp_post2_city_build"

    inst.AnimState:Hide("FIRE")
    inst.AnimState:Hide("GLOW")    

    inst.AnimState:SetRayTestOnBB(true);

    inst:AddTag("lightsource")
	
    inst:AddComponent("inspectable")
    inst.components.inspectable.getstatus = GetStatus


    inst:AddComponent("lootdropper")

    inst:AddComponent("workable")
    inst.components.workable:SetWorkAction(ACTIONS.HAMMER)
    inst.components.workable:SetWorkLeft(4)
    inst.components.workable:SetOnFinishCallback(onhammered)
    inst.components.workable:SetOnWorkCallback(onhit)    

    inst:WatchWorldState("isday", function()
        inst:DoTaskInTime(1/30, function() updatelight(inst) end)
    end)
    inst:WatchWorldState("isdusk", function()
        inst:DoTaskInTime(1/30, function() updatelight(inst) end)
    end)

    inst:ListenForEvent("onbuilt", onbuilt)

    inst.OnSave = function(inst, data)
        if inst.lighton then
			data.permanent = inst.permanent or nil
			data.colour = inst.colour or nil
			data.build = inst.build or nil
            data.lighton = inst.lighton
        end
    end        

    inst.OnLoad = function(inst, data)    
        if data then
            if data.lighton then 
                fadein(inst)
                inst.Light:Enable(true)
                inst.Light:SetIntensity(INTENSITY)            
                inst.AnimState:Show("FIRE")
                inst.AnimState:Show("GLOW")        
                inst.lighton = true
            end
			inst.colour = data.colour or nil
			inst.build = data.build or nil
			inst.permanent = data.permanent or nil
        end
    end

    inst.audiotask = inst:DoPeriodicTask(1.0, function() UpdateAudio(inst) end, math.random())
    
	inst:DoTaskInTime(0, function(inst)
		if inst.permanent then
			inst:RemoveComponent("workable")
		end
		
		if inst.colour then --r, g, b, radius, falloff
			local c = inst.colour
			if c.r and c.g and c.b then
				inst.Light:SetColour(c.r, c.g, c.b)
			end
			inst.Light:SetIntensity(c.intensity and c.intensity or INTENSITY)
			inst.Light:SetFalloff( c.falloff and c.falloff or 0.9 )
			inst.Light:SetRadius( c.radius and c.radius or 5 )
		end
		
		if inst.build then
			inst.AnimState:SetBuild(inst.build)
		end
		updatelight(inst)
	end)
    

    inst.OnEntitySleep = OnEntitySleep
    inst.OnEntityWake = OnEntityWake    
	return inst
end

return Prefab( "common/objects/city_lampp", fn, assets),
MakePlacer("common/city_lampp_placer", "lamp_post", "lamp_post2_city_build", "idle", false, false, true)

