require "prefabutil"

local assets =
{
	Asset("ANIM", "anim/hedge.zip"),
	Asset("ANIM", "anim/hedge1_build.zip"),	
	Asset("ANIM", "anim/hedge2_build.zip"),
	Asset("ANIM", "anim/hedge3_build.zip"),	
}

local prefabs =
{
	"collapse_small",
}

local function normalize(coord)         
    local temp = coord%0.5 
    coord = coord + 0.5 - temp

    if  coord%1 == 0 then
        coord = coord -0.5
    end

    return coord
end

local function MakeWallItem(id)
	local function ondeploywall(inst, pt, deployer)
        --inst.SoundEmitter:PlaySound("dontstarve/creatures/spider/spider_egg_sack")
        local wall = SpawnPrefab("wall_hedge"..id.."p") 
        if wall ~= nil then 
            local x = math.floor(pt.x) 
            local z = math.floor(pt.z) 
            wall.Physics:SetCollides(false)
            wall.Physics:Teleport(x, 0, z)
            wall.Physics:SetCollides(true)
            inst.components.stackable:Get():Remove()
        end
    end
	
	local function fn()
		local inst = CreateEntity()

		inst.entity:AddTransform()
		inst.entity:AddAnimState()
		inst.entity:AddSoundEmitter()
		inst.entity:AddNetwork()

		MakeInventoryPhysics(inst)
		
		MakeInventoryFloatable(inst)
	
		inst.AnimState:SetBank("hedge")
		inst.AnimState:SetBuild("hedge"..id.."_build")
		inst.AnimState:PlayAnimation("idle")

		inst:AddTag("wallbuilder")

		inst.entity:SetPristine()

		if not TheWorld.ismastersim then
			return inst
		end

		inst:AddComponent("stackable")
		inst.components.stackable.maxsize = TUNING.STACK_SIZE_MEDITEM   
		
		inst:AddComponent("inspectable")

		inst:AddComponent("fuel")
		inst.components.fuel.fuelvalue = TUNING.LARGE_FUEL

		MakeSmallBurnable(inst, TUNING.LARGE_BURNTIME)
		MakeSmallPropagator(inst)
		MakeHauntableLaunchAndIgnite(inst)
	
		inst:AddComponent("inventoryitem")
		inst.components.inventoryitem.atlasname = "images/inventoryimages/ppham_icons.xml"

		inst:AddComponent("deployable")
        inst.components.deployable.ondeploy = ondeploywall
        inst.components.deployable:SetDeployMode(DEPLOYMODE.WALL)

		if HAMLET_PP_MODE then
			local ham_prefabs = {
				"hedge_block_item",
				"hedge_cone_item",
				"hedge_layered_item"
			}
			PlayablePets.ReplacePrefab(inst, ham_prefabs[id])
		end

		return inst
	end
	
	return Prefab("wall_hedge"..id.."_itemp", fn, assets)
end

return MakeWallItem(1),
	MakeWallItem(2),
	MakeWallItem(3),
	MakePlacer("wall_hedge1_itemp_placer", "hedge", "hedge1_build", "growth1", false, false, true, nil, nil, "eight"),
	MakePlacer("wall_hedge2_itemp_placer", "hedge", "hedge2_build", "growth1", false, false, true, nil, nil, "eight"),
	MakePlacer("wall_hedge3_itemp_placer", "hedge", "hedge3_build", "growth1", false, false, true, nil, nil, "eight"),
	MakePlacer("lawndecor1p_placer", "topiary", "topiary01_build", "idle"),
	MakePlacer("lawndecor2p_placer", "topiary", "topiary02_build", "idle"),
	MakePlacer("lawndecor3p_placer", "topiary", "topiary03_build", "idle"),
	MakePlacer("lawndecor4p_placer", "topiary", "topiary04_build", "idle"),
	MakePlacer("lawndecor5p_placer", "topiary", "topiary05_build", "idle"),
	MakePlacer("lawndecor6p_placer", "topiary", "topiary06_build", "idle"),
	MakePlacer("lawndecor7p_placer", "topiary", "topiary07_build", "idle")