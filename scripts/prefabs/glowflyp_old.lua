local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--
---------------------------

--Don't add assets unless absolutely needed.
local assets = 
{
	Asset("ANIM", "anim/lantern_fly.zip"),
	Asset("ANIM", "anim/glowfly_shiny_build_08.zip"),
	Asset("ANIM", "anim/rabid_beetle_shiny_build_08.zip"),
}

local getskins = {"8"}	

--Might be completely pointless here. I don't know.
local prefabs = 
{	

}
	
--Note: Becareful when adding starting items.
--People are likely to spam changecharacter to get more of them.	
local start_inv = 
{
	--'prefab',
	--"spear",
}

local start_inv2 =
{
   --"beehome",
}

if MOBHOUSE == "Enable1" or MOBHOUSE == "Enable3" then
	start_inv = start_inv2
end
-----------------------
--Stats--
local mob = 
{
	health = 20,
	hunger = 60,
	hungerrate = 0.075, 
	sanity = 80,
	runspeed = 6,
	walkspeed = 5,
	damage = 20*2,
	range = 3,
	bank = "lantern_fly",
	build = "lantern_fly",
	shiny = "glowfly",
	--build2 = "bee_guard_puffy_build",
	scale = 0.6,
	stategraph = "SGglowflyp",
	minimap = "glowflyp.tex",
	
}

--Loot that drops when you die, duh.
SetSharedLootTable('glowflyp',
{
    {'lightbulb', 1},
})

local sounds =
{
	takeoff = "dontstarve/creatures/mosquito/mosquito_takeoff",
	attack = "dontstarve/creatures/mosquito/mosquito_attack",
	buzz = "dontstarve_DLC003/creatures/glowfly/buzz_LP",
	hit = "dontstarve_DLC003/creatures/glowfly/hit",
	death = "dontstarve_DLC003/creatures/glowfly/death",
	explode = "dontstarve/creatures/mosquito/mosquito_explo",
}

local function GetExcludeTags(inst)
	if TheNet:GetPVPEnabled() then
		return {"lightningimmune", "lightningrod", "notarget"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion", "lightningimmune", "lightningrod", "notarget"}
	else	
		return {"player", "companion", "lightningimmune", "lightningrod", "notarget"}
	end
end

local function onattacked(inst, data)
    inst.components.combat:SetTarget(data.attacker)
    inst.components.combat:ShareTarget(data.attacker, 30, function(dude) return dude:HasTag("rabidbeetle") and not dude.components.health:IsDead() end, 30)
end

local function onhitother2(inst, other)
	if inst.isaltattacking then
		PlayablePets.SetPoison(inst, other)
	end
    inst.components.combat:SetTarget(other)
    inst.components.combat:ShareTarget(other, 30, function(dude) return dude:HasTag("rabidbeetle") and not dude.components.health:IsDead() end, 30)
end

local function EquipHat(inst) --If a hat removes the head, this fixes that.
    local head = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HEAD)
	if head then
		inst.AnimState:Show("HEAD")
	end 
end

local function Equip(inst)
    local hands = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS) or nil
	local head = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HEAD) or nil
	local body = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.BODY) or nil
	if hands ~= nil and hands.components.weapon then
		hands.components.weapon:SetDamage(45*2)
        hands.components.weapon:SetRange(TUNING.BISHOP_ATTACK_DIST+2, TUNING.BISHOP_ATTACK_DIST+6)
        hands.components.weapon:SetProjectile("spider_web_spit")
	end
end

local NIGHTVISION_COLOURCUBES =
{
    day = "images/colour_cubes/ruins_light_cc.tex",
    dusk = "images/colour_cubes/ruins_dim_cc.tex",
    night = "images/colour_cubes/ruins_light_cc.tex",
    full_moon = "images/colour_cubes/purple_moon_cc.tex",
}

local function SetNightVision(inst, enable) --This should be obvious
    if (TheWorld.state.isnight or TheWorld:HasTag("cave")) and MOBNIGHTVISION == "Enable" then
        inst.components.playervision:ForceNightVision(true)
        inst.components.playervision:SetCustomCCTable(NIGHTVISION_COLOURCUBES)
    else
        inst.components.playervision:ForceNightVision(false)
        inst.components.playervision:SetCustomCCTable(nil)
    end
end

local function RestoreNightImmunity(inst) --Resets immunity to Grue
	if MOBNIGHTVISION ~= "Disable2" then inst.components.grue:AddImmunity("mobplayer") end
end

------------------------------------------------------
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
		inst.isadult = data.isadult or false
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
	data.isadult = inst.isadult or false
end
-------------------------------------------------------

local function setSkin(inst)
	--inst.isshiny should always be a string now.
	if not inst:HasTag("playerghost") then
		local mobskin = inst.components.skinner.skin_name
		local getshiny = (mobskin ~= inst.prefab and mobskin ~= "") and string.gsub(mobskin, inst.prefab.."_", "" ) --will return false if no skin is equipped.
		if SKIN_RARITY_COLORS.ModMade ~= nil then
			if getshiny ~= false then
				inst.isshiny = getshiny --used for other purposes
				if inst.isadult and inst.isadult == true then
					inst.AnimState:SetBuild("rabid_beetle_shiny_build_0"..inst.isshiny)
				else
					inst.AnimState:SetBuild("glowfly_shiny_build_0"..inst.isshiny)
				end	
			end		
		end
		if inst.isshiny ~= nil and inst.isshiny ~= 0 then
			if inst.isadult and inst.isadult == true then
				inst.AnimState:SetBuild("rabid_beetle_shiny_build_0"..inst.isshiny)
			else
				inst.AnimState:SetBuild("glowfly_shiny_build_0"..inst.isshiny)
			end	
		end
	end	
end

local function setChar(inst) --sets character when the player loads.
	if not inst:HasTag("playerghost") then
	
		if inst.isadult then
			inst.AnimState:SetBank("rabid_beetle")
			inst.AnimState:SetBuild("rabid_beetle")
	
			inst.components.health:SetMaxHealth(200)
			inst.components.hunger:SetMax(120)
			inst.components.combat:SetDefaultDamage(30*2)
			inst.components.locomotor.runspeed = 12
			inst.components.locomotor.walkspeed = 12
			
			inst.DynamicShadow:SetSize(2.5, 1.5)

			ChangeToCharacterPhysics(inst)
			inst.Transform:SetScale(mob.scale, mob.scale, mob.scale)
			inst.Transform:SetFourFaced()
			
			inst.components.hunger:SetRate(0.15)
			
			inst:RemoveTag("flying")
			inst:AddTag("monster")
			
			inst.components.eater:SetDiet({ FOODTYPE.MEAT, FOODTYPE.MEAT }, { FOODTYPE.MEAT, FOODTYPE.MEAT }) 
			inst.components.eater:SetAbsorptionModifiers(1,1.75,1) --This multiplies food stats.
			inst.components.eater:SetCanEatHorrible() --check to make them eat monster meat.
			inst.components.eater.strongstomach = true -- can eat monster meat!
			--inst.components.eater.eatwholestack = true --should eat the entire stack and get all the benefits.
			inst.components.eater.ignoresspoilage = true -- Can eat spoiled food without consequence. Might keep food stats as if fresh.
	
			inst:SetStateGraph("SGrabidbeetlep")
			
			if inst.Light then
				inst.Light:Enable(false)
			end
	
			inst.taunt2 = nil
		else
			inst.AnimState:SetBank(mob.bank)
			inst.AnimState:SetBuild(mob.build)
			inst:SetStateGraph(mob.stategraph)
			inst.components.locomotor.runspeed = (mob.runspeed)
			inst.components.locomotor.walkspeed = (mob.walkspeed)
		end	
		if MONSTERHUNGER == "Disable" then
			inst.components.hunger:Pause()
		end
		inst.components.sanity.ignore = true
		
	end
	inst.components.locomotor.fasteronroad = false	
end

local function RemovePenalty(inst)
	inst.components.health:DeltaPenalty(-1.00) --Removes health penalty when reviving
	inst.components.health:DoDelta(20000, false) --Returns health to max health upon reviving
end

local function SetSkinDefault(inst, num)
	if num ~= nil and num ~= 0 then
			if inst.isadult then 
				inst.AnimState:SetBuild("rabid_beetle_shiny_build_0"..num)
				--inst.Light:SetColour(50/255, 129/255, 255/255)
			else
				inst.AnimState:SetBuild("glowfly_shiny_build_0"..num)
			end
	end
	if num == 0 then
		setChar(inst) --resets lights and hopefully nothing else
	end
end

local common_postinit = function(inst) 
	inst.MiniMapEntity:SetIcon(mob.minimap)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false)
	  --ThePlayer.HUD.controls.status.brain:Hide()
		if MONSTERHUNGER == "Disable" then
			--ThePlayer.HUD.controls.status.stomach:Hide()
		end
   end
end)
	--inst:WatchWorldState( "onphasechanged", function() SetNightVision(inst) end)
	inst:WatchWorldState( "isday", function() SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() SetNightVision(inst)  end)
	
	SetNightVision(inst)
	
end

local function DontTriggerCreep(inst)
    inst.components.locomotor.runspeed = (inst.isadult == true and 12 or mob.runspeed)
	inst.components.locomotor.walkspeed = (mob.walkspeed)
end


-------------Forge------------------------------------------
local function CommonActualRez(inst)
    inst.player_classified.MapExplorer:EnableUpdate(true)

    if inst.components.revivablecorpse ~= nil then
        inst.components.inventory:Show()
    else
        inst.components.inventory:Open()
    end

    inst.components.health.canheal = true
    if not GetGameModeProperty("no_hunger") then
        inst.components.hunger:Resume()
    end
    if not GetGameModeProperty("no_temperature") then
        inst.components.temperature:SetTemp() --nil param will resume temp
    end
    inst.components.frostybreather:Enable()

    MakeMediumBurnableCharacter(inst, "torso")
    inst.components.burnable:SetBurnTime(TUNING.PLAYER_BURN_TIME)
    inst.components.burnable.nocharring = true

    MakeLargeFreezableCharacter(inst, "torso")
    inst.components.freezable:SetResistance(4)
    inst.components.freezable:SetDefaultWearOffTime(TUNING.PLAYER_FREEZE_WEAR_OFF_TIME)

    inst:AddComponent("grogginess")
    inst.components.grogginess:SetResistance(3)
    --inst.components.grogginess:SetKnockOutTest(ShouldKnockout)

    inst.components.moisture:ForceDry(false)

    inst.components.sheltered:Start()

    inst.components.debuffable:Enable(true)

    --don't ignore sanity any more
    inst.components.sanity.ignore = GetGameModeProperty("no_sanity")

    inst.components.age:ResumeAging()

    --ConfigurePlayerLocomotor(inst)
    --ConfigurePlayerActions(inst)

    if inst.rezsource ~= nil then
        local announcement_string = GetNewRezAnnouncementString(inst, inst.rezsource)
        if announcement_string ~= "" then
            TheNet:AnnounceResurrect(announcement_string, inst.entity)
        end
        inst.rezsource = nil
    end
    inst.remoterezsource = nil
end

local function DoActualRezFromCorpse(inst, source)
    if not inst:HasTag("corpse") then
        return
    end

    SpawnPrefab("lavaarena_player_revive_from_corpse_fx").entity:SetParent(inst.entity)

    inst.components.inventory:Hide()
    inst:PushEvent("ms_closepopups")

    --inst:SetStateGraph("SGbearplayer")
    inst.sg:GoToState("corpse_rebirth")

    inst.player_classified:SetGhostMode(false)

    local respawn_health_precent = inst.components.revivablecorpse ~= nil and inst.components.revivablecorpse:GetReviveHealthPercent() or 1

    if source ~= nil and source:IsValid() then
        if source.components.talker ~= nil then
            source.components.talker:Say(GetString(source, "ANNOUNCE_REVIVED_OTHER_CORPSE"))
        end

        if source.components.corpsereviver ~= nil then
            respawn_health_precent = respawn_health_precent + source.components.corpsereviver:GetAdditionalReviveHealthPercent()
        end
    end

    --V2C: Let stategraph do it
    --[[inst.Physics:ClearCollisionMask()
    inst.Physics:CollidesWith(COLLISION.WORLD)
    inst.Physics:CollidesWith(COLLISION.OBSTACLES)
    inst.Physics:CollidesWith(COLLISION.SMALLOBSTACLES)
    inst.Physics:CollidesWith(COLLISION.CHARACTERS)
    inst.Physics:CollidesWith(COLLISION.GIANTS)]]

    CommonActualRez(inst)

    inst.components.health:SetCurrentHealth(inst.components.health:GetMaxWithPenalty() * math.clamp(respawn_health_precent, 0, 1))
    inst.components.health:ForceUpdateHUD(true)

    inst.components.revivablecorpse:SetCorpse(false)
    if TheWorld.components.lavaarenaevent ~= nil and not TheWorld.components.lavaarenaevent:IsIntermission() then
        inst:AddTag("NOCLICK")
    end

    inst:PushEvent("ms_respawnedfromghost", { corpse = true, reviver = source })
end

local function OnRespawnFromMobCorpse(inst, data)
    if not inst:HasTag("corpse") then
        return
    end

    inst.deathclientobj = nil
    inst.deathcause = nil
    inst.deathpkname = nil
    inst.deathbypet = nil
    if inst.components.talker ~= nil then
        inst.components.talker:ShutUp()
    end

    inst:DoTaskInTime(0, DoActualRezFromCorpse, data and data.source or nil)
    inst.remoterezsource = nil

    inst.rezsource =
        data ~= nil and (
            (data.source ~= nil and data.source.prefab ~= "reviver" and data.source.name) or
            (data.user ~= nil and data.user:GetDisplayName())
        ) or
        STRINGS.NAMES.SHENANIGANS
end

local ex_fns = require "prefabs/player_common_extensions"

local function DoHealBuff(target)
    if target:IsValid() and not target.components.health:IsDead() and not target.buffimmune and not target.defbuffed and target.components.debuffable then
		
	end
end

local function DoHealPulse(inst)
	--OnPulse(inst)
	if inst.components.health and not inst.components.health:IsDead() then
    local x, y, z = inst.Transform:GetWorldPosition()
    local ents = TheSim:FindEntities(x, y, z, 4, nil, { "playerghost", "ghost", "battlestandard", "INLIMBO", "LA_MOB" } )
		if #ents > 1 then
			for i, v in ipairs(ents) do
				if v and v.components.health and not v.components.health:IsDead() then
					v.components.health:DoDelta(2, false)
				end
			end
		end
	end
end

local function SetForge(inst)
	inst.components.health:SetMaxHealth(PPHAM_FORGE.GLOWFLY.HEALTH)
	
	inst.components.combat:SetDefaultDamage(PPHAM_FORGE.GLOWFLY.DAMAGE)
	inst.components.combat:SetAttackPeriod(PPHAM_FORGE.GLOWFLY.ATTACKPERIOD)
	
	inst.components.locomotor.walkspeed = PPHAM_FORGE.GLOWFLY.WALKSPEED
    inst.components.locomotor.runspeed = PPHAM_FORGE.GLOWFLY.RUNSPEED
	
	inst.components.combat:SetRange(PPHAM_FORGE.GLOWFLY.ATTACK_RANGE, PPHAM_FORGE.GLOWFLY.HIT_RANGE)
	--inst.components.combat:SetAreaDamage(PPHAM_FORGE.HIPPO.AOE_RANGE, PPHAM_FORGE.HIPPO.AOE_DMGMULT)
	
	inst.mobsleep = false
	inst.taunt2 = false
	--inst.acidmult = 1.25
	
	inst:DoTaskInTime(0, function(inst)
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"books", "staves", "darts", "melees"})
	end)
	
	inst:DoPeriodicTask(0.5, DoHealPulse)
	
	--inst.components.combat.onhitotherfn = OnForgeHitOther
	
	inst.components.combat:SetDamageType(1)
	
	inst.components.revivablecorpse.revivespeedmult = 0.3
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", OnRespawnFromMobCorpse)
end

local INTENSITY = .75
-----------------------------------------------------------------------

local master_postinit = function(inst)
	------------------------------------------
	--Stats--
    inst.components.health:SetMaxHealth(mob.health)
	inst.components.hunger:SetMax(mob.hunger)
	inst.components.sanity:SetMax(mob.sanity)
	inst.components.hunger:SetRate(mob.hungerrate)
	------------------------------------------
	--Combat--
	inst.components.combat.playerdamagepercent = MOBPVP
	inst.components.combat:SetAttackPeriod(0)
	inst.components.combat:SetRange(2, mob.range)
	inst.components.combat:SetDefaultDamage(mob.damage) --does double damage against mobs.
	
    inst.components.temperature.inherentinsulation = -TUNING.INSULATION_SMALL
	inst.AnimState:SetBank(mob.bank)
	inst.AnimState:SetBuild(mob.build)
	
	inst.OnSetSkin = function(skin_name)
		inst.AnimState:SetBuild(mob.build)
		inst:SetStateGraph(mob.stategraph)
	
		inst:ListenForEvent("ms_respawnedfromghost", DontTriggerCreep)
		DontTriggerCreep(inst)	
	end
	inst:SetStateGraph(mob.stategraph)
	inst.components.talker:IgnoreAll()
	if MONSTERHUNGER == "Disable" then
		inst.components.hunger:Pause()
	end
	inst.components.sanity.ignore = true
	----------------------------------
	--Locomotor--
    inst.components.locomotor.runspeed = (mob.runspeed)
	inst.components.locomotor.walkspeed = (mob.walkspeed)
	inst.components.locomotor.fasteronroad = false
	
	----------------------------------
	--Loot drops--
	inst:AddComponent("lootdropper")
    inst.components.lootdropper:SetChanceLootTable("glowflyp")
	----------------------------------
	--Temperature and Weather Components--
	inst.components.temperature.maxtemp = 60 --prevents overheating
	inst.components.temperature.mintemp = 20 --prevents freezing
	----------------------------------
	--Tags--
    inst:AddTag("glowfly")
	inst:AddTag("flying")
	--inst:AddTag("groundpoundimmune")
	
	inst.mobsleep = true
	inst.taunt = true
	inst.taunt2 = true
	
	inst.mobplayer = true
	inst.ghostbuild = "ghost_monster_build"

	inst.isshiny = 0
	
	inst.getskins = getskins
	inst.setskin_defaultfn = SetSkinDefault
	
	inst.sounds = sounds
	
	inst.acidmult = 3
	inst.poisonmult = 3	
	inst.components.health.fire_damage_scale = 3
	
    MakeLargeBurnableCharacter(inst, "eyes")
    MakeSmallFreezableCharacter(inst, "eyes")
	----------------------------------
	--Eater--

	--inst.components.eater:SetDiet({ FOODTYPE.MEAT, FOODTYPE.MEAT }, { FOODTYPE.MEAT, FOODTYPE.MEAT }) 
    inst.components.eater:SetAbsorptionModifiers(1,1.75,1) --This multiplies food stats.
	inst.components.eater:SetCanEatHorrible() --check to make them eat monster meat.
	inst.components.eater.strongstomach = true -- can eat monster meat!
	--inst.components.eater.eatwholestack = true --should eat the entire stack and get all the benefits.
	inst.components.eater.ignoresspoilage = true -- Can eat spoiled food without consequence. Might keep food stats as if fresh.
	---------------------------------
	--Physics and Scale--
	
	inst.DynamicShadow:SetSize( .8, .5 )
    inst.Transform:SetSixFaced()

    MakeCharacterPhysics(inst, 5, .5)
    inst.Transform:SetScale(mob.scale, mob.scale, mob.scale)
	
	inst.Physics:SetCollisionGroup(COLLISION.FLYERS)
	inst.Physics:CollidesWith(COLLISION.FLYERS)
	
	PlayablePets.SetAmphibious(inst, nil, nil, true)
	---------------------------------
	--Listeners--
	
	inst:ListenForEvent("equip", EquipHat) --Shows head when hats make heads disappear.
	---------------------------------
	--Functions that saves and loads data.

    inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
	--Forge--
	if inst.components.revivablecorpse then
		inst.forge_fn = SetForge(inst)
	end
    ------------------------------------------------------
	--Light and Character Functions. Don't Touch.--
	local light = inst.entity:AddLight()
    light:SetFalloff(.7)
    light:SetIntensity(INTENSITY)
    light:SetRadius(2)
    light:SetColour(120/255, 120/255, 120/255)
    light:Enable(true)
	
	inst.SoundEmitter:PlaySound(inst.sounds.buzz, "buzz")
	
    if MOBNIGHTVISION ~= "Disable2" then inst.components.grue:AddImmunity("mobplayer") end
	
	inst:ListenForEvent("respawnfromghost", RestoreNightImmunity)

    inst:DoTaskInTime(0, setChar) --Sets Character.
	inst:DoTaskInTime(3, setSkin)
    inst:ListenForEvent("respawnfromghost", function() --Runs functions when character revives from Ghost.
        inst:DoTaskInTime(5, setChar)
		inst:DoTaskInTime(5.1, setSkin)
		inst:DoTaskInTime(6, RemovePenalty) --Removes death penalties and restore health if wanted.
		inst:DoTaskInTime(6, DontTriggerCreep)-- Restores speed on revive.
		inst:DoTaskInTime(10, RestoreNightImmunity) --explains itself.
    end)
	
    return inst
	
end

return MakePlayerCharacter("glowflyp", prefabs, assets, common_postinit, master_postinit, start_inv)
