local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--
---------------------------
local pu = require ("prefabs/pugaliskp_util")

local function ShakeIfClose(inst)
    ShakeAllCameras(CAMERASHAKE.VERTICAL, .5, .03, .7, inst, 30)
end

local assets =
{	
    Asset("ANIM", "anim/python.zip"),
    Asset("ANIM", "anim/python_test.zip"),
    Asset("ANIM", "anim/python_segment_broken02_build.zip"),    
    Asset("ANIM", "anim/python_segment_broken_build.zip"),    
    Asset("ANIM", "anim/python_segment_build.zip"),                            
    Asset("ANIM", "anim/python_segment_tail02_build.zip"), 
    Asset("ANIM", "anim/python_segment_tail_build.zip"),     
}

--Might be completely pointless here. I don't know.
local prefabs = 
{	

}
	
--Note: Becareful when adding starting items.
--People are likely to spam changecharacter to get more of them.	
local start_inv = 
{
	--'prefab',
	--"spear",
}

local start_inv2 =
{
   --"beehome",
}

if MOBHOUSE == "Enable1" or MOBHOUSE == "Enable3" then
	start_inv = start_inv2
end
-----------------------
--Stats--
local mob = 
{
	health = 4000,
	hunger = 350,
	hungerrate = TUNING.WILSON_HUNGER_RATE, 
	sanity = 100,
	runspeed = 5,
	walkspeed = 5,
	damage = 150*2,
	range = 20,
	hit_range = 6,
	attackperiod = 3,
	bank = "giant_snake",
	build = "python_test",
	shiny = "pugalisk",
	--build2 = "bee_guard_puffy_build",
	scale = 1.5,
	stategraph = "SGpugaliskp",
	minimap = "pugaliskp.tex",
	
}
-----------------------
--Loot that drops when you die, duh.
SetSharedLootTable('pugaliskp',
{
    {'monstermeat',     1.0},
    {'monstermeat',     1.0},
	{'monstermeat',     1.0},
})

local function GetExcludeTags(inst)
	if TheNet:GetPVPEnabled() then
		return {"INLIMBO", "playerghost", "notarget", "shadow", "pugalisk", "wall", "groundpoundimmune"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion", "playerghost", "INLIMBO", "notarget", "groundpoundimmune", "pugalisk"}
	else	
		return {"player", "companion", "INLIMBO", "playerghost", "notarget", "shadow", "pugalisk", "wall", "groundpoundimmune"}
	end
end

local function OnForgeHitOther(inst, other)
    SpawnPrefab("forgeelectricute_fx"):SetTarget(other, true)
end
------------------------------------------------------
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
end
-------------------------------------------------------

local common_postinit = function(inst) 
	inst.MiniMapEntity:SetIcon(mob.minimap)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false)
	  --ThePlayer.HUD.controls.status.brain:Hide()
		if MONSTERHUNGER == "Disable" then
			--ThePlayer.HUD.controls.status.stomach:Hide()
		end
   end
end)

	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst)
	
	MakeInventoryFloatable(inst, "large", 0.6, {0.6, 1.5, 0.6})
	
end

-------------Forge------------------------------------------
local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	inst.components.health:SetMaxHealth(PPHAM_FORGE.PUGALISK.HEALTH)
	
	inst.components.combat:SetDefaultDamage(PPHAM_FORGE.PUGALISK.DAMAGE)
	inst.components.combat:SetAttackPeriod(PPHAM_FORGE.PUGALISK.ATTACKPERIOD)
	
	inst.components.locomotor.walkspeed = PPHAM_FORGE.PUGALISK.WALKSPEED
    inst.components.locomotor.runspeed = PPHAM_FORGE.PUGALISK.RUNSPEED
	
	inst.components.combat:SetRange(PPHAM_FORGE.PUGALISK.ATTACK_RANGE, PPHAM_FORGE.PUGALISK.HIT_RANGE)
	--inst.components.combat:SetAreaDamage(PPHAM_FORGE.TREEGUARD.AOE_RANGE, PPHAM_FORGE.TREEGUARD.AOE_DMGMULT)
	inst.components.health:StopRegen()
	inst.mobsleep = false
	
	--inst.acidmult = 1.25
	inst.healmult = 1
	
	
	inst:DoTaskInTime(0, function(inst)
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"melees", "books", "staves", "darts", "armors"})
	end)
	
	inst.components.combat:SetDamageType(1)
	
	inst.components.revivablecorpse.revivespeedmult = 2
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end
-----------------------------------------------------------------------

local function redirecthealth(inst, amount, overtime, cause, ignore_invincible, afflicter, ignore_absorb)
	----print("DEBUG: Running redirect health for "..inst.prefab)
	----print("DEBUG: Pugalisk part HP is "..inst.components.health and inst.components.health.currenthealth or 0)

    if amount < 0 and (inst.components.segmentedp and inst.components.segmentedp.vulnerablesegments == 0) and inst.host and inst.host:IsValid() and inst.host.components and inst.host.components.health and not inst.components.health:IsDead() then
			----print("DEBUG: Armored redirect passed for "..inst.prefab)
		if inst.host and inst.host.components.health and not inst.host.components.health:IsDead() then
			inst.host.components.health:DoDelta(amount*0.01, overtime, cause, ignore_invincible, afflicter, true)
		end		
        inst.SoundEmitter:PlaySound("dontstarve/common/destroy_metal",nil,.25)
        inst.SoundEmitter:PlaySound("dontstarve/wilson/hit_metal")

    elseif amount < 0 and inst.host and inst.host:IsValid() and (inst.components.segmentedp and inst.components.segmentedp.vulnerablesegments > 0) then
		----print("DEBUG: Weakspot redirect passed for "..inst.prefab)

		if inst.host and inst.host.components.health and not inst.host.components.health:IsDead() then
			inst.host.components.health:DoDelta(amount, overtime, cause, ignore_invincible, afflicter, true)
			inst.host:PushEvent("attacked", {damage = amount, attacker = afflicter, weakspot = true})	
		end
	elseif amount > 0 and inst.host and inst.host:IsValid() then --this is you!
		--print("DEBUG: Head redirect passed for "..inst.prefab)
		if inst.host.components.health and not inst.host.components.health:IsDead() then
			inst.host.components.health:DoDelta(amount)
		end 
		
    end   
	return true
end


local function RetargetTailFn(inst)  

    local targetDist = 40

    local notags = {"FX", "NOCLICK","INLIMBO"}
    return FindEntity(inst, targetDist, function(guy)
        return (guy:HasTag("character") or guy:HasTag("animal") or guy:HasTag("monster") and not guy:HasTag("pugalisk")) and (inst.host and guy ~= inst.host)
               and inst.components.combat:CanTarget(guy)
    end, nil, GetExcludeTags())
end

local function RetargetFn(inst)  

    local targetDist = 40

    local notags = {"FX", "NOCLICK","INLIMBO"}
    return FindEntity(inst, targetDist, function(guy)
        return (guy:HasTag("character") or guy:HasTag("animal") or guy:HasTag("monster") and not guy:HasTag("pugalisk")) and (inst.host and guy ~= inst.host)
               and inst.components.combat:CanTarget(guy)
    end, nil, GetExcludeTags())

end

local function OnHitSegment(inst, attacker)    
    local host = inst
    if inst.host then
        host = inst.host      
    end    

    if attacker then
        host.target = attacker 
        host.components.combat.target = attacker
    end
    
end

local function segmentfn(Sim)
    local inst = CreateEntity()
    local trans = inst.entity:AddTransform()
    local anim = inst.entity:AddAnimState()
    local sound = inst.entity:AddSoundEmitter()
	inst.entity:AddNetwork()
	
	----print("Segment spawned")
	
    local s = 1.5
    trans:SetScale(s,s,s)
    inst.Transform:SetEightFaced()

    inst.AnimState:SetFinalOffset( -10 )
    
    anim:SetBank("giant_snake")
    anim:SetBuild("python_test")
    anim:PlayAnimation("test_segment")

    inst:AddTag("NOCLICK")
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst:AddTag("companion")
	end
	
	inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

	inst.invulnerable = true
	
	inst:AddComponent("named")
	inst:DoTaskInTime(0, function(inst)
		inst.components.named:SetName(inst.host and inst.host:GetDisplayName() or "Unknown Pugalisk")
	end)
	
	--inst:AddComponent("combat")
   -- inst.components.combat:SetDefaultDamage(0)
    --inst.components.combat.hiteffectsymbol = "test_segments"-- "wormmovefx"
    --inst.components.combat.onhitfn = OnHitSegment    

	inst.poisonimmune = true
	inst.acidimmune = true
	
    --inst:AddComponent("health")
    --inst.components.health:SetMaxHealth(9999)
    --inst.components.health.destroytime = 5
   -- inst.components.health.redirect = redirecthealth
    --inst.components.health:StartRegen(1, 2)
	
    --inst:AddComponent("inspectable")
   -- inst.components.inspectable.nameoverride = "pugalisk"  
    --inst.name = inst.host and inst.host:GetDisplayName() or "Unknown Pugalisk"

    inst:AddComponent("lootdropper")
    inst.components.lootdropper.lootdropangle = 360
    inst.components.lootdropper.speed = 3 + (math.random()*3)

    inst.AnimState:Hide("broken01")
    inst.AnimState:Hide("broken02")

    inst.persists = false

    return inst
end

--======================================================================

local function RemoveSegments(inst)
	inst.components.health:StopRegen()
	inst.components.multibodyp:KillAll()
end

local function segment_deathfn(segment)

    segment.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/pugalisk/explode")

    local pt= Vector3(segment.Transform:GetWorldPosition())
	if TheNet:GetServerGameMode() ~= "lavaarena" then
		if math.random() < 0.5 then
			local bone = segment.components.lootdropper:SpawnLootPrefab("boneshard",pt)
       
			if math.random()<0.6 then
				local bone = segment.components.lootdropper:SpawnLootPrefab("monstermeat",pt)
			end        
			if math.random()<0.2 then
				local bone = segment.components.lootdropper:SpawnLootPrefab("boneshard",pt)
			end
			if math.random()<0.005 then
				local bone = segment.components.lootdropper:SpawnLootPrefab("redgem", pt)
			end
			if math.random()<0.005 then
				local bone = segment.components.lootdropper:SpawnLootPrefab("bluegem", pt)
			end
			if math.random()<0.05 then
				local bone = segment.components.lootdropper:SpawnLootPrefab("monstermeat", pt)
			end
		end
	end
    
    local fx = SpawnPrefab("snake_scales_fxp")    
    fx.Transform:SetScale(1.5,1.5,1.5)
    fx.Transform:SetPosition(pt.x,pt.y + 2 + math.random()*2,pt.z)
end

local function bodyfn()

    local inst = CreateEntity()
    local trans = inst.entity:AddTransform()
    local anim = inst.entity:AddAnimState()
    local sound = inst.entity:AddSoundEmitter()
	inst.entity:AddNetwork()

	----print("body spawned")
	
    inst.Transform:SetSixFaced()
    
    local s = 1.5
    trans:SetScale(s,s,s)
    
    MakeObstaclePhysics(inst, 1)

    anim:SetBank("giant_snake") 
    anim:SetBuild("python_test")
    anim:PlayAnimation("dirt_static")

    anim:Hide("broken01")
    anim:Hide("broken02")

    inst.AnimState:SetFinalOffset( 0 )

    --inst.name = STRINGS.NAMES.PUGALISK
    inst.invulnerable = true

    ------------------------------------------

    inst:AddTag("epic")
    inst:AddTag("monster")
    inst:AddTag("pugalisk")
    inst:AddTag("scarytoprey")
    inst:AddTag("groundpoundimmune")
    inst:AddTag("noteleport")
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst:AddTag("companion")
	end
	if CurrentRelease.GreaterOrEqualTo(ReleaseID.IDs.R08_ROT_TURNOFTIDES) then
		MakeInventoryFloatable(inst, "large", 0.1, {0.6, 1.0, 0.6})
	end
	
	inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end
	
	inst:DoTaskInTime(0, function(inst)
		if PlayablePets.CheckWaterTile(inst:GetPosition()) and inst.AnimState:IsCurrentAnimation("dirt_static") then
			LandFlyingCreature(inst)
			inst.AnimState:Hide("wormmovefx")
			inst.AnimState:PlayAnimation("thisisbroken")
		end
	end)

    ------------------
	inst:AddComponent("named")
	inst:DoTaskInTime(0, function(inst)
		inst.components.named:SetName(inst.host and inst.host:GetDisplayName() or "Unknown Pugalisk")
	end)
	
    
    inst:AddComponent("health")
    inst.components.health:SetMaxHealth(9999)
    inst.components.health.destroytime = 5
    inst.components.health.redirect = redirecthealth


    inst:ListenForEvent("death", function(inst, data)
        
    end)

    ------------------

    inst:AddComponent("combat")
    inst.components.combat:SetDefaultDamage(TheNet:GetServerGameMode() == "lavaarena" and PPHAM_FORGE.PUGALISK.DAMAGE or mob.damage)
    inst.components.combat.playerdamagepercent = MOBPVP
	inst.components.combat.areahitdamagepercent = TheNet:GetServerGameMode() == "lavaarena" and 0.2 or 0.1
	
	inst.poisonimmune = true
	inst.acidimmune = true

    inst.components.combat.hiteffectsymbol = "hit_target"
    --inst.components.combat.onhitfn = OnHitSegment
    
    ------------------------------------------

    inst:AddComponent("lootdropper")

    ------------------------------------------

    inst:AddComponent("inspectable")
    inst.components.inspectable.nameoverride = "pugalisk"
    ------------------------------------------

    inst:AddComponent("knownlocations")

    ------------------------------------------

    inst:AddComponent("groundpounder")
    inst.components.groundpounder.destroyer = false
    inst.components.groundpounder.damageRings = 0
    inst.components.groundpounder.destructionRings = 0
    inst.components.groundpounder.numRings = 2
    inst.components.groundpounder.groundpounddamagemult = TheNet:GetServerGameMode() == "lavaarena" and mob.damage/5 or mob.damage/10
    --inst.components.groundpounder.groundpoundfx= "groundpound_nosound_fx"

    ------------------------------------------

    inst:AddComponent("segmentedp")
    inst.components.segmentedp.segment_deathfn = segment_deathfn
	
	
    inst:ListenForEvent("bodycomplete", function() 
                if inst.exitpt then
                    inst.exitpt.AnimState:SetBank("giant_snake")
                    inst.exitpt.AnimState:SetBuild("python_test")
                    inst.exitpt.AnimState:PlayAnimation("dirt_static")  
					if PlayablePets.CheckWaterTile(inst.exitpt:GetPosition()) and CurrentRelease.GreaterOrEqualTo(ReleaseID.IDs.R08_ROT_TURNOFTIDES) then
						LandFlyingCreature(inst.exitpt)
						inst.AnimState:Hide("wormmovefx")
						inst.exitpt.AnimState:PlayAnimation("thisisbroken")
					end
                    --local player = GetClosestInstWithTag("player", inst, 15)
                    --if player then
                        --player.components.playercontroller:ShakeCamera(inst, "VERTICAL", 0.5, 0.03, 2, 15)
                    --end

                    --TheCamera:Shake("VERTICAL", 0.5, 0.05, 0.1)
                    inst.exitpt.Physics:SetActive(true)
					if not PlayablePets.CheckWaterTile(inst.exitpt:GetPosition()) then
						if inst.host then
							inst.host.components.combat:DoAreaAttack(inst, 5, nil, nil, nil, GetExcludeTags())
						end
						inst.exitpt.components.groundpounder:GroundPound()  
						ShakeIfClose(inst)
						inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/pugalisk/emerge","emerge")
						inst.SoundEmitter:SetParameter( "emerge", "start", math.random() )
					else
						inst.SoundEmitter:PlaySound("turnoftides/common/together/water/splash/large")
						local splash = SpawnPrefab(CurrentRelease.GreaterOrEqualTo(ReleaseID.IDs.R09_ROT_SALTYDOG) and "splash_green" or "splash_water")
						splash.Transform:SetScale(2, 2, 2)
						splash.Transform:SetPosition(inst.exitpt:GetPosition():Get())
					end	

                    

                    --if inst.host then   
                        --inst.host:PushEvent("bodycomplete",{ pos=Vector3(inst.exitpt.Transform:GetWorldPosition()), angle = inst.angle })                            
                    --end                                     
                end
            end) 
	
    inst:ListenForEvent("bodyfinished", function() 
                if inst.host then  
                    inst.host:PushEvent("bodyfinished",{ body=inst })                                                
                end                      
                inst:Remove()               
            end)

    inst.persists = false

    inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/pugalisk/movement_LP", "speed")
    inst.SoundEmitter:SetParameter("speed", "intensity", 0)

    ------------------------------------------

    return inst
end

--===========================================================

local function tailfn(Sim)

    local inst = CreateEntity()
    local trans = inst.entity:AddTransform()
    local anim = inst.entity:AddAnimState()
    local sound = inst.entity:AddSoundEmitter()
	inst.entity:AddNetwork()

    inst.Transform:SetSixFaced()
    
    local s = 1.5
    trans:SetScale(s,s,s)
    
    MakeObstaclePhysics(inst, 1)

    anim:SetBank("giant_snake")
    anim:SetBuild("python_test") 
    anim:PlayAnimation("tail_idle_loop", true)

    anim:Hide("broken01")
    anim:Hide("broken02")

    inst.AnimState:SetFinalOffset( 0 )

    --inst.name = STRINGS.NAMES.PUGALISK
    inst.invulnerable = true

    ------------------------------------------

    inst:AddTag("tail")
    inst:AddTag("epic")
    inst:AddTag("monster")
    inst:AddTag("pugalisk")
    inst:AddTag("scarytoprey")
	inst:AddTag("notarget")
	
	if CurrentRelease.GreaterOrEqualTo(ReleaseID.IDs.R08_ROT_TURNOFTIDES) then
	MakeInventoryFloatable(inst, "large", 0.6, {0.6, 1.0, 0.6})
	end
    --inst:AddTag("groundpoundimmune")
    inst:AddTag("noteleport")
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst:AddTag("companion")
	end
	
	
	inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end
	
	inst.poisonimmune = true
	inst.acidimmune = true

	
	inst:DoTaskInTime(0, function(inst)
		if CurrentRelease.GreaterOrEqualTo(ReleaseID.IDs.R08_ROT_TURNOFTIDES) then
			if IsOceanTile(TheWorld.Map:GetTileAtPoint(inst:GetPosition():Get())) then
				LandFlyingCreature(inst)
				inst.AnimState:Hide("wormmovefx")
			end
		end	
	end)
	
    ------------------------------------------
	inst:AddComponent("named")
	inst:DoTaskInTime(0, function(inst)
		inst.components.named:SetName(inst.host and inst.host:GetDisplayName() or "Pugalisk")
	end)
	
    
    inst:AddComponent("health")
    inst.components.health:SetMaxHealth(9999)
	inst.components.health:SetInvincible(true)
    inst.components.health.destroytime = 5
    --inst.components.health.redirect = redirecthealth
	

    ------------------------------------------  

    inst:AddComponent("combat")
    inst.components.combat:SetDefaultDamage(TheNet:GetServerGameMode() == "lavaarena" and PPHAM_FORGE.PUGALISK.DAMAGE/2 or mob.damage/2)
    inst.components.combat.playerdamagepercent = MOBPVP/2
    inst.components.combat:SetRange(5, 5)
    inst.components.combat.hiteffectsymbol = "hit_target" -- "wormmovefx"
    inst.components.combat:SetAttackPeriod(3/2)
    inst.components.combat:SetRetargetFunction(0.5, RetargetTailFn)
    --inst.components.combat.onhitfn = OnHitSegment

    ------------------------------------------

    inst:AddComponent("locomotor")

    ------------------------------------------

    inst:AddComponent("lootdropper")

    ------------------------------------------

    inst:AddComponent("inspectable")
    inst.components.inspectable.nameoverride = "pugalisk"

    ------------------------------------------

    inst.persists = false
    ------------------------------------------
	local brain = require "brains/Pugaliskp_tailBrain"
    inst:SetBrain(brain)    
	
    inst:SetStateGraph("SGpugaliskp_tail")

    return inst
end

local function onhostdeath(inst)

    --TheCamera:Shake("FULL",3, 0.05, .2)
    local mb = inst.components.multibodyp
    for i,body in ipairs(mb.bodies)do
		if body and body.components.health then
			body.components.health:Kill()
		end	
    end
    if mb.tail then
		mb.tail:Remove()
        --mb.tail.components.health:Kill()
    end    
    mb:KillAll()
	inst.components.health:StopRegen()
end

--===========================================================

local master_postinit = function(inst)
	------------------------------------------
	--Stats--
    PlayablePets.SetCommonStats(inst, mob) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20, 1) --heat, cold, wetness
	inst.components.health:SetAbsorptionAmount(0.95)
	if not inst.components.revivablecorpse then
	inst.components.health:StartRegen(5, 3)
	end
	------------------------------------------
	--Combat--
	inst.components.combat.areahitdamagepercent = TheNet:GetServerGameMode() == "lavaarena" and 0.2 or 0.1
	----------------------------------
	--Loot drops--
	inst:AddComponent("lootdropper")
    inst.components.lootdropper:SetChanceLootTable("pugaliskp")
	----------------------------------
	--Temperature and Weather Components--
	inst:RemoveComponent("burnable")
	----------------------------------
	--Tags--
    inst:AddTag("head")
	inst:AddTag("epic")
	inst:AddTag("pugalisk")
	inst:AddTag("monster")
	
	--inst.mobsleep = true
	inst.taunt = true
	--inst.taunt2 = true
	
	inst.wantstogaze = true

	inst.poisonimmune = true
	inst.acidimmune = true
	inst.mobplayer = true
	inst.ghostbuild = "ghost_monster_build"
	inst.isshiny = 0
	PlayablePets.SetCommonStatResistances(inst, 0, 0, 0) --fire, acid, poison
	----------------------------------
	--Multibody--
	inst:AddComponent("multibodyp")    
    inst.components.multibodyp:Setup(PUGSEG,"pugaliskp_body") 
	
	--inst:AddComponent("segmentedp")

	inst:ListenForEvent("bodycomplete", function(inst, data) 
		--print("bodycomplete pushed")
        --local pt = pu.findsafelocation( data.pos , data.angle/DEGREES )
        --inst.Transform:SetPosition(pt.x,0,pt.z)
        inst:DoTaskInTime(0.75, function() 

            --local player = GetClosestInstWithTag("player", inst, SHAKE_DIST)
            --if player then                                   
                --player.components.playercontroller:ShakeCamera(inst, "VERTICAL", 0.3, 0.03, 1, SHAKE_DIST)
            --end 
			if not PlayablePets.CheckWaterTile(inst:GetPosition()) then
				inst.components.groundpounder:GroundPound()
				ShakeIfClose(inst)
            
				inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/boss/pugalisk/emerge","emerge")
				inst.SoundEmitter:SetParameter( "emerge", "start", math.random() )
			else
				local splash = SpawnPrefab(CurrentRelease.GreaterOrEqualTo(ReleaseID.IDs.R09_ROT_SALTYDOG) and "splash_green" or "splash_water")
				splash.Transform:SetScale(2, 2, 2)
				splash.Transform:SetPosition(inst:GetPosition():Get())
				inst.SoundEmitter:PlaySound("turnoftides/common/together/water/splash/large")
			end	
            
            --pu.DetermineAction(inst)
        end)
    end)

	inst:ListenForEvent("bodyfinished", function(inst, data) 
		----print("removing body")
        inst.components.multibodyp:RemoveBody(data.body)
    end)
	
	inst:ListenForEvent("onremove", RemoveSegments)
	inst:ListenForEvent("death", onhostdeath)
	----------------------------------
	--Groundpounder--
	inst:AddComponent("groundpounder")
    inst.components.groundpounder.destroyer = false
    inst.components.groundpounder.damageRings = 0
    inst.components.groundpounder.destructionRings = 0
    inst.components.groundpounder.numRings = 2
    inst.components.groundpounder.groundpounddamagemult = 0.5
    --inst.components.groundpounder.groundpoundfx= "groundpound_nosound_fx"
	
	
	
	----------------------------------
	--Eater--
	inst.components.eater:SetDiet({ FOODTYPE.MEAT, FOODTYPE.MEAT }, { FOODTYPE.MEAT, FOODTYPE.MEAT }) 
	inst.components.eater:SetCanEatHorrible() --check to make them eat monster meat.
	inst.components.eater.strongstomach = true -- can eat monster meat!
	inst.components.eater:SetAbsorptionModifiers(1,3,1) --This multiplies food stats.
	inst.components.eater.eatwholestack = true --should eat the entire stack and get all the benefits.
	inst.components.eater.ignoresspoilage = true -- Can eat spoiled food without consequence. Might keep food stats as if fresh.
	---------------------------------
	--Physics and Scale--
	
	inst.DynamicShadow:SetSize(1.5, 0.75)
	inst.DynamicShadow:Enable(false)

    MakeCharacterPhysics(inst, 50, 1)
	inst.Physics:SetMass(99999)
	--MakeObstaclePhysics(inst, 1)
    inst.Transform:SetScale(mob.scale, mob.scale, mob.scale)
    inst.Transform:SetSixFaced()
	
	PlayablePets.SetAmphibious(inst, mob.bank, mob.bank)
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	---------------------------------
	--Functions that saves and loads data.

    inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
	--Forge--
	if inst.components.revivablecorpse then
		inst.forge_fn = SetForge(inst)
	end
    ------------------------------------------------------
	--Light and Character Functions. Don't Touch.--
    if MOBNIGHTVISION ~= "Disable2" then inst.components.grue:AddImmunity("mobplayer") end
	
	inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob, nil, true, true) inst.components.multibodyp:Setup(PUGSEG,"pugaliskp_body")  end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob, nil, true, true) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob, nil, true, true) inst.components.multibodyp:Setup(PUGSEG,"pugaliskp_body")  end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman, amphibious)
    end)
	
    return inst
	
end

return MakePlayerCharacter("pugaliskp", prefabs, assets, common_postinit, master_postinit, start_inv),
		Prefab( "pugaliskp_body", bodyfn, assets, prefabs),
        Prefab( "pugaliskp_tail", tailfn, assets, prefabs),
        Prefab( "pugaliskp_segment", segmentfn, assets, prefabs)
