
------------------------------------------------------------------
-- Recipe strings
------------------------------------------------------------------
STRINGS.RECIPE_DESC.LAWNDECOR1P = "Fancy plants for a fancy yard"
STRINGS.RECIPE_DESC.LAWNDECOR2P = STRINGS.RECIPE_DESC.LAWNDECOR1P
STRINGS.RECIPE_DESC.LAWNDECOR3P = STRINGS.RECIPE_DESC.LAWNDECOR1P
STRINGS.RECIPE_DESC.LAWNDECOR4P = STRINGS.RECIPE_DESC.LAWNDECOR1P
STRINGS.RECIPE_DESC.LAWNDECOR5P = STRINGS.RECIPE_DESC.LAWNDECOR1P
STRINGS.RECIPE_DESC.LAWNDECOR6P = STRINGS.RECIPE_DESC.LAWNDECOR1P
STRINGS.RECIPE_DESC.LAWNDECOR7P = STRINGS.RECIPE_DESC.LAWNDECOR1P

STRINGS.RECIPE_DESC.CITY_LAMPP = "Keep citizens safe at night"
STRINGS.RECIPE_DESC.CITY_PLAYER_HOUSEP = "A house for the classiest pigs"
STRINGS.RECIPE_DESC.CITY_PLAYER_GUARDTOWERP = "A tower for your guards"
STRINGS.RECIPE_DESC.MANDRAKEHOUSEP = "A house for the noisest turnips"
STRINGS.RECIPE_DESC.ANTHILLP = "A house for the busiest of mants"

STRINGS.RECIPE_DESC.DUNGBALLP = "Your favorite form of transportation"
STRINGS.RECIPE_DESC.PIGBANDIT_HIDEOUTP = "Whats a theif without his secret stash?"

STRINGS.RECIPE_DESC.PIG_SHOP_PRODUCEP = "For when you're too rich to farm"
STRINGS.RECIPE_DESC.PIG_SHOP_HOOFSPAP = "Mudspas are actually pretty nice"
STRINGS.RECIPE_DESC.PIG_SHOP_DELIP = "Finally, a less dangerous source of food"
STRINGS.RECIPE_DESC.PIG_SHOP_BANKP = "Better manage your oincs"
STRINGS.RECIPE_DESC.PIG_SHOP_WEAPONSP = "Don't worry, theres no wait limit"
STRINGS.RECIPE_DESC.PIG_SHOP_FLORISTP = "Run by pansies, for pansies"
STRINGS.RECIPE_DESC.PIG_SHOP_HATSHOPP = "Hats galore at the dollar store"
STRINGS.RECIPE_DESC.PIG_SHOP_ANTIQUITIESP = "For all your weird needs"
STRINGS.RECIPE_DESC.PIG_SHOP_GENERALP = "All your general needs here"
STRINGS.RECIPE_DESC.PIG_SHOP_ARCANEP = "Shiftiest store in town"
STRINGS.RECIPE_DESC.PIGHOUSE_CITYP = "Convince your subjects to move in"

STRINGS.RECIPE_DESC.OINCP = "An oinc a day keeps the curse away"
STRINGS.RECIPE_DESC.OINC10P = "Moving on up in Pig Society"
STRINGS.RECIPE_DESC.OINC100P = "Time to become the 1%"
------------------------------------------------------------------
-- Character select screen strings
------------------------------------------------------------------
STRINGS.CHARACTER_TITLES.snapdragonp = "Snapdragon"
STRINGS.CHARACTER_NAMES.snapdragonp = "Snapdragon"
STRINGS.CHARACTER_DESCRIPTIONS.snapdragonp = "*Is bulky\n*Eats bugs and converts them into food\n*Grows by contributing to nature"
STRINGS.CHARACTER_QUOTES.snapdragonp = "Literally the 'Nature Freak' mob"

STRINGS.CHARACTER_TITLES.chickenp = "Chicken"
STRINGS.CHARACTER_NAMES.chickenp = "CHICKEN"
STRINGS.CHARACTER_DESCRIPTIONS.chickenp = "*Is a chicken\n*Is frail\n*Can fly"
STRINGS.CHARACTER_QUOTES.chickenp = "Hamlet's lil doydoy"

STRINGS.CHARACTER_TITLES.scorpionp = "Scorpion"
STRINGS.CHARACTER_NAMES.scorpionp = "SCORPION"
STRINGS.CHARACTER_DESCRIPTIONS.scorpionp = "*Is a monster\n*Is poisonous\n*Can dodge"
STRINGS.CHARACTER_QUOTES.scorpionp = "Its pretty much just a spider with a tail"

STRINGS.CHARACTER_TITLES.robinp = "Ro Bin"
STRINGS.CHARACTER_NAMES.robinp = "RO BIN"
STRINGS.CHARACTER_DESCRIPTIONS.robinp = "*Is bulky\n*Is a glutton\n*Can fly over water"
STRINGS.CHARACTER_QUOTES.robinp = "Big Bird but if he was thick"

STRINGS.CHARACTER_TITLES.pidgeon2p = "City Pigeon"
STRINGS.CHARACTER_NAMES.pidgeon2p = "City Pigeon"
STRINGS.CHARACTER_DESCRIPTIONS.pidgeon2p = "*Is a bird\n*Is very weak\n*Can fly"
STRINGS.CHARACTER_QUOTES.pidgeon2p = "Pretty much the same as the gorge one"

STRINGS.CHARACTER_TITLES.kingfisherp = "Kingfisher"
STRINGS.CHARACTER_NAMES.kingfisherp = "KINGFISHER"
STRINGS.CHARACTER_DESCRIPTIONS.kingfisherp = "*Is a bird\n*Is very weak\n*Can fly"
STRINGS.CHARACTER_QUOTES.kingfisherp = "Loves fish I guess?"

STRINGS.CHARACTER_TITLES.parrot2p = "Blue Parrot"
STRINGS.CHARACTER_NAMES.parrot2p = "BLUE PARROT"
STRINGS.CHARACTER_DESCRIPTIONS.parrot2p = "*Is a bird\n*Is very weak\n*Can fly"
STRINGS.CHARACTER_QUOTES.parrot2p = "Good thing this one doesn't talk"

STRINGS.CHARACTER_TITLES.pigghostp = "Pig Ghost"
STRINGS.CHARACTER_NAMES.pigghostp = "PIG GHOST"
STRINGS.CHARACTER_DESCRIPTIONS.pigghostp = "*Is a ghost\n*Can go invisible\n*Can phase through anything"
STRINGS.CHARACTER_QUOTES.pigghostp = "When your own food haunts you"

STRINGS.CHARACTER_TITLES.peagawkp = "Peagawk"
STRINGS.CHARACTER_NAMES.peagawkp = "PEAGAWK"
STRINGS.CHARACTER_DESCRIPTIONS.peagawkp = "*Is a gobbler\n*Can attack\n*Transforms during a fullmoon"
STRINGS.CHARACTER_QUOTES.peagawkp = "A much better Gobbler, no wonder everyone wants it dead"

STRINGS.CHARACTER_TITLES.ancient_hulkp = "Large Iron Hulk"
STRINGS.CHARACTER_NAMES.ancient_hulkp = "Large Iron Hulk"
STRINGS.CHARACTER_DESCRIPTIONS.ancient_hulkp = "*Is a giant\n*Has a wide arrange of attacks\n*Is slow"
STRINGS.CHARACTER_QUOTES.ancient_hulkp = "ANCIENT ROBOTS ASSEMBLE!"

STRINGS.CHARACTER_TITLES.pikop = "Piko"
STRINGS.CHARACTER_NAMES.pikop = "PIKO"
STRINGS.CHARACTER_DESCRIPTIONS.pikop = "*Is a small animal\n*Can eat just about anything\n*Transforms during the fullmoon"
STRINGS.CHARACTER_QUOTES.pikop = "So good at stealing that it puts Krampus to shame"

STRINGS.CHARACTER_TITLES.pogp = "Pog"
STRINGS.CHARACTER_NAMES.pogp = "POG"
STRINGS.CHARACTER_DESCRIPTIONS.pogp = "*Is cute\n*Transforms during the fullmoon"
STRINGS.CHARACTER_QUOTES.pogp = "Sissy Hound"

STRINGS.CHARACTER_TITLES.dungbeetlep = "Dungbeetle"
STRINGS.CHARACTER_NAMES.dungebeetlep = "DUNGBEETLE"
STRINGS.CHARACTER_DESCRIPTIONS.dungbeetlep = "*Loves to eat poop\n*Can craft poopballs\n*Can ride poopballs"
STRINGS.CHARACTER_QUOTES.dungbeetlep = "He loves to eat poop"

STRINGS.CHARACTER_TITLES.grabbingvinep = "Vine"
STRINGS.CHARACTER_NAMES.grabbingvinep = "VINE"
STRINGS.CHARACTER_DESCRIPTIONS.grabbingvinep = "*Is a totally normal vine\n*is hard to track\n*Is weak to fire"
STRINGS.CHARACTER_QUOTES.grabbingvinep = "When tentacle hentai wasn't enough"

STRINGS.CHARACTER_TITLES.gnatp = "Gnats"
STRINGS.CHARACTER_NAMES.gnatp = "GNATS"
STRINGS.CHARACTER_DESCRIPTIONS.gnatp = "*Extremely weak to acid, poison, and fire\n*Rots anything they eat\n*Have a chance to dodge attacks"
STRINGS.CHARACTER_QUOTES.gnatp = "The Best of the Pest"

STRINGS.CHARACTER_TITLES.pangoldenp = "Pangolden"
STRINGS.CHARACTER_NAMES.pangoldenp = "PANGOLDEN"
STRINGS.CHARACTER_DESCRIPTIONS.pangoldenp = "*Likes to drink\n*Can poop gold after eating\n*Can hide in a ball"
STRINGS.CHARACTER_QUOTES.pangoldenp = "It'll slurp anything"

STRINGS.CHARACTER_TITLES.frog2p = "Poison Dartfrog"
STRINGS.CHARACTER_NAMES.frog2p = "Poison Dartfrog"
STRINGS.CHARACTER_DESCRIPTIONS.frog2p = "*Is a frog\n*Can swim\n*Is poisonous"
STRINGS.CHARACTER_QUOTES.frog2p = "Do not lick it"

STRINGS.CHARACTER_TITLES.billp = "Platapine"
STRINGS.CHARACTER_NAMES.billp = "PLATAPINE"
STRINGS.CHARACTER_DESCRIPTIONS.billp = "*Is a vegetarian\n*Can swim\n*Can roll"
STRINGS.CHARACTER_QUOTES.billp = "Likes to collect rings for some reason"

STRINGS.CHARACTER_TITLES.glowflyp = "Glowfly"
STRINGS.CHARACTER_NAMES.glowflyp = "GLOWFLY"
STRINGS.CHARACTER_DESCRIPTIONS.glowflyp = "*Is pretty harmless\n*Provides light\n*Can transform during Spring"
STRINGS.CHARACTER_QUOTES.glowflyp = "Something that will light up your day"

STRINGS.CHARACTER_TITLES.rabidbeetlep = "Rabid Beetle"
STRINGS.CHARACTER_NAMES.rabidbeetlep = "RABID BEETLE"
STRINGS.CHARACTER_DESCRIPTIONS.rabidbeetlep = "*Is a monster\n*Is pretty fast\n*Very weak alone"
STRINGS.CHARACTER_QUOTES.rabidbeetlep = "*it's just a bug* -A dead Wilson"

STRINGS.CHARACTER_TITLES.heraldp = "Ancient Herald"
STRINGS.CHARACTER_NAMES.heraldp = "Ancient Herald"
STRINGS.CHARACTER_DESCRIPTIONS.heraldp = "*Is a giant\n*Is a shadow\n*Can summon minions with C"
STRINGS.CHARACTER_QUOTES.heraldp = "He's warning you that hes going to kill you"

STRINGS.CHARACTER_TITLES.ancient_robot_headp = "Iron Hulk(Head)"
STRINGS.CHARACTER_NAMES.ancient_robot_headp = "Iron Hulk(Head)"
STRINGS.CHARACTER_DESCRIPTIONS.ancient_robot_headp = "*Is bulky\n*Has a laser slam\n*Is slow"
STRINGS.CHARACTER_QUOTES.ancient_robot_headp = "Gotta keep your head in the game"

STRINGS.CHARACTER_TITLES.ancient_robot_legp = "Iron Hulk(Leg)"
STRINGS.CHARACTER_NAMES.ancient_robot_legp = "Iron Hulk(Leg)"
STRINGS.CHARACTER_DESCRIPTIONS.ancient_robot_legp = "*Is bulky\n*Has a laser slam\n*Is slow"
STRINGS.CHARACTER_QUOTES.ancient_robot_legp = "This is not what we meant when we said 'break a leg'"

STRINGS.CHARACTER_TITLES.ancient_robot_clawp = "Iron Hulk(Arm)"
STRINGS.CHARACTER_NAMES.ancient_robot_clawp = "Iron Hulk(Arm)"
STRINGS.CHARACTER_DESCRIPTIONS.ancient_robot_clawp = "*Is bulky\n*Has a laser attack\n*Is slow"
STRINGS.CHARACTER_QUOTES.ancient_robot_clawp = "Hes gonna lend you a hand... whether you like it or not"

STRINGS.CHARACTER_TITLES.ancient_robot_spiderp = "Iron Hulk(Ribs)"
STRINGS.CHARACTER_NAMES.ancient_robot_spiderp = "Iron Hulk(Ribs)"
STRINGS.CHARACTER_DESCRIPTIONS.ancient_robot_spiderp = "*Is bulky\n*Has a laser slam\n*Is slow"
STRINGS.CHARACTER_QUOTES.ancient_robot_spiderp = "The only ribs WX-78 likes"

STRINGS.CHARACTER_TITLES.hippop = "Hippoptamoose"
STRINGS.CHARACTER_NAMES.hippop = "Hippoptamoose"
STRINGS.CHARACTER_DESCRIPTIONS.hippop = "*Is bulky\n*Can stomp\n*Can swim"
STRINGS.CHARACTER_QUOTES.hippop = "Enjoys sitting in large ponds"

STRINGS.CHARACTER_TITLES.weevolep = "Weevole"
STRINGS.CHARACTER_NAMES.weevolep = "WEEVOLE"
STRINGS.CHARACTER_DESCRIPTIONS.weevolep = "*Loves to eat wood\n*Can hide underground\n*3x weak to fire, poison, and acid"
STRINGS.CHARACTER_QUOTES.weevolep = "We got an infestation on our hands"

STRINGS.CHARACTER_TITLES.giantgrubp = "Giant Grub"
STRINGS.CHARACTER_NAMES.giantgrubp = "GIANT GRUB"
STRINGS.CHARACTER_DESCRIPTIONS.giantgrubp = "*Consume enemies on kill\n*Prefers live food\n*3x weak to fire, poison, and acid"
STRINGS.CHARACTER_QUOTES.giantgrubp = "Probably eats mants for breakfast"

STRINGS.CHARACTER_TITLES.snake2p = "Viper"
STRINGS.CHARACTER_NAMES.snake2p = "VIPER"
STRINGS.CHARACTER_DESCRIPTIONS.snake2p = "*Is a snake\n*Is amphibious\n*Is pretty slow"
STRINGS.CHARACTER_QUOTES.snake2p = "They learned how to swim, no one is safe now"

STRINGS.CHARACTER_TITLES.spidermonkeyp = "Spider Monkey"
STRINGS.CHARACTER_NAMES.spidermonkeyp = "SPIDER MONKEY"
STRINGS.CHARACTER_DESCRIPTIONS.spidermonkeyp = "*Is a monster\n*Is bulky\n*Has spider traits"
STRINGS.CHARACTER_QUOTES.spidermonkeyp = "King of the Jungle Spiders"

STRINGS.CHARACTER_TITLES.spidermonkey2p = "Baby Spider Monkey"
STRINGS.CHARACTER_NAMES.spidermonkey2p = "BABY SPIDER MONKEY"
STRINGS.CHARACTER_DESCRIPTIONS.spidermonkey2p = "*Is a monster\n*Is a monkey\n*Has spider traits"
STRINGS.CHARACTER_QUOTES.spidermonkey2p = "Not-So-King of the Jungle Spiders"

STRINGS.CHARACTER_TITLES.mandrakemanp = "Elder Mandrake"
STRINGS.CHARACTER_NAMES.mandrakemanp = "ELDER MANDRAKE"
STRINGS.CHARACTER_DESCRIPTIONS.mandrakemanp = "*Is vegetarian\n*Has natural resistances \n*Can craft"
STRINGS.CHARACTER_QUOTES.mandrakemanp = "mandrake of your nightmares"

STRINGS.CHARACTER_TITLES.thunderbirdp = "Thunderbird"
STRINGS.CHARACTER_NAMES.thunderbirdp = "THUNDERBIRD"
STRINGS.CHARACTER_DESCRIPTIONS.thunderbirdp = "*Is vegetarian\n*attacks with lightning \n*very creepy..."
STRINGS.CHARACTER_QUOTES.thunderbirdp = "Gaze into the abyss and it will gaze back"

STRINGS.CHARACTER_TITLES.flytrapp = "Snaptooth Seedling"
STRINGS.CHARACTER_NAMES.flytrapp = "SNAPTOOTH SEEDLING"
STRINGS.CHARACTER_DESCRIPTIONS.flytrapp = "*Can only eat meat\n*Grows through eating \n*Doesn't like the cold"
STRINGS.CHARACTER_QUOTES.flytrapp = "A mean green mother from hamlet"

STRINGS.CHARACTER_TITLES.citypig_malep = "Pig"
STRINGS.CHARACTER_NAMES.citypig_malep = "PIG"
STRINGS.CHARACTER_DESCRIPTIONS.citypig_malep = "*Can craft\n*Is dapper\n*Is civilized"
STRINGS.CHARACTER_QUOTES.citypig_malep = "Oink"

STRINGS.CHARACTER_TITLES.citypig_femalep = "Pig"
STRINGS.CHARACTER_NAMES.citypig_femalep = "PIG"
STRINGS.CHARACTER_DESCRIPTIONS.citypig_femalep = "*Can craft\n*Is dapper\n*Is civilized"
STRINGS.CHARACTER_QUOTES.citypig_femalep = "Oink"

STRINGS.CHARACTER_TITLES.citypig_guardp = "Pig Guard"
STRINGS.CHARACTER_NAMES.citypig_guardp = "PIG GUARD"
STRINGS.CHARACTER_DESCRIPTIONS.citypig_guardp = "*Can craft\n*Has natural armor\n*Is civilized"
STRINGS.CHARACTER_QUOTES.citypig_guardp = "For the QUEEN"

STRINGS.CHARACTER_TITLES.citypig_queenp = "Pig Queen"
STRINGS.CHARACTER_NAMES.citypig_queenp = "PIG QUEEN"
STRINGS.CHARACTER_DESCRIPTIONS.citypig_queenp = "*Can craft\n*Can summon guards\n*Is civilized"
STRINGS.CHARACTER_QUOTES.citypig_queenp = "Long live the Queen"

STRINGS.CHARACTER_TITLES.citypig_thiefp = "Pig Bandit"
STRINGS.CHARACTER_NAMES.citypig_thiefp = "PIG BANDIT"
STRINGS.CHARACTER_DESCRIPTIONS.citypig_thiefp = "*Can craft\n*Can turn invisible\n*Can teleport"
STRINGS.CHARACTER_QUOTES.citypig_thiefp = "Every city needs crime"

STRINGS.CHARACTER_TITLES.vampirebatp = "Vampire Bat"
STRINGS.CHARACTER_NAMES.vampirebatp = "VAMPIRE BAT"
STRINGS.CHARACTER_DESCRIPTIONS.vampirebatp = "*Is a bat\n*Steals health on hit\n*Can fly"
STRINGS.CHARACTER_QUOTES.vampirebatp = "This is why we don't live in the jungle"

STRINGS.CHARACTER_TITLES.antmanp = "Mant Drone"
STRINGS.CHARACTER_NAMES.antmanp = "MANT DRONE"
STRINGS.CHARACTER_DESCRIPTIONS.antmanp = "*Is a mant\n*Can craft\n*Is 3x weak to poison, fire, and acid"
STRINGS.CHARACTER_QUOTES.antmanp = "Only exists to serve the queen"

STRINGS.CHARACTER_TITLES.antguardp = "Mant Warrior"
STRINGS.CHARACTER_NAMES.antguardp = "MANT WARRIOR"
STRINGS.CHARACTER_DESCRIPTIONS.antguardp = "*Is a mant\n*Can craft\n*Is 3x weak to poison, fire, and acid"
STRINGS.CHARACTER_QUOTES.antguardp = "Only exists to protect the queen"

STRINGS.CHARACTER_TITLES.antqueenp = "Queen Womant"
STRINGS.CHARACTER_NAMES.antqueenp = "WOMANT"
STRINGS.CHARACTER_DESCRIPTIONS.antqueenp = "*Is a giant\n*Can summon minions\n*Can stun enemies with c"
STRINGS.CHARACTER_QUOTES.antqueenp = "100% of all mants are in her fan club"

STRINGS.CHARACTER_TITLES.pugaliskp = "Pugalisk"
STRINGS.CHARACTER_NAMES.pugaliskp = "PUGALISK"
STRINGS.CHARACTER_DESCRIPTIONS.pugaliskp = "*Is a giant\n*Has natural armor\n*Is pretty slow"
STRINGS.CHARACTER_QUOTES.pugaliskp = "You won't make fun of snakes anymore..."

STRINGS.CHARACTER_TITLES.rocp = "BFB"
STRINGS.CHARACTER_NAMES.rocp = "BFB"
STRINGS.CHARACTER_DESCRIPTIONS.rocp = "*Is huge\n*Can fly\n*Is pretty slow"
STRINGS.CHARACTER_QUOTES.rocp = "God of birds"
------------------------------------------------------------------
-- Character Speech Strings
------------------------------------------------------------------
STRINGS.CHARACTERS.CITYPIG_GUARDP = require("speech_citypigp")
STRINGS.CHARACTERS.CITYPIG_MALEP = require("speech_citypigp")
STRINGS.CHARACTERS.CITYPIG_FEMALEP = require("speech_citypigp")
STRINGS.CHARACTERS.CITYPIG_QUEENP = require("speech_citypigp")
STRINGS.CHARACTERS.CITYPIG_THIEFP = require("speech_citypigp")
------------------------------------------------------------------
-- The prefab names as they appear in-game
------------------------------------------------------------------
STRINGS.NAMES.CHICKEN_ALTAR = "???"

STRINGS.NAMES.PUGALISK_FOUNTAINP = "Fountain"

STRINGS.NAMES.PIGCROWNP = "Royal Crown"

STRINGS.NAMES.WALL_HEDGE1P = "Hedge"
STRINGS.NAMES.WALL_HEDGE2P = STRINGS.NAMES.WALL_HEDGE1P
STRINGS.NAMES.WALL_HEDGE3P = STRINGS.NAMES.WALL_HEDGE1P
STRINGS.NAMES.WALL_HEDGE1_ITEMP = STRINGS.NAMES.WALL_HEDGE1P
STRINGS.NAMES.WALL_HEDGE2_ITEMP = STRINGS.NAMES.WALL_HEDGE1P
STRINGS.NAMES.WALL_HEDGE3_ITEMP = STRINGS.NAMES.WALL_HEDGE1P

STRINGS.NAMES.LAWNDECOR1P = "Layer Tree"
STRINGS.NAMES.LAWNDECOR2P = "Irregular Tree"
STRINGS.NAMES.LAWNDECOR3P = "Umbrella Tree"
STRINGS.NAMES.LAWNDECOR4P = "Floopy Tree"
STRINGS.NAMES.LAWNDECOR5P = "Funbrella Tree"
STRINGS.NAMES.LAWNDECOR6P = "Flowering Tree"
STRINGS.NAMES.LAWNDECOR7P = "Spiral Tree"

STRINGS.NAMES.LAWNDECOR_PIGMANP = "Intricate Topiaries"
STRINGS.NAMES.LAWNDECOR_PIGKINGP = STRINGS.NAMES.LAWNDECOR_PIGMANP
STRINGS.NAMES.LAWNDECOR_BEEFALOP = STRINGS.NAMES.LAWNDECOR_PIGMANP
STRINGS.NAMES.LAWNDECOR_WEREPIGP = STRINGS.NAMES.LAWNDECOR_PIGMANP

STRINGS.NAMES.CITY_LAMPP = "Street Light"
STRINGS.NAMES.MONSTER_WPNHAM = "Mysterious Power"
STRINGS.NAMES.POOPWEAPON_FORGE = "Poop"
STRINGS.NAMES.POISONAREAP = "Poison Mist"
STRINGS.NAMES.ANTMAN_WARRIOR_EGGP = "Mant Warrior Egg"
STRINGS.NAMES.ANTMAN_WARRIORP = "Mant Warrior"
STRINGS.NAMES.CITY_PLAYER_GUARDTOWERP = "Watch Tower"
STRINGS.NAMES.CITYHALLP = "City Hall"
STRINGS.NAMES.CITY_PLAYER_HOUSEP = "Pig House"
STRINGS.NAMES.MANDRAKEHOUSEP = "Jungle Hut"
STRINGS.NAMES.ANTHILLP = "Mant Hill"
STRINGS.NAMES.DUNGBALLP = "Ball of Dung"
STRINGS.NAMES.PIGBANDIT_HIDEOUTP = "Secret Bandit Camp"
STRINGS.NAMES.HALBERDP = "Halberd"
STRINGS.NAMES.GUARDKEYP = "Guard Key"
STRINGS.NAMES.OINCP = "Oinc"
STRINGS.NAMES.OINC10P = "Tenpiece Oinc"
STRINGS.NAMES.OINC100P = "Centapiece Oinc"
STRINGS.NAMES.HERALD_NIGHTMAREBEAK = "Terrorbeak"
STRINGS.NAMES.HERALD_CRAWLINGNIGHTMARE = "Crawlinghorror"
STRINGS.NAMES.ROC_LEGP = "BFB's Foot"
STRINGS.NAMES.ROCK_BASALTP = "Basalt"
STRINGS.NAMES.ANCIENT_HULK_MINEP = "Ancient Mine"

STRINGS.NAMES.PIG_GUARD_TOWERP = "Watch Tower"
STRINGS.NAMES.PIG_GUARD_TOWER_PALACEP = "Watch Tower"
STRINGS.NAMES.PIGHOUSE_CITYP = "Pig House"
STRINGS.NAMES.PIGHOUSE_FARMP = "Pig House"
STRINGS.NAMES.PIGHOUSE_MINEP = "Pig House"
STRINGS.NAMES.PIGHOUSE_CITYHALLP = "City Hall"
STRINGS.NAMES.PIGHOUSE_PALACEP = "Pig Palace"
STRINGS.NAMES.PIG_PALACEP = "Pig Palace"

STRINGS.NAMES.PIG_SHOP_GENERALP = "Pigg and Pigglet's General Store"
STRINGS.NAMES.PIG_SHOP_DELIP = "The Sterling Trough Deli"
STRINGS.NAMES.PIG_SHOP_FLORISTP = "Miss Sow's Floral Arrangements"
STRINGS.NAMES.PIG_SHOP_HOOFSPAP = "Curly Tails Mud Spa"
STRINGS.NAMES.PIG_SHOP_PRODUCEP = "Swinesbury Fine Grocer's"
STRINGS.NAMES.PIG_SHOP_ANTIQUITIESP = "'The Sty' Oddities Emporium"
STRINGS.NAMES.PIG_SHOP_ARCANEP = "The Flying Pig Arcane Shop"
STRINGS.NAMES.PIG_SHOP_WEAPONSP = "The Boar's Tusk Weapon Shop"
STRINGS.NAMES.PIG_SHOP_HATSHOPP = "The Sow's Ear Hat Shop"
STRINGS.NAMES.PIG_SHOP_BANKP = "Swinesbury Mineral Exchange"

STRINGS.NAMES.PIGMAN_ROYALGUARDP = "Pig Guard"
STRINGS.NAMES.PIGMAN_ROYALGUARD_2P = "Royal Pig Guard"

local items = require("ppham_shoplist")
for i, v in ipairs(items) do
	STRINGS.NAMES[string.upper(v).."_SHOPP"] = STRINGS.NAMES[string.upper(v)]
end

--Experimental--------------------
STRINGS.NAMES.SPIRIT_BELLP = "Spirit Calling Bell"

STRINGS.NAMES.PIG_FOOTSOLDIER_BLUE = "Loyalist Trot Soldier"
STRINGS.NAMES.PIG_FOOTSOLDIER_RED = "Rebel Trot Soldier"

------------------------------------------------------------------
--Reforged Strings
------------------------------------------------------------------
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.snapdragonp = "*Is bulky\n*Gets bigger when taking damage\n*Heals twice as fast\n\nExpertise:\n*Staves"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.chickenp = "*Is a chicken\n*Can be revived 2x faster\n*Too dumb to matter to foes\n\nExpertise:\n*None"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.robinp = "*Is bulky\n*Can equip anything\n*Deals only half damage\n\nExpertise:\n*Melees, Darts, Books, Staves"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.pidgeon2p = "*Is very weak\n*Can fly temporarily\n*Can be revived easily\n\nExpertise:\n*Melees, Darts"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.kingfisherp = "*Is very weak\n*Can fly temporarily\n*Can be revived easily\n\nExpertise:\n*Melees, Darts"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.parrot2p = "*Is very weak\n*Can fly temporarily\n*Can be revived easily\n\nExpertise:\n*Melees, Darts"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.ancient_hulkp = "*Is a giant\n*Has many attacks\n*Has natural armor\n\nExpertise:\nNone"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.pigghostp = "*Is considered a pig\n*Auto-revives\n*Revives other instantly, but loses health\n\nExpertise:\nMelee, Darts, Books, Staves"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.peagawkp = "*Is fast\n*Transforms when in danger\n*Prism form is immune to debuffs\n\nExpertise:\nMelee, Darts"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.pogp = "*70% chance to lose aggro on hit\n*Transforms when in danger\n*Can be revived 25% faster\n\nExpertise:\nMelee"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.pikop = "*Is very weak\n*Transforms when in danger\n*Can be revived 3x faster\n\nExpertise:\nMelee"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.dungbeetlep = "*Brings his favorite ball\n*The ball absorbs all damage\n*Rolling can flip enemies\n\nExpertise:\nNone"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.gnatp = "*Deals damage equal to health\n*Has a 10% dodge chance\n*Extremely weak to acid\n\nExpertise:\nNone"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.scorpionp = "*Is poisonous\n*Can dodge\n*Can taunt by pressing x \n\nExpertise:\nNone"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.grabbingvinep = "*Is pretty fast\n*Can hide with C\n*Can reivive others twice as fast\n\nExpertise:\nMelee"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.pangoldenp = "*Is bulky\n*Is pretty fast\n*Can hide with c\n\nExpertise:\nMelee, Darts"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.frog2p = "*Is pretty frail\n*Inflicts poison on contact\n*Can be revived twice as fast\n\nExpertise:\nNone"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.billp = "*Inflicts 50% damage taken back on melee foes\n*Can taunt with x\n*Can roll by pressing c\n\nExpertise:\nMelees"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.glowflyp = "*Is very weak and can't attack\n*Can heal people within its light\n* Can be revived 3x as fast\n\nExpertise:\nNone"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.rabidbeetlep = "*Is very weak\n*3x weak to fire,acid, and poison\n* Can be revived 3x as fast\n\nExpertise:\nMelee"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.heraldp = "*Is a giant and a shadow\n*Increased firerate with staves\n*Can summon minions with C \n\nExpertise:\nBooks, Staves"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.rocp = "*Is a huge giant\n*Can't flinch\n*Takes 4x longer to revive \n\nExpertise:\nNone"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.ancient_robot_spiderp = "*Is heavy and can auto-revive\n*Is the bulkiest of the parts\n*Has natural armor\n\nExpertise:\nNone"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.ancient_robot_clawp = "*Is heavy and can auto-revive\n*Can sturdily revive others\n*Has natural armor\n\nExpertise:\nNone"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.ancient_robot_headp = "*Is heavy and can auto-revive\n*Can shock enemies\n*Has natural armor\n\nExpertise:\nNone"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.ancient_robot_legp = "*Is heavy and can auto-revive\n*Can heal by pressing x\n*Has natural armor\n\nExpertise:\nNone"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.hippop = "*Is bulky\n*Stomps can break guards\n*Can taunt by pressing x\n\nExpertise:\nNone"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.spidermonkeyp = "*Is bulky\n*Can taunt by pressing x \n*Has a strong punch\n\nExpertise:\nNone"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.spidermonkey2p = "*Is a monkey\n*Can taunt by pressing x\n*Can throw poop \n\nExpertise:\nMelee, Darts"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.mandrakemanp = "*Is bulky\n*25% acid resistance\n*Heals overtime \n\nExpertise:\nBooks, Staves"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.thunderbirdp = "*Deals electric damage\n*Can shock enemies\n\nExpertise:\nNone"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.flytrapp = "*Grows when getting kills\n*Is weak to acid\nIncreased attack speed\n\nExpertise:\nMelee"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.pugaliskp = "*Is a giant\n*Has natural armor\n*Gets less heals\n\nExpertise:\nNone"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.citypig_malep = "*Yummy decoy\n*Isn't effective in fighting\*Is a master in all arts\n\nExpertise:\nMelees, Books, Staves, Darts"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.citypig_femalep = "*Yummy decoy\n*Isn't effective in fighting\*Is a master in all arts\n\nExpertise:\nMelees, Books, Staves, Darts"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.citypig_guardp = "*Has natural armor\n*Brings his own equipment\*Guards his queen\n\nExpertise:\nMelees"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.citypig_queenp = "*Is bulky\n*Perks coming soon!\n\nExpertise:\nBooks, Staves"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.citypig_thiefp = "*Is a pig\n*+10% speed\n*Can steal items with c\n\nExpertise:\nMelee, Darts"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.vampirebatp = "*Is fast\n*Restores HP on hit\n*Can fly with C\n\nExpertise:\nMelee, Darts"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.snake2p = "*Doesn't really do much\n\nExpertise:\nMelee, Darts"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.weevolep = "*Has natural armor\n*3x weak to fire, acid, and poison\n*10% ATK buff per nearby weevole\n\nExpertise:\nMelee, Darts"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.giantgrubp = "*Takes less damage underground\n*Heals 50 HP on kill\n*3x weak to fire, acid, and poison\n\nExpertise:\nMelee, Darts"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.antmanp = "*Revives other ants 2x as fast\n*3x weak to fire, acid, and poison\n*Can use anything\n\nExpertise:\nMelee, Darts, Books, Staves"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.antguardp = "*Always gets 80% HP on revive\n*10% defense buff to other ants\n*Transforms when in danger\n\nExpertise:\nMelee, Darts"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.antqueenp = "*Is a giant\n*Ground pounds with x\n*Stun foes with c\n\nExpertise:\nNone"

------------------------------------------------------------------
-- Mob strings
------------------------------------------------------------------
	STRINGS.ANT_TALK_UNTRANSLATED = {"CHR'IK TIVIK","CR'KIT CR'KIT","KLICK VRR'TIK","K'KRIT","VVR'T C'CHAK","TI'VIK TI'VOK"}
    STRINGS.ANT_TALK_KEEP_AWAY = {"K-K-KEEP BACK-CK-CK", "WE PROTECT-KT-KT QUEEN!", "GET-TK-TK AWAY",}
    STRINGS.ANT_TALK_WANT_MEAT = {"I EAT-TK-TK MEAT"}
    STRINGS.ANT_TALK_WANT_VEGGIE = {"I EAT-TK-TK PLANTS"}
    STRINGS.ANT_TALK_WANT_SEEDS = {"I EAT-TK-TK SEEDS"}
    STRINGS.ANT_TALK_WANT_WOOD = {"I EAT-TK-TK TREES"}


    STRINGS.ANT_TALK_FOLLOWWILSON = {"OBEY ROYAL-TK-TK-TY", "AWAIT INSTRUCT-TK-TK-TIONS", "ASSIST-T-T THE HIVE", "I CK-CK-COMPLY"}
    STRINGS.ANT_TALK_FIGHT = {"CHK-CHK-CHARGE!", "ATT-TK-TACK!", "HIVE MUST-TK-TK CONTINUE!", "PROTECT-KT-KT THE HIVE!"}
    STRINGS.ANT_TALK_HELP_CHOP_WOOD = {"TK-TK-TASKS!", "CHK-CHK-CHOP WOOD", "WORK-K-KT TOGETHER!", "ACT-CT-CTIVITY!", "CK-CK-CONSTRUCT!"}
    STRINGS.ANT_TALK_ATTEMPT_TRADE = {"YOU WORK-K-K TOO?", "ASSIST-TK-TK COLONY?", "CONSTRUCT TK-TK-TOGETHER?",}
    STRINGS.ANT_TALK_PANIC = {"AAAAACH-CH-CH!!", "RETREAT-TK-TK!", "GET BACK-CK-CK!" }
    STRINGS.ANT_TALK_PANICFIRE = {"SCORCH-CHK-CHK!", "I COMBUST-TK-TK!", "I SAC-C-CRIFICE!"}
    STRINGS.ANT_TALK_FIND_MEAT = {"STOCK-CK-CK FOOD!", "COLLECT-TK-TK FOOD!", "C-C-CONSUME!", "STASH MEAT-TK-TK!",}
	
	STRINGS.CITY_PIG_TALK_FOLLOWWILSON = {
                                    DEFAULT = {"WHEREFORE ART THINE BAD GUYS?", "ONCE MORE UNTO THE BREACH, UNPIG", "TAKE ARMS!", "KILL, KILL, KILL!", "CRY HAVOC!", "THE GAME IS AFOOTS!",},
                                    pigman_beauticianp = {"VISIT MINE SHOP FOR MEDICINES", "SELLEST ME THINE FEATHERS", "I HATH NEED OF FEATHERS"},
                                    pigman_mechanicp = {"SELLEST ME THINE REFINED'D THINGS", "IN NEED OF FIXINGS?", "SELLEST ME THINE ROPE", "SELLEST ME THINE BOARDS"},
                                    pigman_mayorp = {"YOU VOTETH?", "GIVE ME THINE GOLD FOR GOOD CAUSE", "ENDORSETH ME?",},
                                    pigman_collectorp = {"WANT'ST STRANGE THINGS?", "SELLEST ME THINE WEIRDITIES",},
                                    pigman_bankerp = {"ALL THAT GLITTERS WORTH OINCS", "HAST THOU JEWELS?", "I BUY THOU JEWELS"},
                                    pigman_floristp = {"VISIT MINE SHOP FOR SEEDS", "HAST THOU PLOP TO SELL?", "HAST THOU PETALS?"},
                                    pigman_farmerp = {"I HAST FARM", "SELLETH ME THINE GRASS", "STEALEST NAUGHT MINE STUFFS"},
                                    pigman_minerp = {"I WILLST BUY THINE ROCKS", "HAST THOU ROCKS TO SELL?", "STEAL NOT FROM MINE MINE"},
                                    pigman_shopkeepp = {"HAST THOU CLIPPINGS?", "GIVETH ME YON CLIPPINGS FROM YON HEDGE", "TRIMETH YON SHRUBS"},
                                    pigman_storeownerp = {"HAS'T THOU CUTTINGS O' HEDGE?", "YON HEDGES NEED'TH TRIMMING",},
                                    pigman_eruditep = {"NEED'ST THOU MAGICS? COME'ST TO MINE SHOP", "NEED'ST FUEL O' NIGHTMARES", "HAST THOU FUEL O' NIGHTMARES?"},
                                    pigman_hatmakerp = {"NEED'ST HATS? VISIT YON HAT SHOP", "HAST THOU SILK?", "THY HEAD IS'T IN NEED O' COVERINGS"},
                                    pigman_professorp = {"VISIT YON ACADEMY", "HA'ST THOU RELICS?", "HATH THOU STUFFS O' OLDEN'D TIMES?"},
                                    pigman_hunterp = {"NEED'ST THOU WEAPONS? VISIT MINE SHOP", "HAS'T THOU TOOTH O' THE HOUND?", "GET THEE TO YON WEAPONS SHOP"},
                                }
    STRINGS.CITY_PIG_TALK_FIND_LIGHT = {
                                    DEFAULT = {"ONCE MORE UNTO THE LIGHT!", "WHEREFORE ART LIGHT?", "WHEREFORE IS'T SUN?", "FREINDS, PIGGIES, UNPIGGIES, LEND ME YOUR LIGHT"},
                                    pigman_beauticianp = {"I AM TOO MUCH I' THE DARK", "'TIS DARK!", "WHEREFORE ART THE LIGHT?"},
                                    pigman_mechanicp = {"IS'T DARK! IS'T SCARY!", "THAT WAY MADNESS LIES!",},
                                    pigman_mayorp = {"FRIENDS, PIGGIES, UNPIGMEN, LEND ME YOUR LIGHT!", "WHEREFORE ART MINE LIGHT?"},
                                    pigman_royalguardp = {"TAKE NAUGHT THE LIGHT!", "TAKE ARMS!", "ONCE MORE UNTO THE LIGHT!"},
                                    pigman_royalguardp = {"TAKE NAUGHT THE LIGHT!", "TAKE ARMS!", "ONCE MORE UNTO THE LIGHT!"},
                                    pigman_collectorp = {"WHEREFORE ART THE LIGHT?!", "I NEED'ST LIGHT!",},
                                    pigman_bankerp = {"BRINGETH BACK THE LIGHT!", "THIS WAY DARKNESS LIES!"},
                                    pigman_floristp = {"I AM TOO MUCH I' THE DARK!", "THIS WAY DARKNESS LIES!",},
                                    pigman_storeownerp = {"ONCE MORE UNTO THE LIGHT!", "WHEREFORE ART THOU, LIGHT?"},
                                    pigman_farmerp = {"WHEREFORE IS'T SUN?", "NEED'ST LIGHT!",},
                                    pigman_minerp = {"FINDETH LIGHT!", "IS'T TOO MUCH SCARY"},
                                    pigman_shopkeepp = {"PROTECTETH ME!", "TAKETH NAUGHT THE LIGHT!",},
                                    pigman_eruditep = {"'TIS TOO MUCH DARKNESS!", "DARKNESS IS AFOOTS!"},
                                    pigman_hatmakerp = {"THIS WAY DARKNESS LIES!", "I AM TOO MUCH I' THE DARK!"},
                                    pigman_professorp = {"THIS WAY DARKNESS LIES!", "DARKNESS 'TIS NAUGHT GOOD!"},
                                    pigman_hunterp = {"BRING'ST BACK THE LIGHT!", "IS'T DARK! IS'T SCARY!"},

                                }
    STRINGS.CITY_PIG_TALK_LOOKATWILSON_TRADER = {
                                    pigman_beauticianp = {"HAS'T THOU FEATHERS?", "VISIT YON SHOP IF THOU HAS'T BOO-BOOS", "SELLEST ME FEATHERS"},
                                    pigman_mechanicp = {"WHATFORE IS YOU?", "HAS'T THOU BOARDS?", "HAS'T THOU ROPE?", "I BUY'ST THINGS REFINED'D"},
                                    pigman_mayorp = {"GOOD FORTUNE FOR THOSE WHO HAVE RAREST GEM", "HAVE RAREST GEM?", "VOTE FOR ME ONLY"},
                                    pigman_collectorp = {"HAS'T THOU ODD STUFFS?", "I CAN'ST BUY YOUR ODD STUFFS?", "HAS'T THOU SILK, STINGERS, AND WOOL?"},
                                    pigman_bankerp = {"HAS'T THOU JEWELS?", "I BUY'ST JEWELS FROM THOU", "WILL-TH THOU SELLEST ME THINE JEWELS?"},
                                    pigman_floristp = {"NEED'ST I STUFFS O' PLANTS", "HAS'T THOU PLOP?", "HAS'T THOU PETALS?"},
                                    pigman_farmerp = {"HAS'T THOU GRASS?", "DON'T TAKETH MINE FARM STUFFS", "CAN'ST THOU SELL ME THINE GRASS?"},
                                    pigman_minerp = {"HAS'T THOU ROCKS?", "TAKETH NAUGHT MINE ROCKS", "I WILL GIVETH THEE OINCS FOR ROCKS"},
                                    pigman_shopkeepp = {"HAS'T THOU FARM FOOD?", "GET THEE TO A FARM", "SELLEST ME THINE FARM FOOD"},
                                    pigman_storeownerp = {"HAS'T THOU FARM FOOD?", "GET THEE TO A FARM", "SELLEST ME THINE FARM FOOD"},
                                    pigman_eruditep = {"HAS'T THOU RELICS O' PIGGIES?", "SELLEST ME THINE PIGGY RELICS", "RELIC HATH MUCH WORTH"},
                                    pigman_hatmakerp = {"HAS'T THOU HATS?", "ME HAVE HATS FOR UNPIG HEAD", "ME BUY FEATHERS FROM YOU"},
                                    pigman_professorp = {"YOU HAVE THULECITE?", "I RESEARCH HERE FOR RUINS", "I NEED OLD STUFFS"},
                                    pigman_hunterp = {"ME TAKE GIANT LOOT FOR OINCS", "WEAPON SHOP GOOD SHOP", "WEAPON MAKE YOU NOT SCARED"},

                                    DEFAULT = {"HOW NOW, UNPIG?","HAVE THEE %s?","WELL MET","GOOD FORTUNE, THOSE WHO HAVE %s"},
                                }

    STRINGS.CITY_PIG_TALK_LOOKATROYALTY_TRADER = { 
                                    pigman_beautician = {"THOU HAST MOST BEAUTIFUL SNOUT", "YON MAJESTY", "TAKETH THEE A GIFT?"},
                                    pigman_mechanic = {"HOW NOW, YOUR MAJESTY", "AT-ETH YON SERVICE", "THOU MIGHTY SCEPTER'D BEING"},
                                    pigman_mayor = {"YON MAJESTY!", "ME HONORED'D!", "AT YON SERVICE"},
                                    pigman_royalguard = {"MINE SERVICE UNTO YOU", "YON MAJESTY", "LONG LIVE YON MAJESTY!"},
                                    pigman_royalguard_2 = {"THY HONOR US LOWLY PIGS", "YON MAJESTY", "THOU MIGHTY SCEPTER'D BEING"},
                                    pigman_collector = {"AT THINE SERVICE", "THOU DO-EST US AN HONOR", "TAKEST THOU A GIFT?"},
                                    pigman_banker = {"YON MAJESTY!", "THOU DO-EST ME AN HONOR", "I AM AT-ETH THY SERVICE",},
                                    pigman_florist = {"TAKEST THINE MINE FLOWERS?", "AT YON SERVICE", "ME HONORED'D AT THINE PRESENCE!"},
                                    pigman_farmer = {"THINE HUMBL'EST SERVENT", "YOUR MAJESTY", "AT THINE SERVICE"},
                                    pigman_miner = {"YOU HONOR ME", "TAKE'ST THOU A GIFT", "ACCEPT'ETH MINE GIFT!"},
                                    pigman_shopkeep = {"HOW NOW, YON MAJESTY?", "LIKETH THY MINE GIFT?", "TAKE'ST THEE A GIFT?",},
                                    pigman_storeowner = {"AT THINE SERVICE", "LIKEST THY MINE GIFT?", "THOU MIGHTY SCEPTER'D BEING"},
                                    pigman_erudite = {"ACCEPT MINE HUMBLEST GIFT", "LONG LIVE YON MAJESTY", "WILL'ST THOU ACCEPT A GIFT?"},
                                    pigman_hatmaker = {"YON MAJESTY", "AT THINE SERVICE", "THOU MIGHTY SCEPTER'D BEING"},
                                    pigman_professor = {"ACCEPT'ST THOU A GIFT?", "THY HONOR ME?", "I AM THOU MOST HUMBLEST PIGGY SERVANT"},
                                    pigman_hunter = {"THY HONOR US LOWLY PIGS", "YON MAJESTY", "AT YON SERVICE, YON MAJESTY"},

                                    DEFAULT = {"GREETINGS YOUR HIGHNESS","NEED'ST THOU %s?","GOOD DAY","NEED'ST %s. HAV'TH THOU THAT?","I WANT'ST %s. WILL BUY", "YOUR MAJESTY"},
                                }


    STRINGS.CITY_PIG_TALK_LOOKATWILSON = {
                                    DEFAULT = {"WHAT HO, UNPIG", "HOW NOW, GOOD UNPIG", "WHAT NEWS?", "WELL MET"},
                                    ROYALTY = {"LONG LIVE THE QUEEN!","HAIL, GOOD NOBLE PIG!", "MINE LIEGE", "HAIL TO YOU, GOOD GENTLEPIGGY",}, 
                                }

    STRINGS.CITY_PIG_TALK_APORKALYPSE_SOON = { 
                                        DEFAULT = { "THE APORKALYPSE APPROACH-ETH", "WOE, DESTRUCTION, RUIN, AND DECAY!", "DOOMSDAY IS NEAR!", "SOMETHING WICKED THIS WAY COMES", "PORKTENTOUS FIGURES!", "FEAR AND PORKTENT!"},
                                    }

    CITY_TALK_ANNOUNCE_APORKALYPSE = {
                                        DEFAULT = { "THE APORKALYPSE COMETH!", "THE CRACK OF DOOM!", "DEATH HAVE ITS DAY!", "THEY HATH RETURNED!", "THEY KILL US FOR THEIR SPORT", "A PLAGUE ON ALL OUR HOUSES!", "BE ALL AND END ALL!"},
                                }

    STRINGS.CITY_PIG_TALK_RUNAWAY_WILSON = {
                                    DEFAULT = {"THOUST ART NOT KIND!", "STAYEST THOU AWAY!", "GET-ETH THEE AWAY!", "ADIEU YOU!"},
                                    pigman_beauticianp = {"GOODNESS!", "EEP! SNORT!", "LEAVETH THOU!!", "THOU HAST THE SMELL OF UNPIG"},
                                    pigman_mechanicp = {"I TRUST THEE NOT!", "STAY'ST THOU BACK", "GET THEE GONE, UNPIG"},
                                    pigman_mayorp = {"GET THEE GONE!", "GO-EST THOU AWAY!", "GUARDS! PROTECT-ETH ME!",},
                                    pigman_shopkeepp = {"TOO CLOSE! THOU TOO CLOSE!", "BACK-ETH THEE OFF!", "GET THEE GONE!"},
                                    pigman_royalguardp = {"STAYEST THOU AWAY!", "GET THEE GONE!", "THOU HAST THE STENCH OF UNPIG!"},
                                    pigman_royalguard_2p = {"GO'ST THOU AWAY", "BACK-ETH THEE OFF!", "TALK'ST THOU TO ME?"},
                                    pigman_storeownerp = {"THINE CLOSENESS DOST OFFENDETH ME", "TOO CLOSE!", "BACK'ST THOU UP!"},
                                    pigman_farmerp = {"WHATFORE THOU WANT'ETH WITH ME?", "GET'ST BACK!", "BACK-ETH OFF!"},
                                    pigman_minerp = {"LEAVE'ST ME BE!", "SHOO!", "STAYEST THOU AWAY!"},
                                    pigman_collectorp = {"TAKETH NAUGHT MINE STUFFS!", "GET THEE GONE", "GO'EST THOU AWAY!"},
                                    pigman_bankerp = {"THOU HAST THE SMELL OF UNPIG", "TAKE-ETH OFF!", "GET THEE GONE!"},
                                    pigman_floristp = {"ADIEU!", "THOU ART TOO CLOSE!", "STAND-EST NAUGHT SO CLOSE TO ME!"},
                                    pigman_eruditep = {"LEAVETH ME BE!", "WHYFORE YOU BUG-ETH ME?", "BACK-EST THOU OFF!"},
                                    pigman_hatmakerp = {"GO'ST THOU AWAY!", "WANT THOU TO STEAL'EST MINE STUFFS?", "GET THEE GONE!", "I TRUSTETH THEE NAUGHT!"},
                                    pigman_professorp = {"LEAVETH ME BE!", "GO-EST THOU AWAY", "WHYFORE YOU SO CLOSE?"},
                                    pigman_hunterp = {"BACK-ETH AWAY!", "CROWD-EST ME NAUGHT!", "GET-ETH THEE AWAY!"},

                                }
    STRINGS.CITY_PIG_TALK_FIGHT = {
                                    DEFAULT = {"AVAST!", "TO THINE DEATH!", "RAAAWR!", "I BITE MY HOOF AT THEE!"},
                                    pigman_beauticianp = {"DIE-EST NOW!", "ME KILL THEE", "TO THY DEATH!"},
                                    pigman_mechanicp = {"I HAMMER THEE!", "HIT, HAMMER! HIT!", "I DESTROY THEE!"},
                                    pigman_mayorp = {"KILL!", "DIE THOU!", "ME MOST VEXED!", "DESTROY!"},
                                    pigman_shopkeepp = {"I GET THEE!", "THOU NOT NICE!", "I GET THEE!"},
                                    pigman_storeownerp = {"GETEST THEE OUT!", "THY NOT BE WELCOME!", "GET OFF MINE PROPERTY!"},
                                    pigman_farmerp = {"GET THEE BACK!!", "I BURY THINE!", "I GET THEE!"},
                                    pigman_minerp = {"I CRUSHETH YOU!", "THY DYING!", "THOU DONE IT NOW!"},
                                    pigman_collectorp = {"THOU ART DONE FOR!", "GET THINE HENCE!", "THY BAD UNPIG!"},
                                    pigman_bankerp = {"THY NOT MINE FRIEND!", "I CHOP-ETH THEE!", "DIE! DIE!"},
                                    pigman_floristp = {"THOU ART THE WORST!", "GO AWAY-EST!", "THOU ART A VILLAIN!"},
                                    pigman_eruditep = {"VILLAIN UNPIG! BAD!", "THINE ART THE BADEST!", "AWAY FROM ME!"},
                                    pigman_hatmakerp = {"YOU BE UNDONE!", "I SAY THEE OUT!!", "YOU MOST VEXING!"},
                                    pigman_professorp = {"BE DONE-EST WITH THEE!", "WILL NOT YOU GO?!!", "OUT DAMNED UNPIG!"},
                                    pigman_hunterp = {"I ATTACK-ETH THEE!", "GET THOU AWAY FROM HERE!", "YOU MOST NOT WELCOME!"},
                                }

    STRINGS.CITY_PIG_TALK_DAILYGIFT = {
                                    DEFAULT = {"WARE FOR ART THOU", "THY HUMBL'ST PIGGY SERVANT", "REMEMBER'ST ME", "GIFT FOR'ST YOU", "WITH MINE HUMBLEST GRATITUDE","TAKEST THEE MINE GIFT, YOUR MAJESTY"}
                                }

    STRINGS.CITY_PIG_TALK_POOPTIP = {
                                    DEFAULT = {"OUT, DAMNED PLOP", "HUMBLE THANKS, KIND UNPIG", "MY THANKS"},
                                    pigman_beautician = {"TAKEST THEE THY COIN", "HERE BE YOUR OUTRAGEOUS FORTUNE", "THANK YE, UNPIG"},
                                    pigman_mechanic = {"OUT, DAMNED PLOP!", "FAIR PRICE, FAIR UNPIG?", "LET-EST ME PAY FOR THYST HELP"},
                                    pigman_mayor = {"IS THAT PLOP I SEE BEFORE THEE?", "HONEST UNPIG", "KINDEST UNPIG"},
                                    pigman_shopkeep = {"UNPIG IS AN HONORABLE UNPIG", "I GIVE EVERY PLOP MINE OINC", "FAIR TERMS"},
                                    pigman_storeowner = {"FOR THY PLOP PICKING", "MOST EXCELLENT PICKING", "HONORABLE UNPIG"},
                                    pigman_farmer = {"FOR THY HONEST MANURING", "TAKEST THOU WHAT THOU'ST OWED", "HONEST OINC FOR UNPIG"},
                                    pigman_miner = {"'TIS FOUL", "IS A JUST PAYMENT?", "'TIS WORTHY DEED"},
                                    pigman_collector = {"HERE BE OINC FOR THOUST MANURE", "ALAS POOR UNPIG", "FORTUNE SMILES ONST THEE"},
                                    pigman_banker = {"AN OINC FOR THY TROUBLE", "A TAX FOR THY PLOP PICKING", "MANY THANKS"},
                                    pigman_florist = {"WE WILLST PAY FOR THY PLOPPING", "IS POO, UNPIG?", "ME PAYEST FOR PLOP PICKING"},
                                    pigman_erudite = {"FAIR PRICE FOR FOUL DEED", "THY PLOP-PICKING IS MOST PROFESSIONALS", "FOR PLOP OF PIGGY MAN"},
                                    pigman_hatmaker = {"I GIVE THE UNPIG ITS DUE", "WHAT PIECE OF WORK IS UNPIG", "YOU'ST PAID FOR PLOP OF PIG"},
                                    pigman_professor = {"THOUST MILK OF UNPIG KINDNESS", "SUCH STUFF AS PLOP IS MADE ON", "FOR THY TROUBLES"},
                                    pigman_hunter = {"PLOP AND CIRCUMSTANCE", "FOR POUND OF PLOP", "HAVE OINCS THRUST UPON THEE"}, 
                                }

    STRINGS.CITY_PIG_TALK_ROYAL_POOPTIP = {
                                    DEFAULT = {"OUT, DAMNED PLOP", "HUMBLE THANKS, YOUR HIGHNESS",},
                                    pigman_beautician = {"ACCEPTEST THEE MINE COIN?", "HERE BE YOUR OUTRAGEOUS FORTUNE", "HUMBLE THANKS, YOUR MAJESTY"},
                                    pigman_mechanic = {"OUT, DAMNED PLOP!", "THOU ART THE FINEST OF PLOP PICKERS!", "THOU DO US HONOR"},
                                    pigman_mayor = {"WE ARE NOT WORTHY OF THINE PLOP", "'TIS A KIND THING YOU DO", "ACCEPTEST THEE MINE COIN?"},
                                    pigman_shopkeep = {"WE ARE NOT WORTHY OF THINE SERVICE", "I GIVE EVERY PLOP MY OINC", "HUMBLE THANKS, YOUR MAJESTY"},
                                    pigman_storeowner = {"FOR THY FINE PLOP PICKING", "MOST EXCELLENT PICKING", "HONORABLE PLOP PICKER!"},
                                    pigman_farmer = {"FOR THY HONEST MANURING", "FOR THY ROYAL PLOP PICKING", "THOU DO US'T HONOR"},
                                    pigman_miner = {"OUT, DAMNED PLOP!", "FOR THY ROYAL PLOP PICKING", "HUMBLEST OF THANKS, YOUR MAJESTY"},
                                    pigman_collector = {"HERE BE OINC FOR THOUST ROYAL MANURE", "FOR THY ROYAL PLOP PICKING", "THOU ART THE FINEST OF PLOP PICKERS"},
                                    pigman_banker = {"AN OINC FOR THY TROUBLE", "'TIS A KIND THING YOU DO", "THOU DO US HONOR"},
                                    pigman_florist = {"THOU ART THE FINEST OF PLOP PICKERS!", "HUMBLE THANKS, YOUR MAJESTY", "ACCEPTEST THEE MINE COIN?"},
                                    pigman_erudite = {"FAIR PRICE FOR FOUL DEED", "THY PLOP-PICKING IS MOST EXCELLENTEST", "THOU DO'ST US HONOR"},
                                    pigman_hatmaker = {"ACCEPTEST THEE MINE COIN?", "OUT, DAMNED PLOP", "THOU ARE THE MOST EXCELLENTEST OF PLOP PICKERS"},
                                    pigman_professor = {"THOUST MILK OF UNPIG KINDNESS", "SUCH STUFF AS PLOP IS MADE ON", "FOR THY TROUBLES"},
                                    pigman_hunter = {"PLOP AND CIRCUMSTANCE", "FOR POUND OF PLOP", "MOST EXCELLENT PICKINGS"}, 
                                }

    STRINGS.CITY_PIG_TALK_PAYTAX = {
                                    DEFAULT = {"TAKETH THINE TAX","NEED'ST THOU MINE TAX", "TAKEST THOU MINE TAX", "MANY THANKS UNTO YOU, UNPIG MAYOR"},
                                }


    STRINGS.CITY_PIG_TALK_PROTECT = {
                                    DEFAULT = {"MOST FOUL! MOST FOUL!", "TAKE ARMS! TAKE ARMS!", "THOU WRETCH", "THOU COWARD!"},
                                }
    STRINGS.CITY_PIG_TALK_EXTINGUISH = {
                                    DEFAULT = {"OUT OUT, BRIEF FIRES!", "FIRE IS'T BAD!", "YON FIRE BE OUT!"},
                                }                               
    STRINGS.CITY_PIG_TALK_STAYOUT = {
                                    DEFAULT = {"MOST UNWELCOME", "AWAY, YOU CUR!", "BE'ST THEE GONE!", "FIE ON THEE, VILLAIN!"},
                                }
    STRINGS.CITY_PIG_TALK_FLEE = {
                                    DEFAULT = {"ROGUE!", "PEASANT SLAVE!", "O HORRIBLE!", "O STRANGE"},
                                    pigman_beauticianp = {"O HORRIBLE!", "O STRANGE!", "MOST HORRIBLE"},
                                    pigman_mechanicp = {"MOST NOTABLE COWARD", "FLEE!", "AVAST!"},
                                    pigman_mayorp = {"THAT WAY MADNESS LIES!", "NO MORE OF THAT!", "CRY MERCY"},
                                    pigman_royalguardp = {"KILL, KILL, KILL, KILL", "FARE THEE WELL!", "ONCE MORE UNTO THE BREACH!"},
                                    pigman_royalguard_2p = {"TO ARMS!", "I DASH YOU TO PIECES!", "THE GAME IS UP!"},
                                    pigman_shopkeepp = {"FOUL!", "LESS THAN FAIR!", "WOE IS ME!"},
                                    pigman_storeownerp = {"PIGS BE UP IN ARMS", "'TIS A WILD PIG CHASE", "WHAT A PIECE OF WORK!"},
                                    pigman_farmerp = {"THOU LILY-LIVERED UNPIG!", "LIE LOW!", "O MISERY!"},
                                    pigman_minerp = {"IF YOU PRICK US, WE BLEED!", "'TIS RUIN'D!", "A POX ON'T!"},
                                    pigman_collectorp = {"I AM TOO MUCH ON THE RUN!", "FLEE, FIE, FO, FUM!", "SOMETHING ROTTEN!"},
                                    pigman_bankerp = {"ADIEU, ADIEU!", "REMEMBER ME!", "FIE ON THEE!"},
                                    pigman_floristp = {"S'WOUNDS!", "ZOUNDS!", "COWARD!"},
                                    pigman_eruditep = {"A PLAGUE ON YOUR HOUSES!", "THOU'RT MAD!", "AWAY FROM ME!"},
                                    pigman_hatmakerp = {"RUINOUS!", "HARK! NO MORE!", "CUR!"},
                                    pigman_professorp = {"MOST FOUL!", "NOT FAIR!", "LEAVE ME!"},
                                    pigman_hunterp = {"TAKETH THEM AWAY!", "FIE ON THEE!", "TOIL AND TROUBLE!"},
                                }
    STRINGS.CITY_PIG_TALK_RUN_FROM_SPIDER = {
                                    DEFAULT = {"SPIDER IS'T BAD!", "LIKETH NAUGHT YON SPIDER!", "GO'ST THOU AWAY!"},
                                    pigman_beauticianp = {"O MONSTROUS!", "O HORRIBLE!", "MOST VEXING!"},
                                    pigman_mayorp = {"GET-ETH THEE AWAY!", "ALAS!! ALACK!", "I HATE-ETH THEE, SPIDERS!"},
                                    pigman_mechanicp = {"O MONSTROUS THING!", "WRECK-ETH NAUGHT OUR STUFFS", "GO'ST THOU AWAY!"},
                                    pigman_royalguardp = {"PROTECT-ETH THE CITY!", "SPIDERS NAUGHT WELCOME!", "GET THEE GONE!"},
                                    pigman_royalguard_2p = {"PROTECT-ETH THE CITY", "SQUASH-ETH THE SPIDERS!", "GET THEE GONE!"},
                                    pigman_shopkeepp = {"PROTECT-ETH MINE STORE!", "GET-ETH THEE GONE!", "GO-EST AWAY!"},
                                    pigman_storeownerp = {"EEK! SNORT! AVAST!", "MOST VEXING!", "O HORRIBLE!"},
                                    pigman_farmerp = {"O MONSTROUS THING!", "SPIDERS NAUGHT WELCOME!", "MOST VEXING!"},
                                    pigman_minerp = {"GO-ETH THOU AWAY!", "GET THEE GONE!", "THOU AREN'ST WELCOME HENCE"},
                                    pigman_collectorp = {"GET THEE GONE!", "THOU ART BAD GUY!!", "MOST VEXING!"},
                                    pigman_bankerp = {"FOUL! FOUL! MOST FOUL!", "MONSTROUS THING!", "GET THEE GONE!!"},
                                    pigman_floristp = {"SPIDER IS'T BAD!", "O MONSTROUS!!", "O HORROR!"},
                                    pigman_eruditep = {"GET THEE GONE!", "AWAY! AWAY!!", "EEK!"},
                                    pigman_hatmakerp = {"CREEP NAUGHT HENCH!", "GO'ST THOU AWAY!", "GUARD!"},
                                    pigman_professorp = {"GET-ETH THEE AWAY!", "ALAS! ALACK!", "GET THEE GONE!"},
                                    pigman_hunterp = {"I HATE-ETH THEE!", "LIKE-ETH THEE NAUGHT!", "EEK! SNORT! AVAST!!"},

                                }
    STRINGS.CITY_PIG_TALK_HELP_CHOP_WOOD = {
                                    DEFAULT = {"TAKETH THAT TREE!", "I SMASH-ETH YON TREE!", "I PUNCH-ETH TREE!"},
                                    pigman_beauticianp = {"I SHALL CHOP-ETH!", "YON TREE NEEDS CHOPPIN'!",},
                                    pigman_mechanicp = {"SHALL I COMPARE TREE TO SUMMER'S DAY?", "WORK-ETH, WORK-ETH, WORK-ETH",},
                                    pigman_mayorp = {"WHAT PIECE OF WORK IS CHOPPING", "FALL, TREE!",},
                                    pigman_royalguardp = {"I TAKETH DOWN YON TREE", "I CHOPTING", "I GOOD FRIEND, I CHOPT TREE"},
                                    pigman_royalguard_2p = {"CHOP'T CHOP'T", "I AXE THEE!", "FAIR TREE SHALT FALL!"},
                                    pigman_shopkeepp = {"'TIS HARD WORK", "I SMASH-ETH", "I SMASH THEE"},
                                    pigman_storeownerp = {"SMASH-ETH! SMASH-ETH", "DOTH HARD WORK!", "HAVE AT THEE TREE!"},
                                    pigman_farmerp = {"THIS TREE DOTH CHOP'T", "I TOIL", "CHOP'T CHOP'T!"},
                                    pigman_minerp = {"YON TREE IS'T CHOP'T", "'TIS EASIER THAN MINING", "YON TREE IS'T DONE FOR!"},
                                    pigman_collectorp = {"'TIS HARD WORK", "WHAT A PIECE OF WORK IS CHOPPING",},
                                    pigman_bankerp = {"MINE HOOVES GET-ETH DIRTY", "FALL, TREE!",},
                                    pigman_floristp = {"CHOP'T, CHOP'T", "SMASHINGS!", "TOIL, TOIL"},
                                    pigman_eruditep = {"I HELP-ETH", "FALL, TREE!", "I AXE THEE!"},
                                    pigman_hatmakerp = {"YON TREE SHALL FALL!", "THUS FALL-ETH THY TREE",},
                                    pigman_professorp = {"WITH MINE LAST BREATH I CHOP AT THEE!", "CHOP'T CHOP'T", "HAVE AT THEE TREE"},
                                    pigman_hunterp = {"THUS FALL THE TREE", "I BID THEE FALL!", "I WIN!"},
                                }
    STRINGS.CITY_PIG_TALK_ATTEMPT_TRADE = {
                                    DEFAULT = {"WHAT HAS'T THEE, UNPIG?", "NEED'ST THEE WARES?"},
                                    pigman_beauticianp = {"THEE HAS'T FEATHERS?", "THEE HAS'T BIRDY FEATHERS?", "HAS'T THEE PRETTY FEATHERS?"},
                                    pigman_mechanicp = {"THINE NEED'ST REPAIRS?", "BRING FORTH YON REPAIRS", "THEE HAS'T REFINED GOODS?"},
                                    pigman_mayorp = {"HAS'T THOU RAREST GEM?", "YES?", "THY NEED'ST SOMETHING?"},
                                    pigman_shopkeepp = {"GET THEE TO SOME SHRUBBERY!", "HAS'T THEE CLIPPINGS?", "HAS'T THEE SHRUB STUFFS?"},
                                    pigman_storeownerp = {"GET THEE TO SOME SHRUBBERY!", "HAS'T THEE CLIPPINGS?", "HAS'T THEE SHRUB STUFFS?"},
                                    pigman_farmerp = {"HAS'T THOU GRASS?", "SELL'ST ME THINE GRASS PARTS", "ME WANT'ST GRASS STUFFS"},
                                    pigman_minerp = {"HAS'T THOU A ROCK?", "ME PAY'ST FOR ROCKS", "I GIVETH OINC FOR THY ROCK"},
                                    pigman_collectorp = {"HAS'T THOU STRANGE THINGS?", "I BUYEST THING O' STRANGENESS", "SELLEST ME THINE WANT WEIRD STUFF?"},
                                    pigman_bankerp = {"HAST THOU JEWELS?", "I WILL'ST BUYEST JEWELS FROM THEE", "I PAYST OINCS FOR JEWELS"},
                                    pigman_floristp = {"HAS'T THOU PLOP?", "PETALS FOR MINE SHOP?", "ME LIKE'ST PRETTY FLOWER", "ME LIKE'ST SMELLY PLOP"},
                                    pigman_eruditep = {"HAS'T THOU DARK MAGICS?", "HAS'T THOU FUEL O' NIGHTMARE?", "SELL'ST ME THY DARK MAGICS STUFFS"},
                                    pigman_hatmakerp = {"HAST THOU SILK?", "I NEED'TH SILK", "SELLEST ME THINE SILK"},
                                    pigman_professorp = {"RELICS?", "HAS'T THOU RELICS?", "PAY'ST THOU OINCS FOR RELICS"},
                                    pigman_hunterp = {"HAS'T THOU GIANT LOOT?", "SELLETH THEE GIANT LOOT?", "I BUY'ST LOOT O' THE GIANTS"},                               

                                }
    STRINGS.CITY_PIG_TALK_PANIC = {
                                    DEFAULT = {"O HORRIBLE", "AAAAAAAAAH!!", "AVAST!", "NO LIKETH", "HURLEYBURLY!"},
                                    pigman_beauticianp = {"EVIL 'TIS AFOOT", "SOMETHING WICKED THIS WAY COMES!", "O HORRORS"},
                                    pigman_mechanicp = {"ADIEU!", "I AM TOO MUCH IN THE FEAR", "MOST FOUL! MOST FOUL!"},
                                    pigman_mayorp = {"MOST HORRIBLE", "O' CURSED SPITE", "THIS BE MADNESS"},
                                    pigman_royalguardp = {"ONCE MORE INTO THE BREACH!", "GUARDS PROTECT THEE!", "AVAST! AVAIL!"},
                                    pigman_royalguard_2p = {"ONCE MORE INTO THE BREACH!", "GUARDS PROTECT THEE!", "AVAST! AVAIL!"},
                                    pigman_shopkeepp = {"O HORROR! O HORROR! O HORROR!", "O SLINGS AND ARROWS!", "O OUTRAGEOUS FORTUNE!"},
                                    pigman_storeownerp = {"HEIGH, MY HEARTS!", "A PLAGUE UPON IT!", "ALL LOST!"},
                                    pigman_farmerp = {"ALL IS'T LOST!", "ADIEU! ADIEU!", "I EXEUNT!"},
                                    pigman_minerp = {"I GET ME GONE!", "O, WOE THE DAY!", "'TIS THE END!"},
                                    pigman_collectorp = {"BAD THINGS ARE NIGH!", "ME PROTEST SO MUCH!", "I WANT'ST NOT TO DIE!"},
                                    pigman_bankerp = {"O MONSTROUS!", "O STRANGE!", "HELP'TH ME!"},
                                    pigman_floristp = {"OUT! OUT!", "TAKE ARMS!", "SOMETHING WICKED THIS WAY COMES!"}, 
                                    pigman_eruditep = {"O CURSE'D SPITE!", "GO AWAY'TH!", "MOST UNKIND!"},
                                    pigman_hatmakerp = {"OUT! OUT!", "SAVETH ME!", "MOST HORRIBLE! MOST STRANGE!"},
                                    pigman_professorp = {"CRY YOU MERCY!", "MOST HORRIBLE! MOST STRANGE!", "IT COMETH FOR US!"},
                                    pigman_hunterp = {"SOUND AND FURY!", "HOWL, HOWL, HOWL, HOWL!", "ALL IS'T LOST!"},
                                }
    STRINGS.CITY_PIG_TALK_PANICFIRE = {
                                    DEFAULT = {"IT BURN-ETH!", "FIRE BURN AND PIGGY BUBBLES", "FIGHT FIRE WITH WATER!", "SOMETHING FIREY THIS WAY COMES!", "FIRE FIRE FIRE"},
                                }
    STRINGS.CITY_PIG_TALK_FIND_MEAT = {
                                    DEFAULT = {"IS'T FOOD!", "'TIS FOOD I SEE BEFORE ME?!", "I EAT'TH!", "FOOD TIME IS NIGH!"},
                                    pigman_beauticianp = {"FOOD BE THE FOOD OF LOVE", "I HATH STOMACH FOR IT", "VERILY I EAT",},
                                    pigman_mechanicp = {"THIS FOOD I SEE BEFORE ME?", "WELL SERVED", "SOMETHING YUMMY THIS WAY COMES"},
                                    pigman_mayorp = {"'TIS A DISH FIT FOR PIGS", "GIVE'ST MAYOR MINE DUE"},
                                    pigman_royalguardp = {"EAT OR NOT TO EAT, THERE BE NO QUESTION", "FOODS FOR'ST PIGGY"},
                                    pigman_royalguard_2p = {"TO MINE OWN BELLY BE TRUE!", "'TIS FOOD! 'TIS FOOD!", "HUZZA!"},
                                    pigman_shopkeepp = {"TO MINE OWN BELLY BE TRUE", "ALLS WELL THAT ENDS IN BELLY",},
                                    pigman_storeownerp = {"'TIS EATS!", "I EATS!", "SOMETHING YUMMY THIS WAY COMES"},
                                    pigman_farmerp = {"MMMM...FOOD MOST FOUL!", "FOR MINE FAT PAUNCH!", "BELLY BURN, AND FOOD BUBBLE"},
                                    pigman_minerp = {"FOOD BE THE SOUL OF FOOD", "WOULD IT WERE IN MY BELLY", "MARRY THOUGH I LOVETH FOODS!"},
                                    pigman_collectorp = {"'TIS SLOP! 'TIS FOOD!", "WHENCE COME THIS FOOD?", "PRITHEE, LET ME EAT!"},
                                    pigman_bankerp = {"THE FOOD'S THE THING", "ZOUNDS, FOR MINE FAT PAUNCH!", "WHENCE COMES THIS FOOD?!"},
                                    pigman_floristp = {"MARRY, 'TIS FOOD!", "ALACK THE DAY,'TIS FOOD!"},
                                    pigman_eruditep = {"AY, THERE'S THE GRUB!", "INTO THE BOWELS OF MY BELLY", "FOOD GOETH IN MINE BELLY!"},
                                    pigman_hatmakerp = {"MINE BELLY NOT PROTEST TOO MUCH", "SIRRAH! MY FOOD!", "NOT SALAD DAYS!"},
                                    pigman_professorp = {"MMM... FOOD THRUST UPON ME", "WHYFORE ART THERE GROUND FOOD?"},
                                    pigman_hunterp = {"A POUND OF FLESH!", "'TIS GOOD, 'TIS GOOD INDEED!", "MUCH ADO ABOUT FOOD!"},
                                }
    STRINGS.CITY_PIG_TALK_FIND_MONEY = {    
                                    DEFAULT = {"'TIS SHINY THING!", "ALL THAT GLITTERS IS GOLD!", "YEA, THO I HAST SHINY THING!", "I GO FORTH AND BUY'ST THINGS"},
                                    pigman_beautician = {"OINC HAST PRETTYNESS!", "'TIS PRETTIES", "OINC HATH WORTH"},
                                    pigman_mechanic = {"LIKEST OINCS!", "PUT'ST OINC IN MINE POCKET", "CAN'ST ME BUY'ST STUFFS"},
                                    pigman_mayor = {"MINE-ETH!", "IS'T BUY'ST VOTES", "GIVE MAYOR MINE DUE"},
                                    pigman_royalguardp = {"OUTRAGEOUS FORTUNE!", "ME KEEP'TH", "PUT MONEY IN MINE PURSE!"},
                                    pigman_royalguard_2p = {"MINE OINC BE TRUE!", "FORTUNE SMILE 'PON ME"},
                                    pigman_shopkeep = {"IT SUFFICETH", "ME TAKETH!", "SOMETHING SHINY THIS WAY COMES"},
                                    pigman_storeowner = {"MARRY!", "ME LIKETH", "WILL SCREW THIS TO STICKING-PLACE", "ME TAKE'ST!"},
                                    pigman_farmer = {"MOST EXCELLENT FANCY", "'TIS FOR HONEST DAYS WORK", "WHATFORE ART THIS?"},
                                    pigman_miner = {"MINE'ST!", "A POUND OF OINC", "'TIS MINE"},
                                    pigman_collector = {"YEA, THO I HAST SHINY THING!", "MINE OWN PURSE BE TRUE",},
                                    pigman_banker = {"A POUND OF OINC!", "MARRY, 'TIS COIN!", "'TIS LOST OINC FOR MINE PURSE"},
                                    pigman_florist = {"HATH LOVELINESS", "'TIS PRETTY!", "ALL THAT GLITTERS IS GOLD"},
                                    pigman_erudite = {"CATCHETH MINE EYE", "HAST VALUE", "'TIS SHININESS"},
                                    pigman_hatmaker = {"ME LIKETH!", "PUT MONEY IN MINE PURSE", "WHATFORE ART THIS?"},
                                    pigman_professor = {"OUTRAGEOUS FORTUNE!", "WANT MONEYS", "CATCHETH MINE EYE"},
                                    pigman_hunter = {"'TIS MINE!", "FORTUNE SMILE 'PON ME!"},
                                }
    
    STRINGS.CITY_PIG_TALK_FORGIVE_PLAYER = {    
                                    DEFAULT = {"I SHOW QUALITY OF MERCY", "ALL IS'T FORGIVEN", "HEARTILY I FORGIVEST THEE", "A POUND OF OINCS HATH SUFFICETH"},
                                }
    STRINGS.CITY_PIG_TALK_NOT_ENOUGH = {
                                    DEFAULT = {"I WANT-ETH MORE", "DOTH NOT SUFFICETH", "I REQUIRETH MORE", "NEEDETH MORE"}, -- NEW
                                }

    STRINGS.CITY_PIG_TALK_EAT_MEAT = {
                                    DEFAULT = {"NOM-ETH NOM-ETH, NOM-ETH", "O FOOD! O SLOP!", "MUNCH'D, AND MUNCH'D, AND MUNCH'D"},
                                  
                                }
    STRINGS.CITY_PIG_TALK_GO_HOME = {
                                    DEFAULT = {"ANON! ADIEU!", "I GET ME TO BED!"},
                                    pigman_beauticianp = {"MOST HUMBLY I LEAVES", "I SLEEP", "PERCHANCE I DREAMS"},
                                    pigman_mechanicp = {"FARES'T THEE WELL", "MY KINGDOM FOR SOME JAMMIES", "GOOD EVENTIME"},
                                    pigman_mayorp = {"FAIR ME WELL", "I MAKE ME BEDFELLOWS", "MY DREAMS MAY COME"},
                                    pigman_shopkeepp = {"GET ME TO A BEDDY-BYES", "I GO MY CHAMBERS", "NIGHTY NIGHTS, SWEET UNPIGS"},
                                    pigman_storeownerp = {"I SEE WHAT DREAMS MAY COME", "ADIEU ADIEU", "REMEMBER ME"},
                                    pigman_farmerp = {"UNPIG, GOOD NIGHT", "PARTING SUCH SWEET SORROW", "TIL IT BE MORROW"},
                                    pigman_minerp = {"TIL TOMORROW AND TOMORROW AND TOMORROW", "I SLUMBER'DING", "TO SLEEP OR NOT TO SLEEP?"},
                                    pigman_collectorp = {"GOOD NIGHT UNTO YOU ALL", "SWEET GOOD NIGHT!", "I BID ADIEU TO YOUS"},
                                    pigman_bankerp = {"ADIEU, UNPIG", "FARE THEE WELL", "GOOD PIGS, LET'S RETIRE"},
                                    pigman_floristp = {"GENTLE NIGHTY-NIGHTS", "ONCE MORE UNTO MY JAMMIES", "ANON, GOOD NIGHT"},
                                    pigman_eruditep = {"NOW IS NIGHTTIMES OF OUR DISCONTENT", "I BID ADIEUS", "UNTIL THE MORROW"},
                                    pigman_hatmakerp = {"WHEREFORE ART MY JAMMIES?", "ALAS, I DEPART", "I BID THEE NIGHTY NIGHTS"},
                                    pigman_professorp = {"LIGHT THROUGH MY WINDOW BREAKS", "TIS WITCHING TIME OF NIGHT", "I TAKING MY LEAVE"},
                                    pigman_hunterp = {"ME DREAM A DREAM TONIGHT", "MY TOO TIRED FLESH GOES SLEEPIES", "SEE YOU ON THE MORROW"},
                                }
    STRINGS.CITY_PIG_TALK_FIX = {
                                    DEFAULT = {"ALL FIX'D!", "I DO'ST GOOD FIXINGS!"},
                                    pigman_beautician = {"I FIXETH NOT", "GET THEE TO A MECHANIC"},
                                    pigman_mechanic = {"I MAKETH NICE NICE", "BUILD, BUILD", "I USETH MINE HAMMER GOOD"},
                                    pigman_mayor = {"MAYOR SHALL NOT FIXETH","I FIXETH NOT", "GET THEE TO A MECHANIC"},
                                    
                                }
    STRINGS.CITY_PIG_GUARD_TALK_TORCH = {
                                    DEFAULT = {"BURN BRIGHT THE TORCHES!", "LIGHT THE TORCHES!", "BURN, TORCHES, CLEAR AND BRIGHT!"},
                                }
    STRINGS.CITY_PIG_GUARD_TALK_FIGHT = {
                                    DEFAULT = {"I STAB AT THEE!", "HAVE AT THEE!", "AWAY, CUR! AWAY!"},
                                }
    STRINGS.CITY_PIG_GUARD_TALK_GOHOME = {
                                    DEFAULT = {"STAND HO!", "WHOFORE IS THAT?", "WHATFORE THAT?", "WHAT HO!"},
                                }
    STRINGS.CITY_PIG_GUARD_TALK_LOOKATWILSON = {
                                    DEFAULT = {"MAKE NOT TROUBLES", "WHOFORE GO'ST THERE?", "TRESPASS NOT!",},
                                }
    STRINGS.CITY_PIG_GUARD_LIGHT_TORCH = {
                                    DEFAULT = {"ME LIGHT A FIERY TORCH", "TORCHES, TORCHES!", "CURFEW'S RUNGETH"},
                                }
    STRINGS.CITY_PIG_TALK_REFUSE_GIFT = {
                                    DEFAULT = {"HAVE THEE %s? WILL PAY"},                                   
                                }                                
    STRINGS.CITY_PIG_TALK_REFUSE_GIFT_DELAY = {
                                    DEFAULT = {"COMES'T BACK IN %s DAYS"},                                   
                                }                                
    STRINGS.CITY_PIG_TALK_REFUSE_GIFT_DELAY_TOMORROW = {
                                    DEFAULT = {"COME'ST BACK ON THE MORROW"},                                   
                                }
    STRINGS.CITY_PIG_TALK_REFUSE_PURPLEGEM = {
                                    DEFAULT = {"NAY! HAST THE SCARY BAD MAGICS!", "TAKETH IT AWAY!"},                                   
                                }                                                                                                
    STRINGS.CITY_PIG_TALK_RELIC_GIFT = {
                                    DEFAULT = {"TAKEST TO YON MUSEUM", "THE STY BE THE PLACE FOR IT"},            -- \"THE STY\" 
                                }
    STRINGS.CITY_PIG_TALK_TAKE_GIFT = {
                                    DEFAULT = {"MANY THANKS, GIVES'T THOU MORE PLEASE"},                                   
                                }
    STRINGS.CITY_PIG_TALK_GIVE_REWARD = {
                                    DEFAULT = {"A WORTHY JOB. TAKE THEE REWARD", "'TIS NOBLE JOB THEE DO", "A FINE JOB"},                                   
                                }
    STRINGS.CITY_PIG_TALK_GIVE_TRINKET_REWARD = {
                                    DEFAULT = {"OH, HOW LOVELY! TAKE THIS GIFT"},                                   
                                }     
    STRINGS.CITY_PIG_TALK_REFUSE_TRINKET_GIFT = {
                                    DEFAULT = {"NO MORE JUNK, THANK YOU"},                                   
                                }                             
    STRINGS.CITY_PIG_TALK_REFUSE_PRICELESS_GIFT = {
                                    DEFAULT = {"NO 'TIS PRICELESS!","IT IS FOUND! MUST FIND QUEEN","I CANNOT TAKE, BELONG TO ROYALTY"},                                   
                                }                                                        
    STRINGS.CITY_PIG_TALK_GIVE_RELIC_REWARD = {
                                    DEFAULT = {"MOST EXCELLENT!", "IS'T TREASURE!", "FROM YON OLDEN TIMES!"},                                   
                                }                              
    STRINGS.CITY_PIG_GUARD_TALK_ANGRY_PLAYER = {
                                    DEFAULT = {"HAS'T THEE RETURNED?!", "CUR! VILLAIN!", "OUT! FLEE!", "YOU A SEA OF TROUBLES!"},
                                }
    STRINGS.CITY_PIG_GUARD_TALK_RESCUE = {
                                    DEFAULT = { "I HELPEST THOU!", "O! THOU STUCK’DED", "NEED’ST HELP?" },
                                }
    STRINGS.CITY_PIG_TALK_ATTEMPT_TRADE = {
                                    DEFAULT = {"WHAT HATH THEE?", "THY WANTS'T TRADES?", "MAKE'ST THEE DEALS?"},
                                }

    STRINGS.CITY_PIG_SHOPKEEPER_NOT_ENOUGH = {"THY LACKETH THE OINCS", "GET THEE MORE OINCS"}
    STRINGS.CITY_PIG_SHOPKEEPER_DONT_HAVE = {"BRINGETH THEE ITEM", "THOU NEED'ST ITEM", "OINCS NOT SUFFICETH, ONLY ITEM"}
    STRINGS.CITY_PIG_SHOPKEEPER_SALE = {"MY THANKS", "A FINE EXCHANGE", "MANY THANKS", "THOU GOOD UNPIG"}
    
    STRINGS.CITY_PIG_SHOPKEEPER_ROBBED = {"WHOFORE HAST DONE THIS?!", "ROBBED! ROBBED! ROBBED!","OH THE PIGANITY!", "REVENGE!"}
    
    STRINGS.CITY_PIG_SHOPKEEPER_GREETING = {
                                    DEFAULT = {"WHAT SAY YOU, UNPIG?","THOU LOOK'ST FOR THINGS?","I HATH THEE WARES","BUY'ST THOU STUFFS TODAY?"},
                                    pigman_mayor_shopkeep = {"NEED'ST THOU A HOUSE?", "NEED'ST GUARD?", "WANT'ST THOU TO DWELL'ETH HERE?",},
                                    pigman_beautician = {"LOOKETH AT MINE MEDICINES", "THOU NEED'ST MEDICINES?", "NEED'ST THINGS FOR BOO-BOOS?"},
                                    pigman_mechanic = {"HAS'T THOU THINGS TO FIX?", "I FIXETH", "I FIXETH BROKEN THING", "YOU NEEDETH FIXINGS?"},
                                    pigman_miner = {"SELLETH ROCKS?", "I LIKETH ROCKS", "SELLEST ME THINE ROCKS",},
                                    pigman_collector = {"HAST THOU STRANGE THINGS?", "I DEALETH WITH THINGS O' STRANGENESS", "WANTS THOU STRANGE THINGS?"},
                                    pigman_banker = {"HAST THOU JEWELS?", "ME GIVETH OINCS FOR JEWELS", "ME LIKETH SPARKLY JEWELS"},
                                    pigman_florist = {"THOU NEEDS OF SEEDS?", "NEEDETH STUFFS FOR PLANTINGS?", "HAST THOU PLOP?", "HAST THOU PETALS?"},
                                    pigman_erudite = {"NEEDETH MAGIC THINGS?", "I SELL MAGIC THINGS?", "I SELLEST THINGS BAD DREAMS ARE MADE ON"},
                                    pigman_hatmaker = {"HATS? NEED'ST THOU HATS?", "NEEDST THING TO COVER THINE HEAD?", "BUY'ST THY HATS FROM ME"},
                                    pigman_professor = {"IN NEED'ST OF OLD THINGS?", "I LIKETH RELICS FROM YON TEMPLES", "NEED'ST THOU RELICS?",},
                                    pigman_hunter = {"IN NEED O' WEAPONS", "I SELL'ST SMASHY THINGS", "NEEDST THOU MURDERING THINGS?"},
                                }


    STRINGS.CITY_PIG_TALK_FIESTA = {
                                    DEFAULT = {"HUZZAH! HOORAY","'TIS SWINE FIESTA DAYS!","BAD GUYS GO-ETH AWAY","PIGGIES COME OUT-ETH TO PLAY!"},
                                }
    STRINGS.CITY_PIG_TALK_APORKALYPSE_REWARD = {
                                    DEFAULT = {"THOU SAVETH US!","TAKEST THOU REWARD!","THOU IS'T GOOD!","APORKALYPSE 'TIS DONE!"},
                                }                                

    --"WELCOME","LOOKING FOR THING?","I HAVE WARES","YOU BUY TODAY?"},

    STRINGS.CITY_PIG_SHOPKEEPER_CLOSING = {"IS'T THE TIME O' CLOSINGS","COME BACK ON THE 'MORROW","ME CLOSETH SHOP", "ADIEU, ADIEU, REMEMBER ME", "ME EXEUNT"}

    STRINGS.CITY_PIG_COLLECTOR_TRADE = "ODDITIES"
    STRINGS.CITY_PIG_BANKER_TRADE = "JEWELS"
    STRINGS.CITY_PIG_BEAUTICIAN_TRADE = "FEATHERS"
    STRINGS.CITY_PIG_FLORIST_TRADE = "PETALS"
    STRINGS.CITY_PIG_ERUDITE_TRADE = "NIGHTMARE FUEL"
    STRINGS.CITY_PIG_HUNTER_TRADE = "GIANT LOOT"
    STRINGS.CITY_PIG_MAYOR_TRADE = "RAREST GEM"
    STRINGS.CITY_PIG_MECHANIC_TRADE = "REFINED GOODS"
    STRINGS.CITY_PIG_PROFESSOR_TRADE = "THULECITE"
    STRINGS.CITY_PIG_HATMAKER_TRADE = "SILK"
    STRINGS.CITY_PIG_QUEEN_TRADE = "ROYAL BELONGINGS"
    --STRINGS.CITY_PIG_GUARD_TRADE = "SPEARS",
    STRINGS.CITY_PIG_STOREOWNER_TRADE = "FARM FOOD"
    STRINGS.CITY_PIG_FARMER_TRADE = "GRASS"
    STRINGS.CITY_PIG_MINER_TRADE = "ROCKS"
    STRINGS.CITY_PIG_SHOPKEEP_TRADE = "GOODS"
    STRINGS.CITY_PIG_USHER_TRADE = "BERRIES"    

    STRINGS.BANDIT_TALK_FIGHT = {"HAVE AT THEE","TAKE FROM THE RICH!","ENGUARD!"}

------------------------------------------------------------------
-- Pigman-specific speech strings
------------------------------------------------------------------

--STRINGS.CHARACTERS.PIGMANPLAYER = require "speech_pigmanplayer"

------------------------------------------------------------------
-- The default responses of characters examining the prefabs
------------------------------------------------------------------

STRINGS.CHARACTERS.GENERIC.DESCRIBE.PUGALISK_FOUNTAINP = 
{
	GENERIC = "Its a fountain.",
}

STRINGS.CHARACTERS.GENERIC.DESCRIBE.LAWNDECOR1P = 
{
	GENERIC = "Fancy.",
}
STRINGS.CHARACTERS.GENERIC.DESCRIBE.LAWNDECOR2P = STRINGS.CHARACTERS.GENERIC.DESCRIBE.LAWNDECOR1P
STRINGS.CHARACTERS.GENERIC.DESCRIBE.LAWNDECOR3P = STRINGS.CHARACTERS.GENERIC.DESCRIBE.LAWNDECOR1P
STRINGS.CHARACTERS.GENERIC.DESCRIBE.LAWNDECOR4P = STRINGS.CHARACTERS.GENERIC.DESCRIBE.LAWNDECOR1P
STRINGS.CHARACTERS.GENERIC.DESCRIBE.LAWNDECOR5P = STRINGS.CHARACTERS.GENERIC.DESCRIBE.LAWNDECOR1P
STRINGS.CHARACTERS.GENERIC.DESCRIBE.LAWNDECOR6P = STRINGS.CHARACTERS.GENERIC.DESCRIBE.LAWNDECOR1P
STRINGS.CHARACTERS.GENERIC.DESCRIBE.LAWNDECOR7P = STRINGS.CHARACTERS.GENERIC.DESCRIBE.LAWNDECOR1P

STRINGS.CHARACTERS.GENERIC.DESCRIBE.LAWNDECOR_WEREPIGP = STRINGS.CHARACTERS.GENERIC.DESCRIBE.LAWNDECOR1P
STRINGS.CHARACTERS.GENERIC.DESCRIBE.LAWNDECOR_BEEFALOP = STRINGS.CHARACTERS.GENERIC.DESCRIBE.LAWNDECOR1P
STRINGS.CHARACTERS.GENERIC.DESCRIBE.LAWNDECOR_PIGKINGP = STRINGS.CHARACTERS.GENERIC.DESCRIBE.LAWNDECOR1P
STRINGS.CHARACTERS.GENERIC.DESCRIBE.LAWNDECOR_PIGMANP = STRINGS.CHARACTERS.GENERIC.DESCRIBE.LAWNDECOR1P

STRINGS.CHARACTERS.GENERIC.DESCRIBE.CITY_LAMPP = 
{
	GENERIC = "It turns on at night.",
}

STRINGS.CHARACTERS.GENERIC.DESCRIBE.PIG_PALACEP = 
{
	GENERIC = "Home for the top pigs.",
}

STRINGS.CHARACTERS.GENERIC.DESCRIBE.MONSTER_WPNHAM = 
{
	GENERIC = "I don't think I'm supposed to have this.",
}

STRINGS.CHARACTERS.GENERIC.DESCRIBE.HALBERDP = 
{
	GENERIC = "A very useful weapon.",
}

STRINGS.CHARACTERS.GENERIC.DESCRIBE.DUNGBALLP = 
{
	GENERIC = "Just very gross.",
}

STRINGS.CHARACTERS.GENERIC.DESCRIBE.PIGBANDIT_HIDEOUTP = 
{
	GENERIC = "Whatever it holds, its probably stolen.",
}


STRINGS.CHARACTERS.GENERIC.DESCRIBE.CITYPIGHOUSEP = 
{
	GENERIC = "A normal looking house for once.",
}

STRINGS.CHARACTERS.WEBBER.DESCRIBE.GUARDTOWERP = 
{
	GENERIC = "We better be on our best behaviour!",
}

STRINGS.CHARACTERS.GENERIC.DESCRIBE.GUARDTOWERP = 
{
	GENERIC = "I take it theres rules around here.",
}

STRINGS.CHARACTERS.GENERIC.DESCRIBE.CITYHALLP = 
{
	GENERIC = "The mayor lives there.",
}

STRINGS.CHARACTERS.GENERIC.DESCRIBE.OINCP = 
{
	GENERIC = "Good for trading.",
}

STRINGS.CHARACTERS.GENERIC.DESCRIBE.OINC10P = 
{
	GENERIC = "Really good for trading.",
}

STRINGS.CHARACTERS.GENERIC.DESCRIBE.OINC100P = 
{
	GENERIC = "The star of oincs.",
}

STRINGS.CHARACTERS.GENERIC.DESCRIBE.GUARDKEYP = 
{
	GENERIC = "I don't think I'm supposed to have this.",
}

STRINGS.CHARACTERS.GENERIC.DESCRIBE.ANTMAN_WARRIORP = 
{
	GENERIC = "Best stay away from their queen.",
}

STRINGS.CHARACTERS.GENERIC.DESCRIBE.ANTMAN_WARRIOR_EGGP = 
{
	GENERIC = "We should probably smash it before it hatches.",
}

STRINGS.CHARACTERS.GENERIC.DESCRIBE.PIGHOUSE_CITYP = 
{
	GENERIC = "Pretty nice for a pig.",
}

STRINGS.CHARACTERS.GENERIC.DESCRIBE.PIG_GUARD_TOWERP = 
{
	GENERIC = "Smells of the LAW.",
}

STRINGS.CHARACTERS.GENERIC.DESCRIBE.PIG_GUARD_TOWER_PALACEP = 
{
	GENERIC = "Smells of the LAW.",
}


------------------------------------------------------------------
-- NPC strings
------------------------------------------------------------------

STRINGS.QUEENPIGNAMES=
{
    "Queen Malfalfa",
	"Queen Ashley",
	"Queen Hogsworth",
	"Queen Hambeast",
	"Queen Twiddles",
	"Queen Elizabeth",
	"Queen Angel",
	"Queen Glitterton",
	"Queen Dolly",
	"Queen Etna",
	"Queen Flonne",
	"Queen Dronya",
	"Queen Hutchens",
	"Queen Lame",
}

STRINGS.MAYORPIGNAMES=
{
    "Mayor Rei",
	"Mayor John Wattson from KleiEntertainmentForums.com",
	"Mayor Chris",
	"Mayor Battleborn",
	"Mayor Weebston",
	"Mayor Ragenado",
	"Mayor Seal",
	"Mayor Wobster",
	"Mayor Coxington",
	"Mayor StopKillingME!",
	"Mayor Todd Howard",
	"Mayor DigethDown",
	"Mayor Swinesbury",
	"Mayor Swampston",
	"Mayor Christian Weston Chandler",
	"Mayor Reggie Feels-a-me",
	"Mayor Wario",
	"Mayor Sandman",
	"Mayor Swineclops",
	"Mayor Twirlytail",
	"Mayor Steve",
	"Mayor Graverston",
	"Mayor Wade"
}


STRINGS.CITYPIGNAMES=
{
    UNISEX = {
        "Melbourne",
        "Peel",
        "Derby",
        "Palmerston",
        "Gladstone",
        "Disraeli",
        "Salisbury",
        "Kensington",
        "Conroy",
        "Greville",
        "Hastings",
        "Aberdeen",
        "Talbot",
        "Thames",
        "Stockton",
        "Darlington",
		--
		"Professional Fuelweaver Main",
		"Wheres Gorge Leo",
		"A Normal Pig",
		"Holy shit what did you say about me you lil bitch I'll have you know I solo'd swineclops with no cheats",
		"Phaze Jr.",
    },

    FEMALE =
    {
        "Elizabeth",
        "Alexandrina",
        "Alice",
        "Agnes",
        "Arabella",
        "Belle",
        "Beryl",
        "Briar",
        "Beatrice",
        "Catherine",
        "Charlotte",
        "Della",
        "Ebba",
        "Edith",
        "Flora",
        "Florence",
        "Georgette",
        "Henrietta",
        "Luella",
        "Lilian",
        "Louise",
        "Ottilie",
        "Ophelia",
        "Sophronia",
		---
		"B",
		"Trasha",
		"Kraggy",
    },
    MALE = 
    {
        "William",
        "Clarence",
        "Frederick",
        "Edward",
        "George",
        "Charles",
        "Leopold",
        "Albert",
        "Alfred",
        "Arthur",
        "Ewart",
        "Herbert",
        "Henry",
        "Charley",
        "Douglas",
        "Edison",
        "Edmund",
        "Larkin",
        "Oliver",
        "Merritt",
        "Sterling",
        "Tesla",
        "Thaddeus",
        "Wellington",
        "Gulliver", 
		--PP Names--
		"Geonardo Boxington",
		"Phaze Jr.",
		"Scorp Cultist",
    },
}

STRINGS.SHOP_NAMES =
{
	DELI = {
		"Papa Zach's",
	},
}
------------------------------------------------------------------
-- Shadow skin strings
------------------------------------------------------------------

--[[
STRINGS.SKIN_QUOTES.houndplayer_shadow = "The Rare Varg Hound"
STRINGS.SKIN_NAMES.houndplayer_shadow = "Shadow Hound"
--]]

return STRINGS