require "map/room_functions"

local Layouts = require ("map/layouts").Layouts
local StaticLayout = require ("map/static_layout")

AddRoom("PigTest", {
    colour={r=0,g=0,b=.1,a=.50},
    value = WORLD_TILES.FOREST,
    tags = { "pig_test" },
    contents =  {
        countprefabs = {
            --tree_pillarp = function () return 3 + math.random(0,1) end,
        },
                                        
        distributepercent = .4,
        distributeprefabs=
        {
            evergreen = 0.4,
        },
    }
})

--Making another room crashes.
--locks up at lasteffectivenode, same error when Keys didn't exist... do I need to make a new task?
--[[
AddRoom("PigJungle", {
    colour={r=0,g=0,b=.1,a=.50},
    value = WORLD_TILES.FOREST,
    tags = { "jungle" },
    contents =  {
        countprefabs = {
            tree_pillarp = function () return 3 + math.random(0,1) end,
        },
                                        
        distributepercent = .4,
        distributeprefabs=
        {
            junglegrassp = 0.4,
            vinesp = 0.2,
        },
    }
})]]