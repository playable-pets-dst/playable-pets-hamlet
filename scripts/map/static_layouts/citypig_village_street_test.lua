return {
  version = "1.1",
  luaversion = "5.1",
  orientation = "orthogonal",
  width = 12,
  height = 12,
  tilewidth = 64,
  tileheight = 64,
  properties = {},
  tilesets = {
    {
      name = "forge_tiles",
      firstgid = 1,
      tilewidth = 64,
      tileheight = 64,
      spacing = 0,
      margin = 0,
      image = "D:/MyStuff/Projects/DST mods/Reforged/forge_tiles.png",
      imagewidth = 512,
      imageheight = 640,
      properties = {},
      tiles = {}
    }
  },
  layers = {
    {
      type = "tilelayer",
      name = "BG_TILES",
      x = 0,
      y = 0,
      width = 12,
      height = 12,
      visible = true,
      opacity = 1,
      properties = {},
      encoding = "lua",
      data = {
        7, 7, 7, 7, 7, 2, 2, 7, 20, 20, 7, 7,
        20, 20, 20, 20, 7, 2, 2, 7, 20, 20, 7, 7,
        20, 20, 20, 20, 7, 2, 2, 7, 7, 7, 7, 7,
        20, 20, 20, 20, 7, 2, 2, 7, 20, 20, 20, 20,
        7, 7, 7, 7, 7, 2, 2, 7, 20, 20, 20, 20,
        20, 20, 20, 20, 7, 2, 2, 7, 20, 20, 20, 20,
        20, 20, 20, 20, 7, 2, 2, 7, 7, 7, 7, 7,
        20, 20, 20, 20, 7, 2, 2, 7, 7, 7, 20, 20,
        7, 7, 7, 7, 7, 2, 2, 7, 7, 7, 20, 20,
        7, 7, 20, 20, 7, 2, 2, 7, 20, 20, 20, 20,
        7, 7, 20, 20, 7, 2, 2, 7, 20, 20, 20, 20,
        7, 7, 7, 7, 7, 2, 2, 7, 7, 7, 7, 7
      }
    },
    {
      type = "objectgroup",
      name = "FG_OBJECTS",
      visible = true,
      opacity = 1,
      properties = {},
      objects = {
        {
          name = "",
          type = "pighouse_cityp",
          shape = "rectangle",
          x = 52,
          y = 414,
          width = 0,
          height = 0,
          visible = true,
          properties = {}
        },
        {
          name = "",
          type = "pighouse_cityp",
          shape = "rectangle",
          x = 52,
          y = 152,
          width = 0,
          height = 0,
          visible = true,
          properties = {}
        },
        {
          name = "",
          type = "pighouse_cityp",
          shape = "rectangle",
          x = 710,
          y = 286,
          width = 0,
          height = 0,
          visible = true,
          properties = {}
        },
        {
          name = "",
          type = "pighouse_cityp",
          shape = "rectangle",
          x = 730,
          y = 646,
          width = 0,
          height = 0,
          visible = true,
          properties = {}
        }
      }
    }
  }
}
