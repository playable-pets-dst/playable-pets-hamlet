return {
  version = "1.1",
  luaversion = "5.1",
  orientation = "orthogonal",
  width = 10,
  height = 10,
  tilewidth = 64,
  tileheight = 64,
  properties = {},
  tilesets = {
    {
      name = "forge_tiles",
      firstgid = 1,
      tilewidth = 64,
      tileheight = 64,
      spacing = 0,
      margin = 0,
      image = "D:/MyStuff/Projects/DST mods/Reforged/forge_tiles.png",
      imagewidth = 512,
      imageheight = 640,
      properties = {},
      tiles = {}
    }
  },
  layers = {
    {
      type = "tilelayer",
      name = "BG_TILES",
      x = 0,
      y = 0,
      width = 10,
      height = 10,
      visible = true,
      opacity = 1,
      properties = {},
      encoding = "lua",
      data = {
        7, 7, 7, 7, 7, 7, 7, 7, 7, 7,
        7, 7, 7, 7, 2, 2, 7, 7, 7, 7,
        7, 7, 7, 2, 2, 2, 2, 7, 7, 7,
        7, 7, 2, 2, 7, 7, 2, 2, 7, 7,
        7, 2, 2, 7, 7, 7, 7, 2, 2, 7,
        7, 2, 2, 7, 7, 7, 7, 2, 2, 7,
        7, 7, 2, 2, 7, 7, 2, 2, 7, 7,
        7, 7, 7, 2, 2, 2, 2, 7, 7, 7,
        7, 7, 7, 7, 2, 2, 7, 7, 7, 7,
        7, 7, 7, 7, 7, 7, 7, 7, 7, 7
      }
    },
    {
      type = "objectgroup",
      name = "FG_OBJECTS",
      visible = true,
      opacity = 1,
      properties = {},
      objects = {
        {
          name = "Town Center",
          type = "tree_pillarp",
          shape = "rectangle",
          x = 320,
          y = 320,
          width = 0,
          height = 0,
          visible = true,
          properties = {}
        },
        {
          name = "",
          type = "pighouse_cityp",
          shape = "rectangle",
          x = 48,
          y = 324,
          width = 0,
          height = 0,
          visible = true,
          properties = {}
        },
        {
          name = "",
          type = "pighouse_cityp",
          shape = "rectangle",
          x = 320,
          y = 50,
          width = 0,
          height = 0,
          visible = true,
          properties = {}
        },
        {
          name = "",
          type = "pighouse_cityp",
          shape = "rectangle",
          x = 586,
          y = 318,
          width = 0,
          height = 0,
          visible = true,
          properties = {}
        },
        {
          name = "",
          type = "pighouse_cityp",
          shape = "rectangle",
          x = 320,
          y = 586,
          width = 0,
          height = 0,
          visible = true,
          properties = {}
        }
      }
    }
  }
}
