require ("map/rooms/forest/testrooms")
AddTask("PigCity", {
		locks={LOCKS.PIGCITYTEST},
		keys_given={KEYS.PIGCITYTEST},
		--region_id = "pigtest",
		level_set_piece_blocker = true,
		room_choices={
			["PigTest"] = 1,
            --["PigJungle"] = 1,
		},
		room_bg=WORLD_TILES.FOREST,
		background_room="PigTest",
		colour={r=.0,g=.0,b=.1,a=1}
	})