
------------------------------------------------------------------
-- Variables
------------------------------------------------------------------
local _G = GLOBAL
local require = _G.require
local Ingredient = _G.Ingredient
--modimport("scripts/ppham_techtrees.lua")
--[[
modimport "ctt.lua"

AddNewTechTree("PIG_SHOP_PRODUCEP", 1)
AddNewTechTree("PIG_SHOP_DELIP", 1)
AddNewTechTree("PIG_SHOP_FLORISTP", 1)
AddNewTechTree("PIG_SHOP_HOOFSPAP", 1)
AddNewTechTree("PIG_SHOP_ANTIQUITIESP", 1)
AddNewTechTree("PIG_SHOP_ARCANEP", 1)
AddNewTechTree("PIG_SHOP_WEAPONSP", 1)
AddNewTechTree("PIG_SHOP_HATSHOPP", 1)
AddNewTechTree("PIG_SHOP_BANKP", 1)
AddNewTechTree("PIG_SHOP_GENERALP", 1)
-- Add it to the tech trees

local TechTree = require("techtree")
--table.insert(TechTree.AVAILABLE_TECH, "PIG_SHOP_PRODUCEP")


-- Our tab
-- String and ID, num sort, atlas, image, owner tag, crafting station
local pig_shop_produce = AddRecipeTab("Grocery", 739, "images/inventoryimages/grocery.xml", "grocery.tex", nil, true)
local pig_shop_deli = AddRecipeTab("Deli", 738, "images/inventoryimages/deli.xml", "deli.tex", nil, true)
local pig_shop_florist = AddRecipeTab("Florist", 737.5, "images/inventoryimages/floral.xml", "floral.tex", nil, true)
local pig_shop_hoofspa = AddRecipeTab("Spa", 737, "images/inventoryimages/spa.xml", "spa.tex", nil, true)
local pig_shop_bank = AddRecipeTab("Banking", 736, "images/inventoryimages/pig_shop.xml", "pig_shop.tex", nil, true)
local pig_shop_arcane = AddRecipeTab("Arcane", 735, "images/inventoryimages/arcane.xml", "arcane.tex", nil, true)
local pig_shop_hatshop = AddRecipeTab("Hat Shop", 734, "images/inventoryimages/hatshop.xml", "hatshop.tex", nil, true)
local pig_shop_weaponshop = AddRecipeTab("Weapon Shop", 733, "images/inventoryimages/weaponshop.xml", "weaponshop.tex", nil, true)
local pig_shop_antiquities = AddRecipeTab("Oddities", 732, "images/inventoryimages/oddities.xml", "oddities.tex", nil, true)
local pig_shop_general = AddRecipeTab("General", 731, "images/inventoryimages/toolshop.xml", "toolshop.tex", nil, true)


-- Replace the ID for a the name
--GLOBAL.STRINGS.TABS.PIG_SHOP_PRODUCEP = "Shopping"


-- Our science levels (set to one since one prototyper is enough)
GLOBAL.TECH.PIG_SHOP_PRODUCEP_ONE = {PIG_SHOP_PRODUCEP = 1}
GLOBAL.TECH.PIG_SHOP_DELIP_ONE = {PIG_SHOP_DELIP = 1}
GLOBAL.TECH.PIG_SHOP_FLORISTP_ONE = {PIG_SHOP_FLORISTP = 1}
GLOBAL.TECH.PIG_SHOP_HOOFSPAP_ONE = {PIG_SHOP_HOOFSPAP = 1}
GLOBAL.TECH.PIG_SHOP_BANKP_ONE = {PIG_SHOP_BANKP = 1}
GLOBAL.TECH.PIG_SHOP_ARCANEP_ONE = {PIG_SHOP_ARCANEP = 1}
GLOBAL.TECH.PIG_SHOP_HATSHOPP_ONE = {PIG_SHOP_HATSHOPP = 1}
GLOBAL.TECH.PIG_SHOP_WEAPONSHOPP_ONE = {PIG_SHOP_WEAPONSP = 1}
GLOBAL.TECH.PIG_SHOP_ANTIQUITIESP_ONE = {PIG_SHOP_ANTIQUITIESP = 1}
GLOBAL.TECH.PIG_SHOP_GENERALP_ONE = {PIG_SHOP_GENERALP = 1}


-- Cave machine gets same bonuses as PIG_SHOP_PRODUCEP_ONE so it unlocks it
GLOBAL.TUNING.PROTOTYPER_TREES.PIG_SHOP_PRODUCEP = TechTree.Create({PIG_SHOP_PRODUCEP = 1})
GLOBAL.TUNING.PROTOTYPER_TREES.PIG_SHOP_DELIP = TechTree.Create({PIG_SHOP_DELIP = 1})
GLOBAL.TUNING.PROTOTYPER_TREES.PIG_SHOP_FLORISTP = TechTree.Create({PIG_SHOP_FLORISTP = 1})
GLOBAL.TUNING.PROTOTYPER_TREES.PIG_SHOP_HOOFSPAP = TechTree.Create({PIG_SHOP_HOOFSPAP = 1})
GLOBAL.TUNING.PROTOTYPER_TREES.PIG_SHOP_BANKP = TechTree.Create({PIG_SHOP_BANKP = 1})
GLOBAL.TUNING.PROTOTYPER_TREES.PIG_SHOP_ARCANEP = TechTree.Create({PIG_SHOP_ARCANEP = 1})
GLOBAL.TUNING.PROTOTYPER_TREES.PIG_SHOP_HATSHOPP = TechTree.Create({PIG_SHOP_HATSHOPP = 1})
GLOBAL.TUNING.PROTOTYPER_TREES.PIG_SHOP_WEAPONSP = TechTree.Create({PIG_SHOP_WEAPONSP = 1})
GLOBAL.TUNING.PROTOTYPER_TREES.PIG_SHOP_ANTIQUITIESP = TechTree.Create({PIG_SHOP_ANTIQUITIESP = 1})
GLOBAL.TUNING.PROTOTYPER_TREES.PIG_SHOP_GENERALP = TechTree.Create({PIG_SHOP_GENERALP = 1})]]


local PlayablePets = GLOBAL.PlayablePets
local SETTING = GLOBAL.PP_SETTINGS
local MOBTYPE = GLOBAL.PP_MOBTYPES

local STRINGS = require("ppham_strings")

local TUNING = require("ppham_tuning")

GLOBAL.PPHAM_FORGE = require("ppham_forge")

------------------------------------------------------------------
-- Configuration Data
------------------------------------------------------------------

PlayablePets.Init(env.modname)
GLOBAL.PPHAM_SKINS = false



GLOBAL.PUGSEG = GetModConfigData("PugSegHam")

-- For preventing Growth on mobs that grow into disabled mobs

------------------------------------------------------------------
-- Prefabs
------------------------------------------------------------------

PrefabFiles = {
	-------Houses--------
	"citypighousep",
	"mandrakehousep",
	"anthillp",
	"roc_bodyp",
	"dungballp",
	"pigbandit_hideoutp",
	-------Tools---------
	"monster_wpnham",
	"poisonbubblep",
	"oincp",
	"halberdp",
	"pigcrownp",
	--forge
	"poopweapon_forge",
	"blunderbuss_projectilep",
	-------Mobs----------
	"pigman_cityp",
	"pig_guard_towerp",
	"pighouse_shopp",
	"citypighousep_npc",
	"antman_warriorp",
	"antman_warrior_eggp",
	"herald_minionsp",
	-------Decor---------
	"tree_pillarp",
	"pphambuffs",
	"city_lampp",
	"hamlet_decorp",
	"hedge_itemp",
	"ppevents_chicken",
	-------Fx------------
	"laser_ringp",
	"laserp",
	"ancient_hulkp_util",
	"pugaliskp_fx",
	"herald_meteorp",
	-----Experimental---
	"pig_footsoldierp",
	"spirit_ashesp",
	"spirit_bellp",
}

-- Centralizes a number of related functions about playable mobs to one convenient table
-- Name is the prefab name of the mob
-- Fancyname is the name of the mob, as seen in-game
-- Gender is fairly self-explanatory
-- Skins is a list of any custom mob skins that may be available, using the Modded Skins API by Fidoop
-- Skins are not required for mobs to function, but they will display a custom portrait when the mob is examined by a player
GLOBAL.PPHAM_MobCharacters = {
	scorpionp        = { fancyname = "Scorpion",           gender = "MALE",   mobtype = {}, skins = {}, forge = true },
	robinp        = { fancyname = "Ro Bin",           gender = "MALE",   mobtype = {}, skins = {}, forge = true  },
	parrot2p        = { fancyname = "Blue Parrot",           gender = "MALE",   mobtype = {}, skins = {}, forge = true  },
	kingfisherp        = { fancyname = "Kingfisher",           gender = "MALE",   mobtype = {}, skins = {}, forge = true  },
	pidgeon2p        = { fancyname = "City Pigeon",           gender = "MALE",   mobtype = {}, skins = {}, forge = true  },
	pigghostp        = { fancyname = "Pig Ghost",           gender = "ROBOT",   mobtype = {}, skins = {}, forge = true  },
	pikop        = { fancyname = "Piko",           gender = "MALE",   mobtype = {}, skins = {}, forge = true  },
	pogp        = { fancyname = "Pog",           gender = "FEMALE",   mobtype = {}, skins = {}, forge = true  },
	dungbeetlep        = { fancyname = "Dungbeetle",           gender = "MALE",   mobtype = {}, skins = {}, forge = true  },
	peagawkp        = { fancyname = "Peagawk",           gender = "FEMALE",   mobtype = {}, skins = {}, forge = true  },
	grabbingvinep        = { fancyname = "Vine",           gender = "MALE",   mobtype = {}, skins = {} },
	gnatp        = { fancyname = "Gnats",           gender = "ROBOT",   mobtype = {}, skins = {}, forge = true  },
	pangoldenp        = { fancyname = "Pangolden",           gender = "MALE",   mobtype = {}, skins = {}, forge = true  },
	frog2p        = { fancyname = "Poison Dartfrog",           gender = "MALE",   mobtype = {}, skins = {}, forge = true  },
	billp        = { fancyname = "Platapine",           gender = "MALE",   mobtype = {}, skins = {}, forge = true  },
	glowflyp        = { fancyname = "Glowfly",           gender = "MALE",   mobtype = {}, skins = {}, forge = true  },
	rabidbeetlep        = { fancyname = "Rabid Beetle",           gender = "MALE",   mobtype = {}, skins = {}, forge = true  },
	hippop        = { fancyname = "Hippoptamoose",           gender = "MALE",   mobtype = {}, skins = {}, forge = true  },
	weevolep        = { fancyname = "Weevole",           gender = "MALE",   mobtype = {}, skins = {}, forge = true  },
	giantgrubp        = { fancyname = "Giant Grub",           gender = "MALE",   mobtype = {}, skins = {}, forge = true  },
	snake2p        = { fancyname = "Scorpion",           gender = "MALE",   mobtype = {}, skins = {}, forge = true  },
	spidermonkeyp        = { fancyname = "Spider Monkey",           gender = "MALE",   mobtype = {}, skins = {}, forge = true  },
	spidermonkey2p        = { fancyname = "Baby Spider Monkey",           gender = "MALE",   mobtype = {}, skins = {}, forge = true  },
	thunderbirdp        = { fancyname = "Thunderbird",           gender = "MALE",   mobtype = {}, skins = {}, forge = true  },
	flytrapp        = { fancyname = "Snaptooth",           gender = "MALE",   mobtype = {}, skins = {}, forge = true  },
	vampirebatp        = { fancyname = "Vampire Bat",           gender = "MALE",   mobtype = {}, skins = {}, forge = true  },
	mandrakemanp        = { fancyname = "Elder Mandrake",           gender = "MALE",   mobtype = {}, skins = {}, forge = true  },
	pugaliskp        = { fancyname = "Pugalisk",           gender = "MALE",   mobtype = {MOBTYPE.GIANT}, skins = {}, forge = true  },
	rocp        = { fancyname = "BFB",           gender = "FEMALE",   mobtype = {MOBTYPE.GIANT}, skins = {}, forge = true  },
	citypig_malep        = { fancyname = "Pig (Male)",           gender = "MALE",   mobtype = {}, skins = {}, forge = true  },
	citypig_femalep        = { fancyname = "Pig (Female)",           gender = "FEMALE",   mobtype = {}, skins = {}, forge = true  },
	citypig_guardp        = { fancyname = "Pig Guard",           gender = "MALE",   mobtype = {}, skins = {}, forge = true  },
	citypig_queenp        = { fancyname = "Pig Queen",           gender = "FEMALE",   mobtype = {}, skins = {} },
	citypig_thiefp        = { fancyname = "Pig Thief",           gender = "MALE",   mobtype = {}, skins = {}, forge = true  },
	antmanp        = { fancyname = "Mant Drone",           gender = "MALE",   mobtype = {}, skins = {}, forge = true  },
	antguardp        = { fancyname = "Mant Warrior",           gender = "MALE",   mobtype = {}, skins = {}, forge = true  },
	antqueenp        = { fancyname = "Womant",           gender = "FEMALE",   mobtype = {MOBTYPE.GIANT}, skins = {}, forge = true  },
	ancient_robot_spiderp        = { fancyname = "Iron Hulk(Ribs)",           gender = "ROBOT",   mobtype = {}, skins = {}, forge = true  },
	ancient_robot_clawp        = { fancyname = "Iron Hulk(Arm)",           gender = "ROBOT",   mobtype = {}, skins = {}, forge = true  },
	ancient_robot_headp        = { fancyname = "Iron Hulk(Head)",           gender = "ROBOT",   mobtype = {}, skins = {}, forge = true  },
	ancient_robot_legp        = { fancyname = "Iron Hulk(Leg)",           gender = "ROBOT",   mobtype = {}, skins = {}, forge = true  },
	ancient_hulkp        = { fancyname = "Large Iron Hulk",           gender = "ROBOT",   mobtype = {MOBTYPE.GIANT}, skins = {}, forge = true  },
	heraldp        = { fancyname = "Ancient Herald",           gender = "ROBOT",   mobtype = {MOBTYPE.GIANT}, skins = {}, forge = true  },
	--unused mobs--
	chickenp        = { fancyname = "Chicken",           gender = "MALE",   mobtype = {}, skins = {}, forge = true  },
	snapdragonp        = { fancyname = "Snapdragon",           gender = "MALE",   mobtype = {}, skins = {} },
}

-- Necessary to ensure a specific order when adding mobs to the character select screen. This table is iterated and used to index the one above
PPHAM_Character_Order = 
{
	"scorpionp", 
	"robinp", 
	"parrot2p", 
	"kingfisherp", 
	"pidgeon2p", 
	"pogp", 
	"dungbeetlep", 
	"peagawkp", 
	"pigghostp", 
	"frog2p", 
	"grabbingvinep", 
	"glowflyp", 
	"rabidbeetlep", 
	"snake2p", 
	"spidermonkey2p", 
	"spidermonkeyp", 
	"flytrapp", 
	"vampirebatp", 
	"weevolep", 
	"gnatp", 
	"pangoldenp", 
	"thunderbirdp", 
	"mandrakemanp", 
	"billp", 
	"hippop", 
	"pikop", 
	"citypig_malep", 
	"citypig_femalep", 
	"citypig_guardp", 
	"citypig_queenp", 
	"citypig_thiefp", 
	"snapdragonp",
	"ancient_robot_headp", 
	"ancient_robot_clawp", 
	"ancient_robot_spiderp", 
	"ancient_robot_legp", 
	"giantgrubp", 
	"antmanp", 
	"antguardp", 
	"antqueenp", 
	"pugaliskp", 
	"rocp", 
	"ancient_hulkp", 
	"heraldp",
	--unused mobs--
	"chickenp",
}

------------------------------------------------------------------
-- Assets
------------------------------------------------------------------

Assets = {
	---------------------------------------------------------------
	Asset("ANIM", "anim/ghost_monster_build.zip"), -- Might make it not needed on every prefab.
	Asset( "IMAGE", "images/inventoryimages/ppham_icons.tex" ),
	Asset( "ATLAS", "images/inventoryimages/ppham_icons.xml" ),
	
	--Event stuff, remove when done.
	Asset( "IMAGE", "images/inventoryimages/golden_egg.tex" ),
	Asset( "ATLAS", "images/inventoryimages/golden_egg.xml" ),
	--------------------------HOMES--------------------------------
	Asset( "IMAGE", "images/inventoryimages/citypighousep.tex" ),
	Asset( "ATLAS", "images/inventoryimages/citypighousep.xml" ),
	Asset( "IMAGE", "images/inventoryimages/mandrakehousep.tex" ),
	Asset( "ATLAS", "images/inventoryimages/mandrakehousep.xml" ),
	Asset( "IMAGE", "images/inventoryimages/anthomep.tex" ),
	Asset( "ATLAS", "images/inventoryimages/anthomep.xml" ),
	Asset( "IMAGE", "images/inventoryimages/guardhousep.tex" ),
	Asset( "ATLAS", "images/inventoryimages/guardhousep.xml" ),
	Asset( "IMAGE", "images/inventoryimages/dungballp.tex" ),
	Asset( "ATLAS", "images/inventoryimages/dungballp.xml" ),
	--Shops
	Asset( "IMAGE", "images/inventoryimages/pig_shop.tex" ),
	Asset( "ATLAS", "images/inventoryimages/pig_shop.xml" ),
	Asset( "IMAGE", "images/inventoryimages/deli.tex" ),
	Asset( "ATLAS", "images/inventoryimages/deli.xml" ),
	Asset( "IMAGE", "images/inventoryimages/spa.tex" ),
	Asset( "ATLAS", "images/inventoryimages/spa.xml" ),
	Asset( "IMAGE", "images/inventoryimages/grocery.tex" ),
	Asset( "ATLAS", "images/inventoryimages/grocery.xml" ),
	Asset( "IMAGE", "images/inventoryimages/hatshop.tex" ),
	Asset( "ATLAS", "images/inventoryimages/hatshop.xml" ),
	Asset( "IMAGE", "images/inventoryimages/floral.tex" ),
	Asset( "ATLAS", "images/inventoryimages/floral.xml" ),
	Asset( "IMAGE", "images/inventoryimages/oddities.tex" ),
	Asset( "ATLAS", "images/inventoryimages/oddities.xml" ),
	Asset( "IMAGE", "images/inventoryimages/weaponshop.tex" ),
	Asset( "ATLAS", "images/inventoryimages/weaponshop.xml" ),
	Asset( "IMAGE", "images/inventoryimages/arcane.tex" ),
	Asset( "ATLAS", "images/inventoryimages/arcane.xml" ),
	Asset( "IMAGE", "images/inventoryimages/toolshop.tex" ),
	Asset( "ATLAS", "images/inventoryimages/toolshop.xml" ),
	
	Asset( "IMAGE", "images/inventoryimages/oincp_10.tex" ),
	Asset( "ATLAS", "images/inventoryimages/oincp_10.xml" ),
	Asset( "IMAGE", "images/inventoryimages/oinc10p_10.tex" ),
	Asset( "ATLAS", "images/inventoryimages/oinc10p_10.xml" ),
	--------------------------DECOR---------------------------------
	
	----------------------------------------------------------------
	--TODO move these into their prefabs
	Asset("ANIM", "anim/metal_hulk_shiny_build_06.zip"),
	Asset("ANIM", "anim/metal_hulk_bomb_shiny_build_06.zip"),	
	
	Asset("ANIM", "anim/herald_shiny_build_07.zip"),
	
	Asset("ANIM", "anim/bat_vamp_shiny_build_03.zip"),
	Asset("ANIM", "anim/elderdrake_shiny_build_01.zip"),
	
	Asset("ANIM", "anim/scorpion_shiny_build_03.zip"),
	
	Asset("ANIM", "anim/thunderbird_shiny_build_01.zip"),
	Asset("ANIM", "anim/thunderbird_shiny_build_03.zip"),
	
	Asset("ANIM", "anim/metal_hulk_shiny_build_08.zip"),
	
	
}

local DLC_Assets = {
	--Pig City--
	Asset("ANIM", "anim/flag_post_duster_build.zip"),
    Asset("ANIM", "anim/flag_post_perdy_build.zip"),
    Asset("ANIM", "anim/flag_post_royal_build.zip"),
    Asset("ANIM", "anim/flag_post_wilson_build.zip"), 
    Asset("ANIM", "anim/pig_tower_build.zip"),
	Asset("ANIM", "anim/pig_cityhall.zip"),
	Asset("ANIM", "anim/pig_shop.zip"),
	
	Asset("ANIM", "anim/pig_bandit.zip"),
	Asset("ANIM", "anim/pig_usher.zip"),
    Asset("ANIM", "anim/pig_mayor.zip"),
    Asset("ANIM", "anim/pig_miner.zip"),
    Asset("ANIM", "anim/pig_queen.zip"),
    Asset("ANIM", "anim/pig_farmer.zip"),
    Asset("ANIM", "anim/pig_hunter.zip"),
    Asset("ANIM", "anim/pig_banker.zip"),
    Asset("ANIM", "anim/pig_florist.zip"),
    Asset("ANIM", "anim/pig_erudite.zip"),
    Asset("ANIM", "anim/pig_hatmaker.zip"),
    Asset("ANIM", "anim/pig_mechanic.zip"),
    Asset("ANIM", "anim/pig_professor.zip"),
    Asset("ANIM", "anim/pig_collector.zip"),
    Asset("ANIM", "anim/townspig_basic.zip"),
    Asset("ANIM", "anim/pig_beautician.zip"),
    Asset("ANIM", "anim/pig_royalguard.zip"),
    Asset("ANIM", "anim/pig_storeowner.zip"),
    Asset("ANIM", "anim/townspig_attacks.zip"),
    Asset("ANIM", "anim/townspig_actions.zip"),
    Asset("ANIM", "anim/pig_royalguard_2.zip"),
	Asset("ANIM", "anim/pig_royalguard_3.zip"),
	Asset("ANIM", "anim/pig_royalguard_rich.zip"),
	Asset("ANIM", "anim/pig_royalguard_rich_2.zip"),
    Asset("ANIM", "anim/townspig_shop_wip.zip"),
	----
	Asset("ANIM", "anim/pig_house_sale.zip"),	
    Asset("ANIM", "anim/player_small_house1.zip"),
    Asset("ANIM", "anim/player_large_house1.zip"),

    Asset("ANIM", "anim/player_large_house1_manor_build.zip"), 
    Asset("ANIM", "anim/player_large_house1_villa_build.zip"),    
    Asset("ANIM", "anim/player_small_house1_cottage_build.zip"),    
    Asset("ANIM", "anim/player_small_house1_tudor_build.zip"),   
    Asset("ANIM", "anim/player_small_house1_gothic_build.zip"),  
    Asset("ANIM", "anim/player_small_house1_brick_build.zip"),  
    Asset("ANIM", "anim/player_small_house1_turret_build.zip"),  
	
	Asset("ANIM", "anim/pig_townhouse1.zip"),
    Asset("ANIM", "anim/pig_townhouse5.zip"),
    Asset("ANIM", "anim/pig_townhouse6.zip"),    

    Asset("ANIM", "anim/pig_townhouse1_pink_build.zip"),
    Asset("ANIM", "anim/pig_townhouse1_green_build.zip"),

    Asset("ANIM", "anim/pig_townhouse1_brown_build.zip"),
    Asset("ANIM", "anim/pig_townhouse1_white_build.zip"),

    Asset("ANIM", "anim/pig_townhouse5_beige_build.zip"),
    Asset("ANIM", "anim/pig_townhouse6_red_build.zip"),
    
    Asset("ANIM", "anim/pig_farmhouse_build.zip"),
	Asset("ANIM", "anim/elderdrake_house.zip"),
	---
	--Robots
	Asset("ANIM", "anim/metal_spider.zip"),
    Asset("ANIM", "anim/metal_claw.zip"),
    Asset("ANIM", "anim/metal_leg.zip"),
    Asset("ANIM", "anim/metal_head.zip"),
	---
	--Roc
	Asset("ANIM", "anim/roc_head_build.zip"),
	Asset("ANIM", "anim/roc_head_basic.zip"),
	Asset("ANIM", "anim/roc_head_actions.zip"),
	Asset("ANIM", "anim/roc_head_attacks.zip"),
	Asset("ANIM", "anim/roc_shadow.zip"),
	Asset("ANIM", "anim/roc_leg.zip"),
	Asset("ANIM", "anim/roc_tail.zip"),

	Asset("ANIM", "anim/topiary.zip"), 
	Asset("ANIM", "anim/topiary_beefalo_build.zip"), 
	Asset("ANIM", "anim/topiary_pigking_build.zip"),
	Asset("ANIM", "anim/topiary_pigman_build.zip"),
	Asset("ANIM", "anim/topiary_werepig_build.zip"),
	Asset("ANIM", "anim/topiary01_build.zip"),
	Asset("ANIM", "anim/topiary02_build.zip"),
	Asset("ANIM", "anim/topiary03_build.zip"),
	Asset("ANIM", "anim/topiary04_build.zip"),
	Asset("ANIM", "anim/topiary05_build.zip"),
	Asset("ANIM", "anim/topiary06_build.zip"),
	Asset("ANIM", "anim/topiary07_build.zip"),
	
	Asset("ANIM", "anim/hedge.zip"),
	Asset("ANIM", "anim/hedge1_build.zip"),
	Asset("ANIM", "anim/hedge2_build.zip"),
	Asset("ANIM", "anim/hedge3_build.zip"),

	Asset("SOUNDPACKAGE", "sound/dontstarve_DLC003.fev"), 
	Asset( "SOUND", "sound/DLC003_sfx.fsb"), 
}

if not _G.HAMLET_PP_MODE then
	--Rely on DLC mods to load assets for us, if not enabled, load it ourselves.
	for i, v in ipairs(DLC_Assets) do
		table.insert(Assets, v)
	end
end

GLOBAL.PIG_TRADER = require("ppham_tradelist")
local make_tradeable_items =
{
	"stinger",
	"silk",
	"mosquitosack",
	--"chitin",
	--"venus_stalk",
	--"venomgland",
	"spidergland",
	"redgem",
	"bluegem",
	"greengem", 
	"orangegem", 
	"yellowgem",
	"feather_crow",
	"feather_robin",
	"feather_robin_winter",
	"moose_feather", 
	"feather_thunder",
	"thulecite",
	"boards",
	"rope",
	"cutstone",
	"papyrus",
	"bearger_fur",
	"shadowheart",
	"dragon_scales",
	"deerclops_eyeball",
	"shroom_skin",
	"minotaurhorn",
	"opalpreciousgem",
	"hivehat",
	"skeletonhat",
	"meat", 
	"watermelon", 
	"pumpkin", 
	"carrot", 
	"cave_banana", 
	"pomegranate", 
	"pomegranate_cooked",
	"corn", 
	"corn_cooked",
	"eggplant",
	"eggplant_cooked",
	"nightmarefuel",
	"rocks",
	"honey",
	"jammypreserves",
	"icecream",
	"pumpkincookie",
	"waffles",
	"berries",
	"berries_cooked",	
	"torch",
	"garlic",
	"garlic_cooked",
	"pepper",
	"pepper_cooked",
	"durian",
	"houndstooth",
	"boneshard",
	"malbatross_beak",
	"goose_feather",
	"horrorfuel",
	"purebrilliance",
	"dreadstone",
	"lunarplant_husk",
}

local function MakeTradable(inst)
	if not inst.components.tradable then
		inst:AddComponent("tradable")
	end	
end

for i, v in ipairs(make_tradeable_items) do
	AddPrefabPostInit(v, MakeTradable)
end

if _G.HAMLET_PP_MODE then
	--since our dlc prefabs don't need to exist due to DLC mods, we're adding any additional
	--code via postinits to keep them compatible with custom pp stuff.
	local function AddOincPost(inst, value)	
		inst.currencyvalue = value

		inst:AddComponent("edible")
		inst.components.edible.foodtype = "ELEMENTAL"
		inst.components.edible.hungervalue = 10
	end
	AddPrefabPostInit("oinc", function(inst) AddOincPost(inst, 1) end)
	AddPrefabPostInit("oinc10", function(inst) AddOincPost(inst, 10) end)
	AddPrefabPostInit("oinc100", function(inst) AddOincPost(inst, 100) end)
end
------------------------------------------------------------------
-- Custom Recipes
------------------------------------------------------------------
local oinc_prefab = _G.HAMLET_PP_MODE and "oinc" or "oinc1p" 
local oinc10_prefab = _G.HAMLET_PP_MODE and "oinc10" or "oinc10p"
local oinc100_prefab = _G.HAMLET_PP_MODE and "oinc100" or "oinc100p" 

-- Houses
--local citypigrecipe = AddRecipe("city_player_house", {Ingredient("boards", 10),Ingredient("cutstone", 6)}, GLOBAL.RECIPETABS.TOWN, GLOBAL.TECH.SCIENCE_TWO, "city_player_house_placer", nil, nil, 1, "civilised", "images/inventoryimages/city_player_house.xml", "city_player_house.tex")
local citypig_recipe = AddRecipe2(
        "city_player_house", --the name of the recipe
        {Ingredient("boards", 10),Ingredient("cutstone", 6)}, --ingredients
        GLOBAL.TECH.SCIENCE_TWO, --tech tier
        {
            atlas = "images/inventoryimages/citypighousep.xml", 
            image = "citypighousep.tex",
            builder_tag = "civilised",
			placer = "city_player_house_placer",
			min_spacing = 0.1,
			--nounlock = true,
			--numtogive = 2,
			--testfn = function() end,
			--product = "oinc1p",
			--build_mode = "", -- idk what this is.
			--build_distance = 1, --idk what this does either.
        },{"STRUCTURES"} 
    )
local guardtowerrecipe = AddRecipe2(
		"city_player_guardtower", --the name of the recipe
        {Ingredient("boards", 2),Ingredient("cutstone", 10),Ingredient("silk", 4) }, --ingredients
        GLOBAL.TECH.SCIENCE_TWO, --tech tier
        {
            atlas = "images/inventoryimages/guardhousep.xml", 
            image = "guardhousep.tex",
            builder_tag = "civilised",
			placer = "city_player_guardtower_placer",
			min_spacing = 0.1,
        },{"STRUCTURES"} 
    )
local mandrakerecipe = AddRecipe2(
		"mandrakehousep", --the name of the recipe
        {Ingredient("boards", 10), Ingredient("cutgrass", 40)}, --ingredients
        GLOBAL.TECH.SCIENCE_TWO, --tech tier
        {
            atlas = "images/inventoryimages/mandrakehousep.xml", 
            image = "mandrakehousep.tex",
            builder_tag = "mandrakeman",
			placer = "mandrakehousep_placer",
			min_spacing = 0.1,
        },{"STRUCTURES"} 
    )
local anthillrecipe = AddRecipe2(
		"anthillp", --the name of the recipe
        {Ingredient("twigs", 40),Ingredient("cutgrass", 40)}, --ingredients
        GLOBAL.TECH.SCIENCE_TWO, --tech tier
        {
            atlas = "images/inventoryimages/anthomep.xml", 
            image = "anthomep.tex",
            builder_tag = "ant",
			placer = "anthillp_placer",
			min_spacing = 0.1,
        },{"STRUCTURES"} 
    )
-- Other
local dungballrecipe = AddRecipe2(
		"dungballp", --the name of the recipe
        {Ingredient("poop", 10)}, --ingredients
        GLOBAL.TECH.NONE, --tech tier
        {
            atlas = "images/inventoryimages/dungballp.xml", 
            image = "dungballp.tex",
            builder_tag = "dungbeetle",
			placer = "dungballp_placer",
			min_spacing = 0.1,
        },{} 
    )
local banditrecipe = AddRecipe2(
		"pigbandit_hideoutp", --the name of the recipe
        {Ingredient("cutgrass", 4), Ingredient("feather_crow", 2), Ingredient("feather_robin", 2)}, --ingredients
        GLOBAL.TECH.NONE, --tech tier
        {
            atlas = "images/inventoryimages/pigbandit_hideoutp.xml", 
            image = "pigbandit_hideoutp.tex",
            builder_tag = "pigtheif",
			placer = "pigbandit_hideoutp_placer",
			min_spacing = 0.1,
        },{} 
    )
local oinc1recipe = AddRecipe2(
		oinc_prefab, 
        {Ingredient("goldnugget", 5)}, --ingredients
        GLOBAL.TECH.SCIENCE_ONE, --tech tier
        {
            atlas = "images/inventoryimages/oincp.xml", 
            image = "oincp.tex",
            builder_tag = "civilised",
        },{} 
    )
local oinc2recipe = AddRecipe2(
		oinc10_prefab, 
        {Ingredient(oinc_prefab, 10, "images/inventoryimages/oincp.xml")}, --ingredients
        GLOBAL.TECH.SCIENCE_ONE, --tech tier
        {
            atlas = "images/inventoryimages/oinc10p.xml", 
            image = "oinc10p.tex",
            builder_tag = "civilised",
        },{} 
    )
local oinc3recipe = AddRecipe2(
		oinc100_prefab, 
        {Ingredient(oinc10_prefab, 10, "images/inventoryimages/oinc10p.xml")}, --ingredients
        GLOBAL.TECH.SCIENCE_ONE, --tech tier
        {
            atlas = "images/inventoryimages/oinc100p.xml", 
            image = "oinc100p.tex",
            builder_tag = "civilised",
        },{} 
    )
oinc1recipe.no_deconstruction = true
oinc2recipe.no_deconstruction = true
oinc3recipe.no_deconstruction = true

if not _G.HAMLET_PP_MODE then
	for i = 1, 3 do
		AddRecipe2(
			"wall_hedge"..i.."_itemp", 
			{Ingredient("cutgrass", 5), Ingredient("poop", 2)},
			GLOBAL.TECH.SCIENCE_TWO, --tech tier
			{
				atlas = "images/inventoryimages/ppham_icons.xml", 
				image = "wall_hedge"..i.."_itemp.tex",
				numtogive = 4,
				builder_tag = "civilised",
			},{"STRUCTURE"} 
		)
	end

	for i = 1, 7 do
		AddRecipe2(
			"lawndecor"..i.."p", 
			{Ingredient("cutgrass", 5), Ingredient("poop", 2)},
			GLOBAL.TECH.SCIENCE_TWO, --tech tier
			{
				atlas = "images/inventoryimages/ppham_icons.xml", 
				image = "lawndecor"..i.."p.tex",
				placer = "lawndecor"..i.."p_placer",
				builder_tag = "civilised",
			},{"STRUCTURE"} 
		)
	end
end

local function CreateShopRecipe(name, item, amount)
	local recipe = AddRecipe2(
        name, --the name of the recipe
        {Ingredient(GLOBAL.CHARACTER_INGREDIENT.OINCS, 200, "images/inventoryimages/oincp.xml", nil, "oincp"), Ingredient("cutstone", 10), Ingredient(item, amount or 200)}, --ingredients
        GLOBAL.TECH.NONE, --tech tier
        {
            atlas = "images/inventoryimages/ppham_icons.xml", 
            image = name..".tex",
            builder_tag = "pig_royalty",
			placer = name.."_placer",
			min_spacing = 0.2,
			nounlock = true,
        },{"STRUCTURES"} 
    )
	recipe.no_deconstruction = true
	return recipe
end

local pig_shop_generalrecipe = CreateShopRecipe("pig_shop_generalp", "log")
local pig_shop_antiquitiesrecipe = CreateShopRecipe("pig_shop_antiquitiesp", "spidergland")
local pig_shop_delirecipe = CreateShopRecipe("pig_shop_delip", "meat", 100)
local pig_shop_arcanerecipe = CreateShopRecipe("pig_shop_arcanep", "purplegem", 100)
local pig_shop_floristrecipe = CreateShopRecipe("pig_shop_floristp", "poop", 150)
local pig_shop_hatshoprecipe = CreateShopRecipe("pig_shop_hatshopp", "silk")
local pig_shop_hoofsparecipe = CreateShopRecipe("pig_shop_hoofspap", "healingsalve", 100)
local pig_shop_producerecipe = CreateShopRecipe("pig_shop_producep", "carrot", 80)
local pig_shop_weaponshoprecipe = CreateShopRecipe("pig_shop_weaponsp", "houndstooth")
local pig_shop_bankrecipe = AddRecipe2(
		"pig_shop_bankp", 
        {Ingredient(oinc_prefab, 100, "images/inventoryimages/oincp.xml"), Ingredient("cutstone", 20), Ingredient("goldnugget", 30)}, --ingredients
        GLOBAL.TECH.NONE, --tech tier
        {
            atlas = "images/inventoryimages/ppham_icons.xml", 
            image = "pig_shop_bankp.tex",
            builder_tag = "pig_royalty",
			placer = "pig_shop_bankp_placer",
			min_spacing = 0.1,
			nounlock = true,
        },{"STRUCTURE"} 
    )
local pighouse_cityp = AddRecipe2(
		"pighouse_cityp", 
        {Ingredient(GLOBAL.CHARACTER_INGREDIENT.OINCS, 30, "images/oincsp.xml"), Ingredient("cutstone", 2), Ingredient("boards", 4)}, --ingredients
        GLOBAL.TECH.NONE, --tech tier
        {
            atlas = "images/inventoryimages/ppham_icons.xml", 
            image = "pighouse_cityp.tex",
            builder_tag = "pig_royalty",
			placer = "pighouse_cityp_placer",
			min_spacing = 0.1,
			nounlock = true,
        },{"STRUCTURE"} 
    )
pighouse_cityp.no_deconstruction = true
pig_shop_bankrecipe.no_deconstruction = true
--testrecipe.sortkey = -20.0
--mermrecipe.sortkey = -8.0
--walrusrecipe.sortkey = -9.0
--pigtorchrecipe.sortkey = -10.0

------------------------------------------------------------------
-- Component Overrides
------------------------------------------------------------------

------------------------------------------------------------------
-- PostInits
------------------------------------------------------------------	
AddClassPostConstruct("widgets/inventorybar", function(self)
	if GLOBAL.TheNet:GetServerGameMode() ~= "lavaarena" then
		self.ppcurrency:Show() --gets unhidden by hamlet pets.
	end
end)
---Container---
local containers = require "containers"

local params = {}

local containers_widgetsetup_base = containers.widgetsetup
function containers.widgetsetup(container, prefab, data, ...)
    local t = params[prefab or container.inst.prefab]
    if t ~= nil then
        for k, v in pairs(t) do
            container[k] = v
        end
        container:SetNumSlots(container.widget.slotpos ~= nil and #container.widget.slotpos or 0)
    else
        containers_widgetsetup_base(container, prefab, data, ...)
    end
end

containers.params["spirit_bellp"] = containers.params.oceanfishingrod

params.pigbandit_hideoutp = 
{
    widget =
    {
        slotpos = {},
        animbank = "ui_chest_3x3",
        animbuild = "ui_chest_3x3",
        pos = GLOBAL.Vector3(0, 200, 0),
        side_align_tip = 160,
    },
    type = "chest",
}
for y = 2, 0, -1 do
    for x = 0, 2 do
        table.insert(params.pigbandit_hideoutp.widget.slotpos, GLOBAL.Vector3(80 * x - 80 * 2 + 80, 80 * y - 80 * 2 + 80, 0))
    end
end

for k, v in pairs(params) do
    containers.MAXITEMSLOTS = math.max(containers.MAXITEMSLOTS, v.widget.slotpos ~= nil and #v.widget.slotpos or 0)
end

GLOBAL.FOODTYPE.POOP = "POOP"
--GLOBAL.FOODTYPE.INSECT = "INSECT"

local function MakePoopEdible(inst)
	inst:AddComponent("edible")
	inst.components.edible.foodtype = GLOBAL.FOODTYPE.POOP
	inst.components.edible.healthvalue = inst.prefab == "guano" and 20 or 10
    inst.components.edible.hungervalue = inst.prefab == "guano" and 40 or 20
    inst.components.edible.sanityvalue = 10
end

--Dungbeetle poop eating
AddPrefabPostInit("poop", MakePoopEdible)
AddPrefabPostInit("guano", MakePoopEdible)

-------------------------------------------------------
--Wardrobe stuff--

--Skin Puppet Stuff
local MobPuppets = require("ppham_puppets")
local MobSkins = require("pph_skins")
PlayablePets.RegisterPuppetsAndSkins(PPHAM_Character_Order, MobPuppets, MobSkins)
------------------------------------------------------------------
-- Commands
------------------------------------------------------------------

------------------------------------------------------------------
-- Asset Population
------------------------------------------------------------------

local assetPaths = { "bigportraits/", "images/map_icons/", "images/avatars/avatar_", "images/avatars/avatar_ghost_" }
local assetTypes = { {"IMAGE", "tex"}, {"ATLAS", "xml"} }

-- Iterate through the player mob table and do the following:
-- 1. Populate the PrefabFiles table with the mob prefab names and their skin prefabs (if applicable)
-- 2. Add an atlas and image for the mob's following assets:
-- 2.1 Character select screen portraits
-- 2.2 Character map icons
-- 2.3 ??? FIXME
-- 2.4 ??? FIXME
--for prefab, mob in pairs(GLOBAL.PPHAM_MobCharacters) do
for _, prefab in ipairs(PPHAM_Character_Order) do
	local mob = GLOBAL.PPHAM_MobCharacters[prefab]
	if PlayablePets.MobEnabled(mob, env.modname) then
		table.insert(PrefabFiles, prefab)
	end
	
	-- Add custom skin prefabs, if available
	-- Example: "dragonplayer_formal"
	for _, skin in ipairs(mob.skins) do
			table.insert(PrefabFiles, prefab.."_"..skin)
	end
	
	for _, path in ipairs(assetPaths) do
		for _, assetType in ipairs(assetTypes) do
			--print("Adding asset: "..assetType[1], path..prefab.."."..assetType[2])
			table.insert( Assets, Asset( assetType[1], path..prefab.."."..assetType[2] ) )
		end
	end
end
------------------------------------------------------------------
-- Mob Character Instantiation
------------------------------------------------------------------

-- Adds a mod character based on an individual mob
-- prefab is the prefab name (e.g. clockwork1player)
-- mob.fancyname is the mob's ingame name (e.g. Knight)
-- mob.gender is fairly self-explanatory
--for prefab, mob in pairs(GLOBAL.PPHAM_MobCharacters) do
for _, prefab in ipairs(PPHAM_Character_Order) do
	local mob = GLOBAL.PPHAM_MobCharacters[prefab]
	PlayablePets.SetGlobalData(prefab, mob)
	if PlayablePets.MobEnabled(mob, env.modname) then
		AddMinimapAtlas("images/map_icons/"..prefab..".xml")
		AddModCharacter(prefab, mob.gender)
	end
end