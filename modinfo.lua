-- This information tells other players more about the mod
name = "Playable Pets -Hamlet"
author = "Leonardo Coxington"
version = "1.574"
description = "Play as Pets/Monsters from Hamlet! \nPress the z,h,j,k,l keys when playing as mobs, they might do something special! \n\nNOTE: You will need Playable Pets Essentials enabled to be able to run this mod. \nVersion:"..version
-- This is the URL name of the mod's thread on the forum; the part after the ? and before the first & in the url
forumthread = "/topic/73911-playable-pets/"

folder_name = folder_name or "workshop-"
if not folder_name:find("workshop-") then
    name = name.." Dev."
end

-- This lets other players know if your mod is out of date, update it to match the current version in the game
api_version = 10

dst_compatible = true
dont_starve_compatible = false
reign_of_giants_compatible = false
all_clients_require_mod = true
forge_compatible = true

icon_atlas = "modicon.xml"
icon = "modicon.tex"

priority = -315

server_filter_tags = {
"mob", "mobs", "playable", "monsters", "PP", "playable pets", "hamlet", "monkey", "sharpener"
}

---------------------------------
--            DATA             --
---------------------------------

-- For initializing configuration settings to disable each mob
local availableMobs = {
	"Chicken",
	"Scorpion", 
	"Ro Bin",
	"Blue Parrot", 
	"Kingfisher", 
	"City Pigeon", 
	"Pog", 
	"Dungbeetle", 
	"Peagawk", 
	"Pig Ghost" ,
	"Poison Dartfrog", 
	"Vine",
	"Glowfly", 
	"Rabid Beetle", 
	"Viper",
	"Spider Monkey", 
	"Vampire Bat", 
	"Weevole" , 
	"Baby Spider Monkey", 
	"Hippoptamoose", 
	"Platapine", 
	"Giant Grub", 
	"Snaptooth", 
	"Elder Mandrake", 
	"Gnats", 
	"Pangolden", 
	"Thunderbird", 
	"Piko", 
	"Pig (Male)", 
	"Pig (Female)", 
	"Pig Guard", 
	"Pig Queen", 
	"Pig Thief", 
	"Snapdragon",
	"Mant Drone", 
	"Mant Warrior",
	"Womant", 
	"Pugalisk", 
	"BFB", 
	"Iron Hulk(Head)", 
	"Iron Hulk(Arm)" ,
	"Iron Hulk(Ribs)", 
	"Iron Hulk(Leg)", 
	"Large Iron Hulk", 
	"Ancient Herald"
}

local defaultSkinName = "Something Different" -- Used if a name is not specified in the skins table

-- For Modded Inventory. Please note that this modified table format requires code at the bottom of this file to make it usable for modded inventory.f
menu_assets =
{
	skins = {
		
	},
}

local SETTING = {
	OFF = "Off",
	DISABLE = "Disable",
	ENABLE = "Enable",
	LOCKED = "Locked",
	
	-- Mob Presets
	ALL_MOBS = "AllMobs",
	NO_GIANTS = "NoGiants",
	NO_BOSSES = "NoBosses",
	BOSSES_ONLY = "BossesOnly",
	GIANTS_ONLY = "GiantsOnly",
	CRAFTY_MOBS = "CraftyMobsOnly",
	
	-- Mob Houses
	HOUSE_CRAFT_ONLY = "Enable2",
	HOUSE_ON_SPAWN = "Enable1",
	HOUSE_BOTH = "Enable3",
	
	-- PvP Damage
	PVP_50_PERCENT_DMG = 0.5,
	PVP_100_PERCENT_DMG = 1.0,
	PVP_150_PERCENT_DMG = 1.5,
	PVP_200_PERCENT_DMG = 2.0,
	
	-- Misc
	HUMANOID_SANITY_ONLY = "Disable1",
	MONSTER_CHARCHANGE_ONLY = "Enable1",
}

---------------------------------
--       CONFIG OPTIONS        --
---------------------------------

configuration_options = {	
	{
		name = "PugSegHam",
		label = "Pugalisk Body Length", -- for performance.
		options = 
		{
			{description = "3",  data = 3,  hover = "Pugalisk will only have 3 bodies (best perfomance)."},
			{description = "5", data = 5, hover = "Default, matches the hamlet variant."},
			{description = "7", data = 7, hover = "Pugalisk will have 7 bodies (not recommended)."},
			{description = "10", data = 10, hover = "Pugalisk will have 10 bodies (definetely not recommended)."},
			{description = "50", data = 50, hover = "Will most likely cause problems performance wise, don't do it."},
			{description = "100", data = 100, hover = "Haha, yeah its funny, seriously, don't do it."},
			{description = "200", data = 200, hover = "Okay, look, this isn't even funny anymore. DON'T DO THIS."},
			{description = "255", data = 255, hover = "Kill your server"},
		},
		default = 5
	},
}

---------------------------------
-- TABLE POPULATION CODE BELOW --
---------------------------------

-- Automatically populate enable/disable configuration settings for mobs
local settingEnable = {
	{description = "Enabled", data = SETTING.ENABLE},
	{description = "Disabled", data = SETTING.DISABLE},
}

for i = 1, #availableMobs do	
	local configOption = {}
	configOption.name = availableMobs[i]
	configOption.label = availableMobs[i]
	configOption.options = settingEnable
	configOption.default = SETTING.ENABLE
	
	configuration_options[#configuration_options + 1] = configOption
end

-- Automatically populate the Modded Inventory skins table
for i = 1, #menu_assets.skins do
		local data = menu_assets.skins[i]
		local skinName = data.name
		
		-- Use a specified name if available. Otherwise, choose the default
		if skinName == nil then
			skinName = defaultSkinName
		end
		
		local skin = {
			name = skinName,
			desc = data[2],
			atlas = "images/skinicons/"..data[1]..".xml",
			image = data[1]..".tex",
		}
		
		menu_assets.skins[i] = skin
end